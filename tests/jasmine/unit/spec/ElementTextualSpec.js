/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Element Text", function () {
    'use strict';
    var $textInput;
    var textInput;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        $textInput = $("input[name='personHandle']");
        textInput = new QfqNS.Element.Textual($textInput);
    });

    it("should throw on wrong type", function () {
        expect(function () {
            new QfqNS.Element.Textual($("input[name='gender']"));
        }).toThrow();
    });

    it("should set text", function () {
        textInput.setValue("new value");
        expect($textInput.val()).toBe("new value");
    });

    it("should get text", function () {
        $textInput.val("externally set");
        expect(textInput.getValue()).toBe("externally set");
    });
});
