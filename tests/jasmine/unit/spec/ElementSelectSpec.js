/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Element Select", function () {
    'use strict';

    var personTitleSelect;
    var selectTest2;

    beforeAll(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        personTitleSelect = new QfqNS.Element.Select($('#personTitle'));
        selectTest2 = new QfqNS.Element.Select($('#selectTest2'));
    });

    it("should throw on wrong type", function () {
        expect(function () {
            new QfqNS.Element.Textual($("textarea[name='mytextarea']"));
        }).toThrow();
    });

    it("should get initial selected value", function () {
        $('#myForm')[0].reset();
        expect(personTitleSelect.getValue()).toEqual(["none"]);
        expect(selectTest2.getValue()).toEqual(['2']);
    });

    it("should get dynamically selected value", function () {
        $('#selectTest2 :selected').prop('selected', false);
        $('#selectTest option[value=1]').prop('selected', true);

        expect(selectTest2.getValue()).toEqual(["1"]);

        $('#personTitle :selected').prop('selected', false);
        $('#personTitle option:nth-child(3)').prop('selected', true);

        expect(personTitleSelect.getValue()).toEqual(['Prof. Dr.']);
    });

    it("should set the selected item properly", function () {
        $('#selectTest2 :selected').prop('selected', false);
        $('#selectTest option[value=3]').prop('selected', true);

        selectTest2.setValue('1');

        expect($('#selectTest2 :selected').length).toBe(1);
        expect($('#selectTest2 :selected').val()).toBe('1');

        $('#personTitle :selected').prop('selected', false);
        $('#personTitle option:nth-child(3)').prop('selected', true);

        personTitleSelect.setValue('none');

        expect($('#personTitle :selected').length).toBe(1);
        expect($('#personTitle :selected').text()).toBe('none');

    });

    it("should set options properly", function () {
        selectTest2.setValue([
            {
                value: 0xff,
                text: "0xff"
            },
            {
                value: 0xfe,
                text: "0xfe",
                selected: true
            }
        ]);

        expect($('#selectTest2 option').length).toBe(2);
        expect($('#selectTest2 option:nth-child(1)').text()).toBe('0xff');
        expect($('#selectTest2 option:nth-child(1)').val()).toBe('255');
        expect($('#selectTest2 option:nth-child(1)').prop('selected')).toBe(false);

        expect($('#selectTest2 option:nth-child(2)').text()).toBe('0xfe');
        expect($('#selectTest2 option:nth-child(2)').val()).toBe('254');
        expect($('#selectTest2 option:nth-child(2)').prop('selected')).toBe(true);

        personTitleSelect.setValue([
            {
                text: "a"
            },
            {
                text: "b",
                selected: false
            },
            {
                text: "c",
                selected: true
            },
            {
                text: "d"
            }
        ]);

        expect($('#personTitle option').length).toBe(4);
        expect($('#personTitle option:nth-child(1)').text()).toBe('a');
        expect($('#personTitle option:nth-child(1)').val()).toBe('a');
        expect($('#personTitle option:nth-child(1)').prop('selected')).toBe(false);

        expect($('#personTitle option:nth-child(2)').text()).toBe('b');
        expect($('#personTitle option:nth-child(2)').val()).toBe('b');
        expect($('#personTitle option:nth-child(2)').prop('selected')).toBe(false);

        expect($('#personTitle option:nth-child(3)').text()).toBe('c');
        expect($('#personTitle option:nth-child(3)').val()).toBe('c');
        expect($('#personTitle option:nth-child(3)').prop('selected')).toBe(true);

        expect($('#personTitle option:nth-child(4)').text()).toBe('d');
        expect($('#personTitle option:nth-child(4)').val()).toBe('d');
        expect($('#personTitle option:nth-child(4)').prop('selected')).toBe(false);

    });
});