/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Element TextArea", function () {
    'use strict';
    var $textArea;
    var textArea;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        $textArea = $("[name=mytextarea]");
        textArea = new QfqNS.Element.TextArea($textArea);
    });

    it("should throw on wrong type", function () {
        expect(function () {
            new QfqNS.Element.TextArea($("input[name='gender']"));
        }).toThrow();
    });

    it("should set text", function () {
        var value = "Only because it works doesn't mean it's right";
        textArea.setValue(value);
        expect($textArea.val()).toBe(value);
    });

    it("should get text", function () {
        $textArea.val("externally set");
        expect(textArea.getValue()).toBe("externally set");
    });
});
