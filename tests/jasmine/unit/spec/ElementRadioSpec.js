/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Element Radio", function () {
    'use strict';
    var $radioInput;
    var radioInput;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        $radioInput = $("input[name='gender']");
        radioInput = new QfqNS.Element.Radio($radioInput);
    });

    it("should throw on wrong type", function () {
        expect(function () {
            new QfqNS.Element.Radio($("input[name='personHandle']"));
        }).toThrow();
    });

    it("should set value", function () {
        radioInput.setValue("male");
        expect($radioInput.filter(':checked').length).toBe(1);
        expect($radioInput.filter(':checked').val()).toBe("male");

        radioInput.setValue("female");
        expect($radioInput.filter(':checked').length).toBe(1);
        expect($radioInput.filter(':checked').val()).toBe("female");
    });

    it("should get text", function () {
        $radioInput.filter(':checked').prop('checked', false);
        $radioInput.filter('[value=female]').prop('checked', true);
        expect(radioInput.getValue()).toBe("female");

        $radioInput.filter(':checked').prop('checked', false);
        $radioInput.filter('[value=male]').prop('checked', true);
        expect(radioInput.getValue()).toBe("male");
    });
});