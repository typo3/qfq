/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Form", function () {
    'use strict';

    var form;

    beforeEach(function () {
        jasmine.Ajax.install();
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        form = new QfqNS.Form("myForm");
    });

    afterEach(function () {
        jasmine.Ajax.uninstall();
    });

    it("should throw exception upon unknown id", function () {
        expect(function () {
            var form = new QfqNS.Form("doesnotexist");
        }).toThrowError(Error);
    });

    it("should throw exception upon id of non-form element", function () {
        expect(function () {
            var form = new QfqNS.Form("qfqTabs");
        }).toThrowError(Error);
    });

    it("should have formChanged() === false upon initialization", function () {
        expect(form.getFormChanged()).toBe(false);
    });

    it("should have form changed timestamp properly initialized", function () {
        expect(form.formChangedTimestampInMilliseconds).toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);
    });

    it("should have formChanged() == false upon explicit initialization", function () {
        form = new QfqNS.Form("myForm", false);
        expect(form.getFormChanged()).toBe(false);
    });

    it("should have formChanged() == true upon explicit initialization", function () {
        form = new QfqNS.Form("myForm", true);
        expect(form.getFormChanged()).toBe(true);
    });

    it("should set the proper state upon form state change", function () {
        expect(form.formChangedTimestampInMilliseconds).toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);

        expect(form.getFormChanged()).toBe(false);
        $('#firstname').change();
        expect(form.getFormChanged()).toBe(true);
        expect(form.formChangedTimestampInMilliseconds).not.toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);
    });

    it("should notify upon form change", function () {
        var changeHandler = jasmine.createSpy('ChangeHandler');

        form.on('form.changed', changeHandler);

        $("#name").change();

        expect(changeHandler).toHaveBeenCalledTimes(1);
        var callArgs = changeHandler.calls.argsFor(0)[0];

        expect(callArgs.target).toBeDefined();
        expect(callArgs.target).toBe(form);
    });

    it("should notify upon state reset", function () {
        var resetHandler = jasmine.createSpy('StateResetHandler');

        form.on('form.reset', resetHandler);

        form.resetFormChanged();

        expect(resetHandler).toHaveBeenCalledTimes(1);
        var callArgs = resetHandler.calls.argsFor(0)[0];
        expect(callArgs.target).toBeDefined();
        expect(callArgs.target).toBe(form);
    });

    it("should reset state upon resetFormChanged()", function () {
        $("#name").change();

        expect(form.formChangedTimestampInMilliseconds).not.toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);
        expect(form.getFormChanged()).toBe(true);

        form.resetFormChanged();

        expect(form.getFormChanged()).toBe(false);
        expect(form.formChangedTimestampInMilliseconds).toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);
    });

    it("should call user handler on successful submit", function () {
        var successHandler = jasmine.createSpy("success");
        var failureHandler = jasmine.createSpy("failure");

        // it should also reset formChanged and formChangedTimestampInMilliseconds information
        form.formChanged = true;
        form.formChangedTimestampInMilliseconds = 1;

        form.on('form.submit.successful', successHandler);
        form.on('form.submit.failed', failureHandler);
        form.submitTo("/test/url");

        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": "application/json",
            "responseText": "{ \"status\": \"success\" }"
        });

        expect(successHandler).toHaveBeenCalledTimes(1);
        expect(failureHandler).not.toHaveBeenCalled();

        expect(form.formChanged).toBe(false);
        expect(form.formChangedTimestampInMilliseconds).toBe(QfqNS.Form.NO_CHANGE_TIMESTAMP);
    });

    it("should call user handler on failed submit", function () {
        var successHandler = jasmine.createSpy("success");
        var failureHandler = jasmine.createSpy("failure");

        // but it should not reset formChanged and formChangedTimestampInMilliseconds information
        form.formChanged = true;
        form.formChangedTimestampInMilliseconds = 1;


        form.on('form.submit.successful', successHandler);
        form.on('form.submit.failed', failureHandler);
        form.submitTo("/test/url");

        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": "404",
            "statusText": "not found",
            "contentType": "application/json",
            "responseText": "{ \"status\": \"not found\" }"
        });

        expect(successHandler).not.toHaveBeenCalled();
        expect(failureHandler).toHaveBeenCalled();

        var callArgs = failureHandler.calls.argsFor(0)[0];

        expect(callArgs.target).toBe(form);
        expect(callArgs.textStatus).toBe("error");
        expect(callArgs.errorThrown).toBe("not found");

        expect(form.formChanged).toBe(true);
        expect(form.formChangedTimestampInMilliseconds).toBe(1);
    });

    it("should add query parameters when provided", function () {
        form.submitTo("/test/url");
        expect(jasmine.Ajax.requests.mostRecent().url).toBe('/test/url');

        form.submitTo("/test/url", {'paramA': 1, 'paramB': 'b'});
        expect(jasmine.Ajax.requests.mostRecent().url).toBe('/test/url?paramA=1&paramB=b');

        form.submitTo("/test/url?paramC=42", {'paramA': 1, 'paramB': 'b'});
        expect(jasmine.Ajax.requests.mostRecent().url).toBe('/test/url?paramC=42&paramA=1&paramB=b');
    });
});