/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("jqxDateTimeInput helper", function () {
    'use strict';

    it('should properly initialize all date time inputs', function () {
        QfqNS.Helper.jqxDateTimeInput();

    });
});
