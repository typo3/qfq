/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global jasmine */
/* global $ */

describe("Element FormGroup", function () {
    'use strict';

    var personGeburtstag;
    var personHandle;
    var personGender;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        personGeburtstag = new QfqNS.Element.FormGroup($("input#personGeburtstag"));
        personHandle = new QfqNS.Element.FormGroup($("input#personHandle"));
        personGender = new QfqNS.Element.FormGroup($("input[name='gender']"));
    });

    it("should identify type properly", function () {
        var element = new QfqNS.Element.FormGroup($('#firstname'));
        expect(element.isType("text")).toBe(true);

        element = new QfqNS.Element.FormGroup($("input[name='gender']"));
        expect(element.isType("radio")).toBe(true);

        expect(element.isType("checkbox")).toBe(false);
    });

    it("should throw exception on non-form elements", function () {
        expect(function () {
            new QfqNS.Element.FormGroup($("#person2"));
        }).toThrow();
    });

    it("should throw exception on empty selection", function () {
        expect(function () {
            new QfqNS.Element.FormGroup($("#you_should_not_find_it"));
        }).toThrow();
    });

    it("should throw on ambiguous form elements", function () {
        expect(function () {
            new QfqNS.Element.FormGroup($("input#personGeburtstag,input#personHandle"));
        }).toThrow();
    });

    it("should throw exception on null form element", function () {
        expect(function () {
            new QfqNS.Element.FormGroup(null);
        }).toThrow();
    });

    it("should find the the enclosing form group", function () {
        expect(personGeburtstag.$formGroup.length).toBe(1);
        expect(personHandle.$formGroup.length).toBe(1);
    });

    it("personGeburtstag should have one input element", function () {
        expect(personGeburtstag.$element.length).toBe(1);
    });

    it("personGeburtstag should have a label", function () {
        expect(personGeburtstag.hasLabel()).toBe(true);
    });

    it("personGeburtstag should not have a help block", function () {
        expect(personGeburtstag.hasHelpBlock()).toBe(false);
    });

    it("personHandle should have one input element", function () {
        expect(personHandle.$element.length).toBe(1);
    });

    it("personHandle should have a label", function () {
        expect(personHandle.hasLabel()).toBe(true);
    });

    it("personHandle should have a help block", function () {
        expect(personHandle.hasHelpBlock()).toBe(true);
    });

    it("personGender should have 2 input elements", function () {
        expect(personGender.$element.length).toBe(2);
    });

    it("personGender should have a label", function () {
        expect(personGender.hasLabel()).toBe(true);
    });

    it("personGender should have a help block", function () {
        expect(personGender.hasHelpBlock()).toBe(true);
    });

    xit("should properly disable the item", function () {
        personGender.setEnabled(false);
    });
});