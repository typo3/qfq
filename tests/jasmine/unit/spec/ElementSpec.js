/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */
describe('Element namespace function getElement()', function () {
    'use strict';

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
    });

    it("should get the proper element instance for radio", function () {
        var element = QfqNS.Element.getElement('gender');
        expect(element instanceof QfqNS.Element.Radio).toBe(true);
    });

    it("should get the proper element instance for text", function () {
        var element = QfqNS.Element.getElement('personHandle');
        expect(element instanceof QfqNS.Element.Textual).toBe(true);
    });

    it("should get the proper element instance for checkbox", function () {
        var element = QfqNS.Element.getElement('reminder');
        expect(element instanceof QfqNS.Element.Checkbox).toBe(true);
    });

    it("should get the proper element instance for select", function () {
        var element = QfqNS.Element.getElement('personTitle');
        expect(element instanceof QfqNS.Element.Select).toBe(true);
    });

    it("should get the proper element instance for textarea", function () {
        var element = QfqNS.Element.getElement('mytextarea');
        expect(element instanceof QfqNS.Element.TextArea).toBe(true);
    });

    it("should throw on non-existent name", function () {
        expect(function () {
            QfqNS.Element.getElement('no_such_name');
        }).toThrow();
    });
});


