/**
 * Created by raos on 6/28/17.
 */

describe("Dirty", function () {
    "use strict";

    var dirtyUnderTest;
    var dirtyUnderTestNoUrl;
    var dirtyEventSpy;

    beforeEach(function () {
        jasmine.Ajax.install();
        dirtyUnderTest = new QfqNS.Dirty("/ajax/mocked");
        dirtyUnderTestNoUrl = new QfqNS.Dirty(null);
        dirtyEventSpy = {
            dirtyNotifySuccess: function () {
            },
            dirtyNotifyDenied: function () {
            },
            dirtyNotifyEnded: function () {
            },
            dirtyNotifyStarted: function () {
            },
            dirtyNotifyFailed: function () {
            },
            dirtyNotifySuccessTimer: function () {
            },
            dirtyNotifyDeniedTimer: function () {
            },
            dirtyReleaseStarted: function () {
            },
            dirtyReleaseEnded: function () {
            },
            dirtyReleaseSuccess: function () {
            },
            dirtyReleaseFailed: function () {
            },
            dirtyRenewalStarted: function () {
            },
            dirtyRenewalEnded: function () {
            },
            dirtyRenewalSuccess: function () {
            },
            dirtyRenewalDenied: function () {
            },
            dirtyRenewalFailed: function () {
            }
        };
        spyOn(dirtyEventSpy, 'dirtyNotifySuccess');
        spyOn(dirtyEventSpy, 'dirtyNotifyDenied');
        spyOn(dirtyEventSpy, 'dirtyNotifyEnded');
        spyOn(dirtyEventSpy, 'dirtyNotifyStarted');
        spyOn(dirtyEventSpy, 'dirtyNotifyFailed');
        spyOn(dirtyEventSpy, 'dirtyNotifySuccessTimer');
        spyOn(dirtyEventSpy, 'dirtyNotifyDeniedTimer');

        spyOn(dirtyEventSpy, 'dirtyReleaseStarted');
        spyOn(dirtyEventSpy, 'dirtyReleaseEnded');
        spyOn(dirtyEventSpy, 'dirtyReleaseSuccess');
        spyOn(dirtyEventSpy, 'dirtyReleaseFailed');

        spyOn(dirtyEventSpy, 'dirtyRenewalStarted');
        spyOn(dirtyEventSpy, 'dirtyRenewalEnded');
        spyOn(dirtyEventSpy, 'dirtyRenewalSuccess');
        spyOn(dirtyEventSpy, 'dirtyRenewalDenied');
        spyOn(dirtyEventSpy, 'dirtyRenewalFailed');


        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.STARTED, dirtyEventSpy.dirtyNotifyStarted);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.ENDED, dirtyEventSpy.dirtyNotifyEnded);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.FAILED, dirtyEventSpy.dirtyNotifyFailed);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.SUCCESS, dirtyEventSpy.dirtyNotifySuccess);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.DENIED, dirtyEventSpy.dirtyNotifyDenied);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.DENIED_TIMEOUT, dirtyEventSpy.dirtyNotifyDeniedTimer);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.SUCCESS_TIMEOUT, dirtyEventSpy.dirtyNotifySuccessTimer);

        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RELEASE_STARTED, dirtyEventSpy.dirtyReleaseStarted);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RELEASE_ENDED, dirtyEventSpy.dirtyReleaseEnded);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RELEASE_FAILED, dirtyEventSpy.dirtyReleaseFailed);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RELEASE_SUCCESS, dirtyEventSpy.dirtyReleaseSuccess);

        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RENEWAL_STARTED, dirtyEventSpy.dirtyRenewalStarted);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RENEWAL_ENDED, dirtyEventSpy.dirtyRenewalEnded);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RENEWAL_SUCCESS, dirtyEventSpy.dirtyRenewalSuccess);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RENEWAL_DENIED, dirtyEventSpy.dirtyRenewalDenied);
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.RENEWAL_FAILED, dirtyEventSpy.dirtyRenewalFailed);

        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.STARTED, dirtyEventSpy.dirtyNotifyStarted);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.ENDED, dirtyEventSpy.dirtyNotifyEnded);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.FAILED, dirtyEventSpy.dirtyNotifyFailed);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.SUCCESS, dirtyEventSpy.dirtyNotifySuccess);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.DENIED, dirtyEventSpy.dirtyNotifyDenied);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.DENIED_TIMEOUT, dirtyEventSpy.dirtyNotifyDeniedTimer);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.SUCCESS_TIMEOUT, dirtyEventSpy.dirtyNotifySuccessTimer);

        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RELEASE_STARTED, dirtyEventSpy.dirtyReleaseStarted);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RELEASE_ENDED, dirtyEventSpy.dirtyReleaseEnded);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RELEASE_FAILED, dirtyEventSpy.dirtyReleaseFailed);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RELEASE_SUCCESS, dirtyEventSpy.dirtyReleaseSuccess);

        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RENEWAL_STARTED, dirtyEventSpy.dirtyRenewalStarted);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RENEWAL_ENDED, dirtyEventSpy.dirtyRenewalEnded);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RENEWAL_SUCCESS, dirtyEventSpy.dirtyRenewalSuccess);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RENEWAL_DENIED, dirtyEventSpy.dirtyRenewalDenied);
        dirtyUnderTestNoUrl.on(QfqNS.Dirty.EVENTS.RENEWAL_FAILED, dirtyEventSpy.dirtyRenewalFailed);
    });

    afterEach(function () {
        jasmine.Ajax.uninstall();
    });

    it("should be properly initialized", function () {
        expect(dirtyUnderTest.successTimerId).toBeNull();
        expect(dirtyUnderTest.deniedTimerId).toBeNull();
        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);
    });

    it("should call the proper notification url", function () {
        dirtyUnderTest.notify("mysip");


        // We use `toMatch()` because `$.ajax` appends a timestamp to the URL to avoid caching.
        expect(jasmine.Ajax.requests.mostRecent().url).toMatch(/^\/ajax\/mocked\?s=mysip&action=lock/);
    });

    it("should call the proper release url", function () {
        dirtyUnderTest.release("mysip");


        // We use `toMatch()` because `$.ajax` appends a timestamp to the URL to avoid caching.
        expect(jasmine.Ajax.requests.mostRecent().url).toMatch(/^\/ajax\/mocked\?s=mysip&action=release/);
    });

    it("should call the proper renewal url", function () {
        dirtyUnderTest.renew("mysip");


        // We use `toMatch()` because `$.ajax` appends a timestamp to the URL to avoid caching.
        expect(jasmine.Ajax.requests.mostRecent().url).toMatch(/^\/ajax\/mocked\?s=mysip&action=extend/);
    });

    it("should call handlers upon successful notification", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.notify("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "success", "message": "ok" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyNotifyStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifySuccess).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifySuccess.calls.argsFor(0)[0].data).toEqual({
            "status": "success",
            "message": "ok"
        });
        expect(dirtyEventSpy.dirtyNotifyEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon denied notification", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.notify("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "conflict", "message": "already open" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyNotifyStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyDenied).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyDenied.calls.argsFor(0)[0].data).toEqual({
            "status": "conflict",
            "message": "already open"
        });
        expect(dirtyEventSpy.dirtyNotifyEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon notification error", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.notify("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 400,
            "contentType": 'application/json',
            "responseText": '{ "status": "error", "message": "foobar" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyNotifyStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyFailed).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should not make ajax request on empty/null url", function () {
        dirtyUnderTestNoUrl.notify("mysip");
        expect(jasmine.Ajax.requests.mostRecent()).toBeUndefined();

        dirtyUnderTestNoUrl.release("mysip");
        expect(jasmine.Ajax.requests.mostRecent()).toBeUndefined();

        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should set the success timer properly", function (done) {
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.SUCCESS_TIMEOUT, function () {
            done();
        });
        QfqNS.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS = 1;

        dirtyUnderTest.notify("mysip");

        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "success", "message": "ok", "lock_timeout": 1 }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(1000);

        expect(dirtyEventSpy.dirtyNotifyStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifySuccess).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifySuccess.calls.argsFor(0)[0].data).toEqual({
            "status": "success",
            "message": "ok",
            "lock_timeout": 1
        });
        expect(dirtyEventSpy.dirtyNotifyEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should set the denied timer properly", function (done) {
        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.DENIED_TIMEOUT, function () {
            done();
        });
        QfqNS.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS = 1;

        dirtyUnderTest.notify("mysip");

        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "conflict", "message": "conflict message", "lock_timeout": 1 }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(1000);

        expect(dirtyEventSpy.dirtyNotifyStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyDenied).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyNotifyDenied.calls.argsFor(0)[0].data).toEqual({
            "status": "conflict",
            "message": "conflict message",
            "lock_timeout": 1
        });
        expect(dirtyEventSpy.dirtyNotifyEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon successful release", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.release("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "success", "message": "released" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyReleaseStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseSuccess).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseSuccess.calls.argsFor(0)[0].data).toEqual({
            "status": "success",
            "message": "released"
        });
        expect(dirtyEventSpy.dirtyReleaseEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon failed release", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.release("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "error", "message": "Error message" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyReleaseStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseFailed).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseFailed.calls.argsFor(0)[0].data).toEqual({
            "status": "error",
            "message": "Error message"
        });
        expect(dirtyEventSpy.dirtyReleaseEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();
    });

    it("should call handlers upon release HTTP error", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.release("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 500,
            "contentType": 'application/json',
            "responseText": '{ "status": "fatal", "message": "Error message" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyReleaseStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseFailed).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyReleaseEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalEnded).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();
    });

    it("should call handlers upon successful renewal", function () {
        // it should also reset the remembered timeout because there is no timeout specified
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.renew("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "success", "message": "renewed" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyRenewalStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalSuccess).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalSuccess.calls.argsFor(0)[0].data).toEqual({
            "status": "success",
            "message": "renewed"
        });
        expect(dirtyEventSpy.dirtyRenewalEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon denied renewal", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.renew("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "denied", "message": "not renewed" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyRenewalStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalDenied).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalDenied.calls.argsFor(0)[0].data).toEqual({
            "status": "denied",
            "message": "not renewed"
        });
        expect(dirtyEventSpy.dirtyRenewalEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();

    });

    it("should call handlers upon renewal HTTP error", function () {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.renew("mysip");

        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 400,
            "contentType": 'application/json',
            "responseText": '{ "status": "error", "message": "unable to renew" }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(QfqNS.Dirty.NO_LOCK_TIMEOUT);

        expect(dirtyEventSpy.dirtyRenewalStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalFailed).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();

    });

    it("should call handlers upon successful renewal and set timer", function (done) {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.SUCCESS_TIMEOUT, function () {
            done();
        });
        QfqNS.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS = 1;


        dirtyUnderTest.renew("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "success", "message": "renewed", "lock_timeout": 1 }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(1000);

        expect(dirtyEventSpy.dirtyRenewalStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalSuccess).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalSuccess.calls.argsFor(0)[0].data).toEqual({
            "status": "success",
            "message": "renewed",
            "lock_timeout": 1
        });
        expect(dirtyEventSpy.dirtyRenewalEnded).toHaveBeenCalledTimes(1);

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(1000);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyDeniedTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalDenied).not.toHaveBeenCalled();

    });

    it("should call handlers upon denied renewal", function (done) {
        // it should also reset the remembered timeout
        dirtyUnderTest.lockTimeoutInMilliseconds = 1;

        dirtyUnderTest.on(QfqNS.Dirty.EVENTS.DENIED_TIMEOUT, function () {
            done();
        });
        QfqNS.Dirty.MINIMUM_TIMER_DELAY_IN_SECONDS = 1;

        dirtyUnderTest.renew("mysip");
        jasmine.Ajax.requests.mostRecent().respondWith({
            "status": 200,
            "contentType": 'application/json',
            "responseText": '{ "status": "denied", "message": "not renewed", "lock_timeout": 1 }'
        });

        expect(dirtyUnderTest.lockTimeoutInMilliseconds).toBe(1000);

        expect(dirtyEventSpy.dirtyRenewalStarted).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalDenied).toHaveBeenCalledTimes(1);
        expect(dirtyEventSpy.dirtyRenewalDenied.calls.argsFor(0)[0].data).toEqual({
            "status": "denied",
            "message": "not renewed",
            "lock_timeout": 1
        });
        expect(dirtyEventSpy.dirtyRenewalEnded).toHaveBeenCalledTimes(1);

        expect(dirtyEventSpy.dirtyNotifyDenied).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccessTimer).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifySuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyNotifyEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyReleaseStarted).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseSuccess).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyReleaseEnded).not.toHaveBeenCalled();

        expect(dirtyEventSpy.dirtyRenewalFailed).not.toHaveBeenCalled();
        expect(dirtyEventSpy.dirtyRenewalSuccess).not.toHaveBeenCalled();

    });
});
