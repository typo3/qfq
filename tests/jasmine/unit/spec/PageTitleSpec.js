/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global jasmine */

describe("Page Title", function () {
        'use strict';

        it("should set/get the document title", function () {
            QfqNS.PageTitle.set("my page title");
            expect(document.title).toBe(QfqNS.PageTitle.get());
        });

        it("should set the page sub title", function () {
            QfqNS.PageTitle.set("title");
            QfqNS.PageTitle.setSubTitle("subtitle");
            expect("title - (subtitle)").toBe(QfqNS.PageTitle.get());

            QfqNS.PageTitle.setSubTitle("new sub");
            expect("title - (new sub)").toBe(QfqNS.PageTitle.get());
        });
    }
);