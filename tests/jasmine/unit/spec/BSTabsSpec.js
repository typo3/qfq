/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */

describe("Bootstrap Tabs", function () {
    'use strict';

    var bsTabs;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        bsTabs = new QfqNS.BSTabs("qfqTabs");
    });


    it("should find all tab anchor elements", function () {
        expect(bsTabs.getTabIds()).toEqual([
            'tab1',
            'tab2',
            'tab3',
            'tab4',
            'tab5'
        ]);
    });

    it("should throw exception on empty selection", function () {
        expect(function () {
            var wdc = new QfqNS.BSTabs("this_shouldnt_be_found");
        }).toThrow();
    });

    it("should activate tab2", function () {
        var retValue = bsTabs.activateTab("tab2");
        expect(bsTabs.getActiveTab()).toBe("tab2");
        expect(retValue).toBe(true);
    });

    it("should handle activation of non-existent tab gracefully", function () {
        var currentTab = bsTabs.getActiveTab();
        var retValue = bsTabs.activateTab("tab_must_not_exist");
        expect(currentTab).toBe(bsTabs.getActiveTab());
        expect(retValue).toBe(false);
    });

    it("retrieval of active tab from DOM should work", function () {
        bsTabs.activateTab("tab3");
        expect(bsTabs.getActiveTab()).toBe(bsTabs.getActiveTabFromDOM());
    });

    it("active tab should set properly upon instantiation", function () {
        bsTabs.activateTab("tab3");
        var newInstance = new QfqNS.BSTabs("qfqTabs");
        expect("tab3").toBe(newInstance.getActiveTab());
    });

    it("should call user 'show tab' handlers", function () {
        var showHandlerSpy = jasmine.createSpy('showhandler');
        bsTabs.on('bootstrap.tab.shown', showHandlerSpy);

        bsTabs.activateTab('tab2');
        expect(showHandlerSpy).toHaveBeenCalled();
    });

    it("should get the proper tab name by id", function () {
        expect('Tab 1').toBe(bsTabs.getTabName('tab1'));
    });

    it("should find the tab a form control element belongs to by the 'name' attribute", function () {
        var tabId = bsTabs.getContainingTabIdForFormControl("input1");
        expect(tabId).toBe('tab2');

        tabId = bsTabs.getContainingTabIdForFormControl("input2");
        expect(tabId).toBe('tab4');
    });

    it("should return null for non-existent form control names", function () {
        expect(bsTabs.getContainingTabIdForFormControl("doesnotexist")).toBeNull();
    });
});
