/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global describe */
/* global it */
/* global expect */
/* global QfqNS */
/* global beforeAll */
/* global beforeEach */
/* global jasmine */
/* global $ */

describe("Element Checkbox", function () {
    'use strict';

    var reminderCheckbox, checkbox2;
    var $reminderCheckbox, $checkbox2;

    beforeEach(function () {
        $("#jasmineTestTemplateTarget")
            .empty()
            .append($("#jasmineTestTemplate").text());
        $reminderCheckbox = $('input[name=reminder]');
        $checkbox2 = $('input[name=checkbox2]');
        reminderCheckbox = new QfqNS.Element.Checkbox($reminderCheckbox);
        checkbox2 = new QfqNS.Element.Checkbox($checkbox2);
    });

    it("should throw on wrong type", function () {
        expect(function () {
            new QfqNS.Element.Checkbox($("input[name='geburtstag']"));
        }).toThrow();
    });

    it('should get the initial checked value', function () {
        expect(reminderCheckbox.getValue()).toBe(false);
        expect(checkbox2.getValue()).toBe(true);
    });

    it('should reflect dynamically changed values', function () {
        $reminderCheckbox.prop('checked', true);
        $checkbox2.prop('checked', false);

        expect(reminderCheckbox.getValue()).toBe(true);
        expect(checkbox2.getValue()).toBe(false);
    });

    it('should set value properly', function () {
        reminderCheckbox.setValue(true);
        checkbox2.setValue(false);

        expect($reminderCheckbox.prop('checked')).toBe(true);
        expect($checkbox2.prop('checked')).toBe(false);
    });

    it('should properly handle several checkboxes in one Form Group', function () {
        var $checkbox3_1 = $("[name='checkbox3_1']");
        var $checkbox3_2 = $("[name='checkbox3_2']");
        var $checkbox3_3 = $("[name='checkbox3_3']");

        var checkbox3_1 = new QfqNS.Element.Checkbox($checkbox3_1);
        checkbox3_1.setValue(true);

        expect(checkbox3_1.getValue()).toBe(true);

        expect($checkbox3_1.prop('checked')).toBe(true);
        expect($checkbox3_2.prop('checked')).toBe(false);
        expect($checkbox3_3.prop('checked')).toBe(false);

        $('#myForm')[0].reset();

        var checkbox3_2 = new QfqNS.Element.Checkbox($checkbox3_2);
        checkbox3_2.setValue(true);

        expect(checkbox3_2.getValue()).toBe(true);

        expect($checkbox3_2.prop('checked')).toBe(true);
        expect($checkbox3_1.prop('checked')).toBe(false);
        expect($checkbox3_3.prop('checked')).toBe(false);

    });
});