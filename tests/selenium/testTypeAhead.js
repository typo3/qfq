/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
'use strict';

const {Builder, By, until} = require('selenium-webdriver'),
    test = require('selenium-webdriver/testing'),
    utils = require('./modules/utils'),
    assert = require('assert'),
    should = require('should');

const MOCK_PATH = "/mockup/typeahead.html";

const BASE_URL = process.env.SELENIUM_BASE_URL || "http://qfq.math.uzh.ch/selenium";
const URL = BASE_URL + MOCK_PATH;

console.log("Use URL: " + URL);


test.describe('Typeahead', function () {
    var driver;

    this.timeout(10000);

    test.before(function () {
        driver = new Builder().forBrowser('chrome').build();
    });

    test.afterEach(function () {
        utils.makeScreenshotIfTestFailed(this.currentTest, driver);
    });

    test.after(function () {
        driver.quit();
    });

    test.it('typeahead 1 should not find string "bla"', function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('dropdown1'))
                .then(
                (el) => el.sendKeys('bla')
                    .then(
                    () => driver.wait(
                        until.elementLocated(By.css('#formgroup1 .tt-menu .tt-dataset-0 div'))
                    ).then(
                        (el) => (el.getText())
                            .then(
                                text => {
                                should(text).be.exactly("'bla' not found");
                                done();
                            }
                        )
                    )
                )
            )
        )
    });

    test.it('typeahead 1 should find "geor" and update shadow field', function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('dropdown1'))
                .then(
                (el) => el.sendKeys('geor')
                    .then(
                    () => driver.wait(
                        until.elementLocated(By.css('#formgroup1 .tt-menu .tt-dataset-0 div'))
                    ).then(
                        () => el.sendKeys("\t")
                    )
                )
                    .then(
                    () => el.getAttribute('value').then(
                            text => {
                            should(text).be.exactly("Georgia");
                        }
                    )
                )
            ).then(
                () => driver.findElement(By.css('[name="dropdown1"]'))
                    .then(
                    (shadowElement) => shadowElement.getAttribute('value')
                        .then(
                            text => {
                            should(text).be.exactly('GA');
                            done();
                        }
                    )
                )
            )
        )
    });

    test.it('typeahead 2 should properly fill shadow element', function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('dropdown2'))
                .then(
                (el) => el.sendKeys('new y')
                    .then(
                    () => driver.wait(
                        until.elementLocated(By.css('#formgroup2 .tt-menu .tt-dataset-1 div'))
                    ).then(
                        () => el.sendKeys("\t")
                    )
                )
                    .then(
                    () => el.getAttribute('value').then(
                            text => {
                            should(text).be.exactly("New York");
                        }
                    )
                )
                    .then(
                    () => driver.findElement(By.css('[name="dropdown2'))
                        .then(
                        (shadowElement) => shadowElement.getAttribute('value')
                            .then(
                                text => {
                                should(text).be.exactly("NY");
                                done();
                            }
                        )
                    )
                )
            )
        )
    });

    test.it('typeahead 2 should clear invalid input', function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('dropdown2'))
                .then(
                (el) => el.sendKeys('bla')
                    .then(
                    // Simply move away from dropdown 2
                    () => driver.findElement(By.id('dropdown1'))
                        .then(
                        (el) => el.click()
                    )
                ).then(
                    driver.findElement(By.css('[name="dropdown2"]'))
                        .then(
                        (shadowElement) => shadowElement.getAttribute('value')
                            .then(
                                text => {
                                should(text).be.empty();
                                done();
                            }
                        )
                    )
                )
            )
        )
    });

    test.it('typeahead 2 should clear shadow properly', function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('dropdown2'))
                .then(
                (el) => el.sendKeys('new y')
                    .then(
                    () => driver.wait(
                        until.elementLocated(By.css('#formgroup2 .tt-menu .tt-dataset-1 div'))
                    ).then(
                        () => el.sendKeys("\t")
                    )
                )
                    .then(
                    () => el.getAttribute('value').then(
                            text => {
                            should(text).be.exactly("New York");
                        }
                    )
                )
                    .then(
                    () => driver.findElement(By.css('[name="dropdown2'))
                        .then(
                        (shadowElement) => shadowElement.getAttribute('value')
                            .then(
                                text => {
                                should(text).be.exactly("NY");
                            }
                        )
                    )
                )
            )
                // Now, enter some arbitrary text. The shadow element must be cleared
                .then(
                () => driver.findElement(By.id('dropdown2'))
                    .then(
                    (el) => el.sendKeys('blabla')
                        .then(
                        () => driver.wait(
                            until.elementLocated(By.css('#formgroup2 .tt-menu .tt-dataset-1 div'))
                        ).then(
                            () => driver.findElement(By.id("dropdown3"))
                                .then(
                                (el) => el.click()
                            )
                        ).then(
                            () => driver.findElement(By.css('[name="dropdown2'))
                                .then(
                                (shadowElement) => shadowElement.getAttribute('value')
                                    .then(
                                        text => {
                                        should(text).be.empty();
                                        done();
                                    }
                                )
                            )
                        )
                    )
                )
            )
        )
    });
});
