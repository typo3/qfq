/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
'use strict';

const {Builder, By, until} = require('selenium-webdriver'),
    test = require('selenium-webdriver/testing'),
    utils = require('./modules/utils'),
    assert = require('assert'),
    should = require('should');

const MOCK_PATH = "/mockup/inputmodeswitcher.html";

const BASE_URL = process.env.SELENIUM_BASE_URL || "http://qfq.math.uzh.ch/selenium";
const URL = BASE_URL + MOCK_PATH;

console.log("Use URL: " + URL);


test.describe('Mode Switcher', function () {
    var driver;

    this.timeout(10000);

    test.before(function () {
        driver = new Builder().forBrowser('chrome').build();
    });

    test.afterEach(function () {
        utils.makeScreenshotIfTestFailed(this.currentTest, driver);
    });

    test.after(function () {
        driver.quit();
    });

    test.it("should switch to readonly", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('input1'))
                .then(
                (textinput) => textinput.sendKeys('typing some text')
                    .then(
                    driver.findElement(By.css('#input1 + div > button'))
                        .then(
                        (button) => button.click()
                    )
                )
            ).then(
                driver.findElement(By.id('input1'))
                    .then(
                    (textinput) => textinput.sendKeys('typing in readonly field')
                )
            ).then(
                driver.findElement(By.id('input1'))
                    .then(
                    (textinput) => (textinput.getAttribute('value'))
                        .then(
                            text => {
                            should(text).be.exactly('typing some text');
                            done();
                        }
                    )
                )
            )
        )
    });

    test.it("should switch back from readonly", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.css('#input1 + div > button'))
                .then(
                (button) => button.click()
                    .then(
                    () => button.click()
                )
            )
        ).then(
            driver.findElement(By.id('input1'))
                .then(
                (textinput) => textinput.clear()
                    .then(
                    () => textinput.sendKeys('This should now be typed')
                ).then(
                    () => textinput.getAttribute('value')
                        .then(
                            text => {
                            should(text).be.exactly('This should now be typed');
                            done();
                        }
                    )
                )
            )
        )
    });

    test.it("should switch to readonly when clicking the button-enclosed element", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('input1'))
                .then(
                (textinput) => textinput.sendKeys('typing some text')
                    .then(
                    driver.findElement(By.css('#input1 + div > button span'))
                        .then(
                        (button) => button.click()
                    )
                )
            ).then(
                driver.findElement(By.id('input1'))
                    .then(
                    (textinput) => textinput.sendKeys('typing in readonly field')
                )
            ).then(
                driver.findElement(By.id('input1'))
                    .then(
                    (textinput) => (textinput.getAttribute('value'))
                        .then(
                            text => {
                            should(text).be.exactly('typing some text');
                            done();
                        }
                    )
                )
            )
        )
    });

});