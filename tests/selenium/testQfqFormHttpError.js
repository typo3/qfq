/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

'use strict';

const {Builder, By, until} = require('selenium-webdriver'),
    qfqForm = require('./modules/qfqform'),
    utils = require('./modules/utils.js'),
    test = require('selenium-webdriver/testing'),
    assert = require('assert'),
    should = require('should');

const MOCK_PATH = "/mockup/qfqform.html";

const BASE_URL = process.env.SELENIUM_BASE_URL || "http://qfq.math.uzh.ch/selenium";
const URL = BASE_URL + MOCK_PATH;

console.log("Use URL: " + URL);


test.describe('Form Submit Error', function () {
    var driver;

    this.timeout(10000);

    test.before(function () {
        driver = new Builder().forBrowser('chrome').build();
    });

    test.afterEach(function () {
        utils.makeScreenshotIfTestFailed(this.currentTest, driver);
    });

    test.after(function () {
        driver.quit();
    });

    test.it("should display error message on HTTP error when submitting form", function (done) {
        driver.get(URL)
            .then(
            () => qfqForm.assertButtonsUnmodifiedForm(driver)
        )
            .then(
            () => driver.findElement(By.id('save-404-error'))
                .then(
                (dropdown) => dropdown.click()
            )
                .then(qfqForm.fillFormForSubmit(driver))
        )
            .then(
            () => qfqForm.saveForm(driver)
        )
            .then(
            () => qfqForm.expectNumberOfAlerts.toBe(1, driver)
                .then(
                () => qfqForm.expectAlertToMatch(1, /Error:\nNot Found/, driver)
                    .then(
                    done()
                )
            )
        )
    });

    test.it("should display error message on HTTP error when deleting form", function (done) {
        driver.get(URL)
            .then(
            () => qfqForm.assertButtonsUnmodifiedForm(driver)
        )
            .then(
            () => driver.findElement(By.id('delete-404-error'))
                .then(
                (dropdown) => dropdown.click()
            )
        )
            .then(
            () => qfqForm.deleteForm(driver)
        )
            .then(
            () => qfqForm.expectNumberOfAlerts.toBe(1, driver)
        )
            .then(
            () => qfqForm.expectAlertToMatch(1, /Do you really want to delete the record\?/, driver)
        )
            .then(
            () => driver.findElement(By.css('div.alert-buttons button:first-child'))
                .then(
                (yesButton) => yesButton.click()
                    .then(
                    () => driver.findElement(By.css('#qfqAlertContainer > div.alert-danger'))
                        .then(
                        (alertContainer) => driver.wait(until.elementIsVisible(alertContainer), 1000)
                    )
                )
            )
        )
            .then(
            () => qfqForm.expectNumberOfAlerts.toBe(1, driver)
                .then(
                () => qfqForm.expectAlertToMatch(1, /Error:\nNot Found/, driver)
                    .then(
                    () => done()
                )
            )
        )
    });

    test.it("should display error message on HTTP error when deleting file", function (done) {
        driver.get(URL)
            .then(
            () => qfqForm.assertButtonsUnmodifiedForm(driver)
        )
            .then(
            () => driver.findElement(By.id('delete-404-error'))
                .then(
                (dropdown) => dropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('link-more-dropdown'))
                .then(
                (link) => link.click()
                    .then(
                    () => driver.findElement(By.id('link-tab-arbeitsgruppe'))
                        .then(
                        (link) => link.click()
                    )
                )
            )
        )
            .then(
            () => driver.findElement(By.css('.uploaded-file button.delete-file'))
                .then(
                (button) => button.click()
                    .then(
                    qfqForm.expectNumberOfAlerts.toBe(1, driver)
                        .then(
                        qfqForm.expectAlertToMatch(1, /Do you want to delete the file\?/, driver)
                    )
                )
            )
        )
            .then(
            () => driver.findElement(By.css('div.alert-buttons button:first-child'))
                .then(
                (yesButton) => yesButton.click()
                    .then(
                    () => driver.findElement(By.css('#qfqAlertContainer > div.alert-danger'))
                        .then(
                        (alertContainer) => driver.wait(until.elementIsVisible(alertContainer), 1000)
                    )
                )
            )
        )
            .then(
            () => qfqForm.expectNumberOfAlerts.toBe(1, driver)
                .then(
                () => qfqForm.expectAlertToMatch(1, /Error:\nNot Found/, driver)
                    .then(
                    () => done()
                )
            )
        )
    });

});