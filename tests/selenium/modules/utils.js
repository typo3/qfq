"use strict";

module.exports.makeScreenshotIfTestFailed = function makeScreenshotIfTestFailed(currentTest, driver) {
    if (currentTest.state === 'failed') {
        (function (testname) {
            driver.takeScreenshot()
                .then((data) => require('fs').writeFile("ERROR-" + testname + '.png', data, 'base64'))
        })(currentTest.title);
    }
};