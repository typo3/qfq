/**
 * Created by raos on 7/12/17.
 */
"use strict";

const {Builder, By, until} = require('selenium-webdriver'),
    should = require('should');
const NEW_BUTTON_ID = 'form-new-button';
const DELETE_BUTTON_ID = 'delete-button';
const CLOSE_BUTTON_ID = 'close-button';
const SAVE_BUTTON_ID = 'save-button';
const ALERT_SELECTOR = '#qfqAlertContainer > div';

function waitForAlert(driver) {
    return driver.findElement(By.id('qfqAlertContainer'))
        .then(
            (alertContainer) => driver.wait(until.elementIsVisible(alertContainer))
        )
}

function expectNumberOfAlerts(number, driver) {
    return waitForAlert(driver)
        .then(
            () => driver.findElements(By.css(ALERT_SELECTOR))
                .then(
                    (elements) => should(elements).be.of.length(number)
                )
        )
}

function expectNumberOfAlertsAtLeast(number, driver) {
    return waitForAlert(driver)
        .then(
            () => driver.findElements(By.css(ALERT_SELECTOR))
                .then(
                    (elements) => should(elements.length).be.aboveOrEqual(number)
                )
        )
}


module.exports.fillFormForSubmit = function fillFormForSubmit(driver) {


    return driver.findElement(By.id('name'))
        .then(
            (input) => input.sendKeys('Berners-Lee')
        )
        .then(
            driver.findElement(By.id('firstname'))
                .then(
                    (input) => input.sendKeys('Tim')
                )
        )
        .then(
            driver.findElement(By.id('nameShort'))
                .then(
                    (input) => input.sendKeys("T.")
                )
        )
        .then(
            driver.findElement(By.id('personTitle-dr'))
                .then(
                    (option) => option.click()
                )
        )
        .then(
            driver.findElement(By.id('gender-male'))
                .then(
                    (option) => option.click()
                )
        )
        .then(
            driver.findElement(By.id('link-tab-person2'))
                .then(
                    (link) => link.click()
                )
        )
        .then(
            driver.switchTo().frame('notes-editor_ifr')
                .then(
                    driver.findElement(By.id('tinymce'))
                        .then(
                            (editor) => editor.sendKeys('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu orci diam. Phasellus lobortis, ante.')
                        )
                ).then(
                driver.switchTo().defaultContent()
            )
        );
};

module.exports.assertButtonsUnmodifiedForm = function assertButtonsUnmodifiedForm(driver) {
    return driver.findElement(By.id(SAVE_BUTTON_ID))
        .then(
            (button) => driver.wait(until.elementIsDisabled(button))
        )
        .then(
            () => driver.findElement(By.id(CLOSE_BUTTON_ID))
                .then(
                    (button) => driver.wait(until.elementIsEnabled(button))
                )
        )
        .then(
            () => driver.findElement(By.id(DELETE_BUTTON_ID))
                .then(
                    (button) => driver.wait(until.elementIsEnabled(button))
                )
        )
        .then(
            () => driver.findElement(By.id(NEW_BUTTON_ID))
                .then(
                    (button) => driver.wait(until.elementIsEnabled(button))
                )
        )
};

module.exports.saveForm = function saveForm(driver) {
    return driver.findElement(By.id(SAVE_BUTTON_ID))
        .then(
            (button) => button.click()
        )
};

module.exports.deleteForm = function deleteForm(driver) {
    return driver.findElement(By.id(DELETE_BUTTON_ID))
        .then(
            (button) => button.click()
        )
};

module.exports.waitForAlert = waitForAlert;

module.exports.expectNumberOfAlerts = {};
module.exports.expectNumberOfAlerts.toBe = expectNumberOfAlerts;
module.exports.expectNumberOfAlerts.toBe.atLeast = expectNumberOfAlertsAtLeast;

module.exports.expectAlertToMatch = function expectAlertToMatch(number, match, driver) {
    return expectNumberOfAlertsAtLeast(number, driver)
        .then(
            () => driver.findElements(By.css(ALERT_SELECTOR))
                .then(
                    (elements) => {
                        let alert = elements[number - 1];
                        alert.getText()
                            .then(
                                (text) => should(text).match(match)
                            )
                    }
                )
        )
};