/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

'use strict';

const {Builder, By, until} = require('selenium-webdriver'),
    test = require('selenium-webdriver/testing'),
    utils = require('./modules/utils'),
    assert = require('assert'),
    should = require('should');

const MOCK_PATH = "/mockup/dirtyform.html";

const BASE_URL = process.env.SELENIUM_BASE_URL || "http://qfq.math.uzh.ch/selenium";
const URL = BASE_URL + MOCK_PATH;

console.log("Use URL: " + URL);


test.describe('Dirty Form', function () {
    var driver;

    this.timeout(10000);

    test.before(function () {
        driver = new Builder().forBrowser('chrome').build();
    });

    test.afterEach(function () {
        utils.makeScreenshotIfTestFailed(this.currentTest, driver);
    });

    test.after(function () {
        driver.quit();
    });

    test.it("should not fail on non 2xx responses", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('option-404error'))
                .then(
                (dirtyEndpointDropdown) => dirtyEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('name'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElements(By.id('qfqAlertContainer'))
                .then(
                (found) => {
                    should(found.length).be.exactly(0);
                    done();
                }
            )
        )
    });

    test.it("should not pop up message on success", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('option-dirty-success'))
                .then(
                (dirtyEndpointDropdown) => dirtyEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('name'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElements(By.id('qfqAlertContainer'))
                .then(
                (found) => {
                    should(found.length).be.exactly(0);
                    done();
                }
            )
        )
    });

    test.it("should pop up message on denied and disable buttons", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('option-dirty-denied'))
                .then(
                (dirtyEndpointDropdown) => dirtyEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('name'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.wait(until.elementLocated(By.css('#qfqAlertContainer > div')))
                .then(
                (element) => driver.wait(until.elementIsVisible(element))
                    .then(
                    (element) => element.getText()
                        .then(
                        (text) => {
                            should(text).be.exactly('other user has lock\nReload');
                        }
                    )
                        .then(
                        () => element.click()
                    )
                )
            )
        )
            .then(
            () => driver.findElement(By.id('save-button'))
                .then(
                (button) => driver.wait(until.elementIsDisabled(button))
            )
        )
            .then(
            () => driver.findElement(By.id('delete-button'))
                .then(
                (button) => driver.wait(until.elementIsDisabled(button))
            )
        )
            .then(
            () => done()
        );
    });

    test.it("should work for happy path", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('option-save-no-redirect'))
                .then(
                (submitToEndpointDropdown) => submitToEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('option-dirty-success'))
                .then(
                (dirtyEndpointDropdown) => dirtyEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('name'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElement(By.id('firstname'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElement(By.id('save-button'))
                .then(
                (button) => button.isEnabled()
                    .then(
                    () => button.click()
                )
            )
        )
            .then(
            () => driver.findElement(By.css('#qfqAlertContainer > div'))
                .then(
                (found) => found.isDisplayed()
                    .then(
                    () => found.getText()
                        .then(
                        (text) => {
                            should(text).be.exactly('save sucessful');
                            done();
                        }
                    )
                )
            )
        );
    });

    test.it("should not save on conflict", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('option-save-conflict'))
                .then(
                (submitToEndpointDropdown) => submitToEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('option-dirty-success'))
                .then(
                (dirtyEndpointDropdown) => dirtyEndpointDropdown.click()
            )
        )
            .then(
            () => driver.findElement(By.id('name'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElement(By.id('firstname'))
                .then(
                (inputElement) => inputElement.sendKeys("test")
            )
        )
            .then(
            () => driver.findElement(By.id('save-button'))
                .then(
                (button) => button.isEnabled()
                    .then(
                    () => button.click()
                        .then(
                        () => driver.wait(until.elementIsVisible(
                            driver.findElement(By.css('#qfqAlertContainer > div'))
                        ))
                            .then(
                            (found) => found.getText()
                                .then(
                                (text) => {
                                    should(text).be.exactly('Form was modified by other user');
                                    done();
                                }
                            )
                        )
                    )
                )
            )
        )
    });
});
