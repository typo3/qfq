README
===

Selenium tests require the ChromeDriver. Running `npm install` in the top level directory will take care of installing required packages.

Doc: https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/testing/index.html