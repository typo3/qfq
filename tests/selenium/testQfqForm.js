/**
 * Created by raos on 7/13/17.
 */
'use strict';

const {Builder, By, until} = require('selenium-webdriver'),
    qfqForm = require('./modules/qfqform'),
    utils = require('./modules/utils.js'),
    test = require('selenium-webdriver/testing'),
    assert = require('assert'),
    should = require('should');

const MOCK_PATH = "/mockup/qfqform.html";

const BASE_URL = process.env.SELENIUM_BASE_URL || "http://qfq.math.uzh.ch/selenium";
const URL = BASE_URL + MOCK_PATH;

console.log("Use URL: " + URL);


test.describe('QFQ Form', function () {
    var driver;

    this.timeout(10000);

    test.before(function () {
        driver = new Builder().forBrowser('chrome').build();
    });

    test.afterEach(function () {
        utils.makeScreenshotIfTestFailed(this.currentTest, driver);
    });

    test.after(function () {
        driver.quit();
    });

    test.it("should mark required fields", function (done) {
        driver.get(URL)
            .then(
            () => driver.findElement(By.id('firstname'))
                .then(
                (firstnameInput) => firstnameInput.click()
            )
        )
            .then(
            () => driver.findElement(By.id('nameShort'))
                .then(
                (shortNameInput) => shortNameInput.click()
            )
        )
            // By now, we should have triggered a validation error on the form
            .then(
            () => driver.findElement(By.css('#firstname + div.help-block > ul.list-unstyled li:first-child'))
                .then(
                (validationItem) => driver.wait(until.elementIsVisible(validationItem), 1000)
                    .then(
                    () => validationItem.getText()
                        .then(
                        (text) => {
                            should(text).be.exactly("Please fill out this field.");
                            done();
                        }
                    )
                )
            )
        )
    });
});
