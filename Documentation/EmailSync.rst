.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links: `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link: :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Add Images:    .. image:: ./Images/a4.jpg
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _emailSync:

Email Sync
==========

Configuration
-------------

Preparation
^^^^^^^^^^^

* Copy `typo3conf/ext/qfq/example.imap.php` to `fileadmin/protected/qfqProject/conf/imap.php`
* Configure `fileadmin/protected/qfqProject/conf/imap.php`

TBD: Account Parameter und alle php-imap settings (ggfs. auf php-imap verweisen).

QFQ Config
^^^^^^^^^^

* Extension > QFQ > Tools > emailSync

  * Multiple accounts.
  * Per account all folder which have to be synced.

Syntax::

  account1=Inbox,Sent|account2=done

* Extension > QFQ > Debug 

  * imapLogMode: debug, verbose, error, none
  * imapLog: fileadmin/protected/qfqProject/log/imap.log
  
Running
^^^^^^^

The php script `typo3conf/ext/qfq/Classes/External/email-sync.php` will sync all mails from specified accounts and folders
to table `EmailSync`::

    email-sync.php sync|listAllFolder|listAllFolderMail

Mode:

* sync: all mails from configured accounts & folders (via `fileadmin/protected/qfqProject/conf/imap.php`) will be copied 
  to table `EmailSync`.
  
  * If the specified folder does not exist, report all folder to imap.log.
  
* listAllFolder: for every configured account list all folders.
* listAllFolderMail: for every configured account list all folders with their emails.
