.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links:          `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link:           :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Anker for internal link: .. _`download`:
.. Add Image:               .. image:: ./Images/a4.jpg
..
.. Add image with caption:   .. figure:: ./Images/black_dot.png
..                              :class: with-border
..                              :width: 20px
..
.. black_dot.png
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented
..
.. -*- coding: utf-8 -*- with BOM.


.. include:: Includes.txt

.. _`license`:

License
=======

* QFQ is licensed under GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007

Software distributed together with QFQ
--------------------------------------

* Bootstrap - http://getbootstrap.com
* Bootstrap-validator.js - https://github.com/1000hz/bootstrap-validator
* Chart.js - https://github.com/nnnick/Chart.js.git
* CodeMirror - https://codemirror.net/
* Datetimepicker - https://getdatepicker.com/, https://github.com/Eonasdan/tempus-dominus
* Event Emitter - https://git.io/ee
* Fabric.js - https://github.com/fabricjs/fabric.js/tree/master
* Filepond - https://github.com/pqina/filepond
* Font Awesome - https://github.com/FortAwesome/Font-Awesome
* Font Password-Dots - https://fontstruct.com/fontstructions/show/1106896  The FontStruction “Password Dots” by “JimProuty” is licensed under a Creative Commons Attribution license (http://creativecommons.org/licenses/by/3.0/).
* FullCalendar - https://fullcalendar.io/
* HTMLPurifier - https://github.com/ezyang/htmlpurifier
* Moment.js - https://github.com/moment/moment/
* PhpSpreadsheet - https://github.com/PHPOffice/PhpSpreadsheet
* Popper - https://popper.js.org/
* Ratchet - https://github.com/ratchetphp/Ratchet - Websocket used by 'Chat'
* SendEmail - https://github.com/mogaal/sendemail
* Tablesorter - https://mottie.github.io/tablesorter/docs/index.html
* TinyMCE - https://github.com/tinymce/tinymce
* Twig - https://twig.symfony.com
* Twitter typeahead JS - https://twitter.github.io/typeahead.js/
* jQWidgets - https://www.jqwidgets.com
* jQuery - http://jquery.com
