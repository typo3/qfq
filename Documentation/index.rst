.. ==================================================
.. FOR YOUR INFORMATION TEST
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _start:

=============================================================
Quick Form Query Extension
=============================================================

:Extension key:
   qfq

:Version:
   |release|

:Language:
    en

:Copyright:
   2017-2024

:Authors:
   Rose, Carsten / Baer, Benjamin / Nuredini, Enis / Haller, Jan / Putyra, Krzysztof

:Further Contributors:
   Chiapolini, Nicola /
   Egger, Marc /
   Gröbelbauer, Philipp /
   Li, Zhoujie /
   Ostertag, Rafael /
   Rössler, Pascal /
   Villiger, Elias /

:Email:
   support@math.uzh.ch

:License:
    This document is published under the Open Publication License
    available from http://www.opencontent.org/openpub/

:Status: Productive, new features and bug fixes comes all the time.

:Rendered:
   |today|

**TYPO3**

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org
<https://typo3.org/>`_ .

**About this manual:**

This manual is a reference. Some basic examples are at the end.

**Community documentation:**

This document is *not* official TYPO3 documentation.

It is maintained as part of a third party extension.

If you find an error or something is missing, please report
an `issue <https://project.math.uzh.ch/projects/qfq>`_

**Extension Manual**

This documentation is for the TYPO3 extension **qfq**.

:ref:`sitemap`


.. toctree::
   :maxdepth: 4
   :hidden:

   General
   Installation
   Concept
   Debug
   Variable
   Security
   Store
   LDAP
   Form
   Report
   REST
   Wiki
   System
   EmailSync
   ApplicationTest
   GeneralTips
   UseCase
   CodingGuideline
   License
   Sitemap
   SearchDocs
   Release

