.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links:          `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link:           :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Anker for internal link: .. _`download`:
.. Add Image:               .. image:: ./Images/a4.jpg
..
.. Add image with caption:   .. figure:: ./Images/black_dot.png
..                              :class: with-border
..                              :width: 20px
..
.. black_dot.png
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _`concept`:

Concept
=======

SIPs
----

The following is a technical background information. Not needed to just use QFQ.

The SIPs (=Server Id Pairs) are uniq timestamps, created/registered on the fly for a specific browser session (=user). Every SIP is
registered on the server (= stored in a browser session) and contains one or more key/value pairs. The key/value pairs never leave
the server. The SIPs will be used:

* to protect values not to be spoofed by anyone,
* to protect values not to be altered by anyone,
* to grant access, e.g.:

  * load and save forms,
  * upload files,
  * download files,
  * PHP AJAX pages.

SIPs becomes invalid, as soon as the current browser session is destroyed. The client (= user) can't manipulate the content
of SIPs - it's only possible to reuse already registered SIPs by the user, who already owns the session.

Access privileges
-----------------

The Typo3 FE Groups can be used to implement access privileges. Such groups are assigned to

* Typo3 FE users,
* Typo3 pages,
* and/or Typo3 content records (e.g. QFQ records).

This will be used for general page structure privileges.

A `record base` privileges controlling (e.g. which user can edit
which person record) will be implicit configured, by the way that records are viewable / editable (or not) through
SQL in the specific QFQ tt-content statements.

Intitialization
---------------

QFQ is executed when a page is opened with at least one QFQ tt-content record on it.
If QFQ is running the first time, or if a newer QFQ extension version is installed, the initialization process starts:

QFQ default tables & stored procedures
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The QFQ standard tables (`Classes/Sql/qfqTableDefault.sql`) and stored procedures (`Classes/Sql/qfqFunctionDefault.sql`), are
automatically created in the QFQ-Database.

QFQ default forms
^^^^^^^^^^^^^^^^^

During initialization, the forms defined in `Resources/Private/Form/*` are imported in Table *Form* and *FormElement*.
These are system forms. Such forms might be updated during a future QFQ update.

QFQ optional tables
^^^^^^^^^^^^^^^^^^^

In case QFQ tries to access a table which does not exist in the database, an exception is thrown.

The exceptions checks if 'show debug info' is active and if the missing table is a QFQ table (`Classes/Sql/qfqTable*.sql`).
If this is the case, a form is offered to create missing optional tables.

.. image:: Images/WizardOptionalTables.png

Show debug info: in QFQ extension config a) debug.showDebugInfo='yes' or
b) debug.showDebugInfo='auto' and a BE user is logged in is given.

Typo3 QFQ content element
-------------------------

Insert one or more QFQ content elements on a Typo3 page. Specify column and language per content record as wished.

The title of the QFQ content element will not be rendered on the frontend. It's only visible to the webmaster in the
backend for orientation.

.. _`qfq_keywords`:

QFQ Keywords (Bodytext)
^^^^^^^^^^^^^^^^^^^^^^^

**All of these parameters are optional.**

+--------------------------------+---------------------------------------------------------------------------------+
| Name                           | Explanation                                                                     |
+================================+=================================================================================+
| form                           | | Formname.                                                                     |
|                                | | Static: **form = person**                                                     |
|                                | | By SIP: **form = {{form:SE}}**                                                |
|                                | | By SQL: **form = {{SELECT c.form FROM Config AS c WHERE c.id={{a:C}} }}**     |
+--------------------------------+---------------------------------------------------------------------------------+
| r                              | | <record id>. The form will load the record with the specified id.             |
|                                | | Static: **r = 123**                                                           |
|                                | | By SQL: **r = {{SELECT ...}}**                                                |
|                                | | If not specified, the SIP parameter 'r' is used.                              |
+--------------------------------+---------------------------------------------------------------------------------+
| dbIndex                        | E.g. `dbIndex = {{indexQfq:Y}}` Select a DB index. Only necessary if a          |
|                                | different than the standard DB should be used.                                  |
+--------------------------------+---------------------------------------------------------------------------------+
| debugShowBodyText              | If='1' and :ref:`configuration`:*showDebugInfo: yes*, shows a                   |
|                                | tooltip with bodytext                                                           |
+--------------------------------+---------------------------------------------------------------------------------+
| sqlLog                         | Overwrites :ref:`configuration`: :ref:`SQL_LOG` . Only affects `Report`,        |
|                                | not `Form`.                                                                     |
+--------------------------------+---------------------------------------------------------------------------------+
| sqlLogMode                     | Overwrites :ref:`configuration`: :ref:`SQL_LOG_MODE<SQL_LOG_MODE>` .            |
|                                | Only affects `Report`, not `Form`.                                              |
+--------------------------------+---------------------------------------------------------------------------------+
| render                         | See :ref:`report-render`. Overwrites :ref:`configuration`: render.              |
+--------------------------------+---------------------------------------------------------------------------------+
| performanceReport              | If='1', a SQL performance report is rendered at the end of the current          |
|                                | tt_content record (only if a BE user is logged in).                             |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.fbeg                   | Start token for every field (=column)                                           |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.fend                   | End token for every field (=column)                                             |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.fsep                   | Separator token between fields (=columns)                                       |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.fskipwrap              | Skip wrapping (via fbeg, fsep, fend) of named columns. Comma separated list of  |
|                                | column id's (starting at 1). See also the :ref:`special-column-names` '_noWrap' |
|                                | to suppress wrapping.                                                           |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.shead                  | Static start token for whole <level>, independent if records are selected       |
|                                | Shown before `head`.                                                            |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.stail                  | Static end token for whole <level>, independent if records are selected.        |
|                                | Shown after `tail`.                                                             |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.head                   | Dynamic start token for whole <level>. Only if at least one record is select.   |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.tail                   | Dynamic end token for whole <level>. Only if at least one record is select.     |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.rbeg                   | Start token for row.                                                            |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.rbgd                   | Alternating (per row) token.                                                    |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.rend                   | End token for row. Will be rendered **before** subsequent levels are processed  |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.renr                   | End token for row. Will be rendered **after** subsequent levels are processed   |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.rsep                   | Seperator token between rows                                                    |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.sql                    | SQL Query                                                                       |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.twig                   | Twig Template                                                                   |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.althead                | If <level>.sql has no rows selected (empty), these token will be rendered.      |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.altsql                 | If <level>.sql has no rows selected (empty) or affected (delete, update, insert)|
|                                | the <altsql> will be fired. Note: Sub queries of <level> are not fired, even if |
|                                | <altsql> selects some rows.                                                     |
+--------------------------------+---------------------------------------------------------------------------------+
| <level>.content                | | *show* (default): content of current and sub level are directly shown.        |
|                                | | *hide*: content of current and sub levels are **stored** and not shown.       |
|                                | | *hideLevel*: content of current and sub levels are **stored** and only sub    |
|                                | | levels are shown.                                                             |
|                                | | *store*: content of current and sub levels are **stored** and shown.          |
|                                | | To retrieve the content: `{{<level>.line.content}}`.                          |
|                                | | See :ref:`syntax-of-report`                                                   |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.count       | Current row index. Will be replaced before the query is fired in case of        |
|                                | ``<level>``/``<alias>`` is an outer/previous level or it will be replaced after |
|                                | a query is fired in case ``<level>``/``<alias>`` is the current level.          |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.total       | Total rows (MySQL ``num_rows`` for *SELECT* and *SHOW*, MySQL ``affected_rows`` |
|                                | for *UPDATE* and *INSERT*.                                                      |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.insertId    | Last insert id for *INSERT*.                                                    |
| <alias>.line.insertId          |                                                                                 |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.content     | Show content of `<level>`/`<alias>` (content have to be stored via              |
|                                | <level>.content=... or <alias>.content=...).                                    |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.altCount    | Like 'line.count' but for 'alt' query.                                          |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.altTotal    | Like 'line.total' but for 'alt' query.                                          |
+--------------------------------+---------------------------------------------------------------------------------+
| <level|alias>.line.altInsertId | Like 'line.insertId' but for 'alt' query.                                       |
+--------------------------------+---------------------------------------------------------------------------------+

.. _`report-render`:

Report: render
--------------

QFQ will render Report or Form in three situations:

#. Browser: The QFQ content is described by QFQ-Report syntax.
#. Browser: The QFQ content is described by a QFQ-Form.
#. API: The QFQ content is handled without Typo3. Typically a single tt-content record is specified in the request - only
   that one is rendered by QFQ. This mode is typically used to export data like Excel Export.

Option 1 and 2 are distinguished by the parameter `form` (STORE_SIP or STORE_TYPO3). If 'form' is given, in most cases only a Form
should be shown (not the report).

+--------+-------------------------------------------------------------------------+
| render | Use                                                                     |
+========+=========================================================================+
| single | Display Form or Report, but not both at the same time. If               |
|        | a SIP parameter 'form' is given, the form is rendered, else the report. |
+--------+-------------------------------------------------------------------------+
| both   | Display Form and Report, both at the same time.                         |
+--------+-------------------------------------------------------------------------+
| api    | Create output only when called via API (no Typo3).                      |
+--------+-------------------------------------------------------------------------+

Example::

   render = both
   form = {{form:SE}}

   10 {
     sql = SELECT ...
   }

.. _`qfq-database`:

QFQ Database
------------

Recommended setup for Typo3 & QFQ Installation is with *two* databases. One for the Typo3 installation and one for QFQ.
A good practice is to name both databases equal, appending the suffix '_t3' and '_db'.

When QFQ is called, it checks for QFQ system tables. If they do not exist or have a lower version than the installed QFQ
version, the system tables will be automatically installed or updated.

.. _`system-tables`:

System tables
^^^^^^^^^^^^^

+---------------+------------+------------+
| Name          | Use        | Database   |
+===============+============+============+
| Clipboard     | Temporary  | QFQ        |
+---------------+------------+------------+
| Cron          | Persistent | QFQ        |
+---------------+------------+------------+
| Dirty         | Temporary  | QFQ | Data |
+---------------+------------+------------+
| Form          | Persistent | QFQ        |
+---------------+------------+------------+
| FormElement   | Persistent | QFQ        |
+---------------+------------+------------+
| FormSubmitLog | Persistent | QFQ | Data |
+---------------+------------+------------+
| MailLog       | Persistent | QFQ | Data |
+---------------+------------+------------+
| Period        | Persistent | Data       |
+---------------+------------+------------+
| Split         | Persistent | Data       |
+---------------+------------+------------+

See :ref:`mail-log-page` and :ref:`form-submit-log-page` for some Frontend views for these tables.

* Check Bug #5459 / support of system tables in different DBs not supported.

.. _`multi-database`:

Multi Database
^^^^^^^^^^^^^^

Base: T3 & QFQ
""""""""""""""

QFQ typically interacts with one database, the QFQ database. The database used by Typo3 is typically a separate one.
Theoretically it might be the same (never tested), but it's strongly recommended to use a separated QFQ database to have
no problems on Typo3 updates and to have a clean separation between Typo3 and QFQ.

QFQ: System & Data
""""""""""""""""""

QFQ itself can be separated in 'QFQ system' (see :ref:`system-tables`) and 'QFQ data' databases (even more than one are
possible). The 'QFQ system' stores the forms, record locking, log tables and so on - `QFQ data` is for the rest.

A `Multi Database` setup is given, if 'QFQ system' is different from 'QFQ data'.

Data: Data1, Data2, ..., Data n
"""""""""""""""""""""""""""""""

Every database needs to be configured via :ref:`configuration` with it's own `index`.

`QFQ data` might switch between different 'data'-databases. In :ref:`configuration` one main `QFQ data` index will be specified
in `indexQfq`. If specific forms or reports should use a different database than that, `dbIndex` might change
`indexData` temporarily.

`dbIndex`: A `Report` (field `dbIndex`) as well as a `Form` (field `parameter`.`dbIndex`) can operate on a specific database.


A `Form` will:

* load the form-definition from `indexQfq` (table `Form` and `FormElement`),
* loads and save data from/in `indexData` (qfq.json) / `dbIndex` (form.parameter.dbIndex),
* retrieve extra information via `dbIndexExtra` - this is useful to offer information from a database and save them in a
  different one.

The simplest setup, QFQ system & data in the same database, needs no `indexQfq / indexData` definition in
:ref:`configuration` or one or both of them set to '1'

To separate QFQ system and data, indexQfq and indexData will have different indexes.


A Multi Database setup might be useful for:

* several independent Typo3 QFQ installations (each have it's own form repository) and one central database, or
* one QFQ installation which should display / load /save records from different databases, or
* a combination of the above two.

Note:

* Option 'A' is the most simple and commonly used.
* Option 'B' separate the T3 and QFQ databases on two database hosts.
* Option 'C' is like 'B' but with a shared 'QFQ data'-database between three 'Typo3 / QFQ' instances.
* Further variants are possible.

+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
|   | Domain         | Website Host | T3                            | QFQ system                   | QFQ data                         |
+===+================+==============+===============================+==============================+==================================+
| A | standalone.edu | 'w'          | <dbHost>, <dbname>_t3, <dbnameSingle>_db                                                        |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
| B | appB1.edu      | 'wApp'       | <dbHostApp>, <dbnameB1>_t3    | <dbHostB1>, <dbnameApp>_db                                      |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
| B | appB2.edu      | 'wApp'       | <dbHostApp>, <dbnameB2>_t3    | <dbHostB2>, <dbnameApp>_db                                      |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
| C | appC1.edu      | 'wAppC'      | <dbHostAppC>, <dbnameC1>_t3   | <dbHostC>, <dbnameSysC1>_db  | <dbHostData>_db, <dbNameData>_db |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
| C | appC2.edu      | 'wAppC'      | <dbHostAppC>, <dbnameC2>_t3   | <dbHostC>, <dbnameSysC2>_db  | <dbHostData>_db, <dbNameData>_db |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+
| C | appC3.edu      | 'wAppC3'     | <dbHostAppC3>, <dbnameC3>_t3  | <dbHostC3>, <dbnameSysC3>_db | <dbHostData>_db, <dbNameData>_db |
+---+----------------+--------------+-------------------------------+------------------------------+----------------------------------+

In :ref:`config-qfq-php` mutliple database credentials can be prepared. Mandatory is at least one credential setup like
`DB_1_USER`, `DB_1_SERVER`, `DB_1_PASSWORD`, `DB_1_NAME`. The number '1' indicates the `dbIndex`. Increment the number
to specify further database credential setups.

By convention, it's necessary that `DB_1` is the one who also have access to the Typo3 database. E.q. :
ref:`qfq-function` or
download links (based on QFQ functions) needs access to the T3 database to directly fetch tt-content records.

You'll find a real world example under :ref:`multi-db-use-case`.

Different QFQ versions, shared database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When using different QFQ versions and a shared 'QFQ data'-database, there is some risk of conflicting
'QFQ system' tables. Best is to always use the same QFQ version on all instances or use a Multi Database setup.
