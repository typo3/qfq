.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links:          `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link:           :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Anker for internal link: .. _`download`:
.. Add Image:               .. image:: ./Images/a4.jpg
..
.. Add image with caption:   .. figure:: ./Images/black_dot.png
..                              :class: with-border
..                              :width: 20px
..
.. black_dot.png
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _`wiki`:

Wiki
====

Get started
-----------

The wiki is a special system report provided by QFQ and is located in the extension directory `typo3conf/ext/qfq/Resources/Private/Report`.
Setup a *report* to get started using the wiki:

* Create a Typo3 page.
* Insert a content record of type *qfq*.
* In the bodytext insert the following code (see explanation of code: :ref:`reportAsFile`)::

    file=_wiki

.. important::

   The root page of the wiki can now be added in the frontend by a logged in Typo3 backend user.

Form: wikiPage
--------------

Provides options for editing the settings of the wiki page and consists of two pills, `General` and `Access`.

* General:

   * `Name`: Name of the wiki page
   * `Page slug`: Page slug of the wiki page (readonly)
   * `Parent page`: Parent page of the wiki page

* Access (can only be accessed by the author of the wiki page and a logged in Typo3 backend user):

   * `Read-only user`: Comma separated string of usernames (`{{feUser:T}}`)
   * `Read-only group`: Checkboxes of available groups (`{{dbNameT3:Y}}.fe_groups`)
   * `Read-write user`: Comma separated string of usernames (`{{feUser:T}}`)
   * `Read-write group`: Checkboxes of available groups (`{{dbNameT3:Y}}.fe_groups`)
   * `Public`: Radio to set public access (`read-only`, `read-write`, `off`). Default `read-only`.
   * `Page Lock`: Checkbox to set page lock. Can only be removed by the user who set it.

.. important::

   Do not use spaces in between usernames.

.. note::

    Any user without a page login (no `{{feUser:T}}`) is considered public.

.. tip::

   When a new wiki page is added using the `Add new page` option from the dropdown, the form automatically copies the settings of the parent page it was created from.
   It is still possible to select a different parent page.

.. note::

   A wiki page can only be deleted if it has no subpages. When a wiki page is deleted, all its previous versions are also deleted.

Form: wikiEditor
----------------

Once a wiki page has been created, content can be added/edited. In general, there are two ways to edit a page.

* The Edit button at the top of the page allows you to edit the entire content.
* The edit buttons next to the headings allow you to edit each section separately.

.. note::

   For example, if you click on a `<h2>`, every subsequent `<h3>`, `<h4>`, etc. up to the next `<h2>` or `<h1>` will be copied into the editor.
   It is important to note that this functionality can only be guaranteed if the headings are not nested within other elements.

Macros
------

The are macros that can be used within the editor:

* `{{toc}}`: Creates a table of contents with links to the headings on the current page. If no headings are found, `{{toc}}` is returned.
* `{{childPages}}`: Generates a list of links to the child pages of the current page. If no child pages are found, `{{childPages}}` is returned.
* `{{<wikiPage>#<Heading>}}`: Creates a link to a wiki page within the same wiki by replacing `<wikiPage>` with the name of the
  wiki page and `<Heading>` with the title of the paragraph. If the wiki page is not found, `{{<wikiPage>#<Heading>}}` is returned.
* `{{<pageSlug>/<wikiPage>#<Heading>}}`: Creates a link to a wiki page in another wiki by replacing `<pageSlug>` with the page slug of the wiki, `<wikiPage>` with
  the name of the wiki page and `<Heading>` with the title of the paragraph. If the wiki page is not found, `{{<pageSlug>/<wikiPage>#<Heading>}}` is returned.
* `{{collapse([Text is shown])}}`
  Text is hidden.
  `{{collapse}}`
  Creates a toggleable element, that will show/hide text. Text within `[ ]` is optional.

.. important::

   `{{toc}}`, `{{childPages}}`, `{{collapse([Text is shown])}}`, `{{collapse}}` must be used on a separate line or with their own `<p>` tag to ensure their functionality.

.. important::

   `Using links (`{{<wikiPage>#<Heading>}}` and `{{<pageSlug>/<wikiPage>#<Heading>}}`) `Heading` is used as the link text.
   This means that `#` must be used, but the heading can theoretically be omitted. In this case the full link will be displayed.

.. tip::

   It is possible to upload images to the editor with "drag and drop" or the upload feature. Images are stored under `fileadmin/wiki/<pageSlug>/img`.

History
-------

The option `History` in the dropdown menu provides a list of changes made to the wiki page. If the author is `anonymous`,
this means that the changes were made by a public user without a `{{feUser:T}}` account.

Print
-----

The option `Print` in the dropdown menu creates a PDF of the wiki page.
