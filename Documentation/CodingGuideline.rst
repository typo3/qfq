.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links:          `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link:           :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Anker for internal link: .. _`download`:
.. Add Image:               .. image:: ./Images/a4.jpg
..
.. Add image with caption:   .. figure:: ./Images/black_dot.png
..                              :class: with-border
..                              :width: 20px
..
.. black_dot.png
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _`CodingGuideline`:

Coding Guideline
================

The following is not mandatory but shows some best practices:

Constants
---------

* Define constants in ``Extensions > QFQ > Custom > ...``

* Dynamic values under ``Extensions > QFQ > Dynamic > ...``


QFQ content record
------------------

* Name the record in the header field with:

  * Regular content: ``[QFQ] ...``
  * Content in the left column: ``[QFQ,L] ...``
  * Content in englisch: ``[QFQ,E] ...``

* The first lines should be comments, explaining what the record does and list all passed variables. Optional variables
  are indicated by using STORE_EMPTY or STORE_ZERO::

    #
    # Shows list of Persons living in {{country:SE}}
    #
    # {{country:SE}}
    #

* A good practice is to define all possible STORE_SIP Parameter in a SQL at the beginning and copy them to STORE_RECORD::

      10 {
        # Normalize variables
        sql = SELECT '{{country:SE}}' AS _country

        # List selected persons per country
        20.sql = SELECT p.name FROM Person AS p WHERE p.country LIKE '{{country:R}}'
      }

* Always comment the queries like shown above.

QFQ Form
--------

* Mandatory SIP parameter should to be mentioned in Form.requiredNew and/or Form.requiredEdit.
* If the title of a FormElement isn't descriptive enough, use tooltip, note or extraButtonInfo to explain to a user.
* Every Form should show a descriptive title to identify the task and current record. E.g. Not 'Person' but 'Person: John Doe'.
* Often the length of a pill title if not sufficient, use a tooltip to give a more descriptive hint.
