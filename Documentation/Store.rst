.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links:          `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link:           :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Anker for internal link: .. _`download`:
.. Add Image:               .. image:: ./Images/a4.jpg
..
.. Add image with caption:   .. figure:: ./Images/black_dot.png
..                              :class: with-border
..                              :width: 20px
..
.. black_dot.png
..
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _`store`:

Store
=====

Only variables that are known in a specified store can be substituted.

+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| Name |                                       Description                                       |                                      Content                                       |
+======+=========================================================================================+====================================================================================+
| B    | :ref:`STORE_BEFORE`: Record - the current record loaded in the form before any update.  | All columns of the current record from the current table. See :ref:`STORE_BEFORE`. |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| C    | :ref:`STORE_CLIENT`: POST variable, if not found: GET variable.                         | Parameter sent from the Client (=Browser). See :ref:`STORE_CLIENT`.                |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| D    | Default values column : The *table.column* specified *default value*.                   |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| E    | *Empty* - always an empty string, might be helpful if a variable is empty or undefined  | Any key                                                                            |
|      | and will be used in an SQL statement.                                                   |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| F    | :ref:`STORE_FORM`: data not saved in database yet.                                      | All native *FormElements*. Recent values from the Browser. See: :ref:`STORE_FORM`  |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| L    | :ref:`STORE_LDAP`: Will be filled on demand during processing of a *FormElement*.       | Custom specified list of LDAP attributes. See :ref:`STORE_LDAP`.                   |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| M    | Column type: The *table.column* specified *type*.                                       |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| P    | Parent record. E.g.: on multi & copy forms the current record of the outer query.       | All columns of the MultiSQL Statement from the table for the current row           |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| R    | :ref:`STORE_RECORD`: Record - the current record loaded in the form.                    | All columns of the current record from the current table. See :ref:`STORE_RECORD`. |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| S    | :ref:`STORE_SIP`: Client parameter 's' will indicate the current SIP, which will be     | sip, r (recordId), form. See :ref:`STORE_SIP`.                                     |
|      | loaded from the SESSION repo to the SIP-Store.                                          |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| T    | :ref:`STORE_TYPO3`: a) Bodytext (ttcontent record), b) Typo3 internal variables.        | See Typo3 tt_content record configuration. See `STORE_TYPO3`_.                     |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| U    | :ref:`STORE_USER`: per user variables, valid as long as the browser session lives.      | Set via report: '...' AS '_=<var name>' See: :ref:`STORE_USER`,                    |
|      |                                                                                         | :ref:`store_user_examples`                                                         |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| V    | :ref:`STORE_VARS`: Generic variables.                                                   | See :ref:`STORE_VARS`.                                                             |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| Y    | :ref:`STORE_SYSTEM`: a) Database, b) helper vars for logging/debugging:                 | See :ref:`STORE_SYSTEM`.                                                           |
|      | SYSTEM_SQL_RAW ... SYSTEM_FORM_ELEMENT_COLUMN, c) Any custom fields: CONTACT, HELP, ... |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+
| 0    | *Zero* - always value: 0, might be helpful if a variable is empty or undefined and      | Any key                                                                            |
|      | will be used in an SQL statement.                                                       |                                                                                    |
+------+-----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------+

* Default *<prio>*: *FSRVD* - Form / SIP / Record / Vars / Table definition.
* Hint: Preferable, parameter should be submitted by SIP, not by Client (=URL).

  * Warning: Data submitted via 'Client' can be easily spoofed and altered.
  * Best: Data submitted via SIP never leaves the server, cannot be spoofed or altered by the user.
  * SIPs can _only_ be defined by using *Report*. Inside of *Report* use columns 'Link' (with attribute 's'), 'page?' or 'Page?'.

.. _`STORE_FORM`:

Store: *FORM* - F
-----------------

* Sanitized: *yes*
* Represents the values in the form, typically before saving them.
* Used for:

  * *FormElements* which will be rerendered, after a parent *FormElement* has been changed by the user.
  * *FormElement* actions, before saving the form.
  * Values will be sanitized by the class configured in corresponding the *FormElement*. By default, the sanitize class
    is *alnumx*.

+---------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| Name                            | Explanation                                                                                                                                |
+=================================+============================================================================================================================================+
| <FormElement name>              | Name of native *FormElement*. To get, exactly and only, the specified *FormElement* (for 'pId'): *{{pId:F}}*                               |
+---------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------+

.. _`STORE_SIP`:

Store: *SIP* - S
----------------

* Sanitized: *no*
* Filled automatically by creating links. E.g.:

  * in *Report* by using ``_page*`` or ``_link`` (with active 's')
  * in *Form* by using subrecords: *new*, *edit*, *delete* links (system) or by column type ``_page*``, ``_link``.

+-------------------------+-----------------------------------------------------------+
| Name                    | Explanation                                               |
+=========================+===========================================================+
| sip                     | 13 char uniqid                                            |
+-------------------------+-----------------------------------------------------------+
| r                       | current record id                                         |
+-------------------------+-----------------------------------------------------------+
| form                    | current form name                                         |
+-------------------------+-----------------------------------------------------------+
| table                   | current table name                                        |
+-------------------------+-----------------------------------------------------------+
| urlparam                | all non Typo3 parameter in one string                     |
+-------------------------+-----------------------------------------------------------+
| <user defined>          | additional user defined link parameter                    |
+-------------------------+-----------------------------------------------------------+

.. _`STORE_RECORD`:

Store: *RECORD* - R
-------------------


* Sanitized: *no*
* *Form*: Current record.
* *Report*: See :ref:`access-column-values`
* If r=0, all values are empty.

+------------------------+-------------------------------------------------------------------------------------------------------------------------+
| Name                   | Type     | Explanation                                                                                                  |
+========================+==========+==============================================================================================================+
| <column name>          | Form     | Name of a column of the primary table (as defined in the current form). Example: *{{pId:R}}*                 |
+------------------------+----------+--------------------------------------------------------------------------------------------------------------+
| <column name>          | Report   | Name of a column of a previous fired SQL query. Example: *{{pId:R}}*                                         |
+------------------------+----------+--------------------------------------------------------------------------------------------------------------+
| &<column name>         | Report   | Name of a column of a previous fired SQL query, typically used by columns with a :ref:`special-column-names`.|
|                        | (final)  | Final value. Example: '{{link:R}}' returns 'p:home&form=Person|s|b:success|t:Edit'.                          |
|                        |          | Whereas '{{&link:R}}' returns '<span class="btn btn-success"><a href="?home&s=badcaffee1234">Edit</a></span> |
+------------------------+----------+--------------------------------------------------------------------------------------------------------------+

.. _`STORE_BEFORE`:

Store: *BEFORE* - B
-------------------


* Sanitized: *no*
* Current record loaded in Form without any modification.
* If r=0, all values are empty.

This store is handy to compare new and old values of a form.

+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------+
| Name                   | Explanation                                                                                                                                      |
+========================+==================================================================================================================================================+
| <column name>          | Name of a column of the primary table (as defined in the current form). To get, exactly and only, the specified form *FormElement*: *{{pId:B}}*  |
+------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------+

.. _`STORE_CLIENT`:

Store: *CLIENT* - C
-------------------

* Sanitized: *yes*

+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| Name                    | Explanation                                                                                                                              |
+=========================+==========================================================================================================================================+
| s                       | =SIP                                                                                                                                     |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| r                       | record id. Only if specified as GET parameter - typically stored in SIP (=STORE_SIP)                                                     |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| form                    | Name of form to load. Only if specified as GET parameter - typically stored in SIP (=STORE_SIP)                                          |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| HTTP_HOST               | current HTTP HOST                                                                                                                        |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| REMOTE_ADDR             | Client IP address                                                                                                                        |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| All HTTP Header         | SERVER_ADDR, SERVER_NAME, SERVER_SOFTWARE,SERVER_PROTOCOL, REQUEST_METHOD, REQUEST_TIME, REQUEST_TIME_FLOAT, QUERY_STRING, DOCUMENT_ROOT,|
| variables               | HTTP_ACCEPT, HTTP_ACCEPT_CHARSET, HTTP_ACCEPT_ENCODING, HTTP_ACCEPT_LANGUAGE, HTTP_CONNECTION, HTTP_HOST, HTTP_REFERER,                  |
|                         | HTTP_USER_AGENT, HTTPS, REMOTE_ADDR, REMOTE_HOST, REMOTE_PORT, REMOTE_USER, REDIRECT_REMOTE_USER, SCRIPT_FILENAME, SERVER_ADMIN,         |
|                         | SERVER_PORT, SERVER_SIGNATURE, PATH_TRANSLATED, SCRIPT_NAME, REQUEST_URI, PHP_AUTH_DIGEST, PHP_AUTH_USER, PHP_AUTH_PW,                   |
|                         | AUTH_TYPE, PATH_INFO, ORIG_PATH_INFO                                                                                                     |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| Authorization           | Value of the HTTP Header 'Authorization'. This is typically not set. Mostly used for authentication of REST requests                     |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+
| X-Api-Key               | Value of the HTTP Header 'X-Api-Key'. This is typically not set.                                                                         |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------+

.. tip::

    Variable names are case sensitive.


.. _`STORE_TYPO3`:

Store: *TYPO3* (Bodytext) - T
-----------------------------

* Sanitized: *no*

+------------------------------------------+-------------------------------------------------------------------+-----+
| Name                                     | Explanation                                                       | API |
+==========================================+===================================================================+=====+
| form                                     | | Formname defined in ttcontent record bodytext                   | no  |
|                                          | | * Fix. E.g. *form = person*                                     |     |
|                                          | | * via SIP. E.g. *form = {{form:SE}}*                            |     |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageId                                   | Record id of current Typo3 page                                   | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageAbstract                             | Abstract of current Typo3 page                                    | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageAlias (DEPRECATED)                   | Alias of current Typo3 page. If empty, take  pageId.              | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageSlug                                 | Slug  of current Typo3 page.                                      | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageDescription                          | Description of current Typo3 page                                 | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageKeywords                             | Keywords of current Typo3 page                                    | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageLanguage                             | Current selected page language (typically URL parameter 'L')      | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageNavTitle                             | Alternative navigation title of current Typo3 page                | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageTitle                                | Title of current Typo3 page                                       | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| pageType                                 | Current selected page type (typically URL parameter 'type')       | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| ttcontentSubheader                       | Subheader of the current ttcontent record                         | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| ttcontentUid                             | Record id of current Typo3 content element                        | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| feUser                                   | Logged in Typo3 FE User                                           | yes |
+------------------------------------------+-------------------------------------------------------------------+-----+
| feUserEmail                              | Email of logged in Typo3 FE User (if given in T3)                 | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| feUserGroup                              | FE groups of logged in Typo3 FE User                              | yes |
+------------------------------------------+-------------------------------------------------------------------+-----+
| feUserUid                                | Logged in Typo3 FE User uid                                       | yes |
+------------------------------------------+-------------------------------------------------------------------+-----+
| beUser                                   | Logged in Typo3 BE User                                           | yes |
+------------------------------------------+-------------------------------------------------------------------+-----+
| beUserUid                                | T3 Uid of logged in Typo3 BE User                                 | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| beUserEmail                              | Email of logged in Typo3 BE User (if given in T3)                 | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| beUserLoggedIn                           | 'yes' | 'no' - Status if a BE-User is logged in                   | yes |
+------------------------------------------+-------------------------------------------------------------------+-----+
| t3version                                | Current T3 version                                                | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcResourceId                           | OIDC frontend: the id of the last accessed resource               | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcProvider                             | OIDC frontend: the identifier of the identity provider            | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcSub                                  | OIDC frontend: `sub` claim of the resource                        | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcEmail                                | OIDC frontend: `email` claim of the resource                      | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcEmailVerified                        | OIDC frontend: `email_verified` claim or `null` if not present    | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcGivenName                            | OIDC frontend: `given_name` claim of the resource                 | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+
| oidcFamilyName                           | OIDC frontend: `family_name` claim of the resource                | no  |
+------------------------------------------+-------------------------------------------------------------------+-----+



* **API**: available:

  * in :ref:`dynamic-update` or
  * by *FormElement* class 'action' with type 'beforeSave', 'afterSave', 'beforeDelete', 'afterDelete'.

* **oidc...** Variables

  * Only available if the T3 `oidc_client` extension is installed.

.. _`STORE_VARS`:

Store: *VARS* - V
-----------------

* Sanitized: *no*
* The store contains automatically filled *QFQ values* and optional *custom values*.

QFQ values
^^^^^^^^^^

+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| Name              | Scope       | Explanation                                                                                                                                |
+===================+=============+============================================================================================================================================+
| random            | Always      | Random string with length of 32 alphanum chars (lower & upper case). See :ref:`RANDOM`.                                                    |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| slaveId           | FE *any*    | See :ref:`slave-id`                                                                                                                        |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| allRequiredGiven  | Form        | Form save - Set during check of all required FE. If every *required* FE is given: *1*. Else: *0*. If there is no required FE: *1*.         |
|                   |             | Even with ``formModeGlobal = requiredOff | requiredOffButMark`` the variable will be set.                                                  |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| filename          | FE *upload* | Original filename of an uploaded file via an 'upload'-FormElement. Valid only during processing of the current 'upload'-formElement.       |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| filenameOnly      | FE *upload* | Like filename, but without path.                                                                                                           |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| filenameBase      | FE *upload* | Like *filename*, but without an optional extension. E.g. filename='image.png' comes to filenameBase='image'                                |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| filenameExt       | FE *upload* | Like *filename*, but only the optional extension. E.g. filename='image.png' comes to filenameExt='png'                                     |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| fileDestination   | FE *upload* | Destination (path & filename) for an uploaded file. Defined in an 'upload'-FormElement.parameter. Valid: same as 'filename'.               |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| fileSize          | FE *upload* | Size of the uploaded file.                                                                                                                 |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| mimeType          | FE *upload* | Mime type of the uploaded file.                                                                                                            |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| indexData         | Form /      | By default value of `Settings > Extenions > QFQ > Database > indexData`.                                                                   |
|                   | Report      |                                                                                                                                            |
+-------------------+-------------+--------------------------------------------------------------------------------------------------------------------------------------------+

.. _`RANDOM`:

{{random:V}}
^^^^^^^^^^^^

* Random string with length of 32 alphanum chars (lower & upper case).
* The variable *{{random:V}}* is always filled.
* Hint: QFQ variables will be replaced **before** a SQL statement is fired.

  * Each access gives a new value.
  * Execution of a SQL statement is **one** access - therefore ``SELECT '{{random:V}}' FROM Person`` will give the same
    random value for all *Person*.
  * Several ``SELECT ...{{random:V}}...`` will give several different random values.

* Uniqueness is added via PHP function *uniqid()*

  * This just prepends 13 chars at the beginning of the random string.
  * *uniqid()* is only a very precise timestamp - timestamp means it becomes bigger and bigger and the first characters stays the same.
  * These first 13 chars should *not* be taken as random! They will make *uniqness* for *{{random:V}}* more likely.

Custom values (via fillStoreVar)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The directive *fillStoreVar* fills STORE_VARS with custom values::

    # The values can be retrieved via `{{name:V}}` and `{{email:V}}`
    fillStoreVar = {{! SELECT p.name, p.email FROM Person AS p WHERE p.id={{pId:S}} }}

* `fillStoreVar` can be used in :ref:`Form.parameter<form-parameter>` and :ref:`FormElement.parameter<fe-parameter-attributes>`.
* If *fillStoreVar* is defined in Form and/or multiple FormElements:

    * Already existing variables with different names will be merged.
    * Already existing variables with same names will be overwritten by the latest definition.

.. tip::

    Specify STORE_VARS explicit to avoid naming collision. E.g. ``{{person:V}}`` references cleary the STORE_VAR,
    instead ``{person}}`` uses the default *FSRVD*, where as STORE_VARS is late on position 4 and might be overwritten
    by STORE_FORM, STORE_SIP and STORE_RECORD.

.. _`STORE_LDAP`:

Store: *LDAP* - L
-----------------

* Sanitized: *yes*
* See also :ref:`LDAP`

+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------+
| Name                    | Explanation                                                                                                                                |
+=========================+============================================================================================================================================+
| <custom defined>        | See *ldapAttributes*                                                                                                                       |
+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------+



.. _`STORE_SYSTEM`:

Store: *SYSTEM* - Y
-------------------

* Sanitized: *no*

All settings can be accessed via this store. See :ref:`configuration` for a list of all settings.

Additional variables:

+-------------------------+------------------------------------------------------------------------------+
| Name                    | Explanation                                                                  |
+=========================+==============================================================================+
| qfqProjectPath          | Absolute path to the qfq project directory. See :ref:`qfq-project-path-php`  |
+-------------------------+------------------------------------------------------------------------------+

.. _`STORE_USER`:

Store: *USER* - U
-----------------

* Sanitized: *no*

At start of a new browser session (=user calls the website the first time or was logged out before) the store is empty.
As soon as a value is set in the store, it remains as long as the browser session is alive (=until user logs out).

Values can be set in report via ``... AS "_=<var name>"``

See also: :ref:`store_user_examples`
