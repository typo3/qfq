<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="packages/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="packages/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="packages/jqwidgets/css/jqx.base.css">
    <link rel="stylesheet" href="packages/jqwidgets/css/jqx.darkblue.css">

    <script src="packages/jquery/js/jquery.min.js"></script>
    <script src="packages/bootstrap/js/bootstrap.min.js"></script>
    <script src="packages/jqwidgets/js/jqx-all.js"></script>

    <title>QFQ DevWrapper</title>

    <style>
        /*inline elements in horizontal mode are too much left*/
        .form-horizontal .form-inline .form-group {
            margin: 0;
        }

        .form-inline .control-label {
            font-weight: normal;
        }
    </style>
</head>
<body>

<?php
require_once("t3/qfq.php");
require_once("qfq/Constants.php"); // just for the debug string below to simulate the t3 bodytext

//qfq\renderForm("\n# some notes\ndebugLoad = 5\n debugSave = 0\n form = mytestform");
qfq\renderForm("\n10.sql = SELECT * FROM Form\n10.head = <p>Hello world</p><p>\n10.tail=</p>\n");

?>

</body>
</html>
