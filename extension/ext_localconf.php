<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */
defined('TYPO3') or defined('TYPO3_MODE') or die ('Access denied.');

(static function () {

    if (method_exists(\TYPO3\CMS\Core\Utility\VersionNumberUtility::class, 'getNumericTypo3Version')) {
        // For TYPO3 v10 and later
        $typo3Version = \TYPO3\CMS\Core\Utility\VersionNumberUtility::getNumericTypo3Version();
    } else {
        // For TYPO3 v9 and earlier
        $typo3Version = TYPO3_version;
    }

    $typo3VersionInteger = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger($typo3Version);

    if ($typo3VersionInteger >= 10000000) {
        // TYPO3 version is 10.0.0 or higher
        $controllerAction = [\IMATHUZH\Qfq\Controller\QfqController::class => 'show'];
        $nonCacheableControllerAction = [\IMATHUZH\Qfq\Controller\QfqController::class => 'show'];
    } else {
        // TYPO3 version is lower than 10.0.0
        $controllerAction = array('Qfq' => 'show');
        $nonCacheableControllerAction = array('Qfq' => 'show');
    }

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'qfq',
        'Qfq',
        $controllerAction,
        $nonCacheableControllerAction,
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:qfq/Configuration/PageTSconfig/PageTSconfig.ts">');

    // By default, the 'tt_content' Header will be rendered: Avoid this, because it's much nicer to use the header in the backend as a description title of what the record does.
    $addLine = '
    tt_content.qfq_qfq = COA
    tt_content.qfq_qfq {
            10 >
    }
    ';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('qfq', 'setup', '# Setting ' . 'qfq' . $addLine . '', 'defaultContentRendering');

    // Enable the authentication service by Qfq
    $extensionConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
    )->get('qfq');

    if ($extensionConfig['authEnable']) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
            'qfq',
            'auth' /* sv type */,
            'tx_qfq_auth' /* sv key */,
            [
                'title' => 'Authentication service',
                'description' => 'Authentication with Qfq',
                'subtype' => 'getUserFE,authUserFE',
                'available' => true,
                'priority' => intval($extensionConfig['authPriority']),
                'quality' => intval($extensionConfig['authQuality']),
                'os' => '',
                'exec' => '',
                'className' => \IMATHUZH\Qfq\Core\Typo3\QfqAuthenticationService::class,
            ]
        );
    }


})();
