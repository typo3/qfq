<?php

/**
 * This file is copied to extension/ for phpunit execution without typo3 on gitlab runner. See Makefile > phpunit
 * ATTENTION: The arrays below are serialized. If you change the content you might break the unserialization in Config.php
 */

return [
    'BE' => [
        'debug' => false,
        'explicitADmode' => 'explicitAllow',
        'installToolPassword' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'loginSecurityLevel' => 'rsa',
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8',
                'dbname' => 'xxxxxx_qfq_t3',
                'driver' => 'mysqli',
                'host' => '127.0.0.1',
                'password' => 'xxxxxxxxxxxxxxxx',
                'port' => 0000,
                'user' => 'xxxxxx_qfq',
            ],
        ],
    ],
    'EXT' => [
        'extConf' => [
//            'qfq' => 'a:108:{s:13:"documentation";s:62:"https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html";s:18:"thumbnailDirSecure";s:32:"fileadmin/protected/qfqThumbnail";s:18:"thumbnailDirPublic";s:22:"typo3temp/qfqThumbnail";s:11:"cmdInkscape";s:8:"inkscape";s:10:"cmdConvert";s:7:"convert";s:14:"cmdWkhtmltopdf";s:30:"/opt/wkhtmltox/bin/wkhtmltopdf";s:7:"baseUrl";s:40:"http://xxxxxxxxxxxxxxxxxxxxxxxxxxxx/qfq/";s:16:"sendEMailOptions";s:0:"";s:10:"dateFormat";s:10:"dd.mm.yyyy";s:21:"fillStoreSystemBySql1";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg1";s:0:"";s:21:"fillStoreSystemBySql2";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg2";s:0:"";s:21:"fillStoreSystemBySql3";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg3";s:0:"";s:17:"redirectAllMailTo";s:0:"";s:6:"logDir";s:31:"fileadmin/protected/log/qfq.log";s:6:"qfqLog";s:31:"fileadmin/protected/log/qfq.log";s:10:"sqlLogMode";s:6:"modify";s:6:"sqlLog";s:31:"fileadmin/protected/log/sql.log";s:17:"formSubmitLogMode";s:3:"all";s:7:"mailLog";s:32:"fileadmin/protected/log/mail.log";s:13:"showDebugInfo";s:4:"auto";s:4:"init";s:14:"set names utf8";s:6:"update";s:4:"auto";s:9:"indexData";s:1:"1";s:8:"indexQfq";s:1:"1";s:17:"escapeTypeDefault";s:1:"m";s:20:"securityVarsHoneypot";s:23:"email,username,password";s:19:"securityAttackDelay";s:1:"5";s:19:"securityShowMessage";s:4:"true";s:20:"securityGetMaxLength";s:2:"50";s:24:"recordLockTimeoutSeconds";s:3:"900";s:13:"enterAsSubmit";s:1:"1";s:12:"editFormPage";s:4:"form";s:20:"formDataPatternError";s:0:"";s:21:"formDataRequiredError";s:0:"";s:18:"formDataMatchError";s:0:"";s:13:"formDataError";s:0:"";s:17:"showIdInFormTitle";s:1:"0";s:16:"cssClassColumnId";s:10:"text-muted";s:20:"cssClassQfqContainer";s:0:"";s:15:"cssClassQfqForm";s:0:"";s:19:"cssClassQfqFormPill";s:16:"qfq-color-grey-1";s:19:"cssClassQfqFormBody";s:16:"qfq-color-grey-2";s:13:"formBsColumns";s:2:"12";s:18:"formBsLabelColumns";s:1:"3";s:18:"formBsInputColumns";s:1:"6";s:17:"formBsNoteColumns";s:1:"3";s:21:"extraButtonInfoInline";s:70:"<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>";s:20:"extraButtonInfoBelow";s:80:"<span class="glyphicon glyphicon-info-sign text-info" aria-hidden="true"></span>";s:23:"extraButtonInfoPosition";s:4:"auto";s:20:"extraButtonInfoClass";s:0:"";s:15:"formLanguageAId";s:0:"";s:18:"formLanguageALabel";s:0:"";s:15:"formLanguageBId";s:0:"";s:18:"formLanguageBLabel";s:0:"";s:15:"formLanguageCId";s:0:"";s:18:"formLanguageCLabel";s:0:"";s:15:"formLanguageDId";s:0:"";s:18:"formLanguageDLabel";s:0:"";s:14:"saveButtonText";s:0:"";s:17:"saveButtonTooltip";s:4:"Save";s:15:"saveButtonClass";s:26:"btn btn-default navbar-btn";s:23:"saveButtonClassOnChange";s:19:"alert-info btn-info";s:19:"saveButtonGlyphIcon";s:12:"glyphicon-ok";s:15:"closeButtonText";s:0:"";s:18:"closeButtonTooltip";s:5:"Close";s:16:"closeButtonClass";s:26:"btn btn-default navbar-btn";s:20:"closeButtonGlyphIcon";s:16:"glyphicon-remove";s:16:"deleteButtonText";s:0:"";s:19:"deleteButtonTooltip";s:6:"Delete";s:17:"deleteButtonClass";s:26:"btn btn-default navbar-btn";s:21:"deleteButtonGlyphIcon";s:15:"glyphicon-trash";s:13:"newButtonText";s:0:"";s:16:"newButtonTooltip";s:3:"New";s:14:"newButtonClass";s:26:"btn btn-default navbar-btn";s:18:"newButtonGlyphIcon";s:14:"glyphicon-plus";s:7:"custom1";s:0:"";s:7:"custom2";s:0:"";s:7:"custom3";s:0:"";s:7:"custom4";s:0:"";s:7:"custom5";s:0:"";s:7:"custom6";s:0:"";s:7:"custom7";s:0:"";s:7:"custom8";s:0:"";s:7:"custom9";s:0:"";s:8:"custom10";s:0:"";s:8:"custom11";s:0:"";s:8:"custom12";s:0:"";s:8:"custom13";s:0:"";s:8:"custom14";s:0:"";s:8:"custom15";s:0:"";s:8:"custom16";s:0:"";s:8:"custom17";s:0:"";s:8:"custom18";s:0:"";s:8:"custom19";s:0:"";s:8:"custom20";s:0:"";s:8:"custom21";s:0:"";s:8:"custom22";s:0:"";s:8:"custom23";s:0:"";s:8:"custom24";s:0:"";s:8:"custom25";s:0:"";s:8:"custom26";s:0:"";s:8:"custom27";s:0:"";s:8:"custom28";s:0:"";s:8:"custom29";s:0:"";s:8:"custom30";s:0:"";}',
            'qfq' => 'a:108:{s:13:"documentation";s:62:"https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html";s:18:"thumbnailDirSecure";s:32:"fileadmin/protected/qfqThumbnail";s:18:"thumbnailDirPublic";s:22:"typo3temp/qfqThumbnail";s:11:"cmdInkscape";s:8:"inkscape";s:10:"cmdConvert";s:7:"convert";s:14:"cmdWkhtmltopdf";s:30:"/opt/wkhtmltox/bin/wkhtmltopdf";s:7:"baseUrl";s:19:"http://example.com/";s:16:"sendEMailOptions";s:0:"";s:10:"dateFormat";s:10:"dd.mm.yyyy";s:21:"fillStoreSystemBySql1";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg1";s:0:"";s:21:"fillStoreSystemBySql2";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg2";s:0:"";s:21:"fillStoreSystemBySql3";s:0:"";s:29:"fillStoreSystemBySqlErrorMsg3";s:0:"";s:17:"redirectAllMailTo";s:0:"";s:6:"logDir";s:31:"fileadmin/protected/log/qfq.log";s:6:"qfqLog";s:31:"fileadmin/protected/log/qfq.log";s:10:"sqlLogMode";s:6:"modify";s:6:"sqlLog";s:31:"fileadmin/protected/log/sql.log";s:17:"formSubmitLogMode";s:3:"all";s:7:"mailLog";s:32:"fileadmin/protected/log/mail.log";s:13:"showDebugInfo";s:4:"auto";s:4:"init";s:14:"set names utf8";s:6:"update";s:4:"auto";s:9:"indexData";s:1:"1";s:8:"indexQfq";s:1:"1";s:17:"escapeTypeDefault";s:1:"m";s:20:"securityVarsHoneypot";s:23:"email,username,password";s:19:"securityAttackDelay";s:1:"5";s:19:"securityShowMessage";s:4:"true";s:20:"securityGetMaxLength";s:2:"50";s:24:"recordLockTimeoutSeconds";s:3:"900";s:13:"enterAsSubmit";s:1:"1";s:12:"editFormPage";s:4:"form";s:20:"formDataPatternError";s:0:"";s:21:"formDataRequiredError";s:0:"";s:18:"formDataMatchError";s:0:"";s:13:"formDataError";s:0:"";s:17:"showIdInFormTitle";s:1:"0";s:16:"cssClassColumnId";s:10:"text-muted";s:20:"cssClassQfqContainer";s:0:"";s:15:"cssClassQfqForm";s:0:"";s:19:"cssClassQfqFormPill";s:16:"qfq-color-grey-1";s:19:"cssClassQfqFormBody";s:16:"qfq-color-grey-2";s:13:"formBsColumns";s:2:"12";s:18:"formBsLabelColumns";s:1:"3";s:18:"formBsInputColumns";s:1:"6";s:17:"formBsNoteColumns";s:1:"3";s:21:"extraButtonInfoInline";s:70:"<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>";s:20:"extraButtonInfoBelow";s:80:"<span class="glyphicon glyphicon-info-sign text-info" aria-hidden="true"></span>";s:23:"extraButtonInfoPosition";s:4:"auto";s:20:"extraButtonInfoClass";s:0:"";s:15:"formLanguageAId";s:0:"";s:18:"formLanguageALabel";s:0:"";s:15:"formLanguageBId";s:0:"";s:18:"formLanguageBLabel";s:0:"";s:15:"formLanguageCId";s:0:"";s:18:"formLanguageCLabel";s:0:"";s:15:"formLanguageDId";s:0:"";s:18:"formLanguageDLabel";s:0:"";s:14:"saveButtonText";s:0:"";s:17:"saveButtonTooltip";s:4:"Save";s:15:"saveButtonClass";s:26:"btn btn-default navbar-btn";s:23:"saveButtonClassOnChange";s:19:"alert-info btn-info";s:19:"saveButtonGlyphIcon";s:12:"glyphicon-ok";s:15:"closeButtonText";s:0:"";s:18:"closeButtonTooltip";s:5:"Close";s:16:"closeButtonClass";s:26:"btn btn-default navbar-btn";s:20:"closeButtonGlyphIcon";s:16:"glyphicon-remove";s:16:"deleteButtonText";s:0:"";s:19:"deleteButtonTooltip";s:6:"Delete";s:17:"deleteButtonClass";s:26:"btn btn-default navbar-btn";s:21:"deleteButtonGlyphIcon";s:15:"glyphicon-trash";s:13:"newButtonText";s:0:"";s:16:"newButtonTooltip";s:3:"New";s:14:"newButtonClass";s:26:"btn btn-default navbar-btn";s:18:"newButtonGlyphIcon";s:14:"glyphicon-plus";s:7:"custom1";s:0:"";s:7:"custom2";s:0:"";s:7:"custom3";s:0:"";s:7:"custom4";s:0:"";s:7:"custom5";s:0:"";s:7:"custom6";s:0:"";s:7:"custom7";s:0:"";s:7:"custom8";s:0:"";s:7:"custom9";s:0:"";s:8:"custom10";s:0:"";s:8:"custom11";s:0:"";s:8:"custom12";s:0:"";s:8:"custom13";s:0:"";s:8:"custom14";s:0:"";s:8:"custom15";s:0:"";s:8:"custom16";s:0:"";s:8:"custom17";s:0:"";s:8:"custom18";s:0:"";s:8:"custom19";s:0:"";s:8:"custom20";s:0:"";s:8:"custom21";s:0:"";s:8:"custom22";s:0:"";s:8:"custom23";s:0:"";s:8:"custom24";s:0:"";s:8:"custom25";s:0:"";s:8:"custom26";s:0:"";s:8:"custom27";s:0:"";s:8:"custom28";s:0:"";s:8:"custom29";s:0:"";s:8:"custom30";s:0:"";}',
            'rsaauth' => 'a:1:{s:18:"temporaryDirectory";s:0:"";}',
            'saltedpasswords' => 'a:2:{s:3:"BE.";a:4:{s:21:"saltedPWHashingMethod";s:41:"TYPO3\\CMS\\Saltedpasswords\\Salt\\Pbkdf2Salt";s:11:"forceSalted";i:0;s:15:"onlyAuthService";i:0;s:12:"updatePasswd";i:1;}s:3:"FE.";a:5:{s:7:"enabled";i:1;s:21:"saltedPWHashingMethod";s:41:"TYPO3\\CMS\\Saltedpasswords\\Salt\\Pbkdf2Salt";s:11:"forceSalted";i:0;s:15:"onlyAuthService";i:0;s:12:"updatePasswd";i:1;}}',
            'xxxxxxxxxxxxxxx' => 'a:0:{}',
        ],
    ],
    'FE' => [
        'debug' => false,
        'loginSecurityLevel' => 'rsa',
    ],
    'GFX' => [
        'jpg_quality' => '80',
        'processor' => 'GraphicsMagick',
        'processor_allowTemporaryMasksAsPng' => false,
        'processor_colorspace' => 'RGB',
        'processor_effects' => -1,
        'processor_enabled' => true,
        'processor_path' => '/usr/bin/',
        'processor_path_lzw' => '/usr/bin/',
    ],
    'MAIL' => [
        'transport' => 'sendmail',
        'transport_sendmail_command' => '/usr/sbin/sendmail -t -i ',
        'transport_smtp_encrypt' => '',
        'transport_smtp_password' => '',
        'transport_smtp_server' => '',
        'transport_smtp_username' => '',
    ],
    'SYS' => [
        'caching' => [
            'cacheConfigurations' => [
                'extbase_object' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\ApcuBackend',
                    'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
                    'groups' => [
                        'system',
                    ],
                    'options' => [
                        'defaultLifetime' => 0,
                    ],
                ],
            ],
        ],
        'devIPmask' => '',
        'displayErrors' => 0,
        'enableDeprecationLog' => false,
        'encryptionKey' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'exceptionalErrors' => 20480,
        'isInitialDatabaseImportDone' => true,
        'isInitialInstallationInProgress' => false,
        'sitename' => 'New TYPO3 site',
        'sqlDebug' => 0,
        'systemLogLevel' => 2,
    ],
];
