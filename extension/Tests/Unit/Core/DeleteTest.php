<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/15/16
 * Time: 8:24 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;

use IMATHUZH\Qfq\Core\Delete;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;

require_once(__DIR__ . '/Database/AbstractDatabaseTest.php');

/**
 * Class DeleteTest
 * @package qfq
 */
class DeleteTest extends AbstractDatabaseTest {

    /**
     */
    public function testProcessException() {

        $this->expectException(\CodeException::class);
        $delete = new Delete(true);

        // empty 'form' not allowed
        $delete->process('', 123);
    }

    /**
     */
    public function testProcessException2() {

        $this->expectException(\CodeException::class);
        $delete = new Delete(true);

        // empty record id not allowed
        $formName = 'Person';
        $delete->process($formName, '');
    }

    /**
     */
    public function testProcessException3() {

        $this->expectException(\CodeException::class);
        $delete = new Delete(true);
        $formName = 'Person';

        // record id = 0 not allowed
        $delete->process($formName, 0);
    }

    /**
     */
    public function testProcessException4() {

        $this->expectException(\CodeException::class);
        $delete = new Delete(true);

        // unknown table not allowed
        $form[F_TABLE_NAME] = 'UnknownTable';
        $delete->process($form, 0);
    }

    /**
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {

        $this->store = Store::getInstance('form=TestFormName', true);
        parent::setUp();

        $this->store->setVar('form', 'TestFormName', STORE_TYPO3);

        $tempAppDir = '/tmp/deleteTest';
        if (!is_dir($tempAppDir)) {
            mkdir('/tmp/deleteTest');
        }
        Path::setAbsoluteApp('/tmp/deleteTest');
        // The above replaces the following line with a Path:: function. Probably won't work.
        // $this->store->setVar(SYSTEM_SITE_PATH_ABSOLUTE, '/tmp', STORE_SYSTEM, true);

        $this->executeSQLFile(__DIR__ . '/Database/fixtures/Generic.sql', true);
    }

    /**
     * @throws \UserFormException
     */
    protected function tearDown(): void {
        parent::tearDown();
        Path::findAbsoluteApp();
    }

}
