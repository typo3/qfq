<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/2/16
 * Time: 8:47 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;


use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Save;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;

require_once(__DIR__ . '/Database/AbstractDatabaseTest.php');

/**
 * Class SaveTest
 */
class SaveTest extends AbstractDatabaseTest {

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testUpdateRecord() {

        $save = new Save([F_DB_INDEX => DB_INDEX_DEFAULT], array(), array(), array());
//        $db = new Database();

        $values = ['name' => 'Doe', 'firstName' => 'John'];
        $id = $save->insertRecord('Person', $values);

        $values = ['name' => 'Doe', 'firstName' => 'Big John'];
        $save->updateRecord('Person', $values, $id);

        $sql = "SELECT name, firstName FROM Person WHERE id = ? ";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, [$id]);

        $this->assertEquals($values, $result[0]);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testInsertRecord() {

        $save = new Save([F_DB_INDEX => DB_INDEX_DEFAULT], array(), array(), array());
//        $db = new Database();

        $values = ['name' => 'Doe', 'firstName' => 'John'];
        $id = $save->insertRecord('Person', $values);

        $sql = "SELECT name, firstName FROM Person WHERE id = ? ";

        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, [$id]);

        $this->assertEquals($values, $result[0]);

    }

    /**
     * Very basic test: Fake a form with two text inputs.
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSaveBasicText() {
        $formSpec = [
            F_DB_INDEX => DB_INDEX_DEFAULT,
            F_TABLE_NAME => 'Person',
            F_MULTI_SQL => '',
            F_PRIMARY_KEY => F_PRIMARY_KEY_DEFAULT,
        ];

        $feSpecNative = [
            [
                FE_ID => 1,
                FE_TYPE => FE_TYPE_TEXT,
                FE_NAME => 'name',
            ],
            [
                FE_ID => 2,
                FE_TYPE => FE_TYPE_TEXT,
                FE_NAME => 'firstName',
            ]
        ];

        // Clean
        $this->dbArray[DB_INDEX_DEFAULT]->sql('DELETE FROM Person WHERE name LIKE "DoeTmp"');

        // Prepare Setup
        $tableDefinition = $this->dbArray[DB_INDEX_DEFAULT]->getTableDefinition('Person');
        $this->store->fillStoreTableDefaultColumnType($tableDefinition);

        // Init FE
        $feSpecNative = HelperFormElement::formElementSetDefault($feSpecNative, $formSpec);
        // Fake POST value
        $this->store::setStore(['name' => 'DoeTmp', 'firstName' => 'John'], STORE_FORM, true);

        // ------------- Check: Insert -------------
        // 1) Expect INSERT into Person
        $save = new Save($formSpec, [], $feSpecNative, $feSpecNative);
        $save->process(0);

        // Get newly save record
        $sql = "SELECT name, firstName FROM Person WHERE name like 'DoeTmp'";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_IMPLODE_ALL);

        // Compare
        $this->assertEquals('DoeTmpJohn', $result);


        // ------------- Check: Update -------------
        $id = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id FROM Person WHERE name="DoeTmp"', ROW_IMPLODE_ALL);
        // Fake POST value
        $this->store::setStore(['name' => 'DoeTmp', 'firstName' => 'JohnUpdate'], STORE_FORM, true);
        $this->store::setVar(SIP_RECORD_ID, $id, STORE_SIP, true);

        // 1) Expect UPDATE Person
        $save->process($id);

        // Get newly save record
        $sql = "SELECT name, firstName FROM Person WHERE name like 'DoeTmp'";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_IMPLODE_ALL);

        // Compare
        $this->assertEquals('DoeTmpJohnUpdate', $result);
    }

    /**
     * Test 'slaveId': Fake a form with two text inputs.
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSaveSlaveId() {

        $this->dbArray[DB_INDEX_DEFAULT]->sql('DELETE FROM Address WHERE street="deleteMe"');

        $formSpec = [
            F_DB_INDEX => DB_INDEX_DEFAULT,
            F_TABLE_NAME => 'Person',
            F_MULTI_SQL => '',
            F_PRIMARY_KEY => F_PRIMARY_KEY_DEFAULT,
        ];

        $feSpecNative = [
            [
                FE_ID => 1,
                FE_TYPE => FE_TYPE_TEXT,
                FE_NAME => 'firstName',
                FE_SQL_BEFORE => '{{UPDATE Address SET city=CONCAT(city, "-") WHERE id={{slaveId:V}} }}',
                FE_SLAVE_ID => '{{SELECT id FROM Address WHERE street="deleteMe"}}',
                FE_SQL_INSERT => '{{INSERT INTO Address (street, city) VALUES ("deleteMe","Zurich")}}',
                FE_SQL_UPDATE => '{{UPDATE Address SET city=CONCAT(city, " Oerlikon") WHERE id={{slaveId:V}} }}',
                FE_SQL_AFTER => '{{UPDATE Address SET city=CONCAT(city, "+") WHERE id={{slaveId:V}} }}',
            ]
        ];

        // Prepare Setup
        $tableDefinition = $this->dbArray[DB_INDEX_DEFAULT]->getTableDefinition('Person');
        $this->store->fillStoreTableDefaultColumnType($tableDefinition);

        // Init FE
        $feSpecNative = HelperFormElement::formElementSetDefault($feSpecNative, $formSpec);
        // Fake POST value
        $this->store::setStore(['name' => 'DoeTmp', 'firstName' => 'John'], STORE_FORM, true);

        /*
         * Check: sqlBefore/sqlInsert/sqlAfter
         *
         * slaveId becomes '0', cause there is no Address record 'street="deleteMe"'.
         * sqlBefore does nothing, cause there is no Address record 'street="deleteMe"'.
         * sqlInsert will create an Address record 'street="deleteMe"'.
         * sqlAfter adds '+' after city name.
         */
        $save = new Save($formSpec, [], $feSpecNative, $feSpecNative);
        $save->process(0);

        // Get newly save record
        $sql = "SELECT city FROM Address WHERE street like 'deleteMe'";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_IMPLODE_ALL);

        // Compare
        $this->assertEquals('Zurich+', $result);

        /*
         * Check: sqlBefore/sqlUpdate/sqlAfter - the previous 'deleteMe' record have to exist.
         *
         * slaveId becomes id of the previous created Address record 'street="deleteMe"'.
         * sqlBefore adds '-' after city name.
         * sqlUpdate adds ' Oerlikon' after city name.
         * sqlAfter adds '+' after city name.
         */

        $save->process(0);

        // Get newly save record
        $sql = "SELECT city FROM Address WHERE street like 'deleteMe'";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_IMPLODE_ALL);

        // Compare
        $this->assertEquals('Zurich+- Oerlikon+', $result);


        /*
         * Check: fillStoreVar['slaveId'] (no explicit fe.slaveId)
         *
         * slaveId becomes '0', cause there is no Address record 'street="deleteMe"'.
         * sqlBefore does nothing, cause there is no Address record 'street="deleteMe"'.
         * sqlInsert will create an Address record 'street="deleteMe"'.
         * sqlAfter adds '+' after city name.
         */

        // fillStoreVar
        $feSpecNative = [
            [
                FE_ID => 1,
                FE_TYPE => FE_TYPE_TEXT,
                FE_NAME => 'firstName',
                FE_FILL_STORE_VAR => '{{!SELECT id AS slaveId FROM Address WHERE street="deleteMe"}}',
                FE_SQL_UPDATE => '{{UPDATE Address SET city="Schwamedingen" WHERE id={{slaveId:V}} }}',
            ]
        ];

        $feSpecNative = HelperFormElement::formElementSetDefault($feSpecNative, $formSpec);

        $save = new Save($formSpec, [], $feSpecNative, $feSpecNative);
        $save->process(0);

        // Get newly save record
        $sql = "SELECT city FROM Address WHERE street like 'deleteMe'";
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_IMPLODE_ALL);

        // Compare
        $this->assertEquals('Schwamedingen', $result);

    }

    /**
     * UNIT Tests
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {
        parent::setUp();

        $this->executeSQLFile(__DIR__ . '/Database/fixtures/Generic.sql', true);
    }

}
