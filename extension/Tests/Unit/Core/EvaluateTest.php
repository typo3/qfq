<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/15/16
 * Time: 8:24 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;

use IMATHUZH\Qfq\Core\Evaluate;
use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;

require_once(__DIR__ . '/Database/AbstractDatabaseTest.php');

/**
 * Class EvaluateTest
 * @package qfq
 */
class EvaluateTest extends AbstractDatabaseTest {

    /**
     *
     */
    public function testVars() {
        try {
            $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

            // no variable
            $this->assertEquals('nothing', $eval->parse('nothing'));
            $this->assertEquals('TestFormName', $eval->parse('{{form:T}}'));

            //  pure digit variable
            $this->store->setVar('a', '1', 'C');
            $this->assertEquals('1', $eval->parse('{{a:C}}'));

            // variable contains variable
            $this->store->setVar('a', '{{b:C}}', 'C');
            $this->store->setVar('b', '1234', 'C');
            $this->assertEquals('1234', $eval->parse('{{a:C:all}}'));

            // multiple vaules, Recursive
            $this->store->setVar('m1', 'M1=[{{m2:C:all}}]', 'C');
            $this->store->setVar('m2', 'M2=[{{m3:C:all}}]', 'C');
            $this->store->setVar('m3', 'Some really nice text', 'C');
            $this->assertEquals('These are the variables > m1:M1=[M2=[Some really nice text]], m2:M2=[Some really nice text], m3:Some really nice text - end outer line',
                $eval->parse('These are the variables > m1:{{m1:C:all}}, m2:{{m2:C:all}}, m3:{{m3:C:all}} - end outer line'));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParse() {

        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        // database: lower case
        $this->assertEquals('1DoeJohn0malec2SmithJane0femalea,c', $eval->parse('{{select id,name,firstName,adrId,gender,groups from Person where id < 3 order by id}}'));

        // database: upper case
        $this->assertEquals('1DoeJohn0malec2SmithJane0femalea,c', $eval->parse('{{SELECT id,name,firstName,adrId,gender,groups FROM Person WHERE id < 3 ORDER BY id}}'));


        $this->store->setVar('sql', 'SELECT id,name,firstName,adrId,gender,groups FROM Person WHERE id < 3 ORDER BY id', 'C');
        $this->assertEquals('SELECT id,name,firstName,adrId,gender,groups FROM Person WHERE id < 3 ORDER BY id', $eval->parse('{{sql:C:all}}'));
        $this->assertEquals('1DoeJohn0malec2SmithJane0femalea,c', $eval->parse('{{{{sql:C:all}}}}'));

        // Get 2 row Array
        $expected = [
            ['id' => '1', 'name' => 'Doe',],
            ['id' => '2', 'name' => 'Smith',],
        ];
        $this->assertEquals($expected, $eval->parse('{{!SELECT id, name FROM Person WHERE id < 3 ORDER BY id}}'));

        // Get 2 row Array with nested query
        $expected = [
            ['id' => 1, 'name' => 'Doe', 'cnt' => 1],
            ['id' => 2, 'name' => 'Smith', 'cnt' => 1],
        ];
        $this->assertEquals($expected, $eval->parse('{{!SELECT id, name, {{SELECT 1 }} AS cnt FROM Person WHERE id < 3 ORDER BY id}}'));

        // Get empty array
        $this->assertEquals(array(), $eval->parse('{{!SELECT id, name FROM Person WHERE id=0}}'));


        // INSERT: Use 'Eval' to write a record
        $eval->parse('{{INSERT INTO Person (name, firstname) VALUES (\'Sinatra\', \'Frank\')}}');
        $this->assertEquals('Frank Sinatra', $eval->parse('{{SELECT firstname, " ", name FROM Person WHERE name="Sinatra" AND firstname="Frank"}}'));

        // INSERT & DELETE: Use eval() to write a record
        $eval->parse('{{INSERT INTO Person (name, firstname) VALUES (\'Sinatra\', \'Frank\')}}');
        $eval->parse('{{DELETE FROM Person WHERE name="Sinatra" AND firstname="Frank" LIMIT 1}}');
        $this->assertEquals('1', $eval->parse('{{SELECT count(id) FROM Person WHERE name="Sinatra" AND firstname="Frank"}}'));

        // UPDATE
        $eval->parse('{{UPDATE Person set name=\'Zappa\' WHERE name="Sinatra" AND firstname="Frank" LIMIT 1}}');
        $this->assertEquals('1', $eval->parse('{{SELECT count(id) FROM Person WHERE name="Zappa" AND firstname="Frank"}}'));

        // SHOW tables
        //$expected = "idbigint(20)NOPRIauto_incrementnamevarchar(128)YESfirstNamevarchar(128)YESadrIdint(11)NO0genderenum('','male','female')NOmalegroupsset('','a','b','c')NOmodifieddatetimeYEScurrent_timestamp()on update current_timestamp()createddatetimeYES";
        $expected = "idbigint(20)NOPRIauto_incrementnamevarchar(128)YESfirstNamevarchar(128)YESaccountvarchar(128)YESadrIdint(11)NO0genderenum('','male','female')NOmalegroupsset('','a','b','c')NOmodifieddatetimeYEScurrent_timestamp()on update current_timestamp()createddatetimeYES";

        $version = $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT @@version;", ROW_IMPLODE_ALL);
        preg_match('/^\d+(\.\d+)*(\.\d+)/', $version, $matches);
        $numericVersion = $matches[0];
        if (version_compare($numericVersion, '10.3', '<')) {
            # In 10.2,10.2:  CURRENT_TIMESTAMP
            $expected = "idbigint(20)NOPRIauto_incrementnamevarchar(128)YESfirstNamevarchar(128)YESadrIdint(11)NO0genderenum('','male','female')NOmalegroupsset('','a','b','c')NOmodifiedtimestampNOCURRENT_TIMESTAMPon update CURRENT_TIMESTAMPcreateddatetimeNO";
        }

        $this->assertEquals($expected, $eval->parse('{{SHOW COLUMNS FROM Person}}'));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParseStore() {

        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $allStores = [STORE_BEFORE, STORE_CLIENT, STORE_TABLE_DEFAULT, STORE_FORM, STORE_LDAP,
            STORE_TABLE_COLUMN_TYPES, STORE_PARENT_RECORD, STORE_SIP, STORE_TYPO3, STORE_USER, STORE_VAR, STORE_SYSTEM];

        // Not found anywhere
        foreach ($allStores as $store) {
            $this->assertEquals("$store - start {{not-set-in-any-store:$store}} end", $eval->parse("$store - start {{not-set-in-any-store:$store}} end"));
        }
        $this->assertEquals("E - start  end", $eval->parse('E - start {{not-set-in-any-store:E}} end'));
        $this->assertEquals("0 - start 0 end", $eval->parse('0 - start {{not-set-in-any-store:0}} end'));

        // All stores (but empty,zero): Check default
        foreach ($allStores as $store) {

            if ($store == STORE_EMPTY || $store == STORE_ZERO) {
                continue;
            }
            $this->assertEquals("$store - start Jane end", $eval->parse("$store - start {{not-set-in-any-store:$store:::Jane}} end"));
        }

        // Set and retrieve
        foreach ($allStores as $store) {
            $this->store::setVar('value', '0123', $store);
            $this->assertEquals("$store - start 0123 end", $eval->parse("$store - start {{value:$store}} end"));
        }

        // Check default sanitize class
        // Set and retrieve
        foreach ($allStores as $store) {
            if ($store == STORE_CLIENT || $store == STORE_FORM) {
                continue;
            }
            $this->store::setVar('value', 'John', $store);
            $this->assertEquals("$store - start John end", $eval->parse("$store - start {{value:$store}} end"));
        }
        $this->store::setVar('value', 'John', STORE_CLIENT);
        $this->assertEquals("start !!digit!! end", $eval->parse('start {{value:C}} end'));
        $this->store::setVar('value', 'John', STORE_FORM);
        $this->assertEquals("start !!digit!! end", $eval->parse('start {{value:F}} end'));

        // All Stores: Sanitize Class explizit set: alnumx
        foreach ($allStores as $store) {
            $this->assertEquals("start John end", $eval->parse('start {{value:' . $store . ':alnumx}} end'));
        }

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDbVars() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $eval->parse('{{INSERT INTO Person (name, firstname) VALUES (\'Holiday\', \'Billie\')}}');
        $this->store->setVar('a', '{{b:C:all}}', 'C');
        $this->store->setVar('b', 'Holiday', 'C');
        $this->assertEquals('Holiday', $eval->parse('{{SELECT name FROM Person WHERE name=\'{{a:C:all}}\'}}'));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParseArray() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $data = [
            'formName' => 'MyTestForm',
            'title' => 'Person: {{SELECT firstname, " ", name FROM Person LIMIT 1}} / Employee',
            'key1' => 'Hello',
            'key2' => 'World',
        ];
        $expected = [
            'formName' => 'MyTestForm',
            'title' => 'Person: John Doe / Employee',
            'key1' => 'Hello',
            'key2' => 'World',
        ];

        $this->assertEquals($expected, $eval->parseArray($data));

        $expected2 = $expected;
        $expected2['title'] = $data['title'];
        $this->assertEquals($expected2, $eval->parseArray($data, ['formName', 'title']));

        $expected2 = $expected;
        $data['formName'] = 'SELECT * FROM unknown, garbage, WITH missing, parameter';
        $expected2['formName'] = $data['formName'];

        $expected2['title'] = $data['title'];
        $this->assertEquals($expected2, $eval->parseArray($data, ['formName', 'title']));
    }


    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParseArrayOfArray() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);
        $data = [
            [
                'formName' => 'MyTestForm',
                'title' => 'Person: {{SELECT firstname, " ", name FROM Person LIMIT 1}} / Employee',
            ],
            [
                'order' => '100',
                'class' => 'Person: {{SELECT firstname, " ", name FROM Person LIMIT 1}} / Employee',
            ],
        ];
        $expected = [
            [
                'formName' => 'MyTestForm',
                'title' => 'Person: John Doe / Employee',
            ],
            [
                'order' => '100',
                'class' => 'Person: John Doe / Employee',
            ],
        ];

        $this->assertEquals($expected, $eval->parseArray($data));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSubstituteSql() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $expectArr = [0 => ['name' => 'Holiday', 'firstname' => 'Billie'], 1 => ['name' => 'Holiday', 'firstname' => 'Billie']];

        $eval->parse('{{INSERT INTO Person (name, firstname) VALUES (\'Holiday\', \'Billie\')}}');
        $eval->parse('{{INSERT INTO Person (name, firstname) VALUES (\'Holiday\', \'Billie\')}}');
        $this->store->setVar('a', '{{b:C:all}}', 'C');
        $this->store->setVar('b', 'Holiday', 'C');

        // Fire query: empty
        $this->assertEquals('', $eval->substitute('SELECT name FROM Person WHERE id=0', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query
        $this->assertEquals('Holiday', $eval->substitute('SELECT name FROM Person WHERE name LIKE "Holiday" LIMIT 1', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, surrounding whitespace
        $this->assertEquals('Holiday', $eval->substitute('    SELECT name FROM Person WHERE name LIKE "Holiday" LIMIT 1   ', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, lowercase
        $this->assertEquals('Holiday', $eval->substitute('   SELECT    name FROM Person WHERE name LIKE "Holiday" LIMIT 1', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, join two records
        $this->assertEquals('HolidayBillieHolidayBillie', $eval->substitute('SELECT name, firstname FROM Person WHERE name LIKE "Holiday" LIMIT 2', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, two records as assoc
        $this->assertEquals($expectArr, $eval->substitute('!SELECT name, firstname FROM Person WHERE name LIKE "Holiday" LIMIT 2', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, no result
        $this->assertEquals(array(), $eval->substitute('!SELECT name, firstname FROM Person WHERE id=0', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);

        // Fire query, two records as assoc, surrounding whitespace
        $this->assertEquals($expectArr, $eval->substitute('  !select  name, firstname FROM Person WHERE name LIKE "Holiday" LIMIT 2  ', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_IN_STORE_QUERY, $foundInStore);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSubstituteVar() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        // Retrieve in PRIO `FSRD`
        $this->store->setVar('a', '1234', STORE_TABLE_DEFAULT, true);
        $this->assertEquals('1234', $eval->substitute('a', $foundInStore));
        $this->assertEquals(STORE_TABLE_DEFAULT, $foundInStore);

        $this->store->setVar('a', '12345', STORE_RECORD, true);
        $this->assertEquals('12345', $eval->substitute('a', $foundInStore));
        $this->assertEquals(STORE_RECORD, $foundInStore);

        $this->store->setVar('a', '123456', STORE_SIP, true);
        $this->assertEquals('123456', $eval->substitute('a', $foundInStore));
        $this->assertEquals(STORE_SIP, $foundInStore);

        $this->store->setVar('a', '1234567', STORE_FORM, true);
        $this->assertEquals('1234567', $eval->substitute('a', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals(false, $eval->substitute('notFound', $foundInStore));
        $this->assertEquals('', $foundInStore);

        // Specific Store
        $this->assertEquals('1234', $eval->substitute('a:D', $foundInStore));
        $this->assertEquals(STORE_TABLE_DEFAULT, $foundInStore);

        $this->assertEquals('12345', $eval->substitute('a:R', $foundInStore));
        $this->assertEquals(STORE_RECORD, $foundInStore);

        $this->assertEquals('123456', $eval->substitute('a:S', $foundInStore));
        $this->assertEquals(STORE_SIP, $foundInStore);

        $this->assertEquals('1234567', $eval->substitute('a:F', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals(false, $eval->substitute('a:V', $foundInStore));
        $this->assertEquals('', $foundInStore);

        // Sanitize Class: digits
        $this->assertEquals('1234567', $eval->substitute('a:F:digit', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('1234567', $eval->substitute('a:F:alnumx', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('1234567', $eval->substitute('a:F:allbut', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('1234567', $eval->substitute('a:F:all', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // Sanitize Class: text
        $this->store->setVar('a', 'Hello world @-_.,;: /()', STORE_FORM, true);

        $this->assertEquals('!!digit!!', $eval->substitute('a:F:digit', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('Hello world @-_.,;: /()', $eval->substitute('a:F:alnumx', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('Hello world @-_.,;: /()', $eval->substitute('a:F:allbut', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('Hello world @-_.,;: /()', $eval->substitute('a:F:all', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSubstituteVarEscape() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        // No escape
        $this->store->setVar('a', 'hello', STORE_FORM, true);
        $this->assertEquals('hello', $eval->substitute('a:F:all', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // None, Single Tick
        $this->store->setVar('a', 'hello', STORE_FORM, true);
        $this->assertEquals('hello', $eval->substitute('a:F:all:s', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // None, Double Tick
        $this->store->setVar('a', 'hello', STORE_FORM, true);
        $this->assertEquals('hello', $eval->substitute('a:F:all:d', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // None, Colon
        $this->store->setVar('a', 'hello', STORE_FORM, true);
        $this->assertEquals('hello', $eval->substitute('a:F:all:C', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);


        // ', Single Tick
        $this->store->setVar('a', 'hel\'lo', STORE_FORM, true);
        $this->assertEquals('hel\\\'lo', $eval->substitute('a:F:all:s', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // ' Double Tick
        $this->store->setVar('a', 'hel\'lo', STORE_FORM, true);
        $this->assertEquals('hel\'lo', $eval->substitute('a:F:all:d', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // Colon
        $this->store->setVar('a', 'hel:lo', STORE_FORM, true);
        $this->assertEquals('hel\:lo', $eval->substitute('a:F:all:C', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);


        // ", Single Tick
        $this->store->setVar('a', 'hel"lo', STORE_FORM, true);
        $this->assertEquals('hel"lo', $eval->substitute('a:F:all:s', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // ", Double Tick
        $this->store->setVar('a', 'hel"lo', STORE_FORM, true);
        $this->assertEquals('hel\"lo', $eval->substitute('a:F:all:d', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);


        // Multi ', Single Tick
        $this->store->setVar('a', "h\"e' 'l\"lo ' ", STORE_FORM, true);
        $this->assertEquals("h\"e\' \'l\"lo \' ", $eval->substitute('a:F:all:s', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        // Multi , Double Tick
        $this->store->setVar('a', 'h"e\' \'l"lo \' ', STORE_FORM, true);
        $this->assertEquals('h\"e\' \'l\"lo \' ', $eval->substitute('a:F:all:d', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

//---
        $this->store->setVar('a', ' hello world ', STORE_FORM, true);
        $this->assertEquals(' hello world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', ' hel\lo world ', STORE_FORM, true);
        $this->assertEquals(' hel\5clo world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', ' hel*lo world ', STORE_FORM, true);
        $this->assertEquals(' hel\2alo world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', ' hel(lo world ', STORE_FORM, true);
        $this->assertEquals(' hel\28lo world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', ' hel)lo world ', STORE_FORM, true);
        $this->assertEquals(' hel\29lo world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', " hel\x00lo world ", STORE_FORM, true);
        $this->assertEquals(' hel\00lo world ', $eval->substitute('a:F:all:l', $foundInStore));

        $this->store->setVar('a', ' h\e*l(l)o world ', STORE_FORM, true);
        $this->assertEquals(' h\5ce\2al\28l\29o world ', $eval->substitute('a:F:all:l', $foundInStore));


//        LDAP_ESCAPE_FILTER => array('\\', '*', '(', ')', "\x00"),
//                LDAP_ESCAPE_DN => array('\\', ',', '=', '+', '<', '>', ';', '"', '#'),
        $this->store->setVar('a', ' hello world ', STORE_FORM, true);
//        $this->assertEquals('\\20hello world\\20', $eval->substitute('a:F:all:L', $foundInStore));

        $this->store->setVar('a', 'h\e,l=l+o< >w;o"r#ld', STORE_FORM, true);
        $this->assertEquals('h\5ce\2cl\3dl\2bo\3c \3ew\3bo\22r\23ld', $eval->substitute('a:F:all:L', $foundInStore));

        $this->store->setVar('a', ' hel;lo world ', STORE_FORM, true);
//        $this->assertEquals('\20hel\3blo world\20', $eval->substitute('a:F:all:sL', $foundInStore));


        // The test needs a T3 installation to call the T3 password hash function - this is not available on alfred16
        // The password contains a dynamic salt - we can't force the salt and we can't force the hash
//        $this->store->setVar('a', 'password', STORE_FORM, true);
//        $result = $eval->substitute('a:F::p', $foundInStore);
//
//        $this->assertEquals(34, strlen($result));
//        $this->assertEquals('$P$', substr($result, 0, 3));

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSubstituteDefault() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        // No escape
        $this->store->setVar('a', 'hello', STORE_FORM, true);
        $this->assertEquals('hello', $eval->substitute('a:F:all::world', $foundInStore));
        $this->assertEquals(STORE_FORM, $foundInStore);

        $this->assertEquals('world', $eval->substitute('a-notused:F:all::world', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_AS_DEFAULT, $foundInStore);

        $this->assertEquals('hello:world', $eval->substitute('a-notused:F:all::hello\\:world', $foundInStore));
        $this->assertEquals(TOKEN_FOUND_AS_DEFAULT, $foundInStore);

        $this->assertEquals(false, $eval->substitute('a-notused:F:all::', $foundInStore));
        $this->assertEquals('', $foundInStore);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testTypeViolateMessage() {

        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $store = STORE_CLIENT;

        $this->store::setVar('value', 'Joh%n', $store);
        $this->assertEquals("$store - start 0 end", $eval->parse("$store - start {{value:$store:digit:::0}} end"));

        $this->store::setVar('value', 'Joh%n', $store);
        $this->assertEquals("$store - start  end", $eval->parse("$store - start {{value:$store:digit:::e}} end"));

        $this->store::setVar('value', 'Joh%n', $store);
        $this->assertEquals("$store - start !!digit!! end", $eval->parse("$store - start {{value:$store:digit:::c}} end"));

        $this->store::setVar('value', 'Joh%n', $store);
        $this->assertEquals("$store - start custom message end", $eval->parse("$store - start {{value:$store:digit:::custom message}} end"));
    }


    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParseSpecialCharInParameter() {

        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $store = STORE_CLIENT;

        // default
        $this->assertEquals("$store - start default mit : in string end", $eval->parse("$store - start {{not in store:$store:alnumx::default mit \: in string}} end"));
        $this->assertEquals("$store - start default mit ' in string end", $eval->parse("$store - start {{not in store:$store:alnumx::default mit ' in string}} end"));
        $this->assertEquals("$store - start default mit \" in string end", $eval->parse("$store - start {{not in store:$store:alnumx::default mit \" in string}} end"));

        // typeMessageViolate
        $this->store::setVar('value', 'John', $store);
        $this->assertEquals("$store - start violate standard end", $eval->parse("$store - start {{value:$store::::violate standard}} end"));
        $this->assertEquals("$store - start violate with : in string end", $eval->parse("$store - start {{value:$store::::violate with \: in string}} end"));
        $this->assertEquals("$store - start violate with \' in string end", $eval->parse("$store - start {{value:$store::::violate with ' in string}} end"));
        $this->assertEquals("$store - start violate with \\\" in string end", $eval->parse("$store - start {{value:$store::::violate with \" in string}} end"));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testParseActionEscape() {
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $this->store->setVar('hidden', 'magic value', STORE_FORM, true);
        $this->store->setVar('a', 'start {{hidden:F:alnumx}} end', STORE_FORM, true);

        // Recursive replace (already tested)
        $this->assertEquals('go start magic value end stop', $eval->parse('go {{a:F:all}} stop'));

        // Stop Recursive replace
        $this->assertEquals('go start {{hidden:F:alnumx}} end stop', $eval->parse('go {{a:F:all:S}} stop'));

        // Check that there is no exception if not found in the first/second store but in third store
        $this->store->setVar('neverUsedBefore', 'sunshine', STORE_FORM, true);
        $this->assertEquals('go sunshine stop', $eval->parse('go {{neverUsedBefore:SRF:all:X}} stop'));

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testVariableNotFoundException() {

        $this->expectException(\UserFormException::class);
        $eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);

        $eval->parse('go {{unknownVar:S::X}} stop');
    }

    /**
     */
    protected function setUp(): void {

        try {
            $this->store = Store::getInstance('form=TestFormName', true);
            parent::setUp();

            $this->store->setVar('form', 'TestFormName', STORE_TYPO3);
            $this->executeSQLFile(__DIR__ . '/Database/fixtures/Generic.sql', true);

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
