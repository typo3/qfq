<?php
/**
 *  Created by PhpStorm
 *  User: zhoujl
 *  Date: 17.05.2024
 *  Time: 14:00 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;

use IMATHUZH\Qfq\Core\BuildFormBootstrap;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;
use ReflectionClass;


/**
 * Class MultiFormTest
 *
 * This class tests the functionality of MultiForm using PHPUnit.
 * It extends AbstractDatabaseTest and mocks the necessary dependencies.
 */
class MultiFormTest extends AbstractDatabaseTest {

    private $testBuildForm;
    private $formSpec;
    private $feSpecAction;
    private $feSpecNative;
    protected $store;
    private $filter;
    private $modeCollectFe;

    /**
     * Sets up the test environment.
     *
     * This method initializes the form specification and other dependencies
     * required for the tests.
     *
     * @return void
     */
    protected function setUp(): void {
        // Mocking data
        $this->formSpec = [
            F_DB_INDEX => DB_INDEX_DEFAULT,
            F_MULTI_SQL => '{{!SELECT id AS id FROM Person}}',
            FE_FILL_STORE_VAR => '',
            F_TABLE_NAME => 'Person',
            F_PRIMARY_KEY => 'id',
            F_SAVE_BUTTON_CLASS => '',
            F_ID => 'id'
        ];

        $this->feSpecAction = [];
        $this->feSpecNative = [];
        $this->dbArray[DB_INDEX_DEFAULT] = new \IMATHUZH\Qfq\Core\Database\Database();

        $this->filter = 'native_subrecord';
        $this->modeCollectFe = 'flagDynamicUpdate';
    }

    /**
     * Tests if the 'plus' button is rendered correctly.
     *
     * This method sets the form specification to add a row and then uses
     * reflection to invoke the protected buildMultiForm method to check if
     * the 'addRowButton' class is present in the rendered HTML.
     *
     * @return void
     * @throws \CodeException
     * @throws \ReflectionException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderPlusButton() {
        // Set the form specification to add a row
        $this->formSpec[F_MULTIFORM_ADD_ROW] = '1';

        // Initialize the BuildFormBootstrap instance with the specified form and action configurations
        $this->testBuildForm = new BuildFormBootstrap($this->formSpec, $this->feSpecAction, $this->feSpecNative, $this->dbArray);

        // Use reflection to access the protected method 'buildMultiForm'
        $reflection = new ReflectionClass($this->testBuildForm);
        $method = $reflection->getMethod('buildMultiForm');

        // Initialize an array to collect JSON data
        $rcJson = [];

        // Invoke the protected method with the specified parameters and collect the rendered HTML
        $html = $method->invokeArgs($this->testBuildForm, [$this->filter, $this->modeCollectFe, &$rcJson]);

        // Assert that the rendered HTML contains the 'addRowButton' class
        $this->assertStringContainsString('class="addRowButton"', $html);
    }

    /**
     * Tests if a dummy row is generated correctly.
     *
     * This method sets the form specification to add a row and then uses
     * reflection to invoke the protected buildMultiForm method to check if
     * the 'dummy-row' class is present in the rendered HTML.
     *
     * @return void
     * @throws \CodeException
     * @throws \ReflectionException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDummyRowGeneratedCorrectly() {
        // Set the form specification to add a row
        $this->formSpec[F_MULTIFORM_ADD_ROW] = '1';

        // Initialize the BuildFormBootstrap instance with the specified form and action configurations
        $this->testBuildForm = new BuildFormBootstrap($this->formSpec, $this->feSpecAction, $this->feSpecNative, $this->dbArray);

        // Use reflection to access the protected method 'buildMultiForm'
        $reflection = new ReflectionClass($this->testBuildForm);
        $method = $reflection->getMethod('buildMultiForm');

        // Initialize an array to collect JSON data
        $rcJson = [];

        // Invoke the protected method with the specified parameters and collect the rendered HTML
        $html = $method->invokeArgs($this->testBuildForm, [$this->filter, $this->modeCollectFe, &$rcJson]);

        // Assert that the rendered HTML contains the 'dummy-row' class
        $this->assertStringContainsString('class="dummy-row"', $html);
    }

    /**
     * Tests that the delete button is not rendered.
     *
     * This method sets the form specification to not allow deleting rows and
     * then uses reflection to invoke the protected buildMultiForm method to
     * check if the 'delete row' class is absent in the rendered HTML.
     *
     * @return void
     * @throws \CodeException
     * @throws \ReflectionException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDeleteButtonNotRendered() {
        $this->formSpec[F_MULTIFORM_DELETE_ROW] = '0';
        $this->testBuildForm = new BuildFormBootstrap($this->formSpec, $this->feSpecAction, $this->feSpecNative, $this->dbArray);

        // Use reflection to access the protected method
        $reflection = new ReflectionClass($this->testBuildForm);
        $method = $reflection->getMethod('buildMultiForm');

        $rcJson = [];
        $html = $method->invokeArgs($this->testBuildForm, [$this->filter, $this->modeCollectFe, &$rcJson]);

        $this->assertStringNotContainsString('class="delete row"', $html);
    }

    /**
     * Tests checkbox activation with processRow.
     *
     * This method initializes the form specification and invokes the
     * processRow method to verify if the checkboxes are activated correctly.
     *
     * @return void
     * @throws \CodeException
     * @throws \ReflectionException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckboxActivationWithProcessRow() {
        // Set the form specification for processing rows
        $this->formSpec[F_PROCESS_ROW] = '';

        // Initialize the BuildFormBootstrap instance with the specified form and action configurations
        $this->testBuildForm = new BuildFormBootstrap($this->formSpec, $this->feSpecAction, $this->feSpecNative, $this->dbArray);

        // Test data for processRow
        $parentRecords = [
            ['id' => 0, F_PROCESS_ROW_COLUMN => 1],
        ];
        $idName = 'id';
        $counter = 1;

        // Use reflection to access the protected method 'processRow'
        $reflection = new ReflectionClass($this->testBuildForm);
        $method = $reflection->getMethod('processRow');

        // Call the method with the test data
        $method->invokeArgs($this->testBuildForm, [&$parentRecords, $idName, &$counter]);

        // Verify the processRow modifications
        foreach ($parentRecords as $record) {
            $processRowKey = null;

            // Find the key for the process-row checkbox
            foreach (array_keys($record) as $key) {
                if (strpos($key, '<label class="checkbox process-row-all process-row-label-header"><input type="checkbox"><span></span') !== false) {
                    $processRowKey = $key;
                    break;
                }
            }
        }

        // Check if the checkbox is rendered as checked
        $this->assertStringContainsString('checked="checked"', $parentRecords[0][$processRowKey]);
    }
}