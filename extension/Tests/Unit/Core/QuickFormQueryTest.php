<?php

/**
 * Created by PhpStorm.
 * User: crose
 * Date: 12/23/15
 * Time: 7:11 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;


use IMATHUZH\Qfq\Core\QuickFormQuery;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;
use PHPUnit\Framework\TestCase;

/**
 * Class QuickFormQueryTest
 */
class QuickFormQueryTest extends TestCase {

    /**
     * @var \mysqli
     */
    static protected $mysqli = null;

    private $store;

//* FORM_LOAD:
//*   Specified in T3 body text with form=<formname>            Returned Store:Typo3
//*   Specified in T3 body text with form={{form}} ':FSRD'      Returned Store:SIP
//*   Specified in T3 body text with form={{form:C:ALNUMX}}     Returned Store:Client
//*   Specified in T3 body text with form={{SELECT registrationFormName FROM Conference WHERE id={{conferenceId:S0}} }}
//     *   Specified in T3 body text with form={{SELECT registrationFormName FROM Conference WHERE id={{conferenceId:C0:DIGIT}} }}
//     *   Specified in SIP
//*
//     * FORM_SAVE:
//     *   Specified in SIP
//*
//     *
//     * @param string $mode FORM_LOAD|FORM_SAVE|FORM_UPDATE
//* @param string $foundInStore
//* @return array|bool|mixed|null|string  Formname (Form.name) or FALSE, if no formname found.
//     * @throws \CodeException
//* @throws \UserFormException
//*/

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGetFormName() {

        $store = Store::getInstance();

        // <empty> bodytext
        $store->unsetStore(STORE_TYPO3);
        $t3data[T3DATA_BODYTEXT] = "\n   \n \n";
        $t3data[T3DATA_UID] = "123";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
        $this->assertEquals('', $result);

        // form=
        $store->unsetStore(STORE_TYPO3);
        $t3data[T3DATA_BODYTEXT] = "\n   \n" . TYPO3_FORM . "=\n";
        $t3data[T3DATA_UID] = "123";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
        $this->assertEquals('', $result);

        // form=<formname>
        $store->unsetStore(STORE_TYPO3);
        $t3data[T3DATA_BODYTEXT] = "\n   \n" . TYPO3_FORM . "=myForm\n";
        $t3data[T3DATA_UID] = "123";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
        $this->assertEquals('myForm', $result);

        // form={{SELECT 'nextForm'}}
        $store->unsetStore(STORE_TYPO3);
        $t3data[T3DATA_BODYTEXT] = "\n   \n" . TYPO3_FORM . "={{SELECT 'nextForm'}}\n";
        $t3data[T3DATA_UID] = "123";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
        $this->assertEquals('nextForm', $result);

        // form={{form:C:alnumx}}
        $store->unsetStore(STORE_TYPO3);
        $store->setVar(CLIENT_FORM, 'formNameViaGet', STORE_CLIENT);
        $t3data[T3DATA_BODYTEXT] = "\n   \n" . TYPO3_FORM . "={{form:C:alnumx}}\n";
        $t3data[T3DATA_UID] = "123";
//        $_SERVER['form'] = 'formNameViaGet';
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
        $this->assertEquals('formNameViaGet', $result);

        // form={{form}}
//        $sip = new Sip('fakesessionname', true);
//        $sip->sipUniqId('badcaffee1234');
//        $t3data[T3DATA_BODYTEXT] = "\n   \n" . TYPO3_FORM . "={{form}}\n";
//        $t3data[T3DATA_UID] = "123";
//        $_SERVER[CLIENT_SIP]='badcaffee1234';
//        $dummy = $sip->queryStringToSip("http://example.com/index.php?id=input&r=1&form=person", RETURN_URL);
//        $qfq = new QuickFormQuery($t3data, true);
//        $result = $qfq->getFormName(FORM_LOAD, $foundInStore);
//        $this->assertEquals('person', $result);


    }

    public function testRenderMode() {
        $store = Store::getInstance();

        // form={{form:SE}}
        // 10.sql = SELECT '&Hello World'
        $store->unsetStore(STORE_TYPO3);

        // Set dummys for QuickFormQuery test
        $store->setVar(SYSTEM_UNIT_TEST_FORM_CONTENT, 'FormLoaded', STORE_SYSTEM);
        $store->setVar(SYSTEM_DRAG_AND_DROP_JS, 'false', STORE_SYSTEM);
        $store->setVar('form', 'phpunit_person', STORE_SIP);
        $store->setVar('r', '0', STORE_SIP);
        $store->setVar(SIP_SIP, 'fakeSip', STORE_SIP);
        $t3data[T3DATA_UID] = "123";

        // QFQ Config - render = single
        $store->setVar(SYSTEM_RENDER, SYSTEM_RENDER_SINGLE, STORE_SYSTEM);

        // In report - nothing given
        $t3data[T3DATA_BODYTEXT] = "form={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded', $result);

        // In report - render = both
        $t3data[T3DATA_BODYTEXT] = "render=both\nform={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded&Hello World', $result);

        // In report - render = single
        $t3data[T3DATA_BODYTEXT] = "render=single\nform={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded', $result);

        // QFQ Config - render = both
        $store->setVar(SYSTEM_RENDER, SYSTEM_RENDER_BOTH, STORE_SYSTEM);

        // In report - nothing given
        $t3data[T3DATA_BODYTEXT] = "form={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded&Hello World', $result);

        // In report - render = both
        $t3data[T3DATA_BODYTEXT] = "render=both\nform={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded&Hello World', $result);

        // In report - render = single
        $t3data[T3DATA_BODYTEXT] = "render=single\nform={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('FormLoaded', $result);

        // In report - render = api. Unset unit test variable for this one.
        $store->unsetVar(SYSTEM_UNIT_TEST_FORM_CONTENT, STORE_SYSTEM);
        $GLOBALS['TYPO3_CONF_VARS'] = 'fakeVars';
        $t3data[T3DATA_BODYTEXT] = "render=api\nform={{form:SE}}\n20.sql = SELECT '&Hello World'";
        $qfq = new QuickFormQuery($t3data, true);
        $result = $qfq->process();
        $this->assertEquals('', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \Exception
     */
    protected function setUp(): void {

        Session::getInstance(true);

        // Init the store also reads db credential configuration
        $this->store = Store::getInstance('', true);

        $dbName = $this->store->getVar('DB_1_NAME', STORE_SYSTEM);

        if ($dbName == '') {
            throw new \CodeException('Missing DB_1_NAME in ' . CONFIG_QFQ_PHP, ERROR_MISSING_REQUIRED_PARAMETER);
        } else {
            if (strpos($dbName, '_phpunit') === false) {
                $dbName .= '_phpunit';
                $this->store->setVar('DB_1_NAME', $dbName, STORE_SYSTEM);
            }
        }

        /// Establish additional mysqli access
        $dbserver = $this->store->getVar('DB_1_SERVER', STORE_SYSTEM);
        $dbuser = $this->store->getVar('DB_1_USER', STORE_SYSTEM);
        $db = $this->store->getVar('DB_1_NAME', STORE_SYSTEM);
        $dbpw = $this->store->getVar('DB_1_PASSWORD', STORE_SYSTEM);

        if (self::$mysqli === null) {

            self::$mysqli = new \mysqli($dbserver, $dbuser, $dbpw, $db);

            if (self::$mysqli->connect_errno) {
                throw new \Exception("Unable to connect to mysql server");
            }
        }

        try {
            AbstractDatabaseTest::executeSQLFile(__DIR__ . '/Database/fixtures/TestForm.sql', true, self::$mysqli);
        } catch (\Exception $e) {
            echo $e->getMessage();
            return;
        }
    }

    /**
     *
     */
    protected function tearDown(): void {
        parent::tearDown();

        self::$mysqli->real_query("TRUNCATE TABLE FormElement");
        self::$mysqli->real_query("TRUNCATE TABLE Form");
    }
}
