<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Renderer\Element;

use IMATHUZH\Qfq\Core\Exception\RendererException;
use PHPUnit\Framework\TestCase;
use IMATHUZH\Qfq\Core\Renderer\Element\IconRenderer;

class IconRendererTest extends TestCase {
    protected function assertHtmlOutput(array $args, string $tag, string $family, string $icon) {
        $expected = "<$tag class=\"$family $family-$icon\" ></$tag>";

        $static = IconRenderer::renderStatic($args);
        $this->assertEquals($expected, $static);

        $renderer = new IconRenderer();
        $html = $renderer->render($args);
        $this->assertEquals($expected, $html);
    }

    public function testDefaultArguments() {
        $this->assertHtmlOutput(
            [ICON_RENDERER_NAME => 'my-icon'],
            ICON_RENDERER_DEFAULT_TAG,
            ICON_RENDERER_DEFAULT_FAMILY,
            'my-icon'
        );
    }

    public function testDirectArguments() {
        $this->assertHtmlOutput(
            [
                ICON_RENDERER_TAG => 'iconTag',
                ICON_RENDERER_FAMILY => 'myFam',
                ICON_RENDERER_NAME => 'myIcon'
            ],
            'iconTag', 'myFam', 'myIcon'
        );
    }

    public function testMissingIconName() {
        try {
            IconRenderer::renderStatic([]);
            $this->fail("Expecting RendererException but none was thrown");
        } catch (RendererException $e) {
            $msg = $e->getMessage();
            $this->assertStringContainsString(IconRenderer::class, $msg);
            $this->assertStringContainsString(ICON_RENDERER_NAME, $msg);
        }
    }
}