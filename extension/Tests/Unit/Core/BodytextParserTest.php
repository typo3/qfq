<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 3/21/16
 * Time: 9:29 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core;

use IMATHUZH\Qfq\Core\BodytextParser;

use PHPUnit\Framework\TestCase;

/**
 * Class BodytextParserTest
 * @package qfq
 */
final class BodytextParserTest extends TestCase {

    public function testProcessPlain() {
        $btp = new BodytextParser();

        // Simple row, nothing to remove
        $given = "10.sql = SELECT 'Hello World'";
        $expected = $given;
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple row, nothing to remove
        $given = "{\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple row, nothing to remove
        $given = "myAlias {\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Several rows, remove all but one
        $given = "\n#some comments\n10.sql = SELECT 'Hello World'\n\n    \n   #more comment";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Several rows, remove all but one
        $given = "\n#some comments\n{\nsql = SELECT 'Hello World'\n\n    \n   #more comment\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Several rows, remove all but one
        $given = "\n#some comments\nmyAlias {\nsql = SELECT 'Hello World'\n\n    \n   #more comment\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Several rows, all to remove
        $given = "\n#some comments\n\n\n    \n   #more comment";
        $expected = "";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Join a line
        $given = "\n10.sql = SELECT 'Hello World',\n'more content'\n      WHERE help=1";
        $expected = "10.sql = SELECT 'Hello World', 'more content' WHERE help=1";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Join a line
        $given = "\n{\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}";
        $expected = "1.sql = SELECT 'Hello World', 'more content' WHERE help=1";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Join a line
        $given = "\n    myAlias     {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}";
        $expected = "1.sql = SELECT 'Hello World', 'more content' WHERE help=1";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Join several lines, incl. form
        $given = "\n10.sql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n 20.head = <table>\n 30.sql     =   SELECT\n col1,\n col2, \n col3\n  # Query stops here\nform = Person\n";
        $expected = "10.sql = SELECT 'Hello World', 'more content' WHERE help=1\n20.head = <table>\n30.sql     =   SELECT col1, col2, col3\nform = Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Join several lines, incl. form
        $given = "\n{\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}\n{\nhead = <table>\n}\n{\nsql     =   SELECT\n col1,\n col2, \n col3\n  # Query stops here\n}\nform = Person\n";
        $expected = "1.sql = SELECT 'Hello World', 'more content' WHERE help=1\n2.head = <table>\n3.sql     =   SELECT col1, col2, col3\nform = Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Join several lines, incl. form
        $given = "\nmyAlias {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}\n   mySecondAlias  {\nhead = <table>\n}\nmyThirdAlias  {\nsql     =   SELECT\n col1,\n col2, \n col3\n  # Query stops here\n}\nform = Person\n";
        $expected = "1.sql = SELECT 'Hello World', 'more content' WHERE help=1\n2.head = <table>\n3.sql     =   SELECT col1, col2, col3\nform = Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one.
        $given = "10{\nsql = SELECT 'Hello World'\n}\n";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one.
        $given = "{\nsql = SELECT 'Hello World'\n}\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one.
        $given = "myAlias {\nsql = SELECT 'Hello World'\n}\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one. No LF at the end
        $given = "10{\nsql = SELECT 'Hello World'\n}";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one. No LF at the end
        $given = "{\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one. No LF at the end
        $given = "myAlias {\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression, one, added some white spaces
        $given = "\n\n10 { \n \n   sql    =    SELECT 'Hello World'     \n\n    }\n\n";
        $expected = "10.sql    =    SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression, one, added some white spaces
        $given = "\n\n { \n \n   sql    =    SELECT 'Hello World'     \n\n    }\n\n";
        $expected = "1.sql    =    SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression, one, added some white spaces
        $given = "\n\n myAlias  { \n \n   sql    =    SELECT 'Hello World'     \n\n    }\n\n";
        $expected = "1.sql    =    SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: multiple, simple
        $given = "10.sql = SELECT 'Hello World'\n20 {\nsql='Hello world2'\n}\n30 {\nsql='Hello world3'\n}\n";
        $expected = "10.sql = SELECT 'Hello World'\n20.sql='Hello world2'\n30.sql='Hello world3'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: multiple, simple
        $given = "{\nsql = SELECT 'Hello World'\n}\n{\nsql='Hello world2'\n}\n {\nsql='Hello world3'\n}\n";
        $expected = "1.sql = SELECT 'Hello World'\n2.sql='Hello world2'\n3.sql='Hello world3'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: multiple, simple
        $given = "myAlias {\nsql = SELECT 'Hello World'\n}\nmyAlias10{\nsql='Hello world2'\n}\n 10myAlias{\nsql='Hello world3'\n}\n";
        $expected = "1.sql = SELECT 'Hello World'\n2.sql='Hello world2'\n3.sql='Hello world3'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: complex
        $given = "10.sql = SELECT 'Hello World'\n20 {\nsql='Hello world2'\n30 { \n sql=SELECT 'Hello World3'\n40 { \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = "10.sql = SELECT 'Hello World'\n20.sql='Hello world2'\n20.30.sql=SELECT 'Hello World3'\n20.30.40.sql = SELECT 'Hello World4'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: complex
        $given = "{\nsql = SELECT 'Hello World'\n}\n {\nsql='Hello world2'\n { \n sql=SELECT 'Hello World3'\n { \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = "1.sql = SELECT 'Hello World'\n2.sql='Hello world2'\n2.3.sql=SELECT 'Hello World3'\n2.3.4.sql = SELECT 'Hello World4'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: complex
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}\n myAlias2{\nsql='Hello world2'\n myAlias3{ \n sql=SELECT 'Hello World3'\n myAlias4{ \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = "1.sql = SELECT 'Hello World'\n2.sql='Hello world2'\n2.3.sql=SELECT 'Hello World3'\n2.3.4.sql = SELECT 'Hello World4'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // form=...., {{ }}
        $given = "10.sql = SELECT 'Hello World'\nform = {{form:S}}\n20.sql = SELECT 'Hello World2'\n30 {\nsql=SELECT 'Hello World'\n}\n   form=Person\n";
        $expected = "10.sql = SELECT 'Hello World'\nform = {{form:S}}\n20.sql = SELECT 'Hello World2'\n30.sql=SELECT 'Hello World'\nform=Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // form=...., {{ }}
        $given = "{\nsql = SELECT 'Hello World'\n}\nform = {{form:S}}\n{\nsql = SELECT 'Hello World2'\n}\n {\nsql=SELECT 'Hello World'\n}\n   form=Person\n";
        $expected = "1.sql = SELECT 'Hello World'\nform = {{form:S}}\n2.sql = SELECT 'Hello World2'\n3.sql=SELECT 'Hello World'\nform=Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // form=...., {{ }}
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}\nform = {{form:S}}\nmyAlias2 {\nsql = SELECT 'Hello World2'\n}\n myAlias3{\nsql=SELECT 'Hello World'\n}\n   form=Person\n";
        $expected = "1.sql = SELECT 'Hello World'\nform = {{form:S}}\n2.sql = SELECT 'Hello World2'\n3.sql=SELECT 'Hello World'\nform=Person";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested: open bracket alone
        $given = "10.sql = SELECT 'Hello World'\n20\n{\nhead=test\n}\n30.sql = SELECT 'Hello World'\n";
        $expected = "10.sql = SELECT 'Hello World'\n20.head=test\n30.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested: open bracket alone
        $given = "{\nsql = SELECT 'Hello World'\n}\n\n{\nhead=test\n}\n{\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'\n2.head=test\n3.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested: open bracket alone
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}\n\nmySecondAlias{\nhead=test\n}\nmyThirdAlias{\nsql = SELECT 'Hello World'\n}";
        $expected = "1.sql = SELECT 'Hello World'\n2.head=test\n3.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Single open bracket inside a string.
        $given = "10.sql = SELECT 'Hello { World'";
        $expected = "10.sql = SELECT 'Hello { World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Single open bracket inside a string.
        $given = "{\nsql = SELECT 'Hello { World'\n}";
        $expected = "1.sql = SELECT 'Hello { World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Single open bracket inside a string.
        $given = "myAlias{\nsql = SELECT 'Hello { World'\n}";
        $expected = "1.sql = SELECT 'Hello { World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Complex test
        $given = "10.sql = SELECT '[\*]{7} [0-9]{5}<br>'\n20 {\n   10 {\n      5 {\n         sql = SELECT 'hello world<br>'\n      }\n   }\n}\n20.10.5.head = Terific\n20.sql = SELECT 20, '<br>'\n20.10.sql = SELECT  '20.10<br>'";
        $expected = "10.sql = SELECT '[\*]{7} [0-9]{5}<br>'\n20.10.5.sql = SELECT 'hello world<br>'\n20.10.5.head = Terific\n20.sql = SELECT 20, '<br>'\n20.10.sql = SELECT  '20.10<br>'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Complex test
        $given = "{\nsql = SELECT '[\*]{7} [0-9]{5}<br>'\n}\n{\n   {\n       {\n         sql = SELECT 'hello world<br>'\n  head = Terrific   \n}\n   }\n}\n{\nsql = SELECT 5, '<br>'\n}\n{\nsql = SELECT  '6<br>'\n}";
        $expected = "1.sql = SELECT '[\*]{7} [0-9]{5}<br>'\n2.3.4.sql = SELECT 'hello world<br>'\n2.3.4.head = Terrific\n5.sql = SELECT 5, '<br>'\n6.sql = SELECT  '6<br>'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Complex test
        $given = "myAliasOne{\nsql = SELECT '[\*]{7} [0-9]{5}<br>'\n}\nmyAliasTwo{\n   myAliasThree{\n       myAliasFour{\n         sql = SELECT 'hello world<br>'\n  head = Terrific   \n}\n   }\n}\nmyAliasFive  {\nsql = SELECT 5, '<br>'\n}\nmyLastAlias{\nsql = SELECT  '6<br>'\n}";
        $expected = "1.sql = SELECT '[\*]{7} [0-9]{5}<br>'\n2.3.4.sql = SELECT 'hello world<br>'\n2.3.4.head = Terrific\n5.sql = SELECT 5, '<br>'\n6.sql = SELECT  '6<br>'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

    }

    public function testNestingToken() {
        $btp = new BodytextParser();

        // Nested expression: one level curly
        $given = "10 { \n     sql = SELECT 'Hello World' \n , 'next line', \n \n 'end' \n} \n";
        $expected = "10.sql = SELECT 'Hello World' , 'next line', 'end'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level curly
        $given = "{ \n     sql = SELECT 'Hello World' \n , 'next line', \n \n 'end' \n} \n";
        $expected = "1.sql = SELECT 'Hello World' , 'next line', 'end'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level curly
        $given = "myAlias{ \n     sql = SELECT 'Hello World' \n , 'next line', \n \n 'end' \n} \n";
        $expected = "1.sql = SELECT 'Hello World' , 'next line', 'end'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level angle
        $given = "#<\n10 < \n     sql = SELECT 'Hello World'\n>\n";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level angle
        $given = "#<\n < \n     sql = SELECT 'Hello World'\n>\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level angle
        $given = "#<\n myAlias< \n     sql = SELECT 'Hello World'\n>\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level angle and single curly
        $given = "#<\n10 < \n  head = data { \n '1','2','3' \n }\n>\n";
        $expected = "10.head = data { '1','2','3' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level angle and single curly
        $given = "#<\n < \n  head = data { \n '1','2','3' \n }\n>\n";
        $expected = "1.head = data { '1','2','3' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level angle and single curly
        $given = "#<\n myAlias< \n  head = data { \n '1','2','3' \n }\n>\n";
        $expected = "1.head = data { '1','2','3' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level angle and single curly
        $given = "  #   < \n 10 < \n  sql = SELECT 'Hello World' \n>\n";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level angle and single curly
        $given = "  #   < \n  < \n  sql = SELECT 'Hello World' \n>\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level angle and single curly
        $given = "  #   myAlias1< \n  myAlias2< \n  sql = SELECT 'Hello World' \n>\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level round bracket
        $given = "  #   ( \n 10 ( \n  sql = SELECT 'Hello World' \n)\n";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level round bracket
        $given = "  #   ( \n  ( \n  sql = SELECT 'Hello World' \n)\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level round bracket
        $given = "  #   ( \n  myAlias( \n  sql = SELECT 'Hello World' \n)\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level square bracket
        $given = "  #   [ \n 10 [ \n  sql = SELECT 'Hello World' \n]\n";
        $expected = "10.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level square bracket
        $given = "  #   [ \n  [ \n  sql = SELECT 'Hello World' \n]\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level square bracket
        $given = "  #   [ \n  myAlias[ \n  sql = SELECT 'Hello World' \n]\n";
        $expected = "1.sql = SELECT 'Hello World'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: one level angle - garbage
        $given = "  #   < \n 10 { \n  sql = SELECT 'Hello World' \n}\n";
        $expected = "10 {\nsql = SELECT 'Hello World' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: one level angle - garbage
        $given = "  #   < \n  { \n  sql = SELECT 'Hello World' \n}\n";
        $expected = "{\nsql = SELECT 'Hello World' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: one level angle - garbage
        $given = "  #   < \n  myAlias{ \n  sql = SELECT 'Hello World' \n}\n";
        $expected = "myAlias{\nsql = SELECT 'Hello World' }";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Nested expression: '</script>' is allowed here - with bad implemented parsing, it would detect a nesting token end, which is not the meant.
        $given = "  #   < \n 10 < \n  sql = SELECT 'Hello World' \n head = <script> \n>\n20.tail=</script>\n\n\n30.sql=SELECT 'something'";
        $expected = "10.sql = SELECT 'Hello World'\n10.head = <script>\n20.tail=</script>\n30.sql=SELECT 'something'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Nested expression: '</script>' is allowed here - with bad implemented parsing, it would detect a nesting token end, which is not the meant.
        $given = "  #   < \n  < \n  sql = SELECT 'Hello World' \n head = <script> \n>\n<\ntail=</script>\n>\n<\nsql=SELECT 'something'\n>";
        $expected = "1.sql = SELECT 'Hello World'\n1.head = <script>\n2.tail=</script>\n3.sql=SELECT 'something'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Nested expression: '</script>' is allowed here - with bad implemented parsing, it would detect a nesting token end, which is not the meant.
        $given = "  #   < \n  myAlias < \n  sql = SELECT 'Hello World' \n head = <script> \n>\n myAlias2 <\ntail=</script>\n>\nmyAlias3<\nsql=SELECT 'something'\n>";
        $expected = "1.sql = SELECT 'Hello World'\n1.head = <script>\n2.tail=</script>\n3.sql=SELECT 'something'";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $open = '<';
        $close = '>';
        // muliple nesting, unnested rows  inbetween
        $given = <<<EOF
                # $open

                5.head = <h1>

                3.sql = SELECT ...

                10  $open
                  20 $open
                    sql = SELECT 10.20
                    head = <script>
                    tail = </script>
                    30 $open
                        head = <div>
                    $close
                  $close
                  30 $open
                    sql = SELECT 10.30
                  $close
                  sql = SELECT 10
                  50 $open
                    60 $open
                        tail = }
                        }
                        (:
                        }
                        ]
                        sql = SELECT 10.50.60
                        head = {
                          {
                          )
                          {
                          ;
                          [
                    $close
                    sql = SELECT 10.50
                    65 $open
                        sql = SELECT 10.50.65
                    $close
                  $close
                  40 $open
                    head = <table>
                  $close
                $close
                20.sql = SELECT 20
EOF;
        $expected = "5.head = <h1>\n3.sql = SELECT ...\n10.20.sql = SELECT 10.20\n10.20.head = <script>\n10.20.tail = </script>\n10.20.30.head = <div>\n10.30.sql = SELECT 10.30\n10.sql = SELECT 10\n10.50.60.tail = } } (: } ]\n10.50.60.sql = SELECT 10.50.60\n10.50.60.head = { { ) { ; [\n10.50.sql = SELECT 10.50\n10.50.65.sql = SELECT 10.50.65\n10.40.head = <table>\n20.sql = SELECT 20";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // muliple nesting, unnested rows  inbetween
        $given = <<<EOF
                # $open
                
                $open
                    head = <h1>
                $close
                $open
                    sql = SELECT ...
                $close
                $open
                  $open
                    sql = SELECT 3.4
                    head = <script>
                    tail = </script>
                    $open
                      head = <div>
                    $close
                  $close
                  $open
                    sql = SELECT 3.6
                  $close
                  $open
                    sql = SELECT 3.7
                  $close
                  $open
                    $open
                        tail = }
                        }
                        (:
                        }
                        ]
                        sql = SELECT 3.8.9
                        head = {
                          {
                          )
                          {
                          ;
                          [
                    $close
                    $open
                      sql = SELECT 3.8.10
                    $close
                    $open
                        sql = SELECT 3.8.11
                    $close
                  $close
                  $open
                    head = <table>
                  $close
                $close
                $open
                  sql = SELECT 13
                $close
EOF;
        $expected = "1.head = <h1>\n2.sql = SELECT ...\n3.4.sql = SELECT 3.4\n3.4.head = <script>\n3.4.tail = </script>\n3.4.5.head = <div>\n3.6.sql = SELECT 3.6\n3.7.sql = SELECT 3.7\n3.8.9.tail = } } (: } ]\n3.8.9.sql = SELECT 3.8.9\n3.8.9.head = { { ) { ; [\n3.8.10.sql = SELECT 3.8.10\n3.8.11.sql = SELECT 3.8.11\n3.12.head = <table>\n13.sql = SELECT 13";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // muliple nesting, unnested rows  inbetween
        $given = <<<EOF
                # $open
                
                myAlias1 $open
                    head = <h1>
                $close
                myAlias2 $open
                    sql = SELECT ...
                $close
                myAlias3 $open
                  myAlias4 $open
                    sql = SELECT 3.4
                    head = <script>
                    tail = </script>
                    myAlias4 $open
                      head = <div>
                    $close
                  $close
                  myAlias5 $open
                    sql = SELECT 3.6
                  $close
                  myAlias6 $open
                    sql = SELECT 3.7
                  $close
                  myAlias7 $open
                    myAlias8 $open
                        tail = }
                        }
                        (:
                        }
                        ]
                        sql = SELECT 3.8.9
                        head = {
                          {
                          )
                          {
                          ;
                          [
                    $close
                    myAlias9 $open
                      sql = SELECT 3.8.10
                    $close
                    myAlias10$open
                        sql = SELECT 3.8.11
                    $close
                  $close
                  myAlias11$open
                    head = <table>
                  $close
                $close
                myAlias12$open
                  sql = SELECT 13
                $close
EOF;
        $expected = "1.head = <h1>\n2.sql = SELECT ...\n3.4.sql = SELECT 3.4\n3.4.head = <script>\n3.4.tail = </script>\n3.4.5.head = <div>\n3.6.sql = SELECT 3.6\n3.7.sql = SELECT 3.7\n3.8.9.tail = } } (: } ]\n3.8.9.sql = SELECT 3.8.9\n3.8.9.head = { { ) { ; [\n3.8.10.sql = SELECT 3.8.10\n3.8.11.sql = SELECT 3.8.11\n3.12.head = <table>\n13.sql = SELECT 13";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);
    }

    public function testVariousNestingToken() {
        $btp = new BodytextParser();

        $tokenList = '{}[]<>()';
        for ($idx = 0; $idx < 4; $idx++) {
            $open = $tokenList[$idx * 2];
            $close = $tokenList[$idx * 2 + 1];

            // level open
            $given = <<<EOF
                # $open
                10 $open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "10.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // level open
            $given = <<<EOF
                # $open
                 $open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // level open
            $given = <<<EOF
                # $open
                 myAlias $open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // level \n alone
            $given = <<<EOF
                # $open
                10
                $open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "10.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // level \n alone
            $given = <<<EOF
                # $open
                
                $open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // level \n alone
            $given = <<<EOF
                # $open
                
                myAlias$open
                  sql = SELECT 'Hello World'
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // various linebreaks
            $given = <<<EOF
                # $open

                10  $open

                  sql = SELECT 'Hello World'

                $close

EOF;
            $expected = "10.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // various linebreaks
            $given = <<<EOF
                # $open

                  $open

                  sql = SELECT 'Hello World'

                $close

EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // various linebreaks
            $given = <<<EOF
                # $open

                  myAlias $open

                  sql = SELECT 'Hello World'

                $close

EOF;
            $expected = "1.sql = SELECT 'Hello World'";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // multi line
            $given = <<<EOF
                # $open
                10  $open
                  sql = SELECT 'Hello World'
                        FROM Person

                        ORDER BY id

                        LIMIT 4
                   head = <div>
                $close
                10.tail = </div>
EOF;
            $expected = "10.sql = SELECT 'Hello World' FROM Person ORDER BY id LIMIT 4\n10.head = <div>\n10.tail = </div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // multi line
            $given = <<<EOF
                # $open
                  $open
                  sql = SELECT 'Hello World'
                        FROM Person

                        ORDER BY id

                        LIMIT 4
                   head = <div>
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World' FROM Person ORDER BY id LIMIT 4\n1.head = <div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // multi line
            $given = <<<EOF
                # $open
                  myAlias $open
                  sql = SELECT 'Hello World'
                        FROM Person

                        ORDER BY id

                        LIMIT 4
                   head = <div>
                $close
EOF;
            $expected = "1.sql = SELECT 'Hello World' FROM Person ORDER BY id LIMIT 4\n1.head = <div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // mulitple nesting
            $given = <<<EOF
                # $open
                5.head = <h1>
                10  $open
                  sql = SELECT 'Hello World'
                  20 $open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                    30 $open
                        head = <div>
                    $close

                  $close

                $close

EOF;
            $expected = "5.head = <h1>\n10.sql = SELECT 'Hello World'\n10.20.sql = SELECT 'Hi'\n10.20.head = <script>\n10.20.tail = </script>\n10.20.30.head = <div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // mulitple nesting
            $given = <<<EOF
                # $open
                $open
                  head = <h1>
                $close
                  $open
                  sql = SELECT 'Hello World'
                   $open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                     $open
                        head = <div>
                    $close

                  $close

                $close

EOF;
            $expected = "1.head = <h1>\n2.sql = SELECT 'Hello World'\n2.3.sql = SELECT 'Hi'\n2.3.head = <script>\n2.3.tail = </script>\n2.3.4.head = <div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // mulitple nesting
            $given = <<<EOF
                # $open
                myAlias$open
                  head = <h1>
                $close
                  myAlias2$open
                  sql = SELECT 'Hello World'
                   myAlias3$open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                     myAlias4$open
                        head = <div>
                    $close

                  $close

                $close

EOF;
            $expected = "1.head = <h1>\n2.sql = SELECT 'Hello World'\n2.3.sql = SELECT 'Hi'\n2.3.head = <script>\n2.3.tail = </script>\n2.3.4.head = <div>";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // muliple nesting, unnested rows  inbetween
            $given = <<<EOF
                # $open
                5.head = <h1>
                10  $open
                  sql = SELECT 'Hello World'
                  20 $open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                    30 $open
                        head = <div>
                    $close
                  $close
                  30 $open
                    sql = SELECT 'After'
                  $close
                $close
                20.sql = SELECT ...

EOF;
            $expected = "5.head = <h1>\n10.sql = SELECT 'Hello World'\n10.20.sql = SELECT 'Hi'\n10.20.head = <script>\n10.20.tail = </script>\n10.20.30.head = <div>\n10.30.sql = SELECT 'After'\n20.sql = SELECT ...";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias'
            // muliple nesting, unnested rows  inbetween
            $given = <<<EOF
                # $open
                $open
                  head = <h1>
                $close
                $open
                  sql = SELECT 'Hello World'
                  $open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                    $open
                      head = <div>
                    $close
                  $close
                  $open
                    sql = SELECT 'After'
                  $close
                $close
                $open
                  sql = SELECT ...
                $close

EOF;
            $expected = "1.head = <h1>\n2.sql = SELECT 'Hello World'\n2.3.sql = SELECT 'Hi'\n2.3.head = <script>\n2.3.tail = </script>\n2.3.4.head = <div>\n2.5.sql = SELECT 'After'\n6.sql = SELECT ...";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

            // Report notation 'alias' with alias
            // muliple nesting, unnested rows  inbetween
            $given = <<<EOF
                # $open
                myAlias$open
                  head = <h1>
                $close
                mySecondAlias$open
                  sql = SELECT 'Hello World'
                  myThirdAlias$open
                    sql = SELECT 'Hi'
                    head = <script>
                    tail = </script>
                    myFourthAlias$open
                      head = <div>
                    $close
                  $close
                  myFifthAlias$open
                    sql = SELECT 'After'
                  $close
                $close
                mySixthAlias$open
                  sql = SELECT ...
                $close

EOF;
            $expected = "1.head = <h1>\n2.sql = SELECT 'Hello World'\n2.3.sql = SELECT 'Hi'\n2.3.head = <script>\n2.3.tail = </script>\n2.3.4.head = <div>\n2.5.sql = SELECT 'After'\n6.sql = SELECT ...";
            $result = $btp->process($given);
            $this->assertEquals($expected, $result);

        }

    }

    public function testProcessPlainEscape() {
        $btp = new BodytextParser();

        // Simple row, nothing to remove
        $given = "10.sql = SELECT 'Hello World', 'p:id=1&\\\ngrId=2";
        $expected = "10.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "10.sql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  ";
        $expected = "10.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "10.sql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  \\\n 20.sql = SELECT ''";
        $expected = "10.sql = SELECT 'Hello World', 'p:id=1&grId=2\n20.sql = SELECT ''";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple row, nothing to remove
        $given = "{\nsql = SELECT 'Hello World', 'p:id=1&\\\ngrId=2\n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "{\nsql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  \n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "{\nsql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  \\\n }\n{\nsql = SELECT ''\n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2\n2.sql = SELECT ''";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple row, nothing to remove
        $given = "myAlias{\nsql = SELECT 'Hello World', 'p:id=1&\\\ngrId=2\n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "myAlias{\nsql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  \n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);

        $given = "myAlias{\nsql = SELECT 'Hello World', 'p:id=1&  \\\n  grId=2  \\\n }\n{\nsql = SELECT ''\n}";
        $expected = "1.sql = SELECT 'Hello World', 'p:id=1&grId=2\n2.sql = SELECT ''";
        $result = $btp->process($given);
        $this->assertEquals($expected, $result);
    }

    /**
     *
     */
    public function testProcessExceptionClose() {
        $btp = new BodytextParser();

        $this->expectException(\UserFormException::class);

        // Nested: unmatched close bracket
        $btp->process("10.sql = SELECT 'Hello World'\n } \n30.sql = SELECT 'Hello World'\n");

        // Report notation 'alias'
        // Nested: unmatched close bracket
        $btp->process("sql = SELECT 'Hello World'\n } \n{\nsql = SELECT 'Hello World'\n}");

        // Report notation 'alias' with alias
        // Nested: unmatched close bracket
        $btp->process("sql = SELECT 'Hello World'\n } \nmyAlias{\nsql = SELECT 'Hello World'\n}");

    }

    /**
     *
     */
    public function testProcessExceptionOpen() {
        $btp = new BodytextParser();
        $this->expectException(\UserFormException::class);

        // Nested: unmatched open bracket
        $btp->process("10.sql = SELECT 'Hello World'\n20 { \n30.sql = SELECT 'Hello World'\n");

        // Report notation 'alias'
        // Nested: unmatched open bracket
        $btp->process("{\nsql = SELECT 'Hello World'\n}\n { \n30.sql = SELECT 'Hello World'\n");

        // Report notation 'alias' with alias
        // Nested: unmatched open bracket
        $btp->process("myAlias{\nsql = SELECT 'Hello World'\n}\n myAlias2{ \n30.sql = SELECT 'Hello World'\n");

    }

    /**
     *
     */
    public function testReportLines() {
        $btp = new BodytextParser();

        // Simple statement
        $given = "10.sql = SELECT 'Hello World'";
        $expected = [10 => 1];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statement, nested
        $given = "10 {\nsql = SELECT 'Hello World'\n}";
        $expected = [10 => 2];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statement, nested
        $given = "{\nsql = SELECT 'Hello World'\n}";
        $expected = [1 => 2];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statement, nested
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}";
        $expected = [1 => 2];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statement, multiple unnecessary lines
        $given = "\n#some comments\n10.sql = SELECT 'Hello World'\n\n    \n   #more comment";
        $expected = [10 => 3];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statement, multiple unnecessary lines, nested
        $given = "\n#some comments\n10\n{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment";
        $expected = [10 => 5];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statement, multiple unnecessary lines, nested
        $given = "\n#some comments\n\n{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment";
        $expected = [1 => 5];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statement, multiple unnecessary lines, nested
        $given = "\n#some comments\n\nmyAlias{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment";
        $expected = [1 => 5];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // No statement
        $given = "\n#some comments\n\n\n    \n   #more comment";
        $expected = [];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statement on multiple lines
        $given = "\n10.sql = SELECT 'Hello World',\n'more content'\n      WHERE help=1";
        $expected = [10 => 2];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statement on multiple lines, nested
        $given = "\n10 {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}";
        $expected = [10 => 3];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statement on multiple lines, nested
        $given = "\n {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}";
        $expected = [1 => 3];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statement on multiple lines, nested
        $given = "\n myAlias{\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}";
        $expected = [1 => 3];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements
        $given = "10.sql = SELECT 'Hello World'\n20.sql = SELECT 'Hello World'\n30.sql = SELECT 'Hello World'";
        $expected = [10 => 1, 20 => 2, 30 => 3];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements, nested
        $given = "10 {\nsql = SELECT 'Hello World'\n}\n20 {\nsql = SELECT 'Hello World'\n}\n30 {\nsql = SELECT 'Hello World'\n}";
        $expected = [10 => 2, 20 => 5, 30 => 8];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statements, nested
        $given = "{\nsql = SELECT 'Hello World'\n}\n{\nsql = SELECT 'Hello World'\n}\n{\nsql = SELECT 'Hello World'\n}";
        $expected = [1 => 2, 2 => 5, 3 => 8];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statements, nested
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}\nmyAlias2{\nsql = SELECT 'Hello World'\n}\nmyAlias3{\nsql = SELECT 'Hello World'\n}";
        $expected = [1 => 2, 2 => 5, 3 => 8];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements, multiple unnecessary lines
        $given = "\n#some comments\n10.sql = SELECT 'Hello World'\n\n    \n   #more comment \n 20.sql = SELECT 'Hello World'\n #more comment\n\n\n 30.sql=SELECT 'Hello World'";
        $expected = [10 => 3, 20 => 7, 30 => 11];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements, multiple unnecessary lines, nested
        $given = "\n#some comments\n10\n{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment\n\n 20 {\nsql = SELECT 'Hello World'\n\n}\n #more comment \n\n\n 30 {\nsql = SELECT 'Hello World'\n\n}";
        $expected = [10 => 5, 20 => 12, 30 => 19];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statements, multiple unnecessary lines, nested
        $given = "\n#some comments\n\n{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment\n\n  {\nsql = SELECT 'Hello World'\n\n}\n #more comment \n\n\n  {\nsql = SELECT 'Hello World'\n\n}";
        $expected = [1 => 5, 2 => 12, 3 => 19];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statements, multiple unnecessary lines, nested
        $given = "\n#some comments\n\nmyAlias{\nsql = SELECT 'Hello World'\n\n}\n    \n   #more comment\n\n  myAlias2{\nsql = SELECT 'Hello World'\n\n}\n #more comment \n\n\n  myAlias3{\nsql = SELECT 'Hello World'\n\n}";
        $expected = [1 => 5, 2 => 12, 3 => 19];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements on multiple lines
        $given = "\n10.sql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n\n 20.sql = SELECT 'Hello World', \n 'some more content'";
        $expected = [10 => 2, 20 => 6];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Simple statements on multiple lines, nested
        $given = "\n10 {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}\n\n20\n {\nsql = SELECT 'Hello World', \n 'some more content'\n}";
        $expected = [10 => 3, 20 => 10];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Simple statements on multiple lines, nested
        $given = "\n {\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}\n\n{\nsql = SELECT 'Hello World', \n 'some more content'\n}";
        $expected = [1 => 3, 2 => 9];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Simple statements on multiple lines, nested
        $given = "\n myAlias{\nsql = SELECT 'Hello World',\n'more content'\n      WHERE help=1\n}\n\nmyAlias2{\nsql = SELECT 'Hello World', \n 'some more content'\n}";
        $expected = [1 => 3, 2 => 9];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Complex statements
        $given = "10.sql = SELECT 'Hello World'\n20.sql='Hello world2'\n20.30.sql=SELECT 'Hello World3'\n20.30.40.sql = SELECT 'Hello World4'";
        $expected = [10 => 1, 20 => 2, '20.30' => 3, '20.30.40' => 4];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Complex statements, nested
        $given = "10.sql = SELECT 'Hello World'\n20 {\nsql='Hello world2'\n30 { \n sql=SELECT 'Hello World3'\n40 { \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = [10 => 1, 20 => 3, '20.30' => 5, '20.30.40' => 7];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias'
        // Complex statements: complex
        $given = "{\nsql = SELECT 'Hello World'\n}\n {\nsql='Hello world2'\n { \n sql=SELECT 'Hello World3'\n { \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = [1 => 2, 2 => 5, '2.3' => 7, '2.3.4' => 9];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);

        // Report notation 'alias' with alias
        // Complex statements: complex
        $given = "myAlias{\nsql = SELECT 'Hello World'\n}\n myAlias2{\nsql='Hello world2'\n myAlias3{ \n sql=SELECT 'Hello World3'\n myAlias4{ \n sql = SELECT 'Hello World4'\n  }  \n  } \n  }  ";
        $expected = [1 => 2, 2 => 5, '2.3' => 7, '2.3.4' => 9];
        $btp->process($given);
        $result = $btp->reportLines;
        $this->assertEquals($expected, $result);
    }
}