<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Database;


require_once(__DIR__ . '/AbstractDatabaseTest.php');

/**
 * Class DatabaseTest
 */
class DatabaseTest extends AbstractDatabaseTest {

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testFetchAll() {
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 2');

        $this->assertCount(2, $allRows);
        $this->assertEquals(1, $allRows[0]['id']);
        $this->assertEquals(2, $allRows[1]['id']);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testFetchAllEmpty() {
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person WHERE id=0 ORDER BY id');

        $this->assertEquals(array(), $allRows);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQuerySimple() {

        $expected = [
            [
                'id' => '1',
                'name' => 'Doe',
                'firstName' => 'John',
                'adrId' => '0',
                'gender' => 'male',
                'groups' => 'c',
            ],
            [
                'id' => '2',
                'name' => 'Smith',
                'firstName' => 'Jane',
                'adrId' => '0',
                'gender' => 'female',
                'groups' => 'a,c',
            ],
        ];


        // Check read rows
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 1');
        // Check count
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));

        // Check read rows
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person ORDER BY id LIMIT 2');

        // Check count
        $this->assertEquals(2, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Compare rows
        $this->assertEquals($expected, $dataArray);
        // Check rows

        // Same as above, but specify 'ROW_REGULAR'
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person ORDER BY id LIMIT 2', ROW_REGULAR);
        $this->assertEquals(2, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals($expected, $dataArray);

        // ROW_EXPECT_0
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person WHERE id=0', ROW_EXPECT_0);
        $this->assertEquals(0, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals(array(), $dataArray);

        // ROW_EXPECT_1
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person WHERE id=1', ROW_EXPECT_1);
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals($expected[0], $dataArray);

        // ROW_EXPECT_0_1: 1
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person WHERE id=1', ROW_EXPECT_0_1);
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals($expected[0], $dataArray);

        // ROW_EXPECT_0_1: 0
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person WHERE id=0', ROW_EXPECT_0_1);
        $this->assertEquals(0, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals(array(), $dataArray);

        //ROW_EXPECT_GE_1 - one record
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person WHERE id=1', ROW_EXPECT_GE_1);
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals([$expected[0]], $dataArray);

        // ROW_EXPECT_GE_1 - two records
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person ORDER BY id LIMIT 2', ROW_EXPECT_GE_1);
        $this->assertEquals(2, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        // Check rows
        $this->assertEquals($expected, $dataArray);

        // Check Implode: 2 records
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person ORDER BY id LIMIT 2', ROW_IMPLODE_ALL);
        $this->assertEquals(implode($expected[0]) . implode($expected[1]), $data);

        // Check Implode: 1 record
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id,name,firstName,adrId,gender,`groups` FROM Person ORDER BY id LIMIT 1', ROW_IMPLODE_ALL);
        $this->assertEquals(implode($expected[0]), $data);

        // Check Implode 0 record
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person WHERE id=0 ORDER BY id LIMIT 2', ROW_IMPLODE_ALL);
        $this->assertEquals('', $data);

        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT "a", "b", "c"', ROW_IMPLODE_ALL);
        $this->assertEquals('abc', $data);

        // Multiple columns with same columnname
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT "a", "b", "a"', ROW_IMPLODE_ALL);
        $this->assertEquals('aba', $data);

        // Multiple columns with same columnname
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT id, name, firstname, name, id FROM Person', ROW_IMPLODE_ALL);
        $this->assertEquals('1DoeJohnDoe12SmithJaneSmith2', $data);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQuerySimpleParameter() {
        $stat = array();
        $dummy = array();

        // Parameter Susbstitution by '?'
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person WHERE name LIKE ? ORDER BY id', ROW_REGULAR, ['Smith']);
        // Check count
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));

        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('UPDATE Person SET `groups` = ? WHERE 1', ROW_REGULAR, ['a,b,c']);
        $this->assertEquals(2, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));

        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('INSERT INTO Person (`name`, `firstname`, `groups`) VALUES ( ?, ? ,? )', ROW_REGULAR, ['Meier', 'John', 'a'], 'fake', $dummy, $stat);
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        $this->assertEquals(3, $stat[DB_INSERT_ID]);

        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('INSERT INTO Person (`name`, `firstname`, `groups`) VALUES ( ?, ? ,? )', ROW_REGULAR, ['Meier', 'Jan', 'b'], 'fake', $dummy, $stat);
        $this->assertEquals(1, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));
        $this->assertEquals(4, $stat[DB_INSERT_ID]);

        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sql('DELETE FROM Person WHERE name = ?', ROW_REGULAR, ['Meier']);
        $this->assertEquals(2, $this->store->getVar(SYSTEM_SQL_COUNT, STORE_SYSTEM));

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlExceptionExpect0() {

        $this->expectException(\DbException::class);

        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 1', ROW_EXPECT_0);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlExceptionExpect1_0() {

        $this->expectException(\DbException::class);

        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 0', ROW_EXPECT_1);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlExceptionExpect1_2() {
        $this->expectException(\DbException::class);
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id ', ROW_EXPECT_1);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlExceptionExpect01() {
        $this->expectException(\DbException::class);
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id ', ROW_EXPECT_0_1);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlExceptionExpectGE1() {
        $this->expectException(\DbException::class);
        $data = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 0', ROW_EXPECT_GE_1);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSanitizeException() {
        $this->expectException(\DbException::class);
        $this->dbArray[DB_INDEX_DEFAULT]->sql('some garbage');
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testReportException() {
        $this->expectException(\DbException::class);
        $this->dbArray[DB_INDEX_DEFAULT]->sql("INSERT INTO Person (`id`,`name`) VALUES (1,'test')");
    }

    /**
     * Check to skip mysqli errno 1060
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSkipErrno() {
        $dummy = array();
        $result = $this->dbArray[DB_INDEX_DEFAULT]->sql("ALTER TABLE  `Person` ADD  `name` CHAR(16) NOT NULL  AFTER  `id`",
            ROW_REGULAR, array(), '', $dummy, $dummy, [1060]);

        $this->assertEquals('-1', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testGetSetValueList() {
        $valueList = $this->dbArray[DB_INDEX_DEFAULT]->getEnumSetValueList('Person', 'gender');
        $expected = [0 => '', 1 => 'male', 2 => 'female'];
        $this->assertEquals($expected, $valueList);

        $expected = [0 => '', 1 => 'a', 2 => 'b', 3 => 'c'];
        $valueList = $this->dbArray[DB_INDEX_DEFAULT]->getEnumSetValueList('Person', 'groups');
        $this->assertEquals($expected, $valueList);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSelectReturn() {
        $dummy = array();
        $stat = array();

        $sql = "SELECT name FROM Person ORDER BY id";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);
        $this->assertEquals([0 => ['name' => 'Doe'], 1 => ['name' => 'Smith']], $rc);

        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(2, $stat[DB_NUM_ROWS]);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testShowReturn() {
        $dummy = array();
        $stat = array();

        $sql = "SHOW TABLES";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);

        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $all = '';
        foreach ($rc as $val) {
            $all .= '-' . implode('-', $val);
        }

        $this->assertEquals(3, $stat[DB_NUM_ROWS]);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testExplainReturn() {
        $dummy = array();
        $stat = array();

        $sql = "EXPLAIN Person";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);

        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(9, $stat[DB_NUM_ROWS]);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testInsertReturn() {
        $dummy = array();
        $stat = array();

        $sql = "INSERT INTO Person (id, name, firstname, gender, `groups`) VALUES (NULL, 'Doe', 'Jonni', 'male','')";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);
        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(3, $stat[DB_INSERT_ID]);
        $this->assertEquals(1, $stat[DB_AFFECTED_ROWS]);

        $this->assertEquals(3, $rc);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testUpdateReturn() {
        $dummy = array();
        $stat = array();

        $sql = "UPDATE Person SET `groups` = 'a' WHERE 1";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);

        $this->assertEquals(2, $rc);

        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(2, $stat[DB_AFFECTED_ROWS]);
    }

    /**
     * @throws \Exception
     */
    public function testDeleteReturn() {
        $dummy = array();
        $stat = array();

        $this->executeSQLFile(__DIR__ . '/fixtures/Generic.sql', true);

        $sql = "DELETE FROM Person WHERE id=2";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);

        $this->assertEquals(1, $rc);

        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(1, $stat[DB_AFFECTED_ROWS]);

    }


    /**
     * @throws \Exception
     */
    public function testReplaceReturn() {
        $dummy = array();
        $stat = array();

        $this->executeSQLFile(__DIR__ . '/fixtures/Generic.sql', true);

        $sql = "REPLACE INTO Person (id, name, firstname, gender, `groups`) VALUES (1, 'Doe', 'John', 'male','')";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);
        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(1, $stat[DB_INSERT_ID]);
        $this->assertEquals(2, $stat[DB_AFFECTED_ROWS]);

        $this->assertEquals(1, $rc);


        $sql = "REPLACE INTO Person (id, name, firstname, gender, `groups`) VALUES (100, 'Lincoln', 'Abraham', 'male','')";

        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql($sql, ROW_REGULAR, $dummy, 'fake', $dummy, $stat);
        // DB_NUM_ROWS | DB_INSERT_ID | DB_AFFECTED_ROWS
        $this->assertEquals(100, $stat[DB_INSERT_ID]);
        $this->assertEquals(1, $stat[DB_AFFECTED_ROWS]);

        $this->assertEquals(100, $rc);
    }

    /**
     *
     */
    public function testGetTableDefinition() {
        $expected = [
            ['Field' => 'id', 'Type' => 'bigint(20)', 'Null' => 'NO', 'Key' => 'PRI', 'Default' => '', 'Extra' => 'auto_increment'],
            ['Field' => 'name', 'Type' => 'varchar(128)', 'Null' => 'YES', 'Key' => '', 'Default' => '', 'Extra' => ''],
            ['Field' => 'firstName', 'Type' => 'varchar(128)', 'Null' => 'YES', 'Key' => '', 'Default' => '', 'Extra' => ''],
            ['Field' => 'account', 'Type' => 'varchar(128)', 'Null' => 'YES', 'Key' => '', 'Default' => '', 'Extra' => ''],
            ['Field' => 'adrId', 'Type' => 'int(11)', 'Null' => 'NO', 'Key' => '', 'Default' => '0', 'Extra' => ''],
            ['Field' => 'gender', 'Type' => "enum('','male','female')", 'Null' => 'NO', 'Key' => '', 'Default' => 'male', 'Extra' => ''],
            ['Field' => 'groups', 'Type' => "set('','a','b','c')", 'Null' => 'NO', 'Key' => '', 'Default' => '', 'Extra' => ''],
            ['Field' => 'modified', 'Type' => "datetime", 'Null' => 'YES', 'Key' => '', 'Default' => 'current_timestamp()', 'Extra' => 'on update current_timestamp()'],
            ['Field' => 'created', 'Type' => "datetime", 'Null' => 'YES', 'Key' => '', 'Default' => null, 'Extra' => ''],
        ];

        $version = $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT @@version;", ROW_IMPLODE_ALL);
        preg_match('/^\d+(\.\d+)*(\.\d+)/', $version, $matches);
        $numericVersion = $matches[0];
        if (version_compare($numericVersion, '10.3', '<')) {
            # In 10.2,10.2:  CURRENT_TIMESTAMP
            $expected[6] = ['Field' => 'modified', 'Type' => "datetime", 'Null' => 'YES', 'Key' => '', 'Default' => 'CURRENT_TIMESTAMP', 'Extra' => 'on update CURRENT_TIMESTAMP'];
        }

        $this->assertEquals($expected, $this->dbArray[DB_INDEX_DEFAULT]->getTableDefinition('Person'));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testSqlKeys() {
        $keys = array();

        $expected = [
            ['0' => '1', '1' => 'Doe', '2' => '0'],
            ['0' => '2', '1' => 'Smith', '2' => '0'],
        ];
        // Same as above, but specify 'ROW_REGULAR'
        $stat = array();
        $dataArray = $this->dbArray[DB_INDEX_DEFAULT]->sqlKeys('SELECT id AS "id", name, "0" AS "id" FROM Person ORDER BY id LIMIT 3', $keys, $stat);

        // Check rows
        $this->assertEquals($expected, $dataArray);

        // Check keys
        $this->assertEquals(['id', 'name', 'id'], $keys);
    }

    /**
     *
     */
    public function testhasLimit() {

        $this->assertEquals(false, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit(''));
        $this->assertEquals(false, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT'));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1'));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT  1  '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1,1'));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1,1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT  1,1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT  1,1  '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1 ,1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1 , 1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1 ,  1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1  ,  1 '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT 1  ,  1  '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT  1  ,  1  '));
        $this->assertEquals(true, $this->dbArray[DB_INDEX_DEFAULT]->hasLimit('SELECT ... LIMIT   1   ,   1   '));
    }

    /**
     * @return void
     */
    public function testMakeArrayDict() {

        $this->assertEquals(array(), $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict(array(), 'key', 'value'));

        $arr[] = ['red', 'green'];
        $expect[] = ['key' => 'red', 'value' => 'green'];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr[] = ['blue', 'orange'];
        $expect[] = ['key' => 'blue', 'value' => 'orange'];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['key1' => 'red', 'value1' => 'green'], ['key1' => 'blue', 'value1' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['key' => 'red', 'value1' => 'green'], ['key' => 'blue', 'value1' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['key1' => 'red', 'value' => 'green'], ['key1' => 'blue', 'value' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['key' => 'red', 'value' => 'green'], ['key' => 'blue', 'value' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['value' => 'green', 'key' => 'red'], ['value' => 'orange', 'key' => 'blue']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['green'], ['orange']];
        $expect = [['key' => 'green', 'value' => 'green'], ['key' => 'orange', 'value' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['value1' => 'green'], ['value1' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['key' => 'green'], ['key' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

        $arr = [['value' => 'green'], ['value' => 'orange']];
        $this->assertEquals($expect, $this->dbArray[DB_INDEX_DEFAULT]->makeArrayDict($arr, 'key', 'value'));

    }

    /**
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testLeadingBrace() {
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('(SELECT * FROM Person WHERE id=0)');
        $this->assertEquals(array(), $allRows);

        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('(SELECT * FROM Person LIMIT 1)');
        $this->assertEquals(1, count($allRows));

        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('(SELECT * FROM Person LIMIT 1) UNION ALL (SELECT * FROM Person LIMIT 1)');
        $this->assertEquals(2, count($allRows));
    }


    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \Exception
     */
    protected function setUp(): void {
        parent::setUp();

        // remove existing tables
        $allTables = $this->dbArray[DB_INDEX_DEFAULT]->sql("SHOW TABLES", ROW_REGULAR);
        foreach ($allTables as $val) {
            $table = current($val);
            $this->dbArray[DB_INDEX_DEFAULT]->sql("DROP TABLE $table");
        }

        $this->executeSQLFile(__DIR__ . '/fixtures/Generic.sql', true);
    }
}
