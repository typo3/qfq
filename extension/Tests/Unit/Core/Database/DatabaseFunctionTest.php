<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Database;

require_once(__DIR__ . '/AbstractDatabaseTest.php');

/**
 * Class DatabaseTest
 */
class DatabaseFunctionTest extends AbstractDatabaseTest {

    /**
     * @var \IMATHUZH\Qfq\Core\Database\Database
     */
    protected $db = null;

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQMore() {

        $this->assertEquals('', $this->db->sql('SELECT QMORE("",10)', ROW_IMPLODE_ALL));
        $this->assertEquals('', $this->db->sql('SELECT QMORE("",0)', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT QMORE("test",10)', ROW_IMPLODE_ALL));
        $this->assertEquals('te<span class="qfq-more-text">st</span>', $this->db->sql('SELECT QMORE("test",3)', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQBar() {

        $this->assertEquals('', $this->db->sql('SELECT QBAR("")', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT QBAR("test")', ROW_IMPLODE_ALL));
        $this->assertEquals('te\|st', $this->db->sql('SELECT QBAR("te|st")', ROW_IMPLODE_ALL));
        $this->assertEquals('\|', $this->db->sql('SELECT QBAR("|")', ROW_IMPLODE_ALL));
        $this->assertEquals('\|\|\|', $this->db->sql('SELECT QBAR("|||")', ROW_IMPLODE_ALL));
        $this->assertEquals('a\|b\|', $this->db->sql('SELECT QBAR("a|b|")', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQNl2Br() {

        $this->assertEquals('', $this->db->sql('SELECT QNL2BR("")', ROW_IMPLODE_ALL));
        $this->assertEquals('test<br>newline', $this->db->sql('SELECT QNL2BR("test<br>newline")', ROW_IMPLODE_ALL));
        $this->assertEquals("test<br>newline", $this->db->sql('SELECT QNL2BR("test' . PHP_EOL . 'newline")', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQIfEmpty() {

        $this->assertEquals('test', $this->db->sql('SELECT QIFEMPTY("test","-")', ROW_IMPLODE_ALL));
        $this->assertEquals('<', $this->db->sql('SELECT QIFEMPTY("<","-")', ROW_IMPLODE_ALL));

        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY(null,"-")', ROW_IMPLODE_ALL));
        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY("","-")', ROW_IMPLODE_ALL));

        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY("0","-")', ROW_IMPLODE_ALL));
        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY(0,"-")', ROW_IMPLODE_ALL));

        $this->assertEquals('1', $this->db->sql('SELECT QIFEMPTY("1","-")', ROW_IMPLODE_ALL));
        $this->assertEquals('1', $this->db->sql('SELECT QIFEMPTY(1,"-")', ROW_IMPLODE_ALL));

        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY("0000-00-00","-")', ROW_IMPLODE_ALL));
        $this->assertEquals('-', $this->db->sql('SELECT QIFEMPTY("0000-00-00 00:00:00","-")', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testStrip_Tags() {

        $this->assertEquals('', $this->db->sql('SELECT strip_tags("")', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT strip_tags("test")', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT strip_tags("te<a>st")', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT strip_tags("te<a class=\"exp\">st")', ROW_IMPLODE_ALL));
        $this->assertEquals('test', $this->db->sql('SELECT strip_tags("te<a class=\"exp\">s</a>t")', ROW_IMPLODE_ALL));
        $this->assertEquals('world', $this->db->sql('SELECT strip_tags("<a class=\"exp\">world</a>")', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function testQDate_Format() {

//        $this->assertEquals('-', $this->db->sql('SELECT QDATE_FORMAT("")', ROW_IMPLODE_ALL));
        $this->assertEquals('-', $this->db->sql('SELECT QDATE_FORMAT("0000-00-00")', ROW_IMPLODE_ALL));
        $this->assertEquals('-', $this->db->sql('SELECT QDATE_FORMAT("0000-00-00 00:00:00")', ROW_IMPLODE_ALL));
        $this->assertEquals('02.11.2019 00:00', $this->db->sql('SELECT QDATE_FORMAT("2019-11-02")', ROW_IMPLODE_ALL));
        $this->assertEquals('02.11.2019 01:23', $this->db->sql('SELECT QDATE_FORMAT("2019-11-02 01:23")', ROW_IMPLODE_ALL));
        $this->assertEquals('02.11.2019 01:23', $this->db->sql('SELECT QDATE_FORMAT("2019-11-02 01:23:45")', ROW_IMPLODE_ALL));
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \Exception
     */
    protected function setUp(): void {
        parent::setUp();

        $this->db = $this->dbArray[DB_INDEX_DEFAULT];

        $this->executeSQLFile(__DIR__ . '../../../../../Classes/Sql/' . QFQ_FUNCTION_DEFAULT_SQL, true);

    }
}
