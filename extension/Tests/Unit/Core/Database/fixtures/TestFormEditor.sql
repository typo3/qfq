# Before MariaDB 10.2.1, 'text' column could not have a 'default' value.
# To not be forced to specify each text column on INSERT() statements, switch off strict checking:
SET sql_mode = "NO_ENGINE_SUBSTITUTION";

CREATE TABLE IF NOT EXISTS `Form`
(
    `id`                       INT(11)                                                     NOT NULL AUTO_INCREMENT,
    `name`                     VARCHAR(255)                                                NOT NULL DEFAULT '',
    `title`                    VARCHAR(511)                                                NOT NULL DEFAULT '',
    `noteInternal`             TEXT                                                        NOT NULL DEFAULT '',
    `tableName`                VARCHAR(255)                                                NOT NULL DEFAULT '',
    `primaryKey`               VARCHAR(255)                                                NOT NULL DEFAULT '',

    `permitNew`                ENUM ('sip', 'logged_in', 'logged_out', 'always', 'never')  NOT NULL DEFAULT 'sip',
    `permitEdit`               ENUM ('sip', 'logged_in', 'logged_out', 'always', 'never')  NOT NULL DEFAULT 'sip',
    `escapeTypeDefault`        VARCHAR(32)                                                 NOT NULL DEFAULT 'c',
    `render`                   ENUM ('bootstrap', 'table', 'plain')                        NOT NULL DEFAULT 'bootstrap',
    `requiredParameterNew`     VARCHAR(255)                                                NOT NULL DEFAULT '',
    `requiredParameterEdit`    VARCHAR(255)                                                NOT NULL DEFAULT '',
    `dirtyMode`                ENUM ('exclusive', 'advisory', 'none')                      NOT NULL DEFAULT 'exclusive',
    `showButton`               SET ('new', 'delete', 'close', 'save')                      NOT NULL DEFAULT 'new,delete,close,save',
    `multiMode`                ENUM ('none', 'horizontal', 'vertical')                     NOT NULL DEFAULT 'none',
    `multiSql`                 TEXT                                                        NOT NULL DEFAULT '',
    `multiDetailForm`          VARCHAR(255)                                                NOT NULL DEFAULT '',
    `multiDetailFormParameter` VARCHAR(255)                                                NOT NULL DEFAULT '',

    `forwardMode`              ENUM ('client', 'no', 'url', 'url-skip-history', 'url-sip') NOT NULL DEFAULT 'client',
    `forwardPage`              VARCHAR(255)                                                NOT NULL DEFAULT '',

    `bsLabelColumns`           VARCHAR(255)                                                NOT NULL DEFAULT '',
    `bsInputColumns`           VARCHAR(255)                                                NOT NULL DEFAULT '',
    `bsNoteColumns`            VARCHAR(255)                                                NOT NULL DEFAULT '',

    `parameter`                TEXT                                                        NOT NULL DEFAULT '',
    `parameterLanguageA`       TEXT                                                        NOT NULL DEFAULT '',
    `parameterLanguageB`       TEXT                                                        NOT NULL DEFAULT '',
    `parameterLanguageC`       TEXT                                                        NOT NULL DEFAULT '',
    `parameterLanguageD`       TEXT                                                        NOT NULL DEFAULT '',
    `recordLockTimeoutSeconds` INT(11)                                                     NOT NULL DEFAULT 900,

    `deleted`                  ENUM ('yes', 'no')                                          NOT NULL DEFAULT 'no',
    `modified`                 TIMESTAMP                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP
        ON UPDATE CURRENT_TIMESTAMP,
    `created`                  DATETIME                                                    NOT NULL DEFAULT '0000-00-00 00:00:00',

    PRIMARY KEY (`id`),
    KEY `name` (`name`),
    KEY `name_deleted` (`name`, `deleted`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 1000;


CREATE TABLE IF NOT EXISTS `FormElement`
(
    `id`                 INT(11)                                                                    NOT NULL AUTO_INCREMENT,
    `formId`             INT(11)                                                                    NOT NULL DEFAULT '0',
    `feIdContainer`      INT(11)                                                                    NOT NULL DEFAULT '0',
    `dynamicUpdate`      ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'no',

    `enabled`            ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'yes',

    `name`               VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `label`              VARCHAR(511)                                                               NOT NULL DEFAULT '',

    `mode`               ENUM ('show', 'required', 'readonly', 'hidden')                            NOT NULL DEFAULT 'show',
    `modeSql`            TEXT                                                                       NOT NULL DEFAULT '',
    `class`              ENUM ('native', 'action', 'container')                                     NOT NULL DEFAULT 'native',
    `type`               ENUM ('checkbox', 'date', 'datetime', 'dateJQW', 'datetimeJQW', 'extra', 'gridJQW', 'text',
        'editor', 'time', 'note', 'password', 'radio', 'select', 'subrecord', 'upload',
        'annotate', 'imageCut', 'fieldset', 'pill', 'templateGroup',
        'beforeLoad', 'beforeSave', 'beforeInsert', 'beforeUpdate', 'beforeDelete', 'afterLoad',
        'afterSave', 'afterInsert', 'afterUpdate', 'afterDelete', 'sendMail',
        'paste')                                                                                    NOT NULL DEFAULT 'text',
    `subrecordOption`    SET ('edit', 'delete', 'new')                                              NOT NULL DEFAULT '',
    `encode`             ENUM ('none', 'specialchar')                                               NOT NULL DEFAULT 'specialchar',
    `checkType`          ENUM ('auto', 'alnumx', 'digit', 'numerical', 'email', 'pattern', 'allbut',
        'all')                                                                                      NOT NULL DEFAULT 'auto',
    `checkPattern`       VARCHAR(255)                                                               NOT NULL DEFAULT '',

    `onChange`           VARCHAR(255)                                                               NOT NULL DEFAULT '',

    `ord`                INT(11)                                                                    NOT NULL DEFAULT '0',
    `tabindex`           INT(11)                                                                    NOT NULL DEFAULT '0',

    `size`               VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `maxLength`          VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `bsLabelColumns`     VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `bsInputColumns`     VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `bsNoteColumns`      VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `rowLabelInputNote`  SET ('row', 'label', '/label', 'input', '/input', 'note', '/note', '/row') NOT NULL DEFAULT 'row,label,/label,input,/input,note,/note,/row',
    `note`               TEXT                                                                       NOT NULL DEFAULT '',
    `adminNote`          TEXT                                                                       NOT NULL DEFAULT '',
    `tooltip`            VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `placeholder`        VARCHAR(2048)                                                              NOT NULL DEFAULT '',

    `value`              TEXT                                                                       NOT NULL DEFAULT '',
    `sql1`               TEXT                                                                       NOT NULL DEFAULT '',
    `parameter`          TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageA` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageB` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageC` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageD` TEXT                                                                       NOT NULL DEFAULT '',
    `clientJs`           TEXT                                                                       NOT NULL DEFAULT '',

    `deleted`            ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'no',
    `modified`           TIMESTAMP                                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP
        ON UPDATE CURRENT_TIMESTAMP,
    `created`            DATETIME                                                                   NOT NULL DEFAULT '0000-00-00 00:00:00',

    PRIMARY KEY (`id`),
    KEY `formId` (`formId`),
    KEY `formId_class_enabled_deleted` (`formId`, `class`, `enabled`, `deleted`),
    KEY `feIdContainer` (`feIdContainer`),
    KEY `ord` (`ord`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;


CREATE TABLE IF NOT EXISTS `Dirty`
(
    `id`                   INT(11)                                NOT NULL AUTO_INCREMENT,
    `sip`                  VARCHAR(255)                           NOT NULL DEFAULT '',
    `tableName`            VARCHAR(255)                           NOT NULL DEFAULT '',
    `recordId`             INT(11)                                NOT NULL DEFAULT '0',
    `expire`               DATETIME                               NOT NULL DEFAULT '',
    `recordHashMd5`        CHAR(32)                               NOT NULL DEFAULT '',
    `tabUniqId`            CHAR(32)                               NOT NULL DEFAULT '',
    `feUser`               VARCHAR(255)                           NOT NULL DEFAULT '',
    `qfqUserSessionCookie` VARCHAR(255)                           NOT NULL DEFAULT '',
    `dirtyMode`            ENUM ('exclusive', 'advisory', 'none') NOT NULL DEFAULT 'exclusive',
    `remoteAddress`        VARCHAR(45)                            NOT NULL DEFAULT '',
    `modified`             TIMESTAMP                              NOT NULL DEFAULT CURRENT_TIMESTAMP
        ON UPDATE CURRENT_TIMESTAMP,
    `created`              DATETIME                               NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`id`),
    KEY `sip` (`sip`),
    KEY `tableName` (`tableName`),
    KEY `recordId` (`recordId`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;



#
# FormEditor: Form
INSERT INTO Form (name, title, noteInternal, tableName, permitNew, permitEdit, render, multiSql, parameter)
VALUES ('form', 'Form Editor: {{SELECT id, " / ", title FROM Form WHERE id = {{recordId:S0}}}}',
        'Please secure the form',
        'Form', 'always', 'always', 'bootstrap', '', 'maxVisiblePill=3');

# FormEditor: FormElements
INSERT INTO FormElement (formId, name, label, mode, type, class, ord, size, maxLength, note, clientJs, value, sql1,
                         parameter, feIdContainer, modeSql)
VALUES (1, 'basic', 'Basic', 'show', 'pill', 'container', 10, 0, 0, '', '', '', '', '', 0, ''),
       (1, 'permission', 'Permission', 'show', 'pill', 'container', 20, 0, 0, '', '', '', '', '', 0, ''),
       (1, 'various', 'Various', 'show', 'pill', 'container', 30, 0, 0, '', '', '', '', '', 0, ''),
       (1, 'formelement', 'Formelement', 'show', 'pill', 'container', 40, 0, 0, '', '', '', '', '', 0, ''),

       (1, 'id', 'id', 'readonly', 'text', 'native', 100, 10, 11, '', '', '', '', '', 1, ''),
       (1, 'name', 'Name', 'show', 'text', 'native', 120, 40, 255, '', '', '', '', 'autofocus=on', 1, ''),
       (1, 'title', 'Title', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 1, ''),
       (1, 'noteInternal', 'Note', 'show', 'text', 'native', 140, '40,3', 0, '', '', '', '', '', 1, ''),
       (1, 'tableName', 'Table', 'required', 'select', 'native', 150, 0, 0, '', '', '', '{{!SHOW tables}}',
        'emptyItemAtStart', 1, ''),

       (1, 'permitNew', 'Permit New', 'show', 'radio', 'native', 160, 0, 0, '', '', '', '', '', 2, ''),
       (1, 'permitEdit', 'Permit Edit', 'show', 'radio', 'native', 170, 0, 0, '', '', '', '', '', 2, ''),
       (1, 'permitUrlParameter', 'Permit Url Parameter', 'show', 'text', 'native', 180, 40, 255, '', '', '', '', '', 2,
        ''),
       (1, 'render', 'Render', 'show', 'radio', 'native', 190, 0, 0, '', '', '', '', '', 2, ''),

       (1, 'multi', 'Multi', 'show', 'fieldset', 'native', 210, 0, 0, '', '', '', '', '', 3, ''),
       (1, 'multiMode', 'Multi Mode', 'show', 'radio', 'native', 220, 0, 0, '', '', '', '', '', 3, ''),
       (1, 'multiSql', 'Multi SQL', 'show', 'text', 'native', 230, '40,3', 0, '', '', '', '', '', 3, ''),
       (1, 'multiDetailForm', 'Multi Detail Form', 'show', 'text', 'native', 240, 40, 255, '', '', '', '', '', 3, ''),
       (1, 'multiDetailFormParameter', 'Multi Detail Form Parameter', 'show', 'text', 'native', 250, 40, 255, '', '',
        '', '', '', 3, ''),
       (1, 'forwardMode', 'Forward', 'show', 'radio', 'native', 260, 0, 0, '', '', '', '', '', 3, ''),
       (1, 'forwardPage', 'Forward Page', 'show', 'text', 'native', 270, 40, 255, '', '', '', '', '', 3, ''),
       (1, 'bsLabelColumns', 'BS Label Columns', 'show', 'text', 'native', 280, 40, 250, '', '', '', '', '', 3, ''),
       (1, 'bsInputColumns', 'BS Input Columns', 'show', 'text', 'native', 290, 40, 250, '', '', '', '', '', 3, ''),
       (1, 'bsNoteColumns', 'BS Note Columns', 'show', 'text', 'native', 300, 40, 250, '', '', '', '', '', 3, ''),

       (1, 'deleted', 'Deleted', 'show', 'checkbox', 'native', 400, 0, 0, '', '', '', '', '', 3, ''),
       (1, 'modified', 'Modified', 'readonly', 'text', 'native', 410, 40, 20, '', '', '', '', '', 3, ''),
       (1, 'created', 'Created', 'readonly', 'text', 'native', 420, 40, 20, '', '', '', '', '', 3, ''),

       (1, '', 'FormElements', 'show', 'subrecord', 'native', 500, 0, 0, '', '', '',
        '{{!SELECT * FROM FormElement WHERE formId={{id:R0}}}}', 'form=formElement\npage=form.php', 4, '');


#
# FormEditor: FormElement
INSERT INTO Form (name, title, noteInternal, tableName, permitNew, permitEdit, render, multiSql, parameter)
VALUES ('formElement', 'Form Element Editor : {{SELECT id, " / ", title FROM Form WHERE id = {{recordId:S0}}}}',
        'Please secure the form',
        'FormElement', 'always', 'always', 'bootstrap', '', 'maxVisiblePill=3');

# FormEditor: FormElements
INSERT INTO FormElement (formId, name, label, mode, type, class, ord, size, maxLength, note, clientJs, value, sql1,
                         parameter, feIdContainer, modeSql)
VALUES (2, 'basic', 'Basic', 'show', 'pill', 'container', 10, 0, 0, '', '', '', '', '', 0, ''),
       (2, 'check_order', 'Check & Order', 'show', 'pill', 'container', 20, 0, 0, '', '', '', '', '', 0, ''),
       (2, 'layout', 'Layout', 'show', 'pill', 'container', 20, 0, 0, '', '', '', '', '', 0, ''),
       (2, 'value', 'Value', 'show', 'pill', 'container', 20, 0, 0, '', '', '', '', '', 0, ''),
       (2, 'info', 'Info', 'show', 'pill', 'container', 20, 0, 0, '', '', '', '', '', 0, ''),

       (2, 'id', 'id', 'readonly', 'text', 'native', 100, 10, 11, '', '', '', '', '', 100, ''),
       (2, 'formId', 'formId', 'readonly', 'text', 'native', 120, 40, 255, '', '', '', '', '', 100, ''),
       (2, 'feIdContainer', 'Container', 'show', 'select', 'native', 150, 0, 0, '', '', '',
        '{{!SELECT fe.id, CONCAT(fe.class, " / ", fe.label) FROM FormElement As fe WHERE fe.formId={{id}} AND fe.class="container" ORDER BY fe.ord }}'
           , 'emptyItemAtStart', 100, ''),
       (2, 'enabled', 'Enabled', 'show', 'checkbox', 'native', 120, 0, 0, '', '', '', '', '', 100, ''),
       (2, 'name', 'Name', 'show', 'text', 'native', 120, 40, 255, '', '', '', '', '', 100, ''),
       (2, 'label', 'Label', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 100, ''),
       (2, 'mode', 'Mode', 'show', 'select', 'native', 120, 0, 255, '', '', '', '', '', 100, ''),
       (2, 'class', 'Class', 'show', 'select', 'native', 120, 0, 255, '', '', '', '', '', 100, ''),
       (2, 'type', 'Type', 'show', 'select', 'native', 120, 0, 255, '', '', '', '', '', 100, ''),
       (2, 'checkType', 'Check Type', 'show', 'select', 'native', 120, 0, 255, '', '', '', '', '', 101, ''),
       (2, 'checkPattern', 'Check Pattern', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 101, ''),
       (2, 'onChange', 'JS onChange', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 101, ''),
       (2, 'ord', 'Order', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 101, ''),
       (2, 'tabindex', 'tabindex', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 101, ''),
       (2, 'size', 'Size', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'maxlenght', 'Maxlength', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'note', 'note', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'tooltip', 'Tooltip', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'placeholder', 'Placeholder', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'value', 'value', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 102, ''),
       (2, 'sql1', 'sql1', 'show', 'text', 'native', 130, '40,4', 255, '', '', '', '', '', 103, ''),
       (2, 'parameter', 'Parameter', 'show', 'text', 'native', 130, '40,4', 255, '', '', '', '', '', 103, ''),
       (2, 'clientJs', 'ClientJS', 'show', 'text', 'native', 130, 40, 255, '', '', '', '', '', 103, ''),
       (2, 'debug', 'Debug', 'show', 'checkbox', 'native', 130, 0, 0, '', '', '', '', '', 104, ''),
       (2, 'deleted', 'Deleted', 'show', 'checkbox', 'native', 400, 0, 0, '', '', '', '', '', 104, ''),
       (2, 'modified', 'Modified', 'readonly', 'text', 'native', 410, 40, 20, '', '', '', '', '', 104, ''),
       (2, 'created', 'Created', 'readonly', 'text', 'native', 420, 40, 20, '', '', '', '', '', 104, '');


# FormEditor: Small
INSERT INTO Form (id, name, title, noteInternal, tableName, permitNew, permitEdit, render, multiSql, parameter)
VALUES (3, 'phpunit_person',
        'Person {{SELECT ": ", firstName, " ", name, " (", id, ")" FROM Person WHERE id = {{recordId:S0}}}}',
        'Please secure the form',
        'Person', 'always', 'always', 'bootstrap', '', '');

# FormEditor: FormElements
INSERT INTO FormElement (formId, name, label, mode, type, class, ord, size, maxLength, note, clientJs, value, sql1,
                         parameter, feIdContainer, modeSql)
VALUES (3, 'name', 'Name', 'show', 'text', 'native', 10, 50, 255, '', '', '', '', '', 0, ''),
       (3, 'firstName', 'Firstname', 'show', 'text', 'native', 10, 50, 255, '', '', '', '', '', 0, '');
