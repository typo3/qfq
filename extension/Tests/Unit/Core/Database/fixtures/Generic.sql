/* Errors in SQL statement (after the first one) are not reported!!! */

DROP TABLE IF EXISTS Person;
CREATE TABLE Person
(
    id        BIGINT AUTO_INCREMENT PRIMARY KEY,
    name      VARCHAR(128)                NULL     DEFAULT '',
    firstName VARCHAR(128)                NULL     DEFAULT '',
    account   VARCHAR(128)                NULL     DEFAULT '',
    adrId     INT(11)                     NOT NULL DEFAULT 0,
    gender    ENUM ('', 'male', 'female') NOT NULL DEFAULT 'male',
    groups    SET ('', 'a', 'b', 'c')     NOT NULL DEFAULT '',
    modified  DATETIME                    NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created   DATETIME                    NULL     DEFAULT NULL
);

INSERT INTO Person (name, firstName, gender, groups, account)
VALUES ('Doe', 'John', 'male', 'c', 'jdoe'),
       ('Smith', 'Jane', 'female', 'a,c', 'jsmi');

DROP TABLE IF EXISTS Note;
CREATE TABLE Note
(
    id       BIGINT AUTO_INCREMENT PRIMARY KEY,
    grId     INT(11)      NOT NULL DEFAULT 0,
    personId BIGINT(20)            DEFAULT NULL,
    value    VARCHAR(128) NOT NULL DEFAULT '',
    t1       TEXT         NOT NULL DEFAULT '',
    t2       TEXT         NOT NULL DEFAULT ''
);

INSERT INTO Note (grId, value, t1, t2)
VALUES (1, 'First note - constants', 'Latest Information', 'Dear User\nt\n you\'re invited to to our Christmas party'),
       (2, 'Second note - some variables', 'Latest Information: {{party:C:all}}',
        'Dear {{Firstname:C:all}} {{Lastname:C:all}}\n\n you\'re invited to to our Christmas party');


DROP TABLE IF EXISTS Address;
CREATE TABLE Address
(
    id       BIGINT(20)                                               NOT NULL AUTO_INCREMENT,
    personId BIGINT(20)                                               NOT NULL DEFAULT 0,
    street   VARCHAR(128)                                             NOT NULL DEFAULT '',
    city     VARCHAR(128)                                             NOT NULL DEFAULT '',
    country  ENUM ('', 'Switzerland', 'Austria', 'France', 'Germany') NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
);

