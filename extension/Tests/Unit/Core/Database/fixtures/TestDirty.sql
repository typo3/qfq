DELETE
FROM Dirty;

DELETE
FROM Form
WHERE name LIKE 'lock%';

# FormEditor: Small
INSERT INTO Form (name, title, noteInternal, tableName, permitNew, permitEdit, render, multiSql, parameter, dirtyMode)
VALUES ('lockNone', 'Text None', 'note internal', 'Person', 'always', 'always', 'bootstrap', '', '', 'none'),
       ('lockAdvisory', 'Text Advisory', 'note internal', 'Person', 'always', 'always', 'bootstrap', '', '',
        'advisory'),
       ('lockExclusive', 'Text Exclusive', 'note internal', 'Person', 'always', 'always', 'bootstrap', '', '',
        'exclusive'),
       ('lockAdvisory2', 'Text Advisory', 'note internal', 'Person', 'always', 'always', 'bootstrap', '', '',
        'advisory'),
       ('lockExclusive2', 'Text Exclusive', 'note internal', 'Person', 'always', 'always', 'bootstrap', '', '',
        'exclusive');

