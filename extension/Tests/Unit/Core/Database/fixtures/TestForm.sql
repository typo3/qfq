# Form: Small
INSERT INTO Form (id, name, title, noteInternal, tableName, permitNew, permitEdit, render, multiSql, parameter)
VALUES (3, 'phpunit_person',
        'Person {{SELECT ": ", firstName, " ", name, " (", id, ")" FROM Person WHERE id = {{recordId:S0}}}}',
        'Please secure the form',
        'Person', 'always', 'always', 'bootstrap', '', '');

# FormEditor: FormElements
INSERT INTO FormElement (id, formId, name, label, mode, type, class, ord, size, maxLength, note, clientJs, value, sql1,
                         parameter, feIdContainer, modeSql)
VALUES (200, 3, 'name', 'Name', 'show', 'text', 'native', 10, 50, 255, '', '', '', '', '', 0, ''),
       (201, 3, 'firstName', 'Firstname', 'show', 'text', 'native', 10, 50, 255, '', '', '', '', '', 0, '');
