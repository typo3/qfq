<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Database;

use IMATHUZH\Qfq\Core\Database\DatabaseUpdate;
use IMATHUZH\Qfq\Core\Store\Store;


require_once(__DIR__ . '/AbstractDatabaseTest.php');

/**
 * Class DatabaseTest
 */
class DatabaseUpdateTest extends AbstractDatabaseTest {

//    /**
//     */
//    public function testFetchAll() {
//        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT * FROM Person ORDER BY id LIMIT 2');
//
//        $this->assertCount(2, $allRows);
//        $this->assertEquals(1, $allRows[0]['id']);
//        $this->assertEquals(2, $allRows[1]['id']);
//    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckNupdate() {

        // By default at least 5 tables are QFQ core functionality
        $countQfqTables = 4;

        $store = Store::getInstance();

        $dbUpdate = new DatabaseUpdate($this->dbArray[DB_INDEX_DEFAULT], $store);

        $dbUpdate->checkNupdate(SYSTEM_DB_UPDATE_NEVER);
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SHOW tables;');
        $this->assertCount(0, $allRows);

        $dbUpdate->checkNupdate(SYSTEM_DB_UPDATE_AUTO);
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SHOW tables;');
        $this->assertGreaterThan($countQfqTables, count($allRows));

        // After deleting FormElement, but keeping Form, the update should not played with 'AUTO'
        $this->dbArray[DB_INDEX_DEFAULT]->sql('DROP TABLE FormElement;');
        $dbUpdate->checkNupdate(SYSTEM_DB_UPDATE_AUTO);
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SHOW tables;');
        $this->assertGreaterThan($countQfqTables - 1, count($allRows));

        // Now we should have all tables again
        $dbUpdate->checkNupdate(SYSTEM_DB_UPDATE_ALWAYS);
        $allRows = $this->dbArray[DB_INDEX_DEFAULT]->sql('SHOW tables;');
        $this->assertGreaterThan($countQfqTables, count($allRows));

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {
        parent::setUp();

        // remove existing tables
        $allTables = $this->dbArray[DB_INDEX_DEFAULT]->sql("SHOW TABLES", ROW_REGULAR);
        foreach ($allTables as $val) {
            $table = current($val);
            $this->dbArray[DB_INDEX_DEFAULT]->sql("DROP TABLE $table");
        }

//        $this->executeSQLFile(__DIR__ . '/fixtures/Generic.sql', true);
    }
}
