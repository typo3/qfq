<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Database;

use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractDatabaseTest
 */
//abstract class AbstractDatabaseTest extends PHPUnit_Framework_TestCase {
abstract class AbstractDatabaseTest extends TestCase {

    /**
     * @var \mysqli
     */
    static protected $mysqli = null;

    /**
     * @var Database[]
     */
    protected $dbArray = array();

    /**
     * @var Store
     */
    protected $store = null;

    /**
     * @var Sip
     */
    protected $sip = null;

    /**
     * @param $filename
     * @param $ignoreError
     *
     * @throws \Exception
     */
    public static function executeSQLFile($filename, $ignoreError, $mysqli = null): void {

        if ($mysqli === null) {
            $mysqli = AbstractDatabaseTest::$mysqli;
        }
        $sqlStatements = file_get_contents($filename);
        if (!$mysqli->multi_query($sqlStatements) && !$ignoreError) {
            throw new \Exception("mysqli->multi_query() failed: " . $mysqli->error);
        }

        // discard all results
        do {
            if ($res = $mysqli->store_result()) {
                $res->free();
            }
        } while ($mysqli->more_results() && $mysqli->next_result());
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \Exception
     */
    protected function setUp(): void {

        // Init the store also reads db credential configuration
        $this->store = Store::getInstance('', true);

        $this->sip = new Sip(true);
        $this->sip->sipUniqId('badcaffee1234');

        $dbName = $this->store->getVar('DB_1_NAME', STORE_SYSTEM);
        $dbserver = $this->store->getVar('DB_1_SERVER', STORE_SYSTEM);

        if ($dbName == '') {
            throw new \CodeException('Missing DB_1_NAME in ' . CONFIG_QFQ_PHP, ERROR_MISSING_REQUIRED_PARAMETER);
        } else {
            if (strpos($dbName, '_phpunit') === false) {
                $dbName .= '_phpunit';
                $this->store->setVar('DB_1_NAME', $dbName, STORE_SYSTEM);
//                $dbserver .= '_phpunit';
//                $this->store->setVar('DB_1_SERVER', $dbserver, STORE_SYSTEM);
            }
        }

        $dbIndexData = $this->store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM);
        $dbIndexQfq = $this->store->getVar(SYSTEM_DB_INDEX_QFQ, STORE_SYSTEM);
        if ($dbIndexData != $dbIndexQfq) {
            throw new \CodeException('phpUnit Tests are not prepared for MultiDB tests');
        }

        /// Establish additional mysqli access
        $dbserver = $this->store->getVar('DB_1_SERVER', STORE_SYSTEM);
        $dbuser = $this->store->getVar('DB_1_USER', STORE_SYSTEM);
        $db = $this->store->getVar('DB_1_NAME', STORE_SYSTEM);
        $dbpw = $this->store->getVar('DB_1_PASSWORD', STORE_SYSTEM);

        if (self::$mysqli === null) {

            self::$mysqli = new \mysqli($dbserver, $dbuser, $dbpw, '');

            if (self::$mysqli->connect_errno) {
                throw new \Exception("Unable to connect to mysql server");
            }
        }

        self::$mysqli->query('CREATE DATABASE IF NOT EXISTS ' . $db . ';');
        self::$mysqli->query('USE ' . $db . ';');

        if (empty($this->dbArray)) {
            $this->dbArray[DB_INDEX_DEFAULT] = new Database();
        }
    }
}
