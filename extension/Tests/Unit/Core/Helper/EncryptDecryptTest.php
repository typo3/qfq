<?php
/**
 * Created by PhpStorm.
 * User: Enis Nuredini
 * Date: 05/25/22
 * Time: 9:00 AM
 */

namespace IMATHUZH\QfqTests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Helper\EncryptDecrypt;
use PHPUnit\Framework\TestCase;


/**
 * Class EncryptDecryptText
 * @package qfq
 */
class EncryptDecryptTest extends TestCase {

    /**
     * @var string
     */
    private static $encryptionKey = 'testKey';


    /**
     * Test encryption process
     *
     * Check for correctly used methods
     *
     */
    public function testEncryption() {
        $encryptionMethodAes128 = ENCRYPTION_METHOD_AES_128;
        $encryptionMethodAes256 = ENCRYPTION_METHOD_AES_256;
        $invalidEncryptionMethod = 'AES-300';
        $opensslCbcNameAes128 = 'aes-128-cbc';
        $opensslCbcNameAes256 = 'aes-256-cbc';
        $plaintext = 'Hello World';

        // Use invalid encryption method
        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, self::$encryptionKey, $invalidEncryptionMethod);
        $this->assertEquals(false, $encryptedValue);

        // Use given method AES 128 and 256 and check if used correctly
        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, self::$encryptionKey, $encryptionMethodAes128);
        $methodFromValue = EncryptDecrypt::getDecryptParameter($encryptedValue, ENCRYPTION_CIPHER_METHOD);
        $this->assertEquals($opensslCbcNameAes128, $methodFromValue);

        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, self::$encryptionKey, $encryptionMethodAes256);
        $methodFromValue = EncryptDecrypt::getDecryptParameter($encryptedValue, ENCRYPTION_CIPHER_METHOD);
        $this->assertEquals($opensslCbcNameAes256, $methodFromValue);

        // returned string should have four parts
        $allParameter = EncryptDecrypt::getDecryptParameter($encryptedValue);
        $this->assertCount(4, $allParameter);
    }

    /**
     * Test getting correct parameter from built encryption string
     *
     */
    public function testDecryptionParameter() {
        $encryptionKey = 'testKey';
        $encryptionTag = ENCRYPTION_KEYWORD_ENCRYPTED_DATA;
        $encryptionMethod = ENCRYPTION_METHOD_AES_128;
        $opensslMethodName = 'aes-128-cbc';
        $plaintext = 'Hello World';
        $lowestIvNumber = 1000000000000000;
        $highestIvNumber = 9000000000000000;

        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, $encryptionKey, $encryptionMethod);

        $encryptionTagFromValue = EncryptDecrypt::getDecryptParameter($encryptedValue, ENCRYPTION_KEYWORD_ENCRYPTED_DATA);
        $this->assertEquals($encryptionTag, $encryptionTagFromValue);

        // Check function for returning right string
        $encryptionMethodFromValue = EncryptDecrypt::getDecryptParameter($encryptedValue, ENCRYPTION_CIPHER_METHOD);
        $this->assertEquals($opensslMethodName, $encryptionMethodFromValue);

        // Check for correct used and stored iv
        $encryptionIvFromValue = EncryptDecrypt::getDecryptParameter($encryptedValue, ENCRYPTION_IV);
        $this->assertTrue($encryptionIvFromValue >= $lowestIvNumber && $encryptionIvFromValue <= $highestIvNumber, 'Iv was not correctly given.');

        // Use function with no encrypted data
        $parameter = EncryptDecrypt::getDecryptParameter($plaintext);
        $this->assertEquals(false, $parameter);

    }

    /**
     * Test decryption process
     *
     * Using correct and false key
     * Processing not encrypted data correctly
     */
    public function testDecryption() {
        $encryptionMethod = ENCRYPTION_METHOD_AES_128;
        $plaintext = 'Hello World';
        $falseEncryptionKey = 'failKey';

        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, self::$encryptionKey, $encryptionMethod);
        $decryptedValue = EncryptDecrypt::getPlaintext($encryptedValue, self::$encryptionKey);
        $this->assertEquals($plaintext, $decryptedValue);

        // Using false key to decrypt
        $decryptedValue = EncryptDecrypt::getPlaintext($encryptedValue, $falseEncryptionKey);
        $this->assertNotEquals($plaintext, $decryptedValue);

        // Situation when no encrypted value is used in method getPlaintext
        $valueFromNoEncryptedData = EncryptDecrypt::getPlaintext($plaintext, self::$encryptionKey);
        $this->assertEquals($plaintext, $valueFromNoEncryptedData);
    }

    /**
     * Test input check of valid encryption method
     *
     */
    public function testValidEncryptionMethod() {
        $encryptionMethodAes128 = ENCRYPTION_METHOD_AES_128;
        $encryptionMethodAes256 = ENCRYPTION_METHOD_AES_256;
        $invalidMethod = 'AES-300';

        $isValid = EncryptDecrypt::checkForValidEncryptMethod($encryptionMethodAes128);
        $this->assertEquals(true, $isValid);

        $isValid = EncryptDecrypt::checkForValidEncryptMethod($encryptionMethodAes256);
        $this->assertEquals(true, $isValid);

        $isValid = EncryptDecrypt::checkForValidEncryptMethod($invalidMethod);
        $this->assertEquals(false, $isValid);
    }

    /**
     * Test checking values if are encrypted or not
     *
     */
    public function testCheckForEncryptedValue() {
        $plaintext = 'Hello World';
        $encryptionMethodAes256 = ENCRYPTION_METHOD_AES_256;

        $encryptedValue = EncryptDecrypt::buildDatabaseValue($plaintext, self::$encryptionKey, $encryptionMethodAes256);
        $isEncrypted = EncryptDecrypt::checkForEncryptedValue($encryptedValue);
        $this->assertEquals(true, $isEncrypted);

        $isEncrypted = EncryptDecrypt::checkForEncryptedValue($plaintext);
        $this->assertEquals(false, $isEncrypted);
    }

    /** Test method hasEnoughSpace for correctly work
     *
     * @throws \UserFormException
     */
    public function testEnoughSpaceInDbField() {
        $plaintext1 = 'Hello World';
        $plaintext2 = 'Test123Test123Test123Test123Test123Test123Test123';
        $plaintext3 = 'Test123Test123Test123Test123Test123Test123Test123Test123Test123Test123Test123Test123Test123';
        $encryptionMethodAes128 = ENCRYPTION_METHOD_AES_128;
        $encryptedValue1 = EncryptDecrypt::buildDatabaseValue($plaintext1, self::$encryptionKey, $encryptionMethodAes128);
        $encryptedValue2 = EncryptDecrypt::buildDatabaseValue($plaintext2, self::$encryptionKey, $encryptionMethodAes128);
        $encryptedValue3 = EncryptDecrypt::buildDatabaseValue($plaintext3, self::$encryptionKey, $encryptionMethodAes128);

        $elementsToEncrypt = array(
            array(
                "name" => "secretField1",
                "encryptionMethod" => "AES-128",
                "encryptedValue" => $encryptedValue1
            ),
            array(
                "name" => "secretField2",
                "encryptionMethod" => "AES-128",
                "encryptedValue" => $encryptedValue2
            ),
            array(
                "name" => "secretField3",
                "encryptionMethod" => "AES-128",
                "encryptedValue" => $encryptedValue3
            )
        );

        $dbFieldAttributes = array(
            "id" => array(
                "name" => "id",
                "type" => "varchar",
                "length" => "11"
            ),
            "secretField1" => array(
                "name" => "secretField1",
                "type" => "varchar",
                "length" => "100"
            ),
            "secretField2" => array(
                "name" => "secretField2",
                "type" => "varchar",
                "length" => "200"
            ),
            "secretField3" => array(
                "name" => "secretField3",
                "type" => "varchar",
                "length" => "255"
            ),
        );

        $hasEnoughSpace = EncryptDecrypt::hasEnoughSpace($elementsToEncrypt, $dbFieldAttributes);
        $this->assertEquals(true, $hasEnoughSpace);
    }


    /**
     * Test for fetching attributes name, type and length of all fields from table clipboard
     *
     */
    public function testGetDbFieldAttributes() {
        $dbObject = new Database();

        // Fetch from existing form in php_unit DB with table name clipboard
//       $formId = 1000;
        $formName = 'Dirty';
        $expectedAttributes = 3;
        $expectedColumns = 13;

        // Check if the attributes of first column from table clipboard are fetched correctly
        $expectedName = 'id';
        $expectedType = 'int';
        $expectedLength = '11';

        $attributes = EncryptDecrypt::getDbFieldAttributes($formName, $dbObject);
        $this->assertCount($expectedColumns, $attributes);
        $this->assertCount($expectedAttributes, $attributes['id']);

        $actualName = $attributes['id'][DB_COLUMN_NAME];
        $actualType = $attributes['id'][DB_COLUMN_TYPE];
        $actualLength = $attributes['id'][DB_COLUMN_LENGTH];
        $this->assertEquals($expectedName, $actualName);
        $this->assertEquals($expectedType, $actualType);
        $this->assertEquals($expectedLength, $actualLength);
    }

}

