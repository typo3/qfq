<?php

/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/26/16
 * Time: 2:56 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;


use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use PHPUnit\Framework\TestCase;

/**
 * Class HelperFormElementTest
 * @package qfq
 */
class HelperFormElementTest extends TestCase {

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExplodeParameter() {
        $a = array();

        HelperFormElement::explodeParameter($a, FE_PARAMETER);
        $this->assertEquals(array(), $a, "Both arrays should be equal and empty");

        $a = ['id' => 100, 'parameter' => "work=12\nlist=1:new,2:edit,3:delete"];
//        $b = ['id' => 100, 'parameter' => "work=12\nlist=1:new,2:edit,3:delete", 'work' => 12, 'list' => '1:new,2:edit,3:delete'];
        $b = ['id' => 100, 'parameter' => "", 'work' => 12, 'list' => '1:new,2:edit,3:delete'];

        HelperFormElement::explodeParameter($a, FE_PARAMETER);
        $this->assertEquals($b, $a, "Both arrays should be equal");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExplodeParameterInArrayElements() {
        $a = array();
        HelperFormElement::explodeParameterInArrayElements($a, FE_PARAMETER);
        $this->assertEquals(array(), $a, "Both arrays should be equal and empty");

        $a = [0 => ['id' => 100, 'parameter' => "work=12\nlist=1:new,2:edit,3:delete"]];
//        $b = [0 => ['id' => 100, 'parameter' => "work=12\nlist=1:new,2:edit,3:delete", 'work' => 12, 'list' => '1:new,2:edit,3:delete']];
        $b = [0 => ['id' => 100, 'parameter' => "", 'work' => 12, 'list' => '1:new,2:edit,3:delete']];

        HelperFormElement::explodeParameterInArrayElements($a, FE_PARAMETER);
        $this->assertEquals($b, $a, "Both arrays should be equal");
    }


    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExplodeFieldParameterException() {

        $this->expectException(\UserFormException::class);

        $a = [0 => ['id' => 111, 'name' => 'hello', 'collision' => 'dummy', 'parameter' => "work=12\ncollision=1:new,2:edit,3:delete"]];
        HelperFormElement::explodeParameterInArrayElements($a, FE_PARAMETER);

    }

    /**
     *
     */
    public function testDuplicateRetypeElements() {

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note']];
        $b = HelperFormElement::duplicateRetypeElements($a);
        $this->assertEquals($a, $b, "Both arrays should be equal");

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note', FE_RETYPE => '']];
        $b = HelperFormElement::duplicateRetypeElements($a);
        $this->assertEquals(count($a) + 1, count($b), "Both arrays should be same count");
        $this->assertEquals('1', $b[0][FE_RETYPE], "FE_RETYPE should be set to '1'");

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note', FE_RETYPE => '0']];
        $b = HelperFormElement::duplicateRetypeElements($a);
        $this->assertEquals($a, $b, "Both arrays should be same count");
        $this->assertEquals('0', $b[0][FE_RETYPE], "FE_RETYPE should be unchanged");

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note', FE_RETYPE => '1']];
        $b = HelperFormElement::duplicateRetypeElements($a);
        $this->assertEquals(count($a) + 1, count($b), "Both arrays should be same count");
        $this->assertEquals('1', $b[0][FE_RETYPE], "FE_RETYPE should be unchanged");
        $this->assertEquals($b[0][FE_NAME] . FE_RETYPE_NAME_EXTENSION, $b[1][FE_NAME], "New name should be extended by: " . FE_RETYPE_NAME_EXTENSION);
        $this->assertEquals($b[0][FE_LABEL], $b[1][FE_LABEL], "Both text should be equal");
        $this->assertEquals($b[0][FE_NOTE], $b[1][FE_NOTE], "Both text should be equal");

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note', FE_RETYPE => '1', FE_RETYPE_LABEL => '', FE_RETYPE_NOTE => '']];
        $b = HelperFormElement::duplicateRetypeElements($a);
        $this->assertEquals('', $b[1][FE_LABEL], "String should be empty");
        $this->assertEquals('', $b[1][FE_NOTE], "String should be empty");

        $a = [0 => [FE_NAME => 'hello', FE_LABEL => 'my label', FE_NOTE => 'my note', FE_RETYPE => '1', FE_RETYPE_LABEL => 'new label', FE_RETYPE_NOTE => 'new note']];
        $b = HelperFormElement::duplicateRetypeElements($a);

        $this->assertEquals('my label', $b[0][FE_LABEL], "String should be equal");
        $this->assertEquals('my note', $b[0][FE_NOTE], "String should be equal");
        $this->assertEquals('new label', $b[1][FE_LABEL], "String should be equal");
        $this->assertEquals('new note', $b[1][FE_NOTE], "String should be equal");

    }

    /**
     *
     */
    public function testInitActionFormElement() {

        $list = [FE_TYPE, FE_SLAVE_ID, FE_SQL_VALIDATE, FE_SQL_BEFORE, FE_SQL_INSERT, FE_SQL_UPDATE, FE_SQL_DELETE,
            FE_SQL_AFTER, FE_EXPECT_RECORDS, FE_REQUIRED_LIST, FE_ALERT, FE_QFQ_LOG, FE_MESSAGE_FAIL, FE_SENDMAIL_TO, FE_SENDMAIL_CC,
            FE_SENDMAIL_BCC, FE_SENDMAIL_FROM, FE_SENDMAIL_SUBJECT, FE_SENDMAIL_REPLY_TO, FE_SENDMAIL_FLAG_AUTO_SUBMIT,
            FE_SENDMAIL_GR_ID, FE_SENDMAIL_X_ID, FE_SENDMAIL_X_ID2, FE_SENDMAIL_X_ID3, FE_SENDMAIL_BODY_MODE,
            FE_SENDMAIL_BODY_HTML_ENTITY, FE_SENDMAIL_SUBJECT_HTML_ENTITY];

        $expect = array();
        foreach ($list as $key) {
            $expect[$key] = '';
        }

        // Complete empty
        $result = HelperFormElement::initActionFormElement(array());
        $this->assertEquals($expect, $result, "Both arrays should be equal");

        // Two known elements
        $fe = array();
        $fe[FE_TYPE] = FE_TYPE_AFTER_DELETE;
        $fe[FE_SENDMAIL_TO] = 'info@example.com';
        $expect[FE_TYPE] = $fe[FE_TYPE];
        $expect[FE_SENDMAIL_TO] = $fe[FE_SENDMAIL_TO];
        $result = HelperFormElement::initActionFormElement($fe);
        $this->assertEquals($expect, $result, "Both arrays should be equal");

        $fe['unknown'] = 'unknown';
        $expect['unknown'] = 'unknown';
        $result = HelperFormElement::initActionFormElement($fe);
        $this->assertEquals($expect, $result, "Both arrays should be equal");
    }

    /**
     *
     */
    public function testInitUploadFormElement() {

        $list = [FE_SQL_BEFORE, FE_SQL_INSERT, FE_SQL_UPDATE, FE_SQL_DELETE, FE_SQL_AFTER];

        $expect = array();
        foreach ($list as $key) {
            $expect[$key] = '';
        }

        // Complete empty
        $result = HelperFormElement::initUploadFormElement(array());
        $this->assertEquals($expect, $result, "Both arrays should be equal");

        // Two known elements
        $fe = array();
        $fe[FE_SQL_BEFORE] = 'SELECT willi';
        $fe[FE_SQL_AFTER] = 'UPDATE jane';
        $expect[FE_SQL_BEFORE] = $fe[FE_SQL_BEFORE];
        $expect[FE_SQL_AFTER] = $fe[FE_SQL_AFTER];
        $result = HelperFormElement::initUploadFormElement($fe);
        $this->assertEquals($expect, $result, "Both arrays should be equal");

        $fe['unknown'] = 'unknown';
        $expect['unknown'] = 'unknown';
        $result = HelperFormElement::initUploadFormElement($fe);
        $this->assertEquals($expect, $result, "Both arrays should be equal");
    }

    public function testTgGetMaxLength() {
        $result = HelperFormElement::tgGetMaxLength('');
        $this->assertEquals(FE_TEMPLATE_GROUP_DEFAULT_MAX_LENGTH, $result, "Expect Default");

        $result = HelperFormElement::tgGetMaxLength(0);
        $this->assertEquals(FE_TEMPLATE_GROUP_DEFAULT_MAX_LENGTH, $result, "Expect Default");

        $result = HelperFormElement::tgGetMaxLength('0');
        $this->assertEquals(FE_TEMPLATE_GROUP_DEFAULT_MAX_LENGTH, $result, "Expect Default");

        $result = HelperFormElement::tgGetMaxLength('1');
        $this->assertEquals('1', $result, "Expect Default");

        $result = HelperFormElement::tgGetMaxLength('10');
        $this->assertEquals('10', $result, "Expect Default");

        $result = HelperFormElement::tgGetMaxLength(10);
        $this->assertEquals('10', $result, "Expect Default");

    }

    /**
     * @throws \UserFormException
     */
    public function testPenColorToHex() {
        $result = HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => '']);
        $this->assertEquals('', $result, "Expect empty string");

        $result = HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => '000000']);
        $this->assertEquals('{"red":0,"green":0,"blue":0}', $result, "Expect black");

        $result = HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => 'ffffff']);
        $this->assertEquals('{"red":255,"green":255,"blue":255}', $result, "Expect white");

        $result = HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => '123456']);
        $this->assertEquals('{"red":18,"green":52,"blue":86}', $result, "Expect white");
    }

    //Todo Tests wieder aktivieren
//    /**
//     * @throws \CodeException
//     * @throws \UserFormException
//     * @throws \UserReportException
//     */
//    public function testGetKeyValueListFromSqlEnumSpec() {
//        $form = array();
//        $formElement = array();
//
//        $this->templateFormNFormElement($form, $formElement);
//        $formElement['name'] = 'deleted';
//
//        $build = new BuildFormPlain($form, array(), [$formElement], $this->dbArray);
//
//        $keys = array();
//        $values = array();
//
//        // Spec Enum
//        $expect = ['yes', 'no'];
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//
//        // Spec Enum + emptyItemAtStart
//        $expect = ['', 'yes', 'no'];
//        $formElement[FE_EMPTY_ITEM_AT_START] = '';
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//
//        // Spec Enum + emptyItemAtEnd
//        $expect = ['', 'yes', 'no', ''];
//        $formElement[FE_EMPTY_ITEM_AT_END] = '';
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//
//        // clean
//        unset($formElement[FE_EMPTY_ITEM_AT_START]);
//        unset($formElement[FE_EMPTY_ITEM_AT_END]);
//
//        // listItem: only value
//        $expect = ['a', 'b', 'c'];
//        $formElement['itemList'] = 'a,b,c';
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//
//        // listItem: key/value
//        $expectKeys = ['A', 'B', 'C'];
//        $expectValues = ['a', 'b', 'c'];
//        $formElement['itemList'] = 'A:a,B:b,C:c';
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expectKeys, $keys);
//        $this->assertEquals($expectValues, $values);
//
//        // listItem: key/value + emptyItemAtEnd
//        $formElement['emptyItemAtEnd'] = '';
//        $expectKeys = ['A', 'B', 'C', ''];
//        $expectValues = ['a', 'b', 'c', ''];
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expectKeys, $keys);
//        $this->assertEquals($expectValues, $values);
//        unset($formElement['emptyItemAtEnd']);
//
//        // SQL: any content is fine, but should always be the same to do checks
//        $formElement['sql1'] = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT name FROM Form AS f ORDER BY f.id LIMIT 3');
////        $expect = ['form', 'formElement', 'phpunit_person'];
//        $expect = ['form', 'formElement', 'copyForm'];
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//
//        // SQL (one column, no keyword) + emptyItemAtStart
//        $formElement[FE_EMPTY_ITEM_AT_START] = '';
//        $formElement['sql1'] = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT name FROM Form AS f ORDER BY f.id LIMIT 3');
////        $expect = ['', 'form', 'formElement', 'phpunit_person'];
//        $expect = ['', 'form', 'formElement', 'copyForm'];
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expect, $keys);
//        $this->assertEquals($expect, $values);
//        unset($formElement[FE_EMPTY_ITEM_AT_START]);
//
//        // SQL (4 columns, none 'id' nor 'label') - Take the first two columns
//        $expectKeys = ['100', '200', '300'];
//        $expectValues = ['basic', 'formelement', 'layout'];
//        $formElement['sql1'] = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT ord, name, created, modified  FROM FormElement AS fe ORDER BY fe.id LIMIT 3');
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expectKeys, $keys);
//        $this->assertEquals($expectValues, $values);
//
//        // SQL (4 columns, none 'id', one 'label' ) - Take the first and the fourth two columns
//        $expectKeys = ['100', '200', '300'];
//        $expectValues = ['basic', 'formelement', 'layout'];
//        $formElement['sql1'] = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT ord, created, modified, name AS label FROM FormElement AS fe ORDER BY fe.id LIMIT 3');
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expectKeys, $keys);
//        $this->assertEquals($expectValues, $values);
//
//        // SQL (4 columns, none 'id', one 'label' ) - Take the first and the fourth two columns
//        $expectKeys = ['1', '2', '3'];
//        $expectValues = ['basic', 'formelement', 'layout'];
//        $formElement['sql1'] = $this->dbArray[DB_INDEX_DEFAULT]->sql('SELECT ord, created, modified, name AS label, id FROM FormElement AS fe ORDER BY fe.id LIMIT 3');
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//        $this->assertEquals($expectKeys, $keys);
//        $this->assertEquals($expectValues, $values);
//    }
//
//    /**
//     *
//     * @throws \CodeException
//     * @throws \UserFormException
//     * @throws \UserReportException
//     */
//    public function testGetKeyValueListFromSqlEnumSpecException() {
//      $this->expectException(\UserFormException::class);
//        $form = array();
//        $formElement = array();
//
//        $this->templateFormNFormElement($form, $formElement);
//
//        $build = new BuildFormPlain($form, array(), [$formElement], $this->dbArray);
//
//        $formElement['name'] = 'noteInternal';
//        $keys = array();
//        $values = array();
//        $build->getKeyValueListFromSqlEnumSpec($formElement, $keys, $values);
//    }


    /**
     */
    public function testtestPenColorToHexException1() {

        $this->expectException(\UserFormException::class);

        // too short
        HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => '12345']);

    }

    /**
     */
    public function testtestPenColorToHexException2() {

        $this->expectException(\UserFormException::class);

        // too long
        HelperFormElement::penColorToHex([FE_DEFAULT_PEN_COLOR => '1234567']);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testBuildLabel() {

        $result = HelperFormElement::buildLabel('myLabel:123', "Hello World");
        $this->assertEquals('<label for="myLabel:123" class="control-label" >Hello World</label>', $result);
    }


//    public function testExplodeTemplateGroupElements() {
//        $formElements = array();
//
//        $result = HelperFormElement::explodeTemplateGroupElements(array());
//        $this->assertEquals(array(), $result, "Both arrays should be equal");
//
//        $formElements = [[FE_NAME => 'nameA', FE_LABEL => 'labelA', FE_VALUE => '1'], [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $result = HelperFormElement::explodeTemplateGroupElements($formElements);
//        $this->assertEquals($formElements, $result, "Both arrays should be equal");
//
//        $formElements = [[FE_NAME => 'nameA', FE_LABEL => 'label' . FE_TEMPLATE_GROUP_NAME_PATTERN . 'A', FE_VALUE => '1'], [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $result = HelperFormElement::explodeTemplateGroupElements($formElements);
//        $this->assertEquals($formElements, $result, "Both arrays should be equal");
//
//        $formElements = [[FE_NAME => 'nameA', FE_LABEL => 'label' . FE_TEMPLATE_GROUP_NAME_PATTERN . 'A', FE_VALUE => '1', NAME_TG_COPIES => 2], [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $result = HelperFormElement::explodeTemplateGroupElements($formElements);
//        $formElements = [[FE_NAME => 'nameA', FE_LABEL => 'label1A', FE_VALUE => '1'],
//            [FE_NAME => 'nameA', FE_LABEL => 'label2A', FE_VALUE => '1'],
//            [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $this->assertEquals($formElements, $result, "Both arrays should be equal");
//
//        $formElements = [[FE_NAME => FE_TEMPLATE_GROUP_NAME_PATTERN . 'nameA', FE_LABEL => 'label' . FE_TEMPLATE_GROUP_NAME_PATTERN . 'A', FE_VALUE => '1', NAME_TG_COPIES => 2], [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $result = HelperFormElement::explodeTemplateGroupElements($formElements);
//        $formElements = [[FE_NAME => '1nameA', FE_LABEL => 'label1A', FE_VALUE => '1'],
//            [FE_NAME => '2nameA', FE_LABEL => 'label2A', FE_VALUE => '1'],
//            [FE_NAME => 'nameB', FE_LABEL => 'labelB', FE_VALUE => '11']];
//        $this->assertEquals($formElements, $result, "Both arrays should be equal");
//    }

    /**
     * @throws \UserFormException
     * @throws \CodeException
     * @throws \UserReportException
     */
    public function testValidateParameterPassingCases() {

        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL_BEFORE . '={{TEST}}' . "\n" . // RULES ['{{']
                FE_ITEM_LIST . '=item:1' . "\n" . // RULES ['']
                FE_FILL_STORE_VAR . '={{!TEST}}' // RULES ['{{!']
        ];
        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);

        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL1 . '={{! TEST  {{VAR}} }} ' . "\n" . // RULES ['{{!', '']
                FE_TYPEAHEAD_SQL . '=TEST' // RULES ['{{', '']
        ];

        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);


        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL1 . "={{! Test \n {{VAR}} \n }}" . "\n" . // RULES ['{{!']
                FE_WRAP_NOTE . "=TEST {{VAR}} \n " // RULES ['']
        ];

        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);

        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL1 . '=' . "\n" . // RULES ['{{!']
                FE_WRAP_NOTE . "={{VAR}} {{VAR}}" // RULES ['']
        ];

        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);

        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL1 . "={{! \n    Multiline Content \n    }}", // Valid as per '{{!'
        ];
        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);

        $element = [
            FE_ID => 1,
            FE_PARAMETER =>
                FE_SQL1 . '={{! Complex Content {{Nested}} Extra }}' . "\n" . // Valid as per '{{!'
                FE_TYPEAHEAD_SQL . "={{Outer}}" . "\n" . // Valid as per '{{'
                FE_WRAP_ROW . "=plainValue", // Valid as per ''
        ];
        HelperFormElement::validateFormRules($element);
        $this->assertTrue(true);

    }

    /**
     * @throws \CodeException
     * @throws \UserReportException
     */
    public function testValidateFormRulesInvalidCases() {
        // Case: Partial Wrapping
        try {
            $element = [
                FE_ID => 1,
                FE_PARAMETER =>
                    FE_SQL1 . '={{! TEST {{VAR}} }} {{EXTRA}} ' . "\n" . // Invalid: content outside `{{! ... }}`
                    FE_WRAP_NOTE . "=plainValue" // Valid as per ''
            ];
            HelperFormElement::validateFormRules($element);
            $this->fail('Expected UserFormException was not thrown for partial wrapping.');
        } catch (\UserFormException $e) {
            $this->assertStringContainsString('is not wrapped correctly', $e->getMessage());
        }

        // Case: Unwrapped Value for `{{`
        try {
            $element = [
                FE_ID => 1,
                FE_PARAMETER =>
                    FE_SQL1 . '=plainValue', // Invalid: should be wrapped as `{{ ... }}`
            ];
            HelperFormElement::validateFormRules($element);
            $this->fail('Expected UserFormException was not thrown for unwrapped value.');
        } catch (\UserFormException $e) {
            $this->assertStringContainsString('is not wrapped correctly', $e->getMessage());
        }

        // Case: Missing Closing Braces
        try {
            $element = [
                FE_ID => 1,
                FE_PARAMETER =>
                    FE_SQL1 . '={{! TEST {{VAR}}', // Invalid: missing closing `}}`
            ];
            HelperFormElement::validateFormRules($element);
            $this->fail('Expected UserFormException was not thrown for missing closing braces.');
        } catch (\UserFormException $e) {
            $this->assertStringContainsString('Missing close delimiter', $e->getMessage());
        }

        // Case: Extra Content After Wrapper
        try {
            $element = [
                FE_ID => 1,
                FE_PARAMETER =>
                    FE_SQL1 . '={{! TEST }} ExtraContent', // Invalid: extra content outside wrapper
            ];
            HelperFormElement::validateFormRules($element);
            $this->fail('Expected UserFormException was not thrown for extra content after wrapper.');
        } catch (\UserFormException $e) {
            $this->assertStringContainsString('is not wrapped correctly', $e->getMessage());
        }

        // Case: Completely Missing Wrapping
        try {
            $element = [
                FE_ID => 1,
                FE_PARAMETER =>
                    FE_SQL1 . '=TEST', // Invalid: does not meet the wrapping requirement
            ];
            HelperFormElement::validateFormRules($element);
            $this->fail('Expected UserFormException was not thrown for completely missing wrapping.');
        } catch (\UserFormException $e) {
            $this->assertStringContainsString('is not wrapped correctly', $e->getMessage());
        }

    }
}
