<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;


use IMATHUZH\Qfq\Core\Helper\KeyValueStringParser;
use PHPUnit\Framework\TestCase;

/**
 * Class KeyValueStringParserTest
 * @package qfq
 */
class KeyValueStringParserTest extends TestCase {


    public function testSingleKeyValuePair() {
        $actual = KeyValueStringParser::parse("key:value");

        $this->assertCount(1, $actual);
        $this->assertArrayHasKey('key', $actual);
        $this->assertEquals('value', $actual['key']);
    }

    public function testKeyWithoutValue() {
        $actual = KeyValueStringParser::parse("keywithoutvalue");
        $this->assertCount(1, $actual);
        $this->assertArrayHasKey('keywithoutvalue', $actual);
        $this->assertSame("", $actual['keywithoutvalue']);
    }

    public function testGOODNAMEHERE() {
        $actual = KeyValueStringParser::parse(",,");

        $this->assertCount(0, $actual);
    }

    /**
     */
    public function testNoKey() {
        $this->expectException(\UserFormException::class);
        KeyValueStringParser::parse(":value,key:value");
    }

    public function testNoValue() {
        $actual = KeyValueStringParser::parse("key1:,key2:value2");
        $this->assertCount(2, $actual);
        $this->assertArrayHasKey('key1', $actual);
        $this->assertArrayHasKey('key2', $actual);
        $this->assertEquals('', $actual['key1']);
        $this->assertEquals('value2', $actual['key2']);
    }

    public function testEmptyKeyValuePairString() {
        $actual = KeyValueStringParser::parse("");

        $this->assertCount(0, $actual);
    }

    public function testMultipleKeyValuePairs() {
        $actual = KeyValueStringParser::parse("key1:value1,key2:value2");

        $this->assertCount(2, $actual);
        $this->assertArrayHasKey('key1', $actual);
        $this->assertArrayHasKey('key2', $actual);
        $this->assertEquals('value1', $actual['key1']);
        $this->assertEquals('value2', $actual['key2']);
    }

    public function testKeyValueSeparatorInValue() {
        $actual = KeyValueStringParser::parse("key1:val:ue1,key2:value2");

        $this->assertCount(2, $actual);
        $this->assertArrayHasKey('key1', $actual);
        $this->assertArrayHasKey('key2', $actual);
        $this->assertEquals('val:ue1', $actual['key1']);
        $this->assertEquals('value2', $actual['key2']);
    }

    public function testWhiteSpaceHandling() {
        $actual = KeyValueStringParser::parse(" key1 : val:ue1 , key2 : value2 ");

        $this->assertCount(2, $actual);
        $this->assertArrayHasKey('key1', $actual);
        $this->assertArrayHasKey('key2', $actual);
        $this->assertEquals('val:ue1', $actual['key1']);
        $this->assertEquals('value2', $actual['key2']);
    }

    public function testSourroundingQuotes() {
        $actual = KeyValueStringParser::parse("key1:\" val:ue1 \", key2:' value2 ', key3:\"value3', key4:''");

        $expected = [
            'key1' => ' val:ue1 ',
            'key2' => ' value2 ',
            'key3' => "\"value3'",
            'key4' => '',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testComments() {
        $actual = KeyValueStringParser::parse(" key1 : val:ue1 , # key2 : value2 , ; : broken key value in comment, key3 : valid ");

        $expected = [
            'key1' => 'val:ue1',
            'key3' => "valid",
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testUnparse() {
        $array = KeyValueStringParser::parse("key1:\" val:ue1 \", key2:' value2 ', key3:\"value3'");
        $actual = KeyValueStringParser::unparse($array);
        $expected = "key1:\" val:ue1 \",key2:\" value2 \",key3:\"value3'";

        $this->assertSame($expected, $actual);

    }

    public function testKeyValuePairCR() {

        $actual = KeyValueStringParser::parse("key1=value1\nkey2=value2", "=", "\n");

        $this->assertCount(2, $actual);
        $this->assertArrayHasKey('key1', $actual);
        $this->assertArrayHasKey('key2', $actual);
        $this->assertEquals('value1', $actual['key1']);
        $this->assertEquals('value2', $actual['key2']);
    }

    public function testParseKeyValueSingle() {
        $actual = KeyValueStringParser::parse('value1,value2', ':', ',', KVP_IF_VALUE_EMPTY_COPY_KEY);
        $expected = [
            'value1' => 'value1',
            'value2' => 'value2',
        ];
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);

        $actual = KeyValueStringParser::parse('key1:value1,key2:value2', ':', ',', KVP_IF_VALUE_EMPTY_COPY_KEY);
        $expected = [
            'key1' => 'value1',
            'key2' => 'value2',
        ];
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);

        $actual = KeyValueStringParser::parse('key1:value1,key2:value2', ':', ',', KVP_VALUE_GIVEN);
        $expected = [
            'key1' => 'value1',
            'key2' => 'value2',
        ];
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);

        $actual = KeyValueStringParser::parse('value1,value2', ':', ',', KVP_VALUE_GIVEN);
        $expected = [
            'value1' => '',
            'value2' => '',
        ];
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);
    }

    public function testParseDefaults() {
        $actual = KeyValueStringParser::parse("key1=value1,key2=value2");
        $expected = KeyValueStringParser::parse("key1=value1,key2=value2", ":", ",");
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);

        $actual = KeyValueStringParser::parse("key1:value1,key2:value2");
        $expected = KeyValueStringParser::parse("key1:value1,key2:value2", ":", ",");
        $this->assertEquals($expected, $actual);
        $this->assertCount(2, $actual);
    }

    public function testParseEscapeMax() {
        $actual = KeyValueStringParser::parse("a:hello A,b:hello B,c:hello C");
        $this->assertEquals(['a' => 'hello A', 'b' => 'hello B', 'c' => 'hello C'], $actual);

        $actual = KeyValueStringParser::parse("a\:new:hello A,b:hello B,c:hello C");
        $this->assertEquals(['a:new' => 'hello A', 'b' => 'hello B', 'c' => 'hello C'], $actual);

        $actual = KeyValueStringParser::parse("a\:new:hello A:A:A,b:hello B,c:hello C");
        $this->assertEquals(['a:new' => 'hello A:A:A', 'b' => 'hello B', 'c' => 'hello C'], $actual);

        // Escape char will be removed
        $actual = KeyValueStringParser::parse("a\:new:hello A\:A\:A,b:hello B,c:hello C");
        $this->assertEquals(['a:new' => 'hello A:A:A', 'b' => 'hello B', 'c' => 'hello C'], $actual);

        // Escape list delimiter
        $actual = KeyValueStringParser::parse("a\,x:A\,A,b\,x:B\,B");
        $this->assertEquals(['a,x' => 'A,A', 'b,x' => 'B,B'], $actual);

        // Escape list delimiter & key/value delimiter
        $actual = KeyValueStringParser::parse("a\,\:x:A\,\:A,b\,\:x:B\,\:B");
        $this->assertEquals(['a,:x' => 'A,:A', 'b,:x' => 'B,:B'], $actual);

        // Escape char in value is untouched
        $actual = KeyValueStringParser::parse("a\,x\,y:h\,e\,l\,l\,o A,b:hello B,c:C,d:D");
        $this->assertEquals(['a,x,y' => 'h,e,l,l,o A', 'b' => 'hello B', 'c' => 'C', 'd' => 'D'], $actual);

        // Escape char in value is untouched
        $actual = KeyValueStringParser::parse("a\,x\,y:h\,e\,l\,l\,o A,b:hello B,c\,x\,y:h\,e\,l\,lo C");
        $this->assertEquals(['a,x,y' => 'h,e,l,l,o A', 'b' => 'hello B', 'c,x,y' => 'h,e,l,lo C'], $actual);
    }

    public function testExplodeContent() {
        $actual = KeyValueStringParser::explodeWrapped('', '');
        $this->assertEquals(false, $actual);

        $actual = KeyValueStringParser::explodeWrapped(':', '');
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeWrapped(':', 'a,b,c');
        $this->assertEquals(['a,b,c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b,c');
        $this->assertEquals(['a', 'b', 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', ' a,b,c');
        $this->assertEquals([' a', 'b', 'c'], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', 'a ,b,c');
        $this->assertEquals(['a ', 'b', 'c'], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', ' a ,b,c');
        $this->assertEquals([' a ', 'b', 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a, b,c');
        $this->assertEquals(['a', ' b', 'c'], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b ,c');
        $this->assertEquals(['a', 'b ', 'c'], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', 'a, b ,c');
        $this->assertEquals(['a', ' b ', 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b, c');
        $this->assertEquals(['a', 'b', ' c'], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b,c ');
        $this->assertEquals(['a', 'b', 'c '], $actual);
        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b, c ');
        $this->assertEquals(['a', 'b', ' c '], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a,"b",c');
        $this->assertEquals(['a', '"b"', 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a,"b,b",c');
        $this->assertEquals(['a', '"b,b"', 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,'b',c");
        $this->assertEquals(['a', "'b'", 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,'b,b',c");
        $this->assertEquals(['a', "'b,b'", 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,'b,b,b',c");
        $this->assertEquals(['a', "'b,b,b'", 'c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "'a,a,a,a','b','c,c,c,c,c'");
        $this->assertEquals(["'a,a,a,a'", "'b'", "'c,c,c,c,c'"], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', " 'a,a,a' , 'b' , 'c,c' ");
        $this->assertEquals([" 'a,a,a' ", " 'b' ", " 'c,c' "], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', 'a,b,c', 2);
        $this->assertEquals(['a', 'b,c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "'a,a',b,c", 2);
        $this->assertEquals(["'a,a'", 'b,c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,'b,b',c", 2);
        $this->assertEquals(['a', "'b,b',c"], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,b,'c,c'", 2);
        $this->assertEquals(['a', "b,'c,c'"], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,b,c", 0);
        $this->assertEquals(['a,b,c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,b,c", 1);
        $this->assertEquals(['a,b,c'], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "'a,b',c", 1);
        $this->assertEquals(["'a,b',c"], $actual);

        $actual = KeyValueStringParser::explodeWrapped(',', "a,'b,c'", 1);
        $this->assertEquals(["a,'b,c'"], $actual);

    }

    public function testExplodeEscapeComma() {

        $actual = KeyValueStringParser::explodeEscape(',', '');
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a');
        $this->assertEquals(['a'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c,d');
        $this->assertEquals(['a', 'b', 'c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', ',');
        $this->assertEquals(['', ''], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b');
        $this->assertEquals(['a', 'b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b\,c,d');
        $this->assertEquals(['a', 'b,c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a\,b,c,d');
        $this->assertEquals(['a,b', 'c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c\,d');
        $this->assertEquals(['a', 'b', 'c,d'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', '\,a,b');
        $this->assertEquals([',a', 'b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b\,');
        $this->assertEquals(['a', 'b,'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,\,b');
        $this->assertEquals(['a', ',b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'pre,a-b:$!@#$%^&*()_-+={}[]|":;/?.><,post');
        $this->assertEquals(['pre', 'a-b:$!@#$%^&*()_-+={}[]|":;/?.><', 'post'], $actual);

    }

    public function testExplodeEscapeColon() {

        $actual = KeyValueStringParser::explodeEscape(':', '');
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a');
        $this->assertEquals(['a'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:b:c:d');
        $this->assertEquals(['a', 'b', 'c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', ':');
        $this->assertEquals(['', ''], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:b');
        $this->assertEquals(['a', 'b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:b\:c:d');
        $this->assertEquals(['a', 'b:c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a\:b:c:d');
        $this->assertEquals(['a:b', 'c', 'd'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:b:c\:d');
        $this->assertEquals(['a', 'b', 'c:d'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', '\:a:b');
        $this->assertEquals([':a', 'b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:b\:');
        $this->assertEquals(['a', 'b:'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'a:\:b');
        $this->assertEquals(['a', ':b'], $actual);

        $actual = KeyValueStringParser::explodeEscape(':', 'pre:a-b$!@#$%^&*()_-+={}[]|";/?,><:post');
        $this->assertEquals(['pre', 'a-b$!@#$%^&*()_-+={}[]|";/?,><', 'post'], $actual);

    }

    public function testExplodeEscapeMax() {

        $actual = KeyValueStringParser::explodeEscape(',', '', 0);
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', '', 1);
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', '', 2);
        $this->assertEquals([''], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c', 4);
        $this->assertEquals(['a', 'b', 'c'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c', 3);
        $this->assertEquals(['a', 'b', 'c'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c', 2);
        $this->assertEquals(['a', 'b,c'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a,b,c', 1);
        $this->assertEquals(['a,b,c'], $actual);

        $actual = KeyValueStringParser::explodeEscape(',', 'a\,b,c', 2);
        $this->assertEquals(['a,b', 'c'], $actual);

    }

}
