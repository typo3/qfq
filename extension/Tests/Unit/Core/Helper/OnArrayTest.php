<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/12/16
 * Time: 9:28 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\OnArray;
use PHPUnit\Framework\TestCase;

/**
 * Class OnArrayTest
 * @package qfq
 */
class OnArrayTest extends TestCase {

    public function testSortByKey() {
        $unsorted = [
            'a' => 'z',
            '1' => 'y',
            'Aa' => 'y0',
            'zZ' => 'X9',
            '0000' => 'h',
        ];

        $sorted = [
            '0000' => 'h',
            '1' => 'y',
            'Aa' => 'y0',
            'a' => 'z',
            'zZ' => 'X9',
        ];

        $expected = $unsorted;
        OnArray::sortKey($expected);
        $this->assertEquals($sorted, $expected);

        $expected = array();
        OnArray::sortKey($expected);
        $this->assertEquals(array(), $expected);

    }

    public function testTrimArray() {
        $raw = ['hello', '"next"', '"without trailing', 'without leading"', ' with whitespace ', '" with tick and whitespace "', ''];
        $expected = ['hello', 'next', 'without trailing', 'without leading', ' with whitespace ', ' with tick and whitespace ', ''];

        $this->assertEquals($expected, OnArray::trimArray($raw, '"'));
    }

    public function testRemoveEmptyElementsFromArray() {
        $this->assertEquals(array(), OnArray::removeEmptyElementsFromArray(array()));

        $expected = array('Hello world');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($expected));

        $expected = array('Hello world', 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($expected));

        $raw2 = array('Hello world', '', 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));

        $raw2 = array('Hello world', 'Blue sky', '');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));

        $raw2 = array('', 'Hello world', 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));


        $expected = array('first' => 'Hello world');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($expected));

        $expected = array('first' => 'Hello world', 'second' => 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($expected));

        $raw2 = array('first' => 'Hello world', 'second' => '', 'third' => 'Blue sky');
        $expected = array('first' => 'Hello world', 'third' => 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));

        $raw2 = array('first' => 'Hello world', 'second' => 'Blue sky', 'third' => '');
        $expected = array('first' => 'Hello world', 'second' => 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));

        $raw2 = array('first' => '', 'second' => 'Hello world', 'third' => 'Blue sky');
        $expected = array('second' => 'Hello world', 'third' => 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));


        $raw2 = array('first' => '', '' => 'Hello world', 'third' => 'Blue sky');
        $expected = array('' => 'Hello world', 'third' => 'Blue sky');
        $this->assertEquals($expected, OnArray::removeEmptyElementsFromArray($raw2));

    }

    public function testCopyValuesIfNotAlreadyExist() {

        $this->assertEquals(array(), OnArray::copyArrayItemsIfNotAlreadyExist(array(), array(), array()));

        $this->assertEquals(array(), OnArray::copyArrayItemsIfNotAlreadyExist(['a' => 'something'], array(), array()));

        $this->assertEquals(array(), OnArray::copyArrayItemsIfNotAlreadyExist(['a' => 'something'], array(), ['b']));

        $this->assertEquals(['a' => 'base'], OnArray::copyArrayItemsIfNotAlreadyExist(['a' => 'something'], ['a' => 'base'], ['unknown']));

        $this->assertEquals(['b' => 'base', 'a' => 'something'], OnArray::copyArrayItemsIfNotAlreadyExist(['a' => 'something'], ['b' => 'base'], ['a']));

        $this->assertEquals(['a' => 'base'], OnArray::copyArrayItemsIfNotAlreadyExist(['a' => 'something'], ['a' => 'base'], ['a']));

    }

    public function testGetArrayItems() {
        $this->assertEquals(array(), OnArray::getArrayItems(array(), array()));
        $this->assertEquals(array(), OnArray::getArrayItems(['a' => 'hello'], array()));
        $this->assertEquals(array(), OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], array()));
        $this->assertEquals(array(), OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['c']));
        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['a']));
        $this->assertEquals(['a' => 'hello', 'b' => 'world'], OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['a', 'b']));

        $this->assertEquals(array(), OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['c'], false));
        $this->assertEquals(['c' => ''], OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['c'], true));

        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['a', 'c'], false));
        $this->assertEquals(['a' => 'hello', 'c' => ''], OnArray::getArrayItems(['a' => 'hello', 'b' => 'world'], ['a', 'c'], true));

        $this->assertEquals(['a' => 'hello', 'b' => '0'], OnArray::getArrayItems(['a' => 'hello', 'b' => '0'], ['a', 'b'], false));
        $this->assertEquals(['a' => 'hello', 'b' => '0'], OnArray::getArrayItems(['a' => 'hello', 'b' => 0], ['a', 'b'], false));

        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItems(['a' => 'hello', 'b' => '0'], ['a', 'b'], false, true));
        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItems(['a' => 'hello', 'b' => 0], ['a', 'b'], false, true));

        $this->assertEquals(['a' => 'hello', 'b' => ''], OnArray::getArrayItems(['a' => 'hello', 'b' => ''], ['a', 'b'], false, false, false));
        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItems(['a' => 'hello', 'b' => ''], ['a', 'b'], false, false, true));


    }

    public function testArrayValueReplace() {
        $this->assertEquals(array(), OnArray::arrayValueReplace(array(), '', ''));
        $this->assertEquals(array(), OnArray::arrayValueReplace(array(), 'a', ''));
        $this->assertEquals(array(), OnArray::arrayValueReplace(array(), '', 'b'));
        $this->assertEquals(array(), OnArray::arrayValueReplace(array(), 'a', 'b'));

        $src = ['fruit1' => 'apple', 'fruit2' => 'cherry', 'fruit3' => 'banana'];
        $this->assertEquals($src, OnArray::arrayValueReplace($src, '', ''));
        $this->assertEquals($src, OnArray::arrayValueReplace($src, 'Z', ''));
        $this->assertEquals($src, OnArray::arrayValueReplace($src, 'Z', 'Y'));

        $expected = ['fruit1' => 'Apple', 'fruit2' => 'cherry', 'fruit3' => 'bAnAnA'];
        $this->assertEquals($expected, OnArray::arrayValueReplace($src, 'a', 'A'));
    }

    public function testGetArrayItemKeyNameStartWith() {
        $this->assertEquals(array(), OnArray::getArrayItemKeyNameStartWith(array(), ''));
        $this->assertEquals(['a' => 'hello'], OnArray::getArrayItemKeyNameStartWith(['a' => 'hello'], ''));
        $this->assertEquals(array(), OnArray::getArrayItemKeyNameStartWith(['a' => 'hello'], 'b'));
//        $this->assertEquals([0 => 'hello'], OnArray::getArrayItemKeyNameStartWith(['a' => 'hello'], 'a'));
        $this->assertEquals(['' => 'hello'], OnArray::getArrayItemKeyNameStartWith(['a' => 'hello'], 'a'));
        $this->assertEquals(['b' => 'hello'], OnArray::getArrayItemKeyNameStartWith(['ab' => 'hello'], 'a'));
        $this->assertEquals(array(), OnArray::getArrayItemKeyNameStartWith(['ba' => 'hello'], 'a'));
        $this->assertEquals(['a' => 'is', 'b' => 'john'], OnArray::getArrayItemKeyNameStartWith(['1_a' => 'my', '1_b' => 'name', '2_a' => 'is', '2_b' => 'john'], '2_'));
        $this->assertEquals(['a' => 'is', 'b' => 'john'], OnArray::getArrayItemKeyNameStartWith(['1z_a' => 'my', '1z_b' => 'name', '2z_a' => 'is', '2z_b' => 'john'], '2z_'));

    }

    public function testArrayEscapeshellarg() {
        $this->assertEquals(array(), OnArray::arrayEscapeshellarg(array()));
        $this->assertEquals(['name' => "'john'"], OnArray::arrayEscapeshellarg(['name' => 'john']));
        $this->assertEquals(['name' => "'jo\"hn'"], OnArray::arrayEscapeshellarg(['name' => 'jo"hn']));
        $this->assertEquals(['name' => "'john'", 'surname' => "'doe'"], OnArray::arrayEscapeshellarg(['name' => 'john', 'surname' => 'doe']));
        $this->assertEquals(['name' => "'john'", 'sub' => ['surname' => "'doe'"]], OnArray::arrayEscapeshellarg(['name' => 'john', 'sub' => ['surname' => 'doe']]));
    }

    public function testArrayKeyNameRemoveLeadingUnderscore() {

        // Tip: the test source is the second argument, the check is the first argument
        $this->assertEquals(array(), OnArray::keyNameRemoveLeadingUnderscore(array()));
        $this->assertEquals(['name' => 'john'], OnArray::keyNameRemoveLeadingUnderscore(['name' => 'john']));
        $this->assertEquals(['name' => 'john', 'surname' => 'doe'], OnArray::keyNameRemoveLeadingUnderscore(['name' => 'john', 'surname' => 'doe']));
        $this->assertEquals(['name' => '_john'], OnArray::keyNameRemoveLeadingUnderscore(['_name' => '_john']));
        $this->assertEquals(['name' => 'john', 'surname' => 'doe'], OnArray::keyNameRemoveLeadingUnderscore(['_name' => 'john', 'surname' => 'doe']));
        $this->assertEquals(['name' => 'john', 'surname' => 'doe'], OnArray::keyNameRemoveLeadingUnderscore(['name' => 'john', '_surname' => 'doe']));
        $this->assertEquals(['name' => 'john', 'surname' => 'doe'], OnArray::keyNameRemoveLeadingUnderscore(['_name' => 'john', '_surname' => 'doe']));
        $this->assertEquals(['name' => '_john', 'surname' => '_doe'], OnArray::keyNameRemoveLeadingUnderscore(['_name' => '_john', '_surname' => '_doe']));
    }

    public function testStrposArr() {
        $this->assertEquals(false, OnArray::strposArr(array(), 'test'));
        $this->assertEquals(false, OnArray::strposArr(['', ''], 'test'));
        $this->assertEquals('test', OnArray::strposArr(['test', ''], 'test'));
        $this->assertEquals('test', OnArray::strposArr(['', 'test'], 'test'));
        $this->assertEquals('test', OnArray::strposArr(['test', ''], 'te'));
        $this->assertEquals('test', OnArray::strposArr(['', 'test'], 'te'));
        $this->assertEquals(false, OnArray::strposArr(['fake1', 'fake2'], 'te'));

    }
}
