<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 11/02/18
 * Time: 9:16 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\OnString;
use PHPUnit\Framework\TestCase;

/**
 * Class OnStringTest
 * @package qfq
 */
class OnStringTest extends TestCase {

    /**
     *
     */
    public function testStrrstr() {

        $this->assertEquals('', OnString::strrstr('', ''));
        $this->assertEquals('', OnString::strrstr('hello world to the limit', ''));
        $this->assertEquals('limit', OnString::strrstr('hello world to the limit', ' '));
        $this->assertEquals('', OnString::strrstr('', ' '));
    }

    /**
     *
     */
    public function testStripFirstCharIf() {
        $this->assertEquals('', OnString::stripFirstCharIf('', ''));
        $this->assertEquals('', OnString::stripFirstCharIf('c', ''));
        $this->assertEquals('', OnString::stripFirstCharIf('c', 'c'));
        $this->assertEquals('def', OnString::stripFirstCharIf('c', 'cdef'));
        $this->assertEquals('def', OnString::stripFirstCharIf('c', 'def'));
        $this->assertEquals('def', OnString::stripFirstCharIf('cd', 'def'));

    }

    /**
     *
     */
    public function testTrimQuote() {
        # no quote
        $this->assertEquals('', OnString::trimQuote(''));
        $this->assertEquals('test', OnString::trimQuote('test'));
        $this->assertEquals(' test', OnString::trimQuote(' test'));
        $this->assertEquals('test ', OnString::trimQuote('test '));
        $this->assertEquals(' test ', OnString::trimQuote(' test '));

        # single occurence at start/end
        $this->assertEquals('" test ', OnString::trimQuote('" test '));
        $this->assertEquals('" test \'', OnString::trimQuote('" test \''));
        $this->assertEquals('\' test "', OnString::trimQuote('\' test "'));

        # single occurence in the middle
        $this->assertEquals(' te"st ', OnString::trimQuote("' te\"st '"));
        $this->assertEquals(' te\'st ', OnString::trimQuote("' te'st '"));

        # double tick
        $this->assertEquals('test', OnString::trimQuote('"test"'));
        $this->assertEquals(' test', OnString::trimQuote('" test"'));
        $this->assertEquals('test ', OnString::trimQuote('"test "'));
        $this->assertEquals(' test ', OnString::trimQuote('" test "'));

        # single tick
        $this->assertEquals("test", OnString::trimQuote("'test'"));
        $this->assertEquals(" test", OnString::trimQuote("' test'"));
        $this->assertEquals("test ", OnString::trimQuote("'test '"));
        $this->assertEquals(" test ", OnString::trimQuote("' test '"));

        # double qutoe: only remove outermost
        $this->assertEquals("\"test", OnString::trimQuote("'\"test'"));
        $this->assertEquals("\"test\"", OnString::trimQuote("'\"test\"'"));
        $this->assertEquals("'test'", OnString::trimQuote("''test''"));

    }

    /**
     *
     */
    public function testTrimQuoteTag() {
        # no quote
        $this->assertEquals('', OnString::trimQuoteTag(''));
        $this->assertEquals('test', OnString::trimQuoteTag('test'));
        $this->assertEquals(' test', OnString::trimQuoteTag(' test'));
        $this->assertEquals('test ', OnString::trimQuoteTag('test '));
        $this->assertEquals(' test ', OnString::trimQuoteTag(' test '));

        # less/equal sign
        $this->assertEquals("test", OnString::trimQuoteTag("<test>"));
        $this->assertEquals("<test", OnString::trimQuoteTag("<test"));
        $this->assertEquals("test>", OnString::trimQuoteTag("test>"));
    }

    /**
     *
     */
    public function testRemoveNewlinesInNestedExpression() {
        $this->assertEquals("", OnString::removeNewlinesInNestedExpression(""));
        $this->assertEquals("test", OnString::removeNewlinesInNestedExpression("test"));
        $this->assertEquals("{{test}}", OnString::removeNewlinesInNestedExpression("{{test}}"));
        $this->assertEquals("{{test }}", OnString::removeNewlinesInNestedExpression("{{test\n}}"));
        $this->assertEquals("{{test two}}", OnString::removeNewlinesInNestedExpression("{{test\ntwo}}"));
        $this->assertEquals("Pre{{test two}}Post", OnString::removeNewlinesInNestedExpression("Pre{{test\ntwo}}Post"));
        $this->assertEquals("{{test{{two}}}}", OnString::removeNewlinesInNestedExpression("{{test{{two}}}}"));
        $this->assertEquals("{{a {{b }}}}",
            OnString::removeNewlinesInNestedExpression("{{a\n{{b\n}}}}"));
        $this->assertEquals("{{a b{{c d}}{{e f}} h}}",
            OnString::removeNewlinesInNestedExpression("{{a\nb{{c\nd}}{{e\nf}}\nh}}"));
        $this->assertEquals("param1=abc\nsqlInsert = {{SELECT * FROM test WHERE '{{var}}'='true'}}",
            OnString::removeNewlinesInNestedExpression("param1=abc\nsqlInsert = {{SELECT *\nFROM test\nWHERE '{{var}}'='true'}}"));
        $this->assertEquals("sqlInsert = {{SELECT * FROM test WHERE '{{var}}'='true'}}\nparam1=abc",
            OnString::removeNewlinesInNestedExpression("sqlInsert = {{SELECT *\nFROM test\nWHERE '{{var}}'='true'}}\nparam1=abc"));
    }

    /**
     * @throws \UserFormException
     */
    public function testExtractFormRecordId() {
        $arrId = array();
        $arrForm = array();

        $this->assertEquals('', OnString::splitPathInfoToIdForm('', $arrId, $arrForm));
        $this->assertEquals([], $arrId);
        $this->assertEquals([], $arrForm);

        $this->assertEquals('', OnString::splitPathInfoToIdForm('/', $arrId, $arrForm));
        $this->assertEquals([], $arrId);
        $this->assertEquals([], $arrForm);

        $this->assertEquals('', OnString::splitPathInfoToIdForm('//', $arrId, $arrForm));
        $this->assertEquals([], $arrId);
        $this->assertEquals([], $arrForm);

        $this->assertEquals('path1', OnString::splitPathInfoToIdForm('/path1', $arrId, $arrForm));
        $this->assertEquals([0], $arrId);
        $this->assertEquals(['path1'], $arrForm);

        $this->assertEquals('path1', OnString::splitPathInfoToIdForm('/path1/', $arrId, $arrForm));
        $this->assertEquals([0], $arrId);
        $this->assertEquals(['path1'], $arrForm);

        $this->assertEquals('path1', OnString::splitPathInfoToIdForm('path1/', $arrId, $arrForm));
        $this->assertEquals([0], $arrId);
        $this->assertEquals(['path1'], $arrForm);

        $this->assertEquals('path1', OnString::splitPathInfoToIdForm('/path1/12', $arrId, $arrForm));
        $this->assertEquals([12], $arrId);
        $this->assertEquals(['path1'], $arrForm);

        $this->assertEquals('path1', OnString::splitPathInfoToIdForm('/path1/12/', $arrId, $arrForm));
        $this->assertEquals([12], $arrId);
        $this->assertEquals(['path1'], $arrForm);

        $this->assertEquals('path2', OnString::splitPathInfoToIdForm('/path1/12/path2', $arrId, $arrForm));
        $this->assertEquals([12, 0], $arrId);
        $this->assertEquals(['path1', 'path2'], $arrForm);

        $this->assertEquals('path2', OnString::splitPathInfoToIdForm('/path1/12/path2/34', $arrId, $arrForm));
        $this->assertEquals([12, 34], $arrId);
        $this->assertEquals(['path1', 'path2'], $arrForm);

    }

    /**
     *
     */
    public function testExtractFormRecordId_1() {
        $this->expectException(\UserFormException::class);

        $arrId = array();
        $arrForm = array();

        # An alnum string is requested as path
        $this->assertEquals('', OnString::splitPathInfoToIdForm('/%', $arrId, $arrForm));
    }

    /**
     */
    public function testExtractFormRecordId_2() {

        $this->expectException(\UserFormException::class);

        $arrId = array();
        $arrForm = array();

        # A numerical value is requested as id
        $this->assertEquals('', OnString::splitPathInfoToIdForm('/path1/path2', $arrId, $arrForm));
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingClosing_1() {
        $this->expectException(\UserFormException::class);

        $str = OnString::removeNewlinesInNestedExpression("Hi! {{Test ");
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingClosing_2() {
        $this->expectException(\UserFormException::class);

        $str = OnString::removeNewlinesInNestedExpression("Hi! {{Test}");
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingClosing_3() {
        $this->expectException(\UserFormException::class);

        $str = OnString::removeNewlinesInNestedExpression("Hi! {{Test {{var }}");
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingOpening_1() {
        $this->expectException(\UserFormException::class);
        $str = OnString::removeNewlinesInNestedExpression("}}");
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingOpening_2() {
        $this->expectException(\UserFormException::class);
        $str = OnString::removeNewlinesInNestedExpression("{}}");
    }

    /**
     */
    public function testRemoveNewlinesInNestedExpression_missingOpening_3() {
        $this->expectException(\UserFormException::class);
        $str = OnString::removeNewlinesInNestedExpression("{{Test}}}}");
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGetEuropeanTimezone() {

        // It seems in phpunit there is no default time zone set - set it manually.
        date_default_timezone_set('Europe/Berlin');

        # Check 2020 incl. hour
        $this->assertEquals('CET', OnString::getEuropeanTimezone('29.03.2020 01:00'));
        $this->assertEquals('CEST', OnString::getEuropeanTimezone('29.03.2020 03:00'));
        $this->assertEquals('CEST', OnString::getEuropeanTimezone('25.10.2020 01:00'));
        $this->assertEquals('CET', OnString::getEuropeanTimezone('25.10.2020 03:00'));

        # Check 2021 incl. hour
        $this->assertEquals('CET', OnString::getEuropeanTimezone('28.03.2021 01:00'));
        $this->assertEquals('CEST', OnString::getEuropeanTimezone('28.03.2021 03:00'));
        $this->assertEquals('CEST', OnString::getEuropeanTimezone('31.10.2021 01:00'));
        $this->assertEquals('CET', OnString::getEuropeanTimezone('31.10.2021 03:00'));

        # Check 2021 only date
        $this->assertEquals('CET', OnString::getEuropeanTimezone('28.03.2021'));
        $this->assertEquals('CEST', OnString::getEuropeanTimezone('29.03.2021'));

        $this->assertContains(OnString::getEuropeanTimezone(''), ['CET', 'CEST']);

        # Check if action parameter 't' is recognized.
//        $this->store->setVar('start', '02.06.2020', STORE_FORM, true);
//        $this->assertEquals('CEST', $eval->substitute('start:F:all:t', $foundInStore));
    }

    /**
     *
     */
    public function testEscapeSingleTickInHtml() {

        $this->assertEquals('<b>hello</b>', OnString::escapeSingleTickInHtml('<b>hello</b>'));
        $this->assertEquals("<b>hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b>hel'lo</b>"));
        $this->assertEquals('<b>hel"lo</b>', OnString::escapeSingleTickInHtml('<b>hel"lo</b>'));
        $this->assertEquals('<b>hel&apos;l"o</b>', OnString::escapeSingleTickInHtml('<b>hel\'l"o</b>'));

        $this->assertEquals("<b title=test>hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title=test>hel'lo</b>"));
        $this->assertEquals("<b title = test > hel&apos;lo </b>", OnString::escapeSingleTickInHtml("<b title = test > hel'lo </b>"));

        $this->assertEquals("<b title=\"test\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title='test'>hel'lo</b>"));
        $this->assertEquals("<b title = \"test\" > hel&apos;lo </b>", OnString::escapeSingleTickInHtml("<b title = 'test' > hel'lo </b>"));

        $this->assertEquals("<b title=\"test\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title=\"test\">hel'lo</b>"));
        $this->assertEquals("<b title= \"test\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= 'test'>hel'lo</b>"));
        $this->assertEquals("<b title= \"test\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= \"test\">hel'lo</b>"));
        $this->assertEquals("<b title=\"test\" >hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title=\"test\" >hel'lo</b>"));
        $this->assertEquals("<b title= \"test\" >hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= \"test\" >hel'lo</b>"));

        $this->assertEquals("<b title=\"te&apos;st\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title=\"te'st\">hel'lo</b>"));
        $this->assertEquals("<b title= \"te&apos;st\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= \"te'st\">hel'lo</b>"));
        $this->assertEquals("<b title= \"te&apos;st\" >hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= \"te'st\" >hel'lo</b>"));

        $this->assertEquals("<b title=\"te&quot;st\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title='te\"st'>hel'lo</b>"));
        $this->assertEquals("<b title= \"te&quot;st\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= 'te\"st'>hel'lo</b>"));
        $this->assertEquals("<b title= \"te&quot;st\" >hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b title= 'te\"st' >hel'lo</b>"));

        $this->assertEquals("<b src=gif title=\"test\" alt=jpg>hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=gif title='test' alt=jpg>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"test\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src='gif' title='test' alt='jpg'>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"test\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=\"gif\" title='test' alt='jpg'>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"test\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src='gif' title='test' alt=\"jpg\">hel'lo</b>"));

        $this->assertEquals("<b src=gif title=\"te&quot;st\" alt=jpg>hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=gif title='te\"st' alt=jpg>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"te&quot;st\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src='gif' title='te\"st' alt=\"jpg\">hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"te&quot;st\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=\"gif\" title='te\"st' alt='jpg'>hel'lo</b>"));

        $this->assertEquals("<b src=gif title=\"te&apos;st\" alt=jpg>hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=gif title=\"te'st\" alt=jpg>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"te&apos;st\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src='gif' title=\"te'st\" alt='jpg'>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"te&apos;st\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src=\"gif\" title=\"te'st\" alt='jpg'>hel'lo</b>"));
        $this->assertEquals("<b src=\"gif\" title=\"te&apos;st\" alt=\"jpg\">hel&apos;lo</b>", OnString::escapeSingleTickInHtml("<b src='gif' title=\"te'st\" alt=\"jpg\">hel'lo</b>"));

        $expected = "<img src=\"pig.gif\" title=\"Pig\"><img src=\"sun.gif\" title=\"Sun\">";
        $this->assertEquals($expected, OnString::escapeSingleTickInHtml("<img src='pig.gif' title='Pig'><img src='sun.gif' title='Sun'>"));

        $expected = "<img src=\"pig.gif\" title=\"Pig &quot;blue&quot;\"><img src=\"sun.gif\" title=\"Sun &quot;bright&quot;\">";
        $this->assertEquals($expected, OnString::escapeSingleTickInHtml("<img src='pig.gif' title='Pig \"blue\"'><img src='sun.gif' title='Sun \"bright\"'>"));
    }

    public function testRemoveLeadingBrace() {
        $this->assertEquals('', OnString::removeLeadingBrace(""));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace("SELECT"));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace(" SELECT"));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace("( SELECT"));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace(" (SELECT"));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace("((SELECT"));
        $this->assertEquals('SELECT', OnString::removeLeadingBrace("  (  (  SELECT"));

    }

    public function testUrlStripFile() {
        $this->assertEquals('', OnString::urlStripFile(""));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/index.php"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/index.php?id=100"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/?id=100"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/#new"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/help#new"));
        $this->assertEquals('www.example.com', OnString::urlStripFile("www.example.com/index.php?help#new"));

        $this->assertEquals('http://www.example.com', OnString::urlStripFile("http://www.example.com/help#new"));
        $this->assertEquals('http://www.example.com:9090', OnString::urlStripFile("http://www.example.com:9090/help#new"));

        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub"));
        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub/"));
        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub/?id=1"));
        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub/index.php"));
        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub/index.php?id=1"));
        $this->assertEquals('www.example.com/help/sub', OnString::urlStripFile("www.example.com/help/sub/index.php?id=1#freak"));
    }

    public function testsplitEmailRealname() {
        $this->assertEquals([NAME_EMAIL => '', NAME_REALNAME => ''], OnString::splitEmailRealname(''));
        $this->assertEquals([NAME_EMAIL => 'john@doe.com', NAME_REALNAME => ''], OnString::splitEmailRealname('john@doe.com'));
        $this->assertEquals([NAME_EMAIL => 'john@doe.com', NAME_REALNAME => ''], OnString::splitEmailRealname('<john@doe.com>'));
        $this->assertEquals([NAME_EMAIL => 'john@doe.com', NAME_REALNAME => 'John Doe'], OnString::splitEmailRealname('John Doe <john@doe.com>'));
        $this->assertEquals([NAME_EMAIL => 'john@doe.com', NAME_REALNAME => 'John Doe'], OnString::splitEmailRealname('John Doe john@doe.com'));
    }

    public function testBalancedBraces() {
        $this->assertTrue(OnString::validateBraceBalance("{{valid}}"));
        $this->assertTrue(OnString::validateBraceBalance("Some text {{inside}} braces"));
        $this->assertTrue(OnString::validateBraceBalance("{{}}{{}}"));  // Multiple pairs
        $this->assertTrue(OnString::validateBraceBalance("Prefix {{data}} Suffix"));
        $this->assertTrue(OnString::validateBraceBalance("{{multiple}} {{valid}} {{pairs}}"));
    }

    public function testUnbalancedBraces() {
        $this->assertFalse(OnString::validateBraceBalance("{{unmatched}"));  // Missing closing `}}`
        $this->assertFalse(OnString::validateBraceBalance("missing opening}}"));  // Missing opening `{{`
        $this->assertFalse(OnString::validateBraceBalance("{{valid}} text }} extra"));  // Extra `}}`
        $this->assertFalse(OnString::validateBraceBalance("{{unmatched start"));  // No closing `}}`
        $this->assertFalse(OnString::validateBraceBalance("}}"));  // Unmatched `}}`
    }

}