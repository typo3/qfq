<?php
/**
 * Created by PhpStorm.
 * User: megger
 * Date: 12/17/18
 * Time: 9:14 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\BindParam;
use PHPUnit\Framework\TestCase;

/**
 * Class BindParamTest
 * @package qfq
 */
class BindParamTest extends TestCase {
    /**
     * @covers ::BindParam
     */
    public function testParamInitiallyEmptyString() {
        $bp = new BindParam;
        $this->assertEquals([''], $bp->get());
        return $bp;
    }

    /**
     * @covers  BindParam::add
     * @covers  BindParam::get
     * @depends testParamInitiallyEmptyString
     * @param $bp
     */
    public function testAddValues($bp) {
        $valInt = 1;
        $bp->add($valInt);
        $this->assertEquals(['i', 1], $bp->get());
        $valStr = 'a';
        $bp->add($valStr);
        $this->assertEquals(['is', 1, 'a'], $bp->get());
    }
}