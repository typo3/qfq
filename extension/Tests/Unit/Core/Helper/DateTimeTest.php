<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\DateTime;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase {

    public function testBuildDateTime() {
        $formElement[FE_TYPE] = 'datetime';
        $formElement[FE_DATE_TIME_PICKER_TYPE] = 'qfq';
        $formElement[FE_DATE_FORMAT] = 'dd.mm.yyyy';
        $formElement[FE_MIN] = '23.04.2021 21:00';
        $formElement[FE_MAX] = '14.08.2021 12:00';
        $formElement[FE_SHOW_SECONDS] = '0';
        $formElement[FE_PLACEHOLDER] = '';

        self::setFormElementDefaults($formElement);

        $json = array();
        $store = Store::getInstance();

        $formSpec[F_MULTI_MODE] = '';
        $formSpec[F_ID] = 1337;
        $formSpec[F_NAME] = 'TestForm';
        $formSpec[F_DB_INDEX] = 1;
        $formSpec[F_PRIMARY_KEY] = F_PRIMARY_KEY_DEFAULT;
        $formSpec[FE_CLASS_NATIVE] = array();
        $formSpec[F_ACTIVATE_FIRST_REQUIRED_TAB] = false;
        Store::setVar(SYSTEM_RENDERER, "Bootstrap3Renderer", STORE_SYSTEM);

        $result = DateTime::buildDateTime($formElement, 'dateFrom', '23.05.2022 23:32', $json, $formSpec, $store, null, 'fixed');
        $excepted = '<input id="10" name="dateFrom" class="form-control qfq-datepicker" data-reference="testReference" type="text" data-format="DD.MM.YYYY HH:mm" data-show-clear-button="false" data-minDate="23.04.2021 21:00" data-maxDate="14.08.2021 12:00" value="23.05.2022 23:32" data-pattern-error="Please match the format: DD.MM.YYYY HH:mm" placeholder="DD.MM.YYYY HH:mm" min="23.04.2021 21:00" max="14.08.2021 12:00" data-hidden="no" data-disabled="no" data-required="no" ><div class="help-block with-errors hidden"></div>';
        $this->assertEquals($excepted, $result);
    }

    /**
     *  Convert dateTime from form to store it in database
     *
     * @return void
     * @throws \UserFormException
     */
    public function testDoDateTime() {
        $formElement = array();
        $formElement[FE_DATE_TIME_PICKER_TYPE] = 'qfq';
        $formElement[FE_DATE_FORMAT] = 'dd.mm.yyyy';
        $formElement[FE_SHOW_SECONDS] = 1;
        $formElement[FE_MIN] = '';
        $formElement[FE_MAX] = '';

        $formElement[FE_TYPE] = 'datetime';
        $result = DateTime::doDateTime($formElement, '24.04.2020 23:55:12');
        $this->assertEquals('2020-04-24 23:55:12', $result);

        $formElement[FE_TYPE] = 'date';
        $result = DateTime::doDateTime($formElement, '24.04.2020');
        $this->assertEquals('2020-04-24', $result);

        $formElement[FE_TYPE] = 'time';
        $result = DateTime::doDateTime($formElement, '23:29:12');
        $this->assertEquals('23:29:12', $result);

        // Test with offset seconds
        $formElement[FE_SHOW_SECONDS] = 0;
        $result = DateTime::doDateTime($formElement, '23:29:00');
        $this->assertEquals('23:29', $result);

        $formElement[FE_TYPE] = 'datetime';
        $result = DateTime::doDateTime($formElement, '24.04.2020 23:55:00');
        $this->assertEquals('2020-04-24 23:55', $result);

        // International date format
        $formElement[FE_DATE_FORMAT] = 'yyyy-mm-dd';
        $result = DateTime::doDateTime($formElement, '2020-04-24 23:55');
        $this->assertEquals('2020-04-24 23:55', $result);

        $formElement[FE_TYPE] = 'date';
        $result = DateTime::doDateTime($formElement, '2020-04-24');
        $this->assertEquals('2020-04-24', $result);

    }

    public function setFormElementDefaults(&$formElement) {
        $formElement[FE_ID] = 10;
        $formElement[FE_HTML_ID] = '10';
        $formElement[FE_NAME] = 'form1';
        $formElement[FE_SHOW_ZERO] = '0';
        $formElement[FE_DATA_REFERENCE] = 'testReference';
        $formElement[FE_CHECK_PATTERN] = '';
        $formElement[FE_CHECK_TYPE] = 'pattern';
        $formElement[FE_MAX_LENGTH] = '';
        $formElement[F_FE_DATA_PATTERN_ERROR] = '';
        $formElement[F_FE_DATA_PATTERN_ERROR_SYSTEM] = '';
        $formElement[FE_DYNAMIC_UPDATE] = 'no';
        $formElement[FE_TOOLTIP] = '';
        $formElement[FE_MODE] = 'show';
        $formElement[FE_VALUE] = '';
        $formElement[FE_NOTE] = '';
    }
}
