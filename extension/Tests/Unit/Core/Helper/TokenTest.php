<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 11/02/18
 * Time: 9:16 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\Token;
use PHPUnit\Framework\TestCase;


/**
 * Class TokenTest
 * @package qfq
 */
class TokenTest extends TestCase {

    public function testExplodeTokenString() {

        // Edge cases: flagAssoc=true
        $this->assertEquals(array(), Token::explodeTokenString(''));
        $this->assertEquals(['   ' => ''], Token::explodeTokenString('   '));

        $this->assertEquals(['' => ''], Token::explodeTokenString(':'));
        $this->assertEquals(['' => '::'], Token::explodeTokenString(':::'));

        $this->assertEquals(array(), Token::explodeTokenString('|'));
        $this->assertEquals(array(), Token::explodeTokenString('|||'));

        $this->assertEquals(['1' => ''], Token::explodeTokenString('1'));
        $this->assertEquals(['T' => ''], Token::explodeTokenString('T:'));

        $this->assertEquals(['T' => '', 'W' => ''], Token::explodeTokenString('T|W'));

        $this->assertEquals(['T' => '', 'W' => ''], Token::explodeTokenString('||T|W||'));

        // Regular
        $this->assertEquals(['T' => 'arg1'], Token::explodeTokenString('T:arg1'));
        $this->assertEquals(['T' => 'arg1', 'W' => 'arg2'], Token::explodeTokenString('T:arg1|W:arg2'));
        $this->assertEquals(['T' => 'arg1:arg1.1', 'W' => 'arg2'], Token::explodeTokenString('T:arg1:arg1.1|W:arg2'));
        $this->assertEquals(['T' => ':arg1:arg1.1::', 'W' => 'arg2'], Token::explodeTokenString('T::arg1:arg1.1::|W:arg2'));
    }


    public function testExplodeDimension() {

        $this->assertEquals('', Token::explodeDimension(''));
        $this->assertEquals('-w 150', Token::explodeDimension('150'));
        $this->assertEquals('-w 150', Token::explodeDimension('150x'));
        $this->assertEquals('-w 150 -h 200', Token::explodeDimension('150x200'));
        $this->assertEquals('-h 200', Token::explodeDimension('x200'));
    }
}

