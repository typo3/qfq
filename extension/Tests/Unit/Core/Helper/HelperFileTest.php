<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 11/02/18
 * Time: 9:16 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;

use IMATHUZH\Qfq\Core\Helper\HelperFile;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class HelperFileTest
 * @package qfq
 */
class HelperFileTest extends TestCase {

    /**
     * @throws \UserFormException
     */
    public function testGetFileTypeHighlight() {

        $this->assertEquals('', HelperFile::getFileTypeHighlight('', ''));
        $this->assertEquals('', HelperFile::getFileTypeHighlight('', 'fileadmin/test.js'));

        $this->assertEquals('', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_OFF, ''));
        $this->assertEquals('', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_OFF, 'fileadmin/test.js'));

        $this->assertEquals('', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, ''));

        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/javascript.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_JAVASCRIPT, ''));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.qfq.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_QFQ, ''));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.py.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_PYTHON, ''));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.m.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_MATLAB, ''));

        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/javascript.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_JAVASCRIPT, 'fileadmin/test.js'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.qfq.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_QFQ, 'fileadmin/test.js'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.py.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_PYTHON, 'fileadmin/test.js'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.m.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_MATLAB, 'fileadmin/test.js'));

        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/javascript.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, 'fileadmin/test.js'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.php.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, 'fileadmin/test.php'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.qfq.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, 'fileadmin/test.qfq'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.py.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, 'fileadmin/test.py'));
        $this->assertEquals(Path::urlExt(Path::EXT_TO_HIGHLIGHT_JSON) . '/highlight.m.json', HelperFile::getFileTypeHighlight(FE_HIGHLIGHT_AUTO, 'fileadmin/test.m'));
    }

    public function testJoinPathFilename() {

        $this->assertEquals('', HelperFile::joinPathFilename('', ''));
        $this->assertEquals('/', HelperFile::joinPathFilename('/', ''));
        $this->assertEquals('/', HelperFile::joinPathFilename('', '/'));
        $this->assertEquals('/', HelperFile::joinPathFilename('/', '/'));

        $this->assertEquals('a/b', HelperFile::joinPathFilename('a', 'b'));
        $this->assertEquals('/a/b', HelperFile::joinPathFilename('/a', 'b'));
        $this->assertEquals('/b', HelperFile::joinPathFilename('a', '/b'));
        $this->assertEquals('/b', HelperFile::joinPathFilename('/a', '/b'));

        $this->assertEquals('a/b', HelperFile::joinPathFilename('a/', 'b'));
        $this->assertEquals('a/b', HelperFile::joinPathFilename('a', 'b'));
        $this->assertEquals('/b', HelperFile::joinPathFilename('a/', '/b'));
        $this->assertEquals('/b', HelperFile::joinPathFilename('a', '/b'));
    }

    /**
     *
     */
    protected function setUp(): void {
        parent::setUp();
    }

}

