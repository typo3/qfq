<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/2/16
 * Time: 11:10 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;


use IMATHUZH\Qfq\Core\Helper\Sanitize;
use PHPUnit\Framework\TestCase;

/**
 * Class SanitizeTest
 * @package qfq
 */
class SanitizeTest extends TestCase {

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSanitize() {

        # Violates SANITIZE class: SANITIZE string is always an empty string.
        # Access are cached: use new variables for every test.

        # Check ''
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals('', Sanitize::sanitize('', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check '1'
        $this->assertEquals('1', Sanitize::sanitize('1', SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('1', Sanitize::sanitize('1', SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize('1', SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals('1', Sanitize::sanitize('1', SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals('1', Sanitize::sanitize('1', SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals('1', Sanitize::sanitize('1', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check '-3'
        $this->assertEquals('-3', Sanitize::sanitize('-3', SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('!!digit!!', Sanitize::sanitize('-3', SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize('-3', SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals('-3', Sanitize::sanitize('-3', SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals('-3', Sanitize::sanitize('-3', SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals('-3', Sanitize::sanitize('-3', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check 'a'
        $this->assertEquals('a', Sanitize::sanitize('a', SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('!!digit!!', Sanitize::sanitize('a', SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize('a', SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals('a', Sanitize::sanitize('a', SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals('a', Sanitize::sanitize('a', SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals('a', Sanitize::sanitize('a', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");


        # Check 'a@-_.,;Z09'
        $val = 'a@-_.,;Z09';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('!!digit!!', Sanitize::sanitize($val, SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check 'a+Z09'
        $val = 'a+Z09';
        $this->assertEquals('!!alnumx!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('!!digit!!', Sanitize::sanitize($val, SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check 'ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüÿ'
        $val = 'ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜŸäëïöüÿ';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALNUMX), "SANITIZE_ALNUMX fails");
        $this->assertEquals('!!digit!!', Sanitize::sanitize($val, SANITIZE_ALLOW_DIGIT), "SANITIZE_DIGIT fails");
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_EMAIL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '.*'), "SANITIZE_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL), "SANITIZE_ALL fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLBUT fails");

        # Check Decimal Format
        $msg = "Sanitize: Decimal Format check fails";
        $val = '123.45';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '10,3'), $msg);
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '4,2'), $msg);
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,1'), $msg);
        $val = '-123.45';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '10,3'), $msg);
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '4,2'), $msg);
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,1'), $msg);
        $val = 'a.00';
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
        $val = '-0.1e9';
        $this->assertEquals('!!all!!', Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
        $val = '-4';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
        $val = '.42';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_ALL, '', '5,2'), $msg);
    }

    /**
     * @throws \UserFormException
     */
    public function testSanitizeMinMax() {
        $msg = "SANITIZE_MIN_MAX fails";

        # Check numerical min/max
        $val = 56;
        $this->assertEquals('', Sanitize::checkMinMax($val, "0", "2"), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "0", ""), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "", "56"), $msg);

        $this->assertEquals('', Sanitize::checkMinMax($val, "57", ""), $msg);
        $this->assertEquals('', Sanitize::checkMinMax($val, "", "2"), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "0", "200"), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "-100", "200"), $msg);

        $val = -56;
        $this->assertEquals('', Sanitize::checkMinMax($val, "0", "2"), $msg);
        $this->assertEquals('', Sanitize::checkMinMax($val, "0", "200"), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "-100", "200"), $msg);

        # Check min/max dates
        $msg = "SANITIZE_MIN_MAX Date fails";
        $val = "2010-05-01";
        $this->assertEquals($val, Sanitize::checkMinMax($val, "2010-01-01", "2010-12-31"), $msg);
        $this->assertEquals('', Sanitize::checkMinMax($val, "2010-01-01", "2010-04-30"), $msg);
        $this->assertEquals('', Sanitize::checkMinMax($val, "2010-01-01", "2009-12-31"), $msg);
        $this->assertEquals('', Sanitize::checkMinMax($val, "2011-01-01", "2009-12-31"), $msg);
        $this->assertEquals($val, Sanitize::checkMinMax($val, "2010-05-01", "2010-05-01"), $msg);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSanitizeEmail() {

        # Check
        $val = 'john';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'john@';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'john@doe';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'john@doe.com';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'john@ doe.com';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = '<john@doe.com>';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'John Doe <john@doe.com>';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = '_john@doe.com';
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'jo*hn@doe.com';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'jo[hn@doe.com';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'jo\hn@doe.com';
        $this->assertEquals('!!email!!', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");

        $val = 'jo%hn@doe.com';
        $this->assertEquals('jo%hn@doe.com', Sanitize::sanitize($val, SANITIZE_ALLOW_EMAIL), "SANITIZE_ALLOW_EMAIL fails");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSanitizePattern() {

        # Check
        $val = 'john';
        $this->assertEquals('!!pattern!!', Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '\d'), "SANITIZE_ALLOW_PATTERN fails");
        $this->assertEquals('!!pattern!!', Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '\s'), "SANITIZE_ALLOW_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, 'john'), "SANITIZE_ALLOW_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '(john)?'), "SANITIZE_ALLOW_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '(john)*'), "SANITIZE_ALLOW_PATTERN fails");
        $this->assertEquals($val, Sanitize::sanitize($val, SANITIZE_ALLOW_PATTERN, '(John)*'), "SANITIZE_ALLOW_PATTERN fails");
    }

    //[ ]  { } % & \ #

    /**
     */
    public function testSanitizeExceptionAllBut() {
        $bad = "[]{}%\\#";
        $good = 'abCD01`~&!@$^*()_+=-|":;.,<>/?\'';

        // Single
        $this->assertEquals('!!allbut!!', Sanitize::sanitize('[', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLOW_ALLBUT fails");
        $this->assertEquals('a', Sanitize::sanitize('a', SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLOW_ALLBUT fails");


        for ($i = 0; $i < strlen($bad); $i++) {
            $str = '-' . substr($bad, $i, 1) . '-';
            $this->assertEquals('!!allbut!!', Sanitize::sanitize($str, SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLOW_ALLBUT fails");
        }

        for ($i = 0; $i < strlen($good); $i++) {
            $str = '-' . substr($good, $i, 1) . '-';
            $this->assertEquals($str, Sanitize::sanitize($str, SANITIZE_ALLOW_ALLBUT), "SANITIZE_ALLOW_ALLBUT fails");
        }
    }

    /**
     */
    public function testSanitizeException() {
        $this->expectException(\CodeException::class);
        Sanitize::sanitize('Hello World', 'invalid sanitize class');
    }

    /**
     */
    public function testSanitizeExceptionCheckFailed() {
        $this->expectException(\UserFormException::class);
        Sanitize::sanitize('string', SANITIZE_ALLOW_DIGIT, '', '', SANITIZE_EXCEPTION);
    }

    /**
     * Test string, numeric, array, subarray
     *
     * @throws \CodeException
     */
    public function testNormalize() {

        // Nothing changed
        $char_A_ring = "\xC3\x85"; // 'LATIN CAPITAL LETTER 'A' WITH RING ABOVE' (U+00C5)
        $this->assertEquals($char_A_ring, Sanitize::normalize($char_A_ring), "'A' with ring above");

        // Convert "\xCC\x8A" to  "\xC3\x85"
        $char_combining_ring_above = 'A' . "\xCC\x8A";  // 'COMBINING RING ABOVE' (U+030A)
        $this->assertEquals($char_A_ring, Sanitize::normalize($char_combining_ring_above), "Combined 'A' with ring above");

        $in = [$char_A_ring, $char_combining_ring_above, $char_A_ring];
        $out = [$char_A_ring, $char_A_ring, $char_A_ring];
        $this->assertEquals($out, Sanitize::normalize($in), "Combined 'A' with ring above");

        $value = 0;
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = '';
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = 'string';
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = 123.45;
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['string', 'second'];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['string', 'second', 123.45];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['string', 0, ''];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['a' => 'string', 'b' => 'second'];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['a' => 'string', 'b' => 'second', 'c' => 123.45];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        $value = ['a' => 'string', 'b' => 0, 'c' => ''];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        // subarray
        $value = [$value, 'a' => 'string', 'b' => 0, 'c' => ''];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');

        // sub sub array
        $value = [$value, 'a' => 'string', 'b' => 0, 'c' => ''];
        $this->assertEquals($value, Sanitize::normalize($value), 'Check simple string');
    }


    /**
     * Test string, numeric, array, subarray
     *
     */
    public function testSafeFilename() {

        $value = '';
        $this->assertEquals($value, Sanitize::safeFilename($value), 'Empty string');

        $value = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $this->assertEquals($value, Sanitize::safeFilename($value), 'Alnum string');

        $value = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.pdf';
        $this->assertEquals($value, Sanitize::safeFilename($value), 'Alnum string with .');

        $value = '1ü2ö3ä4Ü5Ö6Ä7';
        $this->assertEquals('1ue2oe3ae4Ue5Oe6Ae7', Sanitize::safeFilename($value), 'Alnum string with umlaut');

        $value = '`~!@#$%^&*()_+=-[]{}\|;:\'"/?.> ,<`';
        $this->assertEquals('_______________-____________._____', Sanitize::safeFilename($value), 'Alnum string with umlaut');


        $value = '';
        $this->assertEquals($value, Sanitize::safeFilename($value, true), 'Empty string');

        $value = 'test';
        $this->assertEquals('test', Sanitize::safeFilename($value, true));

        $value = 'test,./hello?ö';
        $this->assertEquals('hello_oe', Sanitize::safeFilename($value, true));


        $value = '';
        $this->assertEquals($value, Sanitize::safeFilename($value, false, true), 'Empty string');

        $value = 'test';
        $this->assertEquals('test', Sanitize::safeFilename($value, false, true));

        $value = 'test,./?ö';
        $this->assertEquals('test_./_oe', Sanitize::safeFilename($value, false, true));

    }

    /**
     * Test string, numeric
     *
     */
    public function testDigitCheckAndCleanGet() {

        unset ($_GET[CLIENT_PAGE_LANGUAGE]);
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals('', $_GET[CLIENT_PAGE_LANGUAGE]);

        $_GET[CLIENT_PAGE_LANGUAGE] = '';
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals($_GET[CLIENT_PAGE_LANGUAGE], '');

        $_GET[CLIENT_PAGE_LANGUAGE] = '0';
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals($_GET[CLIENT_PAGE_LANGUAGE], '0');

        $_GET[CLIENT_PAGE_LANGUAGE] = '1234';
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals($_GET[CLIENT_PAGE_LANGUAGE], '1234');

        $_GET[CLIENT_PAGE_LANGUAGE] = 'abc';
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals($_GET[CLIENT_PAGE_LANGUAGE], '');

        $_GET[CLIENT_PAGE_LANGUAGE] = '54abc';
        Sanitize::digitCheckAndCleanGet(CLIENT_PAGE_LANGUAGE);
        $this->assertEquals($_GET[CLIENT_PAGE_LANGUAGE], '5');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testTypeMessageViolate() {

        // Default
        $result = Sanitize::sanitize('badstring', SANITIZE_ALLOW_DIGIT);
        $this->assertEquals('!!digit!!', $result);

        // SANITIZE_TYPE_MESSAGE_VIOLATE_CLASS
        $result = Sanitize::sanitize('badstring', SANITIZE_ALLOW_DIGIT, '', '', SANITIZE_EMPTY_STRING,
            '', SANITIZE_TYPE_MESSAGE_VIOLATE_CLASS);
        $this->assertEquals('!!digit!!', $result);

        // SANITIZE_TYPE_MESSAGE_VIOLATE_EMPTY
        $result = Sanitize::sanitize('badstring', SANITIZE_ALLOW_DIGIT, '', '', SANITIZE_EMPTY_STRING,
            '', SANITIZE_TYPE_MESSAGE_VIOLATE_EMPTY);
        $this->assertEquals('', $result);

        // SANITIZE_TYPE_MESSAGE_VIOLATE_ZERO
        $result = Sanitize::sanitize('badstring', SANITIZE_ALLOW_DIGIT, '', '', SANITIZE_EMPTY_STRING,
            '', SANITIZE_TYPE_MESSAGE_VIOLATE_ZERO);
        $this->assertEquals('0', $result);

        // SANITIZE_TYPE_MESSAGE_VIOLATE_ ... custom
        $result = Sanitize::sanitize('badstring', SANITIZE_ALLOW_DIGIT, '', '', SANITIZE_EMPTY_STRING,
            '', 'custom message');
        $this->assertEquals('custom message', $result);

    }
}
