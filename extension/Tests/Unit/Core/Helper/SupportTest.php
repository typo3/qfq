<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/28/16
 * Time: 9:58 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Helper;


use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class FakeTSFE
 * @package qfq
 */
class FakeTSFE {
    public $id = 1;
    public $type = 2;
    public $sys_language_uid = 3;
}

/**
 * Class SupportTest
 * @package qfq
 */
class SupportTest extends TestCase {

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAppendTypo3ParameterToArray() {

        unset($_GET);

        $this->store->setVar(TYPO3_PAGE_ID, 1, STORE_TYPO3, true);
        $this->store->setVar(TYPO3_PAGE_TYPE, 2, STORE_TYPO3, true);
        $this->store->setVar(TYPO3_PAGE_LANGUAGE, 3, STORE_TYPO3, true);
        $queryArray = array();
        Support::appendTypo3ParameterToArray($queryArray);
        $this->assertEquals(['id' => 1, 'type' => 2, 'L' => 3], $queryArray);

        $queryArray = array();
        $this->store->setVar(TYPO3_PAGE_LANGUAGE, 0, STORE_TYPO3, true);
        Support::appendTypo3ParameterToArray($queryArray);
        $this->assertEquals(['id' => 1, 'type' => 2], $queryArray);

        $queryArray = array();
        $this->store->setVar(TYPO3_PAGE_TYPE, 0, STORE_TYPO3, true);
        Support::appendTypo3ParameterToArray($queryArray);
        $this->assertEquals(['id' => 1], $queryArray);

        $queryArray = ['a' => '100'];
        $expected = ['a' => '100', 'id' => 1];
        Support::appendTypo3ParameterToArray($queryArray);
        $this->assertEquals($expected, $queryArray);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testappendTypo3ParameterUrl() {

        $this->assertEquals('', Support::appendTypo3ParameterToUrl(''));
        $this->assertEquals('http://example.com', Support::appendTypo3ParameterToUrl('http://example.com'));
        $this->assertEquals('https://example.com', Support::appendTypo3ParameterToUrl('https://example.com'));

        $this->store->setVar(TYPO3_PAGE_ID, 1, STORE_TYPO3, true);
        $this->store->setVar(TYPO3_PAGE_TYPE, 0, STORE_TYPO3, true);
        $this->store->setVar(TYPO3_PAGE_LANGUAGE, 0, STORE_TYPO3, true);

        // Kein type, L
        $url = '?id=1';
        $this->assertEquals($url, Support::appendTypo3ParameterToUrl($url));
        $url = '?id=1&type=0';
        $this->assertEquals($url, Support::appendTypo3ParameterToUrl($url));
        $url = '?id=1&type=1';
        $this->assertEquals($url, Support::appendTypo3ParameterToUrl($url));

        $url = '?id=1&L=0';
        $this->assertEquals($url, Support::appendTypo3ParameterToUrl($url));
        $url = '?id=1&L=1';
        $this->assertEquals($url, Support::appendTypo3ParameterToUrl($url));

        // type und L given
        $this->store->setVar(TYPO3_PAGE_TYPE, 2, STORE_TYPO3, true);
        $this->store->setVar(TYPO3_PAGE_LANGUAGE, 3, STORE_TYPO3, true);

        $url = '?id=1';
        $this->assertEquals($url . '&type=2&L=3', Support::appendTypo3ParameterToUrl($url));
        $url = '?id=1&type=2';
        $this->assertEquals($url . '&L=3', Support::appendTypo3ParameterToUrl($url));
        $url = '?id=1&L=3';
        $this->assertEquals($url . '&type=2', Support::appendTypo3ParameterToUrl($url));

    }


    public function testGetFormLogFileName() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     *
     */
    public function testArrayToQueryString() {
        $actual = Support::arrayToQueryString(array());
        $expected = '';
        $this->assertEquals($expected, $actual);

        $actual = Support::arrayToQueryString(['id', 'name']);
        $expected = '';
        $this->assertEquals('id&name', $actual);

        $actual = Support::arrayToQueryString(['id' => '55', 'name']);
        $expected = '';
        $this->assertEquals('id=55&name', $actual);

        $actual = Support::arrayToQueryString(['id' => '55', 'name' => 'Heinz']);
        $expected = '';
        $this->assertEquals('id=55&name=Heinz', $actual);

        $actual = Support::arrayToQueryString(['id' => '55', 'name' => 'He<b>inz']);
        $expected = '';
        $this->assertEquals('id=55&name=He%3Cb%3Einz', $actual);
    }


    /**
     *
     */
    public function testWrapTag() {
//        $build = new BuildFormPlain(array(), array(), array());

        $result = Support::wrapTag("", '');
        $this->assertEquals('', $result);

        $result = Support::wrapTag(" ", '');
        $this->assertEquals('', $result);

        $result = Support::wrapTag("", ' ');
        $this->assertEquals(' ', $result);

        $result = Support::wrapTag(" ", ' ');
        $this->assertEquals(' ', $result);

        $result = Support::wrapTag("", '', true);
        $this->assertEquals('', $result);

        $result = Support::wrapTag(" ", '', true);
        $this->assertEquals('', $result);

        $result = Support::wrapTag("", ' ', true);
        $this->assertEquals(' ', $result);

        $result = Support::wrapTag(" ", ' ', true);
        $this->assertEquals(' ', $result);

        $result = Support::wrapTag("<p>", '', true);
        $this->assertEquals('', $result);

        $result = Support::wrapTag("<p>", '', false);
        $this->assertEquals('<p></p>', $result);

        $result = Support::wrapTag("<p> ", '', true);
        $this->assertEquals('', $result);

        $result = Support::wrapTag(" <p>", '', false);
        $this->assertEquals('<p></p>', $result);

        $result = Support::wrapTag("<p class='test'>", 'Hello World', false);
        $this->assertEquals("<p class='test'>Hello World</p>", $result);

        $result = Support::wrapTag("<p class='test'>", 'Hello World', true);
        $this->assertEquals("<p class='test'>Hello World</p>", $result);

        $result = Support::wrapTag("<p class='test'>", '', false);
        $this->assertEquals("<p class='test'></p>", $result);

        $result = Support::wrapTag("<p class='test'>", '', true);
        $this->assertEquals('', $result);

        $result = Support::wrapTag("<p>", 'Hello World', true);
        $this->assertEquals('<p>Hello World</p>', $result);

        $result = Support::wrapTag("<p>", 'Hello World', false);
        $this->assertEquals('<p>Hello World</p>', $result);

        $result = Support::wrapTag("<p><i>", 'Hello World', false);
        $this->assertEquals('<p><i>Hello World</i></p>', $result);

        $result = Support::wrapTag("  <p>  <i>  ", 'Hello World', false);
        $this->assertEquals('<p><i>Hello World</i></p>', $result);

        $result = Support::wrapTag("<p><i><strong>", 'Hello World', false);
        $this->assertEquals('<p><i><strong>Hello World</strong></i></p>', $result);

        $result = Support::wrapTag("<p class='test'><i>", 'Hello World', false);
        $this->assertEquals("<p class='test'><i>Hello World</i></p>", $result);

        $result = Support::wrapTag("<p class='test1'><i class='test2'><strong class='test3'>", 'Hello World', false);
        $this->assertEquals("<p class='test1'><i class='test2'><strong class='test3'>Hello World</strong></i></p>", $result);

    }

    public function testRenderGlyphIcon() {
        // TBD
        $this->assertEquals(1, 1);

    }

    /**
     *
     */
    public function testUnWrapTag() {
        $this->assertEquals('', Support::unWrapTag('', ''));
        $this->assertEquals('', Support::unWrapTag('<p>', ''));
        $this->assertEquals('Hello World', Support::unWrapTag('<p>', 'Hello World'));
        $this->assertEquals('Hello World', Support::unWrapTag('<p>', '<p>Hello World</p>'));
    }

    public function testDoTooltip() {
        // TBD
        $this->assertEquals(1, 1);
    }

    public function testDoAttribute() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @throws \CodeException
     */
    public function testEscapeDoubleTick() {

        // PLAIN

        // empty string
        $new = Support::escapeDoubleTick('');
        $this->assertEquals('', $new);

        // nothing to replace
        $new = Support::escapeDoubleTick('hello world');
        $this->assertEquals('hello world', $new);

        // last word
        $new = Support::escapeDoubleTick('hello "world"');
        $this->assertEquals('hello \\"world\\"', $new);

        // first word
        $new = Support::escapeDoubleTick('"hello" world');
        $this->assertEquals('\\"hello\\" world', $new);

        // three double tick
        $new = Support::escapeDoubleTick('"""');
        $this->assertEquals('\\"\\"\\"', $new);

        // just "
        $new = Support::escapeDoubleTick('"');
        $this->assertEquals('\\"', $new);

        // already ESCAPED

        // just \"
        $new = Support::escapeDoubleTick('\\"');
        $this->assertEquals('\\"', $new);

        // already escaped: middle
        $new = Support::escapeDoubleTick('hello \\"T world');
        $this->assertEquals('hello \\"T world', $new);

        // already escaped: start
        $new = Support::escapeDoubleTick('\\"T hello world');
        $this->assertEquals('\\"T hello world', $new);

        // already escaped: end
        $new = Support::escapeDoubleTick('hello world \\"');
        $this->assertEquals('hello world \\"', $new);

        // three double tick
        $new = Support::escapeDoubleTick('\\"\\"\\"');
        $this->assertEquals('\\"\\"\\"', $new);
    }

    /**
     * @throws \CodeException
     */
    public function testInsertAttribute() {

        $new = Support::insertAttribute('<i>', 'class', 'qfq');
        $this->assertEquals('<i class="qfq" >', $new);

        $new = Support::insertAttribute('<div>', 'class', 'qfq');
        $this->assertEquals('<div class="qfq" >', $new);

        $new = Support::insertAttribute(' <div> ', 'class', 'qfq');
        $this->assertEquals('<div class="qfq" >', $new);

        $new = Support::insertAttribute('<div >', 'class', 'qfq');
        $this->assertEquals('<div class="qfq" >', $new);

        $new = Support::insertAttribute('<div class="123">', 'class', 'qfq');
        $this->assertEquals('<div class="qfq" class="123">', $new);
    }

    public function testFindInSet() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @throws \UserFormException
     */
    public function testDateTimeGermanToInternational() {

        // date
        $this->assertEquals('0000-00-00 00:00:00', Support::dateTimeGermanToInternational(''));

        $this->assertEquals('2069-12-31 00:00:00', Support::dateTimeGermanToInternational('31.12.69'));
        $this->assertEquals('1970-01-01 00:00:00', Support::dateTimeGermanToInternational('1.1.70'));
        $this->assertEquals('1999-02-01 00:00:00', Support::dateTimeGermanToInternational('01.02.99'));
        $this->assertEquals('2079-02-01 00:00:00', Support::dateTimeGermanToInternational('1.2.2079'));
        $this->assertEquals('2079-02-01 00:00:00', Support::dateTimeGermanToInternational('01.02.2079'));

        $this->assertEquals('1234-01-02 00:00:00', Support::dateTimeGermanToInternational('1234-01-02'));

        // time
        $this->assertEquals('0000-00-00 03:04:00', Support::dateTimeGermanToInternational('3:4'));
        $this->assertEquals('0000-00-00 03:04:00', Support::dateTimeGermanToInternational('03:04'));
        $this->assertEquals('0000-00-00 03:14:00', Support::dateTimeGermanToInternational('3:14'));
        $this->assertEquals('0000-00-00 13:14:00', Support::dateTimeGermanToInternational('13:14'));
        $this->assertEquals('0000-00-00 13:14:01', Support::dateTimeGermanToInternational('13:14:1'));
        $this->assertEquals('0000-00-00 13:14:41', Support::dateTimeGermanToInternational('13:14:41'));

        // date time
        $this->assertEquals('2069-02-01 01:02:00', Support::dateTimeGermanToInternational('1.2.69 1:2'));
        $this->assertEquals('2016-12-31 23:48:59', Support::dateTimeGermanToInternational('31.12.2016 23:48:59'));
        $this->assertEquals('2016-12-31 23:48:59', Support::dateTimeGermanToInternational('2016-12-31 23:48:59'));
    }

    /**
     */
    public function testDateTimeGermanToInternationalException01() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException02() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException03() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException04() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException05() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException06() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.1.');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException07() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.1.1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException08() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('123.1.11');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException09() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.123.11');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException10() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.123');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException11() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1.1.12345');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException12() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1-01-01');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException13() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('12-01-01');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException14() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('123-01-01');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException15() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('12345-01-01');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException16() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1234-1-01');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException17() {
        $this->expectException(\UserFormException::class);
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1234-01-1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException18() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1:');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException19() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1:1:');
    }


    /**
     */
    public function testDateTimeGermanToInternationalException20() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1:1:1:1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException21() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('123:1:1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException22() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1:123:1');
    }

    /**
     */
    public function testDateTimeGermanToInternationalException23() {
        $this->expectException(\UserFormException::class);
        Support::dateTimeGermanToInternational('1:1:123');
    }

    /**
     * @return void
     */
    public function testDateTimeRegexp() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     */
    public function testConvertDateTime() {

//        $this->expectException(\UserFormException::class);
        $dateFormat = FORMAT_DATE_INTERNATIONAL;

        $value = '';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0000-00-00';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '00.00.0000';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

//        $value='00:00';
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
//        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
//        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

//        $value='0:0';
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
//        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
//        $this->assertEquals('0000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('0000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('0000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0.0.00';
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('2000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('2000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '00-0-0';
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('2000-00-00', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('2000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('2000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $dateFormat = FORMAT_DATE_GERMAN;

        $value = '';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0000-00-00';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '00.00.0000';
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

//        $value='00:00';
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
//        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
//        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

//        $value='0:0';
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
//        $this->assertEquals('', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
//        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
//        $this->assertEquals('00.00.0000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('00.00.0000 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('00.00.0000 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '0.0.00';
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('2000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('2000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        $value = '00-0-0';
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 0, 1));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 1, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 0, 1, 1));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 1, 0, 0));
        $this->assertEquals('00.00.2000', Support::convertDateTime($value, $dateFormat, 1, 0, 1));
//        $this->assertEquals('2000-00-00 00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 0));
//        $this->assertEquals('2000-00-00 00:00:00', Support::convertDateTime($value, $dateFormat, 1, 1, 1));

        # ----
        # real dates

        $dateFormat = FORMAT_DATE_GERMAN;

        $value = '01.02.3000';
        $this->assertEquals('01.02.3000', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01.02.3000 ';
        $this->assertEquals('01.02.3000', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01.02.3000 00:00';
        $this->assertEquals('01.02.3000 00:00', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01.02.34';
        $this->assertEquals('01.02.2034', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '1.02.34';
        $this->assertEquals('01.02.2034', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01.2.34';
        $this->assertEquals('01.02.2034', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '1.2.34';
        $this->assertEquals('01.02.2034', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        #-----
        # time
        $value = '01:23';
        $this->assertEquals('01:23', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01:23:45';
        $this->assertEquals('01:23', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '01:23:45';
        $this->assertEquals('01:23:45', Support::convertDateTime($value, $dateFormat, 0, 0, 1));

        $value = '1:23:45';
        $this->assertEquals('01:23:45', Support::convertDateTime($value, $dateFormat, 0, 0, 1));

        $value = '01:3:45';
        $this->assertEquals('01:03:45', Support::convertDateTime($value, $dateFormat, 0, 0, 1));

        $value = '01:03:4';
        $this->assertEquals('01:03:04', Support::convertDateTime($value, $dateFormat, 0, 0, 1));

        $value = '1:3:4';
        $this->assertEquals('01:03:04', Support::convertDateTime($value, $dateFormat, 0, 0, 1));

        # -----
        # date & time
        $value = '01.02.2034 11:23';
        $this->assertEquals('01.02.2034 11:23', Support::convertDateTime($value, $dateFormat, 0, 0, 0));

        $value = '1.2.34 1:3';
        $this->assertEquals('01.02.2034 01:03', Support::convertDateTime($value, $dateFormat, 0, 0, 0));
    }

    /**
     * @return void
     */
    public function testDateTimeZero() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @return void
     */
    public function testSplitDateToArray() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @return void
     */
    public function testGetDateTimePlaceholder() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @return void
     */
    public function testEncryptDoubleCurlyBraces() {
#/+open+/#
#/+close+/#


        $arr = [
            ['', ''],
            ['1', '1'],
            ["1\n2", "1\n2"],
            ['{', '{'],
            ['#/+open+/#', '{{'],
            ['-\{-', '-\{-'],
            ['#/+open+/##/+close+/#-#/+open+/##/+close+/#', '{{}}-{{}}'],
            ['#/+open+/#hello#/+open+/#world#/+close+/##/+close+/#', '{{hello{{world}}}}'],
            ["\n\n##/+open+/#\n#/+close+/#", "\n\n#{{\n}}"],
        ];

        foreach ($arr as $tuple) {
            $this->assertEquals($tuple[0], Support::encryptDoubleCurlyBraces($tuple[1]));
            $this->assertEquals($tuple[1], Support::decryptDoubleCurlyBraces($tuple[0]));
        }
    }

    /**
     * @return void
     */
    public function testRandomAlphaNum() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     *
     */
    public function testConcatUrlParam() {
        $url = Support::concatUrlParam('', '');
        $this->assertEquals('', $url);

        $url = Support::concatUrlParam('', []);
        $this->assertEquals('', $url);

        $url = Support::concatUrlParam('', ['']);
        $this->assertEquals('', $url);

        $url = Support::concatUrlParam('http://example.com', '');
        $this->assertEquals('http://example.com', $url);

        $url = Support::concatUrlParam('http://example.com', []);
        $this->assertEquals('http://example.com', $url);

        $url = Support::concatUrlParam('http://example.com', ['']);
        $this->assertEquals('http://example.com', $url);

        $url = Support::concatUrlParam('', 'a=1');
        $this->assertEquals('?a=1', $url);

        $url = Support::concatUrlParam('', ['a=1']);
        $this->assertEquals('?a=1', $url);

        $url = Support::concatUrlParam('http://example.com', 'a=100');
        $this->assertEquals('http://example.com?a=100', $url);

        $url = Support::concatUrlParam('http://example.com', ['a=100']);
        $this->assertEquals('http://example.com?a=100', $url);

        $url = Support::concatUrlParam('http://example.com?id=2', 'a=100');
        $this->assertEquals('http://example.com?id=2&a=100', $url);

        $url = Support::concatUrlParam('http://example.com?id=2', ['a=100']);
        $this->assertEquals('http://example.com?id=2&a=100', $url);

        $url = Support::concatUrlParam('http://example.com', 'a=100&b=201');
        $this->assertEquals('http://example.com?a=100&b=201', $url);

        $url = Support::concatUrlParam('http://example.com', ['a=100&b=201']);
        $this->assertEquals('http://example.com?a=100&b=201', $url);

        $url = Support::concatUrlParam('http://example.com', ['a=100', 'b=201']);
        $this->assertEquals('http://example.com?a=100&b=201', $url);

        $url = Support::concatUrlParam('http://example.com?id=34', 'a=100&b=201');
        $this->assertEquals('http://example.com?id=34&a=100&b=201', $url);

        $url = Support::concatUrlParam('http://example.com?id=34', ['a=100&b=201']);
        $this->assertEquals('http://example.com?id=34&a=100&b=201', $url);

        $url = Support::concatUrlParam('http://example.com?id=34', ['a=100', 'b=201']);
        $this->assertEquals('http://example.com?id=34&a=100&b=201', $url);
    }

    /**
     * @throws \CodeException
     */
    public function testMergeUrlComponents() {
        $this->assertEquals('', Support::mergeUrlComponents('', '', ''));

        $this->assertEquals('example.com/', Support::mergeUrlComponents('example.com', '', ''));
        $this->assertEquals('example.com/', Support::mergeUrlComponents('example.com/', '', ''));

        $this->assertEquals('http://example.com/', Support::mergeUrlComponents('http://example.com', '', ''));

        $this->assertEquals('example.com/sub', Support::mergeUrlComponents('example.com', 'sub', ''));
        $this->assertEquals('example.com/sub', Support::mergeUrlComponents('example.com/', 'sub', ''));
        $this->assertEquals('example.com/sub', Support::mergeUrlComponents('example.com', '/sub', ''));
        $this->assertEquals('example.com/sub', Support::mergeUrlComponents('example.com/', '/sub', ''));
        $this->assertEquals('example.com/', Support::mergeUrlComponents('example.com/', '/', ''));

        $this->assertEquals('example.com/sub/sub2', Support::mergeUrlComponents('example.com/sub', 'sub2', ''));
        $this->assertEquals('example.com/sub/sub2', Support::mergeUrlComponents('example.com/sub/', 'sub2', ''));
        $this->assertEquals('example.com/sub/sub2', Support::mergeUrlComponents('example.com/sub', '/sub2', ''));
        $this->assertEquals('example.com/sub/sub2', Support::mergeUrlComponents('example.com/sub/', '/sub2', ''));
        $this->assertEquals('http://example.com/sub', Support::mergeUrlComponents('http://example.com', 'sub', ''));
        $this->assertEquals('http://example.com/sub', Support::mergeUrlComponents('http://example.com/', 'sub', ''));
        $this->assertEquals('http://example.com/sub', Support::mergeUrlComponents('http://example.com/', '/sub', ''));
        $this->assertEquals('http://example.com/sub', Support::mergeUrlComponents('http://example.com', '/sub', ''));

        $this->assertEquals('sub', Support::mergeUrlComponents('', 'sub', ''));
        $this->assertEquals('/sub', Support::mergeUrlComponents('', '/sub', ''));
        $this->assertEquals('/sub/', Support::mergeUrlComponents('', '/sub/', ''));
        $this->assertEquals('/', Support::mergeUrlComponents('', '/', ''));

        $this->assertEquals('example.com/sub?id=1', Support::mergeUrlComponents('example.com', 'sub', 'id=1'));
        $this->assertEquals('example.com/sub?id=1', Support::mergeUrlComponents('example.com', 'sub', '?id=1'));
        $this->assertEquals('example.com/sub/?id=1', Support::mergeUrlComponents('example.com', 'sub/', 'id=1'));
        $this->assertEquals('example.com/sub/?id=1', Support::mergeUrlComponents('example.com', 'sub/', '?id=1'));

        $this->assertEquals('sub?id=1', Support::mergeUrlComponents('', 'sub', 'id=1'));
        $this->assertEquals('/sub?id=1', Support::mergeUrlComponents('', '/sub', '?id=1'));
        $this->assertEquals('sub/?id=1', Support::mergeUrlComponents('', 'sub/', 'id=1'));
        $this->assertEquals('/sub/?id=1', Support::mergeUrlComponents('', '/sub/', '?id=1'));

        $this->assertEquals('example.com/sub/index.php?id=1', Support::mergeUrlComponents('example.com', 'sub/index.php', 'id=1'));
        $this->assertEquals('example.com/sub/index.php?id=1', Support::mergeUrlComponents('example.com', 'sub/index.php', '?id=1'));

        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com', '', 'id=1'));
        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com', '', '?id=1'));

        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com/', '', 'id=1'));
        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com/', '', '?id=1'));

        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com', '/', 'id=1'));
        $this->assertEquals('example.com/?id=1', Support::mergeUrlComponents('example.com', '/', '?id=1'));

        $this->assertEquals('?', Support::mergeUrlComponents('', '', '?'));
        $this->assertEquals('?id=1', Support::mergeUrlComponents('', '', 'id=1'));
        $this->assertEquals('?id=1', Support::mergeUrlComponents('', '', '?id=1'));

        $this->assertEquals('', Support::mergeUrlComponents('', '', 'index.php'));
        $this->assertEquals('?', Support::mergeUrlComponents('', '', 'index.php?'));
        $this->assertEquals('?id=1', Support::mergeUrlComponents('', '', 'index.php?id=1'));

    }

    /**
     */
    public function testMergeUrlComponentsException() {
        $this->expectException(\CodeException::class);
        Support::mergeUrlComponents('', '', 'sub?id=1');
    }

    /**
     * @return void
     */
    public function testSetFeDefaults() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     */
    public function testAdjustFeToColumnDefinition() {

        // Test CheckType Auto
        $formElementTemplate = [
            FE_NAME => 'feTest',
            FE_MAX_LENGTH => '',
            FE_ENCODE => FE_ENCODE_SPECIALCHAR,
            FE_CHECK_TYPE => SANITIZE_ALLOW_AUTO,
            FE_TYPE => FE_TYPE_TEXT,
            FE_INPUT_TYPE => '',
            FE_MIN => '',
            FE_MAX => '',
        ];

        $formElement = $formElementTemplate;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'int(11) not null');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_NUMERICAL;
        $expected[FE_INPUT_TYPE] = 'number';
        $expected[FE_MIN] = -2147483648;
        $expected[FE_MAX] = 2147483647;
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '11';
        }
        $this->assertEquals($expected, $formElement, "CheckType Auto should switch to numerical for signed int");

        $formElement = $formElementTemplate;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'int(11) unsigned not null');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_DIGIT;
        $expected[FE_INPUT_TYPE] = 'number';
        $expected[FE_MIN] = 0;
        $expected[FE_MAX] = 4294967295;
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '11';
        }

        $this->assertEquals($expected, $formElement, "CheckType Auto should switch to digit for unsigned int");

        $formElement = $formElementTemplate;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'decimal(10,2) not null');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_NUMERICAL;
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '13';
        }
        $this->assertEquals($expected, $formElement, "CheckType Auto should switch to numerical for decimal");

        $formElement = $formElementTemplate;
        $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALNUMX;
        Support::adjustFeToColumnDefinition($formElement, 'int(11) not null');
        $this->assertEquals(SANITIZE_ALLOW_ALNUMX, $formElement[FE_CHECK_TYPE], "CheckType should not switch if set to non-auto");

        $formElement = $formElementTemplate;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'varchar(255)');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALL;
        $expected[FE_MAX_LENGTH] = '255';
//        $expected[FE_MAX_LENGTH] = '';
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '255';
        }
        $this->assertEquals($expected, $formElement, "Checktype Auto should switch to all for text with encode=specialchars");

        $formElement = $formElementTemplate;
        $formElement[FE_ENCODE] = FE_ENCODE_NONE;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'varchar(64)');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALNUMX;
        $expected[FE_MAX_LENGTH] = 64;
//        $expected[FE_MAX_LENGTH] = '';
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '64';
        }
        $this->assertEquals($expected, $formElement, "Checktype Auto should switch to alnumx for text with encode=none");

        $formElement = $formElementTemplate;
        $formElement[FE_TYPEAHEAD_SQL] = "SELECT chocolate WHERE tastes IS good";
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'int(11) not null');
        $expected[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALNUMX;
        $expected[FE_MIN] = -2147483648;
        $expected[FE_MAX] = 2147483647;
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = '11';
        }
        $this->assertEquals($expected, $formElement, "Checktype Auto should switch to alnumx if typeAhead is defined");

        $formElement = $formElementTemplate;
        $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_ALNUMX;
        $expected = $formElement;
        Support::adjustFeToColumnDefinition($formElement, 'datetime');
        $expected[FE_MAX_LENGTH] = strlen('0000/00/00 00:00:00');
        if (isset($formElement[FE_MAX_LENGTH_COLUMN])) {
            $expected[FE_MAX_LENGTH_COLUMN] = strlen('0000/00/00 00:00:00');
        }
        $this->assertEquals($expected, $formElement, "maxLength for datetime should be correct");
    }

    /**
     *
     */
    public function testSetIfNotSet() {
        $new = array();

        Support::setIfNotSet($new, 'id');
        $this->assertEquals(['id' => ''], $new);

        $new = array();
        $new['id'] = 1;
        Support::setIfNotSet($new, 'id');
        $this->assertEquals(['id' => 1], $new);

        $new = array();
        Support::setIfNotSet($new, 'id', 2);
        $this->assertEquals(['id' => 2], $new);

    }

    /**
     * @return void
     */
    public function testExtendFilename() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     *
     */
    public function testJoinPath() {
        $this->assertEquals('', Support::joinPath('', ''));
        $this->assertEquals('etc', Support::joinPath('etc', ''));
        $this->assertEquals('file', Support::joinPath('', 'file'));
        $this->assertEquals('/etc', Support::joinPath('/etc', ''));
        $this->assertEquals('/file', Support::joinPath('', '/file'));

        $this->assertEquals('etc/', Support::joinPath('etc/', ''));
        $this->assertEquals('file/', Support::joinPath('', 'file/'));
        $this->assertEquals('/etc/', Support::joinPath('/etc/', ''));
        $this->assertEquals('/file/', Support::joinPath('', '/file/'));

        $this->assertEquals('etc/file', Support::joinPath('etc', 'file'));
        $this->assertEquals('/etc/file', Support::joinPath('/etc', 'file'));
        $this->assertEquals('/file', Support::joinPath('etc', '/file'));
        $this->assertEquals('/file', Support::joinPath('/etc', '/file'));
        $this->assertEquals('/file', Support::joinPath('/etc/', '/file'));
        $this->assertEquals('/file/', Support::joinPath('/etc/', '/file/'));

        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2', 'file/file2'));
        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2/', 'file/file2'));
        $this->assertEquals('/file/file2', Support::joinPath('/etc/etc2/', '/file/file2'));

        #--
        $this->assertEquals('', Support::joinPath('', '', FILE_PRIORITY));
        $this->assertEquals('etc', Support::joinPath('etc', '', FILE_PRIORITY));
        $this->assertEquals('file', Support::joinPath('', 'file', FILE_PRIORITY));
        $this->assertEquals('/etc', Support::joinPath('/etc', '', FILE_PRIORITY));
        $this->assertEquals('/file', Support::joinPath('', '/file', FILE_PRIORITY));

        $this->assertEquals('etc/', Support::joinPath('etc/', '', FILE_PRIORITY));
        $this->assertEquals('file/', Support::joinPath('', 'file/', FILE_PRIORITY));
        $this->assertEquals('/etc/', Support::joinPath('/etc/', '', FILE_PRIORITY));
        $this->assertEquals('/file/', Support::joinPath('', '/file/', FILE_PRIORITY));

        $this->assertEquals('etc/file', Support::joinPath('etc', 'file', FILE_PRIORITY));
        $this->assertEquals('/etc/file', Support::joinPath('/etc', 'file', FILE_PRIORITY));
        $this->assertEquals('/file', Support::joinPath('etc', '/file', FILE_PRIORITY));
        $this->assertEquals('/file', Support::joinPath('/etc', '/file', FILE_PRIORITY));
        $this->assertEquals('/file', Support::joinPath('/etc/', '/file', FILE_PRIORITY));
        $this->assertEquals('/file/', Support::joinPath('/etc/', '/file/', FILE_PRIORITY));

        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2', 'file/file2', FILE_PRIORITY));
        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2/', 'file/file2', FILE_PRIORITY));
        $this->assertEquals('/file/file2', Support::joinPath('/etc/etc2/', '/file/file2', FILE_PRIORITY));

        #--
        $this->assertEquals('', Support::joinPath('', '', PATH_FILE_CONCAT));
        $this->assertEquals('etc', Support::joinPath('etc', '', PATH_FILE_CONCAT));
        $this->assertEquals('file', Support::joinPath('', 'file', PATH_FILE_CONCAT));
        $this->assertEquals('/etc', Support::joinPath('/etc', '', PATH_FILE_CONCAT));
        $this->assertEquals('/file', Support::joinPath('', '/file', PATH_FILE_CONCAT));

        $this->assertEquals('etc/', Support::joinPath('etc/', '', PATH_FILE_CONCAT));
        $this->assertEquals('file/', Support::joinPath('', 'file/', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/', Support::joinPath('/etc/', '', PATH_FILE_CONCAT));
        $this->assertEquals('/file/', Support::joinPath('', '/file/', PATH_FILE_CONCAT));

        $this->assertEquals('etc/file', Support::joinPath('etc', 'file', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/file', Support::joinPath('/etc', 'file', PATH_FILE_CONCAT));
        $this->assertEquals('etc/file', Support::joinPath('etc', '/file', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/file', Support::joinPath('/etc', '/file', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/file', Support::joinPath('/etc/', '/file', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/file/', Support::joinPath('/etc/', '/file/', PATH_FILE_CONCAT));

        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2', 'file/file2', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2/', 'file/file2', PATH_FILE_CONCAT));
        $this->assertEquals('/etc/etc2/file/file2', Support::joinPath('/etc/etc2/', '/file/file2', PATH_FILE_CONCAT));

    }

    /**
     * @return void
     */
    public function testMoveFile() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     *
     */
    public function testFalseEmptyToZero() {
        $this->assertEquals('0', Support::falseEmptyToZero(''));
        $this->assertEquals('0', Support::falseEmptyToZero(false));
        $this->assertEquals('false', Support::falseEmptyToZero('false'));
        $this->assertEquals('true', Support::falseEmptyToZero('true'));
        $this->assertEquals('123', Support::falseEmptyToZero('123'));
    }

    /**
     *
     */
    public function testIsEnabled() {
        $this->assertEquals(false, Support::isEnabled(array(), ''));
        $this->assertEquals(false, Support::isEnabled(array(), 'test'));
        $this->assertEquals(false, Support::isEnabled(['test' => '0'], 'test'));

        $this->assertEquals(true, Support::isEnabled(['test' => ''], 'test'));
        $this->assertEquals(true, Support::isEnabled(['test' => 'val'], 'test'));
        $this->assertEquals(true, Support::isEnabled(['test' => '1'], 'test'));

        $this->assertEquals(false, Support::isEnabled(['a' => '1', 'b' => '2', 'c' => '3'], 'test'));
        $this->assertEquals(true, Support::isEnabled(['a' => '1', 'b' => '2', 'c' => '3'], 'a'));
        $this->assertEquals(true, Support::isEnabled(['a' => '1', 'b' => '2', 'c' => '3'], 'b'));
        $this->assertEquals(false, Support::isEnabled(['a' => '1', 'b' => '0', 'c' => '3'], 'b'));
    }

    /**
     *
     */
    public function testunEscapeSpaceComment() {

        $this->assertEquals('', Support::handleEscapeSpaceComment(''));
        $this->assertEquals('', Support::handleEscapeSpaceComment('   '));

        $this->assertEquals('Hello world.', Support::handleEscapeSpaceComment('Hello world.'));
        $this->assertEquals('Hello world.', Support::handleEscapeSpaceComment('  Hello world.  '));
        $this->assertEquals('', Support::handleEscapeSpaceComment('#Hello world.'));
        $this->assertEquals('', Support::handleEscapeSpaceComment('  #Hello world.  '));
        $this->assertEquals('', Support::handleEscapeSpaceComment('  # Hello world.  '));
        $this->assertEquals('# Hello world.', Support::handleEscapeSpaceComment('\# Hello world.  '));

        $this->assertEquals('Hello world.  ', Support::handleEscapeSpaceComment('Hello world.  \\'));
        $this->assertEquals('Hello world.  ', Support::handleEscapeSpaceComment('Hello world.  \ '));
        $this->assertEquals(' Hello world.', Support::handleEscapeSpaceComment('\ Hello world.'));
        $this->assertEquals(' Hello world. ', Support::handleEscapeSpaceComment('\ Hello world. \ '));
    }

    /**
     */
    public function testInsertAttributeException1() {
        $this->expectException(\CodeException::class);
        Support::insertAttribute('<>', 'class', 'qfq');
    }


    /**
     * @throws \UserFormException
     */
    public function testHtmlEntityEncodeDecode() {
        $this->assertEquals('', Support::htmlEntityEncodeDecode(MODE_NONE, ''));
        $this->assertEquals('test', Support::htmlEntityEncodeDecode(MODE_NONE, 'test'));
        $this->assertEquals('te"st', Support::htmlEntityEncodeDecode(MODE_NONE, 'te"st'));
        $this->assertEquals("te'st", Support::htmlEntityEncodeDecode(MODE_NONE, "te'st"));
        $this->assertEquals("te's\\'t", Support::htmlEntityEncodeDecode(MODE_NONE, "te's\\'t"));
        $this->assertEquals("te & st", Support::htmlEntityEncodeDecode(MODE_NONE, "te & st"));
        $this->assertEquals("te & &amp; st", Support::htmlEntityEncodeDecode(MODE_NONE, "te & &amp; st"));
        $this->assertEquals("te < st", Support::htmlEntityEncodeDecode(MODE_NONE, "te < st"));
        $this->assertEquals("te > st", Support::htmlEntityEncodeDecode(MODE_NONE, "te > st"));
        $this->assertEquals("te &lt; st", Support::htmlEntityEncodeDecode(MODE_NONE, "te &lt; st"));
        $this->assertEquals("te &gt; st", Support::htmlEntityEncodeDecode(MODE_NONE, "te &gt; st"));

        $this->assertEquals('', Support::htmlEntityEncodeDecode(MODE_ENCODE, ''));
        $this->assertEquals('test', Support::htmlEntityEncodeDecode(MODE_ENCODE, 'test'));
        $this->assertEquals('te&quot;st', Support::htmlEntityEncodeDecode(MODE_ENCODE, 'te"st'));
        $this->assertEquals('te&#039;st', Support::htmlEntityEncodeDecode(MODE_ENCODE, "te'st"));
        $this->assertEquals('te&#039;s\&#039;t', Support::htmlEntityEncodeDecode(MODE_ENCODE, "te's\\'t"));
        $this->assertEquals('te &amp; st', Support::htmlEntityEncodeDecode(MODE_ENCODE, "te & st"));
        $this->assertEquals('te &amp; &amp;amp; st', Support::htmlEntityEncodeDecode(MODE_ENCODE, "te & &amp; st"));
        $this->assertEquals("te &lt; st", Support::htmlEntityEncodeDecode(MODE_ENCODE, "te < st"));
        $this->assertEquals("te &gt; st", Support::htmlEntityEncodeDecode(MODE_ENCODE, "te > st"));
        $this->assertEquals("te &amp;lt; st", Support::htmlEntityEncodeDecode(MODE_ENCODE, "te &lt; st"));
        $this->assertEquals("te &amp;gt; st", Support::htmlEntityEncodeDecode(MODE_ENCODE, "te &gt; st"));

        $this->assertEquals('', Support::htmlEntityEncodeDecode(MODE_DECODE, ''));
        $this->assertEquals('test', Support::htmlEntityEncodeDecode(MODE_DECODE, 'test'));
        $this->assertEquals('te"st', Support::htmlEntityEncodeDecode(MODE_DECODE, 'te"st'));
        $this->assertEquals("te'st", Support::htmlEntityEncodeDecode(MODE_DECODE, "te'st"));
        $this->assertEquals("te's\\'t", Support::htmlEntityEncodeDecode(MODE_DECODE, "te's\\'t"));
        $this->assertEquals('te & st', Support::htmlEntityEncodeDecode(MODE_DECODE, "te & st"));
        $this->assertEquals('te & & st', Support::htmlEntityEncodeDecode(MODE_DECODE, "te & &amp; st"));
        $this->assertEquals("te < st", Support::htmlEntityEncodeDecode(MODE_DECODE, "te &lt; st"));
        $this->assertEquals("te > st", Support::htmlEntityEncodeDecode(MODE_DECODE, "te &gt; st"));
    }

    /**
     *
     */
    public function testReturnBytes() {
        $this->assertEquals('', Support::returnBytes(''));
        $this->assertEquals(0, Support::returnBytes(0));
        $this->assertEquals(1, Support::returnBytes(1));
        $this->assertEquals(1000, Support::returnBytes(1000));

        $this->assertEquals(0, Support::returnBytes('0'));
        $this->assertEquals(1, Support::returnBytes('1'));
        $this->assertEquals(1000, Support::returnBytes('1000'));

        $this->assertEquals(0, Support::returnBytes('0K'));
        $this->assertEquals(1024, Support::returnBytes('1K'));
        $this->assertEquals(1024000, Support::returnBytes('1000K'));

        $this->assertEquals(0, Support::returnBytes('0M'));
        $this->assertEquals(1048576, Support::returnBytes('1M'));
        $this->assertEquals(1048576000, Support::returnBytes('1000M'));

        $this->assertEquals(0, Support::returnBytes('0G'));
        $this->assertEquals(1073741824, Support::returnBytes('1G'));
        $this->assertEquals(1073741824000, Support::returnBytes('1000G'));

        $this->assertEquals(0, Support::returnBytes('0k'));
        $this->assertEquals(1024, Support::returnBytes('1k'));
        $this->assertEquals(1024000, Support::returnBytes('1000k'));

        $this->assertEquals(0, Support::returnBytes('0m'));
        $this->assertEquals(1048576, Support::returnBytes('1m'));
        $this->assertEquals(1048576000, Support::returnBytes('1000m'));

        $this->assertEquals(0, Support::returnBytes('0g'));
        $this->assertEquals(1073741824, Support::returnBytes('1g'));
        $this->assertEquals(1073741824000, Support::returnBytes('1000g'));
    }

    /**
     * @return void
     */
    public function testQfqExec() {
        // TBD
        $this->assertEquals(1, 1);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {
        parent::setUp();

//        Path::setUrlApp(BASE_URL_FAKE);
        $GLOBALS["TSFE"] = new FakeTSFE();
        $this->store = Store::getInstance('', true);
    }

}
