<?php
/**
 * @package qfq
 * @author kputyr
 * @date: 09.11.2023
 */

namespace Unit\Core\Parser;

use IMATHUZH\Qfq\Core\Parser\StringTokenizer;
use PHPUnit\Framework\TestCase;

class StringTokenizerTest extends TestCase {

    protected function assertTokenizer($delimiters, $data, $expected) {
        $parser = new StringTokenizer($delimiters);
        $generator = $parser->tokenized($data);
        foreach ($expected as $data) {
            list($token, $delimiter) = $generator->current();
            if (is_array($data)) {
                $this->assertSame($data[0], $token->value, "Expecting $data[0]");
                $this->assertSame($data[1], $delimiter, "Expecting '$data[1]'");
            } else {
                $this->assertSame($data, $token->value, "Expecting $data");
            }
            $generator->next();
        }
        $this->assertNull($generator->current());
    }

    public function testOffset() {
        $parser = new StringTokenizer(':|');
        $input = 'a:bc:de|f';
        $offsets = [1, 4, 7, null];
        $tokens = $parser->tokenized($input);
        foreach ($offsets as $value) {
            $tokens->current();
            if (!is_null($value)) $this->assertSame($value, $parser->offset());
            $tokens->next();
        }
        $this->assertFalse($tokens->valid());
    }

    public function testEmptyString() {
        $this->assertTokenizer(':,[]{}', '', []);
    }

    public function testSimpleString() {
        $this->assertTokenizer(':,[]{}', 'abc', [['abc', null]]);
    }

    public function testUnescapedDelimiters() {
        $this->assertTokenizer(
            ':,./]{',
            "x:7,y.z/24]{x",
            [['x', ':'], ['7', ','], ['y', '.'], ['z', '/'],
                ['24', ']'], ['', '{'], ['x', null]]
        );
    }

    public function testEscapedDelimiters() {
        $this->assertTokenizer(
            ':,./]{',
            "x\\:7\\,y\\.z\\/24\\]\\{x",
            [["x:7,y.z/24]{x", null]]
        );
    }

    public function testEscapedNonDelimiters() {
        $this->assertTokenizer(
            ':',
            'x\\\\y\\n',
            ['x\\\\y\\n']
        );
    }

    public function testTokensAreTrimmed() {
        $this->assertTokenizer(
            '|',
            ' a|b | cd ',
            ['a', 'b', 'cd']
        );
    }

    public function testSpacesInside() {
        $this->assertTokenizer(
            ':',
            "x y: sy   ca : f\t\ng",
            ['x y', 'sy   ca', "f\t\ng"]
        );
    }

    public function testQuotesAreRemoved() {
        $this->assertTokenizer(
            ':',
            '"x y":ab\'x\'cd',
            ['x y', 'abxcd']
        );
    }

    public function testQuotedSpacesArePreserved() {
        $this->assertTokenizer(
            ':',
            '" "x:ab" x ":\'  \'',
            [' x', 'ab x ', '  ']
        );
    }

    public function testEscapedQuotesArePreserved() {
        $this->assertTokenizer(
            ':',
            '\"x y\":ab\\\'cd',
            ['"x y"', "ab'cd"]
        );
    }

    public function testNestedQuotesAreNotParsed() {
        $this->assertTokenizer(
            ':',
            '"\'":\'"\'',
            ["'", '"']
        );
    }

    public function testQuotedDelimitersAreIgnored() {
        $this->assertTokenizer(
            ':,|',
            'x:a\\|b|c\\,d\\:e:24',
            ['x', 'a|b', 'c,d:e', '24']
        );
    }
}
