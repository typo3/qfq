<?php
/**
 * @package qfq
 * @author kputyr
 * @date: 09.11.2023
 */

namespace Unit\Core\Parser;

use IMATHUZH\Qfq\Core\Parser\MixedTypeParser;
use PHPUnit\Framework\TestCase;

/**
 * Class MixedTypeParserTest
 * @package qfq
 */
class MixedTypeParserTest extends TestCase {

    protected function assertGeneratorOutput($generator, $output) {
        foreach ($output as $value) {
            $data = $generator->current();
            $data[0] = strval($data[0]);
            $this->assertSame($value, $data, "Expecting " . json_encode($value));
            $generator->next();
        }
        $this->assertNull($generator->current());
    }

    public function testLiteralValues() {
        $parser = new MixedTypeParser(null, ['empty' => 18]);
        $data = [
            'abc' => 'abc', ' x\\:y  ' => 'x:y',
            '' => 18, '""' => '',
            ' ' => 18, '" "' => ' ',
            '123' => 123, '"123"' => '123',
            '12.5' => 12.5, '"12.5"' => '12.5',
            'false' => false, '"false"' => 'false',
            'true' => true, '"true"' => 'true',
            'yes' => true, '"yes"' => 'yes',
            'no' => false, '"no"' => 'no',
            'null' => null, '"null"' => 'null'
        ];
        foreach ($data as $input => $expected)
            $this->assertSame($expected, $parser->parse($input));
    }

    public function testParseFlatList() {
        $parser = new MixedTypeParser();
        // Empty list
        $this->assertEquals([], $parser->parseList(''));
        $this->assertEquals([], $parser->parseList('  '));
        // Compact list
        $this->assertEquals(
            ['a', 'b', 'c', 'd'],
            $parser->parseList("a,b,c,d")
        );
        // Spaces are stripped
        $this->assertEquals(
            ['a', 'b', 'c'],
            $parser->parseList("a, b,  c ")
        );
        // Internal spaces are preserved
        $this->assertEquals(
            ['a', 'b c', 'd'],
            $parser->parseList("a, b c , d")
        );
        // Escaped commas are ignored
        $this->assertEquals(
            ['a,b', 'c'],
            $parser->parseList("a\\,b,c")
        );
        // Quoted commas are ignored
        $this->assertEquals(
            ['a, b', 'c'],
            $parser->parseList("'a, b', c")
        );
        // Trailing comma adds an empty element
        $this->assertEquals(
            [null, null, 'c', null],
            $parser->parseList(",,c,")
        );
    }

    public function testParseListValues() {
        $parser = new MixedTypeParser();
        $this->assertEquals([], $parser->parse("[]"));
        $this->assertEquals([], $parser->parse("[ ]"));

        //  $this->assertEquals([''], $parser->parse("['']"));

        $this->assertEquals(
            ['a', 'b', 'c', 'd'],
            $parser->parse("[a,b,c,d]")
        );
        $this->assertEquals(
            ['a', 'b', 'c', 'd'],
            $parser->parse(" [a, b, c ,d] ")
        );

    }

    public function testParseFlatDictionary() {
        $parser = new MixedTypeParser();
        // Empty object
        $this->assertEquals([], $parser->parseDictionary(''));
        $this->assertEquals([], $parser->parseDictionary('   '));
        // Compact dictionary
        $this->assertEquals(
            ['a' => '1', 'b' => '2', 'c' => '3', 'd' => '4'],
            $parser->parseDictionary("a:1,b:2,c:3,d:4")
        );
        // Spaces are stripped
        $this->assertEquals(
            ['a' => '1', 'b' => '2'],
            $parser->parseDictionary(" a : 1, b: 2\n\t ")
        );
        // Internal spaces are preserved
        $this->assertEquals(
            ['a b' => '1', 'b' => '2 3 4'],
            $parser->parseDictionary(" a b: 1, b: 2 3 4")
        );
        // Escaped delimiters are ignored
        $this->assertEquals(
            ['a,b' => '1', 'b' => '2:3,4'],
            $parser->parseDictionary("a\\,b:1,b:2\\:3\\,4")
        );
        // Quoted delimiters are ignored
        $this->assertGeneratorOutput(
            $parser->tokenized("'a:1,b':\"23,4\""),
            [['a:1,b', ':'], ['23,4', null]]
        );
        $this->assertEquals(
            ['a:1,b' => '23,4'],
            $parser->parseDictionary("'a:1,b':\"23,4\"")
        );
        // Trailing commas are ignored
        $this->assertEquals(
            ['a' => 1, 'b' => 2],
            $parser->parseDictionary('a:1, b:2, ')
        );
    }

    public function testParseDictValues() {
        $parser = new MixedTypeParser();
        $this->assertEquals([], $parser->parse("{}"));
        $this->assertEquals([], $parser->parse("{ }"));

        $this->assertEquals(['a' => null], $parser->parse("{a:}"));

        $this->assertEquals(
            ['a' => '1', 'b' => '2', 'c' => '3', 'd' => '4'],
            $parser->parse("{a:1,b:2,c:3,d:4}")
        );

        $this->assertEquals(
            ['a' => '1', 'b' => '2', 'c' => '3', 'd' => '4'],
            $parser->parse(" { a:1, b:2, c : 3 ,d:4 } ")
        );
    }

    public function testParseNestedStructures() {
        $parser = new MixedTypeParser();
        // Nested lists
        $this->assertEquals(
            ['a', 'b', ['ca', 'cb'], 'd'],
            $parser->parse("[a, b, [ca, cb], d]")
        );
        // Dictionary nested in a list
        $this->assertEquals(
            ['a', 'b', ['ca' => '0', 'cb' => '1'], 'd'],
            $parser->parse("[a, b, {ca:0, cb:1}, d]")
        );
        // List nested in a dictionary
        $this->assertEquals(
            ['a' => '0', 'b' => '1', 'c' => ['ca', 'cb'], 'd' => '3'],
            $parser->parse("{a:0, b:1, c:[ca, cb], d:3}")
        );
        // Nested dictionaries
        $this->assertEquals(
            ['a' => '0', 'b' => '1', 'c' => ['ca' => '5', 'cb' => '6'], 'd' => '3'],
            $parser->parse("{a:0, b:1, c:{ca:5, cb:6}, d:3}")
        );
    }

    public function testRestrictedParser() {
        $parser = new MixedTypeParser(',:  {}');
        $this->assertEquals(
            ['a' => '[b]', 'c' => '[d', 'e]' => ''],
            $parser->parse('{a:[b],c:[d,e]}')
        );
    }
}