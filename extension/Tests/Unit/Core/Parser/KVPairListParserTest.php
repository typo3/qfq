<?php
/**
 * @package qfq
 * @author kputyr
 * @date: 09.11.2023
 */

namespace Unit\Core\Parser;

use IMATHUZH\Qfq\Core\Parser\KVPairListParser;
use IMATHUZH\Qfq\Core\Parser\SimpleParser;
use PHPUnit\Framework\TestCase;

class KVPairListParserTest extends TestCase {
    public function testEmptyString() {
        $parser = new KVPairListParser('|', ':');
        $this->assertEquals(
            [],
            $parser->parse('')
        );
    }

    public function testSinglePair() {
        $parser = new KVPairListParser('|', ':');
        $this->assertEquals(['a' => 42], $parser->parse('a:42'));
    }

    public function testSingleKey() {
        $parser = new KVPairListParser('|', ':', [
            SimpleParser::OPTION_KEY_IS_VALUE => true
        ]);
        $this->assertEquals(['a' => 'a'], $parser->parse('a:'));

        $parser = new KVPairListParser('|', ':', [
            SimpleParser::OPTION_KEY_IS_VALUE => false,
            SimpleParser::OPTION_EMPTY_VALUE => 18
        ]);
        $this->assertEquals(['a' => 18], $parser->parse('a:'));
        $this->assertEquals(['a' => 18], $parser->parse('a'));
    }

    public function testSimpleList() {
        $parser = new KVPairListParser('|', ':');
        $this->assertEquals(
            ['ab' => 'x', 'cd' => 'y', 'ef' => 'z'],
            $parser->parse('ab:x|cd:y|ef:z')
        );
    }

    public function testEscapedSeparators() {
        $parser = new KVPairListParser('|', ':');
        $this->assertEquals(
            ['ab' => 'x', 'cd:y|ef' => 'z:a'],
            $parser->parse('ab:x|cd\\:y\\|ef:z\\:a')
        );
    }

    public function testQuotedSeparators() {
        $parser = new KVPairListParser('|', ':');
        $this->assertEquals(
            ['ab' => 'x', 'cd:y| ef' => 'z:a'],
            $parser->parse('ab:x|"cd:y| ef":z":a"')
        );
    }

    public function testIterate() {
        $parser = new KVPairListParser('|', ':');
        $iterator = $parser->iterate('a:1|b:2|c:3');
        $expected = ['a' => 1, 'b' => 2, 'c' => 3];
        foreach ($iterator as $key => $value) {
            $expectedKey = key($expected);
            $expectedValue = current($expected);
            $this->assertSame($expectedKey, $key);
            $this->assertSame($expectedValue, $value);
            next($expected);
        }
        $this->assertFalse(current($expected));
    }
}
