<?php
/**
 * @package qfq
 * @author kputyr
 * @date: 09.11.2023
 */

namespace Unit\Core\Parser;

use IMATHUZH\Qfq\Core\Parser\SimpleParser;
use PHPUnit\Framework\TestCase;

class SimpleParserTest extends TestCase {

    public function testEmptyString() {
        $parser = new SimpleParser(',');
        $this->assertEquals(
            [],
            $parser->parse('')
        );
    }

    public function testSingletons() {
        $parser = new SimpleParser(',');
        $this->assertEquals(['abcd'], $parser->parse('abcd'));
        $this->assertEquals([42], $parser->parse('42'));
    }

    public function testSimpleList() {
        $parser = new SimpleParser(',');
        $this->assertEquals(
            ['ab', 'cd', 'ef'],
            $parser->parse('ab,cd,ef')
        );
        $parser = new SimpleParser('|');
        $this->assertEquals(
            ['ab', 'cd', 'ef'],
            $parser->parse('ab|cd|ef')
        );
        $parser = new SimpleParser('|,.');
        $this->assertEquals(
            ['ab', 'cd', 'ef', 'gh'],
            $parser->parse('ab|cd,ef.gh')
        );
    }

    public function testEscapedSeparators() {
        $parser = new SimpleParser(',');
        $this->assertEquals(
            ['ab', 'cd,ef'],
            $parser->parse('ab,cd\\,ef')
        );
    }

    public function testQuotedSeparators() {
        $parser = new SimpleParser(',');
        $this->assertEquals(
            ['ab', 'cd, ef'],
            $parser->parse('ab,"cd, ef"')
        );
    }

    public function testIterate() {
        $parser = new SimpleParser(',');
        $iterator = $parser->iterate('a,b,c');
        $expected = ['a', 'b', 'c'];
        foreach ($iterator as $value) {
            $this->assertSame(current($expected), $value);
            next($expected);
        }
        $this->assertFalse(current($expected));

    }
}

