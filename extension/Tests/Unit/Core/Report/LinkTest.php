<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 3/31/16
 * Time: 10:06 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Report;

use IMATHUZH\Qfq\Core\Exception\RedirectResponse;
use IMATHUZH\Qfq\Core\Report\Link;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class LinkTest
 * @package qfq
 */
class LinkTest extends TestCase {

    /**
     * @var Sip
     */
    private $sip = null;

    /**
     * @var Store
     */
    private $store = null;

    private $baseUrl = '';

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testUnknownTokenException1() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $link->renderLink('x:hello world');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testUnknownTokenException2() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $link->renderLink('abc:hello world');
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testfillParameter() {

        $args = ['mail' => '',
            'url' => '',
            'page' => '',
            'text' => '',
            'altText' => '',
            'bootstrapButton' => '',
            'image' => '',
            'imageTitle' => '',
            'glyph' => '',
            'glyphTitle' => '',
            'question' => '',
            'target' => '',
            'toolTip' => '',
            'toolTipJs' => '',
            'param' => '',
            'extraContentWrap' => '',
            'mode' => '',
            'downloadElements' => array(),
            'copyToClipBoard' => '',
            NAME_ATTRIBUTE => '',
            NAME_ATTRIBUTE2 => '',
            'render' => 0,
            'picturePositionRight' => 'l',
            'sip' => '0',
            'encryption' => '0',
            'delete' => '',
            'monitor' => '0',
            'linkClass' => '',
            'linkClassDefault' => '',
            'actionDelete' => '',
            'finalHref' => '',
            'finalContent' => '',
            'finalSymbol' => '',
            'finalToolTip' => '',
            'finalClass' => '',
            'finalQuestion' => '',
            'finalThumbnail' => '',
            'beforeLink' => '',
            'afterLink' => '',
            'orderText' => '',
            'finalButtonId' => '',
            'finalJs' => '',
            'finalIsLink' => true,
            'finalHttpCode' => HTTP_REDIRECT_DEFAULT,
            FINAL_RENDER_HREF => true
        ];

        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $rcTokenGiven = array();

        // Empty definition
        $expect = $args;
        $result = $link->fillParameter(array(), '', $rcTokenGiven);
        $this->assertEquals($expect, $result);

        // 1 Parameter
        $expect = $args;
        $expect['page'] = $this->baseUrl . 'page1';
        $result = $link->fillParameter(array(), 'p:page1', $rcTokenGiven);
        $this->assertEquals($expect, $result);

        // 1 Parameter, 1 Default
        $expect = $args;
        $expect['page'] = $this->baseUrl . 'page1';
        $expect['text'] = 'comment';
        $result = $link->fillParameter(array(), 'p:page1', $rcTokenGiven, 't:comment');
        $this->assertEquals($expect, $result);

        // 1 Parameter, 1 Default
        $expect = $args;
        $expect['page'] = $this->baseUrl . 'page1';
        $expect['text'] = 'comment';
        $expect['sip'] = '1';
        $result = $link->fillParameter(array(), 'p:page1|t:comment', $rcTokenGiven, 'p:page1|t:comment|s');
        $this->assertEquals($expect, $result);

        $expect = $args;
//        $expect['url'] = '' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php';
//        $expect['url'] = $this->baseUrl . 'typo3conf/ext/qfq/Classes/Api/download.php';
        $expect['url'] = 'typo3conf/ext/qfq/Classes/Api/download.php';
        $expect['text'] = 'text';
        $expect['bootstrapButton'] = '0';
        $expect['glyph'] = 'glyphicon-file';
        $expect['glyphTitle'] = 'Download';
        $expect['extraContentWrap'] = '<span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="#downloadPopupReplaceTitle#" data-text="#downloadPopupReplaceText#" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));">';
//        $expect['downloadElements'] = ['p:id=page1'];
        $expect['downloadElements'] = ['p:page1'];
        $expect['sip'] = '1';
        $expect['linkClassDefault'] = 'no_class';
        $expect['_exportFilename'] = 'download.pdf';

        $result = $link->fillParameter(array(), 'p:page1|t:text', $rcTokenGiven, 'd:download.pdf');

        // unit test behaves different on webwork16/crose/qfq and gitlab runner: so remove problematic BASE_DIR_FAKE
//        $result = str_replace(BASE_DIR_FAKE, '', $result);
        $result['url'] = preg_replace('/.*\/typo3conf\//', 'typo3conf/', $result['url']);

        $this->assertEquals($expect, $result);

        $expect['mode'] = 'file';
//        $expect['downloadElements'] = ['p:id=page1', 'F:file.pdf'];
        $expect['downloadElements'] = ['p:page1', 'F:file.pdf'];
        $result = $link->fillParameter(array(), 'p:page1|t:text|d:download.pdf', $rcTokenGiven, 's|M:file|d|F:file.pdf');
        // unit test behaves different on webwork16/crose/qfq and gitlab runner: so remove problematic BASE_DIR_FAKE
//        $result = str_replace(BASE_DIR_FAKE, '', $result);
        $result['url'] = preg_replace('/.*\/typo3conf\//', 'typo3conf/', $result['url']);
        $this->assertEquals($expect, $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkUrlBasic() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        Store::setVar(TYPO3_PAGE_ID, 'firstPage', STORE_TYPO3);

        $result = $link->renderLink('');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com?id=100&t=2&param=hello');
        $this->assertEquals('<a href="http://example.com?id=100&t=2&param=hello" >http://example.com?id=100&t=2&param=hello</a>', $result);

        $result = $link->renderLink('u:example.com');
        $this->assertEquals('<a href="example.com" >example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com|t:Hello world');
        $this->assertEquals('<a href="http://example.com" >Hello world</a>', $result);

        $result = $link->renderLink('u:http://example.com?id=100&t=2&param=hello|t:Hello world');
        $this->assertEquals('<a href="http://example.com?id=100&t=2&param=hello" >Hello world</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \DbException
     */
    public function testLinkUrlBasicExceptionDouble() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $link->renderLink('u:http://example.com|u:http://new.org');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkPageBasic() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        Store::setVar(TYPO3_PAGE_SLUG, 'firstPage', STORE_TYPO3);

        $result = $link->renderLink('p');
        $this->assertEquals('<a href="' . $this->baseUrl . 'firstPage" >' . $this->baseUrl . 'firstPage</a>', $result);

        $result = $link->renderLink('p:');
        $this->assertEquals('<a href="' . $this->baseUrl . 'firstPage" >' . $this->baseUrl . 'firstPage</a>', $result);

        $result = $link->renderLink('p:secondPage');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >' . $this->baseUrl . 'secondPage</a>', $result);

        $result = $link->renderLink('p:id=secondPage');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >' . $this->baseUrl . 'secondPage</a>', $result);

        $result = $link->renderLink('p:id=secondPage&id=100&t=2&param=hello');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage?id=100&t=2&param=hello" >' . $this->baseUrl . 'secondPage?id=100&t=2&param=hello</a>', $result);

        $result = $link->renderLink('p:secondPage|t:Hello world');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >Hello world</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkMailBasic() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('m:john@doe.com');
        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

        $result = $link->renderLink('m:john@doe.com|t:John Doe');
        $this->assertEquals('<a href="mailto:john@doe.com" >John Doe</a>', $result);
    }


    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testMailEncryption() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        //TODO: aktivieren sobald encrypted Mails implemented.
//        $result = $link->renderLink('m:john@doe.com|e');
//        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);
//
//        $result = $link->renderLink('m:john@doe.com|e:');
//        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

        $result = $link->renderLink('m:john@doe.com|e:0');
        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

//        $result = $link->renderLink('m:john@doe.com|e:1');
//        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderModeUrl() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        Store::setVar(TYPO3_PAGE_ID, 'firstPage', STORE_TYPO3);

        $result = $link->renderLink('u:http://example.com');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('t:Example');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u|t:Example');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example');
        $this->assertEquals('<a href="http://example.com" >Example</a>', $result);

        $result = $link->renderLink('u:http://example.com|r:0');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('t:Example|r:0');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u|t:Example|r:0');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:0');
        $this->assertEquals('<a href="http://example.com" >Example</a>', $result);

        // r: 1
        $result = $link->renderLink('u:http://example.com|r:1');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('t:Example|r:1');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u|t:Example|r:1');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:1');
        $this->assertEquals('<a href="http://example.com" >Example</a>', $result);

        // r: 2
        $result = $link->renderLink('u:http://example.com|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u|t:Example|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:2');
        $this->assertEquals('<a href="http://example.com" >Example</a>', $result);

        // r: 3
        $result = $link->renderLink('u:http://example.com|r:3');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        $result = $link->renderLink('t:Example|r:3');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u|t:Example|r:3');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:3');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        // r: 4
        $result = $link->renderLink('u:http://example.com|r:4');
        $this->assertEquals('http://example.com', $result);

        $result = $link->renderLink('t:Example|r:4');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u|t:Example|r:4');
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:4');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        // r: 5
        $result = $link->renderLink('u:http://example.com|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u|t:Example|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:5');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderModePage() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        Store::setVar(TYPO3_PAGE_ID, 'firstPage', STORE_TYPO3);

        // r: default (0)
        $result = $link->renderLink('p');
        $this->assertEquals('<a href="' . $this->baseUrl . 'firstPage" >' . $this->baseUrl . 'firstPage</a>', $result);

        $result = $link->renderLink('p:secondPage');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >' . $this->baseUrl . 'secondPage</a>', $result);

        $result = $link->renderLink('t:Example');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|t:Example');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >Example</a>', $result);

        // r: 0
        $result = $link->renderLink('p|r:0');
        $this->assertEquals('<a href="' . $this->baseUrl . 'firstPage" >' . $this->baseUrl . 'firstPage</a>', $result);

        $result = $link->renderLink('p:secondPage|r:0');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >' . $this->baseUrl . 'secondPage</a>', $result);

        $result = $link->renderLink('t:Example|r:0');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:0');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >Example</a>', $result);

        // r: 1
        $result = $link->renderLink('p|r:1');
        $this->assertEquals('<a href="' . $this->baseUrl . 'firstPage" >' . $this->baseUrl . 'firstPage</a>', $result);

        $result = $link->renderLink('p:secondPage|r:1');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >' . $this->baseUrl . 'secondPage</a>', $result);

        $result = $link->renderLink('t:Example|r:1');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:1');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >Example</a>', $result);

        // r: 2
        $result = $link->renderLink('p|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:2');
        $this->assertEquals('<a href="' . $this->baseUrl . 'secondPage" >Example</a>', $result);

        // r: 3
        $result = $link->renderLink('p|r:3');
//        $this->assertEquals('<span >' . $this->baseUrl . 'firstPage</span>', $result);
        $this->assertEquals($this->baseUrl . 'firstPage', $result);

        $result = $link->renderLink('p:secondPage|r:3');
//        $this->assertEquals('<span >' . $this->baseUrl . 'secondPage</span>', $result);
        $this->assertEquals($this->baseUrl . 'secondPage', $result);

        $result = $link->renderLink('t:Example|r:3');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:3');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        // r: 4
        $result = $link->renderLink('p|r:4');
//        $this->assertEquals('<span >' . $this->baseUrl . 'firstPage</span>', $result);
        $this->assertEquals($this->baseUrl . 'firstPage', $result);

        $result = $link->renderLink('p:secondPage|r:4');
//        $this->assertEquals('<span >' . $this->baseUrl . 'secondPage</span>', $result);
        $this->assertEquals($this->baseUrl . 'secondPage', $result);

        $result = $link->renderLink('t:Example|r:4');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:4');
//        $this->assertEquals('<span >' . $this->baseUrl . 'secondPage</span>', $result);
        $this->assertEquals($this->baseUrl . 'secondPage', $result);

        // r: 5
        $result = $link->renderLink('p|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('p:secondPage|t:Example|r:5');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderModeMail() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $result = $link->renderLink('m:john@doe.com');
        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

        $result = $link->renderLink('t:Example');
        $this->assertEquals('', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example');
        $this->assertEquals('<a href="mailto:john@doe.com" >Example</a>', $result);

        // r: 0
        $result = $link->renderLink('m:john@doe.com|r:0');
        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

        $result = $link->renderLink('t:Example|r:0');
        $this->assertEquals('', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:0');
        $this->assertEquals('<a href="mailto:john@doe.com" >Example</a>', $result);

        // r: 1
        $result = $link->renderLink('m:john@doe.com|r:1');
        $this->assertEquals('<a href="mailto:john@doe.com" >mailto:john@doe.com</a>', $result);

        $result = $link->renderLink('t:Example|r:1');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:1');
        $this->assertEquals('<a href="mailto:john@doe.com" >Example</a>', $result);

        // r: 2
        $result = $link->renderLink('m:john@doe.com|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:2');
        $this->assertEquals('', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:2');
        $this->assertEquals('<a href="mailto:john@doe.com" >Example</a>', $result);

        // r: 3
        $result = $link->renderLink('m:john@doe.com|r:3');
//        $this->assertEquals('<span >mailto:john@doe.com</span>', $result);
        $this->assertEquals('mailto:john@doe.com', $result);

        $result = $link->renderLink('t:Example|r:3');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:3');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        // r: 4
        $result = $link->renderLink('m:john@doe.com|r:4');
//        $this->assertEquals('<span >mailto:john@doe.com</span>', $result);
        $this->assertEquals('mailto:john@doe.com', $result);

        $result = $link->renderLink('t:Example|r:4');
//        $this->assertEquals('<span >Example</span>', $result);
        $this->assertEquals('Example', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:4');
//        $this->assertEquals('<span >mailto:john@doe.com</span>', $result);
        $this->assertEquals('mailto:john@doe.com', $result);

        // r: 5
        $result = $link->renderLink('m:john@doe.com|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:5');
        $this->assertEquals('', $result);

        $result = $link->renderLink('m:john@doe.com|t:Example|r:5');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderModeUrlPicture() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $result = $link->renderLink('u:http://example.com|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" ></a>', $result);

        $result = $link->renderLink('t:Example|P:picture.gif');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</a>', $result);

        // r: 0
        $result = $link->renderLink('u:http://example.com|r:0|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" ></a>', $result);

        $result = $link->renderLink('t:Example|r:0|P:picture.gif');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:0|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</a>', $result);

        // r: 1
        $result = $link->renderLink('u:http://example.com|r:1|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" ></a>', $result);

        $result = $link->renderLink('t:Example|r:1|P:picture.gif');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="picture.gif" src="picture.gif" title="picture.gif" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:1|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</a>', $result);

        // r: 2
        //TODO: no link if text is empty - image is linked here: this is not what the user expects.
        $result = $link->renderLink('u:http://example.com|r:2|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" ></a>', $result);

        $result = $link->renderLink('t:Example|r:2|P:picture.gif');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:2|P:picture.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</a>', $result);

        // r: 3:
        $result = $link->renderLink('u:http://example.com|r:3|P:picture.gif');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > </span>', $result);
        $this->assertEquals('<img alt="picture.gif" src="picture.gif" title="picture.gif" >', $result);

        $result = $link->renderLink('t:Example|r:3|P:picture.gif');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="picture.gif" src="picture.gif" title="picture.gif" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:3|P:picture.gif');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="picture.gif" src="picture.gif" title="picture.gif" > Example', $result);

        // r: 4
        $result = $link->renderLink('u:http://example.com|r:4|P:picture.gif');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        $result = $link->renderLink('t:Example|r:4|P:picture.gif');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="picture.gif" src="picture.gif" title="picture.gif" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:4|P:picture.gif');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        // r: 5
        $result = $link->renderLink('u:http://example.com|r:5|P:picture.gif');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:5|P:picture.gif');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:5|P:picture.gif');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRenderModeUrlButton() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $base = $this->store::getVar(SYSTEM_BASE_URL, STORE_SYSTEM);

        // r: default (0)
        $result = $link->renderLink('u:http://example.com|B:yellow');

        // phpunit on `webwork16/crose/qfq` returns 'typo3conf/ext/qfq/Resources/Public/...' - on gitlab runner they return  BASE_DIR_FAKE
//        $base = (strpos($result, BASE_DIR_FAKE) === false) ? '' : BASE_DIR_FAKE;
        $base = $this->baseUrl;
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('t:Example|B:yellow');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example</a>', $result);

        // r: 0
        $result = $link->renderLink('u:http://example.com|r:0|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('t:Example|r:0|B:yellow');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:0|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example</a>', $result);

        // r: 1
        $result = $link->renderLink('u:http://example.com|r:1|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('t:Example|r:1|B:yellow');
        $this->assertEquals('<img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:1|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example</a>', $result);

        // r: 2
        //TODO: no link if text is empty - image is linked here: this is not what the user expects.
        $result = $link->renderLink('u:http://example.com|r:2|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('t:Example|r:2|B:yellow');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:2|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example</a>', $result);

        // r: 3:
        $result = $link->renderLink('u:http://example.com|r:3|B:yellow');
        $this->assertEquals('<img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" >', $result);

        $result = $link->renderLink('t:Example|r:3|B:yellow');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:3|B:yellow');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example', $result);

        // r: 4
        $result = $link->renderLink('u:http://example.com|r:4|B:yellow');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        $result = $link->renderLink('t:Example|r:4|B:yellow');
//        $this->assertEquals('<span ><img alt="picture.gif" src="picture.gif" title="picture.gif" > Example</span>', $result);
        $this->assertEquals('<img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" > Example', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:4|B:yellow');
//        $this->assertEquals('<span >http://example.com</span>', $result);
        $this->assertEquals('http://example.com', $result);

        // r: 5
        $result = $link->renderLink('u:http://example.com|r:5|B:yellow');
        $this->assertEquals('', $result);

        $result = $link->renderLink('t:Example|r:5|B:yellow');
        $this->assertEquals('', $result);

        $result = $link->renderLink('u:http://example.com|t:Example|r:5|B:yellow');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlypicon() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|E');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|N');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|D');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Delete" ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|H');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|I');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|S');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span></a>', $result);

        $result = $link->renderLink('u:http://example.com|E|o:specific');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="specific" ><span class="glyphicon glyphicon-pencil" ></span></a>', $result);
    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphiconCopyToClipboard() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $copyFeedback = 'let btn = $(this); let btnIcon = $(this).find("span"); btnIcon.fadeOut(400); btnIcon.removeClass("glyphicon-copy").addClass("glyphicon-ok"); this.style.borderColor = "green"; btnIcon.fadeIn(400);  var delayInMilliseconds = 3000; setTimeout(function() { btnIcon.removeClass("glyphicon-ok").addClass("glyphicon-copy"); btn.css("border-color", "");},delayInMilliseconds);';
        $result = $link->renderLink('y:some content');
        $this->assertEquals('<span title="Copy to clipboard" ><button class="btn btn-default" onClick=\'new QfqNS.Clipboard({"text":"some content"});let btn = $(this); let btnIcon = $(this).find("span"); btnIcon.fadeOut(400); btnIcon.removeClass("glyphicon-copy").addClass("glyphicon-ok"); this.style.borderColor = "green"; btnIcon.fadeIn(400);  var delayInMilliseconds = 3000; setTimeout(function() { btnIcon.removeClass("glyphicon-ok").addClass("glyphicon-copy"); btn.css("border-color", "");},delayInMilliseconds);\'><span class="glyphicon glyphicon-copy" ></span></button></span>', $result);

// broken: #12327
//        $result = $link->renderLink('y:some content|N');
//        $this->assertEquals('<span title="Copy to clipboard" ><button class="btn btn-default" onClick=\'new QfqNS.Clipboard({"text":"some content"});\'><span class="glyphicon glyphicon-plus" ></span></button></span>', $result);

    }

    /**
     * @throws \UserReportException
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testBullet() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|B');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:green');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:blue');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet blue" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-blue.gif" title="blue" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:gray');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet gray" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-gray.gif" title="gray" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:pink');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet pink" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-pink.gif" title="pink" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:red');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet red" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-red.gif" title="red" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|B|o:specific');
        $this->assertEquals('<a href="http://example.com" title="specific" ><img alt="Bullet green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="specific" ></a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testChecked() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|C');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:green');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:blue');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked blue" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-blue.gif" title="blue" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:gray');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked gray" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-gray.gif" title="gray" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:pink');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked pink" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-pink.gif" title="pink" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:red');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked red" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-red.gif" title="red" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C:yellow');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked yellow" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-yellow.gif" title="yellow" ></a>', $result);

        $result = $link->renderLink('u:http://example.com|C|o:specific');
        $this->assertEquals('<a href="http://example.com" title="specific" ><img alt="Checked green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="specific" ></a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyph() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|G:glyphicon-envelope');
        $this->assertEquals('<a href="http://example.com" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-envelope" ></span></a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testBeforeAfterText() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|t:Link|v:Hello ');
        $this->assertEquals('Hello <a href="http://example.com" >Link</a>', $result);

        $result = $link->renderLink('u:http://example.com|t:Link|V: world');
        $this->assertEquals('<a href="http://example.com" >Link</a> world', $result);

        $result = $link->renderLink('u:http://example.com|t:Link|v:Hello |V: world');
        $this->assertEquals('Hello <a href="http://example.com" >Link</a> world', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphException() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Missing 'glyph'-name
        $result = $link->renderLink('u:http://example.com|G');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testPictureException1() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|P:picture.gif|B');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testPictureException2() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|C|B');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testPictureException3() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|B:green|B:red');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testPictureException4() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|C:green|C:red');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble1() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|N');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble2() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|D');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble3() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|H');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble4() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|I');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble5() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|S');
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGlyphExceptionDouble6() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|E|G');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkUrlParam() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $result = $link->renderLink('u:http://example.com|U:');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com|U:');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com|U:a=1234');
        $this->assertEquals('<a href="http://example.com?a=1234" >http://example.com?a=1234</a>', $result);

        $result = $link->renderLink('u:http://example.com|U:a=1234&b=abcd');
        $this->assertEquals('<a href="http://example.com?a=1234&b=abcd" >http://example.com?a=1234&b=abcd</a>', $result);

        $result = $link->renderLink('u:http://example.com|U:a');
        $this->assertEquals('<a href="http://example.com?a" >http://example.com?a</a>', $result);

        $result = $link->renderLink('u:http://example.com|U:a=');
        $this->assertEquals('<a href="http://example.com?a=" >http://example.com?a=</a>', $result);

        $result = $link->renderLink('u:http://example.com?A=hello|U:a=world');
        $this->assertEquals('<a href="http://example.com?A=hello&a=world" >http://example.com?A=hello&a=world</a>', $result);

        $result = $link->renderLink('u:http://example.com?A=hello&B=nice|U:a=world');
        $this->assertEquals('<a href="http://example.com?A=hello&B=nice&a=world" >http://example.com?A=hello&B=nice&a=world</a>', $result);

        $result = $link->renderLink('p:form|U:a=1234');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?a=1234" >' . $this->baseUrl . 'form?a=1234</a>', $result);

        $result = $link->renderLink('p:form|U:a=1234&b=abcd');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?a=1234&b=abcd" >' . $this->baseUrl . 'form?a=1234&b=abcd</a>', $result);

        $result = $link->renderLink('p:form|U:a');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?a" >' . $this->baseUrl . 'form?a</a>', $result);

        $result = $link->renderLink('p:form|U:a=');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?a=" >' . $this->baseUrl . 'form?a=</a>', $result);
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testTooltip() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // standard case
        $result = $link->renderLink('u:http://example.com|o:hello world');
        $this->assertEquals('<a href="http://example.com" title="hello world" >http://example.com</a>', $result);

        // standard case, swapped parameter
        $result = $link->renderLink('o:hello world|u:http://example.com');
        $this->assertEquals('<a href="http://example.com" title="hello world" >http://example.com</a>', $result);

        // no text: this is ok
        $result = $link->renderLink('u:http://example.com|o');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        // no text: this is ok
        $result = $link->renderLink('u:http://example.com|o:');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        // some text, with double ticks inside
        $result = $link->renderLink('u:http://example.com|o:hello world "some more text" end');
        $this->assertEquals('<a href="http://example.com" title="hello world &quot;some more text&quot; end" >http://example.com</a>', $result);

        // some text with single ticks
        $result = $link->renderLink('u:http://example.com|o:hello world \'some more text\' end');
        $this->assertEquals('<a href="http://example.com" title="hello world \'some more text\' end" >http://example.com</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAltText() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // standard case
        $result = $link->renderLink('u:http://example.com|a:hello world|P:image.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="hello world" src="image.gif" title="image.gif" ></a>', $result);

        // standard: swapped parameter
        //TODO: fixme
//        $result = $link->renderLink('P:image.gif|a:hello world|u:http://example.com');
//        $this->assertEquals('<a href="http://example.com" ><img alt="hello world" src="image.gif" title="image.gif" ></a>', $result);

        // alt text empty
        $result = $link->renderLink('u:http://example.com|a:|P:image.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="image.gif" src="image.gif" title="image.gif" ></a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAttribute() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Standard
        $result = $link->renderLink('p:editperson|A:data-reference="editperson"');
        $this->assertEquals('<a href="' . $this->baseUrl . 'editperson" data-reference="editperson" >' . $this->baseUrl . 'editperson</a>', $result);

        // BS Button & SIP
        $result = $link->renderLink('p:editperson|A:data-reference="editperson"|b|s');
        $this->assertEquals('<a href="' . $this->baseUrl . 'editperson?s=badcaffee1234" class="btn btn-default" data-reference="editperson" >' . $this->baseUrl . 'editperson?s=badcaffee1234</a>', $result);

        // Render mode 3 (disabled)
        $result = $link->renderLink('p:editperson|A:data-reference="editperson"|r:3');
        $this->assertEquals('<span data-reference="editperson" >' . $this->baseUrl . 'editperson</span>', $result);

    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testClass() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // no class
        $result = $link->renderLink('u:http://example.com|c:n');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        // specific class
        $result = $link->renderLink('u:http://example.com|c:myClass');
        $this->assertEquals('<a href="http://example.com" class="myClass" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com|c');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('u:http://example.com|c:');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        $result = $link->renderLink('p:form');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form" >' . $this->baseUrl . 'form</a>', $result);
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testTarget() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // no target
        $result = $link->renderLink('u:http://example.com|g');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        // no target
        $result = $link->renderLink('u:http://example.com|g:');
        $this->assertEquals('<a href="http://example.com" >http://example.com</a>', $result);

        // target _blank
        $result = $link->renderLink('u:http://example.com|g:_blank');
        $this->assertEquals('<a href="http://example.com" target="_blank" >http://example.com</a>', $result);

        // target someName
        $result = $link->renderLink('u:http://example.com|g:someName');
        $this->assertEquals('<a href="http://example.com" target="someName" >http://example.com</a>', $result);

        // target someName, swapped parameter
        $result = $link->renderLink('g:someName|u:http://example.com');
        $this->assertEquals('<a href="http://example.com" target="someName" >http://example.com</a>', $result);
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testRight() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Bullet, LEFT (Standard)
        $result = $link->renderLink('u:http://example.com|t:Hello World|B');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="http://example.com" ><img alt="Bullet green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" > Hello World</a>', $result);

        // Bullet, RIGHT
        $result = $link->renderLink('u:http://example.com|t:Hello World|B|R');
        $this->assertEquals('<a href="http://example.com" >Hello World <img alt="Bullet green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" ></a>', $result);

        // Checked, LEFT (Standard)
        $result = $link->renderLink('u:http://example.com|t:Hello World|C');
        $this->assertEquals('<a href="http://example.com" ><img alt="Checked green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" > Hello World</a>', $result);

        // Checked, RIGHT
        $result = $link->renderLink('u:http://example.com|t:Hello World|C|R');
        $this->assertEquals('<a href="http://example.com" >Hello World <img alt="Checked green" src="' . $base . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" ></a>', $result);

        // Picture, LEFT (Standard)
        $result = $link->renderLink('u:http://example.com|t:Hello World|P:image.gif');
        $this->assertEquals('<a href="http://example.com" ><img alt="image.gif" src="image.gif" title="image.gif" > Hello World</a>', $result);

        // Picture, RIGHT
        $result = $link->renderLink('u:http://example.com|t:Hello World|P:image.gif|R');
        $this->assertEquals('<a href="http://example.com" >Hello World <img alt="image.gif" src="image.gif" title="image.gif" ></a>', $result);

        // swapped param
        $result = $link->renderLink('R|P:image.gif|t:Hello World|u:http://example.com');
        $this->assertEquals('<a href="http://example.com" >Hello World <img alt="image.gif" src="image.gif" title="image.gif" ></a>', $result);
    }


    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSip() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Sip: URL, s
        $result = $link->renderLink('u:?form&r=12&xId=2345&L=1&type=99&gId=55|s');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234" >' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234</a>', $result);

        // Sip: URL, s:0
        $result = $link->renderLink('u:?form&r=12&xId=2345&L=1&type=99&gId=55|s:0');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?r=12&xId=2345&L=1&type=99&gId=55" >' . $this->baseUrl . 'form?r=12&xId=2345&L=1&type=99&gId=55</a>', $result);

        // Sip: URL, s:1
        $result = $link->renderLink('u:?form&r=12&xId=2345&L=1&type=99&gId=55|s:1');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234" >' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234</a>', $result);

        // Sip: Page, s
        $result = $link->renderLink('p:form&r=12&xId=2345&L=1&type=99&gId=55|s');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234" >' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234</a>', $result);

        // Sip: Page, s:0
        $result = $link->renderLink('p:form&r=12&xId=2345&L=1&type=99&gId=55|s:0');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?r=12&xId=2345&L=1&type=99&gId=55" >' . $this->baseUrl . 'form?r=12&xId=2345&L=1&type=99&gId=55</a>', $result);

        // Sip: Page, s:1
        $result = $link->renderLink('p:form&r=12&xId=2345&L=1&type=99&gId=55|s:1');
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234" >' . $this->baseUrl . 'form?L=1&type=99&s=badcaffee1234</a>', $result);
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSipException1() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|s:s');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSipException2() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // r: default (0)
        $link->renderLink('u:http://www.example.com|s:2');
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testQuestion() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // Question: all default
        $result = $link->renderLink('p:person|c:n|q');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: all default
        $result = $link->renderLink('p:person|c:n|q:');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text
        $js = str_replace('Please confirm', 'do you really want', $js);
        $result = $link->renderLink('p:person|c:n|q:do you really want');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text, level: warning
        $js = str_replace('info', 'warning', $js);
        $result = $link->renderLink('p:person|c:n|q:do you really want:warning');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text, level: warning, positive button: I do
        $js = str_replace('Ok', 'I do', $js);
        $result = $link->renderLink('p:person|c:n|q:do you really want:warning:I do');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text, level: warning, positive button: I do, negative button: Shut up
        $js = str_replace('Cancel', 'Shut up', $js);
        $result = $link->renderLink('p:person|c:n|q:do you really want:warning:I do:Shut up');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text (with escaped colon)), level: warning, positive button: I do, negative button: Shut up
        $js = str_replace('do you really want', 'My Question:some nice value', $js);
        $result = $link->renderLink("p:person|c:n|q:My Question\\:some nice value:warning:I do:Shut up");
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Question: individual text (with escaped colon)), level: warning, positive button: I do (with escaped colon), negative button: Shut up
        $js = str_replace('I do', 'I do: hurry up', $js);
        $result = $link->renderLink("p:person|c:n|q:My Question\\:some nice value:warning:I do\\: hurry up:Shut up");
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Timeout:
        $js = str_replace('timeout: 0', 'timeout: 10000', $js);
        $result = $link->renderLink("p:person|c:n|q:My Question\\:some nice value:warning:I do\\: hurry up:Shut up:10");
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Modal: 1
        $result = $link->renderLink("p:person|c:n|q:My Question\\:some nice value:warning:I do\\: hurry up:Shut up:10:1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

        // Modal: 0
        $js = str_replace('modal: true', 'modal: false', $js);
        $result = $link->renderLink("p:person|c:n|q:My Question\\:some nice value:warning:I do\\: hurry up:Shut up:10:0");
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testQuestion2() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // Question: Hide 'cancel' Button
        $result = $link->renderLink('p:person|c:n|q::::-');
        $this->assertEquals('<a href="' . $this->baseUrl . 'person" ' . $js . ' >' . $this->baseUrl . 'person</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDelete() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Report Delete action, default: 'Report', no Icon
        $result = $link->renderLink('U:form=Person&r=123|x');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234" >' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234</a>', $result);
        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&form=Person&r=123', $result);

        // Report Delete action, explicit 'Report', no Icon
        $result = $link->renderLink('U:form=PersonA&r=1234|x:r');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234" >' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234</a>', $result);
        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&form=PersonA&r=1234', $result);

        // Report Delete action, explicit 'Report', Text
        $result = $link->renderLink('U:form=PersonAa&r=2234|x:r|t:Delete Record');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234" >Delete Record</a>', $result);
        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&form=PersonAa&r=2234', $result);

        // Report Delete action, with Icon
        $result = $link->renderLink('U:form=PersonB&r=1235|x|D');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/delete.php?s=badcaffee1234" class="btn btn-default" title="Delete" ><span class="glyphicon glyphicon-trash" ></span></a>', $result);
        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&form=PersonB&r=1235', $result);

        // Report Delete action: explicit php file, default: 'Report', no Icon
        $result = $link->renderLink('u:mydelete.php|U:form=PersonC&r=1236|x');
        $this->assertEquals('<a href="mydelete.php?s=badcaffee1234" >mydelete.php?s=badcaffee1234</a>', $result);
        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&form=PersonC&r=1236', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testBootstrapButton() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Empty
        $result = $link->renderLink('b');
        $this->assertEquals('', $result);

        // Just a page
        $result = $link->renderLink('b|p:id=fake');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-default" >' . $this->baseUrl . 'fake</a>', $result);

        // Just a page
        $result = $link->renderLink('b:0|p:id=fake');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="0" >' . $this->baseUrl . 'fake</a>', $result);

        // Just a page
        $result = $link->renderLink('b:1|p:id=fake');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-default" >' . $this->baseUrl . 'fake</a>', $result);

        // Just a page
        $result = $link->renderLink('b:primary|p:id=fake');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-primary" >' . $this->baseUrl . 'fake</a>', $result);

        // Just a page
        $result = $link->renderLink('b:btn-primary|p:id=fake');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-primary" >' . $this->baseUrl . 'fake</a>', $result);

        // Button Text
        $result = $link->renderLink('b|p:id=fake|t:Button text');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-default" >Button text</a>', $result);

        // Button Text with Glyph Icon
        $result = $link->renderLink('b|p:id=fake|t:Button text|E');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span> Button text</a>', $result);

        // Button Text with Tooltip
        $result = $link->renderLink('b|p:id=fake|t:Button text|o:Tooltip');
        $this->assertEquals('<a href="' . $this->baseUrl . 'fake" class="btn btn-default" title="Tooltip" >Button text</a>', $result);

        // Button Text with render mode r:0
        $result = $link->renderLink('b|p:id=fake|t:Button text|r:3');
        $this->assertEquals('<span class="btn btn-default disabled" >Button text</span>', $result);

        // Button Text with render mode r:1
        $result = $link->renderLink('b|p:id=fake|t:Button text|r:5');
        $this->assertEquals('', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDownloadException() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Empty
        $result = $link->renderLink('d');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDownloadException1() {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Empty
        $result = $link->renderLink('d|F:file.pdf|F:file2.pdf|s:0');
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDownloadSecureLink() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Single file
        $result = $link->renderLink('d|F:file.pdf');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        // With download filename
        $result = $link->renderLink('d:download.pdf|F:file.pdf');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: download.pdf" data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        // Two files
        $result = $link->renderLink('d|F:file.pdf|F:file2.pdf');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        // Mode: PDF
        $result = $link->renderLink('d|F:file.pdf|M:pdf');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        // Download with Tooltip
        $result = $link->renderLink('d|F:file.pdf|o:Tooltip');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Tooltip" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        // Download & Button Text
        $result = $link->renderLink('d|F:file.pdf|t:Button text');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span> Button text</span></a>', $result);

        // Download, r:3
        $result = $link->renderLink('d|F:file.pdf|r:3');
        $this->assertEquals('<span title="Download" class="btn btn-default disabled" ><span class="glyphicon glyphicon-file" ></span></span>', $result);

        // Download, r:5
        $result = $link->renderLink('d|F:file.pdf|r:5');
        $this->assertEquals('', $result);

        // Single file, no BS button
        $result = $link->renderLink('d|F:file.pdf|t:DL|b:0');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" ><span  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));">DL</span></a>', $result);

        // Single file, no BS button, disabled
        $result = $link->renderLink('d|F:file.pdf|t:DL|b:0|r:3');
        $this->assertEquals('<span class="0 disabled" >DL</span>', $result);

        // Single file, nothing
        $result = $link->renderLink('d|F:file.pdf|t:DL|b:0|r:5');
        $this->assertEquals('', $result);

        // Single file, standard Glyph
        $result = $link->renderLink('d|F:file.pdf|t:DL');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span> DL</span></a>', $result);

        // Single file, default Glyph
        $result = $link->renderLink('d|F:file.pdf|t:DL|G:1');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span> DL</span></a>', $result);

        // Single file, no Glyph
        $result = $link->renderLink('d|F:file.pdf|t:DL|G:0');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));">DL</span></a>', $result);

        // Single file, custom Glyph
        $result = $link->renderLink('d|F:file.pdf|t:DL|G:glyphicon-ok');
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php?s=badcaffee1234" class="0" title="Details" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: " data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-ok" ></span> DL</span></a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDownloadPersistentLink() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        Store::setVar(SYSTEM_SQL_DIRECT_DOWNLOAD . 'downloadphp', 'SELECT "d|F:file.pdf"', STORE_SYSTEM);

        $result = $link->renderLink('d:123|s:0');
        $base = (strpos($result, $this->baseUrl) === false) ? '' : $this->baseUrl;
        $this->assertEquals('<a href="' . $base . 'typo3conf/ext/qfq/Classes/Api/download.php/123" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: 123" data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

        Store::setVar(SYSTEM_SQL_DIRECT_DOWNLOAD . 'dlphp', 'SELECT "d|F:file.pdf"', STORE_SYSTEM);
        $result = $link->renderLink('d:dl.php/123|s:0');
        $this->assertEquals('<a href="dl.php/123" class="0" title="Download" ><span class="btn btn-default"  data-toggle="modal" data-target="#qfqModal101" data-title="Download: dl.php/123" data-text="Please wait" data-backdrop="static" data-keyboard="false"            onclick="$(\'#qfqModalTitle101\').text($(this).data(\'title\')); $(\'#qfqModalText101\').text($(this).data(\'text\'));"><span class="glyphicon glyphicon-file" ></span></span></a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkDropdownPlain() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Empty definition
        $expect = '<span class="dropdown" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical" ></span></span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></span>';
        $result = $link->renderLink('z|b:0');
        $this->assertEquals($expect, $result);

        // Empty definition, G:1 (default glyph)
        $expect = '<div class="btn-group" title="Details" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon 1" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:1|o:Details');
        $this->assertEquals($expect, $result);

        // Dropdown menu with text, no glyph, tooltip.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:0|o:Open menu');
        $this->assertEquals($expect, $result);

        // Dropdown menu with text, custom Glyph and a tooltip.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:glyph-icon-left|o:Open menu');
        $this->assertEquals($expect, $result);

        // Dropdown menu with menu entry.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home" >Home</a></li></ul></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:glyph-icon-left|o:Open menu||p:home|t:Home');
        $this->assertEquals($expect, $result);

        // Dropdown menu disabled.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle disabled" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</span></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:glyph-icon-left|o:Open menu|r:3||p:home|t:Home');
        $this->assertEquals($expect, $result);

        // Dropdown menu with two entries, one is SIP encoded.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home?s=badcaffee1234" >Home</a></li><li><a href="' . $this->baseUrl . 'back" title="Navigate back" >Back</a></li></ul></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:glyph-icon-left|o:Open menu||p:home|t:Home|s||p:back|t:Back|o:Navigate back');
        $this->assertEquals($expect, $result);

        // Dropdown menu header, separator and disabled entry.
        $expect = '<span class="dropdown" title="Open menu" ><span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</span><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home?s=badcaffee1234" >Home</a></li><li><a href="' . $this->baseUrl . 'back" title="Navigate back" >Back</a></li><li role="separator" class="divider"></li><li class="dropdown-header">Header</li><li class="disabled"><a href="#">Disabled entry</a></li></ul></span>';
        $result = $link->renderLink('z|t:Menu|b:0|G:glyph-icon-left|o:Open menu||p:home|t:Home|s||p:back|t:Back|o:Navigate back||r:1|t:---||t:===Header|r:1||r:1|t:---Disabled entry');
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkDropdownBootstrap() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Empty definition
        $expect = '<div class="btn-group" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical" ></span></button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z');
        $this->assertEquals($expect, $result);

        // Empty definition, default glyph
        $expect = '<div class="btn-group" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical" ></span></button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z:');
        $this->assertEquals($expect, $result);

        // Empty definition, G:1 (default glyph)
        $expect = '<div class="btn-group" title="Details" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon 1" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:1|o:Details');
        $this->assertEquals($expect, $result);

        // Dropdown menu with text, no glyph, tooltip.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:0|o:Open menu');
        $this->assertEquals($expect, $result);

        // Dropdown menu with text, custom Glyph and a tooltip.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:glyph-icon-left|o:Open menu');
        $this->assertEquals($expect, $result);

        // Dropdown menu with menu entry.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home" >Home</a></li></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:glyph-icon-left|o:Open menu||p:home|t:Home');
        $this->assertEquals($expect, $result);

        // Dropdown menu disabled.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle disabled btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</button></div>';
        $result = $link->renderLink('z|t:Menu|G:glyph-icon-left|o:Open menu|r:3||p:home|t:Home');
        $this->assertEquals($expect, $result);

        // Dropdown menu with two entries, one is SIP encoded.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home?s=badcaffee1234" >Home</a></li><li><a href="' . $this->baseUrl . 'back" title="Navigate back" >Back</a></li></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:glyph-icon-left|o:Open menu||p:home|t:Home|s||p:back|t:Back|o:Navigate back');
        $this->assertEquals($expect, $result);

        // Dropdown menu header, separator and disabled entry.
        $expect = '<div class="btn-group" title="Open menu" ><button type="button" class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyph-icon-left" ></span> Menu</button><ul style="max-height: 70vh; overflow-y: auto" class="dropdown-menu" aria-labelledby="badcaffee1234"><li><a href="' . $this->baseUrl . 'home?s=badcaffee1234" >Home</a></li><li><a href="' . $this->baseUrl . 'back" title="Navigate back" >Back</a></li><li role="separator" class="divider"></li><li class="dropdown-header">Header</li><li class="disabled"><a href="#">Disabled entry</a></li></ul></div>';
        $result = $link->renderLink('z|t:Menu|G:glyph-icon-left|o:Open menu||p:home|t:Home|s||p:back|t:Back|o:Navigate back||r:1|t:---||t:===Header|r:1||r:1|t:---Disabled entry');
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLinkOrderText() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Normal definition
        $expect = '<span style="display: none;">searchText</span><a href="' . $this->baseUrl . 'testPage" >' . $this->baseUrl . 'testPage</a>';
        $result = $link->renderLink('p:id=testPage|Y:searchText');
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
//    public function testDeleteException1() {
//      $this->expectException(\UserReportException::class);
//        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
//
//        // Missing recordId
//        $link->renderLink('U:form=Person&r=|x');
//    }


    public function httpCodeProvider() {
        return [
            ['301', 301], ['303', 303],
            ['', HTTP_REDIRECT_DEFAULT],
            ['get', 303],
            ['temp', 307],
            ['perm', 308]
        ];
    }

    public function invalidHttpCodeProvider() {
        return [['invalid'], ['200'], ['299'], ['304'], ['310'], ['404']];
    }

    /**
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     *
     * @dataProvider httpCodeProvider
     */
    public function testHttpCodeParameter(string $strCode, int $expectedCode) {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $rcTokenGiven = [];
        $result = $link->fillParameter(array(), "h:$strCode", $rcTokenGiven);
        $this->assertEquals($expectedCode, $result[FINAL_HTTP_CODE]);
    }

    /**
     * @param string $argStr
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     *
     * @dataProvider invalidHttpCodeProvider
     */
    public function testInvalidHttpCodeParameter(string $strCode) {
        $this->expectException(\UserReportException::class);
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $rcTokenGiven = [];
        $link->fillParameter(array(), "h:$strCode", $rcTokenGiven);
    }


    /**
     * @param string $argStr
     * @param string $expLocation
     * @param int $expHttpCode
     * @return void
     *
     * @dataProvider httpCodeProvider
     */
    public function testHttpRedirectMode(string $strCode, int $expHttpCode) {
        // Redirections are implemented with exceptions that carry the data
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $location = "http://redirect.com:1322/path?arg1=valu1&arg2=value2";
        try {
            $link->renderLink("u:$location|h:$strCode|r:9");
            // No exception was thrown!
            $this->fail("Expected a RedirectResponse exception, but none has been thrown");
        } catch (RedirectResponse $response) {
            $this->assertEquals($location, $response->getLocationUrl());
            $this->assertEquals($expHttpCode, $response->getCode());
        }
    }

    /**
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testDefaultHttpRedirectCode() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $location = "http://redirect.com:1322/path?arg1=valu1&arg2=value2";
        try {
            $link->renderLink("u:$location|r:9");
            // No exception was thrown!
            $this->fail("Expected a RedirectResponse exception, but none has been thrown");
        } catch (RedirectResponse $response) {
            $this->assertEquals($location, $response->getLocationUrl());
            $this->assertEquals(HTTP_REDIRECT_DEFAULT, $response->getCode());
        }
    }


    /**
     * @throws \UserFormException
     * @throws \CodeException
     * @throws RedirectResponse
     * @throws \UserReportException
     * @throws \DbException
     */
    public function testBackButton(){
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);

        // Check $link->renderLink() to see the current test.
        $expected = '<a onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back|b');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="0" onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back|b:0');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back|b:1');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-success" onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back|b:success');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" onClick="window.history.back()" >Previous</a>';
        $result = $link->renderLink('p:_back|b|t:Previous');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" title="With Tooltip" onClick="window.history.back()" >Previous</a>';
        $result = $link->renderLink('p:_back|b|t:Previous|o:With Tooltip');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" title="Details" onClick="window.history.back()" ><span class="glyphicon glyphicon-fast-backward" ></span> Back</a>';
        $result = $link->renderLink('p:_back|b|G:glyphicon-fast-backward');
        $this -> assertEquals($expected, $result);

        $expected = 'Back';
        $result = $link->renderLink('p:_back|r:3');
        $this -> assertEquals($expected, $result);

        $expected = '<span class="btn btn-default disabled" >Back</span>';
        $result = $link->renderLink('p:_back|b|r:3');
        $this -> assertEquals($expected, $result);

        $expected = '';
        $result = $link->renderLink('p:_back|r:5');
        $this -> assertEquals($expected, $result);

        $expected = '';
        $result = $link->renderLink('p:_back|b|r:5');
        $this -> assertEquals($expected, $result);

        $expected = '<a class="btn btn-default" data="hello" onClick="window.history.back()" >Back</a>';
        $result = $link->renderLink('p:_back|b|A:data="hello"');
        $this -> assertEquals($expected, $result);

        $expected = 'pre<a class="btn btn-default" onClick="window.history.back()" >Back</a>post';
        $result = $link->renderLink('p:_back|b|v:pre|V:post');
        $this -> assertEquals($expected, $result);
    }
    /**
     * @throws \UserFormException
     * @throws \CodeException
     * @throws RedirectResponse
     * @throws \UserReportException
     * @throws \DbException
     */
    public function testBuildStickyToolTip() {
        $link = new Link($this->sip, DB_INDEX_DEFAULT, true);
        $toolTipText = "Tooltip";

        $result = $link->renderLink("p:sticky|O:$toolTipText");
        $base = $this->baseUrl;
        $expected = '<a href="'. $this->baseUrl .'sticky" class="tooltip-trigger" title="Tooltip" >'. $this->baseUrl .'sticky</a>';
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky" class="tooltip-trigger" >'. $this->baseUrl .'sticky</a>';
        $result = $link->renderLink("p:sticky|O");
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky" class="btn btn-default  tooltip-trigger" title="TooltipEdit" ><span class="glyphicon glyphicon-pencil" ></span></a>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|E");
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky" class="btn btn-default  tooltip-trigger" title="TooltipEdit" ><span class="glyphicon glyphicon-pencil" ></span> simple</a>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|E|t:simple");
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky?s=badcaffee1234" class="tooltip-trigger" title="Tooltip" >'. $this->baseUrl .'sticky?s=badcaffee1234</a><br>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|V:<br>|s");
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky?s=badcaffee1234" class="btn btn-default  tooltip-trigger" title="Tooltip" >'. $this->baseUrl .'sticky?s=badcaffee1234</a>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|s|b|");
        $this -> assertEquals($expected, $result);

        $expected = 'someText<span title="Tooltip" class="btn btn-default disabled tooltip-trigger" >'. $this->baseUrl .'sticky?s=badcaffee1234</span>someText2';
        $result = $link->renderLink("p:sticky|O:$toolTipText|s|b|r:3|v:someText|V:someText2");
        $this -> assertEquals($expected, $result);

        $expected = '<span title="Tooltip" class="btn btn-default disabled tooltip-trigger" >'. $this->baseUrl .'sticky?s=badcaffee1234</span>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|s|b|r:4");
        $this -> assertEquals($expected, $result);

        $expected = '<a href="'. $this->baseUrl .'sticky?s=badcaffee1234" class="btn btn-default  tooltip-trigger" title="Tooltip" >tip</a>';
        $result = $link->renderLink("p:sticky|O:$toolTipText|s|b|t:tip");
        $this -> assertEquals($expected, $result);
    }


    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {
        parent::setUp();

        $this->store = Store::getInstance('', true);
        $this->sip = new Sip(true);
        $this->sip->sipUniqId('badcaffee1234');

        $this->baseUrl = $this->store::getVar(SYSTEM_BASE_URL, STORE_SYSTEM);
    }
}
