<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Report;

use IMATHUZH\Qfq\Core\Evaluate;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Parser\SignedNumber;
use IMATHUZH\Qfq\Core\Report\Report;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;
use Firebase\JWT\JWT;
use Firebase\JWT\Key as FirebaseKey;

require_once(__DIR__ . '/../Database/AbstractDatabaseTest.php');

const TOTAL_COUNT_PERSON_GENERIC_SQL = 2;

/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/2/16
 * Time: 10:07 PM
 */
class ReportTest extends AbstractDatabaseTest {

    /**
     * @var Evaluate
     */
    private $eval = null;

    private $baseUrl = '';

    /**
     * @var Report
     */
    private $report = null;

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportSingleQuery() {

        //empty
        $result = $this->report->process("");
        $this->assertEquals('', $result);

        // comment
        $result = $this->report->process('# 10.sql = SELECT "Hello World"');
        $this->assertEquals('', $result);

        // empty (missing '=')
        $result = $this->report->process('10.sql SELECT "Hello World"');
        $this->assertEquals('', $result);

        // sql
        $result = $this->report->process('10.sql = SELECT "Hello World"');
        $this->assertEquals('Hello World', $result);

        // sql, 2 rows
        $result = $this->report->process('10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2');
        $this->assertEquals('DoeJohnSmithJane', $result);

        // sql, 2 rows, head
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>");
        $this->assertEquals('<table>DoeJohnSmithJane', $result);

        // sql, 2 rows, head, tail
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>");
        $this->assertEquals('<table>DoeJohnSmithJane</table>', $result);

        // sql, 2 rows, head, tail, rbeg
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>");
        $this->assertEquals('<table><tr>DoeJohn<tr>SmithJane</table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = </tr>");
        $this->assertEquals('<table><tr>DoeJohn</tr><tr>SmithJane</tr></table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend, fbeg
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = </tr>\n10.fbeg = <td>");
        $this->assertEquals('<table><tr><td>Doe<td>John</tr><tr><td>Smith<td>Jane</tr></table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend, fbeg, fend
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = </tr>\n10.fbeg = <td>\n10.fend = </td>");
        $this->assertEquals('<table><tr><td>Doe</td><td>John</td></tr><tr><td>Smith</td><td>Jane</td></tr></table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend, fbeg, fend, renr
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = ++\n10.fbeg = <td>\n10.fend = </td>\n10.renr = </tr>");
        $this->assertEquals('<table><tr><td>Doe</td><td>John</td>++</tr><tr><td>Smith</td><td>Jane</td>++</tr></table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend, fbeg, fend, renr, rsep
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = ++\n10.fbeg = <td>\n10.fend = </td>\n10.renr = </tr>\n10.rsep = @");
        $this->assertEquals('<table><tr><td>Doe</td><td>John</td>++</tr>@<tr><td>Smith</td><td>Jane</td>++</tr></table>', $result);

        // sql, 2 rows, head, tail, rbeg, rend, fbeg, fend, renr, rsep, fsep
        $result = $this->report->process("10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2\n10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = ++\n10.fbeg = <td>\n10.fend = </td>\n10.renr = </tr>\n10.rsep = @\n10.fsep = $$");
        $this->assertEquals('<table><tr><td>Doe</td>$$<td>John</td>++</tr>@<tr><td>Smith</td>$$<td>Jane</td>++</tr></table>', $result);

        // no 'sql'
        $result = $this->report->process("10.tail = </table>\n10.rbeg = <tr>\n10.rend = ++\n10.fbeg = <td>\n10.fend = </td>\n10.renr = </tr>\n10.rsep = @\n10.fsep = $$");
        $this->assertEquals('', $result);

        // head, tail, rbeg, rend, fbeg, fend, renr, rsep, fsep, sql 2 rows
        $result = $this->report->process("10.head = <table>\n10.tail = </table>\n10.rbeg = <tr>\n10.rend = ++\n10.fbeg = <td>\n10.fend = </td>\n10.renr = </tr>\n10.rsep = @\n10.fsep = $$\n10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2");
        $this->assertEquals('<table><tr><td>Doe</td>$$<td>John</td>++</tr>@<tr><td>Smith</td>$$<td>Jane</td>++</tr></table>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testUnknownTokenException() {

        $this->expectException(\UserReportException::class);
        // empty (missing '=')
        $result = $this->report->process('10.sql SELECT "Hello = World"');
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportMultipleQuery() {

        // nested 2
        $result = $this->report->process("10.sql = SELECT id FROM Person ORDER BY id LIMIT 2\n10.10.sql = SELECT name FROM Person ORDER BY id LIMIT 2\n");
        $this->assertEquals('1DoeSmith2DoeSmith', $result);

        // nested 3
        $result = $this->report->process("10.sql = SELECT id FROM Person ORDER BY id LIMIT 2\n10.10.sql = SELECT name FROM Person ORDER BY id LIMIT 2\n\n10.10.10.sql = SELECT firstname FROM Person ORDER BY id LIMIT 2\n");
        $this->assertEquals('1DoeJohnJaneSmithJohnJane2DoeJohnJaneSmithJohnJane', $result);

        // nested 2, seq
        $result = $this->report->process("10.sql = SELECT id FROM Person ORDER BY id LIMIT 2\n10.10.sql = SELECT name FROM Person ORDER BY id LIMIT 2\n\n10.20.sql = SELECT firstname FROM Person ORDER BY id LIMIT 2\n");
        $this->assertEquals('1DoeSmithJohnJane2DoeSmithJohnJane', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportLink() {

        // link
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" >http://www.example.com</a>', $result);

        // link, checked
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|C' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" ><img alt="Checked green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" ></a>', $result);

        // linck, checked, text
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|C|t:Hello World' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" ><img alt="Checked green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" > Hello World</a>', $result);

        // link, checked, text, tooltip
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|C|t:Hello World|o:more information' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" title="more information" ><img alt="Checked green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="more information" > Hello World</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportLinkRenderMode() {

        // r:0, url + text
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|t:Hello World|r:0' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" >Hello World</a>', $result);

        // r:0, url
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|r:0' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" >http://www.example.com</a>', $result);

        // r:0, text
        $result = $this->report->process("10.sql = SELECT 'r:0|t:Hello World' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        // r:1, url + text
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|t:Hello World|r:1' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" >Hello World</a>', $result);

        // r:1, url
        $result = $this->report->process("10.sql = SELECT 'u:http://www.example.com|r:1' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="http://www.example.com" >http://www.example.com</a>', $result);

        // r:1, text
        $result = $this->report->process("10.sql = SELECT 'r:1|t:Hello World' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('Hello World', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function testReportMailto() {

        // r:0, mailto
        $result = $this->report->process("10.sql = SELECT 'm:john.doe@example.com' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="mailto:john.doe@example.com" >mailto:john.doe@example.com</a>', $result);

        $result = $this->report->process("10.sql = SELECT 'm:john.doe@example.com|e:0' AS _link FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="mailto:john.doe@example.com" >mailto:john.doe@example.com</a>', $result);


        $result = $this->report->process("10.sql = SELECT '' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        $result = $this->report->process("10.sql = SELECT '|' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        $result = $this->report->process("10.sql = SELECT '|Some Text' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        $code = <<<EOF
<script language=javascript><!--
var email = "john.doe"
var emailHost = "example.com"
var contact = "Some Text"
document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ "><span>" + contact
                    + "</span></a>")
//--></script>
EOF;
        $result = $this->report->process("10.sql = SELECT 'john.doe@example.com|Some Text' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals($code, $result);

        $code = <<<EOF
<script language=javascript><!--
var email = "john.doe"
var emailHost = "example.com"
var contactHost = "example.com"
var contact = "john.doe@"
document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ "><span>" + contact
                    + "</span><span>" + contactHost + "</span></a>")
//--></script>
EOF;
        $result = $this->report->process("10.sql = SELECT 'john.doe@example.com' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals($code, $result);

        $code = <<<EOF
<script language=javascript><!--
var email = "john.doe"
var emailHost = "broken.email.address"
var contact = "john.doe"
document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ "><span>" + contact
                    + "</span></a>")
//--></script>
EOF;
        $result = $this->report->process("10.sql = SELECT 'john.doe' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals($code, $result);

        $code = <<<EOF
<script language=javascript><!--
var email = "john.doe"
var emailHost = "broken.email.address"
var contact = "Some Text"
document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ "><span>" + contact
                    + "</span></a>")
//--></script>
EOF;
        $result = $this->report->process("10.sql = SELECT 'john.doe|Some Text' AS _mailto FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals($code, $result);


        //TODO: enable encryption check
//        $result = $this->report->process("10.sql = SELECT 'm:john.doe@example.com|e:1' AS _link FROM Person ORDER BY id LIMIT 1");
//        $this->assertEquals('<a href="mailto:john.doe@example.com" >mailto:john.doe@example.com</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenSip() {

        // page with sip (default, without explizit definition)
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >Person</a>', $result);

        // page with sip (explizit definition via 's')
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|s' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >Person</a>', $result);

        // page without sip (explizit definition via 's:0')
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|s:0' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?r=123&a=hello&type=5&L=3&final=world" >Person</a>', $result);

        // page with sip (explizit definition via 's:1')
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|s:1' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >Person</a>', $result);

        // page with sip (explizit definition via 's:1')
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenPicture() {

        // page & picture
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|P:picture.gif' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" ><img alt="picture.gif" src="picture.gif" title="picture.gif" ></a>', $result);

        // page & Edit
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|E' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span></a>', $result);

        // page & New
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|N' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span></a>', $result);

        // page & Help
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|H' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span></a>', $result);

        // page & Information
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|I' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span></a>', $result);

        // page & Show
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|S' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span></a>', $result);

        // page & Show & Text
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|S|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span> Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenBullet() {

        // page & bullet (green)
        $result = $this->report->process("10.sql = SELECT 'p:form|B' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Bullet green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" ></a>', $result);

        // page & bullet (green)
        $result = $this->report->process("10.sql = SELECT 'p:form|B||t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Bullet green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" > Person</a>', $result);

        $arr = ['blue', 'gray', 'green', 'pink', 'red', 'yellow', 'fake'];
        foreach ($arr as $color) {
            // page & bullet $color
            $result = $this->report->process("10.sql = SELECT 'p:form|B:$color' AS _page FROM Person ORDER BY id LIMIT 1");
            $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Bullet ' . $color . '" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-' . $color . '.gif" title="' . $color . '" ></a>', $result);
        }
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenCheck() {

        // page & bullet (green)
        $result = $this->report->process("10.sql = SELECT 'p:form|C' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Checked green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" ></a>', $result);

        // page & bullet (green)
        $result = $this->report->process("10.sql = SELECT 'p:form|C|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Checked green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-green.gif" title="green" > Person</a>', $result);

        $arr = ['blue', 'gray', 'green', 'pink', 'red', 'yellow', 'fake'];
        foreach ($arr as $color) {
            // page & bullet $color
            $result = $this->report->process("10.sql = SELECT 'p:form|C:$color' AS _page FROM Person ORDER BY id LIMIT 1");
            $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Checked ' . $color . '" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-' . $color . '.gif" title="' . $color . '" ></a>', $result);
        }
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenUrlParam() {

        // page & Url Param
        $result = $this->report->process("10.sql = SELECT 'p:form|U:r=234|s:0|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?r=234" >Person</a>', $result);

        // page & Url Param=''
        $result = $this->report->process("10.sql = SELECT 'p:form|U|s:0|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form" >Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenToolTip() {

        // page & Tooltip=Tool Tip
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|o:Tool Tip' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" title="Tool Tip" >Person</a>', $result);

        // page & Tooltip=''
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|o' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenAltText() {

        // page & AltText - no image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|a' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & AltText - no image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|a:' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & AltText - no image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|a:Hello World' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & AltText - image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|B|a' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" > Person</a>', $result);

        // page & AltText - image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|B|a:' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" > Person</a>', $result);

        // page & AltText - image
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|B|a:Hello World' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" ><img alt="Hello World" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" > Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenClass() {

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|c' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|c:' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|c:n' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & class (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|c:myclass' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="myclass" >Person</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenTarget() {

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|g' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|g:' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|g:_blank' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" target="_blank" >Person</a>', $result);

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|g:_nextframe' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" target="_nextframe" >Person</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenQuestion() {
        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _Page:  pagealias, param, Text, Tooltip, Question
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question
        $js = str_replace('Please confirm', 'My Question', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q:My Question' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question:Level
        $js = str_replace('info', 'error', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q:My Question:error' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question:Level, Button Ok
        $js = str_replace('Ok', 'YES', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q:My Question:error:YES' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question:Level, Button Cancel
        $js = str_replace('Cancel', 'NO', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q:My Question:error:YES:NO' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question:Level (with escaped colon)
//        $js = str_replace('Cancel', 'NO', $js);
        $js = str_replace('My Question', 'Other Question:some nice value', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form&r=123&a=hello&type=5&L=3&final=world|t:Person|o:This is a tooltip|q:Other Question\\\:some nice value:error:YES:NO' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageTokenRight() {

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|R' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person</a>', $result);

        // page & target (empty)
        $result = $this->report->process("10.sql = SELECT 'p:form|t:Person|R|B' AS _page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >Person <img alt="Bullet green" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-green.gif" title="green" ></a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFix() {

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _Page: only pagealias
        $result = $this->report->process("10.sql = SELECT 'form' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" >' . $this->baseUrl . 'form?s=badcaffee1234</a>', $result);

        // _Page: pagealias, param
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234</a>', $result);

        // _Page: pagealias, param, Text
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" >Person</a>', $result);

        // _Page: pagealias, param, Text, Tooltip
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question
        $js = str_replace('Please confirm', 'My Question', $js);
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="myclass" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target
        $js = str_replace("var href = '';", "var href = 'mytarget';", $js);
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="myclass" target="mytarget" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 0: URL + Text
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget|0' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="myclass" target="mytarget" title="This is a tooltip" ' . $js . ' >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 0: URL
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world||This is a tooltip|My Question|myclass|mytarget|0' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="myclass" target="mytarget" title="This is a tooltip" ' . $js . ' >' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 2: URL
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world||This is a tooltip|My Question|myclass|mytarget|2' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 3: URL  + Text
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget|3' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<span title="This is a tooltip" >Person</span>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 4: URL  + Text
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget|4' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<span title="This is a tooltip" >' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234</span>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 4: URL
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget|4' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<span title="This is a tooltip" >' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234</span>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 4: URL + Text
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip|My Question|myclass|mytarget|5' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode 4: URL
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world||This is a tooltip|My Question|myclass|mytarget|5' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode, Sip ON (1)
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip||myclass|mytarget|0|1' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=5&L=3&s=badcaffee1234" class="myclass" target="mytarget" title="This is a tooltip" >Person</a>', $result);

        // _Page:  pagealias, param, Text, Tooltip, Question, Class, Target, Rendermode, Sip OFF (0)
        $result = $this->report->process("10.sql = SELECT 'form&r=123&a=hello&type=5&L=3&final=world|Person|This is a tooltip||myclass|mytarget|0|0' AS _Page FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?r=123&a=hello&type=5&L=3&final=world" class="myclass" target="mytarget" title="This is a tooltip" >Person</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageC() {

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm!', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _Page: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form' AS _pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >' . $this->baseUrl . 'form?s=badcaffee1234</a>', $result);

        // _Page: other than defaults for the alert.
        $js = str_replace('Please confirm!', 'Do you like to open', $js);
        $js = str_replace("type: 'info'", "type: 'success'", $js);
        $js = str_replace('Ok', 'yes', $js);
        $js = str_replace('Cancel', 'no', $js);
        $js = str_replace('timeout: 0', 'timeout: 10000', $js);
        $js = str_replace('modal: true', 'modal: false', $js);
        $result = $this->report->process("10.sql = SELECT 'p:form|q:Do you like to open:success:yes:no:10:0' AS _pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >' . $this->baseUrl . 'form?s=badcaffee1234</a>', $result);


        $result = $this->report->process("10.sql = SELECT 'p:form|q:Do you like to open:success:yes:no:10:0|t:click me' AS _pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >click me</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixC() {

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Please confirm!', type: 'info', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _Page: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form' AS _Pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >' . $this->baseUrl . 'form?s=badcaffee1234</a>', $result);

        // _Page: other than defaults for the alert.
        $js = str_replace('Please confirm!', 'Do you like to open', $js);
        $js = str_replace("type: 'info'", "type: 'success'", $js);
        $js = str_replace('Ok', 'yes', $js);
        $js = str_replace('Cancel', 'no', $js);
        $js = str_replace('timeout: 0', 'timeout: 10000', $js);
        $js = str_replace('modal: true', 'modal: false', $js);
        $result = $this->report->process("10.sql = SELECT 'form|||Do you like to open:success:yes:no:10:0' AS _Pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >' . $this->baseUrl . 'form?s=badcaffee1234</a>', $result);


        $result = $this->report->process("10.sql = SELECT 'form|click me||Do you like to open:success:yes:no:10:0' AS _Pagec FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?s=badcaffee1234" class="btn btn-default" ' . $js . ' >click me</a>', $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageD() {

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Do you really want to delete the record?', type: 'warning', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'U:table=Person&r=123' AS _paged FROM Person ORDER BY id LIMIT 1");
//        $this->assertEquals('<a href="' . Path::urlApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        // Check das via '_paged' SIP_MODE_ANSWER and SIP_TARGET_URL has been set.
        $result = Session::get('badcaffee1234');
        $this->assertEquals('__dbIndexData=1&_modeAnswer=html&_targetUrl=localhost&r=123&table=Person', $result);

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'U:form=Person&r=123' AS _paged FROM Person ORDER BY id LIMIT 1");
//        $this->assertEquals('<a href="' . Path::urlApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        // _paged: other than defaults for the alert.
        $js = str_replace('Do you really want to delete the record?', 'Move to trash?', $js);
        $js = str_replace("type: 'info'", "type: 'success'", $js);
        $js = str_replace('Ok', 'yes', $js);
        $js = str_replace('Cancel', 'no', $js);
        $js = str_replace('timeout: 0', 'timeout: 10000', $js);
        $js = str_replace('modal: true', 'modal: false', $js);
        $js = str_replace("type: 'warning'", "type: 'success'", $js);
        $result = $this->report->process("10.sql = SELECT 'U:table=Person&r=123|q:Move to trash?:success:yes:no:10:0' AS _paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'U:table=Person&r=123|q:Move to trash?:success:yes:no:10:0|t:click me' AS _paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixD() {

        $js = <<<EOF
id="badcaffee1234" onClick="var alert = new QfqNS.Alert({ message: 'Do you really want to delete the record?', type: 'warning', modal: true, timeout: 0, buttons: [
    { label: 'Ok', eventName: 'ok' }
    , { label: 'Cancel',eventName: 'cancel'}
] } );
alert.on('alert.ok', function() { 
  var href = '';
  if (href.startsWith('http')) {
    window.location = href; 
  } else if (href !== '') {
    window.open($('#badcaffee1234').attr('href'), href); 
  } else {
    window.location = $('#badcaffee1234').attr('href'); 
  }
});

alert.show();
return false;"
EOF;

        // _Paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'table=Person&r=123' AS _Paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);

        // _Paged: other than defaults for the alert.
        $js = str_replace('Do you really want to delete the record?', 'Move to trash?', $js);
        $js = str_replace("type: 'info'", "type: 'success'", $js);
        $js = str_replace('Ok', 'yes', $js);
        $js = str_replace('Cancel', 'no', $js);
        $js = str_replace('timeout: 0', 'timeout: 10000', $js);
        $js = str_replace('modal: true', 'modal: false', $js);
        $js = str_replace("type: 'warning'", "type: 'success'", $js);
        $result = $this->report->process("10.sql = SELECT 'table=Person&r=123|||Move to trash?:success:yes:no:10:0' AS _Paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span></a>', $result);


        $result = $this->report->process("10.sql = SELECT 'table=Person&r=123|click me||Move to trash?:success:yes:no:10:0' AS _Paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . Path::appToApi(API_DELETE_PHP) . '?s=badcaffee1234" class="btn btn-default" title="Delete" ' . $js . ' ><span class="glyphicon glyphicon-trash" ></span> click me</a>', $result);

        // Empty string is ok
        $result = $this->report->process("10.sql = SELECT '' AS _Paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

        // r=0 is ok
        $result = $this->report->process("10.sql = SELECT 'table=Person&r=0' AS _Paged FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('', $result);

    }

    /**
     * Missing missing form or table
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testMissingPagedParameterException1() {
        $this->expectException(\UserReportException::class);
        $this->report->process("10.sql = SELECT 'something&r=1' AS _Paged FROM Person ORDER BY id LIMIT 1");
    }


    /**
     * missing form, missing table
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testMissingPagedParameterException4() {
        $this->expectException(\UserReportException::class);
        $this->report->process("10.sql = SELECT 'r=123' AS _Paged FROM Person ORDER BY id LIMIT 1");
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testMissingPagedParameterException6() {
        $this->expectException(\UserReportException::class);
        $this->report->process("10.sql = SELECT 'table&r=123' AS _Paged FROM Person ORDER BY id LIMIT 1");
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testMissingPagedParameterException7() {
        $this->expectException(\UserReportException::class);
        $this->report->process("10.sql = SELECT 'form&r=123' AS _Paged FROM Person ORDER BY id LIMIT 1");
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageE() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4' AS _pagee FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4|t:click me' AS _pagee FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixE() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4' AS _Pagee FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4|click me' AS _Pagee FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Edit" ><span class="glyphicon glyphicon-pencil" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageH() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4' AS _pageh FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4|t:click me' AS _pageh FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixH() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4' AS _Pageh FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4|click me' AS _Pageh FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Help" ><span class="glyphicon glyphicon-question-sign" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageI() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4' AS _pagei FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4|t:click me' AS _pagei FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixI() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4' AS _Pagei FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4|click me' AS _Pagei FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Information" ><span class="glyphicon glyphicon-info-sign" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageN() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4' AS _pagen FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4|t:click me' AS _pagen FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixN() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4' AS _Pagen FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4|click me' AS _Pagen FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="New" ><span class="glyphicon glyphicon-plus" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageS() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4' AS _pages FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'p:form&a=1&r=3&type=4|t:click me' AS _pages FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageFixS() {

        // _paged: incl. alert
        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4' AS _Pages FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span></a>', $result);

        $result = $this->report->process("10.sql = SELECT 'form&a=1&r=3&type=4|click me' AS _Pages FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals('<a href="' . $this->baseUrl . 'form?type=4&s=badcaffee1234" class="btn btn-default" title="Details" ><span class="glyphicon glyphicon-search" ></span> click me</a>', $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportBullet() {

        $arr = ['blue', 'gray', 'green', 'pink', 'red', 'yellow', 'fake'];
        foreach ($arr as $color) {
            // bullet $color
            $result = $this->report->process("10.sql = SELECT '$color' AS _bullet FROM Person ORDER BY id LIMIT 1");
            $this->assertEquals('<img alt="Bullet ' . $color . '" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/bullet-' . $color . '.gif" title="' . $color . '" >', $result);
        }
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportCheck() {

        $arr = ['blue', 'gray', 'green', 'pink', 'red', 'yellow', 'fake'];
        foreach ($arr as $color) {
            // check $color
            $result = $this->report->process("10.sql = SELECT '$color' AS _check FROM Person ORDER BY id LIMIT 1");
            $this->assertEquals('<img alt="Checked ' . $color . '" src="' . $this->baseUrl . 'typo3conf/ext/qfq/Resources/Public/icons/checked-' . $color . '.gif" title="' . $color . '" >', $result);
        }
    }


    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageWrapper() {

        // Report notation 'alias' with alias
        $this->store->setVar(TOKEN_ALIAS . '.1', 'myAlias', STORE_TYPO3);

        $line = <<<EOF
10.sql = SELECT firstname FROM Person ORDER BY id LIMIT 2
10.head = <table>
10.tail = </table>
10.rbeg = <tr>
10.rend = <br>
10.renr = </tr>
10.fbeg = <td>
10.fend = </td>
10.rsep = --
10.fsep = ++

10.10.sql = SELECT 'nested' FROM (SELECT '') AS fake WHERE '{{10.line.count}}'='1'
10.10.shead = Static head
10.10.stail = Static tail
10.10.head = Dynamic head
10.10.tail = Dynamic tail
10.10.althead = No record found
10.10.altsql = SELECT 'alt sql fired'
EOF;

        $result = $this->report->process($line);
        $expect = "<table><tr><td>John</td><br>Static headDynamic headnestedDynamic tailStatic tail</tr>--<tr><td>Jane</td><br>Static headNo record foundalt sql firedStatic tail</tr></table>";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT firstname FROM Person ORDER BY id LIMIT 2
1.head = <table>
1.tail = </table>
1.rbeg = <tr>
1.rend = <br>
1.renr = </tr>
1.fbeg = <td>
1.fend = </td>
1.rsep = --
1.fsep = ++

1.2.sql = SELECT 'nested' FROM (SELECT '') AS fake WHERE '{{myAlias.line.count}}'='1'
1.2.shead = Static head
1.2.stail = Static tail
1.2.head = Dynamic head
1.2.tail = Dynamic tail
1.2.althead = No record found
1.2.altsql = SELECT 'alt sql fired'
EOF;

        $result = $this->report->process($line);
        $expect = "<table><tr><td>John</td><br>Static headDynamic headnestedDynamic tailStatic tail</tr>--<tr><td>Jane</td><br>Static headNo record foundalt sql firedStatic tail</tr></table>";
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportContent() {

        // Report notation 'alias' with alias
        $this->store->setVar(TOKEN_ALIAS . '.1', 'myAlias', STORE_TYPO3);

        $line = <<<EOF
10.sql = SELECT 'Hello'
20.sql = SELECT 'World'
20.tail = {{10.line.content}} 
EOF;
        $result = $this->report->process($line);
        $expect = "HelloWorld{{10.line.content}}";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT 'Hello'
2.sql = SELECT 'World'
2.tail = {{myAlias.line.content}}
EOF;
        $result = $this->report->process($line);
        $expect = "HelloWorld{{myAlias.line.content}}";
        $this->assertEquals($expect, $result);

        $line = <<<EOF
10.sql = SELECT 'Hello'
10.content = hide
20.sql = SELECT 'World'
20.tail = {{10.line.content}} 
EOF;
        $result = $this->report->process($line);
        $expect = "WorldHello";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT 'Hello'
1.content = hide
2.sql = SELECT 'World'
2.tail = {{myAlias.line.content}} 
EOF;
        $result = $this->report->process($line);
        $expect = "WorldHello";
        $this->assertEquals($expect, $result);

        $line = <<<EOF
10.sql = SELECT 'Hello'
10.content = show
20.sql = SELECT 'World'
20.tail = {{10.line.content}} 
EOF;
        $result = $this->report->process($line);
        $expect = "HelloWorldHello";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT 'Hello'
1.content = show
2.sql = SELECT 'World'
2.tail = {{myAlias.line.content}} 
EOF;
        $result = $this->report->process($line);
        $expect = "HelloWorldHello";
        $this->assertEquals($expect, $result);

        // Check that current row can be reused in head, tail, rbeg, rend, renr
        $line = <<<EOF
10.sql = SELECT 'Hello'
10.content = show
10.head = {{10.line.content}}
10.tail = {{10.line.content}}
10.rbeg = {{10.line.content}}
10.rend = {{10.line.content}}
10.renr = {{10.line.content}}
EOF;
        $result = $this->report->process($line);
        $expect = "HelloHelloHelloHelloHelloHello";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        // Check that current row can be reused in head, tail, rbeg, rend, renr
        $line = <<<EOF
1.sql = SELECT 'Hello'
1.content = show
1.head = {{myAlias.line.content}}
1.tail = {{myAlias.line.content}}
1.rbeg = {{myAlias.line.content}}
1.rend = {{myAlias.line.content}}
1.renr = {{myAlias.line.content}}
EOF;
        $result = $this->report->process($line);
        $expect = "HelloHelloHelloHelloHelloHello";
        $this->assertEquals($expect, $result);

        // Check single tick escape
        $line = <<<EOF
10.sql = SELECT "Hel'lo"
10.content = hide
20.sql = SELECT '--{{10.line.content::s}}--', "--{{10.line.content}}--", "--{{10.line.content::s}}--"
EOF;
        $result = $this->report->process($line);
        $expect = "--Hel'lo----Hel'lo----Hel'lo--";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        // Check single tick escape
        $line = <<<EOF
1.sql = SELECT "Hel'lo"
1.content = hide
2.sql = SELECT '--{{myAlias.line.content::s}}--', "--{{myAlias.line.content}}--", "--{{myAlias.line.content::s}}--"
EOF;
        $result = $this->report->process($line);
        $expect = "--Hel'lo----Hel'lo----Hel'lo--";
        $this->assertEquals($expect, $result);

    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportWrap() {

        $line = <<<EOF
10.sql = SELECT id, name, firstname FROM Person ORDER BY id LIMIT 2
10.head = <table>
10.tail = </table>
10.rbeg = <tr>
10.renr = </tr>
10.rsep = --

10.fbeg = <td>
10.fend = </td>
10.fsep = ++
10.fskipwrap = 1,3
EOF;

        $result = $this->report->process($line);
        $expect = "<table><tr>1++<td>Doe</td>John</tr>--<tr>2++<td>Smith</td>Jane</tr></table>";
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportWrap2() {

        $line = <<<EOF
10.sql = SELECT id AS '_noWrap', name, firstname FROM Person ORDER BY id LIMIT 2
10.head = <table>
10.tail = </table>
10.rbeg = <tr>
10.renr = </tr>
10.rsep = --

10.fbeg = <td>
10.fend = </td>
10.fsep = ++
10.fskipwrap = 3
EOF;

        $result = $this->report->process($line);
        $expect = "<table><tr>1++<td>Doe</td>John</tr>--<tr>2++<td>Smith</td>Jane</tr></table>";
        $this->assertEquals($expect, $result);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportVariables() {

        // Report notation 'alias' with alias
        $this->store->setVar(TOKEN_ALIAS . '.1', 'myAlias', STORE_TYPO3);
        $this->store->setVar(TOKEN_ALIAS . '.1.2', 'myAlias2', STORE_TYPO3);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1");
        $this->assertEquals("normal text ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n10.10.sql = SELECT '{{10.hidden}}'");
        $this->assertEquals("normal text hidden", $result);

        // Report notation 'alias' with alias
        $result = $this->report->process("1.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n1.2.sql = SELECT '{{myAlias.hidden}}'");
        $this->assertEquals("normal text hidden", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n10.10.sql = SELECT '{{10.unknown}}'");
        $this->assertEquals("normal text {{10.unknown}}", $result);

        // Report notation 'alias' with alias
        $result = $this->report->process("1.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n1.2.sql = SELECT '{{myAlias.unknown}}'");
        $this->assertEquals("normal text {{myAlias.unknown}}", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n10.10.sql = SELECT '{{fake}}'");
        $this->assertEquals("normal text {{fake}}", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n10.10.sql = SELECT '{{fake:V}}'");
        $this->assertEquals("normal text {{fake:V}}", $result);

        $this->store->setVar('fake', 'hello world ', STORE_VAR);
        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden ' AS _hidden, 'text ' FROM Person ORDER BY id LIMIT 1\n10.10.sql = SELECT '{{fake:V}}'");
        $this->assertEquals("normal text hello world ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fake:V}}'");
        $this->assertEquals("normal text hello world normal text hello world ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}} '");
        $this->assertEquals("normal text hello world -1-2-0 normal text hello world -2-2-0 ", $result);

        // Report notation 'alias' with alias
        $result = $this->report->process("1.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n1.2.sql = SELECT '{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}} '");
        $this->assertEquals("normal text hello world -1-2-0 normal text hello world -2-2-0 ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fake:V}}-{{10.line.count}}-{{10.line.total}} '");
        $this->assertEquals("normal text hello world -1-2 normal text hello world -2-2 ", $result);

        // Report notation 'alias' with alias
        $result = $this->report->process("1.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n1.2.sql = SELECT '{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}} '");
        $this->assertEquals("normal text hello world -1-2 normal text hello world -2-2 ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.10.line.count}}-{{10.10.line.total}} '");
        $this->assertEquals("normal text hello world -1-2-1-1 normal text hello world -2-2-1-1 ", $result);

        // Report notation 'alias' with alias
        $result = $this->report->process("1.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n1.2.sql = SELECT '{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias2.line.count}}-{{myAlias2.line.total}} '");
        $this->assertEquals("normal text hello world -1-2-1-1 normal text hello world -2-2-1-1 ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fake:V:::not found}} '");
        $this->assertEquals("normal text hello world  normal text hello world  ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fakeDontExist:V:::not found}} '");
        $this->assertEquals("normal text not found normal text not found ", $result);

        $result = $this->report->process("10.sql = SELECT 'normal ', 'hidden' AS _hidden, 'text ' FROM Person ORDER BY id\n10.10.sql = SELECT '{{fakeDontExist:V:::{{editFormPage:Y}}}} '");
        $this->assertEquals("normal text form normal text form ", $result);


//        store various
//        store default
        // head.tail,rbeg,.seop mit variabeln
//        5.sql im 10 head abragen
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testReportPageVariables() {

        $line = <<<EOF
10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2
10.head = h:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.tail = t:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.rbeg = rb:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.rend = re:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.renr = rr:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.fbeg = -fb:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.fend = -fe:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.rsep = rs:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.fsep = fs:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
EOF;

        $this->store->setVar('fake', 'hello world', STORE_VAR);
        $result = $this->report->process($line);
        $expect = "h:hello world-1-2-0,rb:hello world-1-2-0,-fb:hello world-1-2-0,Doe-fe:hello world-1-2-0,fs:hello world-1-2-0,-fb:hello world-1-2-0,John-fe:hello world-1-2-0,re:hello world-1-2-0,rr:hello world-1-2-0,rs:hello world-1-2-0,rb:hello world-2-2-0,-fb:hello world-2-2-0,Smith-fe:hello world-2-2-0,fs:hello world-2-2-0,-fb:hello world-2-2-0,Jane-fe:hello world-2-2-0,re:hello world-2-2-0,rr:hello world-2-2-0,t:hello world-2-2-0,";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2
1.head = h:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.tail = t:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.rbeg = rb:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.rend = re:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.renr = rr:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.fbeg = -fb:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.fend = -fe:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.rsep = rs:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.fsep = fs:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
EOF;

        $this->store->setVar('fake', 'hello world', STORE_VAR);
        $this->store->setVar(TOKEN_ALIAS . '.1', 'myAlias', STORE_TYPO3);
        $result = $this->report->process($line);
        $expect = "h:hello world-1-2-0,rb:hello world-1-2-0,-fb:hello world-1-2-0,Doe-fe:hello world-1-2-0,fs:hello world-1-2-0,-fb:hello world-1-2-0,John-fe:hello world-1-2-0,re:hello world-1-2-0,rr:hello world-1-2-0,rs:hello world-1-2-0,rb:hello world-2-2-0,-fb:hello world-2-2-0,Smith-fe:hello world-2-2-0,fs:hello world-2-2-0,-fb:hello world-2-2-0,Jane-fe:hello world-2-2-0,re:hello world-2-2-0,rr:hello world-2-2-0,t:hello world-2-2-0,";
        $this->assertEquals($expect, $result);

        $line = <<<EOF
10.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2
10.10.sql = SELECT ' blue '
10.10.head = h:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.tail = t:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.rbeg = rb:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.rend = re:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.renr = rr:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.fbeg = -fb:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.fend = -fe:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.rsep = rs:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
10.10.fsep = fs:{{fake:V}}-{{10.line.count}}-{{10.line.total}}-{{10.line.insertId}},
EOF;

        $this->store->setVar('fake', 'hello world', STORE_VAR);
        $result = $this->report->process($line);
        $expect = "DoeJohnh:hello world-1-2-0,rb:hello world-1-2-0,-fb:hello world-1-2-0, blue -fe:hello world-1-2-0,re:hello world-1-2-0,rr:hello world-1-2-0,t:hello world-1-2-0,SmithJaneh:hello world-2-2-0,rb:hello world-2-2-0,-fb:hello world-2-2-0, blue -fe:hello world-2-2-0,re:hello world-2-2-0,rr:hello world-2-2-0,t:hello world-2-2-0,";
        $this->assertEquals($expect, $result);

        // Report notation 'alias' with alias
        $line = <<<EOF
1.sql = SELECT name, firstname FROM Person ORDER BY id LIMIT 2
1.2.sql = SELECT ' blue '
1.2.head = h:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.tail = t:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.rbeg = rb:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.rend = re:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.renr = rr:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.fbeg = -fb:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.fend = -fe:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.rsep = rs:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
1.2.fsep = fs:{{fake:V}}-{{myAlias.line.count}}-{{myAlias.line.total}}-{{myAlias.line.insertId}},
EOF;

        $this->store->setVar('fake', 'hello world', STORE_VAR);
        $this->store->setVar(TOKEN_ALIAS . '.1', 'myAlias', STORE_TYPO3);
        $result = $this->report->process($line);
        $expect = "DoeJohnh:hello world-1-2-0,rb:hello world-1-2-0,-fb:hello world-1-2-0, blue -fe:hello world-1-2-0,re:hello world-1-2-0,rr:hello world-1-2-0,t:hello world-1-2-0,SmithJaneh:hello world-2-2-0,rb:hello world-2-2-0,-fb:hello world-2-2-0, blue -fe:hello world-2-2-0,re:hello world-2-2-0,rr:hello world-2-2-0,t:hello world-2-2-0,";
        $this->assertEquals($expect, $result);
    }


    /**
     * A helper for testing JWT generated by the report
     * @param string $token
     * @param string $key
     * @param int $testStartTime
     * @param array $expectedPayload
     * @param string|null $expectedAlg
     * @return void
     */
    protected function checkJWT(string $token, string $key, int $testStartTime, array $expectedPayload = [], ?string $expectedAlg = null): void {
        $currentTime = time();
        // We will first peek into the token.
        // Normally this is not recommended, but here all token are created by us.
        list($header, $payload, $signature) = explode(".", $token);
        $header = json_decode(base64_decode($header), false);
        $payload = json_decode(base64_decode($payload), true);
        // Check the header
        $this->assertSame('JWT', $header->typ);
        $expectedAlg and $this->assertSame($expectedAlg, $header->alg);
        // Check the payload
        $expectedPayload = array_merge([
            'iss' => 'qfq',
            'iat' => new SignedNumber($currentTime - $testStartTime)
        ], $expectedPayload);
        $this->assertSame(array_keys($expectedPayload), array_keys($payload));
        foreach ($expectedPayload as $claim => $value) {
            $actual = $payload[$claim];
            if ($value instanceof SignedNumber) {
                // iat, nbf, exp
                $value = $value->value;
                $this->assertGreaterThanOrEqual($testStartTime + $value, $actual);
                $this->assertLessThanOrEqual($currentTime + $value, $actual);
            } else {
                $this->assertSame($value, $actual);
            }
        }
        // Finally try to validate the token - this will check if the signature is correct
        JWT::decode($token, new FirebaseKey($key, $header->alg));
    }

    public function testReportJWT() {
        $secretKey = 'abcdef0123456789';

        // Minimal token
        $startTime = time();
        $result = $this->report->process("10.sql = SELECT '|$secretKey' AS _jwt");
        $this->checkJWT($result, $secretKey, $startTime);

        // Custom algorithm
        foreach (['HS256', 'HS512'] as $alg) {
            $startTime = time();
            $result = $this->report->process("10.sql = SELECT '|$secretKey|$alg' AS _jwt");
            $this->checkJWT($result, $secretKey, $startTime, [], $alg);
        }

        // With absolute exp
        $startTime = time();
        $exp = $startTime + 3600;
        $result = $this->report->process("10.sql = SELECT 'exp:$exp,msg:test|$secretKey' AS _jwt");
        $this->checkJWT($result, $secretKey, $startTime, ['exp' => $exp, 'msg' => 'test']);

        // With relative exp
        $startTime = time();
        $result = $this->report->process("10.sql = SELECT 'exp:+60,msg:test|$secretKey' AS _jwt");
        $this->checkJWT($result, $secretKey, $startTime, ['exp' => new SignedNumber(60), 'msg' => 'test']);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {

        parent::setUp();

        $GLOBALS["TSFE"] = new FakeTSFEReport();
        $this->eval = new Evaluate($this->store, $this->dbArray[DB_INDEX_DEFAULT]);
        $this->report = new Report(array(), $this->eval, true);

        $this->executeSQLFile(__DIR__ . '/../Database/fixtures/Generic.sql', true);

        $this->baseUrl = $this->store::getVar(SYSTEM_BASE_URL, STORE_SYSTEM);
    }
}

/**
 * Class FakeTSFEReport
 */
class FakeTSFEReport {
    public $id = 1;
    public $type = 1;
    public $sys_language_uid = 1;
}
