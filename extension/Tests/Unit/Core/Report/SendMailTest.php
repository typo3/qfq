<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/2/16
 * Time: 10:07 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Report;

use IMATHUZH\Qfq\Core\Report\SendMail;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class SendMailTest
 * @package qfq
 */
class SendMailTest extends TestCase {

    /**
     * @var SendMail
     */
    private $sendMail = null;

    public function testParseStringToArray() {

        $this->sendMail = new SendMail();

        // Minimal setup
        $result = $this->sendMail->parseStringToArray('');
        $expect = [];
        $this->assertEquals($expect, $result);

        // Simple 'fixed position' DEPRECATED - not sure if it's ok that the array is not filled up to the maximum.
//        $result = $this->sendMail->parseStringToArray('john@doe.com|jane@miller.com|Latest|Dear John');
//        $expect = ['john@doe.com', 'jane@miller.com', 'Latest', 'Dear John'];
//        $this->assertEquals($expect, $result);

        // Simple 'shorthand token based'
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John'];
        $this->assertEquals($expect, $result);
        // Simple 'speaking word based'
        $result = $this->sendMail->parseStringToArray('to:john@doe.com|from:jane@miller.com|subject:Latest|body:Dear John');
        $this->assertEquals($expect, $result);
        // Simple 'speaking word / shorthand' mixed tokens
        $result = $this->sendMail->parseStringToArray('to:john@doe.com|f:jane@miller.com|s:Latest|body:Dear John');
        $this->assertEquals($expect, $result);

        // All (but attachment) 'token based'
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|r:reply@doe.com|A:on|g:123|x:234|c:july@doe.com,steve@doe.com|B:ceo@doe.com|h:Auto-Submit: fake|y:345|z:456|S:test.php');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_REPLY_TO => 'reply@doe.com',
            SENDMAIL_TOKEN_FLAG_AUTO_SUBMIT => 'on', SENDMAIL_TOKEN_GR_ID => '123', SENDMAIL_TOKEN_X_ID => '234',
            SENDMAIL_TOKEN_RECEIVER_CC => 'july@doe.com,steve@doe.com', SENDMAIL_TOKEN_RECEIVER_BCC => 'ceo@doe.com',
            SENDMAIL_TOKEN_HEADER => 'Auto-Submit: fake', SENDMAIL_TOKEN_X_ID2 => '345', SENDMAIL_TOKEN_X_ID3 => '456', SENDMAIL_TOKEN_SRC => 'test.php'];
        $this->assertEquals($expect, $result);
        // All (but attachment) 'speaking word based'
        $result = $this->sendMail->parseStringToArray('to:john@doe.com|from:jane@miller.com|subject:Latest|body:Dear John|reply-to:reply@doe.com|autosubmit:on|grid:123|xid:234|cc:july@doe.com,steve@doe.com|bcc:ceo@doe.com|header:Auto-Submit: fake|xid2:345|xid3:456|source:test.php');
        $this->assertEquals($expect, $result);

        // Single attachment 'token based'
        $attach = [['F:fileadmin/test1.pdf']];
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|F:fileadmin/test1.pdf');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_ATTACHMENT => $attach];

        $this->assertEquals($expect, $result);

        // Three individual attachment 'token based'
//        $attach = [ [ [ 'F' => 'fileadmin/test1.pdf'  ], [ 'F' => 'fileadmin/test2.pdf' ], [ 'F' => 'fileadmin/test3.pdf' ] ] ] ;
        $attach = [['F:fileadmin/test1.pdf'], ['F:fileadmin/test2.pdf'], ['F:fileadmin/test3.pdf']];
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|F:fileadmin/test1.pdf|F:fileadmin/test2.pdf|F:fileadmin/test3.pdf');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_ATTACHMENT => $attach];
        $this->assertEquals($expect, $result);

        // One individual attachment, one dual combined attachment
        $attach = [['F:fileadmin/test1.pdf'], ['F:fileadmin/test2.pdf', 'F:fileadmin/test3.pdf']];
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|F:fileadmin/test1.pdf|C|F:fileadmin/test2.pdf|F:fileadmin/test3.pdf');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_ATTACHMENT => $attach];
        $this->assertEquals($expect, $result);

        // One individual attachment, one quad combined attachment
        $attach = [['F:fileadmin/test1.pdf'], ['F:fileadmin/test2.pdf', 'F:fileadmin/test3.pdf', 'u:http://nzz.ch', 'U:export&a=100']];
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|F:fileadmin/test1.pdf|C|F:fileadmin/test2.pdf|F:fileadmin/test3.pdf|u:http://nzz.ch|U:export&a=100');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_ATTACHMENT => $attach];
        $this->assertEquals($expect, $result);

        // Two quad combined attachments
        $attach = [['d:output1.pdf', 'F:fileadmin/test11.pdf', 'F:fileadmin/test12.pdf', 'u:http://nzz.ch.1', 'U:export1&a=100'], ['d:output2.pdf', 'F:fileadmin/test21.pdf', 'F:fileadmin/test22.pdf', 'u:http://nzz.ch.2', 'U:export2&a=100']];
        $result = $this->sendMail->parseStringToArray('t:john@doe.com|f:jane@miller.com|s:Latest|b:Dear John|C|d:output1.pdf|F:fileadmin/test11.pdf|F:fileadmin/test12.pdf|u:http://nzz.ch.1|U:export1&a=100|C|d:output2.pdf|F:fileadmin/test21.pdf|F:fileadmin/test22.pdf|u:http://nzz.ch.2|U:export2&a=100');
        $expect = [SENDMAIL_TOKEN_RECEIVER => 'john@doe.com', SENDMAIL_TOKEN_SENDER => 'jane@miller.com',
            SENDMAIL_TOKEN_SUBJECT => 'Latest', SENDMAIL_TOKEN_BODY => 'Dear John', SENDMAIL_TOKEN_ATTACHMENT => $attach];
        $this->assertEquals($expect, $result);

    }


    /**
     * @return void
     */
    public function testGetRedirectAllMailTo() {

        $this->sendMail = new SendMail();

        $store = Store::getInstance();

        // No FE/BE User
        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, '', STORE_SYSTEM);
        $this->assertEquals('', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com,jane@doe.com', STORE_SYSTEM);
        $this->assertEquals('jane@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com,jane@doe.com,son@doe.com', STORE_SYSTEM);
        $this->assertEquals('son@doe.com', $this->sendMail->getRedirectAllMailTo());


        // Fake BE User
        $store::setStore([TYPO3_BE_USER_EMAIL => 'john@doe.com', TYPO3_FE_USER_EMAIL => 'unknown@domain.com'], STORE_TYPO3, true);

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, '', STORE_SYSTEM);
        $this->assertEquals('', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com,jane@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'jane@doe.com,john@doe.com,son@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());


        // Fake FE User
        $store::setStore([TYPO3_BE_USER_EMAIL => 'unknown@domain.com', TYPO3_FE_USER_EMAIL => 'john@doe.com'], STORE_TYPO3, true);

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, '', STORE_SYSTEM);
        $this->assertEquals('', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'john@doe.com,jane@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

        $store::setVar(SYSTEM_REDIRECT_ALL_MAIL_TO, 'jane@doe.com,john@doe.com,son@doe.com', STORE_SYSTEM);
        $this->assertEquals('john@doe.com', $this->sendMail->getRedirectAllMailTo());

    }


}

