<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Store;

use IMATHUZH\Qfq\Core\Store\Session;
use PHPUnit\Framework\TestCase;

/**
 * Class SessionTest
 * @package qfq
 */
class SessionTest extends TestCase {

    public function testGetSession() {

        $session1 = Session::getInstance();
        $session2 = Session::getInstance();

        $this->assertEquals($session1, $session2);

        // should not be null
        $this->assertEquals(false, $session2 === null);

        // changing of 'phpUnit' should not fire an exception
        Session::getInstance(false);

        // changing of 'phpUnit' should not fire an exception
        Session::getInstance(true);
    }

    public function testGet() {

        // write/read data1
        Session::set('var1', 'data1');
        $val = Session::get('var1');
        $this->assertEquals('data1', $val);

        // write/read data2
        Session::set('var2', 'data2');
        $val = Session::get('var2');
        $this->assertEquals('data2', $val);

        // read data1  again
        $val = Session::get('var1');
        $this->assertEquals('data1', $val);

        // rewrite/read data1
        Session::set('var1', 'data1again');
        $val = Session::get('var1');
        $this->assertEquals('data1again', $val);

        // read non existing
        $val = Session::get('var3');
        $this->assertEquals(false, $val);

    }

    public function testClear() {

        // write/read data1
        Session::set('var1', 'data1');

        Session::clearAll();

        $val = Session::get('var1');
        $this->assertEquals(false, $val);
    }

    public function setUp(): void {
        Session::getInstance(true);
    }
}