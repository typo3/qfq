<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/2/16
 * Time: 9:16 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Store;


use IMATHUZH\Qfq\Core\Store\Config;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 * @package qfq
 */
class ConfigTest extends TestCase {


    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckForAttack() {

        $this->expectException(\UserFormException::class);
        $config = Config::setDefaults(array());

        $_GET['fake'] = '012345678901234567890123456789012345678901234567890123456789';
        Config::checkForAttack($config);
    }

    /**
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckForAttack2() {

        $config = Config::setDefaults(array());

        // Check for customized length (below)
        # 60
        $_GET['fake_65'] = '012345678901234567890123456789012345678901234567890123456789';
        Config::checkForAttack($config);

        $this->assertEquals(1, 1);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckForAttack3() {

        $this->expectException(\UserFormException::class);
        $config = Config::setDefaults(array());

        // Check for customized length (above)
        # 80
        $_GET['fake_65'] = '01234567890123456789012345678901234567890123456789012345678901234567890123456789';
        Config::checkForAttack($config);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testCheckForAttack4() {

        $this->expectException(\UserFormException::class);
        $config = Config::setDefaults(array());

        $_POST['email'] = 'no@email.com';
        Config::checkForAttack($config);
    }

    protected function setUp(): void {
        $_GET = array();
        $_POST = array();
    }

    protected function tearDown(): void {
        $_GET = array();
        $_POST = array();
    }
}
