<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/10/16
 * Time: 10:55 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Store;


use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

/**
 * Class SipTest
 * @package qfq
 */
class SipTest extends TestCase {

    /**
     * @var Store
     */
    private $store = null;

    public function testUrlparamToSip() {

        $sip = new Sip(true);
        $sip->sipUniqId('badcaffee1234');

        $result = $sip->queryStringToSip("http://example.com/index.php?id=input&r=1&form=person", RETURN_URL);
        $this->assertEquals('http://example.com/index.php?id=input&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("http://example.com/index.php?id=input&r=1&form=person");
        $this->assertEquals('http://example.com/index.php?id=input&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("index.php?id=input&r=1&form=person", RETURN_URL);
        $this->assertEquals('index.php?id=input&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("?id=input&r=1&form=person", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&form=person", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("input&r=1&form=person", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&form=person&keySemId=23", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&form=person&type=99", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?type=99&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&form=person&L=1", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?L=1&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&L=2&form=person&type=99", RETURN_URL);
        $this->assertEquals($this->baseUrl . 'input?L=2&type=99&s=badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=1&L=2&form=person&type=99", RETURN_SIP);
        $this->assertEquals('badcaffee1234', $result);

        $result = $sip->queryStringToSip("id=input&r=10&L=2&form=person&type=99", RETURN_SIP);
        $this->assertEquals('badcaffee1234', $result);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGetVarsFromSip() {
        $sip = new Sip(true);
        $sip->sipUniqId('badcaffee1234');

        $sip2 = $sip->queryStringToSip("http://example.com/index.php?a=1&b=2&c=3", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['a' => 1, 'b' => 2, 'c' => 3], $arr);
        $this->assertEquals('badcaffee1234', $sip2);

        $sip2 = $sip->queryStringToSip("http://example.com/index.php?e=1&f=2&g=3", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['e' => 1, 'f' => 2, 'g' => 3], $arr);
        $this->assertEquals('badcaffee1234', $sip2);

        $sip->sipUniqId('badcaffee0000');
        $sip2 = $sip->sipUniqId();
        $this->assertEquals('badcaffee0000', $sip2);

        $sip2 = $sip->queryStringToSip("http://example.com/index.php?aa=hello&bb=world", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['aa' => 'hello', 'bb' => 'world'], $arr);
        $this->assertEquals('badcaffee1234', $sip2);

        $sip2 = $sip->queryStringToSip("aaa=Don&bbb=John", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['aaa' => 'Don', 'bbb' => 'John'], $arr);

        // when parameter form is used default for r = 0
        $sip2 = $sip->queryStringToSip("http://example.com/index.php?form=test&a=1&b=2&c=3", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['form' => 'test', 'a' => '1', 'b' => '2', 'c' => '3'], $arr);
        $this->assertEquals('badcaffee1234', $sip2);

        $sip2 = $sip->queryStringToSip("http://example.com/index.php?form=test&e=1&f=2&g=3", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['form' => 'test', 'e' => 1, 'f' => 2, 'g' => 3], $arr);
        $this->assertEquals('badcaffee1234', $sip2);

        $sip2 = $sip->queryStringToSip("http://example.com/index.php?form=test&aa=hello&bb=world", RETURN_SIP);
        $arr = $sip->getVarsFromSip($sip2);
        $this->assertEquals(['form' => 'test', 'aa' => 'hello', 'bb' => 'world'], $arr);
        $this->assertEquals('badcaffee1234', $sip2);
    }

    /**
     *
     */
    public function testFakeUniqId() {
        $sip = new Sip(true);
        $this->assertEquals('badcaffee1234', $sip->sipUniqId('badcaffee1234'));

        $sip = new Sip(true);
        $this->assertEquals('badcaffee5678', $sip->sipUniqId('badcaffee5678'));
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetSipFromUrlParam() {

        $sip = new Sip(true);
        $sip->sipUniqId('badcaffee1234');

        $result = $sip->queryStringToSip("http://example.com/index.php?id=input&r=1&form=person", RETURN_URL);
        $s = $sip->getSipFromQueryString('form=person&r=1');
        $this->assertEquals('badcaffee1234', $s);

        $s = $sip->getSipFromQueryString('UnknwonParameter=1234');
        $this->assertFalse($s);

        // TODO : Test wieder reinnehmen:
//        $sip->sipUniqId('badcaffee1111');
//        $url = $sip->queryStringToSip("a=10&b=20&c=30", RETURN_SIP);
//        $s = $sip->getSipFromQueryString('a=10&b=20&c=30');
//        $this->assertEquals('badcaffee1111', $s);

    }

    /**
     *
     */
    public function testSipUniqId() {
        $sip = new Sip(true);
        $sip->sipUniqId('badcaffee1234');

        $s = $sip->sipUniqId('badcaffee1234');
        $this->assertEquals('badcaffee1234', $s);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetQueryStringFromSip() {
        $sip = new Sip(true);
        $sip->sipUniqId('badcaffee1234');

        $s = $sip->queryStringToSip("http://example.com/index.php?id=input&r=20&form=person", RETURN_SIP);

        $result = $sip->getQueryStringFromSip($s);
        $this->assertEquals('form=person&r=20', $result);
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {
        parent::setUp();

        $this->store = Store::getInstance('', true);

        $this->baseUrl = $this->store::getVar(SYSTEM_BASE_URL, STORE_SYSTEM);
    }
}
