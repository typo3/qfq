<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/2/16
 * Time: 11:10 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Store;


use PHPUnit\Framework\TestCase;

/**
 * Class FillStoreFormTest
 * @package qfq
 */
class FillStoreFormTest extends TestCase {

    // Fake Test: all temp. disabled
    public function testFake() {
        $this->assertEquals(1, 1);
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
//    public function testDoDateTime() {
//        $formElement = [ FE_TYPE => FE_TYPE_DATE,
//            FE_DATE_FORMAT => FORMAT_DATE_INTERNATIONAL,
//            FE_SHOW_SECONDS => 0 ];
//
//        $fsf = new FillStoreForm();
//
//        $msg = 'doDateTime fails';
//        $val = '2010-03-31';
//        $this->assertEquals($val, $fsf->doDateTime($formElement, $val), $msg);
//        $val = '2010-02-28';
//        $this->assertEquals($val, $fsf->doDateTime($formElement, $val), $msg);
//        $val = '2012-02-29';
//        $this->assertEquals($val, $fsf->doDateTime($formElement, $val), $msg);
//
//        $formElement[FE_DATE_FORMAT] = FORMAT_DATE_GERMAN;
//        $this->assertEquals($val, $fsf->doDateTime($formElement, '29.02.2012'), $msg);
//    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
//    public function testDoDateTimeInvalidDate() {
//$this->expectException(\UserReportException::class);
//        $formElement = [ FE_TYPE => FE_TYPE_DATE,
//            FE_DATE_FORMAT => FORMAT_DATE_INTERNATIONAL,
//            FE_SHOW_SECONDS => 0 ];
//
//        $fsf = new FillStoreForm();
//
//        $val = '2010-02-29';
//        $fsf->doDateTime($formElement, $val);
//    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
//    public function testDoDateTimeInvalidDateWithTime() {
//  $this->expectException(\UserReportException::class);
//        $formElement = [ FE_TYPE => FE_TYPE_DATE,
//            FE_DATE_FORMAT => FORMAT_DATE_INTERNATIONAL,
//            FE_SHOW_SECONDS => 0 ];
//        $fsf = new FillStoreForm();
//
//        $val = '2010-02-31 23:25';
//        $fsf->doDateTime($formElement, $val);
//    }
}
