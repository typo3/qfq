<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/2/16
 * Time: 9:16 PM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Store;

use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Store\Store;
use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../ConstantsPhpUnit.php');

/**
 * Class StoreTest
 * @package qfq
 */
class StoreTest extends TestCase {

    /**
     * @var Store
     */
    private $store = null;

    private $setUpDone = false;

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function setUp(): void {
        // Client Variables has to setup before the first instantiation of 'Store'
        $_GET[] = array();
        $_POST[] = array();
        $_GET[CLIENT_RECORD_ID] = '1234';
        $_GET['key01'] = '1234';
        $_POST['key02'] = '2345';
        $_POST['key03'] = '3456';
        $_POST[CLIENT_FORM] = 'testformnameDoNotChange';
        $_GET['key04'] = '4567';
        $_POST['key04'] = '5678';

        $_SERVER[SYSTEM_HTTP_ORIGIN] = 'http://' . DOMAIN_EXAMPLE_COM;
        $_SERVER[CLIENT_REQUEST_SCHEME] = 'http';
        $_SERVER[CLIENT_HTTP_HOST] = DOMAIN_EXAMPLE_COM;

        $this->store = Store::getInstance("\n; some comment\n" . TYPO3_FORM . "=testformnameDoNotChange\n", true, 'init');

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testGetInstance() {
        $a = Store::getInstance('', true);
        $b = Store::getInstance('', true);

        $this->assertFalse($a === null, "There should be a class");

        $this->assertEquals($a, $b, "Both classes should be the same");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetVarStoreTypo3() {
        // T3 Bodytext
        $this->store->setVar(TYPO3_FORM, "testformnameDoNotChange", STORE_TYPO3);
        $this->assertEquals('testformnameDoNotChange', $this->store->getVar(TYPO3_FORM, STORE_TYPO3), "System: " . TYPO3_FORM);

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetVarStoreSystem() {
        // DBUSER in qfq.ini
        $val = $this->store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM);
        $this->assertTrue($val !== false && $val !== '', "SYSTEM_DB_INDEX_DATA found in qfq.ini");

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSetVarStoreSystem() {

        // set new Sessionname
        $this->store->setVar(SYSTEM_SQL_LOG_MODE, "all", STORE_SYSTEM);

        // Sessionname: default value
        $this->assertEquals('all', $this->store->getVar(SYSTEM_SQL_LOG_MODE, STORE_SYSTEM), "System: SQL_LOG");

        // set new Sessionname
        $this->store->setVar(SYSTEM_SQL_LOG_MODE, "modify", STORE_SYSTEM);

        $this->assertEquals('modify', $this->store->getVar(SYSTEM_SQL_LOG_MODE, STORE_SYSTEM), "System: SQL_LOG");

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetVarStoreClient() {

        # Violates SANITIZE class: sanitized string is always an empty string.
        # Access are cached:

        # Test: Retrieve a variable, default sanitize class
        $this->assertEquals('1234', $this->store->getVar(CLIENT_RECORD_ID, STORE_CLIENT), "FormName: default sanitize class");

        # violates default SANITIZE digit: sanitized string is always an empty string.
        $this->assertEquals('', $this->store->getVar(CLIENT_SIP, STORE_CLIENT), "sanitize class digit fails");

        // Test GET
        $this->assertEquals('1234', $this->store->getVar('key01', STORE_CLIENT), "Param: GET");

        // Test POST
        $this->assertEquals('2345', $this->store->getVar('key02', STORE_CLIENT), "Param: POST");

        // Test not found
        $this->assertFalse($this->store->getVar('keyUnknown', STORE_CLIENT), "Param: unknown");

        // Test Cache value
        $_POST['key03'] = 'lost';
        $this->store->getVar('key03', STORE_CLIENT);
        $this->assertEquals('3456', $this->store->getVar('key03', STORE_CLIENT), "Param: read from cache");

        // Test Cache not found
        $this->store->getVar('keyUnknwon2');
        $this->assertFalse($this->store->getVar('keyUnknown2', STORE_CLIENT), "Param: unknown from cache");

        // Test overwrite default sanitize class
        $this->assertEquals('!!digit!!', $this->store->getVar(CLIENT_FORM, STORE_CLIENT, SANITIZE_ALLOW_DIGIT), "Param: overwrite default sanitize class");

        // Test: POST higher priority than GET
        $this->assertEquals('5678', $this->store->getVar('key04', STORE_CLIENT), "Param: POST higher priority than GET");
    }

    /**
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testStorePriority() {
        //default prio  FSRVD

        $this->store->unsetStore(STORE_RECORD);

        $db = new Database();
        $tableDefinition = $db->getTableDefinition('Person');
        $this->store->fillStoreTableDefaultColumnType($tableDefinition);

//        $this->assertEquals('male', $this->store->getVar('gender','','alnumx'), "Get default definition from table person.gender");

        $this->store->setVar('gender', 'female', STORE_RECORD);
        $this->assertEquals('female', $this->store->getVar('gender'), "Retrieve 'gender' from STORE_RECORD");

        $this->store->setVar('gender', 'male2', STORE_SIP);
        $this->assertEquals('male2', $this->store->getVar('gender'), "Retrieve 'gender' from STORE_SIP");

        $this->store->setVar('gender', 'female2', STORE_FORM);
        $this->assertEquals('female2', $this->store->getVar('gender', '', SANITIZE_ALLOW_ALNUMX), "Retrieve 'gender' from STORE_SIP");

    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testStoreDifferentSanitizeClass() {
        //default prio  FSRD

        $this->store->setVar('fruit', 'apple', STORE_RECORD);
        $this->assertEquals('apple', $this->store->getVar('fruit'), "Retrieve 'fruit' from STORE_RECORD");

        $this->store->setVar('color', 'green', STORE_FORM);
        $this->assertEquals('!!digit!!', $this->store->getVar('color'), "Retrieve 'color' from STORE_FORM");
        $this->assertEquals('green', $this->store->getVar('color', '', SANITIZE_ALLOW_ALNUMX), "Retrieve 'color' from STORE_FORM");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetVarStore0() {
        //default prio  FSRD

        $this->assertEquals(0, $this->store->getVar('fakename', STORE_ZERO), "Retrieve anything from STORE_ZERO");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testUnsetStore() {
        $this->store->unsetStore(STORE_RECORD);

        $this->store->setStore(array(), STORE_RECORD);
        $this->assertEquals(false, $this->store->getVar('apple', STORE_RECORD), "Retrieve a value from store.");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSetVarArray() {
        $this->store->unsetStore(STORE_RECORD);
        $arr = ['a' => '1', 'apple' => 'green'];
        $this->store->setStore($arr, STORE_RECORD);
        $this->assertEquals('1', $this->store->getVar('a', STORE_RECORD), "Retrieve a value from store 'fake'");
        $this->assertEquals('green', $this->store->getVar('apple', STORE_RECORD), "Retrieve a value from store.");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSetVarArrayEmpty() {
        $this->store->unsetStore(STORE_RECORD);
        $arr = array();
        $this->store->setStore($arr, STORE_RECORD);
        $this->assertEquals(false, $this->store->getVar('apple', STORE_RECORD), "Retrieve a value from store.");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testSetVar() {

        $this->store = Store::getInstance('', true);

        foreach ([STORE_SIP, STORE_CLIENT, STORE_EXTRA] as $store) {
            // initial set
            $this->store->setVar('unitTest', '123', $store);
            $this->assertEquals('123', $this->store->getVar('unitTest', $store), "Retrieve var.");

            // redefine, using default 'overwrite=true'
            $this->store->setVar('unitTest', '1234', $store);
            $this->assertEquals('1234', $this->store->getVar('unitTest', $store), "Retrieve var.");

            // redefine, using explicit 'overwrite=true'
            $this->store->setVar('unitTest', '12345', $store, true);
            $this->assertEquals('12345', $this->store->getVar('unitTest', $store), "Retrieve var.");

            // redefine, using explicit 'overwrite=false' but with the same value (that is ok)
            $this->store->setVar('unitTest', '12345', $store, false);
            $this->assertEquals('12345', $this->store->getVar('unitTest', $store), "Retrieve var.");
        }
    }

//    /**
//     * @throws \CodeException
//     * @throws \UserFormException
//     * @throws \UserReportException
//     */
//    public function testConfigMandatoryValues() {
//
//      $this->expectException(\UserReportException::class);
//        $fileName = $this->createFile('');
//        // the following produces Undefined index exception since you give an empty config.ini which does not contain the index.
//        // What do you want to test here?
//        $this->store = Store::getInstance('', true, $fileName);
//        unlink($fileName);
//
//        $this->assertEquals(false, $this->store->getStore(STORE_SYSTEM), "Retrieve system store.");
//    }
//
    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAppendToStore() {
        $this->store->unsetStore(STORE_RECORD);

        $this->store->appendToStore(array(), STORE_RECORD);
        $this->assertEquals(array(), $this->store->getStore(STORE_RECORD));

        $param = ['a' => 'hello', 'b' => 'world'];
        $this->store->appendToStore($param, STORE_RECORD);
        $this->assertEquals($param, $this->store->getStore(STORE_RECORD));

        $this->store->appendToStore(null, STORE_RECORD);
        $this->assertEquals($param, $this->store->getStore(STORE_RECORD));

        $this->store->appendToStore(array(), STORE_RECORD);
        $this->assertEquals($param, $this->store->getStore(STORE_RECORD));

        $param2 = ['c' => 'nice', 'd' => 'job'];
        $this->store->appendToStore($param2, STORE_RECORD);
        $this->assertEquals(array_merge($param, $param2), $this->store->getStore(STORE_RECORD));

        $param3 = ['a' => 'well', 'b' => 'done'];
        $this->store->appendToStore($param3, STORE_RECORD);
        $this->assertEquals(array_merge($param3, $param2), $this->store->getStore(STORE_RECORD));

        $this->store->setStore($param, STORE_RECORD, true);
        $this->store->appendToStore([$param2, $param3, $param3], STORE_RECORD);
        $this->assertEquals(array_merge($param, $param2), $this->store->getStore(STORE_RECORD));
    }

    /**
     * @param $body
     * @return bool|string
     */
    private function createFile($body) {

        $tmpFileName = tempnam('/tmp', 'qfq.json.');
        $handle = fopen($tmpFileName, "w");
        fwrite($handle, $body);
        fclose($handle);

        return $tmpFileName;
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testConfigIniDefaultValues() {

        $_GET[] = array();
        $_POST[] = array();

        $expect = [

            SYSTEM_DB_1_USER => '<DBUSER>',
            SYSTEM_DB_1_SERVER => '<DBSERVER>',
            SYSTEM_DB_1_PASSWORD => '<DBPW>',
            SYSTEM_DB_1_NAME => '<DB>',
            SYSTEM_DOCUMENTATION_QFQ => SYSTEM_DOCUMENTATION_QFQ_URL,
            SYSTEM_FLAG_PRODUCTION => 'yes',
            SYSTEM_THUMBNAIL_DIR_SECURE_REL_TO_APP => Path::APP_TO_SYSTEM_THUMBNAIL_DIR_SECURE_DEFAULT,
            SYSTEM_THUMBNAIL_DIR_PUBLIC_REL_TO_APP => Path::APP_TO_SYSTEM_THUMBNAIL_DIR_PUBLIC_DEFAULT,
            SYSTEM_CMD_INKSCAPE => 'inkscape',
            SYSTEM_CMD_CONVERT => 'convert',
            SYSTEM_CMD_PDF2SVG => 'pdf2svg',
            SYSTEM_CMD_PDFTOCAIRO => 'pdftocairo',
            SYSTEM_CMD_WKHTMLTOPDF => '/opt/wkhtmltox/bin/wkhtmltopdf',
            SYSTEM_DATE_FORMAT => 'yyyy-mm-dd',
            SYSTEM_THROW_GENERAL_ERROR => 'no',
            SYSTEM_SQL_LOG_MODE => SQL_LOG_MODE_MODIFY,
            SYSTEM_SHOW_DEBUG_INFO => SYSTEM_SHOW_DEBUG_INFO_AUTO,
            SYSTEM_REPORT_MIN_PHP_VERSION => SYSTEM_REPORT_MIN_PHP_VERSION_AUTO,
            SYSTEM_DB_INIT => 'set names utf8',
            SYSTEM_DB_UPDATE => SYSTEM_DB_UPDATE_AUTO,
            SYSTEM_DB_INDEX_DATA => '1',
            SYSTEM_DB_INDEX_QFQ => '1',
            SYSTEM_ESCAPE_TYPE_DEFAULT => 'm',
            SYSTEM_SECURITY_VARS_HONEYPOT => SYSTEM_SECURITY_VARS_HONEYPOT_NAMES,
            SYSTEM_SECURITY_ATTACK_DELAY => 5,
            SYSTEM_SECURITY_SHOW_MESSAGE => '0',
            SYSTEM_SECURITY_GET_MAX_LENGTH => 50,
            SYSTEM_RECORD_LOCK_TIMEOUT_SECONDS => SYSTEM_RECORD_LOCK_TIMEOUT_SECONDS_DEFAULT,
            SYSTEM_ENTER_AS_SUBMIT => 1,
            SYSTEM_EDIT_FORM_PAGE => 'form',
            SYSTEM_EXTRA_BUTTON_INFO_INLINE => '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>',
            SYSTEM_EXTRA_BUTTON_INFO_BELOW => '<span class="glyphicon glyphicon-info-sign text-info" aria-hidden="true"></span>',
            SYSTEM_EXTRA_BUTTON_INFO_CLASS => '',
            SYSTEM_NOTE_BUTTON_CLASS => 'btn btn-default navbar-btn',

            // Deprecated
            SYSTEM_SAVE_BUTTON_TEXT => '',
            SYSTEM_SAVE_BUTTON_TOOLTIP => 'Save', // SAVE
            SYSTEM_SAVE_BUTTON_CLASS => 'btn btn-default navbar-btn',
            SYSTEM_SAVE_BUTTON_GLYPH_ICON => 'glyphicon-ok', // glyphicon-ok
            SYSTEM_CLOSE_BUTTON_TEXT => '',
            SYSTEM_CLOSE_BUTTON_TOOLTIP => 'Close', // Close
            SYSTEM_CLOSE_BUTTON_CLASS => 'btn btn-default navbar-btn',
            SYSTEM_CLOSE_BUTTON_GLYPH_ICON => 'glyphicon-remove', // glyphicon-remove
            SYSTEM_DELETE_BUTTON_TEXT => '',
            SYSTEM_DELETE_BUTTON_TOOLTIP => 'Delete', // Delete
            SYSTEM_DELETE_BUTTON_CLASS => 'btn btn-default navbar-btn',
            SYSTEM_DELETE_BUTTON_GLYPH_ICON => 'glyphicon-trash', // glyphicon-trash
            SYSTEM_NEW_BUTTON_TEXT => '',
            SYSTEM_NEW_BUTTON_TOOLTIP => 'New', // New
            SYSTEM_NEW_BUTTON_CLASS => 'btn btn-default navbar-btn',
            SYSTEM_NEW_BUTTON_GLYPH_ICON => 'glyphicon-plus', // glyphicon-plus
            SYSTEM_SUBRECORD_EMPTY => 'show',
            F_BS_COLUMNS => 'col-md-12 col-lg-8',
            F_BS_LABEL_COLUMNS => 'col-md-3 col-lg-3',
            F_BS_INPUT_COLUMNS => 'col-md-6 col-lg-6',
            F_BS_NOTE_COLUMNS => 'col-md-3 col-lg-3',
            F_FE_DATA_REQUIRED_ERROR => F_FE_DATA_REQUIRED_ERROR_DEFAULT,
            F_FE_DATA_MATCH_ERROR => F_FE_DATA_MATCH_ERROR_DEFAULT,
            F_FE_DATA_ERROR => F_FE_DATA_ERROR_DEFAULT,
            F_CLASS_PILL => 'qfq-color-grey-1',
            F_CLASS_BODY => 'qfq-color-grey-2',
            F_BUTTON_ON_CHANGE_CLASS => 'btn-info alert-info',
            SYSTEM_DB_NAME_DATA => '<DB>',
            SYSTEM_DB_NAME_QFQ => '<DB>',
            'DB_2_USER' => '<DBUSER>',
            'DB_2_SERVER' => '<DBSERVER>',
            'DB_2_PASSWORD' => '<DBPW>',
            'DB_2_NAME' => '<DB>',
            'DB_3_USER' => '<DBUSER>',
            'DB_3_SERVER' => '<DBSERVER>',
            'DB_3_PASSWORD' => '<DBPW>',
            'DB_3_NAME' => '<DB>',
            'LDAP_1_RDN' => 'LDAP_1_RDN',
            'LDAP_1_PASSWORD' => 'LDAP_1_PASSWORD',
            SYSTEM_LABEL_ALIGN => SYSTEM_LABEL_ALIGN_LEFT,
            F_FE_DATA_PATTERN_ERROR => F_FE_DATA_PATTERN_ERROR_DEFAULT,
            F_FE_DATA_PATTERN_ERROR_SYSTEM => F_FE_DATA_PATTERN_ERROR_DEFAULT,
            SYSTEM_SECURITY_FAILED_AUTH_DELAY => '3',
            SYSTEM_SQL_LOG_MODE_AUTOCRON => 'error',
            F_CLASS => 'qfq-notify',
            SYSTEM_RENDER => SYSTEM_RENDER_SINGLE,
            SYSTEM_CMD_QPDF => 'qpdf',
            SYSTEM_CMD_GS => 'gs',
            SYSTEM_CMD_PDFUNITE => 'pdfunite',
            SYSTEM_CMD_IMG2PDF => 'img2pdf',
            SYSTEM_CMD_HEIF_CONVERT => 'heif-convert',
            SYSTEM_CMD_QFQPDF => '/opt/qfqpdf/qfqpdf',
            SYSTEM_ENCRYPTION_METHOD => 'AES-128',
            SYSTEM_CMD_PDF2PS => 'pdf2ps',
            SYSTEM_CMD_PS2PDF => 'ps2pdf',
            SYSTEM_REMEMBER_LAST_PILL => 1,
            SYSTEM_SHOW_HISTORY => 0,
            SYSTEM_DATE_TIME_PICKER_TYPE => 'qfq',
            SYSTEM_UPLOAD_TYPE => UPLOAD_TYPE_DND,
            SYSTEM_HTTP_ORIGIN => 'http://' . DOMAIN_EXAMPLE_COM,
            SYSTEM_IMAGE_UPLOAD_DIR => 'fileadmin/imageUploadDir',
            SYSTEM_BASE_URL_LANG => HTTP_EXAMPLE_COM,
            SYSTEM_FORCE_SMTP_SENDER => '',
            SYSTEM_CACHE_DIR_SECURE => SYSTEM_CACHE_DIR_SECURE_DEFAULT,
            SYSTEM_CACHE_PURGE_FILES_OLDER_DAYS => SYSTEM_CACHE_PURGE_FILES_OLDER_DAYS_DEFAULT,
            SYSTEM_FIELDSET_CLASS => SYSTEM_FIELDSET_CLASS_DEFAULT,
//            SYSTEM_DO_NOT_LOG_COLUMN => SYSTEM_DO_NOT_LOG_COLUMN_DEFAULT,
            SYSTEM_PROTECTED_FOLDER_CHECK => 1,
            SYSTEM_QFQ_VERSION => QFQ_VERSION_EXAMPLE,
            SYSTEM_CMD_WGET => 'wget >/dev/null 2>&1',
            SYSTEM_IMAP_LOG_MODE => 'error',
            F_UPLOAD_SUCCESS_MESSAGE => 'Upload complete',
            F_UPLOAD_IDLE_TEXT => 'Drag & drop or choose file',
            SYSTEM_RECENT_LOG=> 1,
        ];

        $body = json_encode([
            'DB_1_USER' => '<DBUSER>',
            'DB_1_SERVER' => '<DBSERVER>',
            'DB_1_PASSWORD' => '<DBPW>',
            'DB_1_NAME' => '<DB>',

            'DB_2_USER' => '<DBUSER>',
            'DB_2_SERVER' => '<DBSERVER>',
            'DB_2_PASSWORD' => '<DBPW>',
            'DB_2_NAME' => '<DB>',

            'DB_3_USER' => '<DBUSER>',
            'DB_3_SERVER' => '<DBSERVER>',
            'DB_3_PASSWORD' => '<DBPW>',
            'DB_3_NAME' => '<DB>',

            'LDAP_1_RDN' => 'LDAP_1_RDN',
            'LDAP_1_PASSWORD' => 'LDAP_1_PASSWORD',
        ], JSON_PRETTY_PRINT);

        $absoluteTemporaryConfigFilePath = $this->createFile($body);
        $this->store = Store::getInstance('', true, $absoluteTemporaryConfigFilePath);
        unlink($absoluteTemporaryConfigFilePath);

        $config = $this->store->getStore(STORE_SYSTEM);

        # The following won't be checked by content, cause they will change on different installations.
        foreach ([SYSTEM_SESSION_TIMEOUT_SECONDS, SYSTEM_QFQ_LOG_PATHFILENAME,
                     SYSTEM_SQL_LOG_PATHFILENAME, SYSTEM_MAIL_LOG_PATHFILENAME, SYSTEM_FILE_MAX_FILE_SIZE,
                     SYSTEM_QFQ_PROJECT_PATH, SYSTEM_MERGE_LOG_PATHFILENAME, SYSTEM_IMAP_LOG_PATHFILENAME] as $key) {
            $this->assertTrue(isset($config[$key]), "Missing default value for '$key' ");
            unset ($config[$key]);
        }

        # Depends where the unit test runs
        if (false !== strpos($config[SYSTEM_BASE_URL], FAILED_TO_SET_BASE_URL) || false !== strpos($config[SYSTEM_BASE_URL], 'example.com')) {
            $expect[SYSTEM_BASE_URL] = $config[SYSTEM_BASE_URL];
            $expect[SYSTEM_BASE_URL_LANG] = $config[SYSTEM_BASE_URL_LANG];
        }

        // check default values
        $this->assertEquals($expect, $config, "Retrieve system store.");
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testGetStore() {
        $this->expectException(\UserFormException::class);
        $this->assertEquals(array(), $this->store->getStore('unknownstore'));
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function testSanitizeNonDefault() {
        foreach ([STORE_FORM, STORE_RECORD, STORE_SIP, STORE_BEFORE, STORE_PARENT_RECORD,
                     STORE_TABLE_DEFAULT, STORE_TABLE_COLUMN_TYPES, STORE_CLIENT, STORE_TYPO3,
                     STORE_VAR, STORE_SYSTEM, STORE_USER, STORE_LDAP] as $storeName) {

            $this->store->setVar('color', 'green', $storeName);
            if ($this->store->getSanitizeStore($storeName)) {
                $this->assertEquals('!!digit!!', $this->store->getVar('color', $storeName, SANITIZE_ALLOW_DIGIT), "Retrieve 'color' from STORE $storeName");
            }
        }


        # var is not in R but in C. No sanitize given: C should complain
        $this->store->setVar('color1', 'green', STORE_CLIENT);
        $this->assertEquals('!!digit!!', $this->store->getVar('color1', 'RC'), "Retrieve 'color' from STORE_FORM");
    }

}
