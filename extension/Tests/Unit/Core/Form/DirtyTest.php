<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/2/16
 * Time: 8:47 AM
 */

namespace IMATHUZH\Qfq\Tests\Unit\Core\Form;

use IMATHUZH\Qfq\Core\Database\Database;

use IMATHUZH\Qfq\Core\Form\Dirty;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Tests\Unit\Core\Database\AbstractDatabaseTest;

require_once(__DIR__ . '/../Database/AbstractDatabaseTest.php');

const MSG_RECORD_ALREADY_LOCKED = 'Record already locked';

/*
 * Open to check
 * - FORM_DELETE
 */

/*
 * 1. DirtyMode: none
 * ===============
 * r=0, Alice lock, Alice release first simple unit tests for record locking.-k
 * r>0, Alice lock, Alice release
 * Bob lock, Alice lock
 *
 * 2. DirtyMode: advisory
 * ===================
 * r=0, Alice lock, Alice release
 * r>0, Alice lock, Alice release
 * Alice lock, Alice timeout, Alice release
 * Alice release
 * Alice lock, Record modified by someone else, Alice release
 * Bob lock, Alice lock
 * Bob lock, Bob timeout, Alice lock
 * Alice lock 1, Alice lock 2, Bob lock 3, Bob release 3, Alice release 2, Alice release 1
 * Alice lock 1 Form A, Bob lock 1 Form B
 *
 * 3. DirtyMode: exclusive
 * ====================
 * r=0, Alice lock, Alice release
 * r>0, Alice lock, Alice release
 * Alice lock, Alice timeout, Alice release
 * Alice release
 * Alice lock, Record modified by someone else, Alice release
 * Bob lock, Alice lock
 * Bob lock, Bob timeout, Alice lock
 * Alice lock 1, Alice lock 2, Bob lock 3, Bob release 3, Alice release 2, Alice release 1
 * Alice lock 1 Form A, Bob lock 1 Form B
 *
 * 4. Mix Mode
 * ===========
 *
 * For all: Alice lock 1 Form A, Bob lock 1 Form B
 *   Form A: none, Form B: exclusive
 *   Form A: none, Form B: advisory
 *   Form A: advisory, Form B: none
 *   Form A: advisory, Form B: exclusive
 *   Form A: exclusive, Form B: none
 *   Form A: exclusive, Form B: advisory
 *
 *
 * 5. Special
 * ==========
 *
 * Compute expire time
 * Lock non existing record
 */

/**
 * Class DirtyTest
 */
class DirtyTest extends AbstractDatabaseTest {

    /**
     * @var Sip Instance of class SIP
     */
    protected $sip = null;

    /**
     * @var Database[] - Array of Database instantiated class
     */
    protected $dbArray = null;

    /**********************************************************************
     * 1. DirtyMode: none
     */


    /**
     * r=0, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testNoneR0AliceLockRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=0&form=lockNone", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     * r=1, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testNoneR1AliceLockRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     *  Alice lock, Record modified by someone else, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testNoneAliceLockBobLock() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);

        // Alice lock - but change cookie later to Bob
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $dirty = new Dirty();

        $result = $dirty->process();

        // move lock to another owner (Alice fake becomes Bob) - but there is no lockrecord!
        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET qfqUserSessionCookie='SessionCookieBob' WHERE id=1", ROW_REGULAR);

        // Alice lock again
        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];

        $this->assertEquals($expected, $result);
    }

    /**********************************************************************
     * 1. DirtyMode: advisory
     */

    /**
     * r=0, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryR0AliceLockRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=0&form=lockAdvisory", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     * r=1, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryR1AliceLockRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_1);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     * Alice lock, Alice timeout, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryAliceLockTimeoutRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Timeout
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET expire='2001-01-01 00:00:00' WHERE 1", ROW_REGULAR);
        $this->assertEquals(1, $rc, 'Expect that exactly one record has been updated');

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     *  Alice lock, Record modified by someone else, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryAliceLockModifiedRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Person SET modified='2001-01-01 13:14:15' WHERE id=1", ROW_REGULAR);
        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // change modified
        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Person SET modified='2002-02-02 16:17:18' WHERE id=1", ROW_REGULAR);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();

        //TODO: this should be enabled again.
//        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => 'The record has been modified in the meantime. Please reload the form, edit and save again.'];
//        $this->assertEquals($expected, $result);
    }

    /**
     *  Bob lock, Alice lock
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryBobLockAliceLock() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);

        // Alice lock - but change cookie later to Bob
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        // move lock to another owner (Alice fake becomes Bob)
        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET qfqUserSessionCookie='SessionCookieBob' WHERE id=1", ROW_REGULAR);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        // Alice lock again
        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT_ALLOW_FORCE, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }


    /**
     * Bob lock, Bob timeout, Alice lock
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryBobLockTimeoutAliceLock() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);

        // Alice fake (becomes Bob) lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Set timeout to expired, Change Alice to Bob
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET expire='2001-01-01 00:00:00', qfqUserSessionCookie='SessionCookieBob' WHERE 1", ROW_REGULAR);
        $this->assertEquals(1, $rc, 'Expect that exactly one record has been updated');

        // Alice lock
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);
    }

    /**
     * Alice lock 1, Alice lock 2, Bob lock 3, Bob release 3, Alice release 2, Alice release 1
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryAliceLock1AliceLock2BobLock3BobRelease3AliceRelease2AliceRelease1() {


        // Alice lock 1
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Alice lock 2
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=2&form=lockAdvisory", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Bob becomes logged in user
        $_COOKIE[SESSION_NAME] = 'SessionCookieBob';

        // Create third Person record
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("INSERT INTO Person (`name`, `firstName`) VALUES ('no', 'name')", ROW_REGULAR);

        // Bob lock 3
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=3&form=lockAdvisory", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // RELEASE

        // Bob Release 3
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        $session = Session::getInstance(true);
        $session::clearAll();

        // Alice Release 2
        $_COOKIE[SESSION_NAME] = 'SessionCookieAlice';
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;

        // with phpunit, we only have one sip at a time
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=2&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        // Alice Release 1
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        // with phpunit, we only have one sip at a time
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);
    }

    /**
     *  Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAdvisoryAliceFormALock1BobFormBLock1() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory2", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT_ALLOW_FORCE, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**********************************************************************
     * 3. DirtyMode: exclusive
     */

    /**
     * r=0, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveR0AliceLockRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=0&form=lockExclusive", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     * r=1, Alice lock, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveR1AliceLockRelease() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_1);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     * Alice lock, Alice timeout, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveAliceLockTimeoutRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Timeout
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET expire='2001-01-01 00:00:00' WHERE 1", ROW_REGULAR);
        $this->assertEquals(1, $rc, 'Expect that exactly one record has been updated');

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT id FROM Dirty", ROW_EXPECT_0);
    }

    /**
     */
//    public function testExclusiveAliceReleaseException() {
//
//          $this->expectException(\UserFormException::class);
//        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
//
//        // Alice release
//        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
//        $dirty = new Dirty();
//
//        $dirty->process();
//    }

    /**
     *  Alice lock, Record modified by someone else, Alice release
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveAliceLockModifiedRelease() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Person SET modified='2001-01-01 13:14:15' WHERE id=1", ROW_REGULAR);
        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // change modified
        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Person SET modified='2002-02-02 16:17:18' WHERE id=1", ROW_REGULAR);

        // Alice release
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();

        //TODO: this should be enabled again.
//        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => 'The record has been modified in the meantime. Please reload the form, edit and save again.'];
//        $this->assertEquals($expected, $result);
    }

    /**
     *  Bob lock, Alice lock
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveBobLockAliceLock() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice lock - but change cookie later to Bob
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        // move lock to another owner (Alice fake becomes Bob)
        $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET qfqUserSessionCookie='SessionCookieBob' WHERE id=1", ROW_REGULAR);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        // Alice lock again
        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**
     *  Bob lock, Bob timeout, Alice lock
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveBobLockTimeoutAliceLock() {

        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice fake (becomes Bob) lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Set timeout to expired, Change Alice to Bob
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("UPDATE Dirty SET expire='2001-01-01 00:00:00', qfqUserSessionCookie='SessionCookieBob' WHERE 1", ROW_REGULAR);
        $this->assertEquals(1, $rc, 'Expect that exactly one record has been updated');

        // Alice lock
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);
    }

    /**
     * Alice lock 1, Alice lock 2, Bob lock 3, Bob release 3, Alice release 2, Alice release 1
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveAliceLock1AliceLock2BobLock3BobRelease3AliceRelease2AliceRelease1() {


        // Alice lock 1
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Alice lock 2
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=2&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // Bob becomes logged in user
        $_COOKIE[SESSION_NAME] = 'SessionCookieBob';

        // Create third Person record
        $rc = $this->dbArray[DB_INDEX_DEFAULT]->sql("INSERT INTO Person (`name`, `firstName`) VALUES ('no', 'name')", ROW_REGULAR);

        // Bob lock 3
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=3&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();
        $this->assertEquals($expected, $result);

        // RELEASE

        // Bob Release 3
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        $session = Session::getInstance(true);
        $session::clearAll();

        // Alice Release 2
        $_COOKIE[SESSION_NAME] = 'SessionCookieAlice';
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;

        // with phpunit, we only have one sip at a time
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=2&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        // Alice Release 1
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_RELEASE;
        // with phpunit, we only have one sip at a time
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);
    }

    /**
     *  Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExclusiveAliceFormALock1BobFormBLock1() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive2", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**********************************************************************
     * 4. Mix Mode
     */

    /**
     * Form A: none, Form B: exclusive
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1NoneBobLock1Exclusive() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);
    }

    /**
     * Form A: none, Form B: advisory
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1NoneBobLock1Advisory() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => ''];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);
    }

    /**
     * Form A: advisory, Form B: none
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1AdvisoryBobLock1None() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT_ALLOW_FORCE, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**
     * Form A: advisory, Form B: Exclusive
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1AdvisoryBobLock1Exclusive() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = 'Record already locked ';
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**
     * Form A: exclusive, Form B: none
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1ExclusiveBobLock1None() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockNone", RETURN_SIP);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /**
     * Form A: exclusive, Form B: Advisory
     * Alice lock 1 Form A, Bob lock 1 Form B
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testAliceLock1ExclusiveBobLock1Advisory() {

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 1 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $_GET[TAB_UNIQ_ID] = '1';
        $dirty = new Dirty();

        $result = $dirty->process();

        $expected = [API_STATUS => API_ANSWER_STATUS_SUCCESS, API_MESSAGE => '', API_LOCK_TIMEOUT => 900];
        $this->assertEquals($expected, $result);

        // Create clean environment
        $this->clean('SessionCookieBob');

        // Bob lock 1 Form B
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockAdvisory", RETURN_SIP);
        $_GET[TAB_UNIQ_ID] = '2';
        $dirty = new Dirty();

        $result = $dirty->process();

        $msg = MSG_RECORD_ALREADY_LOCKED;
        $expected = [API_STATUS => API_ANSWER_STATUS_CONFLICT, API_MESSAGE => $msg];

        // cut IP, User and Timestamp
        $result[API_MESSAGE] = substr($result[API_MESSAGE], 0, strlen($msg));

        $this->assertEquals($expected, $result);
    }

    /****************************************************
     * 5. Special
     */

    /**
     *  Compute expire time
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testExpireTime() {

        $this->dbArray[DB_INDEX_DEFAULT]->sql("DELETE FROM Dirty WHERE 1", ROW_REGULAR);
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=1&form=lockExclusive", RETURN_SIP);

        // Alice lock
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $dirty = new Dirty();

        $result = $dirty->process();

        // Check that the expire time is correctly computed
        $row = $this->dbArray[DB_INDEX_DEFAULT]->sql("SELECT expire FROM Dirty", ROW_EXPECT_1);
        $secs = abs(strtotime($row[DIRTY_EXPIRE]) - strtotime(date('Y-m-d H:i:s')) - 900);
        $this->assertLessThan(2, $secs);
    }

    /**
     *  Lock non existing record
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function testLockNonExistingRecord() {

        $this->expectException(\DbException::class);

        // Create clean environment
        $this->clean('SessionCookieAlice');

        // Alice lock 9999 Form A
        $_GET[CLIENT_SIP] = $this->sip->queryStringToSip("input?r=9999&form=lockExclusive", RETURN_SIP);
        $_GET[DIRTY_API_ACTION] = DIRTY_API_ACTION_LOCK;
        $dirty = new Dirty();

        $result = $dirty->process();
    }

    /**
     * Clear Session to have a new clean environment
     *
     * @param string $cookie
     * @throws \CodeException
     */
    private function clean($cookie = '') {

        $session = Session::getInstance(true);
        $session::clearAll();

        if ($cookie != '') {
            $_COOKIE[SESSION_NAME] = $cookie;
        }

    }

    /************************************************
     * Setup
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function setUp(): void {

        Session::getInstance(true);

        parent::setUp();

        try {
            $this->executeSQLFile(__DIR__ . '/../Database/fixtures/TestFormEditor.sql', true);
            $this->executeSQLFile(__DIR__ . '/../Database/fixtures/Generic.sql', true);
            $this->executeSQLFile(__DIR__ . '/../Database/fixtures/TestDirty.sql', true);
        } catch (\Exception $e) {
            echo $e->getMessage();
            return;
        }

        $this->sip = new Sip(true);
        $this->sip->sipUniqId('badcaffee1234');
        $_COOKIE[SESSION_NAME] = 'SessionCookieAlice';
    }

    /**
     *
     */
    protected function tearDown(): void {
        parent::tearDown();

        self::$mysqli->real_query("TRUNCATE TABLE FormElement");
        self::$mysqli->real_query("TRUNCATE TABLE Dirty");
        self::$mysqli->real_query("TRUNCATE TABLE Form");
        //self::$mysqli->real_query("DELETE FROM Form WHERE name='form' AND id!=1000");
    }
}
