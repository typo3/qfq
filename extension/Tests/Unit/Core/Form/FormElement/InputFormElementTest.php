<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Form\FormElement;


use IMATHUZH\Qfq\Core\Form\FormElement\InputFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

class InputFormElementTest extends FormElementTest {
    public function testConstructFormElement() {
        $this->expectNotToPerformAssertions();
        $this->prepareFormElementArray(FE_TYPE_TEXT);
        $this->feAttributes[FE_MAX_LENGTH] = 255;
        $inputFormElement = new InputFormElement($this->feAttributes);
    }
}