<?php

namespace IMATHUZH\Qfq\Tests\Unit\Core\Form\FormElement;

use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Helper\Support;
use PHPUnit\Framework\TestCase;

abstract class FormElementTest extends TestCase {
    protected array $feAttributes = array();

    public abstract function testConstructFormElement();

    /**
     * Prepares the form element array that contains the FE's attributes.
     * This array is used to instantiate the FE.
     * It should be called by any deriving classes.
     * @param string $feType
     * @return void
     */
    protected function prepareFormElementArray(string $feType) {
        $this->feAttributes[FE_TYPE] = $feType;
        $this->feAttributes[FE_ID] = 1;
        $this->feAttributes[FE_NAME] = 'testFormElement';
        $this->feAttributes[FE_CHECK_PATTERN] = '';
        $this->feAttributes[FE_PLACEHOLDER] = '';
        $this->feAttributes[F_FE_DATA_MATCH_ERROR] = '';
        $this->feAttributes[F_FE_DATA_ERROR] = '';
        $this->feAttributes[FE_DYNAMIC_UPDATE] = '';
        $this->feAttributes[FE_TOOLTIP] = '';

        $this->feAttributes = Support::setFeDefaults($this->feAttributes);
    }
}
