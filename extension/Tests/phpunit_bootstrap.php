<?php

use IMATHUZH\Qfq\Core\Helper\Path;

require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * @param $dir
 * @return bool
 */
function delTree($dir) {
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

Path::setProjectToForm(Path::PROJECT_TO_FORM_PHPUNIT);
Path::setProjectToReport(Path::PROJECT_TO_REPORT_PHPUNIT);

// remove old form_phpunit directory and all its content
$absoluteForm = Path::absoluteProject(Path::PROJECT_TO_FORM_PHPUNIT);
if (file_exists($absoluteForm)) {
    $absoluteBackup = Path::join($absoluteForm, Path::FORM_TO_FORM_BACKUP);
    if (file_exists($absoluteBackup)) {
        delTree($absoluteBackup);
    }
    array_map('unlink', glob("$absoluteForm/*.json"));
    rmdir($absoluteForm);
}