# -*- coding: utf-8 -*-
import unittest
import qfqselenium


class TestBasicFunctionality(qfqselenium.QfqSeleniumTestCase):
    """
    this class contains the tests to test the implementation of qfq itself.
    """
    basic_form_report_path = "index.php?id=basicform"
    basic_form_report_header_text = "basicForm"

    # Buttons on report page (data-reference)
    button_new_record_ref = "addBasicData"
    button_delete_record_ref = "deleteBasicData"
    button_edit_record_ref = "editBasicData"

    # Form elements (data-reference)
    text_ref = "text"
    number_ref = "number"
    date_ref = "date"
    datetime_ref = "datetime"
    decimal_ref = "decimal"
    dropdown_ref = "enum"
    dynamic_update_ref = "dynamicUpdate"
    pill_text_ref = "pill_text"
    file_upload_ref = "file"

    # Form Pill names
    pill1_text = "pill1"
    pill2_text = "pill2"

    # Default form data
    default_date = "01.01.2188"
    default_datetime = "02.02.2188 01:08:39"
    default_decimal = "12.84"
    default_dropdown = "first option"
    default_radio = "option a"
    dynamic_update_radio = "option c"
    default_checkbox = "2"

    def _fill_in_default_data(self, unique_text, unique_number):
        """
        this function fills in the default values plus the given unique data into the form
        """
        s = self
        s.qfq_fill_text_field(s.text_ref, unique_text)
        s.qfq_fill_text_field(s.number_ref, unique_number)
        s.qfq_fill_text_field(s.date_ref, s.default_date)
        s.qfq_fill_text_field(s.datetime_ref, s.default_datetime)
        s.qfq_fill_text_field(s.decimal_ref, s.default_decimal)
        s.qfq_dropdown_select(s.dropdown_ref, s.default_dropdown)
        s.qfq_radio_select(s.default_radio)
        s.qfq_checkbox_select(s.default_checkbox)


    def test_qfq_canary(self):
        """
        this test should always succeed because it asserts if 1 equals 1.
        if it doesn't succeed there is probably something wrong with
        the environment.
        """
        s = self
        s.qfq_assert_true(1 == 1)

    def test_qfq_create_edit_delete(self):
        """
        create, save, edit, delete record
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)

        # edit record
        new_unique_text = s.qfq_generate_random_string()
        new_unique_number = str(s.qfq_generate_random_number())
        s.qfq_click_element_with_data_ref(self.button_edit_record_ref + unique_number)
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_clear_textfield(s.text_ref)
        s.qfq_fill_text_field(s.text_ref, new_unique_text)
        s.qfq_clear_textfield(s.number_ref)
        s.qfq_fill_text_field(s.number_ref, new_unique_number)

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(new_unique_text)
        s.qfq_assert_text_exists(new_unique_number)
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

        # delete
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + new_unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(new_unique_text)
        s.qfq_assert_text_absent(new_unique_number)

    def test_qfq_create_incomplete(self):
        """
        can't save incomplete form by pressing save or close + yes
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s.qfq_fill_text_field(s.text_ref, unique_text)
        s.qfq_fill_text_field(s.number_ref, unique_number)

        # save (incomplete form)
        s.qfq_click_save_form_button()

        # close + cancel
        s.qfq_click_close_form_button()
        s.qfq_click_alert_cancel()

        # close + yes (save)
        s.qfq_click_close_form_button()
        s.qfq_click_alert_yes()

        # close + no (don't save)
        s.qfq_click_close_form_button()
        s.qfq_click_alert_no()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_close_empty_form(self):
        """
        this test checks if an never edited new form can be closed
        without confirming.
        """
        s = self

        # open empty form
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)

        # close form
        s.qfq_click_close_form_button()
        s.qfq_assert_true('&s=' not in s.qfq_get_url())

        s.qfq_assert_header_exists(s.basic_form_report_header_text)

    def test_qfq_form_switch_pill(self):
        """
        Create record, switch to pill2, fill text
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_pill_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # fill text in pill2
        s.qfq_open_pill(s.pill2_text)
        s.qfq_fill_text_field(s.pill_text_ref, unique_pill_text)

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_assert_text_exists(unique_pill_text)

        # delete record
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_valid_file_upload(self):
        """
        this test checks if the file upload works
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())
        unique_file_name = s.qfq_generate_random_string(8)

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # upload valid file
        s.qfq_open_pill(s.pill2_text)
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "txt", 32)
        s.qfq_wait(1)

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_assert_text_exists(unique_file_name)

        # delete record
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_wrong_file_type_upload(self):
        """
        this test checks if a wrong file type throws
        an error when being uploaded
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())
        unique_file_name = s.qfq_generate_random_string(8)

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # upload file of type .rtf which is not allowed
        s.qfq_open_pill(s.pill2_text)
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "rtf", 32)
        s.qfq_click_alert_ok()

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_assert_text_absent(unique_file_name)

        # delete
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_wrong_to_large_file_upload(self):
        """
        this test checks if a file that is too large throws
        an error when being uploaded
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())
        unique_file_name = s.qfq_generate_random_string(8)

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # upload the file that is too large (128b, 64b max)
        s.qfq_open_pill(s.pill2_text)
        s.qfq_upload_file(s.file_upload_ref, unique_file_name, "txt", 128)
        s.qfq_click_alert_ok()

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_assert_text_absent(unique_file_name)

        # delete record
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

    def test_qfq_dynamic_update(self):
        """
        this test checks if the dynamic update field
        in the form gets saved correctly
        """
        s = self
        unique_text = s.qfq_generate_random_string()
        unique_dynamic_update_text = s.qfq_generate_random_string()
        unique_number = str(s.qfq_generate_random_number())

        # new record
        s.qfq_goto_page(s.basic_form_report_path)
        s.qfq_click_element_with_data_ref(s.button_new_record_ref)
        s._fill_in_default_data(unique_text, unique_number)

        # dynamic update
        s.qfq_assert_element_invisible(s.dynamic_update_ref)
        s.qfq_radio_select(s.dynamic_update_radio)
        s.qfq_assert_element_visible(s.dynamic_update_ref)
        s.qfq_fill_text_field(s.dynamic_update_ref, unique_dynamic_update_text)

        # save + close
        s.qfq_click_save_form_button()
        s.qfq_click_close_form_button()
        s.qfq_assert_text_exists(unique_text)
        s.qfq_assert_text_exists(unique_number)
        s.qfq_assert_text_exists(unique_dynamic_update_text)

        # delete record
        s.qfq_click_element_with_data_ref(s.button_delete_record_ref + unique_number)
        s.qfq_click_alert_ok()
        s.qfq_assert_text_absent(unique_text)
        s.qfq_assert_text_absent(unique_number)

if __name__ == "__main__":
    unittest.main()
