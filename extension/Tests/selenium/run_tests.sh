#!/bin/bash

# run a single test function. e.g. "test_basic_functionality.TestBasicFunctionality.test_qfq_form_switch_pill"
TEST_FUNCTION="" # NOTE: make sure there are no trailing spaces!

export SELENIUM_URL="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/NoT3Page"
export SELENIUM_BROWSER="chrome"
export SELENIUM_DRIVER_PATH="${PWD}/chromedriver"
export SELENIUM_HEADLESS="no"
export SELENIUM_SLOWDOWN=0

# download chromedriver (Chrome)
if [ ! -f "chromedriver" ]; then
  wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip &> /dev/null
  unzip /tmp/chromedriver.zip chromedriver &> /dev/null
  chmod +x chromedriver &> /dev/null
  echo -e "Successfully downloaded chromedriver"
fi

if [ -z "$TEST_FUNCTION" ]; then
  python3 -W ignore -m unittest discover
else
  python3 -W ignore -m unittest "$TEST_FUNCTION"
fi

exit 0
