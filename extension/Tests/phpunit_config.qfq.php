<?php

/**
 * This file is copied to extension/ for phpunit execution without typo3 on gitlab runner. See Makefile > phpunit
 */

// QFQ configuration
//
// Save this file as: <site path>/typo3conf/config.qfq.php

return [
    'DB_1_USER' => 'phpunit',
    'DB_1_SERVER' => 'localhost',
    'DB_1_PASSWORD' => '#PHPUNIT_PASSWORD#',
    'DB_1_NAME' => 'phpunit_qfq',
];