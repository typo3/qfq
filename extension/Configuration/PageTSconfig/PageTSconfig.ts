mod.wizards.newContentElement.wizardItems.special.elements.qfq_qfq {
    icon = EXT:qfq/ext_icon_lg.png
    title = QFQ Content Element
    description = Quick Form Query(QFQ) offers a Form Editor and a SQL based report syntax.
    tt_content_defValues {
        CType = qfq_qfq
    }
}
mod.wizards.newContentElement.wizardItems.special.show := addToList(qfq_qfq)