<?php
/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 5/3/19
 * Time: 6:30 PM
 */

return [
    'sql' => [
        'module' => 'cm/mode/sql/sql',
        'extensions' => ['sql'],
    ],
    'qfq' => [
        'module' => 'TYPO3/CMS/Qfq/codemirror/qfq',
    ],
    'html' => [
        'module' => 'cm/mode/htmlmixed/htmlmixed',
        'extensions' => ['htm', 'html'],
    ],
];
