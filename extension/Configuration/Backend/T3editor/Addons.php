<?php
/**
 * Created by PhpStorm.
 * User: bbaer
 * Date: 5/3/19
 * Time: 6:31 PM
 */

return [
    'edit/matchbrackets' => [
        'module' => 'cm/addon/edit/matchbrackets',
        'options' => [
            'matchBrackets' => true,
        ],
    ],
    'search/match-highlighter' => [
        'module' => 'cm/addon/search/match-highlighter',
        'options' => [
            'highlightSelectionMatches' => array('showToken' => true, "style" => "matchhighlight")
        ],
    ],
    'mode/overlay' => [
        'module' => 'cm/addon/mode/overlay',
    ],
    'hint/sql-hint' => [
        'module' => 'cm/addon/hint/sql-hint',
        'modes' => ['sql', 'qfq'],
        'cssFiles' => [
            'EXT:qfq/Resources/Public/Css/codemirror.css',
        ],
    ],
];
