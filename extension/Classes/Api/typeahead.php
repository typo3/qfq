<?php
/**
 * Created by PhpStorm.
 * User: ep
 * Date: 12/23/15
 * Time: 6:17 PM
 */


namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Form\TypeAhead;

/**
 * Return JSON encoded answer
 *
 */

// Detect API calls if this is defined. Do not try use "const QFQ_API 'Api call'" in Constanst.php : this will break detection of API Calls
define('QFQ_API', 'Api call');

try {
    $typeAhead = new TypeAhead();

    $answer = $typeAhead->process();

} catch (\Throwable $e) {
//    $answer[API_MESSAGE] = "Generic Exception: " . $e->getMessage();
    $answer = [API_TYPEAHEAD_KEY => 'Error', API_TYPEAHEAD_VALUE => "Error: " . $e->getMessage()];
}

header("Content-Type: application/json");
echo json_encode($answer);

