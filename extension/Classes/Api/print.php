<?php
/**
 * Created by PhpStorm.
 * User: ep
 * Date: 12/23/15
 * Time: 6:17 PM
 */


namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Report\Html2Pdf;
use IMATHUZH\Qfq\Core\Store\Config;


/**
 * Main
 */

// Detect API calls if this is defined. Do not try use "const QFQ_API 'Api call'" in Constanst.php : this will break detection of API Calls
define('QFQ_API', 'Api call');

try {
    $html2pdf = new Html2Pdf(Config::getConfigArray());

    $html2pdf->outputHtml2Pdf();

} catch (\Throwable $e) {
    echo "Exception: " . $e->getMessage();
}
