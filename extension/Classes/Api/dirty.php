<?php
/**
 * Created by PhpStorm.
 * User: ep
 * Date: 12/23/15
 * Time: 6:17 PM
 */

namespace IMATHUZH\Qfq\Api;

require_once(__DIR__ . '/../../vendor/autoload.php');

use IMATHUZH\Qfq\Core\Form\Dirty;

// Detect API calls if this is defined. Do not try use "const QFQ_API 'Api call'" in Constanst.php : this will break detection of API Calls
define('QFQ_API', 'Api call');

/**
 * Return JSON encoded answer
 *
 */
try {
    $dirty = new Dirty();

    $answer = $dirty->process();

} catch (\Throwable $e) {
//    $answer[API_MESSAGE] = "Generic Exception: " . $e->getMessage();
    $answer = [API_STATUS => API_ANSWER_STATUS_ERROR, API_MESSAGE => "Error: " . $e->getMessage()];
}

header("Content-Type: application/json");
echo json_encode($answer);


