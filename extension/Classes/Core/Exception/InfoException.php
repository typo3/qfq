<?php

/**
 * Created by PhpStorm.
 * User: jhaller
 * Date: 09.11.2023
 * Time: 12:47 PM
 */

use IMATHUZH\Qfq\Core\Exception\AbstractException;

/**
 * Class \InfoException
 *
 * Thrown by FormElement on User errors
 *
 * Throw with ONE message
 *
 *   Throw new \InfoException('Failed: sqlValidate()...');
 *
 *   Throw with custom message for User.
 *
 * Call
 *
 * @package Exception
 */
class InfoException extends AbstractException {

    /**
     * $this->getMessage() returns a simple string.
     *
     * There is only one message: shown in the client to the user - no details (timestamp, reference) here!!!
     *
     * @return string
     * @throws \InfoException
     */
    public function formatMessage() {

        return $this->formatException();
    }

    public function formatException() {

        // Get exception message and if JSON, decode it.
        $msg = $this->getMessage();

        return $this->formatMessageUser($msg);
    }

    private function formatMessageUser($msg) {

        $html = '<p>' . $msg . '</p>';

        return $html;
    }
}