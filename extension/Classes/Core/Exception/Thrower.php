<?php

namespace IMATHUZH\Qfq\Core\Exception;

class Thrower {
    /**
     * Throws UserFormException with given messages to user and developer.
     *
     * @param string $errorToUser
     * @param string $errorToDeveloper
     * @param int $errorCode
     * @throws \UserFormException
     */
    public static function userFormException(string $errorToUser, string $errorToDeveloper = '', int $errorCode = E_ERROR) {
        throw new \UserFormException(json_encode([
            ERROR_MESSAGE_TO_USER => $errorToUser,
            ERROR_MESSAGE_TO_DEVELOPER => $errorToDeveloper]), $errorCode);
    }

    /**
     * Throws UserReportException with given messages to user and developer.
     *
     * @param string $errorToUser
     * @param string $errorToDeveloper
     * @param int $errorCode
     * @throws \UserReportException
     */
    public static function userReportException(string $errorToUser, string $errorToDeveloper = '', int $errorCode = E_ERROR) {
        throw new \UserReportException(json_encode([
            ERROR_MESSAGE_TO_USER => $errorToUser,
            ERROR_MESSAGE_TO_DEVELOPER => $errorToDeveloper]), $errorCode);
    }

    /**
     * Throws DbException with given messages to user and developer.
     *
     * @param string $errorToUser
     * @param string $errorToDeveloper
     * @param bool $sanitizeHtml sanitize error messages from HTML
     * @param int $errorCode
     * @throws \DbException
     */
    public static function dbException(string $errorToUser, string $errorToDeveloper = '', bool $sanitizeHtml = true, int $errorCode = E_ERROR) {
        throw new \DbException(json_encode([
            ERROR_MESSAGE_TO_USER => $errorToUser,
            ERROR_MESSAGE_TO_DEVELOPER => $errorToDeveloper,
            ERROR_MESSAGE_TO_DEVELOPER_SANITIZE => $sanitizeHtml
        ]), $errorCode);
    }
}