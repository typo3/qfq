<?php

namespace IMATHUZH\Qfq\Core\Exception;

use CodeException;
use UserFormException;

/**
 * A generic exception thrown by element renderers
 */
class RendererException extends AbstractException {
    /**
     * @return string
     * @throws CodeException|UserFormException
     */
    public function formatMessage(): string {
        $this->messageArrayDebug[EXCEPTION_TYPE] = 'Renderer Exception';
        return parent::formatException();
    }
}
