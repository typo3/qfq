<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/1/16
 * Time: 11:28 PM
 */

use IMATHUZH\Qfq\Core\Exception\AbstractException;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class UserReportException
 *
 * Thrown by Report on User errors
 *
 * Throw with ONE message
 *
 *   throw new \UserFormException('Failed: chmod ....', ERROR_IO_CHMOD);
 *
 * Throw with message for User and message for Support.
 *
 * throw new \UserFormException(  json_encode(
 * [ERROR_MESSAGE_TO_USER => 'Failed: chmod',
 * ERROR_MESSAGE_SUPPORT => "Failed: chmod $mode '$pathFileName'",
 * ERROR_MESSAGE_HTTP_STATUS => 'HTTP/1.0 409 Bad Request' ]),
 * ERROR_IO_CHMOD);
 *
 * @package Exception
 */
class UserReportException extends AbstractException {

    // SUPER HACK: to render inline editor when an exception is thrown
    // Can't use store, since store needs bodytext to be parsed, which might throw exceptions if there is a syntax error.
    public static $report_uid = null;
    public static $report_bodytext = '';
    public static $report_pathFileName = '';
    public static $report_db = null;
    public static $report_header = '';


    /**
     * $this->getMessage() might give a) a simple string or b) an JSON String.
     *
     * JSON String: There are 3+1 different messages:
     *   [ERROR_MESSAGE_TO_USER] 'toUser' - shown in the client to the user - no details here!!!
     *   [ERROR_MESSAGE_SUPPORT] 'support' - help for the developer
     *   [ERROR_MESSAGE_OS] 'os' - message from the OS, like 'file not found'
     *
     * @return string HTML formatted error string
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function formatMessage() {

        $this->messageArrayDebug[EXCEPTION_TYPE] = 'User Report Exception';

        $this->messageArrayDebug[EXCEPTION_SQL_RAW] = Store::getVar(SYSTEM_SQL_RAW, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_SQL_FINAL] = Store::getVar(SYSTEM_SQL_FINAL, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_SQL_PARAM_ARRAY] = Store::getVar(SYSTEM_SQL_PARAM_ARRAY, STORE_SYSTEM);

        $this->messageArrayDebug[EXCEPTION_REPORT_COLUMN_INDEX] = Store::getVar(SYSTEM_REPORT_COLUMN_INDEX, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_REPORT_COLUMN_NAME] = Store::getVar(SYSTEM_REPORT_COLUMN_NAME, STORE_SYSTEM);
        $this->messageArrayDebug[EXCEPTION_REPORT_COLUMN_VALUE] = Store::getVar(SYSTEM_REPORT_COLUMN_VALUE, STORE_SYSTEM);

        return parent::formatException();
    }
}