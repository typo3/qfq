<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/17/17
 * Time: 9:20 PM
 */

use IMATHUZH\Qfq\Core\Exception\AbstractException;


/**
 * Class DownloadException
 *
 * Thrown by Download
 *
 * Throw with ONE message
 *
 *   throw new \UserFormException('Failed: chmod ....', ERROR_IO_CHMOD);
 *
 * Throw with message for User and message for Support.
 *
 * throw new \UserFormException(  json_encode(
 * [ERROR_MESSAGE_TO_USER => 'Failed: chmod',
 * ERROR_MESSAGE_SUPPORT => "Failed: chmod $mode '$pathFileName'",
 * ERROR_MESSAGE_HTTP_STATUS => 'HTTP/1.0 409 Bad Request' ]),
 * ERROR_IO_CHMOD);
 *
 * @package Exception
 */
class DownloadException extends AbstractException {

    /**
     * $this->getMessage() might give a) a simple string or b) an JSON String.
     *
     * JSON String: There are 3+1 different messages:
     *   [ERROR_MESSAGE_TO_USER] 'toUser' - shown in the client to the user - no details here!!!
     *   [ERROR_MESSAGE_SUPPORT] 'support' - help for the developer
     *   [ERROR_MESSAGE_OS] 'os' - message from the OS, like 'file not found'
     *
     * @return string HTML formatted error string
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function formatMessage() {

        $this->messageArrayDebug[EXCEPTION_TYPE] = 'Download Exception';

        return parent::formatException();
    }
}