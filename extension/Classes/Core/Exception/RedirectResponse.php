<?php

namespace IMATHUZH\Qfq\Core\Exception;

class RedirectResponse extends \Exception {
    public function __construct(
        string $locationUrl,
        int    $httpCode = 303
    ) {
        parent::__construct($locationUrl, $httpCode);
    }

    public function getLocationUrl() {
        return $this->getMessage();
    }
}
