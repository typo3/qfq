<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 6/1/16
 * Time: 8:52 AM
 */

namespace IMATHUZH\Qfq\Core;

use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Helper\HelperFile;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Store\Store;


/**
 * Class Delete
 * @package qfq
 */
class Delete {
    /**
     * @var Database
     */
    private $db = null;

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @param bool $dbIndexData
     * @param bool $phpUnit
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($dbIndexData = false, $phpUnit = false) {

        #TODO: rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}"
        $this->db = new Database($dbIndexData);
        $this->store = Store::getInstance('', $phpUnit);
    }

    /**
     * Deletes the record id=$recordId from table $tableName.
     * If the table has a column named COLUMN_PATH_FILE_NAME and the value of that specific record column points
     * to a file: delete such a file if their are no other records in the same table which also have a reference to
     * that file.
     *
     * @param string $tableName
     * @param integer $recordId
     *
     * @param string $primaryKey
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function process($tableName, $recordId, $primaryKey = F_PRIMARY_KEY_DEFAULT) {

        if ($this->store->getVar(FLAG_MULTIFORM, STORE_SIP) == 'true') {
            $recordId = $this->store->getVar(MULTIFORM_DELETE_ROW_ID, STORE_SIP);
        }


        if ($tableName === false || $tableName === '') {
            throw new \CodeException('Missing table name', ERROR_MISSING_TABLE_NAME);
        }

        if ($recordId === 0 || $recordId === '') {
            throw new \CodeException('Invalid record id', ERROR_MISSING_RECORD_ID);
        }

        // Take care the necessary target directories exist.
        $cwd = getcwd();
        $sitePath = Path::absoluteApp();
        if ($cwd === false || $sitePath === false || !HelperFile::chdir($sitePath)) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'getcwd() failed or SITE_PATH undefined or chdir() failed', ERROR_MESSAGE_TO_DEVELOPER => "getcwd() failed or SITE_PATH undefined or chdir('$sitePath') failed."]),
                ERROR_IO_CHDIR);

        }

        // Read record first.
        $row = $this->db->sql("SELECT * FROM `$tableName` WHERE `$primaryKey`=?", ROW_EXPECT_0_1, [$recordId]);
        if (count($row) > 0) {

            $this->deleteReferencedFiles($row, $tableName, $primaryKey);

            $this->db->sql("DELETE FROM `$tableName` WHERE `$primaryKey` =? LIMIT 1", ROW_REGULAR, [$recordId]);
        } else {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Record not found in table', ERROR_MESSAGE_TO_DEVELOPER => "Record $recordId not found in table '$tableName'."]),
                ERROR_RECORD_NOT_FOUND);

        }

        HelperFile::chdir($cwd);
    }

    /**
     * Iterates over array $row and searches for column names with substring COLUMN_PATH_FILE_NAME.
     * For any column found, check if it references a writable file.
     * If yes: check if there are other records (same table, same column) which references the same file.
     *         If no: delete the file
     *         If yes: do nothing, continue with the next column.
     * If no: do nothing, continue with the next column.
     *
     * @param array $row
     * @param       $tableName
     *
     * @param $primaryKey
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    private function deleteReferencedFiles(array $row, $tableName, $primaryKey) {

        foreach ($row as $key => $file) {

            if (false === strpos($key, COLUMN_PATH_FILE_NAME)) {
                continue;
            }

            // check if there is a file referenced in the record which have to be deleted too.
            if ($file !== '' && is_writable($file)) {

                // check if there are other records referencing the same file: do not delete the file now.
                // This check won't find duplicates, if they are spread over different columns or tables.
                $samePathFileName = $this->db->sql("SELECT COUNT($primaryKey) AS cnt FROM `$tableName` WHERE `$key` LIKE ?", ROW_EXPECT_1, [$file]);
                if ($samePathFileName['cnt'] === 1) {
                    HelperFile::unlink($file);
                    $this->db->deleteSplitFileAndRecord($row[$primaryKey], $tableName);
                }
            }
        }
    }

}