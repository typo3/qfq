<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 1/30/16
 * Time: 7:59 PM
 */

namespace IMATHUZH\Qfq\Core;

use HTMLPurifier;
use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Exception\Thrower;
use IMATHUZH\Qfq\Core\Form\Chat;
use IMATHUZH\Qfq\Core\Form\FormAction;
use IMATHUZH\Qfq\Core\Helper\EncryptDecrypt;
use IMATHUZH\Qfq\Core\Helper\HelperFile;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\KeyValueStringParser;
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\SqlQuery;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Store\FillStoreForm;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;
use ZipArchive;

/**
 * Class Save
 * @package qfq
 */
class Save {

    private $formSpec = array();  // copy of the loaded form
    private $feSpecAction = array(); // copy of all formElement.class='action' of the loaded form
    private $feSpecNative = array(); // copy of all formElement.class='native' of the loaded form
    private $feSpecNativeRaw = array(); // copy of all formElement.class='native' of the loaded form

    /**
     * @var FormAction
     */
    private $formAction = null;

    /**
     * @var null|Store
     */
    private $store = null;

    private $dbIndexData = false;
    private $dbIndexQfq = false;

    /**
     * @var Database[] - Array of Database instantiated class
     */
    protected $dbArray = array();

    private $evaluate = null;

    private $qfqLogFilename = '';

    /**
     * @param array $formSpec
     * @param array $feSpecAction
     * @param array $feSpecNative
     * @param array $feSpecNativeRaw
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct(array $formSpec, array $feSpecAction, array $feSpecNative, array $feSpecNativeRaw) {

        $this->formSpec = $formSpec;
        $this->feSpecAction = $feSpecAction;
        $this->feSpecNative = $feSpecNative;
        $this->feSpecNativeRaw = $feSpecNativeRaw;
        $this->store = Store::getInstance();

        $this->dbIndexData = $formSpec[F_DB_INDEX];
        $this->dbIndexQfq = $this->store->getVar(SYSTEM_DB_INDEX_QFQ, STORE_SYSTEM);

        $this->dbArray[$this->dbIndexData] = new Database($this->dbIndexData);

        if ($this->dbIndexData != $this->dbIndexQfq) {
            $this->dbArray[$this->dbIndexQfq] = new Database($this->dbIndexQfq);
        }

        $this->evaluate = new Evaluate($this->store, $this->dbArray[$this->dbIndexData]);
        $this->formAction = new FormAction($formSpec, $this->dbArray);

        $this->qfqLogFilename = Path::absoluteQfqLogFile();
    }

    /**
     * Starts save process. Returns recordId.
     *
     * @param $recordId
     * @return int
     * @throws \CodeException
     * @throws \DbException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function process($recordId) {
        $formAction = new FormAction($this->formSpec, $this->dbArray);

        // If developer is in formElement editor check for valid key and database field type
        $encryptionKey = trim($this->store->getVar(SYSTEM_ENCRYPTION_KEY, STORE_SYSTEM), " ");
        $formName = $this->store->getVar(SIP_FORM, STORE_SIP);
        if ($formName === FORM_NAME_FORM_ELEMENT || $formName === FORM_NAME_FORM){
            // Validate form and formElement params, modeSql, multiSql
            if ($this->store->getVar(SYSTEM_VALIDATE_FORM_ELEMENT, STORE_SYSTEM)) {
                HelperFormElement::validateFormRules($this->store->getStore(STORE_FORM));
            }
        }

        if ($formName === FORM_NAME_FORM_ELEMENT) {

            $encryptionState = $this->store->getVar(FE_ENCRYPTION, STORE_FORM, SANITIZE_ALLOW_ALL);
            $databaseFieldType = 'No real column';
            if ($encryptionState === 'yes') {
                if ($encryptionKey == '') {
                    throw new \UserFormException("Missing encryption key.", ERROR_MISSING_ENCRYPTION_KEY);
                }
                $indexData = $this->store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM, SANITIZE_ALLOW_ALL);
                if (!EncryptDecrypt::validDatabaseFieldType($indexData, $databaseFieldType)) {
                    throw new \UserFormException("Invalid database field type for encryption: " . $databaseFieldType . ". Only varchar or text.", ERROR_INVALID_DATABASE_FIELD_TYPE);
                }
            }
        } else {
            // Get all elements which are to encrypt and their information
            $elementsToEncrypt = EncryptDecrypt::getElementsToEncrypt($this->feSpecNative);

            // Database field type and length from each form element is needed
            $dbObject = $this->dbArray[$this->dbIndexData];
            $dbFieldAttributes = EncryptDecrypt::getDbFieldAttributes($this->formSpec[F_TABLE_NAME], $dbObject);

            if (EncryptDecrypt::hasEnoughSpace($elementsToEncrypt, $dbFieldAttributes)) {
                // Overwrite values in form store with encrypted values
                foreach ($elementsToEncrypt as $element) {
                    $this->store->setVar($element['name'], $element[ENCRYPTION_VALUE], STORE_FORM);
                }
            }
        }

        if ($this->formSpec[F_MULTI_SQL] == '') {
            // If an old record exist: load it. Necessary to delete uploaded files which should be overwritten.
            $this->store->fillStoreWithRecord($this->formSpec[F_TABLE_NAME], $recordId,
                $this->dbArray[$this->dbIndexData], $this->formSpec[F_PRIMARY_KEY]);

            $rc = $this->processSingle($recordId, $formAction);
        } else {
            $rc = $this->saveMultiForm($formAction);
        }

        return $rc;
    }

    /**
     * Process save of a form. Fire all action elements first
     *
     * @param $recordId
     * @param $formAction
     * @return int
     * @throws \CodeException
     * @throws \DbException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function processSingle($recordId, FormAction $formAction) {

        // Action: Before
        $feTypeList = FE_TYPE_BEFORE_SAVE . ',' . ($recordId == 0 ? FE_TYPE_BEFORE_INSERT : FE_TYPE_BEFORE_UPDATE);
        $formAction->elements($recordId, $this->feSpecAction, $feTypeList);

        $this->checkRequiredHidden();

        $rc = $this->elements($recordId);

        // Uploads are handled after processing native form elements.
        $this->processUploads($rc);
        $this->processAllImageCutFE($recordId);

        // Action: After*, Sendmail
        $feTypeList = FE_TYPE_SENDMAIL . ',' . FE_TYPE_AFTER_SAVE . ',' . ($recordId == 0 ? FE_TYPE_AFTER_INSERT : FE_TYPE_AFTER_UPDATE);

        $status = $formAction->elements($rc, $this->feSpecAction, $feTypeList);
        if ($status != ACTION_ELEMENT_NO_CHANGE) {
            // Reload fresh saved record and fill STORE_RECORD with it.
            $this->store->fillStoreWithRecord($this->formSpec[F_TABLE_NAME], $rc, $this->dbArray[$this->dbIndexData], $this->formSpec[F_PRIMARY_KEY]);
        }

        // Action: Paste
        $this->pasteClipboard($this->formSpec[F_ID] ?? '', $formAction);

        return $rc;
    }

    /**
     * @return bool  true if there is at least one paste record, else false.
     */
    private function isPasteRecord() {

        foreach ($this->feSpecAction as $formElement) {
            if ($formElement[FE_TYPE] == FE_TYPE_PASTE) {
                return true;
            }
        }

        return false;
    }

    /**
     * Iterate over all Clipboard source records and fire for each all FE.type=paste records.
     *
     * @param int $formId
     * @param FormAction $formAction
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function pasteClipboard($formId, FormAction $formAction) {

        if (!$this->isPasteRecord()) {
            return;
        }

        $cookieQfq = $this->store->getVar(CLIENT_COOKIE_QFQ, STORE_CLIENT, SANITIZE_ALLOW_ALNUMX);
        if ($cookieQfq === false || $cookieQfq == '') {
            throw new \UserFormException('Qfq Session missing', ERROR_QFQ_SESSION_MISSING);
        }

        # select clipboard records
        $sql = "SELECT c.idSrc as id, c.xId FROM `Clipboard` AS c WHERE `c`.`cookie`='$cookieQfq' AND `c`.`formIdPaste`=$formId ORDER BY `c`.`id`";
        $arrClipboard = $this->dbArray[$this->dbIndexQfq]->sql($sql);

        // Process clipboard records.
        foreach ($arrClipboard as $srcIdRecord) {
            $formAction->doAllFormElementPaste($this->feSpecAction, $this->formSpec[F_TABLE_NAME], $this->formSpec[F_TABLE_NAME], "", $srcIdRecord);
        }

    } # doClipboard()

    /**
     * @param $formAction
     * @return int|string
     * @throws \CodeException
     * @throws \DbException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function saveMultiForm(FormAction $formAction) {

        $parentRecords = $this->evaluate->parse($this->formSpec[F_MULTI_SQL], ROW_REGULAR);

        $processCounter = null;
        // No rows: This must be an error, cause MultiForms must have at least one record.
        if (empty($parentRecords)) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => $this->formSpec[F_MULTI_MSG_NO_RECORD],
                    ERROR_MESSAGE_TO_DEVELOPER => 'Query selects no records: ' . $this->formSpec[F_MULTI_SQL]]),
                ERROR_MISSING_EXPECT_RECORDS);
        }

        // Check for 'id' or '_id' as column name
        $idName = isset($parentRecords[0]['_' . F_MULTI_COL_ID]) ? '_' . F_MULTI_COL_ID : F_MULTI_COL_ID;

        // Author: Zhoujie Li
        // get all Element with id = 0
        $newRow = $this->retriveNewElements();
        // add the amount of new Element into $parentRecords as an array
        HelperFormElement::addGenericElements($parentRecords, count($newRow), $idName);
        // End from author


        // Check that an column 'id' is given
        if (!isset($parentRecords[0][$idName])) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Missing column "_' . F_MULTI_COL_ID . '"', ERROR_MESSAGE_TO_DEVELOPER => $this->formSpec[F_MULTI_SQL]]),
                ERROR_INVALID_OR_MISSING_PARAMETER);
        }

        $fillStoreForm = new FillStoreForm();
        $storeVarBase = $this->store->getStore(STORE_VAR);
        $flagCheckProcessRow = isset($this->formSpec[F_PROCESS_ROW]) && $this->formSpec[F_PROCESS_ROW] != '0';

        // Author: Zhoujie Li
        // Retrieve array keys from $newRow.
        $newRowIndexs = array_keys($newRow);
        // Initialize newRowCounter
        $newRowCounter = 0;
        // Loop through each record in the parent records array
        foreach ($parentRecords as $row) {
            // check if parameter processRow is set
            // Check if the current record ID is 0
            if ($row[$idName] == 0) {
                // Ensure $newRowCounter does not exceed the bounds of $newRowIndexs
                if (isset($newRowIndexs[$newRowCounter])) {
                    // Assign the current new row index to processCounter
                    $processCounter = $newRowIndexs[$newRowCounter];
                    // Increment newRowCounter
                    $newRowCounter++;
                } else {
                    // Handle the case where $newRowCounter exceeds the bounds
                    $processCounter = null;
                }
            } else {
                // Set processCounter to null if the current record ID is not 0
                $processCounter = null;
            }
            // End from author
            if ($flagCheckProcessRow) {

                $processRowName = HelperFormElement::buildFormElementName([FE_NAME => F_PROCESS_ROW_COLUMN], $row[$idName], $processCounter);

                if ('on' !== $this->store->getVar($processRowName, STORE_CLIENT . STORE_ZERO, SANITIZE_ALLOW_ALNUMX)) {
                    continue;
                }
            }

            // Always start with a clean STORE_VAR
            $this->store->setStore($storeVarBase, STORE_VAR, true);

            $this->store->setStore(OnArray::keyNameRemoveLeadingUnderscore($row), STORE_PARENT_RECORD, true);
            $this->store->setVar(F_MULTI_COL_ID, $row[$idName], STORE_PARENT_RECORD); // In case '_id' is used, both '_id' and 'id' should be accessible.

            $record = $this->dbArray[$this->dbIndexData]->sql('SELECT * FROM `' . $this->formSpec[F_TABLE_NAME] . '` WHERE `id`=' . $row[$idName], ROW_EXPECT_0_1);
            $this->store->setStore($record, STORE_RECORD, true);


            $id = $row[$idName];
            // if id = 0 then add new index 0-1, 0-2... to multisave records with id 0
            if ($row[$idName] === 0) {
                $id = $row[$idName] . HTML_DELIMITER_NAME . $processCounter;
            }
            // Fake current recordId
            $this->store->setVar(SIP_RECORD_ID, $id, STORE_SIP);
            $fillStoreForm->process(FORM_SAVE);

            $rc = $this->processSingle($row[$idName], $formAction);
        }

        // If no rows are selected, saving is still possible, thus requiring a null coalescing operator.
        return $rc ?? '';
    }

    /**
     * Get the newly added elements in the $_POST array using explode.
     * Grouped elements share the same last segment (index) after the last hyphen, e.g., -0-1.
     *
     * @return array
     */
    private function retriveNewElements() {
        $newElementsGroups = [];

        // Iterate over each item in the $_POST array
        foreach ($_POST as $key => $value) {
            // explode in to array with delimiter '-'
            $parts = explode('-', $key);
            // Check if the key fits the expected pattern and collect unique indexes
            if (count($parts) >= 3 && $parts[1] === '0') {
                // Get the last part, which is the index
                $index = end($parts);
                // Use the index as key to avoid duplicates
                $newElementsGroups[$index] = true;
            }
        }

        return $newElementsGroups;
    }


    /**
     * Create empty FormElements based on templateGroups, for those who not already exist.
     *
     * @param array $formValues
     *
     * @return array
     * @throws \UserFormException
     */
    private function createEmptyTemplateGroupElements(array $formValues) {

        foreach ($this->feSpecNative as $formElement) {

            switch ($formElement[FE_TYPE]) {
//                case FE_TYPE_EXTRA:
                case FE_TYPE_NOTE:
                case FE_TYPE_SUBRECORD:
                    continue 2;
                default:
                    break;
            }
            $feName = $formElement[FE_NAME];

            // #7705. Skip FE, which are not already expanded. Detect them by '%' (== '%d')
            if (!isset($formValues[$feName]) && false === stripos($feName, '%d') && $this->isMemberOfTemplateGroup($formElement)) {
                $formValues[$feName] = $formElement[FE_VALUE];
            }
        }

        return $formValues;
    }

    /**
     * Check if the current $formElement is member of a templateGroup.
     *
     * @param array $formElement
     * @param int $depth
     * @return bool
     * @throws \UserFormException
     */
    private function isMemberOfTemplateGroup(array $formElement, $depth = 0) {
        $depth++;

        if ($depth > 15) {
            throw new \UserFormException('FormElement nested too much (in each other - endless?): stop recursion',
                ERROR_FE_NESTED_TOO_MUCH);
        }

        if ($formElement[FE_TYPE] == FE_TYPE_TEMPLATE_GROUP) {
            return true;
        }

        if ($formElement[FE_ID_CONTAINER] == 0) {
            return false;
        }

        // Get the parent element
        $formElementArr = OnArray::filter($this->feSpecNativeRaw, FE_ID, $formElement[FE_ID_CONTAINER]);
        if (isset($formElementArr[0])) {
            return $this->isMemberOfTemplateGroup($formElementArr[0], $depth);
        }

        return false; // This should not be reached,
    }

    /**
     *
     * @param $feName
     *
     * @return bool
     */
    private function isSetEmptyMeansNull($feName) {

        $fe = OnArray::filter($this->feSpecNative, FE_NAME, $feName);

        $flag = isset($fe[0][FE_EMPTY_MEANS_NULL]) && $fe[0][FE_EMPTY_MEANS_NULL] != '0';

        return $flag;
    }

    /**
     * SAVES THE RECORD TO THE DB!!!
     * Build an array of all values which should be saved. Values must exist as a 'form value' as well as a regular
     * 'table column'.
     *
     * @param $recordId
     *
     * @return int    record id (in case of insert, it's different from $recordId)
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function elements($recordId) {
        $columnCreated = false;
        $columnModified = false;
        $realColumnFound = false;

        $newValues = array();

        $tableColumns = array_keys($this->store->getStore(STORE_TABLE_COLUMN_TYPES));
        $tableColumnTypes = $this->store->getStore(STORE_TABLE_COLUMN_TYPES);
        $formValues = $this->store->getStore(STORE_FORM);
        $formValues = $this->createEmptyTemplateGroupElements($formValues);

        $feColumnTypes = array();
        foreach ($this->feSpecNative as $fe) {
            $feColumnTypes[$fe[FE_NAME]] = $fe[FE_TYPE];
        }

        // Get htmlAllow parameters of all formValues and store in $feSpecsTags
        $feSpecsTags = $this->getHtmlAllowTags($this->feSpecNative, $formValues);

        // Iterate over all table.columns. Built an assoc array $newValues.
        foreach ($tableColumns as $column) {

            // Never save a predefined 'id': autoincrement values will be given by database..
            if ($column === COLUMN_ID) {
                continue;
            }

            // Skip Upload Elements: those will be processed later.
            if ($this->isColumnUpload($column)) {
                $realColumnFound = true;
                continue;
            }

            if ($column === COLUMN_CREATED) {
                $columnCreated = true;
            }

            if ($column === COLUMN_MODIFIED) {
                $columnModified = true;
            }

            // Is there a value? Do not forget SIP values. Those do not have necessarily a FormElement.
            if (!isset($formValues[$column])) {
                continue;
            }

            $this->store->setVar(SYSTEM_FORM_ELEMENT, "Column: $column", STORE_SYSTEM);
            if (!isset($feColumnTypes[$column])) {
                $feColumnTypes[$column] = '';
            }

            // Convert time to datetime if mysql column is datetime, keep date if given
            if ($tableColumnTypes[$column] === DB_COLUMN_TYPE_DATETIME && $feColumnTypes[$column] === FE_TYPE_TIME) {
                // Keep old date value if exists
                $actualDate = explode(' ', $this->store->getVar($column, STORE_RECORD), 2)[0];
                if (!isset($actualDate) || $actualDate === '0000-00-00' || $actualDate === '') {
                    $actualDate = '0000-00-00';
                }

                // Check if date already is given to prevent double date issues.
                $actualDateFragments = explode(' ', $formValues[$column], 2);
                if (count($actualDateFragments) > 1) {
                    $formValues[$column] = $actualDate . ' ' . $actualDateFragments[1];
                } else {
                    $formValues[$column] = $actualDate . ' ' . $formValues[$column];
                }
            }

            // Convert date to datetime if mysql column is datetime, keep time if given
            if ($tableColumnTypes[$column] === DB_COLUMN_TYPE_DATETIME && $feColumnTypes[$column] === FE_TYPE_DATE) {
                $actualTime = '00:00:00';
                // Keep old time value if exists
                $timeFragment = explode(' ', $this->store->getVar($column, STORE_RECORD), 2);
                if (isset($timeFragment[1])) {
                    $actualTime = $timeFragment[1];
                }

                // Check if time from datetime already is given and prevent issues with double time string
                $actualTimeFragments = explode(' ', $formValues[$column], 2);
                if (!isset($actualTimeFragments[1])) {
                    $formValues[$column] = $formValues[$column] . ' ' . $actualTime;
                }
            }

            // Check if an empty string has to be converted to null.
            if (isset($formValues[$column]) && $formValues[$column] == '' && $this->isSetEmptyMeansNull($column)) {
                $formValues[$column] = null;
            } else {
                Support::setIfNotSet($formValues, $column);
            }

            // Check for existing htmlAllow and strip tags, purify html result to prevent XSS
            if (isset($feSpecsTags[$column]) && $feSpecsTags[$column] !== '') {
                $formValues[$column] = $this->custom_strip_tags($formValues[$column], $feSpecsTags[$column]);
                $formValues[$column] = $this->purifierHtml($formValues[$column]);
            }

            if ($feColumnTypes[$column] === FE_TYPE_SELECT) {
                // Typecast: some maria-db have a problem if an integer is assigned to an enum string.
                $formValues[$column] = (string)$formValues[$column];
            }

            $newValues[$column] = $formValues[$column];
            $realColumnFound = true;
        }

        // Only save record if real columns exist.
        if ($realColumnFound) {

            if ($columnModified && !isset($newValues[COLUMN_MODIFIED])) {
                $newValues[COLUMN_MODIFIED] = date('YmdHis');
            }

            if ($recordId == 0) {
                if ($columnCreated && !isset($newValues[COLUMN_CREATED])) {
                    $newValues[COLUMN_CREATED] = date('YmdHis');
                }
                $recordId = $this->insertRecord($this->formSpec[F_TABLE_NAME], $newValues);

            } else {
                $this->updateRecord($this->formSpec[F_TABLE_NAME], $newValues, $recordId, $this->formSpec[F_PRIMARY_KEY]);
            }
        }

        // Reload fresh saved record and fill STORE_RECORD with it. Do this before nativeDoSlave().
        $this->store->fillStoreWithRecord($this->formSpec[F_TABLE_NAME], $recordId, $this->dbArray[$this->dbIndexData],
            $this->formSpec[F_PRIMARY_KEY]);

        $this->nativeDoSlave($recordId);

        return $recordId;
    }

    /**
     * Get for every formElement htmlAllow tags from parameter
     *
     * @param $feSpecNative
     * @param $formValues
     * @return array
     */
    private function getHtmlAllowTags($feSpecNative, $formValues): array {

        $feSpecsTags = array();

        foreach ($feSpecNative as $formElement) {
            foreach ($formValues as $keyName => $keyValue) {
                if ($formElement[FE_NAME] === $keyName) {
                    if (isset($formElement[FE_HTML_ALLOW]) && $formElement[FE_HTML_ALLOW] !== '') {
                        $feSpecsTags[$keyName] = $formElement[FE_HTML_ALLOW];
                    }
                }
            }
        }

        return $this->setTinyMceSpecificTags($feSpecsTags);
    }

    /**
     * For TinyMCE there are specific tags needed for lists and text decoration (underline, strikethrough).
     * These tags should be added here.
     *
     * @param $feSpecsTags
     * @return array
     */
    private function setTinyMceSpecificTags($feSpecsTags): array {
        $listFlag = false;
        $decorationFlag = false;
        $tableFlag = false;
        $strongFlag = false;
        foreach ($feSpecsTags as $key => $value) {
            $feSpecsTagArray[$key] = explode(',', $value);
            foreach ($feSpecsTagArray[$key] as $key2 => $tag) {
                switch ($tag) {
                    case 'ul':
                    case 'ol':
                        $listFlag = true;
                        break;
                    case 'textDecoration':
                    case 'u':
                    case 'ins':
                    case 'del':
                    case 's':
                        $decorationFlag = true;
                        break;
                    case 'table':
                        $tableFlag = true;
                        break;
                    case 'b':
                        $strongFlag = true;
                        break;
                    default:
                        $feSpecsTagArray[$key][$key2] = $tag;
                        break;
                }
            }

            if ($listFlag) {
                $feSpecsTagArray[$key][] = "li";
                $listFlag = false;
            }

            // In case of TinyMCE span is automatically used for underline and strikethrough
            if ($decorationFlag) {
                $feSpecsTagArray[$key][] = "span";
                $decorationFlag = false;
            }


            if ($strongFlag) {
                $feSpecsTagArray[$key][] = "strong";
                $strongFlag = false;
            }

            if ($tableFlag) {
                array_push($feSpecsTagArray[$key], "th", "td", "tr", "tbody", "thead");
                $tableFlag = false;
            }

            $feSpecsTags[$key] = implode(',', $feSpecsTagArray[$key]);
        }

        return $feSpecsTags;
    }

    /**
     * Process sqlBefore, sqlInsert|.... for all native FE.
     *
     * @param $recordId
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function nativeDoSlave($recordId) {

        foreach ($this->feSpecNative as $fe) {
            // Preparation for Log, Debug
            $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($fe), STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, $fe[FE_ID], STORE_SYSTEM);


            $this->formAction->doSqlBeforeSlaveAfter($fe, $recordId, false);
            $this->typeAheadDoTagGlue($fe);
        }

        $this->store->setVar(SYSTEM_FORM_ELEMENT, '', STORE_SYSTEM);
        $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, 0, STORE_SYSTEM);

    }

    /**
     * typeAhead: if given, process Tag or Glue.
     *
     * @param array $fe
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function typeAheadDoTagGlue(array $fe) {

        // Update 'glue' records?
        if (($fe[FE_TYPEAHEAD_TAG] ?? '0') == '0' || (!isset($fe[FE_TYPEAHEAD_GLUE_INSERT]) && !isset($fe[FE_TYPEAHEAD_TAG_INSERT]))) {
            return;
        }

        if (empty($fe[FE_TYPEAHEAD_GLUE_INSERT]) || empty($fe[FE_TYPEAHEAD_GLUE_DELETE])) {
            throw new \UserFormException("Missing 'typeAheadGlueInsert' or 'typeAheadGlueDelete'", ERROR_MISSING_REQUIRED_PARAMETER);
        }

        // Extract assigned tags: last
        $tagLast = KeyValueStringParser::parse($this->evaluate->parse($fe[FE_VALUE], ROW_EXPECT_0_1));

        // Extract assigned tags: new
        $tagNew = KeyValueStringParser::parse($this->store->getVar($fe[FE_NAME], STORE_FORM,
            ($fe[FE_CHECK_TYPE] == SANITIZE_ALLOW_AUTO) ? SANITIZE_ALLOW_ALNUMX : $fe[FE_CHECK_TYPE]));

        $result = array_diff_assoc($tagNew, $tagLast);

        // Create glue records
        // Add all tags from tagNew which do not exist in tagLast. 
        foreach ($result as $id => $value) {

            $this->store->setVar(VAR_TAG_ID, $id, STORE_VAR);
            $this->store->setVar(VAR_TAG_VALUE, $value, STORE_VAR);

            if ($id == 0 || preg_match('/^0-\d+$/', $id)) {
                if (empty($fe[FE_TYPEAHEAD_TAG_INSERT])) {
                    throw new \UserFormException("Missing 'typeAheadTagInsert'", ERROR_MISSING_REQUIRED_PARAMETER);
                }
                // Create tag
                $id = $this->evaluate->parse($fe[FE_TYPEAHEAD_TAG_INSERT]);
                $this->store->setVar(VAR_TAG_ID, $id, STORE_VAR);
            }

            // Create glue
            $this->evaluate->parse($fe[FE_TYPEAHEAD_GLUE_INSERT]);
        }

        // Get all tags who has been removed
        $result = array_diff_assoc($tagLast, $tagNew);

        // Delete Glue records
        foreach ($result as $id => $value) {
            $this->store->setVar(VAR_TAG_ID, $id, STORE_VAR);
            $this->store->setVar(VAR_TAG_VALUE, $value, STORE_VAR);
            // Delete glue
            $this->evaluate->parse($fe[FE_TYPEAHEAD_GLUE_DELETE]);
        }
    }

    /**
     * Checks if there is a formElement with name '$feName' of type 'upload'
     *
     * @param $feName
     * @return bool
     */
    private function isColumnUpload($feName) {

        foreach ($this->feSpecNative as $formElement) {
            if ($formElement[FE_NAME] === $feName && $formElement[FE_TYPE] == FE_TYPE_UPLOAD)
                return true;
        }

        return false;
    }

    /**
     * Insert new record in table $this->formSpec['tableName'].
     *
     * @param $tableName
     * @param array $values
     *
     * @return int  last insert id
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function insertRecord($tableName, array $values) {

        if (count($values) === 0)
            return 0; // nothing to write, last insert id=0

        list($sql, $parameterArray) = SqlQuery::insertRecord($tableName, $values);

        $rc = $this->dbArray[$this->dbIndexData]->sql($sql, ROW_REGULAR, $parameterArray);

        return $rc;
    }

    /**
     * @param string $tableName
     * @param array $values
     * @param int $recordId
     * @param string $primaryKey
     *
     * @return bool|int     false if $values is empty, else affected rows
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     */
    public function updateRecord($tableName, array $values, $recordId, $primaryKey = F_PRIMARY_KEY_DEFAULT) {

        if (count($values) === 0)
            return 0; // nothing to write, 0 rows affected

        list($sql, $parameterArray) = SqlQuery::updateRecord($tableName, $values, $recordId, $primaryKey);

        $rc = $this->dbArray[$this->dbIndexData]->sql($sql, ROW_REGULAR, $parameterArray);

        return $rc;
    }

    /**
     * Process all Upload Formelements for the given $recordId. After processing &$formValues will be updated with the
     * final filenames.
     *
     * Constellation: # FILE OLD   FILE NEW     FILESIZE
     *                1 none       none
     *                2 none       new
     *                3 exist      no change
     *                4 delete     none
     *                5 delete     new
     *
     * @param $recordId
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function processUploads($recordId) {

        $sip = new Sip(false);
        $newValues = array();

        $flagDoUnzip = false;

        $formValues = $this->store->getStore(STORE_FORM);
        $primaryRecord = $this->store->getStore(STORE_RECORD); // necessary to check if the current formElement exist as a column of the primary table.

        // Upload - Take care the necessary target directories exist.
        $cwd = getcwd();
        $absoluteApp = Path::absoluteApp();
        if ($cwd === false || $absoluteApp === false || !HelperFile::chdir($absoluteApp)) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'getcwd() failed or SITE_PATH undefined or chdir() failed', ERROR_MESSAGE_TO_DEVELOPER => "getcwd() failed or SITE_PATH undefined or chdir('$absoluteApp') failed."]),
                ERROR_IO_CHDIR);
        }

        foreach ($this->feSpecNative as $formElement) {
            // skip non upload formElements
            if ($formElement[FE_TYPE] != FE_TYPE_UPLOAD) {
                continue;
            }

            // Preparation for Log, Debug
            $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($formElement), STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, $formElement[FE_ID], STORE_SYSTEM);

            $formElement = HelperFormElement::initUploadFormElement($formElement);
            if (isset($formElement[FE_FILL_STORE_VAR])) {
                $formElement[FE_FILL_STORE_VAR] = $this->evaluate->parse($formElement[FE_FILL_STORE_VAR], ROW_EXPECT_0_1);
                $this->store->appendToStore($formElement[FE_FILL_STORE_VAR], STORE_VAR);
            }

            $column = $formElement[FE_NAME];

            $statusUpload = $this->store->getVar($formValues[$column] ?? '', STORE_EXTRA);
            // Get file stats
            $vars = array();
            $vars[VAR_FILE_SIZE] = $statusUpload[FILES_SIZE] ?? 0;
            $vars[VAR_FILE_MIME_TYPE] = $statusUpload[FILES_TYPE] ?? '';

            // Check for 'unzip'.
            if (isset($formElement[FE_FILE_UNZIP])
                && $formElement[FE_FILE_UNZIP] != '0'
                && $vars[VAR_FILE_MIME_TYPE] == 'application/zip') {
                $flagDoUnzip = true;
            }

            // Do upload
            $pathFileName = $this->doUpload($formElement, ($formValues[$column] ?? ''), $sip, $modeUpload);
            if ($flagDoUnzip && $pathFileName != '') {
                if ($formElement[FE_FILE_UNZIP] == '' || $formElement[FE_FILE_UNZIP] == '1') {
                    // Set default dir.
                    $formElement[FE_FILE_UNZIP] = HelperFile::joinPathFilename(dirname($pathFileName), FE_FILE_UNPACK_DIR);
                }

                // Backup STORE_VAR - will be changed in doUnzip()
                $tmpStoreVar = $this->store->getStore(STORE_VAR);
                $this->doUnzip($formElement, $pathFileName);
                // Restore STORE_VAR
                $this->store->setStore($tmpStoreVar, STORE_VAR, true);
            }

            if ($modeUpload == UPLOAD_MODE_DELETEOLD && $pathFileName == '') {
                $pathFileNameTmp = '';  // see '4'
            } else {
                if (empty($pathFileName)) {
                    $pathFileNameTmp = $primaryRecord[$column] ?? ''; // see '3'. Attention: in case of Advanced Upload, $primaryRecord[$column] does not exist.
                } else {
                    $pathFileNameTmp = $pathFileName; // see '1,2,5'
                }
            }

            // Get latest file information
            if ($pathFileNameTmp == '') {
                // No new upload and no existing: take care to remove previous upload file statistics.
                $this->store->unsetVar(VAR_FILE_MIME_TYPE, STORE_VAR);
                $this->store->unsetVar(VAR_FILE_SIZE, STORE_VAR);
            } else {
                $this->store->appendToStore($vars, STORE_VAR);
            }

            // If given: fire a sqlBefore query
            if (!$flagDoUnzip) {
                $this->evaluate->parse($formElement[FE_SQL_BEFORE]);
            }

            // Upload Type: Simple or Advanced
            // If (isset($primaryRecord[$column])) { - see #5048 - isset does not deal correctly with NULL!
            if (array_key_exists($column, $primaryRecord)) {
                // 'Simple Upload': no special action needed, just process the current (maybe modified) value.
                if ($pathFileName !== false) {
                    $newValues[$column] = $pathFileName;

                    if (isset($primaryRecord[COLUMN_FILE_SIZE])) {
                        $newValues[COLUMN_FILE_SIZE] = $vars[VAR_FILE_SIZE];
                    }

                    if (isset($primaryRecord[COLUMN_MIME_TYPE])) {
                        $newValues[COLUMN_MIME_TYPE] = $vars[VAR_FILE_MIME_TYPE];
                    }
                }
            } elseif (isset($formElement[FE_IMPORT_TO_TABLE]) && !isset($formElement[FE_SLAVE_ID])) {
                // Excel import on nonexisting column -> no upload
            } elseif ($flagDoUnzip) {
                // If ZIP and advanced upload: process it not here but via doUnzip.
            } else {
                // 'Advanced Upload'
                $this->doUploadSlave($formElement, $modeUpload);
            }

            // If given: fire a sqlAfter query
            if (!$flagDoUnzip) {
                $this->evaluate->parse($formElement[FE_SQL_AFTER]);
            }
        }

        $this->store->setVar(SYSTEM_FORM_ELEMENT, '', STORE_SYSTEM);
        $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, 0, STORE_SYSTEM);

        // Clean up
        HelperFile::chdir($cwd);

        // Only used in 'Simple Upload'
        if (count($newValues) > 0) {
            $this->updateRecord($this->formSpec[F_TABLE_NAME], $newValues, $recordId, $this->formSpec[F_PRIMARY_KEY]);
            // In case there is an update: Reload STORE_RECORD
            $this->store->fillStoreWithRecord($this->formSpec[F_TABLE_NAME], $recordId, $this->dbArray[$this->dbIndexData], $this->formSpec[F_PRIMARY_KEY]);
        }

    }

    /**
     * Unzip $pathFileName to $formElement[FE_FILE_UNZIP]. Before final extract, fire FE_SQL_VALIDATE.
     * For each file in ZIP:
     * - Fill STORE_VAR with VAR_FILENAME, VAR_FILENAME_ONLY, VAR_FILENAME_BASE, VAR_FILENAME_EXT, VAR_FILE_MIME_TYPE, VAR_FILE_SIZE.
     * - Fire $formElement[FE_SQL_VALIDATE]
     * - Fire FE_SLAVE_ID, FE_SQL_BEFORE, FE_SQL_INSERT, FE_SQL_UPDATE, FE_SQL_DELETE, FE_SQL_AFTER
     *
     * @param array $formElement
     * @param string $pathFileName
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function doUnzip(array $formElement, $pathFileName) {

        if (!is_readable($pathFileName)) {
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => "Open ZIP file failed",
                ERROR_MESSAGE_TO_DEVELOPER => "File: " . $pathFileName]),
                ERROR_IO_ZIP_OPEN);
        }

        $zip = new ZipArchive();
        $res = $zip->open($pathFileName);
        if ($res !== true) {
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => "Open ZIP file failed" . HelperFile::zipFileErrMsg($res),
                ERROR_MESSAGE_TO_DEVELOPER => "File: " . $pathFileName]), ERROR_IO_ZIP_OPEN);
        }

        // Extract
        if (false === $zip->extractTo($formElement[FE_FILE_UNZIP])) {
            throw new \UserFormException("Failed to extract ZIP.", ERROR_IO_ZIP_OPEN);
        }

        // Do sqlValidate() - to get mime type of zipped items, the archive has already been extracted.
        if (!empty($formElement[FE_SQL_VALIDATE])) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $stat = $zip->statIndex($i);

                $itemPathFileName = HelperFile::joinPathFilename($formElement[FE_FILE_UNZIP], $stat['name']);
                $this->store->appendToStore(HelperFile::getFileStat($itemPathFileName), STORE_VAR);
                $this->store->appendToStore(HelperFile::pathinfo($itemPathFileName), STORE_VAR);

                HelperFormElement::sqlValidate($this->evaluate, $formElement);
            }
        }

        // Process
        if (!isset($formElement[FE_SLAVE_ID])) {
            $formElement[FE_SLAVE_ID] = '';
        }

        if (!empty($formElement[FE_SLAVE_ID] . $formElement[FE_SQL_BEFORE] . $formElement[FE_SQL_INSERT] .
            $formElement[FE_SQL_UPDATE] . $formElement[FE_SQL_DELETE] . $formElement[FE_SQL_AFTER])) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $stat = $zip->statIndex($i);

                $itemPathFileName = HelperFile::joinPathFilename($formElement[FE_FILE_UNZIP], $stat['name']);
                $this->store->appendToStore(HelperFile::getFileStat($itemPathFileName), STORE_VAR);
                $this->store->appendToStore(HelperFile::pathinfo($itemPathFileName), STORE_VAR);

                $this->evaluate->parse($formElement[FE_SQL_BEFORE]);
                $this->doUploadSlave($formElement, UPLOAD_MODE_NEW);
                $this->evaluate->parse($formElement[FE_SQL_AFTER]);
            }
        }

        // Close Zip
        if (false === $zip->close()) {
            throw new \UserFormException("Failed to close ZIP.", ERROR_IO_ZIP_OPEN);
        }
    }

    /**
     * Process all imageCut FormElements.
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function processAllImageCutFE($recordId): void {

        foreach ($this->feSpecNative as $formElement) {
            // skip non upload formElements
            if ($formElement[FE_TYPE] == FE_TYPE_IMAGE_CUT) {

                // Typically: $htmlElementNameIdZero = true
                // After Saving a record, staying on the form, the FormElements on the Client are still known as '<feName>:0'.
                $htmlFormElementName = HelperFormElement::buildFormElementName($formElement, $recordId);

                $this->extractImageDataReplaceFile($formElement, $htmlFormElementName, $recordId);
            }
        }
    }


    /**
     * Iterates over all FE and checks all 'required' (mode & modeSql) FE.
     * If a required FE is empty, throw an exception.
     * Take care to remove all FE with modeSql='hidden'.
     *
     * Typically, the browser does not allow a submit if a required field is empty.
     *
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function checkRequiredHidden() {

        $formModeGlobal = Support::getFormModeGlobal($this->formSpec[F_MODE_GLOBAL] ?? '');
        $reportRequiredFailed = true;

        switch ($formModeGlobal) {
            case F_MODE_REQUIRED_OFF:
            case F_MODE_REQUIRED_OFF_BUT_MARK:
                $reportRequiredFailed = false;
                break;
        }

        $clientValues = $this->store::getStore(STORE_FORM);

        $flagAllRequiredGiven = 1;

        foreach ($this->feSpecNative as $key => $formElement) {

            // Do not check retype slave FE.
            if (isset($formElement[FE_RETYPE_SOURCE_NAME])) {
                continue;
            }
            // Do not check FE.note
            if ($formElement[FE_TYPE] == FE_TYPE_NOTE){
                continue;
            }

            $this->store->setVar(SYSTEM_FORM_ELEMENT, "Column: " . $formElement[FE_NAME], STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, $formElement[FE_ID], STORE_SYSTEM);

            // Normalize FE_MODE
            $mode = Support::handleEscapeSpaceComment($formElement[FE_MODE_SQL] ?? '');
            $mode = empty($mode) ? ($formElement[FE_MODE] ?? '') : $this->evaluate->parse($mode);
            $this->feSpecNative[$key][FE_MODE] = $formElement[FE_MODE] = $mode;
            $this->feSpecNative[$key][FE_MODE_SQL] = $formElement[FE_MODE_SQL] = '';

            if (isset($formElement[FE_ACCEPT_ZERO_AS_REQUIRED]) && $formElement[FE_ACCEPT_ZERO_AS_REQUIRED] != '0' &&
                isset($clientValues[$formElement[FE_NAME]]) && $clientValues[$formElement[FE_NAME]] == '0') {
                continue;
            }

            // Upload needs special action to check for empty.
            if (($mode == FE_MODE_REQUIRED || $mode == FE_MODE_HIDDEN) && $formElement[FE_TYPE] == FE_TYPE_UPLOAD) {
                // If there is a new upload, returns array '[FILES_NAME, FILES_TMP_NAME, FILES_TYPE, FILES_ERROR, FILES_SIZE, FILES_FLAG_DELETE]'
                $statusUpload = $this->store->getVar($clientValues[$formElement[FE_NAME]] ?? '', STORE_EXTRA);

                // Check if there is a new upload
                if (!empty($statusUpload[FILES_TMP_NAME])) {
                    continue;// Upload given: continue with next FE
                }

                // No new upload: check for existing. To check for a value if it is given in STORE_RECORD, is ok for simple upload, but
                // breaks for advanced upload (non primary column). Therefore we need access to the orignal via UPLOAD SIP.
                $str = $this->store::$sip->getQueryStringFromSip($clientValues[$formElement[FE_NAME]]);
                $arr = KeyValueStringParser::parse($str, '=', '&');
                if (($arr[EXISTING_PATH_FILE_NAME] ?? '') == '' || ($statusUpload[FILES_FLAG_DELETE] ?? 0) == '1') {
                    // Fake to trigger the next if( .. empty...)
                    $clientValues[$formElement[FE_NAME]] = '';
                }
            }

            // Required fieldset is skipped (only child elements are checked)
            if ($mode == FE_MODE_REQUIRED && empty($clientValues[$formElement[FE_NAME]]) && $formElement[FE_TYPE] != FE_TYPE_FIELDSET) {
                $flagAllRequiredGiven = 0;
                if ($reportRequiredFailed) {
                    $name = ($formElement[FE_LABEL] == '') ? $formElement[FE_NAME] : $formElement[FE_LABEL];

                    throw new \UserFormException("Missing required value: $name", ERROR_REQUIRED_VALUE_EMPTY);
                }

                // Check if mode = required was inherited
            } else if (empty($clientValues[$formElement[FE_NAME]]) && $formElement[FE_TYPE] != FE_TYPE_FIELDSET) {
                $name = ($formElement[FE_LABEL] == '') ? $formElement[FE_NAME] : $formElement[FE_LABEL];

                // Check if FE is nested
                $feParent = OnArray::filter($this->feSpecNativeRaw, FE_ID, $formElement[FE_ID_CONTAINER]);

                // Check if parent FE is required fieldset
                // Only reached if JS-required-check is bypassed/skipped
                if (!empty($feParent) && $feParent[0][FE_TYPE] == FE_TYPE_FIELDSET && $feParent[0][FE_MODE] == FE_MODE_REQUIRED) {
                    $parentName = $feParent[0][FE_NAME];

                    throw new \UserFormException("Missing required value: $name (mode inherited from $parentName)", ERROR_REQUIRED_VALUE_EMPTY);
                }
            }

            if ($mode == FE_MODE_HIDDEN) {
                if (!($formElement[FE_TYPE] == FE_TYPE_UPLOAD && isset($statusUpload[FILES_FLAG_DELETE]) && $statusUpload[FILES_FLAG_DELETE] === '1')) {
                    // Removing the value from the store, forces that the value won't be stored.
                    $this->store::unsetVar($formElement[FE_NAME], STORE_FORM);
                }
            }
        }

        $this->store->setVar(SYSTEM_FORM_ELEMENT, '', STORE_SYSTEM);
        $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, 0, STORE_SYSTEM);

        // Save 'allRequiredGiven in STORE_VAR
        $this->store::setVar(VAR_ALL_REQUIRED_GIVEN, $flagAllRequiredGiven, STORE_VAR, true);
    }

    /**
     * imageCut:
     *
     * @param array $formElement
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function extractImageDataReplaceFile(array $formElement, $htmlFormElementName, $recordId) {

        // Get imageSource
        if ('' == ($pathFileName = $this->evaluate->parse($formElement[FE_IMAGE_SOURCE]))) {
            return; // Nothing to do.
        }

        $cwd = getcwd();
        $sitePath = Path::absoluteApp();
        if ($cwd === false || $sitePath === false || !HelperFile::chdir($sitePath)) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'getcwd() failed or SITE_PATH undefined or chdir() failed', ERROR_MESSAGE_TO_DEVELOPER => "getcwd() failed or SITE_PATH undefined or chdir('$sitePath') failed."]),
                ERROR_IO_CHDIR);
        }

        // 'data:image/png;base64,AAAFBfj42Pj4...';
        $data = $this->store->getVar(HTML_NAME_TARGET_IMAGE . $formElement[FE_NAME], STORE_FORM, SANITIZE_ALLOW_ALLBUT);
        if ($data == '' || $data === false) {
            return; // Nothing to do
        }

        if (($formElement[FE_IMAGE_KEEP_ORIGINAL] ?? 1) == 1) {
            HelperFile::createOrigFileIfNotExist($pathFileName);
        } else {
            // Original File will be replaced: therefore remove Fabric JSON string from record, cause the file is already updated.
            $sql = "UPDATE " . $this->formSpec[F_TABLE_NAME] . ' SET ' . $formElement[FE_NAME] . '= "" WHERE id=' . $recordId;
            $this->dbArray[$this->dbIndexData]->sql($sql);
        }

        // Split base64 encoded image: 'data:image/png;base64,AAAFBfj42Pj4...'
        list($type, $imageData) = explode(';', $data, 2); // $type= 'data:image/png;', $imageData='base64,AAAFBfj42Pj4...'
        list(, $extension) = explode('/', $type); // $type='png'
        list(, $imageData) = explode(',', $imageData); // $imageData='AAAFBfj42Pj4...'

        if (false === file_put_contents($pathFileName, base64_decode($imageData))) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Write new image failed', ERROR_MESSAGE_TO_DEVELOPER => "Write new image failed: $pathFileName"]),
                ERROR_IO_WRITE);
        }

        HelperFile::chdir($cwd);
    }

    /**
     * Process upload for the given Formelement. If necessary, delete a previous uploaded file.
     * Calculate the final path/filename and move the file to the new location.
     *
     * Check also: Documentation-develop/CODING.md
     *
     * @param array $formElement FormElement 'upload'
     * @param string $sipUpload SIP
     * @param Sip $sip
     * @param string $modeUpload UPLOAD_MODE_UNCHANGED | UPLOAD_MODE_NEW | UPLOAD_MODE_DELETEOLD |
     *                            UPLOAD_MODE_DELETEOLD_NEW
     * @return false|string New pathFilename or false on error
     * @throws \CodeException
     * @throws \DbException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \UserFormException
     * @throws \UserReportException
     * @internal param $recordId
     */
    private function doUpload($formElement, $sipUpload, Sip $sip, &$modeUpload) {
        $flagDelete = false;
        $modeUpload = UPLOAD_MODE_UNCHANGED;
        $pathFileName = '';

        // Status information about upload file
        $statusUpload = $this->store->getVar($sipUpload, STORE_EXTRA);
        if ($statusUpload === false) {
            return false;
        }

        if (isset($formElement[FE_IMPORT_TO_TABLE]) && isset($statusUpload[FILES_TMP_NAME])) {
            // Import
            $tmpFile = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);
            $this->doImport($formElement, $tmpFile);
        }

        // Delete existing old file.
        if (isset($statusUpload[FILES_FLAG_DELETE]) && $statusUpload[FILES_FLAG_DELETE] == '1') {
            $arr = $sip->getVarsFromSip($sipUpload);
            $oldFile = $arr[EXISTING_PATH_FILE_NAME];
            if (file_exists($oldFile)) {
                //TODO: it might be possible to delete a file, which is referenced by another record - a check would be nice.
                HelperFile::unlink($oldFile, $this->qfqLogFilename);
            }
            $flagDelete = ($oldFile != '');
        }

        // Set $modeUpload
        if (isset($statusUpload[FILES_TMP_NAME]) && $statusUpload[FILES_TMP_NAME] != '') {
            $modeUpload = $flagDelete ? UPLOAD_MODE_DELETEOLD_NEW : UPLOAD_MODE_NEW;
        } else {
            $modeUpload = $flagDelete ? UPLOAD_MODE_DELETEOLD : UPLOAD_MODE_UNCHANGED;
        }

        Logger::logMessageWithPrefix(UPLOAD_LOG_PREFIX . ': modeUpload= ' . $modeUpload, $this->qfqLogFilename);

        // skip uploading the file, if this is an import without a specified file destination
        if (!isset($formElement[FE_IMPORT_TO_TABLE]) || isset($formElement[FE_FILE_DESTINATION])) {
            $pathFileName = $this->copyUploadFile($formElement, $statusUpload);

            // Save final pathFileNames after form has been saved. Makes uploaded files downloadable without page reload.
            $statusUpload[UPLOAD_SIP_DOWNLOAD_KEY] = $statusUpload[UPLOAD_SIP_DOWNLOAD_KEY] ?? '';
            $sipDownloadParams = $this->store::getVar($statusUpload[UPLOAD_SIP_DOWNLOAD_KEY], STORE_EXTRA);
            $sipDownloadParams[FE_FILE_DESTINATION] = $pathFileName;
            $this->store::setVar($statusUpload[UPLOAD_SIP_DOWNLOAD_KEY], $sipDownloadParams, STORE_EXTRA);

            $msg = UPLOAD_LOG_PREFIX . ': ';
            $msg .= ($pathFileName == '') ? 'Remove old upload / no new upload' : 'File "' . $statusUpload[FILES_TMP_NAME] . '" >> "' . $pathFileName . '"';
            Logger::logMessageWithPrefix($msg, $this->qfqLogFilename);
        }

        // Delete current used uniq SIP
        $this->store->setVar($sipUpload, array(), STORE_EXTRA);

        return $pathFileName;
    }

    /**
     * Excel Import
     *
     * @param $formElement
     * @param $fileName
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function doImport($formElement, $fileName) {
        $importNamedSheetsOnly = array();

        Support::setIfNotSet($formElement, FE_IMPORT_TYPE, FE_IMPORT_TYPE_AUTO);

        if (!empty($formElement[FE_IMPORT_NAMED_SHEETS_ONLY])) {
            $importNamedSheetsOnly = explode(',', $formElement[FE_IMPORT_NAMED_SHEETS_ONLY]);
        }

        // Check for keywords which needs an explicit given document type.
        if ($formElement[FE_IMPORT_TYPE] === FE_IMPORT_TYPE_AUTO) {

            $list = [FE_IMPORT_LIST_SHEET_NAMES, FE_IMPORT_READ_DATA_ONLY, FE_IMPORT_LIST_SHEET_NAMES];
            foreach ($list as $token) {
                if (isset($formElement[$token])) {
                    throw new \UserFormException('If ' . $token .
                        ' is given, an explicit document type (like ' . FE_IMPORT_TYPE . '=xlsx) should be set.', ERROR_IMPORT_MISSING_EXPLICIT_TYPE);
                }
            }
        }

        switch ($formElement[FE_IMPORT_TYPE]) {
            case FE_IMPORT_TYPE_AUTO:
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
                break;

            case FE_IMPORT_TYPE_XLS:
            case FE_IMPORT_TYPE_XLSX:
            case FE_IMPORT_TYPE_CSV:
            case FE_IMPORT_TYPE_ODS:
                $inputFileType = ucfirst($formElement[FE_IMPORT_TYPE]);
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

                // setReadDataOnly
                if (($formElement[FE_IMPORT_READ_DATA_ONLY] ?? '0') != '0') {
                    $reader->setReadDataOnly(true);
                }

                // setLoadSheetsOnly
                if (!empty ($importNamedSheetsOnly)) {
                    $reader->setLoadSheetsOnly($importNamedSheetsOnly);
                }

                // Debug option importListSheetNames=1 will list all recognized sheetnames and stops import.
                if (($formElement[FE_IMPORT_LIST_SHEET_NAMES] ?? '0') != '0') {
                    $sheetNames = $reader->listWorksheetNames($fileName);
                    throw new \UserFormException("Worksheets: " . implode(', ', $sheetNames),
                        ERROR_IMPORT_LIST_SHEET_NAMES);
                }

                $spreadsheet = $reader->load($fileName);
                break;

            default:
                throw new \UserFormException("Unknown Excel import type: '" . $formElement[FE_IMPORT_TYPE] . "'.",
                    ERROR_UNKNOWN_EXCEL_IMPORT_TYPE);
        }

        $tableName = $formElement[FE_IMPORT_TO_TABLE];
        $regions = OnArray::trimArray(explode('|', $formElement[FE_IMPORT_REGION] ?? ''));
        $columnNames = OnArray::trimArray(explode(',', $formElement[FE_IMPORT_TO_COLUMNS] ?? ''));
        $importMode = $formElement[FE_IMPORT_MODE] ?? FE_IMPORT_MODE_APPEND;

        $tableDefinition = $this->dbArray[$this->dbIndexData]->getTableDefinition($tableName);
        // Get existing primary key id for later update statement
        $existingId = false;
        foreach ($tableDefinition as $column) {
            if ($column[COLUMN_FIELD] === COLUMN_ID) {
                $existingId = true;
            }
        }
        // Check for exceeding columns
        if ($existingId) {
            $availableColumns = count($tableDefinition) - 1;
        } else {
            $availableColumns = count($tableDefinition);
        }

        // Needed stats to adjust right column and sql query
        $excelStats = $this->setExcelStats();
        foreach ($regions as $region) {
            // region: tab, startColumn, startRow, endColumn, endRow
            $region = OnArray::trimArray(explode(',', $region));
            $tab = 1;
            if (!empty($region[0])) {
                $tab = $region[0];
            }

            try {
                if (is_numeric($tab)) {
                    $worksheet = $spreadsheet->getSheet($tab - 1); // 0-based
                } else {
                    $worksheet = $spreadsheet->getSheetByName($tab);
                    if ($worksheet === null) {
                        throw new \PhpOffice\PhpSpreadsheet\Exception(
                            "No sheet with the name '$tab' could be found."
                        );
                    }
                }
            } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
                throw new \UserFormException($e->getMessage());
            }

            // Set up requested region
            $columnStart = '1';
            $columnEnd = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($worksheet->getHighestColumn());
            $rowStart = 1;
            $rowEnd = $worksheet->getHighestRow();
            if (!empty($region[1])) { // startColumn
                if (!is_numeric($region[1])) $region[1] = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($region[1]);
                if ($region[1] >= $columnStart && $region[1] <= $columnEnd) {
                    $columnStart = $region[1];
                }
            }
            if (!empty($region[3])) { // endColumn
                if (!is_numeric($region[3])) $region[3] = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($region[3]);
                if ($region[3] >= $columnStart && $region[3] <= $columnEnd) {
                    $columnEnd = $region[3];
                }
            }
            if (!empty($region[2]) && $region[2] >= $rowStart && $region[2] <= $rowEnd) {
                $rowStart = $region[2];
            }
            if (!empty($region[4]) && $region[4] >= $rowStart && $region[4] <= $rowEnd) {
                $rowEnd = $region[4];
            }
            if (!empty($region[5]) && $excelStats[FE_IMPORT_FLAG_INSERTED]) {
                if ($region[5] === FE_IMPORT_REGION_APPEND) {
                    $excelStats = $this->setExcelStats($excelStats);
                }
            }

            // Read the specified region
            $rangeStr = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($columnStart) . $rowStart . ':' .
                \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($columnEnd) . $rowEnd;
            $worksheetData = $worksheet->rangeToArray($rangeStr, '', true, false);

            $columnDefinitionArr = [];
            $columnListArr = [];
            // get right table columns
            for ($column = $columnStart; $column <= $columnEnd; ++$column) {
                if (!empty($columnNames[$column - $columnStart]) && !$excelStats[FE_IMPORT_REGION_JOIN]) {
                    $columnName = $columnNames[$column - $columnStart];
                } else if (!empty($columnNames[$excelStats[FE_IMPORT_COLUMN_COUNT]])) {
                    $columnName = $columnNames[$excelStats[FE_IMPORT_COLUMN_COUNT]];
                } else {
                    $columnName = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($column);
                }

                // Throw exception if actual column count exceeds maximal available column names
                if ($excelStats[FE_IMPORT_COLUMN_COUNT] + 1 > $availableColumns) {
                    Thrower::userFormException('Excel columns exceeds amount from table.', 'More columns from excel given than available in table.', ERROR_EXCEL_JOIN_EXCEEDS_TABLE);
                }
                $excelStats[FE_IMPORT_COLUMN_COUNT]++;
                $columnDefinitionArr[] = "`$columnName`   TEXT       NOT NULL  DEFAULT ''";
                $columnListArr[] = "$columnName";
            }

            // SQL time!
            $createTableSql = "CREATE TABLE IF NOT EXISTS `$tableName` (" .
                "`id`        INT(11)    NOT NULL  AUTO_INCREMENT," .
                implode(', ', $columnDefinitionArr) . ',' .
                "`modified`  TIMESTAMP  NOT NULL  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," .
                "`created`   DATETIME   NOT NULL  DEFAULT CURRENT_TIMESTAMP," .
                "PRIMARY KEY (`id`) )" .
                "ENGINE = InnoDB  DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;";
            $this->dbArray[$this->dbIndexData]->sql($createTableSql);

            if ($importMode === FE_IMPORT_MODE_REPLACE) {
                $this->dbArray[$this->dbIndexData]->sql("TRUNCATE $tableName");
                $importMode = FE_IMPORT_MODE_APPEND;
            }

            // Import the data
            if (!$excelStats[FE_IMPORT_FLAG_INSERTED] || !$excelStats[FE_IMPORT_REGION_JOIN]) {
                foreach ($worksheetData as $rowIndex => $row) {
                    $columnList = '`' . implode('`,`', $columnListArr) . '`';
                    $paramPlaceholders = str_repeat('?,', count($worksheetData[0]) - 1) . '?';
                    $insertSql = "INSERT INTO `$tableName` ($columnList) VALUES ($paramPlaceholders)";
                    $this->dbArray[$this->dbIndexData]->sql($insertSql, ROW_REGULAR, $row);
                    $excelStats[FE_IMPORT_INSERTED_IDS][] = $this->dbArray[$this->dbIndexData]->getLastInsertId();
                    if ($excelStats[FE_IMPORT_FIRST_INSERTED_ID] === null) {
                        $excelStats[FE_IMPORT_FIRST_INSERTED_ID] = $this->dbArray[$this->dbIndexData]->getLastInsertId();
                        $excelStats[FE_IMPORT_START_ID_COUNT] = array_search($excelStats[FE_IMPORT_FIRST_INSERTED_ID], $excelStats[FE_IMPORT_INSERTED_IDS]);
                    }
                }
                $excelStats[FE_IMPORT_FLAG_INSERTED] = true;
            } else {
                if (!$existingId) {
                    Thrower::userFormException('No primary key "id" found in table. Is needed to join previous import region.', 'No primary key id found in table', ERROR_EXCEL_NO_PRIMARY_KEY_ID);
                }
                foreach ($worksheetData as $row) {
                    $columnParams = implode('=?,', $columnListArr) . '=?';
                    $updateSql = "UPDATE $tableName SET $columnParams WHERE  id=?";
                    $row[] = $excelStats[FE_IMPORT_INSERTED_IDS][$excelStats[FE_IMPORT_START_ID_COUNT]];
                    $this->dbArray[$this->dbIndexData]->sql($updateSql, ROW_REGULAR, $row);
                    $excelStats[FE_IMPORT_START_ID_COUNT]++;
                }
                $excelStats[FE_IMPORT_START_ID_COUNT] = 0;
            }
            $excelStats[FE_IMPORT_REGION_JOIN] = true;
        }
    }

    /**
     * Set all needed stats for Excel import. If exists, reset 'sameRow', 'columnCount' and 'firstInsertedId'.
     *
     * @param string $excelStats
     * @return array
     */
    private function setExcelStats($excelStats = array()): array {
        if (empty($excelStats)) {
            $excelStats = array(
                FE_IMPORT_COLUMN_COUNT => 0,
                FE_IMPORT_START_ID_COUNT => 0,
                FE_IMPORT_INSERTED_IDS => array(),
                FE_IMPORT_FLAG_INSERTED => false,
                FE_IMPORT_REGION_JOIN => true,
                FE_IMPORT_FIRST_INSERTED_ID => null
            );
        } else {
            $excelStats[FE_IMPORT_REGION_JOIN] = false;
            $excelStats[FE_IMPORT_COLUMN_COUNT] = 0;
            $excelStats[FE_IMPORT_FIRST_INSERTED_ID] = null;
        }

        return $excelStats;
    }

    /**
     * Copy uploaded file from temporary location to final location.
     *
     * Check also: Documentation-develop/CODING.md
     *
     * @param array $formElement
     * @param array $statusUpload
     * @return array|mixed|null|string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function copyUploadFile(array $formElement, array $statusUpload) {
        $pathFileName = '';

        if (!isset($statusUpload[FILES_TMP_NAME]) || $statusUpload[FILES_TMP_NAME] === '') {
            // nothing to upload: e.g. user has deleted a previous uploaded file.
            return '';
        }

        $srcFile = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);

        if (isset($formElement[FE_FILE_DESTINATION])) {

            // Provide variable 'filename'. Might be substituted in $formElement[FE_PATH_FILE_NAME].
            $origFilename = Sanitize::safeFilename($statusUpload[FILES_NAME]);
            $this->store->appendToStore(HelperFile::pathinfo($origFilename), STORE_VAR);

            $pathFileName = $this->evaluate->parse($formElement[FE_FILE_DESTINATION]);
            $pathFileName = Sanitize::safeFilename($pathFileName, false, true); // Dynamically calculated pathFileName might contain invalid characters.

            // Saved in store for later use during 'Advanced Upload'-post processing
            $this->store->setVar(VAR_FILE_DESTINATION, $pathFileName, STORE_VAR);
        }

        if ($pathFileName === '') {
            throw new \UserFormException("Upload failed, no target '" . FE_FILE_DESTINATION . "' specified.", ERROR_NO_TARGET_PATH_FILE_NAME);
        }

        // If given, get chmodDir. Needs to be prefixed with a 0 (=octal) - it should not be quoted! Symbolic mode is not allowed. E.g.: 0660, or 01777
        if (empty($formElement[FE_FILE_CHMOD_DIR])) {
            $chmodDir = false;
        } else {
            $chmodDir = octdec($formElement[FE_FILE_CHMOD_DIR]);
        }

        $overwrite = isset($formElement[FE_FILE_REPLACE_MODE]) && $formElement[FE_FILE_REPLACE_MODE] == FE_FILE_REPLACE_MODE_ALWAYS;
        Support::moveFile($srcFile, $pathFileName, $overwrite, $chmodDir);

        // get chmodFile
        if (empty($formElement[FE_FILE_CHMOD_FILE])) {
            $chmodFile = false;
        } else {
            $chmodFile = octdec($formElement[FE_FILE_CHMOD_FILE]);
        }

        if (1) {
            // HEIC conversion?
            if (strpos($statusUpload['type'] ?? '', 'image/heic') !== false) {
                $cmd = $this->store->getVar(SYSTEM_CMD_HEIF_CONVERT, STORE_SYSTEM);
                if (!empty($cmd)) {
                    exec("$cmd -q 100 $pathFileName $pathFileName.png", $output, $return_var);
                    if ($return_var == 0) {
                        HelperFile::unlink($pathFileName);
                        $pathFileName .= '.png';
                        $statusUpload['type'] = 'image/png';
                        $statusUpload['name'] .= '.png';
                    } else {
                        $msg = error_get_last();
                        throw new \UserFormException(
                            json_encode([ERROR_MESSAGE_TO_USER => 'Upload: HEIC conversion failed', ERROR_MESSAGE_TO_DEVELOPER => $msg . '\nFile: ' . $pathFileName]),
                            ERROR_UPLOAD_FILE_TYPE);
                    }
                }
                // Update STORE_VAR
                $this->store->appendToStore(HelperFile::pathinfo($pathFileName), STORE_VAR);
                $this->store->setVar(VAR_FILE_DESTINATION, $pathFileName, STORE_VAR);
            }
        }

        $this->autoOrient($formElement, $pathFileName);
        HelperFile::chmod($pathFileName, $chmodFile);

        $this->splitUpload($formElement, $pathFileName, $chmodFile, $statusUpload);

        return $pathFileName;
    }

    /**
     * If fe['autoOrient'] is given and the MimeType corresponds to fe['autoOrientMimeType']: the given {{pathFileName:V}} will be converted.
     * ImageMagick 'convert' seems to do a better job than GraficsMagick (Orientation is stable even if multiple times applied).
     *
     * @param array $formElement
     * @param $pathFileName
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function autoOrient(array $formElement, $pathFileName) {

        // 'autoOrient' wished?
        if (!isset($formElement[FE_FILE_AUTO_ORIENT]) || $formElement[FE_FILE_AUTO_ORIENT] == '0') {
            return; // No
        }

        // Upload has matching MimeType?
        $mimeTypeList = empty($formElement[FE_FILE_AUTO_ORIENT_MIME_TYPE]) ? 'image/jpeg,image/png,image/tiff' : $formElement[FE_FILE_AUTO_ORIENT_MIME_TYPE];
        if (!HelperFile::checkFileType($pathFileName, $pathFileName, $mimeTypeList)) {
            return;
        }

        // Get 'autoOrient' command
        $cmd = empty($formElement[FE_FILE_AUTO_ORIENT_CMD]) ? FE_FILE_AUTO_ORIENT_CMD_DEFAULT : $formElement[FE_FILE_AUTO_ORIENT_CMD];
        $cmd = $this->evaluate->parse($cmd);

        // Do 'autoOrient' command
        $output = Support::qfqExec($cmd, $rc);
        if ($rc != 0) {
            throw new \UserFormException(json_encode([ERROR_MESSAGE_TO_USER => 'copy failed', ERROR_MESSAGE_TO_DEVELOPER => "[cmd=$cmd]$output"]), ERROR_IO_COPY);
        }
    }

    /**
     * Check's if the file $pathFileName should be split'ed in one file per PDF page. If no: do nothing and return.
     * The only possible split target file format is 'svg': fileSplit=svg.
     * The split'ed files will be saved under fileDestinationSplit=some/path/to/file.%02d.svg. A printf style token,
     * like '%02d', is needed to create distinguished filename's. See 'man pdf2svg' for further details.
     * For every created file, a record in table 'Split' is created (see splitSvg() ), storing the pathFileName of the
     * current page/file.
     *
     * @param array $formElement
     * @param string $pathFileName
     * @param int $chmod
     * @param array $statusUpload
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function splitUpload(array $formElement, $pathFileName, $chmod, array $statusUpload) {


        // Should the file be split?
        if (empty($formElement[FE_FILE_SPLIT])) {
            return;
        }

        // Is it technically possible to split the file?
        // BTW: the $statusUpload['type'] delivers 'octetstream' for PDF files who do not have the extension '.pdf' - therefore get mimetype again.
        $mime = HelperFile::getMimeType($pathFileName);
        if (false === strpos($mime, MIME_TYPE_SPLIT_CAPABLE) && $statusUpload[FILES_TYPE] != MIME_TYPE_SPLIT_CAPABLE) {
            return;
        }

        $fileDestinationSplit = $this->evaluate->parse($formElement[FE_FILE_DESTINATION_SPLIT] ?? '');
        $fileSplitType = $this->evaluate->parse($formElement[FE_FILE_SPLIT] ?? '');
        $fileSplitTypeOptions = $this->evaluate->parse($formElement[FE_FILE_SPLIT_OPTIONS] ?? '');
        $fileSplitTableName = $this->evaluate->parse($formElement[FE_FILE_SPLIT_TABLE_NAME] ?? '');

        if (empty($fileSplitTableName)) {
            $fileSplitTableName = $this->formSpec[F_TABLE_NAME];
        }

        if ($fileDestinationSplit == '') {
            $ext = ($fileSplitType == FE_FILE_SPLIT_SVG) ? '.%02d.svg' : '.jpg';
            $fileDestinationSplit = $pathFileName . '.split/split' . $ext;
        }

        HelperFile::mkDirParent($fileDestinationSplit);

        // Save CWD
        $cwd = getcwd();

        // Create temporary directory
        $tempDir = HelperFile::mktempdir();
        $newSrc = $tempDir . DIRECTORY_SEPARATOR . QFQ_TEMP_SOURCE;
        HelperFile::copy($pathFileName, $newSrc);

        // Split destination.
        $pathParts = pathinfo($fileDestinationSplit);
        if (empty($pathParts['filename']) || empty($pathParts['basename'])) {
            throw new \UserFormException('Missing filename in ' . FE_FILE_DESTINATION_SPLIT, ERROR_MISSING_FILE_NAME);
        }

        // Extract filename from destination directory.
        $fileNameDest = $pathParts['basename'];

        switch ($fileSplitType) {
            case FE_FILE_SPLIT_SVG:
                $cmd = $this->buildPdf2svg($newSrc, $fileNameDest);
                break;
            case FE_FILE_SPLIT_JPEG:
                $cmd = $this->buildConvertSplit($newSrc, $fileNameDest, $fileSplitTypeOptions);
                break;
            default:
                throw new \UserFormException("Unknown 'fileSplit' type: " . $formElement[FE_FILE_SPLIT], ERROR_UNKNOWN_TOKEN);
        }

        // Split PDF
        HelperFile::chdir($tempDir);
        $cnt = 0;
        do {
            $output = Support::qfqExec($cmd, $rc);
            $cnt++;

            if ($rc != 0) {
                // Split failed
                $cmdRepair = $this->buildPdftocairo($newSrc, $newSrc . '.1');
                $output .= PHP_EOL . $cmdRepair . PHP_EOL;
                $output .= Support::qfqExec($cmdRepair, $rc1);
                if ($rc1 != 0) {
                    // Repair failed too: stop here.
                    break;
                }
                HelperFile::rename($newSrc . '.1', $newSrc);
            }
        } while ($rc != 0 && $cnt < 2);

        HelperFile::chdir($cwd);

        if ($rc != 0) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'pdf split failed', ERROR_MESSAGE_TO_DEVELOPER => "[$cwd][cmd=$cmd]$output"]),
                ERROR_PDF_SPLIT);
        }

        $files = Helperfile::getSplitFileNames($tempDir);

        $xId = $this->store->getVar(COLUMN_ID, STORE_RECORD);

        // Clean optional existing old split records and files from further uploads.
        $this->dbArray[$this->dbIndexData]->deleteSplitFileAndRecord($xId, $fileSplitTableName);

        // IM 'convert' will produce files <file>-1.jpg <file>-10.jpg ... - bring them in natural sort order
        natsort($files);

        // Create DB records according to the extracted filenames.
        $tableName = TABLE_NAME_SPLIT;
        $sql = "INSERT INTO `$tableName` (`tableName`, `xId`, `pathFilename`) VALUES (?,?,?)";

        // 1) Move split files to final location. 2) Created records to reference each split file.
        foreach ($files as $file) {

            if ($file == '.' || $file == '..' || $file == QFQ_TEMP_SOURCE) {
                continue;
            }

            if (!empty($pathParts['dirname'])) {
                $fileDestination = $pathParts['dirname'] . '/' . $file;
            } else {
                $fileDestination = $file;
            }

            Support::moveFile($tempDir . DIRECTORY_SEPARATOR . $file, Support::joinPath($cwd, $fileDestination), true);
            HelperFile::chmod($fileDestination, $chmod);

            // Insert records.
            $this->dbArray[$this->dbIndexData]->sql($sql, ROW_REGULAR, [$fileSplitTableName, $xId, $fileDestination]);
        }

        // Remove duplicated source
        HelperFile::unlink($newSrc);

        // Remove empty directory
        HelperFile::rmdir($tempDir);
    }

    /**
     * @param $src
     * @param $dest
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function buildPdf2svg($src, $dest) {

        $cmd = $this->store->getVar(SYSTEM_CMD_PDF2SVG, STORE_SYSTEM);
        return "$cmd '$src' '$dest' all";
    }

    /**
     * @param $src
     * @param $dest
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function buildPdftocairo($src, $dest) {

        $cmd = $this->store->getVar(SYSTEM_CMD_PDFTOCAIRO, STORE_SYSTEM);
        return "$cmd -pdf '$src' '$dest'";
    }

    /**
     * @param $src
     * @param $dest
     * @param $options
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function buildConvertSplit($src, $dest, $options) {
        if ($options == '') {
            $options = FE_FILE_SPLIT_OPTIONS_JPEG;
        }
        $cmd = $this->store->getVar(SYSTEM_CMD_CONVERT, STORE_SYSTEM);
        return "$cmd $options '$src' '$dest'";
    }

    /**
     * Create/update or delete the slave record.
     *
     * @param array $fe
     * @param string $modeUpload UPLOAD_MODE_NEW|UPLOAD_MODE_DELETEOLD_NEW|UPLOAD_MODE_DELETEOLD
     * @return int
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function doUploadSlave(array $fe, $modeUpload) {
        $sql = '';
        $flagUpdateSlaveId = false;
        $flagSlaveDeleted = false;

        if (!isset($fe[FE_SLAVE_ID])) {
            throw new \UserFormException("Missing 'slaveId'-definition", ERROR_MISSING_SLAVE_ID_DEFINITION);
        }

        // Get the slaveId
        $slaveId = Support::falseEmptyToZero($this->evaluate->parse($fe[FE_SLAVE_ID]));
        // Store the slaveId: it's used and replaced in the update statement.
        $this->store->setVar(VAR_SLAVE_ID, $slaveId, STORE_VAR, true);

        $mode = ($slaveId == '0') ? 'I' : 'U'; // I=Insert, U=Update
        $mode .= ($modeUpload == UPLOAD_MODE_NEW || $modeUpload == UPLOAD_MODE_DELETEOLD_NEW) ? 'N' : ''; // N=New File, '' if no new file.
        $mode .= ($modeUpload == UPLOAD_MODE_DELETEOLD) ? 'D' : ''; // Delete slave record only if there is no new and not 'unchanged'.

        switch ($mode) {
            case 'IN':
                $sql = $fe[FE_SQL_INSERT];
                $flagUpdateSlaveId = true;
                break;
            case 'UN':
                $sql = $fe[FE_SQL_UPDATE];
                break;
            case 'I':
            case 'U':
                $sql = ''; // no old file and no new file.
                break;
            case 'UD':
                $sql = $fe[FE_SQL_DELETE];
                $flagSlaveDeleted = true;
                break;
            default:
                throw new \CodeException('Unknown mode: ' . $mode, ERROR_UNKNOWN_MODE);
        }

        $rc = $this->evaluate->parse($sql);
        // Check if the slave record has been deleted: if yes, set slaveId=0
        if ($flagSlaveDeleted && $rc > 0) {
            $rc = 0;
            $flagUpdateSlaveId = true;
        }

        if ($flagUpdateSlaveId) {
            // Store the slaveId: it's used and replaced in the update statement.
            $this->store->setVar(VAR_SLAVE_ID, $rc, STORE_VAR, true);
            $slaveId = $rc;
        }

        return $slaveId;
    }

    /**
     * Remove not allowed tags.
     *
     * @param array $html_str
     * @param string $allowedTags
     * @param string $allowedAttributes
     * @return string
     */
    function custom_strip_tags($html, string $allowedTags) {
        $allowed_tags = explode(',', $allowedTags);
        $allowed_tags = array_map('strtolower', $allowed_tags);
        $regex_tags = '/<\/?([^>\s]+)[^>]*>/i';
        $matches = array();
        preg_match_all($regex_tags, $html, $matches);
        $rhtml = preg_replace_callback($regex_tags, function ($matches) use (&$allowed_tags) {
            return in_array(strtolower($matches[1]), $allowed_tags) ? $matches[0] : '';
        }, $html);
        return $rhtml;
    }

    /**
     * Remove not allowed attributes and content which is not in whitelist
     * Used in combination with htmlAllow.
     * Author:  Edward Z. Yang
     * Website: http://htmlpurifier.org/
     *
     * @param $html
     * @return array|string|string[]|null
     */
    function purifierHtml($html) {
        $purifier = new HTMLPurifier();
        $rhtml = $purifier->purify($html);
        return $rhtml;
    }

}
