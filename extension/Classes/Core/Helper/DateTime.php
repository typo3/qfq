<?php

namespace IMATHUZH\Qfq\Core\Helper;

use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Form\FormElement\DatetimeFormElement;
use IMATHUZH\Qfq\Core\Renderer\BaseRenderer;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 *
 */
class DateTime {

    /**
     * Builds HTML 'input' element.
     * Format: <input name="$htmlFormElementName" <type="date" [autocomplete="autocomplete"] [autofocus="autofocus"]
     *           [maxlength="$maxLength"] [placeholder="$placeholder"] [size="$size"] [min="$min"] [max="$max"]
     *           [pattern="$pattern"] [required="required"] [disabled="disabled"] value="$value">
     *
     * @param array $formElement
     * @param string $htmlFormElementName
     * @param string $value
     * @param array $json
     * @param array $formSpec
     * @param Store $store
     * @param string $mode FORM_LOAD | FORM_UPDATE | FORM_SAVE
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function buildDateTime(array $formElement, $htmlFormElementName, $value, array &$json, array $formSpec, Store $store, $mode = FORM_LOAD, $wrappedClass = ''): string {
        $attribute = '';
        $placeholder = '';
        $datetimeKeys = array(
            FE_DATE_LOCALE,
            FE_DATE_DAYS_OF_WEEK_ENABLED,
            FE_DATE_FORMAT,
            FE_DATE_VIEWMODE_DEFAULT,
            FE_INPUT_CLEAR_ME,
            FE_DATE_SHOW_CALENDAR_WEEKS,
            FE_DATE_CURRENT_DATETIME,
            FE_DATE_DATETIME_SIDE_SIDE_BY_SIDE,
            FE_MIN,
            FE_MAX
        );

        $datetimeAttributes = array(
            "locale",
            "days-of-week-disabled",
            "format",
            "view-mode-default",
            "show-clear-button",
            "show-calendar-weeks",
            "use-current-datetime",
            "datetime-side-by-side",
            "minDate",
            "maxDate"
        );

        $datePickerClassName = self::getDatePickerClassName($formElement, $formSpec);

        $attribute .= Support::doAttribute('id', $formElement[FE_HTML_ID]);
        $attribute .= Support::doAttribute('name', $htmlFormElementName);
        $attribute .= Support::doAttribute('class', 'form-control ' . $datePickerClassName);
        $attribute .= Support::doAttribute(ATTRIBUTE_DATA_REFERENCE, $formElement[FE_DATA_REFERENCE]);

        $showTime = ($formElement[FE_TYPE] == FE_TYPE_TIME || $formElement[FE_TYPE] == FE_TYPE_DATETIME) ? 1 : 0;
        if ($value == 'CURRENT_TIMESTAMP') {
            $value = date('Y-m-d H:i:s');
        }

        $defaultFormat = self::getDefaultDateFormat($formElement);

        $value = Support::convertDateTime($value, $defaultFormat['date'], $formElement[FE_SHOW_ZERO], $showTime, $formElement[FE_SHOW_SECONDS]);

        // Browser needs own formatted value to show date
        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_BROWSER && $value !== '') {
            $value = self::formatValueForBrowser($formElement[FE_TYPE], $value);
        }

        $tmpPattern = $formElement[FE_CHECK_PATTERN];
        //  If bootstrap datetimepicker (date and datetime FE Type) is used, check pattern is not needed. Keep pattern for FE Type time.
        if ($formElement[FE_TYPE] != FE_TYPE_DATETIME && $formElement[FE_TYPE] != FE_TYPE_DATE && $formElement[FE_TYPE] != FE_TYPE_TIME) {
            $formElement[FE_CHECK_PATTERN] = Support::dateTimeRegexp($formElement[FE_TYPE], $formElement[FE_DATE_FORMAT], $formElement[FE_TIME_IS_OPTIONAL]);
        }

        switch ($formElement[FE_CHECK_TYPE]) {
            case  SANITIZE_ALLOW_PATTERN:
                $formElement[FE_CHECK_PATTERN] = $tmpPattern;
                break;
            case SANITIZE_ALLOW_ALL:
            case SANITIZE_ALLOW_ALNUMX:
            case SANITIZE_ALLOW_ALLBUT:
                $formElement[FE_CHECK_TYPE] = SANITIZE_ALLOW_PATTERN;
                break;
            default:
                throw new \UserFormException("Checktype not applicable for date/time: '" . $formElement[FE_CHECK_TYPE] . "'", ERROR_NOT_APPLICABLE);
        }

        $tableColumnTypes = $store->getStore(STORE_TABLE_COLUMN_TYPES);
        // truncate if necessary
        if ($value != '' && $formElement[FE_MAX_LENGTH] > 0) {
            if ($formElement[FE_TYPE] === FE_TYPE_TIME && isset($tableColumnTypes[$formElement[FE_NAME]]) && $tableColumnTypes[$formElement[FE_NAME]] === DB_COLUMN_TYPE_DATETIME) {
                $value = explode(' ', $value);
                if (count($value) > 1) {
                    $value = $value[1];
                } else {
                    $value = $value[0];
                }
            } else {
                $value = substr($value, 0, $formElement[FE_MAX_LENGTH]);
            }
        }

        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_BROWSER) {
            $type = self::getDateTimeBrowserType($formElement[FE_TYPE]);
        } else {
            // date|time|datetime|datetime-local are not appropriate - only I18n representation possible.
            $type = 'text';
        }

        $attribute .= Support::doAttribute('type', $type);

        //Examples of accepted dateFormats: YYYY-MM-DD , DD.MM.YYYY, HH:mm(24h), hh:mm(12h AM PM)
        // Get dateformat default from T3 config and adjust it for datetimepicker because config default (dd.mm.yyyy) is not the default of bootstrap datetimepicker.
        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $defaultFormat = self::getDateTimeFormatQfq($defaultFormat, $formElement[FE_TYPE]);
        } else if ($formElement[FE_TYPE] == FE_TYPE_DATETIME) {
            if ($defaultFormat['time'] === '') {
                $defaultFormat['time'] = 'hh:mm';
            }
            $defaultFormat['date'] .= ' ' . $defaultFormat['time'];
        }

        // if FE type datetime and showSeconds is set, corrected format is needed
        if ($formElement[FE_TYPE] === FE_TYPE_DATETIME && $formElement[FE_SHOW_SECONDS] == 1 && !isset($defaultFormat['timeParts'][2])) {
            $defaultFormat['date'] .= ':ss';
        }

        // if FE type 'time' is used, overwrite $defaultFormat['date']
        if ($formElement[FE_TYPE] === FE_TYPE_TIME) {
            $defaultFormat = self::getTimeFormat($defaultFormat, $formElement[FE_DATE_TIME_PICKER_TYPE], $formElement[FE_SHOW_SECONDS]);
        }

        $formElement[FE_DATE_DAYS_OF_WEEK_ENABLED] = $formElement[FE_DATE_DAYS_OF_WEEK_ENABLED] ?? '';

        // Set correct parameter value for daysOfWeekDisabled attribute in FE
        if ($formElement[FE_DATE_DAYS_OF_WEEK_ENABLED] != '' && isset($formElement[FE_DATE_DAYS_OF_WEEK_ENABLED])) {
            $disabledDays = self::getDateTimePickerDisabledDays($formElement[FE_DATE_DAYS_OF_WEEK_ENABLED]);
            $formElement[FE_DATE_DAYS_OF_WEEK_ENABLED] = '[' . $disabledDays . ']';
        }

        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $formElement[FE_INPUT_CLEAR_ME] = empty($formElement[FE_INPUT_CLEAR_ME]) ? 'false' : 'true';
        }

        // Add datepicker attributes
        self::setDateTimePickerAttributes($formElement, $defaultFormat['date'], $datetimeKeys, $datetimeAttributes, $attribute);

        // 'maxLength' needs an upper 'L': naming convention for DB tables!
        $attribute .= HelperFormElement::getAttributeList($formElement, ['size', 'maxLength']);
        $attribute .= Support::doAttribute('value', htmlentities($value), false);

        if ($formElement[FE_PLACEHOLDER] == '' && ($formElement[FE_TYPE] === FE_TYPE_TIME || $formElement[FE_TYPE] === FE_TYPE_DATE || $formElement[FE_TYPE] === FE_TYPE_DATETIME)) {
            $placeholder = $defaultFormat['date'];
            $formElement[FE_PLACEHOLDER] = $placeholder;
        }

        if ($formElement[F_FE_DATA_PATTERN_ERROR] == '') {
            $formElement[F_FE_DATA_PATTERN_ERROR] = "Please match the format: " . $placeholder;
        }
        $attribute .= HelperFormElement::getAttributeList($formElement, [F_FE_DATA_PATTERN_ERROR, F_FE_DATA_REQUIRED_ERROR, F_FE_DATA_MATCH_ERROR, F_FE_DATA_ERROR]);

        $attribute .= HelperFormElement::getAttributeList($formElement, [FE_INPUT_AUTOCOMPLETE, 'autofocus', 'placeholder']);
        $attribute .= Support::doAttribute('data-load', ($formElement[FE_DYNAMIC_UPDATE] === 'yes') ? 'data-load' : '');
        $attribute .= Support::doAttribute('title', $formElement[FE_TOOLTIP]);

        $pattern = Sanitize::getInputCheckPattern($formElement[FE_CHECK_TYPE], $formElement[FE_CHECK_PATTERN], '', $sanitizeMessage);
        $attribute .= ($pattern === '') ? '' : 'pattern="' . $pattern . '" ';

        // Use system message only,if no custom one is set.
        if ($formElement[F_FE_DATA_PATTERN_ERROR] == $formElement[F_FE_DATA_PATTERN_ERROR_SYSTEM]) {
            $formElement[F_FE_DATA_PATTERN_ERROR] = $sanitizeMessage;
        }

        // For other dateTimePicker than qfq, min/max values need to be converted
        if ($formElement[FE_DATE_TIME_PICKER_TYPE] !== DATE_TIME_PICKER_QFQ) {
            self::setNoQfqMinMax($formElement);
        }

        $attribute .= HelperFormElement::getAttributeList($formElement, [FE_MIN, FE_MAX]);


        $json = BaseRenderer::getInstance()->renderFormElementJson(new DatetimeFormElement($formElement, new Form($formSpec)));

        $formElement = HelperFormElement::prepareExtraButton($formElement, true);

        $attribute .= HelperFormElement::getAttributeFeMode($formElement[FE_MODE]);

        $input = "<input $attribute>" . HelperFormElement::getHelpBlock() . $formElement[FE_TMP_EXTRA_BUTTON_HTML];

        if ($formElement[FE_TMP_EXTRA_BUTTON_HTML] !== '') {
            $input = Support::wrapTag('<div class="input-group">', $input);
        }

        return $input . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];

    }

    /**
     * @param $formElement
     * @param $formSpec
     * @return string
     */
    private static function getDatePickerClassName($formElement, $formSpec): string {
        $datePickerClassName = '';
        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $datePickerClassName = 'qfq-datepicker';
        } elseif (isset($formElement[FE_INPUT_CLEAR_ME]) && $formElement[FE_INPUT_CLEAR_ME] != '0' && $formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_NO) {
            if (($formSpec[F_MULTI_MODE] ?? '') == 'vertical') {
                $datePickerClassName = ' qfq-clear-me-multiform';
            } else {
                $datePickerClassName = ' qfq-clear-me';
            }
        }
        return $datePickerClassName;
    }

    /**
     * @param $formElement
     * @return array
     */
    private static function getDefaultDateFormat(&$formElement): array {
        $defaultDateFormat = explode(' ', $formElement[FE_DATE_FORMAT], 2);
        $defaultFormat = array();
        if (isset($defaultDateFormat[1])) {
            $defaultFormat['time'] = $defaultDateFormat[1];
            $defaultFormat['date'] = $defaultDateFormat[0];
            $defaultFormat['timeParts'] = explode(':', $defaultFormat['time'], 3);
        } else {
            $defaultFormat['date'] = $formElement[FE_DATE_FORMAT];
            $defaultFormat['timeParts'] = explode(':', $formElement[FE_DATE_FORMAT], 3);
        }

        if ($defaultFormat['timeParts'][2] ?? '' === 'ss') {
            $formElement[FE_SHOW_SECONDS] = 1;
        }
        return $defaultFormat;
    }

    /**
     * @param $feType
     * @param $value
     * @return false|mixed|string
     */
    private static function formatValueForBrowser($feType, $value) {
        $dateOldFormat = date_create($value);
        if ($feType === FE_TYPE_DATE) {
            $value = date_format($dateOldFormat, "Y-m-d");
        } elseif ($feType === FE_TYPE_DATETIME) {
            $value = date_format($dateOldFormat, "Y-m-d H:i");
        }
        return $value;
    }

    /**
     * @param $feType
     * @return string
     */
    private static function getDateTimeBrowserType($feType): string {
        if ($feType == FE_TYPE_DATE) {
            $type = 'date';
        } elseif ($feType == FE_TYPE_TIME) {
            $type = 'time';
        } else {
            $type = 'datetime-local';
        }
        return $type;
    }

    /**
     * @param $defaultFormat
     * @param $feType
     * @return array|string
     */
    private static function getDateTimeFormatQfq($defaultFormat, $feType) {
        switch ($defaultFormat['date']) {
            case FORMAT_DATE_INTERNATIONAL:
            case FORMAT_DATE_INTERNATIONAL_QFQ:
            case FORMAT_DATE_GERMAN:
            case FORMAT_DATE_GERMAN_QFQ:
                if ($feType == FE_TYPE_DATETIME) {
                    $defaultFormat['date'] = strtoupper($defaultFormat['date']);
                    if (empty($defaultFormat['time'])) {
                        $defaultFormat['time'] = 'HH:mm';
                    }
                    $defaultFormat['date'] .= ' ' . $defaultFormat['time'];
                } else {
                    $defaultFormat['date'] = strtoupper($defaultFormat['date']);
                }
                break;
        }
        return $defaultFormat;
    }

    /**
     * @param $defaultFormat
     * @param $feDateTimePickerType
     * @param $feShowSeconds
     * @return mixed
     */
    private static function getTimeFormat($defaultFormat, $feDateTimePickerType, $feShowSeconds) {
        if ($defaultFormat['timeParts'][0] === 'HH' || $defaultFormat['timeParts'][0] === 'hh') {
            $defaultFormat['date'] = $defaultFormat['timeParts'][0] . ':' . $defaultFormat['timeParts'][1];
        } else if ($feDateTimePickerType === DATE_TIME_PICKER_QFQ) {
            $defaultFormat['date'] = 'HH:mm';
        } else {
            $defaultFormat['date'] = 'hh:mm';
        }

        if ($feShowSeconds == 1) {
            $defaultFormat['date'] .= ':ss';
        }
        return $defaultFormat;
    }

    /**
     * @param $enabledDays
     * @return false|string
     */
    private static function getDateTimePickerDisabledDays($enabledDays) {
        // convert enabled days from datetimepicker user input daysOfWeekEnabled to disabled days
        $enabledDays = explode(',', $enabledDays);
        $disabledDays = '';

        for ($i = 0; $i <= 6; $i++) {
            $flagDayPoint = false;
            foreach ($enabledDays as $day) {
                if ($day == $i) {
                    $flagDayPoint = true;
                }
            }
            if (!$flagDayPoint) {
                $disabledDays .= $i . ',';
            }
        }

        // if last char ',' exists, remove it
        $lastChar = substr($disabledDays, -1);
        if ($lastChar == ',') {
            $disabledDays = substr($disabledDays, 0, -1);
        }
        return $disabledDays;
    }

    /**
     * @param $formElement
     * @param $defaultDateFormat
     * @param $dateTimeKeys
     * @param $dateTimeAttributes
     * @param $attribute
     * @return void
     * @throws \CodeException
     */
    private static function setDateTimePickerAttributes($formElement, $defaultDateFormat, $dateTimeKeys, $dateTimeAttributes, &$attribute) {
        $keyCount = 0;
        foreach ($dateTimeKeys as $key) {
            if (isset($formElement[$key]) && $formElement[$key] != "" && $key != FE_DATE_FORMAT) {
                $attribute .= Support::doAttribute('data-' . $dateTimeAttributes[$keyCount], $formElement[$key]);
            } elseif ($key == FE_DATE_FORMAT) {
                $attribute .= Support::doAttribute('data-' . $dateTimeAttributes[$keyCount], $defaultDateFormat);
            }
            $keyCount++;
        }
    }

    /**
     * @param $formElement
     * @return void
     */
    private static function setNoQfqMinMax(&$formElement) {
        if ($formElement[FE_TYPE] == FE_TYPE_DATE) {
            $dateMinMaxFormat = "Y-m-d";
        } else {
            $dateMinMaxFormat = "Y-m-d H:i";
        }

        if (isset($formElement[FE_MIN]) && $formElement[FE_MIN] !== '') {
            $dateMin = date_create($formElement[FE_MIN]);
            $formElement[FE_MIN] = date_format($dateMin, $dateMinMaxFormat);
        }

        if (isset($formElement[FE_MAX]) && $formElement[FE_MAX] !== '') {
            $dateMax = date_create($formElement[FE_MAX]);
            $formElement[FE_MAX] = date_format($dateMax, $dateMinMaxFormat);
        }
    }

    /**
     * Check  $value as date/datetime/time value and convert it to FORMAT_DATE_INTERNATIONAL.
     *
     * @param array $formElement - if not set, set $formElement[FE_DATE_FORMAT]
     * @param string $value - date/datetime/time value in format FORMAT_DATE_INTERNATIONAL or FORMAT_DATE_GERMAN
     *
     * @return string - checked datetime string
     * @throws \UserFormException
     */
    public static function doDateTime(array &$formElement, $value): string {

        // check for browser dateTimePickerType and adjust value
        $typeBrowser = false;

        if ($formElement[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_BROWSER) {
            $typeBrowser = true;
            $datetimeArray = explode("T", $value, 2);
            if (preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $datetimeArray[1])) {
                $value = $datetimeArray[0] . " " . $datetimeArray[1];
            }
        }

        $dateFormat = explode(':', $formElement[FE_DATE_FORMAT], 3);
        if (isset($dateFormat[2])) {
            if ($dateFormat[2] === 'ss')
                $formElement[FE_SHOW_SECONDS] = 1;
        }

        $regexp = Support::dateTimeRegexp($formElement[FE_TYPE], $formElement[FE_DATE_FORMAT], $formElement[FE_TIME_IS_OPTIONAL] ?? "");

        if (1 !== preg_match('/' . $regexp . '/', $value, $matches) && !$typeBrowser) {
            $placeholder = Support::getDateTimePlaceholder($formElement);
            throw new \UserFormException("DateTime format not recognized: $placeholder / $value ", ERROR_DATE_TIME_FORMAT_NOT_RECOGNISED);
        }

        $showTime = $formElement[FE_TYPE] == FE_TYPE_DATE ? '0' : '1';
        $value = Support::convertDateTime($value, FORMAT_DATE_INTERNATIONAL, '1', $showTime, $formElement[FE_SHOW_SECONDS]);
        $formElement[FE_MIN] = Support::convertDateTime($formElement[FE_MIN], FORMAT_DATE_INTERNATIONAL, '1', $showTime, $formElement[FE_SHOW_SECONDS]);
        if (isset($formElement[FE_MAX]) && $formElement[FE_MAX] !== '') {
            $formElement[FE_MAX] = Support::convertDateTime($formElement[FE_MAX], FORMAT_DATE_INTERNATIONAL, '1', $showTime, $formElement[FE_SHOW_SECONDS]);

        }

        if ($formElement[FE_TYPE] !== FE_TYPE_TIME) {
            // Validate date (e.g. 2010-02-31)
            $dateValue = explode(' ', $value)[0];
            $dateParts = explode('-', $dateValue);
            if (!checkdate($dateParts[1], $dateParts[2], $dateParts[0]))
                throw new \UserFormException("$dateValue is not a valid date.", ERROR_INVALID_DATE);
        }
        return $value;
    }
}