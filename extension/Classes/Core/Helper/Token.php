<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 2/10/18
 * Time: 3:14 PM
 */

namespace IMATHUZH\Qfq\Core\Helper;


/**
 * Class Token
 * @package qfq
 */
class Token {

    /**
     * Verify Empty values. If appropriate, set defaults, if not throw an exception.
     *
     * @param string $key
     * @param string $value
     *
     * @return string
     * @throws \UserReportException
     */
    public static function checkForEmptyValue($key, $value) {

        if ($value !== '') {
            return $value;
        }
        $value = '';

        switch ($key) {
            case TOKEN_URL:
            case TOKEN_MAIL:
                break; // No check - can't be generally decided
            case TOKEN_GLYPH:
                throw new \UserReportException ("Missing value for token '$key'", ERROR_MISSING_VALUE);
                break;
            case TOKEN_RIGHT:
                $value = 'r';
                break;
            case TOKEN_ENCRYPTION:
            case TOKEN_SIP:
            case TOKEN_BOOTSTRAP_BUTTON:
            case TOKEN_MONITOR:
            case TOKEN_DROPDOWN:
                $value = '1';
                break;
            case TOKEN_QUESTION:
                $value = DEFAULT_QUESTION_TEXT;
                break;
            case TOKEN_RENDER:
                $value = DEFAULT_RENDER_MODE;
                break;
            case TOKEN_ACTION_DELETE:
                $value = DEFAULT_ACTION_DELETE;
                break;
            case TOKEN_ORDER_TEXT:
                throw new \UserReportException ("Missing value for token '$key'", ERROR_MISSING_VALUE);
            case TOKEN_CACHE:
                $value = DEFAULT_CACHE;
                break;
            default:
        }

        return $value;
    }

    /**
     * Explode a string of the form 'T:<arg1>|W:<arg2>|s' into an array [ 'T' => '<arg1>', 'W' => '<arg2>', 's' => '' ].
     * Checks if a parameter is double defined.
     *
     * @param string $str
     * @return array
     * @throws \UserReportException
     */
    public static function explodeTokenString($str) {
        $tokenGiven = array();
        $control = array();

        $param = explode(PARAM_DELIMITER, $str);

        foreach ($param as $item) {

            // Skip empty entries
            if ($item === '') {
                continue;
            }

            // u:www.example.com
            $arr = explode(":", $item, 2);
            $key = isset($arr[0]) ? $arr[0] : '';
            $value = isset($arr[1]) ? $arr[1] : '';

            // Bookkeeping defined parameter.
            if (isset($tokenGiven[$key])) {
                throw new \UserReportException ("Multiple definitions for key '$key'", ERROR_MULTIPLE_DEFINITION);
            }
            $tokenGiven[$key] = true;

            $control[$key] = self::checkForEmptyValue($key, $value);
        }

        return $control;
    }

    /**
     * Converts a string of the form '[width]x[height]' to '[-w <width>][ -h <height]'
     *
     * @param string $dimension
     * @return string
     */
    public static function explodeDimension($dimension) {
        $width = '';
        $height = '';
        $sep = '';

        if (empty($dimension)) {
            return '';
        }

        $arr = explode('x', $dimension, 2);
        if (!empty($arr[0])) {
            $width = '-w ' . $arr[0];
            $sep = ' ';
        }

        if (!empty($arr[1])) {
            $height = $sep . '-h ' . $arr[1];
        }

        return $width . $height;
    }

}