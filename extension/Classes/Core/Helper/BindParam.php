<?php
/**
 * Created by PhpStorm.
 * User: nick9v at hotmail dot com ¶
 * Date: 2/10/16
 * Time: 2:05 PM
 */

namespace IMATHUZH\Qfq\Core\Helper;


/**
 * Class BindParam
 * @package qfq
 */
class BindParam {

    /* */
    private $values = array(), $types = '';

    /**
     * @param $value
     */
    public function add(&$value) {
        $this->values[] = &$value;
        $this->types .= $this->getParameterBindType($value);
    }

    /**
     * Depending of $value, returns i (integer), d (double) or s (string). This is needed for mysqli_bind().
     *
     * @param $value
     *
     * @return string
     */
    private function getParameterBindType($value) {

        if (is_int($value)) {
            $type = 'i';
        } elseif (is_double($value)) {
            $type = 'd';
        } else {
            $type = 's';
        }

        return $type;
    }

    /**
     * @return array
     */
    public function get() {

        return array_merge(array($this->types), $this->values);
    }
}