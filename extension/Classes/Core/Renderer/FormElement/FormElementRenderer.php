<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\BaseRenderer;

abstract class FormElementRenderer {

    public abstract function renderHtml(AbstractFormElement $fe): string;

    /**
     * @param AbstractFormElement $fe
     * @return array
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderJson(AbstractFormElement $fe): array {
        $wrap = BaseRenderer::getInstance()->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_CLASS];
        return HelperFormElement::getFormElementForJson($fe, $wrap);
    }

    /**
     * Renders a link to edit the given FormElement
     *
     * @param AbstractFormElement $fe
     * @return string
     * @throws \CodeException
     */
    protected function renderFormEditorUrl(AbstractFormElement $fe): string {
        if (!$fe->form->showDebugInfoFlag) return '';
        $titleAttr = Support::doAttribute(HTML_ATTR_TITLE, $fe->formEditorTooltip);
        $icon = Support::wrapTag('<span class="' . GLYPH_ICON . ' ' . GLYPH_ICON_EDIT . '">', '');
        return Support::wrapTag("<a class='hidden " . CLASS_FORM_ELEMENT_EDIT . "' href='" . $fe->formEditorUrl . "' $titleAttr>", $icon);
    }
}