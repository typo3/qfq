<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Base;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\NativeRenderer;

class AnnotateRenderer extends NativeRenderer {

    /**
     * @inheritDoc
     *
     * @param AbstractFormElement $fe
     * @return string
     */
    public function renderInput(AbstractFormElement $fe): string {
        $htmlFabric = Support::wrapTag('<div ' . Support::arrayToXMLAttributes($fe->fabricHtmlAttributes) . ' >', '', false);
        $htmlInput = Support::wrapTag('<input ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . ' >', '', false);
        return $htmlFabric . $htmlInput . HelperFormElement::getHelpBlock();
    }

}