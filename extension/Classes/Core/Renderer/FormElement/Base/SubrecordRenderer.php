<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Base;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;

class SubrecordRenderer extends \IMATHUZH\Qfq\Core\Renderer\FormElement\FormElementRenderer {

    /**
     * @param AbstractFormElement $fe
     * @return string
     */
    public function renderHtml(AbstractFormElement $fe): string {
        // TODO: Implement renderFormElement() method.
        return "SUBRECORD";
    }
}