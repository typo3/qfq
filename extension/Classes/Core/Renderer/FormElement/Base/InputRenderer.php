<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Base;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Renderer\FormElement\FormElementRenderer;
use IMATHUZH\Qfq\Core\Renderer\FormElement\NativeRenderer;

class InputRenderer extends NativeRenderer {

    /**
     * @param AbstractFormElement $fe
     * @return string
     */
    public function renderInput(AbstractFormElement $fe): string {
        // TODO: Base Input
        return "";
    }
}