<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Base;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

class ImageCutRenderer extends \IMATHUZH\Qfq\Core\Renderer\FormElement\NativeRenderer {

    /**
     * @inheritDoc
     */
    public function renderInput(AbstractFormElement $fe): string {
        $fe->loadImage();

        $htmlFabric = Support::wrapTag('<div ' . Support::arrayToXMLAttributes($fe->fabricHtmlAttributes) . ' >', '', false);
        $htmlInput = Support::wrapTag('<input ' . Support::arrayToXMLAttributes($fe->inputHtmlAttributes) . ' >', '', false);
        $htmlTargetImage = Support::wrapTag('<input ' . Support::arrayToXMLAttributes($fe->inputBase64HtmlAttributes) . ' >', '', false);
        // <img id="target-png"> - this element is for future use. It's not necessary for the function and is not used yet.
        return $htmlFabric . HelperFormElement::getHelpBlock() . $htmlInput . $htmlTargetImage;
    }
}