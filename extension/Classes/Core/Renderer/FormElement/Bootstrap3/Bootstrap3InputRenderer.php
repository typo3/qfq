<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\InputRenderer;

class Bootstrap3InputRenderer extends InputRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        $classes = 'form-control ' . implode(' ', $fe->cssClasses);
        $htmlTag = $fe->htmlTag;
        $fe->htmlAttributes[CSS_CLASS] = $classes;
        if ($htmlTag == 'textarea') {
            ($fe->attributes[FE_TEXTAREA_RESIZE] ?? '1') == '0' ? $fe->htmlAttributes[ATTRIBUTE_STYLE] = 'resize: none;' : $fe->htmlAttributes[ATTRIBUTE_STYLE] = 'resize: block;' ;
        }
        $htmlAttributes = Support::arrayToXMLAttributes($fe->htmlAttributes);


        $characterCountHtml = "";
        if (isset($fe->attributes[FE_CHARACTER_COUNT_WRAP])) {
            $characterCountAttributes = Support::arrayToXMLAttributes($fe->characterCountHtmlAttributes);
            $characterCountAttributes .= Support::doAttribute('class', $fe->characterCountCssClasses);
            $customWrapStart = '';
            $customWrapEnd = '';

            // Custom Wrapping Tag, e.g. <p>|</p>
            if ($fe->attributes[FE_CHARACTER_COUNT_WRAP] != '') {
                $arr = explode('|', $fe->attributes[FE_CHARACTER_COUNT_WRAP], 2);
                $arr[] = '';
                $arr[] = ''; //skip check that at least 2 elements exist
                $customWrapStart = $arr[0];
                $customWrapEnd = $arr[1];
            }
            $characterCountHtml = <<<EOL
                                    $customWrapStart
                                        <span $characterCountAttributes></span>
                                    $customWrapEnd
                                    EOL;
        }

        $output = '<' . $htmlTag . ' ' . $htmlAttributes . '>' . ($htmlTag == 'textarea' ? $fe->value : '') . '</' . $htmlTag . '>';

        // Show the extra button if extraButtonInfo or extraButtonPassword or extraButtonLock are set
        if ( ($fe->attributes[FE_TMP_EXTRA_BUTTON_HTML]??'') !== ''
            || isset($fe->attributes[FE_INPUT_EXTRA_BUTTON_PASSWORD])
            || isset($fe->attributes[FE_INPUT_EXTRA_BUTTON_LOCK])
            || isset($fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO])) {
            $fe->attributes = HelperFormElement::prepareExtraButton($fe->attributes, true);
            if ($htmlTag == 'textarea') {
                $output .= $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML];
                $output .= $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO]; // Info text has already been wrapped with HTML in HelperFormElement::prepareExtraButton
            } else {
                $output = Support::wrapTag('<div class="input-group">', $output . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML]);
                $output .= $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO]; // Info text has already been wrapped with HTML in HelperFormElement::prepareExtraButton
            }
        }

        $output .= HelperFormElement::getHelpBlock() . $characterCountHtml;
        if (isset($formElement[FE_INPUT_EXTRA_BUTTON_INFO])) {
            $output .= $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
        }
        return $output;

    }
}