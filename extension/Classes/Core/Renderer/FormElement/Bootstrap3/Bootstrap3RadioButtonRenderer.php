<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\RadioButtonRenderer;

class Bootstrap3RadioButtonRenderer extends RadioButtonRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        if (isset($fe->attributes[FE_BUTTON_CLASS])) {
            if ($fe->attributes[FE_BUTTON_CLASS] == '') {
                $fe->attributes[FE_BUTTON_CLASS] = 'btn-default';
            }
            // BS Buttons
            $html = $this->renderRadioButton($fe, $renderMode);
        } else {
            // Plain
            $html = $this->renderRadioPlain($fe, $renderMode);
        }
        $fe->attributes = HelperFormElement::prepareExtraButton($fe->attributes, false);
        return $html . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] . HelperFormElement::getHelpBlock() . $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO];
    }

    /**
     * Renders the classic round radiobuttons
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     */
    private function renderRadioPlain(AbstractFormElement $fe, string $renderMode): string {
        $html = '';
        $attributeBaseLabel = Support::doAttribute(ATTRIBUTE_STYLE, 'min-width: ' . $fe->attributes[F_FE_MIN_WIDTH] . 'px; font-weight: 400;');

        $orientation = ($fe->attributes[FE_MAX_LENGTH] > 1) ? ALIGN_HORIZONTAL : ALIGN_VERTICAL;
        $radioClass = ($orientation === ALIGN_HORIZONTAL) ? 'radio-inline' : 'radio';
        if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
            $radioClass .= ' qfq-disabled';
        }
        $br = '';

        // TODO: Used in getFormElementForJson() for dynamic update.
        //$formElement[FE_TMP_CLASS_OPTION] = $radioClass;

        $jj = 0;
        for ($ii = 0; $ii < count($fe->itemValues); $ii++) {
            $jj++;
            $optionHtmlAttributes = $fe->htmlAttributes;
            $optionHtmlAttributes[HTML_ATTR_ID] = HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii);
            $optionHtmlAttributes[HTML_ATTR_VALUE] = $fe->itemKeys[$ii];
            $optionHtmlAttributes[ATTRIBUTE_DATA_REFERENCE] = $fe->attributes[FE_DATA_REFERENCE] . '-' . $ii;

            if ($fe->itemKeys[$ii] == $fe->value) {
                $optionHtmlAttributes[FE_CHECKBOX_CHECKED] = FE_CHECKBOX_CHECKED;
            } else if (isset($optionHtmlAttributes[FE_CHECKBOX_CHECKED])) {
                unset($optionHtmlAttributes[FE_CHECKBOX_CHECKED]);
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $tmpValue = ($fe->itemValues[$ii] === '') ? '&nbsp;' : $fe->itemValues[$ii];

            $htmlElement = '<input ' . Support::arrayToXMLAttributes($optionHtmlAttributes) . '>' . $tmpValue;

            if ($fe->attributes[FE_MAX_LENGTH] > 1) {
                if ($jj == $fe->attributes[FE_MAX_LENGTH]) {
                    $jj = 0;
                    $br = '<br>';
                } else {
                    $br = '';
                }
            }

            $wrapAttribute = Support::doAttribute(HTML_ATTR_TITLE, $fe->attributes[FE_TOOLTIP]);
            $wrapAttribute .= Support::doAttribute(HTML_ATTR_CLASS, $radioClass);

            $radioLabelId = HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii, HTML_ID_EXTENSION_LABEL);
            $htmlElement = Support::wrapTag("<label $wrapAttribute $attributeBaseLabel id='$radioLabelId'>", $htmlElement) . $br;

            $html .= $htmlElement;
        }

        return $html;
    }

    /**
     * Renders bootstrap3 buttons instead of the classic round radiobuttons
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     */
    private function renderRadioButton(AbstractFormElement $fe, string $renderMode): string {
        $html = '';
        for ($ii = 0; $ii < count($fe->itemValues); $ii++) {
            $classActive = '';
            $classReadonly = '';

            $optionHtmlAttributes = $fe->htmlAttributes;
            $optionHtmlAttributes[HTML_ATTR_ID] = $fe->attributes[FE_HTML_ID] . '-' . $ii;
            $optionHtmlAttributes[HTML_ATTR_VALUE] = $fe->itemKeys[$ii];
            $optionHtmlAttributes[ATTRIBUTE_DATA_REFERENCE] = $fe->attributes[FE_DATA_REFERENCE] . '-' . $ii;

            if ($fe->itemKeys[$ii] == $fe->value) {
                $optionHtmlAttributes[FE_CHECKBOX_CHECKED] = FE_CHECKBOX_CHECKED;
                $classActive = ' active';
            }

            if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
                $classReadonly = 'noclick';
                if ($fe->itemKeys[$ii] != $fe->value) {
                    $classReadonly .= ' disabled';
                }
            }

            if ($fe->itemValues[$ii] == '') { // In case the value is empty, the rendered button looks bad. Set '&nbsp;'.
                $fe->itemValues[$ii] = '&nbsp;';
            }

            $htmlElement = '<input ' . Support::arrayToXMLAttributes($optionHtmlAttributes) . '>' . $fe->itemValues[$ii];

            $labelAttribute = Support::doAttribute(HTML_ATTR_TITLE, $fe->attributes[FE_TOOLTIP]);
            $labelAttribute .= Support::doAttribute(HTML_ATTR_CLASS, 'btn ' . $fe->attributes[FE_BUTTON_CLASS] . $classReadonly . $classActive);
            $htmlElement = Support::wrapTag("<label $labelAttribute>", $htmlElement);

            $html .= $htmlElement;
        }

        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);
        return $html;
    }
}