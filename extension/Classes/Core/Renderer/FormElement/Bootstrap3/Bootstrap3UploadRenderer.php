<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\UploadRenderer;
use IMATHUZH\Qfq\Core\Report\Link;

class Bootstrap3UploadRenderer extends UploadRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {

        // Filepond
        if ($fe->attributes[UPLOAD_TYPE] === UPLOAD_TYPE_DND) {
            return $this->renderFilepond($fe);

            // Classic
        } else {
            return $this->renderClassic($fe);
        }
    }

    /**
     * Renders a normal/classic upload FE
     *
     * @param AbstractFormElement $fe
     * @return string
     * @throws \CodeException
     */
    private function renderClassic(AbstractFormElement $fe): string {
        $htmlInputFile = '<input ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>';
        $attributeFileLabel = Support::doAttribute('for', $fe->attributes[FE_HTML_ID]);
        $attributeFileLabel .= Support::doAttribute('class', 'btn btn-default ' . implode(' ', $fe->cssClasses) . ($fe->value === '' ? '' : 'hidden'));
        $htmlFilename = Support::wrapTag("<span class='uploaded-file-name'>", $fe->value, false);
        $htmlInputFile = Support::wrapTag("<label $attributeFileLabel>", $htmlInputFile . $fe->attributes[FE_FILE_BUTTON_TEXT]);

        // Delete option enabled
        if (($fe->attributes[FE_FILE_TRASH] ?? '1') == '1') {
            $disabled = ($fe->attributes[FE_MODE] == FE_MODE_READONLY) ? 'disabled' : '';
            $deleteButton = Support::wrapTag("<button type='button' class='btn btn-default delete-file $disabled' $disabled data-sip='" . $fe->uploadLink . "' name='delete-" . $fe->htmlAttributes[HTML_ATTR_NAME] . "'>", "<span class='glyphicon " . GLYPH_ICON_DELETE . "'></span>" . ($fe->attributes[FE_FILE_TRASH_TEXT] ?? ''));

            // Delete option disabled
        } else {
            $deleteButton = '';
        }

        $htmlTextDelete = Support::wrapTag("<div class='uploaded-file " . implode(' ', $fe->textDeleteCssClasses) . "'>", $htmlFilename . ' ' . $deleteButton);
        $hiddenSipUpload = HelperFormElement::buildNativeHidden($fe->htmlAttributes[HTML_ATTR_NAME], $fe->uploadLink);

        return $htmlTextDelete . $htmlInputFile . $hiddenSipUpload . ($fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] ?? '') . ($fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO] ?? '');
    }


    /**
     * Renders a Filepond upload FE (Multi-upload, Drag and Drop)
     *
     * @param AbstractFormElement $fe
     * @return string
     */
    private function renderFilepond(AbstractFormElement $fe): string {

        $disabled = ($fe->attributes[FE_MODE] == FE_MODE_READONLY) ? ' disabled' : '';
        $input = '<input class="' . implode(' ', $fe->cssClasses) . '" data-preloadedFiles="' . ($fe->htmlAttributes['data-preloadedFiles'] ?? '') . '" data-api-urls="' . $fe->htmlAttributes['data-api-urls'] . '" data-sips="' . $fe->htmlAttributes['data-sips'] . '" data-config="' . $fe->htmlAttributes['data-config'] . '" type="file" ' . $disabled . '>';
        //$input = '<input ' . implode(' ', $fe->cssClasses) . ' ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . $disabled . '>';
        $hiddenSipUpload = HelperFormElement::buildNativeHidden($fe->htmlAttributes[HTML_ATTR_NAME], $fe->uploadLink);

        $filepond = $input . $hiddenSipUpload . ($fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] ?? '') . ($fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO] ?? '');;

        return $filepond;
    }
}