<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\NoteRenderer;

class Bootstrap3NoteRenderer extends NoteRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        $htmlAttributes = Support::arrayToXMLAttributes($fe->htmlAttributes);
        return Support::wrapTag("<div $htmlAttributes class='" . CLASS_NOTE . "'>", $fe->value);
    }
}
