<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\EditorRenderer;
use IMATHUZH\Qfq\Core\Helper\Support;

class Bootstrap3EditorRenderer extends EditorRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        switch ($fe->attributes[FE_EDITOR_TYPE]) {
            case FE_EDITOR_TYPE_TINYMCE:
                return $this->renderTinyMCE($fe, $renderMode);
                break;
            case FE_EDITOR_TYPE_CODEMIRROR:
                return $this->renderCodeMirror($fe, $renderMode);
                break;
            default:
                return '<div class="alert alert-danger">Cannot render editor of type "' . $fe->attributes[FE_EDITOR_TYPE] . '"</div>';
        }
    }


    /**
     * Render TinyMCE
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function renderTinyMCE(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        // Prepare Character Count
        $elementCharacterCount = "<span " . Support::arrayToXMLAttributes($fe->htmlAttributesCharacterCount) . "></span>";
        if (isset($fe->attributes[FE_CHARACTER_COUNT_WRAP]) && $fe->attributes[FE_CHARACTER_COUNT_WRAP] != '') {
            $arr = explode('|', $fe->attributes[FE_CHARACTER_COUNT_WRAP], 2);
            $arr[] = '';
            $arr[] = ''; //skip check that at least 2 elements exist
            $elementCharacterCount = $arr[0] . $elementCharacterCount . $arr[1];
        }

        // Render Editor with Character Count
        $html = Support::wrapTag("<textarea " . Support::arrayToXMLAttributes($fe->htmlAttributes) . " class=\"" . implode(' ', $fe->cssClasses) . "\">", htmlentities($fe->value), false);
        $formElement = HelperFormElement::prepareExtraButton($fe->attributes, false);
        return $html . HelperFormElement::getHelpBlock() . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] . $elementCharacterCount . $formElement[FE_INPUT_EXTRA_BUTTON_INFO];
    }


    /**
     * Render TinyMCE
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return void
     *
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function renderCodeMirror(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        $html = Support::wrapTag("<textarea " . Support::arrayToXMLAttributes($fe->htmlAttributes) . " class=\"" . implode(' ', $fe->cssClasses) . "\">", htmlentities($fe->value), false);
        $formElement = HelperFormElement::prepareExtraButton($fe->attributes, false);
        return $html . HelperFormElement::getHelpBlock() . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] . $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO];
    }
}