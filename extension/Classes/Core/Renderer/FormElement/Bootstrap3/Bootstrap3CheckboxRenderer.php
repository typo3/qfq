<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Form\FormElement\CheckboxFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Store\Store;

class Bootstrap3CheckboxRenderer extends \IMATHUZH\Qfq\Core\Renderer\FormElement\Base\CheckboxRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {

        switch ($fe->attributes[FE_CHECKBOX_MODE]) {
            case 'single':
                $html = $this->renderSingle($fe, $renderMode);
                break;
            case 'multi';
                $html = $this->renderMulti($fe, $renderMode);
                break;
            default:
                throw new \UserFormException('checkBoxMode: \'' . $fe->attributes[FE_CHECKBOX_MODE] . '\' is unknown.', ERROR_CHECKBOXMODE_UNKNOWN);
        }

        $fe->attributes = HelperFormElement::prepareExtraButton($fe->attributes, false);

        return $html . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML] . HelperFormElement::getHelpBlock() . $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO];
    }


    /**
     * Render a single Checkbox
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function renderSingle(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {

        $fe->htmlAttributes[HTML_ATTR_ID] = $fe->attributes[FE_HTML_ID] . '-0';
        $fe->htmlAttributes[HTML_ATTR_VALUE] = $fe->attributes[FE_CHECKBOX_CHECKED];
        if ($fe->attributes[FE_DYNAMIC_UPDATE] === 'yes') $fe->htmlAttributes[FE_DATA_LOAD] = FE_DATA_LOAD;
        $fe->htmlAttributes[ATTRIBUTE_DATA_REFERENCE] = $fe->attributes[FE_DATA_REFERENCE];

        if ($fe->attributes[FE_CHECKBOX_CHECKED] === $fe->value) {
            $fe->htmlAttributes[HTML_ATTR_CHECKED] = 'checked';
        }

        if (isset($fe->attributes[FE_AUTOFOCUS])) $fe->htmlAttributes[FE_AUTOFOCUS] = $fe->attributes[FE_AUTOFOCUS];
        if (isset($fe->attributes[F_FE_DATA_REQUIRED_ERROR])) $fe->htmlAttributes[F_FE_DATA_REQUIRED_ERROR] = $fe->attributes[F_FE_DATA_REQUIRED_ERROR];

        if (isset($fe->attributes[FE_BUTTON_CLASS])) {
            // Buttons
            return $this->renderSingleButtons($fe, $renderMode);

        } else {
            // Plain Checkboxes
            return $this->renderSinglePlain($fe, $renderMode);
        }
    }


    /**
     * Build a single HTML plain checkbox based on two values.
     * Create a 'hidden' input field and a 'checkbox' input field - both with the same HTML 'name'.
     * HTML does not submit an unchecked checkbox. Then only the 'hidden' input field is submitted.
     *
     * <input name="$htmlFormElementName" type="radio" [autofocus="autofocus"]
     *            [required="required"] [disabled="disabled"] value="<value>" [checked="checked"] >
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderSinglePlain(AbstractFormElement $fe, string $renderMode): string {

        $htmlHidden = HelperFormElement::buildNativeHidden($fe->htmlAttributes[HTML_ATTR_NAME], $fe->attributes[FE_CHECKBOX_UNCHECKED]);
        Store::getInstance()->setVar($fe->htmlAttributes[HTML_ATTR_NAME], $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);

        $html = '<input ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>';
        $html .= '<span class="checkmark" aria="hidden"></span>';

        if (isset($fe->attributes[FE_LABEL_2])) {
            $html .= Support::wrapTag("<span style='font-weight: 400;'>", $fe->attributes['label2']);
        }

        $fe->attributes[FE_BUTTON_CLASS] = 'checkbox';
        if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
            $fe->attributes[FE_BUTTON_CLASS] .= ' qfq-disabled'; // necessary for own style checkboxes to display them 'disabled'
        }

        $fe->attributes[FE_TMP_CLASS_OPTION] = $fe->attributes[FE_BUTTON_CLASS];
        $labelAttribute = Support::doAttribute(HTML_ATTR_TITLE, $fe->attributes[FE_TOOLTIP]);
        $labelAttribute .= Support::doAttribute(HTML_ATTR_CLASS, $fe->attributes[FE_BUTTON_CLASS]);
        $labelAttribute .= Support::doAttribute(HTML_ATTR_ID, HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], 0, HTML_ID_EXTENSION_LABEL));
        $html = Support::wrapTag("<label $labelAttribute>", $html, true);
        return $html;
    }

    /**
     * Build a Checkbox based on two values with Bootstrap Button class.
     *
     * <div class="btn-group" data-toggle="buttons">
     *    <input type="hidden" name="$htmlFormElementName" value="$valueUnChecked">
     *    <label class="btn btn-primary active">
     *       <input type="checkbox" autocomplete="off" name="$htmlFormElementName" value="$valueChecked"checked>
     *       Checkbox 1 (pre-checked)
     *    </label>
     * </div>
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderSingleButtons(AbstractFormElement $fe, string $renderMode): string {
        $html = '';

        if (!isset($fe->attributes[FE_BUTTON_CLASS]) || $fe->attributes[FE_BUTTON_CLASS] == '') {
            $fe->attributes[FE_BUTTON_CLASS] = 'btn-default';
        }
        $fe->attributes[FE_BUTTON_CLASS] = 'btn ' . $fe->attributes[FE_BUTTON_CLASS];

        if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
            $fe->attributes[FE_BUTTON_CLASS] .= ' disabled';
        }

        $classActive = '';
        if ($fe->attributes[FE_CHECKBOX_CHECKED] === $fe->value) {
            $fe->attributes[FE_BUTTON_CLASS] .= ' active';
        }

        $htmlHidden = HelperFormElement::buildNativeHidden($fe->htmlAttributes[HTML_ATTR_NAME], $fe->attributes[FE_CHECKBOX_UNCHECKED]);
        Store::getInstance()->setVar($fe->htmlAttributes[HTML_ATTR_NAME], $htmlHidden, STORE_ADDITIONAL_FORM_ELEMENTS, false);
        $html = '';

        $htmlElement = '<input ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>';
        if (isset($fe->attributes[FE_LABEL_2])) {
            $htmlElement .= $fe->attributes[FE_LABEL_2];
        } else {
            $htmlElement .= $fe->attributes[FE_CHECKBOX_CHECKED];
        }

        $labelAttribute = Support::doAttribute(HTML_ATTR_TITLE, $fe->attributes[FE_TOOLTIP]);
        $labelAttribute .= Support::doAttribute(HTML_ATTR_CLASS, $fe->attributes[FE_BUTTON_CLASS]);
        $labelAttribute .= Support::doAttribute(HTML_ATTR_ID, HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], 0, HTML_ID_EXTENSION_LABEL));
        $html .= Support::wrapTag("<label $labelAttribute>", $htmlElement, true);
        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);

        $formElement[FE_TMP_CLASS_OPTION] = $fe->attributes[FE_BUTTON_CLASS];
        return $html;
    }


    /**
     * Render CheckboxList
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     */
    private function renderMulti(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        if (isset($fe->attributes[FE_BUTTON_CLASS])) {

            if ($fe->attributes[FE_BUTTON_CLASS] == '') {
                $fe->attributes[FE_BUTTON_CLASS] = 'btn-default';
            }
            $fe->attributes[FE_BUTTON_CLASS] = 'btn ' . $fe->attributes[FE_BUTTON_CLASS];

            if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
                $fe->attributes[FE_BUTTON_CLASS] .= ' disabled';
            }

            return $this->renderMultiButtons($fe, $renderMode);
        } else {
            return $this->renderMultiPlain($fe, $renderMode);
        }
    }


    /**
     * Build as many Checkboxes as items.
     *
     * Layout: The Bootstrap Layout needs very special setup, the checkboxes are wrapped differently with
     *         <div class=checkbox> depending on if they aligned horizontal or vertical
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     */
    private function renderMultiButtons(AbstractFormElement $fe, string $renderMode): string {
        if ($fe->attributes[FE_DYNAMIC_UPDATE] === 'yes') $fe->htmlAttributes[FE_DATA_LOAD] = 'data-load';

        if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
            $fe->htmlAttributes[ATTRIBUTE_DISABLED] = 'disabled';
        }

        $html = '';

        // Used in getFormElementForJson() for dynamic update.
        $labelBaseClass = $fe->attributes[FE_BUTTON_CLASS];

        for ($ii = 0, $jj = 1; $ii < count($fe->itemKeys); $ii++, $jj++) {

            $attribute = $fe->htmlAttributes;
            $classActive = '';

            $htmlFormElementNameUniq = HelperFormElement::prependFormElementNameCheckBoxMulti($fe->htmlAttributes[HTML_ATTR_NAME], $ii, true);
            $attribute[HTML_ATTR_ID] = HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii);
            $attribute[HTML_ATTR_NAME] = $htmlFormElementNameUniq;
            $attribute[ATTRIBUTE_DATA_REFERENCE] = $fe->attributes[FE_DATA_REFERENCE] . '-' . $ii;
            $attribute = Support::arrayToXMLAttributes($attribute);

            if (isset($fe->attributes[FE_AUTOFOCUS])) {
                $attribute .= Support::doAttribute(FE_AUTOFOCUS, $fe->attributes[FE_AUTOFOCUS]);
                unset ($fe->attributes[FE_AUTOFOCUS]);
            }

            $attribute .= Support::doAttribute(HTML_ATTR_VALUE, $fe->itemKeys[$ii], false);

            // Check if the given key is found in field.
            $values = explode(',', $fe->value); // Defines which of the checkboxes will be checked.
            if (false !== array_search($fe->itemKeys[$ii], $values)) {
                $attribute .= Support::doAttribute(HTML_ATTR_CHECKED, HTML_ATTR_CHECKED);
                $classActive = ' active';
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $valueShow = ($fe->itemValues[$ii] === '') ? '&nbsp;' : $fe->itemValues[$ii];

            $htmlElement = '<input ' . $attribute . '>' . $valueShow . '<span class="checkmark" aria="hidden"></span>';

            $labelAttribute = Support::doAttribute('title', $fe->attributes[FE_TOOLTIP]);
            $labelAttribute .= Support::doAttribute('class', $fe->attributes[FE_BUTTON_CLASS] . $classActive);
            $labelAttribute .= Support::doAttribute('id', HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii, HTML_ID_EXTENSION_LABEL));
            $html .= Support::wrapTag("<label $labelAttribute>", $htmlElement, true);

            $fe->attributes[FE_TMP_CLASS_OPTION] = $labelBaseClass . $classActive;
        }

        $html = Support::wrapTag('<div class="btn-group" data-toggle="buttons">', $html);

        return $html;
    }

    /**
     * Build as many Checkboxes as items.
     *
     * Layout: The Bootstrap Layout needs very special setup, the checkboxes are wrapped differently with <div
     * class=checkbox> depending on if they aligned horizontal or vertical.
     *
     *  <div class="col-md-6" required="required">
     *    <label class="checkbox-inline" id="1039-21332-1-0-0-l">
     *      <input checked="checked" id="1039-21332-1-0-0" name="groups-1[]" required="required" type="checkbox" value="1">
     *        label1
     *        <span aria="hidden" class="checkmark"></span>
     *      </label>
     *
     *    <label class="checkbox-inline" id="1039-21332-1-0-2-l">
     *      <input id="1039-21332-1-2-0" name="groups-1[]"
     *        required="required" type="checkbox" value="2">
     *        label2
     *        <span aria="hidden" class="checkmark"></span>
     *    </label>
     *  </div>
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException\
     */
    private function renderMultiPlain(AbstractFormElement $fe, string $renderMode): string {
        $fe->htmlAttributes[FE_DATA_LOAD] = $fe->attributes[FE_DYNAMIC_UPDATE] === 'yes' ? FE_DATA_LOAD : '';

        $attributeBaseLabel = Support::doAttribute('style', 'min-width: ' . $fe->attributes[F_FE_MIN_WIDTH] . 'px;');

        $html = '';

        $orientation = ($fe->attributes[FE_MAX_LENGTH] > 1) ? ALIGN_HORIZONTAL : ALIGN_VERTICAL;
        $checkboxClass = ($orientation === ALIGN_HORIZONTAL) ? 'checkbox-inline' : 'checkbox';
        if ($fe->attributes[FE_MODE] == FE_MODE_READONLY) {
            $checkboxClass .= ' qfq-disabled'; // necessary for own style checkboxes to display them 'disabled'
        }

        // Used in getFormElementForJson() for dynamic update.
        $fe->attributes[FE_TMP_CLASS_OPTION] = $checkboxClass;

        $br = '';

        for ($ii = 0, $jj = 1; $ii < count($fe->itemKeys); $ii++, $jj++) {
            $attribute = $fe->htmlAttributes;
            $htmlFormElementNameUniq = HelperFormElement::prependFormElementNameCheckBoxMulti($fe->htmlAttributes['name'], $ii, true);
            $attribute[HTML_ATTR_ID] = HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii);
            $attribute[HTML_ATTR_NAME] = $htmlFormElementNameUniq;
            $attribute[ATTRIBUTE_DATA_REFERENCE] = $fe->attributes[FE_DATA_REFERENCE] . '-' . $ii;

            if (isset($fe->attributes[FE_AUTOFOCUS])) {
                $attribute[FE_AUTOFOCUS] = $fe->attributes[FE_AUTOFOCUS];
                unset ($fe->attributes[FE_AUTOFOCUS]);
            }

            $attribute[HTML_ATTR_VALUE] = $fe->itemKeys[$ii];

            // Check if the given key is found in field.
            $values = explode(',', $fe->value); // Defines which of the checkboxes will be checked.
            if (false !== array_search($fe->itemKeys[$ii], $values)) {
                $attribute[FE_CHECKBOX_CHECKED] = FE_CHECKBOX_CHECKED;
            }

            // '&nbsp;' - This is necessary to correctly align an empty input.
            $valueShow = ($fe->itemValues[$ii] === '') ? '&nbsp;' : $fe->itemValues[$ii];

            $htmlElement = '<input ' . Support::arrayToXMLAttributes($attribute) . '>' . $valueShow . '<span class="checkmark" aria="hidden"></span>';

            $checkboxLabelId = HelperFormElement::getCheckboxRadioOptionId($fe->attributes[FE_HTML_ID], $ii, HTML_ID_EXTENSION_LABEL);
            $htmlElement = Support::wrapTag("<label class=\"$checkboxClass\" $attributeBaseLabel id=\"$checkboxLabelId\">", $htmlElement, true);

            // control orientation
            if ($fe->attributes[FE_MAX_LENGTH] > 1) {
                if ($jj == $fe->attributes[FE_MAX_LENGTH]) {
                    $jj = 0;
                    $br = '<br>';
                } else {
                    $br = '';
                }
            }
            $html .= $htmlElement . $br;
        }
        return $html;
    }
}