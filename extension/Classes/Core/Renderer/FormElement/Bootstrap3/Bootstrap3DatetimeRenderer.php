<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

class Bootstrap3DatetimeRenderer extends \IMATHUZH\Qfq\Core\Renderer\FormElement\Base\DatetimeRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {


        $attributesWithExtraBtn = HelperFormElement::prepareExtraButton($fe->attributes, true);


        $input = "<input " . Support::arrayToXMLAttributes($fe->htmlAttributes)
            . Support::doAttribute(CSS_CLASS, implode(' ', $fe->cssClasses)) . "/>"
            . HelperFormElement::getHelpBlock() . $attributesWithExtraBtn[FE_TMP_EXTRA_BUTTON_HTML];

        if ($attributesWithExtraBtn[FE_TMP_EXTRA_BUTTON_HTML] !== '') {
            $input = Support::wrapTag('<div class="input-group">', $input);
        }

        return $input . $attributesWithExtraBtn[FE_INPUT_EXTRA_BUTTON_INFO];

    }
}