<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\SelectRenderer;

class Bootstrap3SelectRenderer extends SelectRenderer {

    /**
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function renderInput(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        $option = '';
        $firstSelect = true;
        $jsonValues = array();
        $total = count($fe->itemValues);

        // selectBS
        $firstSelectText = '<span class="caret"></span>';
        $firstSelectValue = '';
        $dropdownElement = count($fe->itemGroupKeys) === 0 ? 'option' : 'li';
        $selectBS = ($dropdownElement === 'li');
        $style = '';
        $groupLevel = 0;

        for ($ii = 0; $ii < $total; $ii++) {

            $groupName = $fe->itemGroupValues[$ii] ?? '';

            // selectBS
            if ($groupName !== '') {

                $previousGroupName = $fe->itemGroupValues[$ii - 1] ?? null;

                // group name or separator should be rendered
                if (is_null($previousGroupName) || $previousGroupName !== $groupName) {

                    // separator
                    if (substr_count($groupName, '|') !== 0 && !is_null($previousGroupName)) {
                        $class = 'class="divider qfq-item-disabled"';
                        $role = 'role="separator"';
                        $option .= "<li {$class} {$style} {$role}></li>";

                        // group name
                    } else if (substr_count($groupName, '|') === 0) {
                        $groupLevel = 0;
                        $style = 'style="padding-left:' . $groupLevel * 5 + 7 . 'px;"';
                        $class = 'class="qfq-item-disabled"';

                        // group name contains multiple parts
                        if (substr_count($groupName, '.') > 0) {
                            $groupNameArr = explode('.', $groupName);

                            // iterate through all parts of group name
                            foreach ($groupNameArr as $groupNamePart) {
                                $previousGroupNameArr = explode('.', $previousGroupName);

                                // group name part has not been used yet
                                if (!in_array($groupNamePart, $previousGroupNameArr)) {
                                    $style = 'style="padding-left:' . $groupLevel * 5 + 7 . 'px;"';
                                    $option .= "<li {$class} {$style}>{$groupNamePart}</li>";

                                    // group name part has already been used
                                } else {

                                    // get previous level based on index of array
                                    $groupLevel = array_search($groupNamePart, $previousGroupNameArr);
                                }

                                $groupLevel++;
                            }

                            // group name does not contain multiple parts
                        } else {
                            $option .= "<li {$class} {$style}>{$groupName}</li>";
                        }

                        $groupLevel++;
                    }
                }

                $style = 'style="padding-left:' . $groupLevel * 5 + 7 . 'px;"';
            }

            $option .= "<{$dropdownElement} {$style}";
            $option .= Support::doAttribute('value', trim(strip_tags($fe->itemKeys[$ii])), false);

            if ($ii == 0 && $fe->attributes[FE_PLACEHOLDER] != '') {
                $option .= 'disabled hidden ';
            }

            $jsonValues[] = [
                HTML_ATTR_VALUE => $fe->itemKeys[$ii],
                HTML_ATTR_TEXT => $fe->itemValues[$ii],
                HTML_ATTR_SELECTED => ($fe->itemKeys[$ii] == $fe->value && $firstSelect),
            ];

            if (trim(strip_tags($fe->itemKeys[$ii])) == $fe->value && $firstSelect) {
                $option .= ($selectBS) ? 'data-selected class="qfq-item-focused"' : 'selected';
                $firstSelect = false;

                // selectBS
                $firstSelectText = $fe->itemValues[$ii] . ' ' . $firstSelectText;
                $firstSelectValue = trim(strip_tags($fe->itemKeys[$ii]));
            }

            $option .= '>' . $fe->itemValues[$ii] . "</{$dropdownElement}>";
        }

        //$wrapSetupClass = $this->wrap[WRAP_SETUP_ELEMENT][WRAP_SETUP_CLASS] ?? '';
        //$json = $this->getFormElementForJson($htmlFormElementName, $jsonValues, $formElement, $wrapSetupClass);

        $fe->attributes = HelperFormElement::prepareExtraButton($fe->attributes, false);
        if (isset($fe->attributes[HTML_ATTR_DATALIST])) {
            if ($fe->attributes[FE_DYNAMIC_UPDATE] === 'yes') {
                throw new \UserFormException("Datalist and dynamic update are not compatible", ERROR_NOT_IMPLEMENTED);
            }
            $fe->htmlAttributes[HTML_ATTR_LIST] = $fe->attributes[FE_HTML_ID] . '-datalist';
            $html = '<input ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '><datalist '
                . Support::doAttribute(HTML_ATTR_ID, $fe->htmlAttributes[HTML_ATTR_LIST]) . '>' . $option . '</datalist>';
        } else {
            // selectBS
            if ($selectBS) {
                $html = '<div class="qfq-select-bs-parent">
                         <input type="hidden" value="' . $firstSelectValue . '" ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>
                         <button type="button" class="form-control btn btn-default qfq-select-bs-button" id="' . $fe->htmlAttributes['id'] . '-b">'
                    . $firstSelectText .
                    '</button>
                         <ul class="dropdown-menu qfq-select-bs-list">' . $option . '</ul></div>';
            } else {
                $html = '<select class="form-control" ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>' . $option . '</select>';
            }
        }

        $html = $html . HelperFormElement::getHelpBlock() . $fe->attributes[FE_TMP_EXTRA_BUTTON_HTML];
        return $html . $fe->attributes[FE_INPUT_EXTRA_BUTTON_INFO];

    }
}