<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

class Bootstrap3PillRenderer extends \IMATHUZH\Qfq\Core\Renderer\FormElement\Base\PillRenderer {

    /**
     * Renders the pill container and all its nested formelements (children).
     * Note that the Navigation (Button to show this pill) is rendered seperately in the method renderFormHead() of BaseRenderer or its derived classes.
     *
     * @param AbstractFormElement $fe
     * @return string
     */
    public function renderHtml(AbstractFormElement $fe): string {
        $childrenHtml = $this->renderChildren($fe);
        $pillHtmlId = $fe->htmlAttributes[HTML_ATTR_ID];

        $class = "";
        if ($fe->attributes[FE_MODE] == FE_MODE_HIDDEN) {
            $class = ' hidden';
        } else {
            $class = $fe->isFirstPill ? ' active' : '';
        }

        return <<<EOL
            <div id="$pillHtmlId" class="tab-pane$class" role="tabpanel">
                <div class="col-md-12 qfq-form-body qfq-color-grey-2">
                    $childrenHtml
                </div>
            </div>
            EOL;
    }

    /**
     * Render the pill as an array that is later turned into JSON.
     * Dynamic Update and FORM_SAVE expect this exact response to update pills.
     *
     * @param AbstractFormElement $fe
     * @return array
     * @throws \UserFormException\
     */
    public function renderJson(AbstractFormElement $fe): array {
        $jsonArray = array();
        $htmlId = $fe->attributes[FE_HTML_ID];
        $htmlIdLi = $htmlId . HTML_ID_EXTENSION_PILL_LI;
        $htmlIdLiA = $htmlId . HTML_ID_EXTENSION_PILL_LI_A;
        $htmlName = $fe->htmlAttributes[HTML_ATTR_NAME];
        $htmlName = $fe->attributes[FE_HTML_ID];

        switch ($fe->attributes[FE_MODE]) {
            case FE_MODE_SHOW:
            case FE_MODE_REQUIRED:
                $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLi][API_ELEMENT_ATTRIBUTE][HTML_ATTR_CLASS] = '';
                $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLiA][API_ELEMENT_ATTRIBUTE][HTML_ATTR_CLASS] = '';
                break;
            case FE_MODE_READONLY:
                $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLi][API_ELEMENT_ATTRIBUTE][HTML_ATTR_CLASS] = HTML_ATTR_DISABLED;
                $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLiA][API_ELEMENT_ATTRIBUTE][HTML_ATTR_CLASS] = HTML_ATTR_NOCLICK;
                break;
            case FE_MODE_HIDDEN:
                $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLi][API_ELEMENT_ATTRIBUTE][HTML_ATTR_CLASS] = HTML_ATTR_HIDDEN;
                break;
            default:
                throw new \UserFormException("Unknown Mode: " . $fe->attributes[FE_MODE], ERROR_UNKNOWN_MODE);
        }
        $jsonArray[$htmlName][API_ELEMENT_UPDATE][$htmlIdLiA][API_ELEMENT_CONTENT] = $fe->attributes[FE_LABEL];
        return $jsonArray;
    }
}