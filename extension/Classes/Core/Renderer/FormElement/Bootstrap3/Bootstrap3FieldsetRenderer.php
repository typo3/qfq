<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement\Bootstrap3;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Form\FormElement\FieldsetFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\FormElement\Base\FieldsetRenderer;

class Bootstrap3FieldsetRenderer extends FieldsetRenderer {

    /**
     * Renders the fieldset container and all its nested formelements (children).
     *
     * @param AbstractFormElement $fe
     * @param string $renderMode
     * @return string
     */
    public function renderHtml(AbstractFormElement $fe, string $renderMode = RENDER_MODE_HTML): string {
        // <fieldset>
        $html = '<fieldset class="' . implode(' ', $fe->cssClasses) . '" ' . Support::arrayToXMLAttributes($fe->htmlAttributes) . '>';

        if ($fe->attributes[FE_LABEL] !== '') {
            if (isset($fe->attributes[FE_FIELDSET_CSS])){
                $html .= '<legend style="' . $fe->attributes[FE_FIELDSET_CSS] . 'border-bottom: none;">' . $fe->attributes[FE_LABEL] . '</legend>';
            } else {
                $html .= '<legend>' . $fe->attributes[FE_LABEL] . '</legend>';
            }
        }

        $html .= $this->renderChildren($fe, $renderMode);
        $html .= '</fieldset>';

        return $html;
    }
}