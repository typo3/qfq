<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement;

use IMATHUZH\Qfq\Core\Form\FormElement\AbstractFormElement;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

abstract class NativeRenderer extends FormElementRenderer {

    /**
     * @var array
     * Defines the html tags that wrap label, input and note. This is initialized in $this->wrapLabelInputNote
     */
    public array $wrap = array();

    /**
     * @param AbstractFormElement $fe
     * @return string
     * @throws \CodeException
     */
    public function renderHtml(AbstractFormElement $fe): string {
        $hidden = ($fe->attributes[FE_MODE] == FE_MODE_HIDDEN) ? 'hidden' : '';

        // Setup Wrap elements for label, input and note
        $this->wrapLabelInputNote($fe->htmlAttributes[HTML_ATTR_ID], $fe->attributes[FE_BS_LABEL_COLUMNS], $fe->attributes[FE_BS_INPUT_COLUMNS], $fe->attributes[FE_BS_NOTE_COLUMNS]);
        $rowId = $fe->htmlAttributes[HTML_ATTR_ID] . "-r";

        return $fe->attributes[FE_HTML_BEFORE]
            . ($fe->attributes[FE_FLAG_ROW_OPEN_TAG] == 1 ? "<div id='$rowId' class='form-group clearfix $hidden'>" : '')
            . $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_START]
            . $this->renderLabel($fe)
            . $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_END]
            . $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_START]
            . $this->renderInput($fe)
            . $this->renderToolTip($fe)
            . $this->renderFormEditorUrl($fe)
            . $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_END]
            . $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_START]
            . $this->renderNote($fe)
            . $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_END]
            . ($fe->attributes[FE_FLAG_ROW_CLOSE_TAG] == 1 ? '</div>' : '')
            . $fe->attributes[FE_HTML_AFTER];
    }

    /**
     * @param AbstractFormElement $fe
     * @return string
     * @throws \CodeException
     */
    protected function renderLabel(AbstractFormElement $fe): string {
        if ($fe->attributes[FE_BS_LABEL_COLUMNS] == 0) return "";
        $additionalCssClasses = is_array($fe->addClassRequired) ? implode(' ', $fe->addClassRequired) : $fe->addClassRequired;
        return HelperFormElement::buildLabel($fe->htmlAttributes[HTML_ATTR_NAME], $fe->attributes[FE_LABEL], $additionalCssClasses);
    }

    /**
     * Renders only the Input element. No label, note or additional wrappers around the input is rendered.
     * This function is public, so that it can be used to create inline-edit elements.
     *
     * @param AbstractFormElement $fe
     * @return string
     */
    public abstract function renderInput(AbstractFormElement $fe): string;

    /**
     * Renders the note part of the formElement.
     *
     * @param AbstractFormElement $fe
     * @return string
     */
    protected function renderNote(AbstractFormElement $fe): string {
        if ($fe->attributes[FE_BS_NOTE_COLUMNS] == 0) return "";
        if (!empty($fe->addClassRequired[FE_NOTE])) {
            $fe->attributes[FE_NOTE] = Support::wrapTag('<span class="' . $fe->addClassRequired[FE_NOTE] . '">', $fe->attributes[FE_NOTE]);
        }
        return $fe->attributes[FE_NOTE];
    }

    /**
     * Fill the BS wrapper for Label/Input/Note.
     * For legacy reasons, $label/$input/$note might be a number (0-12) or the bs column classes ('col-md-12 col-lg-9')
     * If a Renderer is implemented which does not use the BS column system, then this functionality will have to be moved elsewhere.
     *
     * @param $htmlId
     * @param $label
     * @param $input
     * @param $note
     * @return string
     */
    protected function wrapLabelInputNote($htmlId, $label, $input, $note): string {
        $output = "";

        $label = is_numeric($label) ? ($label == 0 ? 'hidden' : 'col-md-' . $label) : $label;
        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_START] = "<div id='$htmlId-l' class='$label qfq-label'>";
        $this->wrap[WRAP_SETUP_LABEL][WRAP_SETUP_END] = "</div>";

        $input = is_numeric($input) ? ($input == 0 ? 'hidden' : 'col-md-' . $input) : $input;
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_START] = "<div id='$htmlId-i' class='$input'>";
        $this->wrap[WRAP_SETUP_INPUT][WRAP_SETUP_END] = "</div>";

        $note = is_numeric($note) ? ($note == 0 ? 'hidden' : 'col-md-' . $note) : $note;
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_START] = "<div id='$htmlId-n' class='$note qfq-note'>";
        $this->wrap[WRAP_SETUP_NOTE][WRAP_SETUP_END] = "</div>";

        return $output;
    }

    /**
     * Wrap content with $wrapArray or, if specified use $formElement[$wrapName]. Inject $htmlId in wrap.
     *
     * Result:
     * - if $bsColumns==0 and empty $formElement[$wrapName]: no wrap
     * - if $formElement[$wrapName] is given: wrap with that one. Else: wrap with $wrapArray
     * - if $htmlId is give, inject it in $wrap.
     *
     * @param array $formElement Complete FormElement, especially some FE_WRAP
     * @param string $htmlElement Content to wrap.
     * @param string $wrapName FE_WRAP_ROW, FE_WRAP_LABEL, FE_WRAP_INPUT, FE_WRAP_NOTE
     * @param int $bsColumns
     * @param array $wrapArray System wide Defaults: [ 'open wrap', 'close wrap' ]
     * @param string $htmlId
     * @param string $class
     */
    protected function customWrap(array $formElement, $htmlElement, $wrapName, $bsColumns, array $wrapArray, $htmlId = '', $class = '') {

        // If $bsColumns==0: do not wrap with default.
        if ($bsColumns == '0') {
            $wrapArray[0] = '';
            $wrapArray[1] = '';
        }

        // If there is a 'per FormElement'-wrap, take it.
        if (isset($formElement[$wrapName])) {
            $wrapArray = explode('|', $formElement[$wrapName], 2);
        }

        if (count($wrapArray) != 2) {
            throw new \UserFormException("Need open & close wrap token for FormElement.parameter" . $wrapName . " - E.g.: <div ...>|</div>", ERROR_MISSING_VALUE);
        }

        if ($wrapArray[0] != '') {
            $wrapArray[0] = Support::insertAttribute($wrapArray[0], HTML_ATTR_ID, $htmlId);
            $wrapArray[0] = Support::insertAttribute($wrapArray[0], HTML_ATTR_CLASS, $class); // might be problematic, if there is already a 'class' defined.
            if ($wrapName == FE_WRAP_LABEL) {
                $wrapArray[0] = Support::insertAttribute($wrapArray[0], HTML_ATTR_STYLE, 'text-align: ' . $formElement[F_FE_LABEL_ALIGN] . ';'); // might be problematic, if there is already a 'class' defined.
            }
            // Insert 'required="required"' for checkboxes and radios
            if ($wrapName == FE_WRAP_INPUT && $formElement[FE_MODE] == FE_MODE_REQUIRED &&
                ($formElement[FE_TYPE] == FE_TYPE_CHECKBOX || $formElement[FE_TYPE] == FE_TYPE_RADIO)) {
                $wrapArray[0] = Support::insertAttribute($wrapArray[0], HTML_ATTR_REQUIRED, HTML_ATTR_REQUIRED); // might be problematic, if there is already a 'class' defined.
            }
        }

        return $wrapArray[0] . $htmlElement . $wrapArray[1];
    }

    protected function renderToolTip($fe): string {
        $tooltip = '';
        if (strlen($fe->attributes[FE_TOOLTIP]) > 0) {
            $tooltip = Support::doTooltip($fe->htmlAttributes['id'] . '-t', $fe->attributes[FE_TOOLTIP]);
        }
        return $tooltip;
    }

}