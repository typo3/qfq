<?php

namespace IMATHUZH\Qfq\Core\Renderer\FormElement;

use IMATHUZH\Qfq\Core\Form\FormElement\ContainerFormElement;
use IMATHUZH\Qfq\Core\Renderer\BaseRenderer;

abstract class ContainerRenderer extends FormElementRenderer {

    /**
     * Render all Children of this container.
     *
     * @param ContainerFormElement $containerFormElement
     * @return string
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function renderChildren(ContainerFormElement $containerFormElement): string {
        $output = "";
        $renderer = BaseRenderer::getInstance();

        foreach ($containerFormElement->formElements as $childFormElement) {
            $output .= $renderer->renderFormElementHtml($childFormElement);
        }

        $output .= $this->renderFormEditorUrl($containerFormElement);

        return $output;
    }
}