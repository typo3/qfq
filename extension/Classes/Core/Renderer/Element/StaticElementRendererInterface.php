<?php

namespace IMATHUZH\Qfq\Core\Renderer\Element;

interface StaticElementRendererInterface {
    /**
     * Renders an element in the requested mode.
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public static function renderStatic(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string;

    /**
     * Throws a RendererException when not all required arguments are passed
     * to the render() method.
     *
     * @param string $rendererName
     * @param string $argName
     * @return void
     */
    public static function throwMissingArgument(string $rendererName, string $argName): void;
}
