<?php

namespace IMATHUZH\Qfq\Core\Renderer\Element;

use Symfony\Polyfill\Intl\Icu\Exception\NotImplementedException;

class SpinnerRenderer implements ElementRendererInterface, StaticElementRendererInterface {
    use RendererExceptionThrowerTrait;

    /**
     * Renders a spinner
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public function render(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        return self::renderStatic($elementSpec, $renderMode);
    }

    /**
     * Renders a spinner
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public static function renderStatic(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        throw new NotImplementedException("ButtonRenderer::renderStatic is not implemented. Use Bootstrap3ButtonRenderer instead.");
    }
}