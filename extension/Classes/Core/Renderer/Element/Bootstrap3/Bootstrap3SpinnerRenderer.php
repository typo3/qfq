<?php

namespace IMATHUZH\Qfq\Core\Renderer\Element\Bootstrap3;

use IMATHUZH\Qfq\Core\Renderer\Element\SpinnerRenderer;
use Symfony\Polyfill\Intl\Icu\Exception\NotImplementedException;

class Bootstrap3SpinnerRenderer extends SpinnerRenderer {
    /**
     * Renders a BS3 spinner
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public function render(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        return self::render($elementSpec, $renderMode);
    }

    /**
     * Renders a BS3 Spinner
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public static function renderStatic(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        throw new NotImplementedException();
    }
}