<?php

namespace IMATHUZH\Qfq\Core\Renderer\Element\Bootstrap3;

use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Renderer\Element\ButtonRenderer;

class Bootstrap3ButtonRenderer extends ButtonRenderer {

    /**
     * Generic function to create a button based on a spec array
     *
     * @param array $elementSpec ['url'] -> link opened onclick of the button
     *                              ['buttonHtmlId'] -> HTML id of the button element
     *                              [BTN_TEXT] -> HTML id of the button element
     *                              ['tooltip'] -> HTML id of the button element
     *                              [BTN_ICON] -> name of the icon, e.g. "glyphicon-pencil"
     *                              ['disabled'] -> button disabled if this is set (no specific value needed)
     *                              ['class'] -> css class of the button
     *                              ['onChangeClass'] -> css class that is assigned to the button when form input changes
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     */
    public function render(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        return self::renderStatic($elementSpec, $renderMode);
    }

    /**
     * Renders a button
     *
     * @param array $elementSpec
     * @param string $renderMode
     * @return string
     * @throws \CodeException
     */
    public static function renderStatic(array $elementSpec, string $renderMode = RENDER_MODE_HTML): string {
        $innerHTML = "";
        if (!isset($elementSpec[BTN_TEXT])) $elementSpec[BTN_TEXT] = "";
        if (!isset($elementSpec[BTN_ICON]) || $elementSpec[BTN_ICON] === "") {
            $innerHTML = $elementSpec[BTN_TEXT];
        } else {
            $innerHTML = "<span " . Support::doAttribute(BTN_CLASS, GLYPH_ICON. ' ' . $elementSpec[BTN_ICON]) . "></span> " . $elementSpec[BTN_TEXT];
        }

        $attribute = "";
        if (isset($elementSpec[BTN_URL])) $attribute .= Support::doAttribute(HTML_ATTR_HREF, $elementSpec[BTN_URL]);
        if (isset($elementSpec[BTN_HTML_ID])) $attribute .= Support::doAttribute(BTN_ID, $elementSpec[BTN_HTML_ID]);
        if (isset($elementSpec[BTN_CLASS])) $attribute .= Support::doAttribute(BTN_CLASS, $elementSpec[BTN_CLASS]);
        if (isset($elementSpec[BTN_TOOLTIP])) $attribute .= Support::doAttribute(BTN_TITLE, $elementSpec[BTN_TOOLTIP]);
        if (isset($elementSpec[BTN_ONCHANGE_CLASS])) $attribute .= Support::doAttribute(DATA_CLASS_ON_CHANGE, $elementSpec[BTN_ONCHANGE_CLASS]);
        if (isset($elementSpec[BTN_ONCLICK])) $attribute .= Support::doAttribute(BTN_ONCLICK, $elementSpec[BTN_ONCLICK]);
        if (isset($elementSpec[BTN_SIP])) $attribute .= Support::doAttribute(DATA_SIP, $elementSpec[BTN_SIP]);
        if (isset($elementSpec[SYSTEM_BASE_URL])) $attribute .= Support::doAttribute(DATA_BASE_URL, $elementSpec[SYSTEM_BASE_URL]);
        if (isset($elementSpec[BTN_TABLESORTER_VIEW])) $attribute .= Support::doAttribute(DATA_TABLESORTER_VIEW_JSON, $elementSpec[BTN_TABLESORTER_VIEW]);
        if (isset($elementSpec[BTN_HISTORY_TITLE])) $attribute .= Support::doAttribute(DATA_HISTORY_TITLE, $elementSpec[BTN_HISTORY_TITLE]);
        if (isset($elementSpec[BTN_DISABLED])) $attribute .= " disabled ";
        if (isset($elementSpec[CLASS_IGNORE_HISTORY])) $attribute .= " ".CLASS_IGNORE_HISTORY." ";

        // disabled links do not show tooltips -> use a span
        $wrapTag = 'button';
        if (isset($elementSpec[BTN_DISABLED]) && $elementSpec[BTN_DISABLED] != '') $wrapTag = 'span';
        if (isset($elementSpec[BTN_URL])) $wrapTag = 'a';
        if ($wrapTag == 'button') $attribute .= Support::doAttribute(HTML_ATTR_TYPE, BTN_TYPE_BUTTON);

        return Support::wrapTag("<$wrapTag $attribute>", $innerHTML);
    }

}