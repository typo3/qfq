<?php

namespace IMATHUZH\Qfq\Core\Renderer\Element;

use IMATHUZH\Qfq\Core\Exception\RendererException;

trait RendererExceptionThrowerTrait {
    /**
     * @param string $rendererClass
     * @param string $argName
     * @return void
     * @throws RendererException
     */
    public static function throwMissingArgument(string $rendererClass, string $argName): void {
        throw new RendererException(
            "$rendererClass::render() misses a required argument $argName",
            ERROR_RENDERER_MISSING_ARGUMENT
        );
    }
}
