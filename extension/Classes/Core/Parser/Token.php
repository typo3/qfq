<?php
/**
 * @package qfq
 * @author kputyr
 * @date: 09.11.2023
 */

namespace IMATHUZH\Qfq\Core\Parser;

/**
 * A token returned by StringTokenizer when parsing a string.
 * It represents a part of the input string except that unescaped quotes
 * and backslashes before delimiters and quotes are removed.
 * @package qfq
 */
class Token {
    /**
     * @var string The string wrapped by the token.
     */
    public string $value;

    /**
     * @var bool True if the token must be treated as a string
     *           (for instance it was surrounded with quotes)
     */
    public bool $isString;

    public function __construct(string $value, bool $isString) {
        $this->value = $value;
        $this->isString = $isString;
    }

    /**
     * Returns true when the token is empty. In particular, this means
     * that there were no quotes in the token.
     * @return bool
     */
    public function empty(): bool {
        return $this->value === '' && !$this->isString;
    }

    public function __toString(): string {
        return $this->value;
    }
}
