<?php

namespace IMATHUZH\Qfq\Core\Form\FormElement;

use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Helper\Support;

class NoteFormElement extends AbstractFormElement {

    /**
     * @param $attributes
     * @param Form|null $form
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($attributes, ?Form $form = null) {
        parent::__construct($attributes, $form);

        if (isset($this->attributes[FE_DATA_REFERENCE])) {
            $this->htmlAttributes[FE_DATA_REFERENCE] = $this->attributes[FE_DATA_REFERENCE];
        }
    }

    /**
     * Note FEs are allowed to have no name and label, because they are readonly anyway.
     *
     * @return bool
     */
    protected function allowEmptyNameAndLabel(): bool {
        return true;
    }

}