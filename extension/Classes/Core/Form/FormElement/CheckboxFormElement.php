<?php

namespace IMATHUZH\Qfq\Core\Form\FormElement;

use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Support;

class CheckboxFormElement extends AbstractFormElement {
    public array $itemKeys = array();
    public array $itemValues = array();

    /**
     * @param $attributes
     * @param Form|null $form
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($attributes, ?Form $form = null) {
        parent::__construct($attributes, $form);

        $this->setKeysAndValues();
        $this->setDefaults();
    }

    /**
     * Fills $this->itemKeys and $this->itemValues
     *
     * @return void
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function setKeysAndValues(): void {
        HelperFormElement::getKeyValueListFromSqlEnumSpec($this->attributes, $this->itemKeys, $this->itemValues);
    }

    /**
     * Set meaningful default values
     *
     * @return void
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    private function setDefaults(): void {
        $this->htmlAttributes['type'] = 'checkbox';
        $this->attributes[FE_CHECKBOX_MODE] = $this->getCheckBoxMode();

        // Checked
        if ($this->attributes['checkBoxMode'] != 'multi') {

            if (!isset($this->attributes[FE_CHECKBOX_CHECKED])) {
                if (isset($this->itemKeys[0])) {
                    // First element in $itemKey list
                    $this->attributes[FE_CHECKBOX_CHECKED] = $this->itemKeys[0];
                } else {
                    // Take column default value
                    $this->attributes[FE_CHECKBOX_CHECKED] = $this->store->getVar($this->attributes[FE_NAME], STORE_TABLE_DEFAULT);
                }
            }

            // Unchecked
            if (!isset($this->attributes[FE_CHECKBOX_UNCHECKED])) {
                if (isset($this->itemKeys[1])) {
                    $this->attributes[FE_CHECKBOX_UNCHECKED] = ($this->itemKeys[0] === $this->attributes[FE_CHECKBOX_CHECKED]) ? $this->itemKeys[1] : $this->itemKeys[0];
                } else {
                    $this->attributes[FE_CHECKBOX_UNCHECKED] = '';
                }
            }

            if ($this->attributes[FE_CHECKBOX_CHECKED] === $this->attributes[FE_CHECKBOX_UNCHECKED]) {
                throw new \UserFormException('FormElement: type=checkbox - checked and unchecked can\'t be the same: ' . $this->attributes[FE_CHECKBOX_CHECKED], ERROR_CHECKBOX_EQUAL);
            }
        }

        // Required Error
        if (isset($this->attributes[F_FE_DATA_REQUIRED_ERROR])) {
            $this->htmlAttributes[F_FE_DATA_REQUIRED_ERROR] = $this->attributes[F_FE_DATA_REQUIRED_ERROR];
        }

    }

    /** Get the state of the checkbox.
     * @return string
     */
    private function getCheckBoxMode(): string {
        // Checkbox mode is set
        if (isset($this->attributes[FE_CHECKBOX_MODE])) {
            return $this->attributes[FE_CHECKBOX_MODE];
        }
        // more than 2 items expected 'multi' checkbox
        if (count($this->itemKeys) > 2) {
            return FE_CHECKBOX_MODE_MULTI;
        }
        // default single checkbox
        return FE_CHECKBOX_MODE_SINGLE;

    }
}