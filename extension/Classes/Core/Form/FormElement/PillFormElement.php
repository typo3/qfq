<?php

namespace IMATHUZH\Qfq\Core\Form\FormElement;

use http\Encoding\Stream\Deflate;
use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Helper\Logger;


class PillFormElement extends ContainerFormElement {
    public bool $isFirstPill = false;

    /**
     * @param $attributes
     * @param Form|null $form
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     * @var array $formElements Child Elements of this Container.
     *
     */
    public function __construct($attributes, ?Form $form = null) {
        parent::__construct($attributes, $form);
        $hrefName = $form == null ? 'noForm' : $form->specFinal[F_NAME];
        $this->htmlAttributes['href'] = '#' . $hrefName . '_' . $this->attributes[FE_ID]; //. '_' . $this->form->recordId;
        $this->htmlAttributes[HTML_ATTR_ID] = $hrefName . "_" . $this->attributes[FE_ID];
    }

    /**
     * Pill FE's need both name and label set!
     *
     * @return void
     * @throws \CodeException
     * @throws \UserFormException
     */
    protected function handleEmptyNameOrLabel(): void {
        if ($this->attributes[FE_NAME] === '' || $this->attributes[FE_LABEL] === '') {
            $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($this->attributes), STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, 'name, label', STORE_SYSTEM);
            throw new \UserFormException("Field 'name' and 'label' are empty", ERROR_NAME_LABEL_EMPTY);
        }
    }
}