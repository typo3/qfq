<?php

namespace IMATHUZH\Qfq\Core\Form\FormElement;

use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\Support;

class DatetimeFormElement extends AbstractFormElement {
    public ?array $format;

    /**
     * @param $attributes
     * @param Form|null $form
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($attributes, ?Form $form = null) {
        parent::__construct($attributes, $form);

        $this->handleCssClasses();
        $this->handleCheckType();
        $this->format = $this->getDefaultDateFormat();
        $this->handleFormat();
        $this->handleValue();
        $this->handleDaysOfWeekEnabled();
        $this->handleClearMe();
        $this->handlePlaceholder();
        $this->handlePattern();
        $this->handleMinMax();
        $this->handleHtmlAttributes();
    }


    /**
     * Adds required css classes to $this->cssClasses
     * Classes depend on the type of datepicker and on form multi/non-multi
     *
     * @return void
     */
    private function handleCssClasses(): void {
        array_push($this->cssClasses, 'form-control');
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            array_push($this->cssClasses, 'qfq-datepicker');
        } elseif (isset($this->attributes[FE_INPUT_CLEAR_ME]) && $this->attributes[FE_INPUT_CLEAR_ME] != '0' && $this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_NO) {
            if (($this->form->specFinal[F_MULTI_MODE] ?? '') == 'vertical') {
                array_push($this->cssClasses, 'qfq-clear-me-multiform');
            } else {
                array_push($this->cssClasses, 'qfq-clear-me');
            }
        }
    }


    /**
     * @return array Returns an array that describes the default format for the used type (datetime / date / time)
     */
    private function getDefaultDateFormat(): array {
        $defaultDateFormat = explode(' ', $this->attributes[FE_DATE_FORMAT], 2);
        $defaultFormat = array();
        if (isset($defaultDateFormat[1])) {
            $defaultFormat['time'] = $defaultDateFormat[1];
            $defaultFormat['date'] = $defaultDateFormat[0];
            $defaultFormat['timeParts'] = explode(':', $defaultFormat['time'], 3);
        } else {
            $defaultFormat['date'] = $this->attributes[FE_DATE_FORMAT];
            $defaultFormat['timeParts'] = explode(':', $this->attributes[FE_DATE_FORMAT], 3);
        }

        if ($defaultFormat['timeParts'][2] ?? '' === 'ss') {
            $this->attributes[FE_SHOW_SECONDS] = 1;
        }
        return $defaultFormat;
    }


    /**
     * Modifies and returns the default format for a QFQ timepicker
     *
     * @return void
     */
    private function useDateTimeFormatQfq(): void {
        switch ($this->format['date']) {
            case FORMAT_DATE_INTERNATIONAL:
            case FORMAT_DATE_INTERNATIONAL_QFQ:
            case FORMAT_DATE_GERMAN:
            case FORMAT_DATE_GERMAN_QFQ:
                if ($this->attributes[FE_TYPE] == FE_TYPE_DATETIME) {
                    $this->format['date'] = strtoupper($this->format['date']);
                    if (empty($this->format['time'])) {
                        $this->format['time'] = 'HH:mm';
                    }
                    $this->format['date'] .= ' ' . $this->format['time'];
                } else {
                    $this->format['date'] = strtoupper($this->format['date']);
                }
                break;
        }
    }


    /**
     * Formats the time part of $this->format to work with different configurations
     *
     * @return void
     */
    private function setTimeFormat(): void {
        if ($this->format['timeParts'][0] === 'HH' || $this->format['timeParts'][0] === 'hh') {
            $this->format['date'] = $this->format['timeParts'][0] . ':' . $this->format['timeParts'][1];
        } else if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $this->format['date'] = 'HH:mm';
        } else {
            $this->format['date'] = 'hh:mm';
        }

        if ($this->attributes[FE_SHOW_SECONDS] == 1) {
            $this->format['date'] .= ':ss';
        }
    }


    /**
     * @return void
     */
    private function formatValueForBrowser(): void {
        $dateOldFormat = date_create($this->value);
        if ($this->attributes[FE_TYPE] === FE_TYPE_DATE) {
            $this->value = date_format($dateOldFormat, "Y-m-d");
        } elseif ($this->attributes[FE_TYPE] === FE_TYPE_DATETIME) {
            $this->value = date_format($dateOldFormat, "Y-m-d H:i");
        }
    }


    /**
     * Returns datetime browser type
     *
     * @return string
     */
    private function getDateTimeBrowserType(): string {
        if ($this->attributes[FE_TYPE] == FE_TYPE_DATE) return 'date';
        if ($this->attributes[FE_TYPE] == FE_TYPE_TIME) return 'time';
        return 'datetime-local';
    }


    /**
     * @return bool|string
     */
    private function getDateTimePickerDisabledDays(): bool|string {
        $enabledDays = $this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED];
        // convert enabled days from datetimepicker user input daysOfWeekEnabled to disabled days
        $enabledDays = explode(',', $enabledDays);
        $disabledDays = '';

        for ($i = 0; $i <= 6; $i++) {
            $flagDayPoint = false;
            foreach ($enabledDays as $day) {
                if ($day == $i) {
                    $flagDayPoint = true;
                }
            }
            if (!$flagDayPoint) {
                $disabledDays .= $i . ',';
            }
        }

        // if last char ',' exists, remove it
        $lastChar = substr($disabledDays, -1);
        if ($lastChar == ',') {
            $disabledDays = substr($disabledDays, 0, -1);
        }
        return $disabledDays;
    }


    /**
     * Fill $this->htmlAttributes
     *
     * @return void
     */
    private function handleHtmlAttributes(): void {
        Support::setIfNotSet($this->attributes, F_FE_DATA_MATCH_ERROR);
        Support::setIfNotSet($this->attributes, F_FE_DATA_ERROR);

        // Set some normal htmlAttributes
        $this->htmlAttributes[ATTRIBUTE_DATA_REFERENCE] = $this->attributes[FE_DATA_REFERENCE];
        $this->htmlAttributes[FE_SIZE] = $this->attributes[FE_SIZE];
        $this->htmlAttributes[FE_MAX_LENGTH] = $this->attributes[FE_MAX_LENGTH];
        $this->htmlAttributes[FE_VALUE] = htmlentities($this->value);
        $this->htmlAttributes[F_FE_DATA_PATTERN_ERROR] = $this->attributes[F_FE_DATA_PATTERN_ERROR];
        $this->htmlAttributes[F_FE_DATA_REQUIRED_ERROR] = $this->attributes[F_FE_DATA_REQUIRED_ERROR];
        $this->htmlAttributes[F_FE_DATA_MATCH_ERROR] = $this->attributes[F_FE_DATA_MATCH_ERROR];
        $this->htmlAttributes[F_FE_DATA_ERROR] = $this->attributes[F_FE_DATA_ERROR];
        $this->htmlAttributes[FE_INPUT_AUTOCOMPLETE] = $this->attributes[FE_INPUT_AUTOCOMPLETE] ?? '';
        $this->htmlAttributes[FE_AUTOFOCUS] = $this->attributes[FE_AUTOFOCUS] ?? '';
        $this->htmlAttributes[FE_PLACEHOLDER] = $this->attributes[FE_PLACEHOLDER];
        if ($this->attributes[FE_DYNAMIC_UPDATE] === 'yes') $this->htmlAttributes[FE_DATA_LOAD] = FE_DATA_LOAD;
        $this->htmlAttributes['title'] = $this->attributes[FE_TOOLTIP];
        $this->htmlAttributes[FE_MIN] = $this->attributes[FE_MIN];
        $this->htmlAttributes[FE_MAX] = $this->attributes[FE_MAX];
        $this->htmlAttributes[FE_CHECK_PATTERN] = $this->attributes[FE_CHECK_PATTERN];


        // Set input type
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_BROWSER) {
            $type = $this->getDateTimeBrowserType();
        } else {
            // date|time|datetime|datetime-local are not appropriate - only I18n representation possible.
            $type = 'text';
        }
        $this->htmlAttributes['type'] = $type;


        // Set 'data-..' htmlAttributes
        $dateTimeKeys = array(
            FE_DATE_LOCALE,
            FE_DATE_DAYS_OF_WEEK_ENABLED,
            FE_DATE_FORMAT,
            FE_DATE_VIEWMODE_DEFAULT,
            FE_INPUT_CLEAR_ME,
            FE_DATE_SHOW_CALENDAR_WEEKS,
            FE_DATE_CURRENT_DATETIME,
            FE_DATE_DATETIME_SIDE_SIDE_BY_SIDE,
            FE_MIN,
            FE_MAX
        );

        $dateTimeAttributes = array(
            "locale",
            "days-of-week-disabled",
            "format",
            "view-mode-default",
            "show-clear-button",
            "show-calendar-weeks",
            "use-current-datetime",
            "datetime-side-by-side",
            "minDate",
            "maxDate"
        );

        $keyCount = 0;
        foreach ($dateTimeKeys as $key) {
            if (isset($this->attributes[$key]) && $this->attributes[$key] != "" && $key != FE_DATE_FORMAT) {
                $this->htmlAttributes['data-' . $dateTimeAttributes[$keyCount]] = $this->attributes[$key];
            } elseif ($key == FE_DATE_FORMAT) {
                $this->htmlAttributes['data-' . $dateTimeAttributes[$keyCount]] = $this->format['date'];
            }
            $keyCount++;
        }
    }


    /**
     * Set FE_MIN and FE_MAX for a date/time FE that is not of type QFQ
     *
     * @return void
     */
    private function handleMinMax(): void {
        // For other dateTimePicker than qfq, min/max values need to be converted
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) return;

        if ($this->attributes[FE_TYPE] == FE_TYPE_DATE) {
            $dateMinMaxFormat = "Y-m-d";
        } else {
            $dateMinMaxFormat = "Y-m-d H:i";
        }

        if (isset($this->attributes[FE_MIN]) && $this->attributes[FE_MIN] !== '') {
            $dateMin = date_create($this->attributes[FE_MIN]);
            $this->attributes[FE_MIN] = date_format($dateMin, $dateMinMaxFormat);
        }

        if (isset($this->attributes[FE_MAX]) && $this->attributes[FE_MAX] !== '') {
            $dateMax = date_create($this->attributes[FE_MAX]);
            $this->attributes[FE_MAX] = date_format($dateMax, $dateMinMaxFormat);
        }
    }


    /**
     * Sets / Sanitizes $this->attributes[FE_CHECK_TYPE] and $this->attributes[FE_CHECK_PATTERN]
     *
     * @return void
     * @throws \UserFormException
     */
    private function handleCheckType(): void {
        $tmpPattern = $this->attributes[FE_CHECK_PATTERN];
        //  If bootstrap datetimepicker (date and datetime FE Type) is used, check pattern is not needed. Keep pattern for FE Type time.
        // pgroeb: This does not make sense: type should be datetime, date or time, but these are all excluded in the following IF
        if ($this->attributes[FE_TYPE] != FE_TYPE_DATETIME && $this->attributes[FE_TYPE] != FE_TYPE_DATE && $this->attributes[FE_TYPE] != FE_TYPE_TIME) {
            $this->attributes[FE_CHECK_PATTERN] = Support::dateTimeRegexp($this->attributes[FE_TYPE], $this->attributes[FE_DATE_FORMAT], $this->attributes[FE_TIME_IS_OPTIONAL]);
        }

        switch ($this->attributes[FE_CHECK_TYPE]) {
            case  SANITIZE_ALLOW_PATTERN:
                $this->attributes[FE_CHECK_PATTERN] = $tmpPattern;
                break;
            case SANITIZE_ALLOW_ALL:
            case SANITIZE_ALLOW_ALNUMX:
            case SANITIZE_ALLOW_ALLBUT:
                $this->attributes[FE_CHECK_TYPE] = SANITIZE_ALLOW_PATTERN;
                break;
            default:
                throw new \UserFormException("Checktype not applicable for date/time: '" . $this->attributes[FE_CHECK_TYPE] . "'", ERROR_NOT_APPLICABLE);
        }
    }


    /**
     * Sets $this->value to work with the current configuration of the FE
     *
     * @return void
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function handleValue(): void {
        $showTime = ($this->attributes[FE_TYPE] == FE_TYPE_TIME || $this->attributes[FE_TYPE] == FE_TYPE_DATETIME) ? 1 : 0;
        if ($this->value == 'CURRENT_TIMESTAMP' || $this->value == 'current_timestamp()') {
            $this->value = date('Y-m-d H:i:s');
        }

        $this->value = Support::convertDateTime($this->value, $this->attributes[FE_DATE_FORMAT], $this->attributes[FE_SHOW_ZERO], $showTime, $this->attributes[FE_SHOW_SECONDS]);

        // Browser needs own formatted value to show date
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_BROWSER && $this->value !== '') {
            $this->formatValueForBrowser();
        }

        $tableColumnTypes = $this->store->getStore(STORE_TABLE_COLUMN_TYPES);
        // truncate if necessary
        if ($this->value != '' && $this->attributes[FE_MAX_LENGTH] > 0) {
            if (!is_array($this->value) && $this->attributes[FE_TYPE] === FE_TYPE_TIME && $tableColumnTypes[$this->attributes[FE_NAME]] === DB_COLUMN_TYPE_DATETIME) {
                $valueArr = explode(' ', $this->value);
                if (count($valueArr) > 1) {
                    $this->value = $valueArr[1];
                } else {
                    $this->value = $valueArr[0];
                }
            } else {
                $this->value = substr($this->value, 0, $this->attributes[FE_MAX_LENGTH]);
            }
        }
    }

    /**
     * Set $this->format according to FE type and configuration
     *
     * @return void
     */
    private function handleFormat(): void {
        //Examples of accepted dateFormats: YYYY-MM-DD , DD.MM.YYYY, HH:mm(24h), hh:mm(12h AM PM)
        // Get dateformat default from T3 config and adjust it for datetimepicker because config default (dd.mm.yyyy) is not the default of bootstrap datetimepicker.
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $this->useDateTimeFormatQfq();
        } else if ($this->attributes[FE_TYPE] == FE_TYPE_DATETIME) {
            if ($this->format['time'] === '') {
                $this->format['time'] = 'hh:mm';
            }
            $this->format['date'] .= ' ' . $this->format['time'];
        }

        // if FE type datetime and showSeconds is set, corrected format is needed
        if ($this->attributes[FE_TYPE] === FE_TYPE_DATETIME && $this->attributes[FE_SHOW_SECONDS] == 1 && !isset($this->format['timeParts'][2])) {
            $this->format['date'] .= ':ss';
        }

        // if FE type 'time' is used, overwrite $this->format['date']
        if ($this->attributes[FE_TYPE] === FE_TYPE_TIME) {
            $this->setTimeFormat();
        }
    }

    /**
     * Sets / Sanitizes $this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED]
     *
     * @return void
     */
    private function handleDaysOfWeekEnabled(): void {
        $this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED] = $this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED] ?? '';

        // Set correct parameter value for daysOfWeekDisabled attribute in FE
        if ($this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED] != '' && isset($this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED])) {
            $disabledDays = $this->getDateTimePickerDisabledDays();
            $this->attributes[FE_DATE_DAYS_OF_WEEK_ENABLED] = '[' . $disabledDays . ']';
        }
    }

    /**
     * Sets / Sanitizes $this->attributes[FE_INPUT_CLEAR_ME]
     *
     * @return void
     */
    private function handleClearMe(): void {
        if ($this->attributes[FE_DATE_TIME_PICKER_TYPE] === DATE_TIME_PICKER_QFQ) {
            $this->attributes[FE_INPUT_CLEAR_ME] = empty($this->attributes[FE_INPUT_CLEAR_ME]) ? 'false' : 'true';
        }
    }

    /**
     * Sets / Sanitizes $this->attributes[FE_PLACEHOLDER]
     *
     * @return void
     */
    private function handlePlaceholder(): void {
        if ($this->attributes[FE_PLACEHOLDER] == '') {
            $placeholder = $this->format['date'];
            $this->attributes[FE_PLACEHOLDER] = $placeholder;
        }
    }

    /**
     * Sets / Sanitizes $this->attributes[FE_CHECK_PATTERN] and $this->attributes[F_FE_DATA_PATTERN_ERROR]
     *
     * @return void
     * @throws \CodeException
     */
    private function handlePattern(): void {
        // HANDLE SANITIZE PATTERN
        if ($this->attributes[F_FE_DATA_PATTERN_ERROR] == '') {
            $this->attributes[F_FE_DATA_PATTERN_ERROR] = "Please match the format: " . $this->attributes[FE_PLACEHOLDER];
        }

        $this->attributes[FE_CHECK_PATTERN] = Sanitize::getInputCheckPattern($this->attributes[FE_CHECK_TYPE], $this->attributes[FE_CHECK_PATTERN], '', $sanitizeMessage);

        // Use system message only,if no custom one is set.
        if ($this->attributes[F_FE_DATA_PATTERN_ERROR] == $this->attributes[F_FE_DATA_PATTERN_ERROR_SYSTEM]) {
            $this->attributes[F_FE_DATA_PATTERN_ERROR] = $sanitizeMessage;
        }
    }

}