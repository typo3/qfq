<?php
/**
 * Created by PhpStorm.
 * User: Pascal Roessler
 * Date: 05/22/23
 * Time: 10:45 AM
 */

namespace IMATHUZH\Qfq\Core\Form\FormElement;

use CodeException;
use DbException;
use IMATHUZH\Qfq\Core\BodytextParser;
use IMATHUZH\Qfq\Core\Database\DatabaseManager;
use IMATHUZH\Qfq\Core\Evaluate;
use IMATHUZH\Qfq\Core\Helper\HelperFormElement;
use IMATHUZH\Qfq\Core\Helper\Ldap;
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Helper\OnArray;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Report\Link;
use IMATHUZH\Qfq\Core\Report\Report;
use IMATHUZH\Qfq\Core\Store\Sip;
use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Core\Form\Form;
use IMATHUZH\Qfq\Core\Typo3\T3Handler;
use UserFormException;
use UserReportException;

/**
 * Class AbstractFormElement
 * @package qfq
 */
abstract class AbstractFormElement {

    /**
     * The unaltered array of attributes as it is passed in the constructor.
     * In a few cases it is necessary to have access to this array.
     * E.g.: Template Group child elements are instantiated using the initialAttributes of the 'template'
     */
    public ?array $initialAttributes = null;
    /**
     * Contains all attributes of the form element.
     * Starts out as the array that is passed in the constructor, but is modified during the object's lifetime
     */
    public ?array $attributes = null;
    public ?Form $form;

    /**
     * @var array contains html attributes and their values to be processed by a renderer class.
     */
    public array $htmlAttributes = array();
    /**
     * @var array contains css classes to be added to the HTML element by a Renderer class.
     * IMPORTANT: Must not contain Renderer-Specific classes (e.g. Bootstrap 3, Bootstrap 5, etc...) - they are added by their respective renderers.
     *            Only classes that will be needed independent of choice of Renderer.
     */
    public array $cssClasses = array();
    public array $addClassRequired = array();
    /**
     * @var string The final value as it is assigned to the html element.
     * Not to be confused with $this->attributes[FE_VALUE], which describes the Form Element's value field.
     */
    public string $value;
    /**
     * If the evaluated value of the FE is an array, it is stored in this field instead of $this->value
     */
    public array $valueArray = array();
    /**
     * @var string The url to the edit form of this FE
     */
    public string $formEditorUrl;
    /**
     * @var string The tooltip for the url to the edit form of this FE
     */
    public string $formEditorTooltip;

    protected ?Store $store = null;
    protected ?DatabaseManager $databaseManager = null;
    public ?Evaluate $evaluate = null;
    protected ?Sip $sip = null;
    protected ?Link $link = null;
    protected bool $showDebugInfoFlag;

    protected array $debugStack = array();
    /**
     * These params will not be evaluated for variables and sql statements on load.
     */
    protected array $skipEvaluationOnLoad = [FE_SQL_UPDATE, FE_SQL_INSERT, FE_SQL_DELETE, FE_SQL_AFTER, FE_SQL_BEFORE, FE_PARAMETER,
        FE_FILL_STORE_VAR, FE_FILE_DOWNLOAD_BUTTON, FE_TYPEAHEAD_GLUE_INSERT, FE_TYPEAHEAD_GLUE_DELETE,
        FE_TYPEAHEAD_TAG_INSERT, FE_TYPEAHEAD_INITIAL_SUGGESTION];

    /**
     * Any deriving class should call parent::__construct($attributes, $form) at the beginning of its constructor!
     * After that, the deriving class can apply specific settings/logic.
     *
     * @param array $attributes
     * @param Form|null $form
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __construct(array $attributes, ?Form $form = null) {
        $this->initialAttributes = $attributes;
        $this->attributes = $attributes;

        Store::setVar(SYSTEM_FORM_ELEMENT_ID, $attributes[FE_ID], STORE_SYSTEM); // If Exception occurs, link can be built to this FE
        Store::setVar(SYSTEM_FORM_ELEMENT, $attributes[FE_NAME], STORE_SYSTEM);
        $this->form = $form;
        $this->store = Store::getInstance();
        $this->databaseManager = DatabaseManager::getInstance();
        $dbIndexEvaluate = $this->form != null ? $this->form->specFinal[F_DB_INDEX] : $this->databaseManager->indexData;
        $this->evaluate = new Evaluate($this->store, $this->databaseManager->getDbByIndex($dbIndexEvaluate));
        $this->showDebugInfoFlag = Support::findInSet(SYSTEM_SHOW_DEBUG_INFO_YES, $this->store->getVar(SYSTEM_SHOW_DEBUG_INFO, STORE_SYSTEM));
        $this->sip = $this->store->getSipInstance();
        $this->link = new Link($this->sip, $this->databaseManager->indexData);

        HelperFormElement::explodeParameter($this->attributes, F_PARAMETER);
        $this->handleFillStoreVar();
        $this->handleFillStoreLdap();
        $this->handleSlaveId();

        $this->handleEvaluate();
        $this->attributes = Support::setFeDefaults($this->attributes, $this->form != null ? $this->form->specFinal : array());

        $this->setValue();
        $this->handleEmptyNameOrLabel();

        // Retype htmlId needs to differ from original htmlId or the eye button will toggle both at the same time
        $htmlIdAppendix = isset($this->attributes[FE_RETYPE_SOURCE_NAME]) ? '-retype' : '';

        $this->htmlAttributes[HTML_ATTR_ID] = HelperFormElement::buildFormElementId($form == null ? "" : $form->getHtmlId(), $this->attributes[FE_ID], $form != null ? $form->recordId : 0, $htmlIdAppendix);
        $this->attributes[FE_HTML_ID] = $this->htmlAttributes[HTML_ATTR_ID];
        $this->htmlAttributes[HTML_ATTR_NAME] = HelperFormElement::buildFormElementName($this->attributes, $form != null ? $form->recordId : 0);

        $this->handleFeMode();
        $this->handleLanguage();
        $this->handleFormEditorUrl();
        $this->handleDebugToolTip();
    }

    /**
     * All values in $this->attributes are parsed (= "{{...}}" statements and variables are replaced).
     * Additionally, the value and note fields are checked for report syntax and processed
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function handleEvaluate(): void {
        $this->attributes[FE_VALUE] = $this->processReportSyntax($this->attributes[FE_VALUE]);
        $this->attributes[FE_NOTE] = $this->processReportSyntax($this->attributes[FE_NOTE]);

        $this->attributes = $this->evaluate->parseArray($this->attributes, $this->skipEvaluationOnLoad, $this->debugStack);
    }


    /**
     * Throw an exception if name AND label of the FE are not set.
     * This method should be overridden by specific FE types that require both or none of the fields.
     *
     * @return void
     * @throws CodeException
     * @throws UserFormException
     */
    protected function handleEmptyNameOrLabel(): void {
        if ($this->allowEmptyNameAndLabel()) return;
        if ($this->attributes[FE_NAME] === '' && $this->attributes[FE_LABEL] === '') {
            $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($this->attributes), STORE_SYSTEM);
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, 'name, label', STORE_SYSTEM);
            throw new \UserFormException("Field 'name' and 'label' are empty", ERROR_NAME_LABEL_EMPTY);
        }
    }

    /**
     * If true then no expection will be thrown if the FE has empty name and label when loaded.
     * Is overridden by deriving class, e.g. Note FE.
     *
     * @return bool
     */
    protected function allowEmptyNameAndLabel(): bool {
        return false;
    }

    /**
     * Detects the language and adjusts the FE accordingly
     *
     * @return void
     */
    private function handleLanguage(): void {
        $parameterLanguageFieldName = $this->store->getVar(SYSTEM_PARAMETER_LANGUAGE_FIELD_NAME, STORE_SYSTEM);
        $this->attributes = HelperFormElement::setLanguage($this->attributes, $parameterLanguageFieldName);
    }

    /**
     * set various htmlAttributes for this FE, depending on $this->attributes[FE_MODE]
     *
     * @param $cssDisable
     * @return void
     * @throws CodeException
     * @throws UserFormException
     */
    private function handleFeMode($cssDisable = true): void {

        HelperFormElement::getFeMode($this->attributes[FE_MODE], $hidden, $disabled, $required);

        // Required Mode
        if ($this->attributes[FE_MODE] == FE_MODE_REQUIRED) {
            Support::addAttributeToArray($this->htmlAttributes, F_FE_DATA_REQUIRED_ERROR, $this->attributes[F_FE_DATA_REQUIRED_ERROR]);
        }
        // Data Required Error
        if (empty($this->attributes[F_FE_DATA_MATCH_ERROR])) {
            $this->attributes[F_FE_DATA_REQUIRED_ERROR] = F_FE_DATA_REQUIRED_ERROR_DEFAULT;
        }

        switch ($this->attributes[FE_MODE]) {
            case FE_MODE_HIDDEN:
            case FE_MODE_SHOW:
            case FE_MODE_SHOW_REQUIRED:
                break;
            case FE_MODE_REQUIRED:
                Support::addAttributeToArray($this->htmlAttributes, FE_MODE_REQUIRED, FE_MODE_REQUIRED);
                break;
            case FE_MODE_READONLY:
                Support::addAttributeToArray($this->htmlAttributes, ATTRIBUTE_DISABLED, ATTRIBUTE_DISABLED);
                break;
            default:
                throw new \UserFormException("Unknown mode '" . $this->attributes[FE_MODE] . "'", ERROR_UNKNOWN_MODE);
                break;
        }

        // Attributes: data-...
        Support::addAttributeToArray($this->htmlAttributes, DATA_HIDDEN, $hidden);
        if ($cssDisable) Support::addAttributeToArray($this->htmlAttributes, DATA_DISABLED, $disabled);
        Support::addAttributeToArray($this->htmlAttributes, DATA_REQUIRED, $required);

        // Set $addClassRequired -> Renderer will add classes to label/input/note if the field is required.
        if (($this->attributes[FE_MODE] == FE_MODE_REQUIRED || $this->attributes[FE_MODE] == FE_MODE_SHOW_REQUIRED) &&
            $this->attributes[FE_INDICATE_REQUIRED] == '1') {
            $this->addClassRequired = HelperFormElement::getRequiredPositionClass($this->attributes[F_FE_REQUIRED_POSITION]);
        }
    }

    /**
     * @return void
     * @throws CodeException
     * @throws UserFormException
     */
    private function handleDebugAndLog(): void {
        // Preparation for Log, Debug
        $this->store->setVar(SYSTEM_FORM_ELEMENT, Logger::formatFormElementName($this->attributes), STORE_SYSTEM);
        $this->store->setVar(SYSTEM_FORM_ELEMENT_ID, $this->attributes[FE_ID], STORE_SYSTEM);
    }

    /**
     * Sets verious required key/value pairs on $this->attributes, which are required for LDAP functionality
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     */
    private function handleFillStoreLdap(): void {
        if (isset($this->attributes[FE_FILL_STORE_LDAP]) || isset($this->attributes[FE_TYPEAHEAD_LDAP])) {
            $keyNames = [F_LDAP_SERVER, F_LDAP_BASE_DN, F_LDAP_ATTRIBUTES,
                F_LDAP_SEARCH, F_TYPEAHEAD_LDAP_SEARCH, F_TYPEAHEAD_LDAP_SEARCH_PER_TOKEN, F_TYPEAHEAD_LDAP_SEARCH_PREFETCH,
                F_TYPEAHEAD_LIMIT, F_TYPEAHEAD_MINLENGTH, F_TYPEAHEAD_LDAP_VALUE_PRINTF,
                F_TYPEAHEAD_LDAP_ID_PRINTF, F_LDAP_TIME_LIMIT, F_LDAP_USE_BIND_CREDENTIALS];
            $this->attributes = OnArray::copyArrayItemsIfNotAlreadyExist($this->form->specFinal, $this->attributes, $keyNames);
        } else {
            return; // no ldap enabled on this FE
        }

        if (isset($this->attributes[FE_FILL_STORE_LDAP])) {

            // Extract necessary elements
            $config = OnArray::getArrayItems($this->attributes, [FE_LDAP_SERVER, FE_LDAP_BASE_DN, FE_LDAP_SEARCH, FE_LDAP_ATTRIBUTES]);

            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN,
                FE_LDAP_SERVER . ',' . FE_LDAP_BASE_DN . ',' . FE_LDAP_SEARCH . ',' . FE_LDAP_ATTRIBUTES, STORE_SYSTEM);
            $config = $this->evaluate->parseArray($config);

            if ($this->attributes[FE_LDAP_USE_BIND_CREDENTIALS] == 1) {
                $config[SYSTEM_LDAP_1_RDN] = $this->store->getVar(SYSTEM_LDAP_1_RDN, STORE_SYSTEM);
                $config[SYSTEM_LDAP_1_PASSWORD] = $this->store->getVar(SYSTEM_LDAP_1_PASSWORD, STORE_SYSTEM);
            }

            $ldap = new Ldap();
            $arr = $ldap->process($config, '', MODE_LDAP_SINGLE);
            $this->store->setStore($arr, STORE_LDAP, true);
        }

    }

    /**
     * Fire FillStoreVar if given
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     */
    private function handleFillStoreVar(): void {
        if (isset($this->attributes[FE_FILL_STORE_VAR])) {
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_FILL_STORE_VAR, STORE_SYSTEM); // debug
            //$this->store[FE_FILL_STORE_VAR] = $this->evaluate->parse($this->attributes[FE_FILL_STORE_VAR], ROW_EXPECT_0_1);
            //$this->store->appendToStore($this->attributes[FE_FILL_STORE_VAR], STORE_VAR);
            $this->store->appendToStore($this->evaluate->parse($this->attributes[FE_FILL_STORE_VAR], ROW_EXPECT_0_1), STORE_VAR);
        }
    }

    /**
     * Fire FE_SLAVE_ID if given.
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     */
    private function handleSlaveId(): void {
        // If given, slaveId will be copied to STORE_VAR
        if (isset($this->attributes[FE_SLAVE_ID])) {
            $this->store->setVar(SYSTEM_FORM_ELEMENT_COLUMN, FE_SLAVE_ID, STORE_SYSTEM); // debug
            $slaveId = Support::falseEmptyToZero($this->evaluate->parse($this->attributes[FE_SLAVE_ID]));
            $this->store->setVar(VAR_SLAVE_ID, $slaveId, STORE_VAR);
        }
    }

    /**
     * Set $this->value
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     */
    private function setValue(): void {
        $storeUse = STORE_USE_DEFAULT;
        if (!is_array($this->attributes[FE_VALUE])) { // value might be an array (Template Group)
            $this->value = $this->attributes[FE_VALUE];
        } else {
            $this->value = '';
            $this->valueArray = $this->attributes[FE_VALUE];
        }

        if ($this->value === '') {
            // #2064 / Only take the default, if the FE is a real table column.
            // #3426 / Dynamic Update: Inputs loose the new content and shows the old value.
            if ($this->store->getVar($this->attributes[FE_NAME], STORE_TABLE_COLUMN_TYPES) === false) {
                $storeUse = str_replace(STORE_TABLE_DEFAULT, '', $storeUse); // Remove STORE_DEFAULT
            }

            // Retrieve value
            $sanitizeClass = $this->attributes[FE_CHECK_TYPE];
            //TODO: $sanitizeClass = ($mode == FORM_UPDATE) ? SANITIZE_ALLOW_ALL : $formElement[FE_CHECK_TYPE];
            $this->value = $this->store->getVar($this->attributes[FE_NAME], $storeUse, $sanitizeClass, $foundInStore);
        }
    }


    /**
     * @param array|string $value
     *
     * @return array|string
     * @throws \CodeException
     * @throws \DbException
     * @throws \DownloadException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \UserFormException
     * @throws \UserReportException
     */
    protected function processReportSyntax($value, ?Report $report = null, ?BodytextParser $bodytextParser = null): array|string {

        if (is_array($value)) {
            $new = array();

            //might happen for e.g Template Groups
            foreach ($value as $item) {
                $new[] = $this->processReportSyntax($item, $report, $bodytextParser);
            }

            return $new;
        }

        $value = trim($value);
        if (substr($value, 0, 8) == SHEBANG_REPORT) {
            if ($report === null) {
                $report = new Report(array(), $this->evaluate, false);
            }

            if ($bodytextParser === null) {
                $bodytextParser = new BodytextParser();
            }

            $storeRecord = $this->store->getStore(STORE_RECORD);
            $value = $report->process($bodytextParser->process($value));
            $this->store->setStore($storeRecord, STORE_RECORD, true);
            $this->store->setVar(SYSTEM_REPORT_FULL_LEVEL, '', STORE_SYSTEM); // debug
        }

        return $value;
    }

    /**
     * If SHOW_DEBUG_INFO=yes: create a link (incl. SIP) to edit this FE.
     * Sets $this->formEditorUrl and $this->formEditorTooltip
     *
     * @return void
     * @throws CodeException
     * @throws DbException
     * @throws UserFormException
     * @throws UserReportException
     */
    public function handleFormEditorUrl(): void {

        $param = [FE_FORM_ID => $this->attributes[FE_FORM_ID]];
        if (!$this->showDebugInfoFlag) {
            return;
        }

        $queryStringArray = [
            F_ID => $this->store->getVar(SYSTEM_EDIT_FORM_PAGE, STORE_SYSTEM),
            FORM_NAME_FORM => FORM_NAME_FORM_ELEMENT,
            CLIENT_RECORD_ID => $this->attributes[FE_ID],
            PARAM_DB_INDEX_DATA => $this->databaseManager->indexQfq
        ];

        if (T3Handler::useSlugsInsteadOfPageAlias()) {
            unset($queryStringArray['id']);
        }

        $queryStringArray = array_merge($queryStringArray, $param);

        $queryString = Support::arrayToQueryString($queryStringArray);

        if (T3Handler::useSlugsInsteadOfPageAlias()) {
            $queryString = Path::urlApp($this->store->getVar(SYSTEM_EDIT_FORM_PAGE, STORE_SYSTEM)) . '?' . $queryString;
        }

        $sip = $this->store->getSipInstance();
        $this->formEditorUrl = $sip->queryStringToSip($queryString);
        $this->formEditorTooltip = $this->form->specFinal[FE_NAME] . ' / ' . $this->attributes[FE_NAME] . ' (' . $this->attributes[FE_TYPE] . ') [' . $this->attributes[FE_ID] . ']';
    }

    private function handleDebugToolTip() {

        if ($this->showDebugInfoFlag && count($this->debugStack) > 0)  {
            $this->attributes[FE_TOOLTIP] = implode("\n", $this->debugStack);;
        }

    }


}