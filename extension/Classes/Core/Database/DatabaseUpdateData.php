<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 5/9/17
 * Time: 9:38 AM
 */

// Always put the latest updates to the end!!!
$UPDATE_ARRAY = array(

    '0.12.0' => [
        "ALTER TABLE  `FormElement` ADD `rowLabelInputNote` SET('row','label','/label','input','/input','note','/note','/row') NOT NULL DEFAULT 'row,label,/label,input,/input,note,/note,/row' AFTER  `bsNoteColumns` ",
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra', 'gridJQW',  'text',  'editor',  'time',  'note',  'password',  'radio',  'select',  'subrecord',  'upload',  'fieldset', 'pill',     'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave', 'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text' ",
    ],

    '0.13.0' => [
        "ALTER TABLE FormElement MODIFY COLUMN checkType ENUM('alnumx','digit','numerical','email','min|max','min|max date', 'pattern','allbut','all') NOT NULL DEFAULT 'alnumx' ",
    ],

    '0.14.0' => [
        "ALTER TABLE `FormElement` CHANGE `placeholder` `placeholder` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ",
    ],

    '0.15.0' => [
        "UPDATE `FormElement` SET `parameter` = REPLACE(parameter, 'typeAheadLdapKeyPrintf', 'typeAheadLdapIdPrintf')",
        "ALTER TABLE  `FormElement` CHANGE  `placeholder`  `placeholder` VARCHAR( 2048 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ",
    ],

    '0.16.0' => [
        "ALTER TABLE `FormElement` ADD INDEX `feIdContainer` (`feIdContainer`)",
        "ALTER TABLE `FormElement` ADD INDEX `ord` (`ord`)",
        "ALTER TABLE `FormElement` ADD `adminNote` TEXT NOT NULL AFTER `note`",
    ],

    '0.17.0' => [
        "ALTER TABLE  `FormElement` ADD  `encode` ENUM(  'none',  'specialchar' ) NOT NULL DEFAULT  'specialchar' AFTER  `subrecordOption`",
        "UPDATE `FormElement` SET encode='none' WHERE class='native' AND type='editor'",

        "ALTER TABLE  `Form` ADD  `escapeTypeDefault` VARCHAR(32) NOT NULL DEFAULT  'c' AFTER  `permitEdit`",
        "UPDATE `Form` SET `escapeTypeDefault`='-'",
    ],

    '0.18.0' => [
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'page',  'url',  'url-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'client'",
        "UPDATE `Form` SET `forwardMode`='url' WHERE `forwardMode`='page'",
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'url',  'url-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'client'",
    ],

    '0.18.3' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'time', 'note',  'password',  'radio',  'select',  'subrecord',  'upload',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete', 'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM(  'client',  'no',  'url',  'url-skip-history',  'url-sip' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'client';",
    ],

    '0.19.0' => [
        "ALTER TABLE  `Form` ADD  `dirtyMode` ENUM( 'exclusive', 'advisory', 'none' ) NOT NULL DEFAULT  'exclusive' AFTER  `requiredParameter`",
        "ALTER TABLE  `Form` ADD  `recordLockTimeoutSeconds` INT NOT NULL DEFAULT  '900' AFTER  `parameter`",
        "CREATE TABLE IF NOT EXISTS `Period` (`id` INT(11) NOT NULL AUTO_INCREMENT, `start` DATETIME NOT NULL, `name` VARCHAR(255) NOT NULL, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL, PRIMARY KEY (`id`), KEY `start` (`start`)) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;",
        "INSERT INTO `Period` (`start`, `name`, `created`) VALUES (NOW(), 'dummy', NOW());"
    ],

    '0.19.2' => [
        "CREATE TABLE IF NOT EXISTS `Cron` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `grId` INT(11) NOT NULL, `type` ENUM('mail', 'website') NOT NULL DEFAULT 'website', `lastRun` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `lastStatus` TEXT NOT NULL, `nextRun` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `frequency` VARCHAR(32) NOT NULL, `inProgress` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', `status` ENUM('enable', 'disable') NOT NULL DEFAULT 'enable', `content` TEXT NOT NULL, `comment` TEXT NOT NULL, `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', PRIMARY KEY (`id`)) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;",
    ],

    '0.19.5' => [
        "ALTER TABLE `Form` ADD `parameterLanguageA` TEXT NOT NULL AFTER `parameter`",
        "ALTER TABLE `Form` ADD `parameterLanguageB` TEXT NOT NULL AFTER `parameterLanguageA`",
        "ALTER TABLE `Form` ADD `parameterLanguageC` TEXT NOT NULL AFTER `parameterLanguageB`",
        "ALTER TABLE `Form` ADD `parameterLanguageD` TEXT NOT NULL AFTER `parameterLanguageC`",

        "ALTER TABLE `FormElement` ADD `parameterLanguageA` TEXT NOT NULL AFTER `parameter`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageB` TEXT NOT NULL AFTER `parameterLanguageA`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageC` TEXT NOT NULL AFTER `parameterLanguageB`",
        "ALTER TABLE `FormElement` ADD `parameterLanguageD` TEXT NOT NULL AFTER `parameterLanguageC`",
    ],

    '0.21.0' => [
        "ALTER TABLE  `Form` CHANGE  `requiredParameter`  `requiredParameterNew` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  ''",
        "ALTER TABLE  `Form` ADD  `requiredParameterEdit` VARCHAR( 255 ) NOT NULL AFTER  `requiredParameterNew`",
        "UPDATE `Form` SET `requiredParameterEdit`=requiredParameterNew",
    ],

    '0.24.0' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'time',  'note',  'password',  'radio',  'select',  'subrecord',  'upload', 'annotate',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.0' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'annotate',  'time',  'note',  'password',  'radio',  'select', 'subrecord',  'upload',  'annotate',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete', 'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.3' => [
        "ALTER TABLE `MailLog` ADD `xId2` INT NOT NULL AFTER `xId`, ADD `xId3` INT NOT NULL AFTER `xId2`",
    ],

    '0.25.7' => [
        "ALTER TABLE `MailLog` CHANGE `attach` `attach` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '0.25.10' => [
        "ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',  'gridJQW',  'text',  'editor',  'annotate',  'imageCut', 'time',  'note',  'password',  'radio',  'select', 'subrecord',  'upload',  'fieldset',  'pill',  'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',  'afterInsert',  'afterUpdate',  'afterDelete', 'sendMail',  'paste' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text';",
    ],

    '0.25.11' => [
        "UPDATE `FormElement` SET `checkType` = 'alnumx', checkPattern = '', parameter = CONCAT(parameter, '\nmin = ', SUBSTRING_INDEX(checkPattern, '|', 1), '\nmax = ', SUBSTRING_INDEX(checkPattern, '|', -1)) WHERE checkType LIKE 'min|max%' AND checkPattern <> ''",
        "ALTER TABLE `FormElement` CHANGE `checkType` `checkType` ENUM('alnumx','digit','numerical','email','pattern','allbut','all') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'alnumx';",
    ],

    '0.25.12' => [
        "ALTER TABLE `FormElement` CHANGE `checkType` `checkType` ENUM('auto','alnumx','digit','numerical','email','pattern','allbut','all') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'auto';",
        "ALTER TABLE `FormElement` CHANGE `label` `label` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '18.04.0' => [
        "ALTER TABLE `Cron` ADD `outputFile` VARCHAR(255) NOT NULL AFTER `comment`",
        "ALTER TABLE `Cron` ADD `outputMode` ENUM('overwrite','append') NOT NULL DEFAULT 'append' AFTER `outputFile`",
        "ALTER TABLE `Cron` ADD `outputPattern` VARCHAR(255) NOT NULL AFTER `outputMode`",
    ],

    '18.6.0' => [
        "ALTER TABLE `Form` CHANGE `forwardMode` `forwardMode` ENUM('auto', 'client','no','url','url-skip-history','url-sip') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'client'",
        "UPDATE `Form` SET `forwardMode`='auto' WHERE `forwardMode`='client'",
        "ALTER TABLE `Form` CHANGE `forwardMode` `forwardMode` ENUM('auto', 'close', 'no','url','url-skip-history','url-sip') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'auto';",
    ],

    '18.8.2' => [
        "ALTER TABLE `Form` CHANGE `title` `title` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''",
    ],

    '18.9.1' => [
        "ALTER TABLE `Form` ADD `primaryKey` VARCHAR(255) NOT NULL DEFAULT '' AFTER `tableName`",
    ],

    '18.10.2' => [
        "ALTER TABLE `MailLog` ADD `cc` TEXT NOT NULL AFTER `receiver`, ADD `bcc` TEXT NOT NULL AFTER `cc`;",
    ],

    '19.1.2' => [
        "ALTER TABLE `Form` ADD `labelAlign` ENUM('default','left','center','right') NOT NULL DEFAULT 'default' AFTER `forwardPage`;",
        "ALTER TABLE `FormElement` ADD `labelAlign` ENUM('default','left','center','right') NOT NULL DEFAULT 'default' AFTER `maxLength`;",
    ],

    '19.2.3' => [
        "ALTER TABLE `Form` ADD `restMethod` SET('get','post','put','delete') NOT NULL DEFAULT '' AFTER `permitEdit`; ",
    ],

    '19.3.2' => [
        "ALTER TABLE  `Form` CHANGE  `forwardMode`  `forwardMode` ENUM('auto','close','no','url','url-skip-history','url-sip','url-sip-skip-history' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'auto';",
        "UPDATE `Form` SET `forwardMode`='url-sip-skip-history' WHERE `forwardMode`='url-sip'",
    ],

    '19.7.2' => [
        "ALTER TABLE `Form` CHANGE `forwardPage` `forwardPage` VARCHAR(511) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
    ],

    '19.9.1' => [
        "ALTER TABLE `Cron` ADD `xId` INT(11) NOT NULL DEFAULT 0 AFTER `grId`; ",
        "ALTER TABLE `Cron` ADD `autoGenerated` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `outputPattern`; ",
    ],

    '19.11.2' => [
        "ALTER TABLE `Form` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Form` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `FormElement` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `FormElement` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `Dirty` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Dirty` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `MailLog` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `MailLog` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `Clipboard` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Clipboard` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `Cron` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Cron` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `Split` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Split` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",

        "ALTER TABLE `Setting` CHANGE `modified` `modified` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
        "ALTER TABLE `Setting` CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; ",
    ],

    '19.12.0' => [
        "ALTER TABLE `Dirty` ADD `tabUniqId` VARCHAR(32) NOT NULL AFTER `recordHashMd5`;",
    ],

    '20.6.0' => [
        "ALTER TABLE `Form` CHANGE `title` `title` VARCHAR(1023) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `label` `label` VARCHAR(1023) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
    ],

    '21.2.0' => [
        "ALTER TABLE `Form` ADD `fileStats` VARCHAR(255) NOT NULL DEFAULT '' AFTER `deleted`;",
        "ALTER TABLE `MailLog` CHANGE `header` `header` VARCHAR(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
        "ALTER TABLE `MailLog` CHANGE `attach` `attach` VARCHAR(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';",
    ],

    '21.4.0' => [
        "ALTER TABLE `FormSubmitLog` ADD `formName`  VARCHAR(255) NOT NULL DEFAULT '' AFTER `formId`;",
    ],

    '21.6.0' => [
        "ALTER TABLE `FormElement` CHANGE `encode` `encode` ENUM('none','specialchar','single tick') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'specialchar';",
    ],

    '22.6.0' => [
        "ALTER TABLE `FormElement` ADD `encryption`  ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `dynamicUpdate`;",
        "ALTER TABLE `FormElement` ADD `encryptionMethod`  ENUM('Default', 'AES-128','AES-256') NOT NULL DEFAULT 'Default' AFTER `encryption`;",
    ],

    '22.8.1' => [
        "ALTER TABLE `FormElement` CHANGE `formId` `formId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `Dirty` CHANGE `recordId` `recordId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `FormSubmitLog` CHANGE `formId` `formId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `FormSubmitLog` CHANGE `recordId` `recordId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `Cron` CHANGE `grId` `grId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `Cron` CHANGE `xId` `xId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `Split` CHANGE `xId` `xId` INT(11) NOT NULL DEFAULT '0';",
        "ALTER TABLE `Setting` CHANGE `public` `public` TINYINT(1) NOT NULL DEFAULT '0';",
    ],

    '23.6.0' => [
        "ALTER TABLE `FormSubmitLog` ADD INDEX IF NOT EXISTS `createdFeUserFormId` (`created`, `feUser`, `formId`);",
    ],

    '23.11.0' => [
        "ALTER TABLE `Cron` CHANGE `lastStatus` `lastStatus` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `frequency` `frequency` varchar(32) NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `sql1` `sql1` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `content` `content` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `comment` `comment` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `outputFile` `outputFile` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Cron` CHANGE `outputPattern` `outputPattern` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `sip` `sip` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `tableName` `tableName` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `expire` `expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00';",
        "ALTER TABLE `Dirty` CHANGE `recordHashMd5` `recordHashMd5` char(32) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `tabUniqId` `tabUniqId` char(32) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `feUser` `feUser` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `qfqUserSessionCookie` `qfqUserSessionCookie` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Dirty` CHANGE `remoteAddress` `remoteAddress` varchar(45) NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `noteInternal` `noteInternal` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `multiSql` `multiSql` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `parameter` `parameter` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `parameterLanguageA` `parameterLanguageA` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `parameterLanguageB` `parameterLanguageB` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `parameterLanguageC` `parameterLanguageC` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Form` CHANGE `parameterLanguageD` `parameterLanguageD` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `modeSql` `modeSql` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `note` `note` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `adminNote` `adminNote` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `value` `value` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `sql1` `sql1` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `parameter` `parameter` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `parameterLanguageA` `parameterLanguageA` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `parameterLanguageB` `parameterLanguageB` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `parameterLanguageC` `parameterLanguageC` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `parameterLanguageD` `parameterLanguageD` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormElement` CHANGE `clientJs` `clientJs` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `formData` `formData` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `sipData` `sipData` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `clientIp` `clientIp` varchar(64) NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `feUser` `feUser` varchar(64) NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `userAgent` `userAgent` text NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `formName` `formName` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `FormSubmitLog` CHANGE `pageId` `pageId` int(11) NOT NULL DEFAULT 0;",
        "ALTER TABLE `FormSubmitLog` CHANGE `sessionId` `sessionId` varchar(32) NOT NULL DEFAULT '';",
        "ALTER TABLE `MailLog` CHANGE `receiver` `receiver` text NOT NULL DEFAULT '';",
        "ALTER TABLE `MailLog` CHANGE `cc` `cc` text NOT NULL DEFAULT '';",
        "ALTER TABLE `MailLog` CHANGE `bcc` `bcc` text NOT NULL DEFAULT '';",
        "ALTER TABLE `MailLog` CHANGE `body` `body` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Setting` CHANGE `type` `type` enum('','tablesorter') NOT NULL DEFAULT '';",
        "ALTER TABLE `Setting` CHANGE `name` `name` varchar(64) NOT NULL DEFAULT '';",
        "ALTER TABLE `Setting` CHANGE `feUser` `feUser` varchar(32) NOT NULL DEFAULT '' COMMENT 'In case there is no logged in user, take QFQ cookie.';",
        "ALTER TABLE `Setting` CHANGE `tableId` `tableId` varchar(64) NOT NULL DEFAULT '';",
        "ALTER TABLE `Setting` CHANGE `view` `view` text NOT NULL DEFAULT '';",
        "ALTER TABLE `Split` CHANGE `tableName` `tableName` varchar(255) NOT NULL DEFAULT '';",
        "ALTER TABLE `Split` CHANGE `pathFileName` `pathFileName` varchar(255) NOT NULL DEFAULT '';",
    ],


    '24.1.0.rc1' => [
        "CREATE TABLE IF NOT EXISTS `FileUpload` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `pathFileName` VARCHAR(512) NOT NULL, `grId` INT(11) NOT NULL DEFAULT '0', `xId` INT(11) NOT NULL DEFAULT '0', `uploadId` INT(11) NOT NULL, `size` VARCHAR(32) NOT NULL DEFAULT '0' COMMENT 'Filesize in bytes', `type` VARCHAR(64) NOT NULL DEFAULT '', `ord` INT(11) NOT NULL DEFAULT '0', `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), KEY `uploadId` (`uploadId`), KEY `pathFileNameGrIdXidUploadId` (`pathFileName`, `grId`, `xId`, `uploadId`) USING BTREE ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;",
        "CREATE TABLE IF NOT EXISTS `WikiPage` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `wpIdParent` INT(11) DEFAULT NULL, `pageSlug` VARCHAR(255) NOT NULL, `name` VARCHAR(255) NOT NULL, `content` TEXT NOT NULL, `author` VARCHAR(64) NOT NULL, `roUser` VARCHAR(512) NOT NULL, `roGroup` VARCHAR(512) NOT NULL, `roPublic` ENUM ('on','off') NOT NULL, `rwUser` VARCHAR(512) NOT NULL, `rwGroup` VARCHAR(512) NOT NULL, `rwPublic` ENUM ('on','off') NOT NULL, `pageLock` VARCHAR(64) NOT NULL, `imageBorder` ENUM ('none','1pxblack','shadow') NOT NULL DEFAULT 'none', `wpIdCurrent` INT(11) DEFAULT NULL, `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;",
        "CREATE TABLE IF NOT EXISTS `WikiAttachment` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `wpId` INT(11) NOT NULL, `feUser` VARCHAR(64) NOT NULL, `pathFileName` VARCHAR(512) NOT NULL, `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 0;",
        // Save chat conversations.
        "CREATE TABLE IF NOT EXISTS `Chat` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `xId` INT(11) NOT NULL DEFAULT '0', `pIdCreator` INT(11) NOT NULL DEFAULT '0', `pIdTeam` INT(11) NOT NULL DEFAULT '0', `cIdTopic` INT(11) NOT NULL DEFAULT '0', `cIdThread` INT(11) NOT NULL DEFAULT '0', `cIdTag` INT(11) NOT NULL DEFAULT '0', `cIdLastRead` INT(11) NOT NULL DEFAULT '0', `xGrIdStatus` INT(11) NOT NULL DEFAULT '0', `type` ENUM('message','topic','tag','reminder','read') NOT NULL DEFAULT 'message', `message` TEXT NOT NULL DEFAULT '', `username` VARCHAR(128) NOT NULL DEFAULT '', `emoticon` VARCHAR(255) NOT NULL DEFAULT '', `reference` VARCHAR(255) NOT NULL DEFAULT '', `modified` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), KEY `xId` (`xId`)) ENGINE = InnoDB DEFAULT CHARSET = utf8;"
    ],

    '24.3.0' => [
        "ALTER TABLE `FileUpload` CHANGE IF EXISTS `type` `mimeType` VARCHAR(64) NOT NULL DEFAULT '';",
        "ALTER TABLE `FileUpload` CHANGE IF EXISTS `size` `fileSize` VARCHAR(32) NOT NULL DEFAULT '0';",
    ],
//TODO/CR bei allen optionalen Tabellen mit 'IF EXISTS' abfangen falls sie nicht existieren: "ALTER TABLE IF EXISTS `Form` CHANGE `show..."
// Test ob das mit unserer MariaDb funktioniert: in https://mariadb.com/kb/en/alter-table/ steht das 'ALTER TABLE IF EXISTS' nicht geht.
    '24.4.0' => [
        "ALTER TABLE `Form` CHANGE `showButton` `showButton` SET('new','delete','close','save','note') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new,delete,close,save,note';",
        "CREATE TABLE IF NOT EXISTS `NoteItem` (`id` int(11) NOT NULL AUTO_INCREMENT,`pIdCreator` int(11) NOT NULL DEFAULT '0', `xId` int(11) NOT NULL DEFAULT '0', `formId` int(11) NOT NULL DEFAULT '0', `formName` varchar(60) NOT NULL DEFAULT '', `title` varchar(100) NOT NULL DEFAULT '', `note` text DEFAULT '', `access` enum('private','all') NOT NULL DEFAULT 'private', `isDone` enum('yes','no') NOT NULL DEFAULT 'no', `hasToolbar` enum('yes','no') NOT NULL DEFAULT 'no', `reminderDate` datetime DEFAULT NULL, `created` datetime NOT NULL DEFAULT current_timestamp(), `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(), PRIMARY KEY (`id`), KEY `formIdxIdpIdCreator` (`formId`, `xId`, `pIdCreator`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;",
        "CREATE TABLE IF NOT EXISTS `Grp` (`id` int(11) NOT NULL AUTO_INCREMENT, `grId` int(11) NOT NULL DEFAULT '0', `name` varchar(255) NOT NULL DEFAULT '', `reference` varchar(255) NULL DEFAULT NULL, `value` varchar(255) NOT NULL DEFAULT '', `created` datetime NOT NULL DEFAULT current_timestamp(), `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(), PRIMARY KEY (`id`), UNIQUE KEY `reference` (`id`), KEY `grIdName` (`grId`, `name`) USING BTREE) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;",
        "CREATE TABLE IF NOT EXISTS `Glue` (`id` int(11) NOT NULL AUTO_INCREMENT, `grId` int(11) NOT NULL, `xId` int(11) NOT NULL, `created` datetime NOT NULL DEFAULT current_timestamp(), `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(), PRIMARY KEY (`id`), KEY `grIdxId` (`grId`, `xId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"
    ],

    '24.12.0' => [
        "ALTER TABLE `Form` CHANGE `title` `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '';",
    ],
);




