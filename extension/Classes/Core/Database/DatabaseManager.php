<?php

namespace IMATHUZH\Qfq\Core\Database;

use IMATHUZH\Qfq\Core\Store\Store;
use IMATHUZH\Qfq\Core\Typo3\T3Handler;

/**
 * pgroeb: For now this is used only by Renderers. Allows access to all connected DBs
 *         as an alternative to passing down dbArray all the time.
 *         More refactoring in the future could make more use of this manager class.
 */
class DatabaseManager {
    public int $indexQfq;
    public int $indexData;
    public int $indexT3;

    public ?array $dbArray = null;

    //<editor-fold desc="Singleton">
    protected static ?DatabaseManager $instance = null;


    private function __construct(array $dbArray, int $indexQfq, int $indexT3, int $indexData) {
        $this->dbArray = $dbArray;
        $this->indexQfq = $indexQfq;
        $this->indexT3 = $indexT3;
        $this->indexData = $indexData;
    }

    /**
     * @return DatabaseManager
     * @throws \CodeException
     * @throws \DbException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public static function getInstance(): DatabaseManager {
        if (self::$instance == null) {
            $dbArray = array();
            $t3DbConfig = array();
            $store = Store::getInstance();
            $dbIndexData = $store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM);
            $dbIndexQfq = $store->getVar(SYSTEM_DB_INDEX_QFQ, STORE_SYSTEM);
            $dbIndexT3 = $dbIndexQfq;

            if (!defined('PHPUNIT_QFQ')) {
                $t3DbConfig = T3Handler::getTypo3DbConfig($dbIndexData, $dbIndexQfq, $dbIndexT3);
            }
            // Create Typo3 db object if config information exist. In case of api, it doesn't exist.
            if (count($t3DbConfig) > 1 && $dbIndexT3 !== 0) {
                $dbArray[$dbIndexT3] = new Database($dbIndexT3, $t3DbConfig);
            } else {
                // Fallback to qfq user credentials. These are used usually for typo3 db.
                $dbArray[$dbIndexT3] = new Database($dbIndexQfq);
            }

            $dbArray[$dbIndexData] = new Database($dbIndexData);

            if ($dbIndexData != $dbIndexQfq) {
                $dbArray[$dbIndexQfq] = new Database($dbIndexQfq);
            }

            self::$instance = new DatabaseManager($dbArray, $dbIndexQfq, $dbIndexT3, $dbIndexData);
        }

        return self::$instance;
    }

    /**
     * For now: Instance is set manually...
     */
    /*public static function setInstance(array $dbArray, int $indexQfq, int $indexT3, int $indexData): void{
        self::$instance = new DatabaseManager($dbArray, $indexQfq, $indexT3, $indexData);
    }*/
    //</editor-fold>

    public function getQfqDb(): Database {
        return $this->dbArray[$this->indexQfq];
    }

    public function getT3Db(): Database {
        return $this->dbArray[$this->indexT3];
    }

    public function getDataDb(): Database {
        return $this->dbArray[$this->indexData];
    }

    public function getDbByIndex($dbIndex): Database {
        return $this->dbArray[$dbIndex];
    }
}