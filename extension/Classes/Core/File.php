<?php
/**
 * Created by PhpStorm.
 * User: crose
 * Date: 4/25/16
 * Time: 10:39 PM
 */

namespace IMATHUZH\Qfq\Core;


use IMATHUZH\Qfq\Core\Database\Database;
use IMATHUZH\Qfq\Core\Helper\HelperFile;
use IMATHUZH\Qfq\Core\Helper\Logger;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\Helper\Sanitize;
use IMATHUZH\Qfq\Core\Helper\Support;
use IMATHUZH\Qfq\Core\Report\Link;
use IMATHUZH\Qfq\Core\Store\Session;
use IMATHUZH\Qfq\Core\Store\Store;

/**
 * Class File
 * @package qfq
 */
class File {

    private $uploadErrMsg = array();
    public $imageUploadFilePath = null;
    public $uniqueFileId = null;
    public $groupId = null;
    public $sipTmp = null;
    public $newFileName = null;
    private $db = null;

    /**
     * @var Store
     */
    private $store = null;

    /**
     * @var Session
     */
    private $session = null;

    private $qfqLogPathFilenameAbsolute = '';

    /**
     * @param bool|false $phpUnit
     * @throws \CodeException
     * @throws \UserFormException
     * @throws \UserReportException
     */
    public function __construct($phpUnit = false) {

        #TODO: rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}"
        $this->session = Session::getInstance($phpUnit);

        $this->store = Store::getInstance('', $phpUnit);
        $this->qfqLogPathFilenameAbsolute = Path::absoluteQfqLogFile();

        $this->dbIndexData = $this->store->getVar(SYSTEM_DB_INDEX_DATA, STORE_SYSTEM);
        $this->dbIndexQfq = $this->store->getVar(SYSTEM_DB_INDEX_QFQ, STORE_SYSTEM);

        // Default is qfq index
        $this->dbIndexFilePond = $this->dbIndexQfq;
        $dbIndexSip = $this->store->getVar(TOKEN_DB_INDEX, STORE_SIP . STORE_EMPTY);
        if (in_array($dbIndexSip, [$this->dbIndexData, $this->dbIndexQfq])) {
            $this->dbIndexFilePond = $dbIndexSip;
        }
        $this->dbArray[$this->dbIndexFilePond] = new Database($this->dbIndexFilePond);

        $this->sip = $this->store->getSipInstance();

        $this->uploadErrMsg = [
            UPLOAD_ERR_INI_SIZE => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
            UPLOAD_ERR_FORM_SIZE => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
            UPLOAD_ERR_PARTIAL => "The uploaded file was only partially uploaded",
            UPLOAD_ERR_NO_FILE => "No file was uploaded",
            UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder",
            UPLOAD_ERR_CANT_WRITE => "Failed to write file to disk",
            UPLOAD_ERR_EXTENSION => "File upload stopped by extension",
        ];
    }

    /**
     * @throws \CodeException
     * @throws \UserFormException
     */
    public function process() {

        $action = $this->store->getVar(FILE_ACTION, STORE_CLIENT . STORE_SIP, SANITIZE_ALLOW_ALNUMX);
        $sipUpload = $this->store->getVar(SIP_SIP, STORE_SIP);
        if ($sipUpload === false && $action != FILE_ACTION_IMAGE_UPLOAD) {

            if (empty($_FILES)) {
                throw new \UserFormException('Missing $_FILES[] - the whole request seems broken', ERROR_UPLOAD_FILES_BROKEN);
            }

            throw new \UserFormException('SIP invalid: ' . $sipUpload, ERROR_SIP_INVALID);
        }

        // Throws an exception if content is too big - if content is bigger than 'post_max_size', the POST is lost together with the PHP Upload error message.
        $size = $_SERVER['CONTENT_LENGTH'] ?? 0;

        if ($action != FILE_ACTION_IMAGE_UPLOAD) {
            $this->checkMaxFileSize($size);
        }

        $statusUpload = $this->store->getVar($sipUpload, STORE_EXTRA, SANITIZE_ALLOW_ALL);
        if ($statusUpload === false) {
            $statusUpload = array();
        }
        $statusUpload[UPLOAD_SIP_DOWNLOAD_KEY] = $this->store->getVar(UPLOAD_SIP_DOWNLOAD_KEY, STORE_SIP);


        switch ($action) {
            case FILE_ACTION_UPLOAD:
            case FILE_ACTION_UPLOAD_2:
                $this->doUpload($sipUpload, $statusUpload);
                break;
            case FILE_ACTION_DELETE:
                $this->doDelete($sipUpload, $statusUpload);
                break;
            case FILE_ACTION_IMAGE_UPLOAD:
                $this->imageUploadFilePath = $this->doImageUpload();
                break;
            default:
                throw new \UserFormException("Unknown FILE_ACTION: $action", ERROR_UPLOAD_UNKNOWN_ACTION);
        }
    }

    /**
     * Checks the max file size, defined per FormElement.
     * Only possible if there is a valid SIP Store.
     *
     * @param $size
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function checkMaxFileSize($size) {

        // Checked and set during form build.
        $maxFileSize = $this->store->getVar(FE_FILE_MAX_FILE_SIZE, STORE_SIP . STORE_ZERO);

        if ($size >= $maxFileSize && $maxFileSize != 0) {
            throw new \UserFormException('File too big. Max allowed size: ' . $maxFileSize . ' Bytes', ERROR_UPLOAD_TOO_BIG);
        }
    }

    /**
     * doUpload
     *
     * @param string $sipUpload
     * @param array $statusUpload
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function doUpload($sipUpload, array $statusUpload) {

        // Upload client
        $action = $this->store->getVar(FILE_ACTION, STORE_SIP, SANITIZE_ALLOW_ALNUMX);

        // New upload
        $newArr = reset($_FILES);
        // Merge new upload date to existing status information
        $statusUpload = array_merge($statusUpload, $newArr);
        HelperFile::fixMimeTypeHeif($statusUpload['tmp_name'], $statusUpload['type']);

        if ($statusUpload[FILES_ERROR] !== UPLOAD_ERR_OK) {
            throw new \UserFormException(
                json_encode([ERROR_MESSAGE_TO_USER => 'Upload: Error', ERROR_MESSAGE_TO_DEVELOPER => $this->uploadErrMsg[$newArr[FILES_ERROR]]]),
                ERROR_UPLOAD);
        }

        Logger::logMessageWithPrefix(UPLOAD_LOG_PREFIX . ': File under ' . $statusUpload['tmp_name'], $this->qfqLogPathFilenameAbsolute);

        if (!empty($sipUpload)) {
            $this->checkMaxFileSize($statusUpload[FILES_SIZE]);
        }

        $accept = $this->store->getVar(FE_FILE_MIME_TYPE_ACCEPT, STORE_SIP . STORE_EMPTY);
        if ($accept != '' && !HelperFile::checkFileType($statusUpload['tmp_name'], $statusUpload['name'], $accept)) {
            throw new \UserFormException('Filetype not allowed. Allowed: ' . $accept, ERROR_UPLOAD_FILE_TYPE);
        }

        // rename uploaded file: ?.cached
        $filenameCached = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);

        // Remember: PHP['upload_tmp_dir']='' means '/tmp' AND upload process is CHROOT to /tmp/systemd-private-...-apache2.service-../
        error_clear_last();
        if ($action === FILE_ACTION_UPLOAD_2) {
            // Generate a unique file ID
            $this->uniqueFileId = $_POST[UPLOAD_ID];
            $fullPath = $_POST[UPLOAD_PATH_FILE_NAME];
            $pathDefault = $_POST[UPLOAD_PATH_DEFAULT];
            $groupId = $_POST['groupId'];
            $recordData = $this->store->getVar(UPLOAD_RECORD_DATA, STORE_SIP);
            $table = $this->store->getVar(SIP_TABLE, STORE_SIP);
            $filename = $statusUpload[FILES_NAME];

            if ($pathDefault === 'undefined') {
                $filenameDummy = basename($fullPath);
                $fileParts = pathinfo($filenameDummy);

                if (isset($fileParts['extension'])) {
                    $filename = $filenameDummy;
                    $fullPath = dirname($fullPath) . '/';
                } else if (substr($fullPath, -1) !== '/') {
                    $fullPath .= '/';
                }
            }

            if ($this->uniqueFileId == $groupId) {
                $this->uniqueFileId = 0;
            }
            $filename = Sanitize::safeFilename($filename);

            $fullPath = Path::absoluteApp($fullPath);
            $changedFileName = HelperFile::getUniqueFileName($fullPath, $filename);
            $fullPath = $fullPath . $changedFileName;

            HelperFile::mkDirParent($fullPath);
            if (!move_uploaded_file($newArr[FILES_TMP_NAME], $fullPath)) {
                $msg = error_get_last();
                throw new \UserFormException(
                    json_encode([ERROR_MESSAGE_TO_USER => 'Upload: Error', ERROR_MESSAGE_TO_DEVELOPER => $msg]),
                    ERROR_UPLOAD_FILE_TYPE);
            }

            if ($this->uniqueFileId == 0) {
                $insertColumns = '';
                $insertValues = [];
                $prepareQuestions = '';
                if ($recordData != '') {
                    $recordData = json_decode($recordData);
                    foreach ($recordData as $key => $value) {
                        $insertColumns .= ',' . $key;
                        $insertValues[] = $value;
                        $prepareQuestions .= ',?';
                    }
                }
                $params = array_merge([$fullPath, $groupId, $statusUpload[FILES_SIZE], $statusUpload[FILES_TYPE], 0], $insertValues);
                // Do database insert with unique file ID and pathFileName
                $dbName = $this->store->getVar(SYSTEM_DB_NAME_QFQ, STORE_SYSTEM);
                $sql = "INSERT INTO `$dbName`.`$table` (pathFileName, uploadId, fileSize, mimeType, ord  $insertColumns)
            VALUES (?,?,?,?,? $prepareQuestions)";
                $this->uniqueFileId = $this->dbArray[$this->dbIndexFilePond]->sql($sql, ROW_REGULAR, $params, "Creating FileUpload failed.");

                if ($groupId == 0) {
                    $groupId = $this->uniqueFileId;
                    $sqlUpdate = "UPDATE `$dbName`.`$table` SET uploadId = ? WHERE id = ?";
                    $this->dbArray[$this->dbIndexFilePond]->sql($sqlUpdate, ROW_REGULAR, [$this->uniqueFileId, $this->uniqueFileId], "Updating FileUpload failed.");
                }
            }
            $this->groupId = $groupId;
            $this->logUpload($this->uniqueFileId);
        } else {
            if (!move_uploaded_file($newArr[FILES_TMP_NAME], $filenameCached)) {
                $msg = error_get_last();
                throw new \UserFormException(
                    json_encode([ERROR_MESSAGE_TO_USER => 'Upload: Error', ERROR_MESSAGE_TO_DEVELOPER => $msg]),
                    ERROR_UPLOAD_FILE_TYPE);
            }

            // Make currently uploaded file downloadable
            $link = new Link($this->sip, $this->dbIndexData);
            $sipDownload = $link->renderLink('', 's|d:output|M:file|r:8|F:' . $filenameCached);
            $this->sipTmp = $sipDownload;

            // Reset sipDownloadKey after fresh upload
            $this->store->setVar($statusUpload[UPLOAD_SIP_DOWNLOAD_KEY], array(), STORE_EXTRA);
        }

        $this->store->setVar($sipUpload, $statusUpload, STORE_EXTRA);
    }

    /**
     * @param $sipUpload
     * @param $statusUpload
     *
     * @throws \CodeException
     * @throws \UserFormException
     * @internal param string $keyStoreExtra
     */
    private function doDelete($sipUpload, $statusUpload) {
        $action = $this->store::getVar(FILE_ACTION, STORE_SIP . STORE_EMPTY);

        // Handle FilePond delete
        if ($action == FILE_ACTION_DELETE) {
            $uploadId = $_POST[UPLOAD_ID];
            $table = $this->store::getVar(SIP_TABLE, STORE_SIP . STORE_EMPTY);;
            $allowDelete = $this->store::getVar('allowDelete', STORE_SIP . STORE_EMPTY);
            $preloadedFileList = $this->store::getVar('preloadedFileIds', STORE_SIP . STORE_EMPTY);
            $preloadedFileArray = explode(',', $preloadedFileList);
            $pathFileName = '';

            // Check if uploadId exists in preloaded files and if it is allowed to delete
            if ($allowDelete == '0' && in_array($uploadId, $preloadedFileArray)) {
                return;
            }

            // Get path from database
            $sqlPath = "SELECT pathFileName FROM `$table` WHERE id = ?";
            $result = $this->dbArray[$this->dbIndexFilePond]->sql($sqlPath, ROW_EXPECT_1, [$uploadId], "File not found in database.");

            if (!empty($result[UPLOAD_PATH_FILE_NAME])) {
                $pathFileName = $result[UPLOAD_PATH_FILE_NAME];
            }

            // Delete file from database with unique file id
            $sqlPath = "DELETE FROM `$table` WHERE id = ?";
            $this->dbArray[$this->dbIndexFilePond]->sql($sqlPath, ROW_EXPECT_1, [$uploadId], "File not deleted from database.");

            // Delete file from server
            HelperFile::unlink($pathFileName);

            $this->logUpload($uploadId, true);

            $this->uniqueFileId = 0;
        } else {
            if (isset($statusUpload[FILES_TMP_NAME]) && $statusUpload[FILES_TMP_NAME] != '') {
                $file = Support::extendFilename($statusUpload[FILES_TMP_NAME], UPLOAD_CACHED);
                if (file_exists($file)) {
                    HelperFile::unlink($file, $this->qfqLogPathFilenameAbsolute);
                }
                $statusUpload[FILES_TMP_NAME] = '';
            }

            $statusUpload[FILES_FLAG_DELETE] = '1';
            $this->store->setVar($sipUpload, $statusUpload, STORE_EXTRA);
        }
    }

    /**
     * Get tinyMce image upload and store it with unique filename in system. Return new filepath.
     *
     * @throws \CodeException
     * @throws \UserFormException
     */
    private function doImageUpload() {
        reset($_FILES);
        $temp = current($_FILES);

        $httpOrigin = Store::getVar(SYSTEM_HTTP_ORIGIN, STORE_SYSTEM, SANITIZE_ALLOW_ALL);

        if (!is_uploaded_file($temp[FILES_TMP_NAME])) {
            // Notify editor that the upload failed
            header(HTTP_500_SERVER_ERROR);
            Logger::logMessageWithPrefix('Internal error. Uploaded file not found.', Path::absoluteQfqLogFile());
            return 0;
        }

        // same-origin requests won't set an origin. If the origin is set, it must be valid.
        if ($httpOrigin === $_SERVER['HTTP_ORIGIN']) {
            header('Access-Control-Allow-Origin: ' . $httpOrigin);
        } else {
            header(HTTP_403_ORIGIN_DENIED);
            Logger::logMessageWithPrefix('Origin denied. From server given origin doesnt match with baseUrl.', Path::absoluteQfqLogFile());
            return 0;
        }

        /*
          If your script needs to receive cookies, set images_upload_credentials : true in
          the configuration and enable the following two headers.
        */
        // header('Access-Control-Allow-Credentials: true');
        // header('P3P: CP="There is no P3P policy."');

        // Sanitize input
        // Disallow 'none alphanumeric'. Allow dot or underscore and conditionally '/'.
        $temp[FILES_NAME] = Sanitize::safeFilename($temp[FILES_NAME]);

        // Verify extension
        // Split between 'Media Type' and 'Media Subtype'
        $fileMimeTypeSplitted = explode('/', $temp[FILES_TYPE], 2);
        if ($fileMimeTypeSplitted[0] !== NAME_IMAGE) {
            header(HTTP_400_INVALID_EXTENSION);
            Logger::logMessageWithPrefix('Mime type error. Invalid picture type.', Path::absoluteQfqLogFile());
            return 0;
        }

        // Base folder path should be with user input changeable -> take over sip given parameters from formElement
        $imageUploadDirUser = Store::getVar(FE_EDITOR_FILE_UPLOAD_PATH, STORE_SIP, SANITIZE_ALLOW_ALL);
        $imageUploadDirUser = base64_decode($imageUploadDirUser);

        // Get upload path from config
        $imageUploadDir = Store::getVar(SYSTEM_IMAGE_UPLOAD_DIR, STORE_SYSTEM, SANITIZE_ALLOW_ALL);

        //overwrite imageUploadDir with user given path if exists
        if ($imageUploadDirUser != '') {
            $imageUploadDir = $imageUploadDirUser;
        }

        $imageUploadDirAbsolute = Path::absoluteApp($imageUploadDir);

        // Name should be unique, add to end of name number if exists to not overwrite it
        $changedFileName = HelperFile::getUniqueFileName($imageUploadDirAbsolute, $temp[FILES_NAME]);
        $neededSlash = '';
        if (substr($imageUploadDirAbsolute, -1) !== '/') {
            $neededSlash = '/';
        }
        $fileToWrite = $imageUploadDirAbsolute . $neededSlash . $changedFileName;
        HelperFile::mkDirParent($fileToWrite);
        move_uploaded_file($temp[FILES_TMP_NAME], $fileToWrite);

        // baseUrl needed for absolute path. Necessary for image upload in T3 V10 or higher
        $baseUrl = Store::getVar(SYSTEM_BASE_URL, STORE_SYSTEM, SANITIZE_ALLOW_ALL);


        // Respond to the successful upload with JSON.
        return $baseUrl . $imageUploadDir . $neededSlash . $changedFileName;
    }

    private function logUpload($uploadId, $deleteFlag = false, $uploadType = 'report'): void {
        $formName = '_uploadInReport';
        if ($uploadType === 'form') {
            $formName = '_uploadInForm';
        }

        $recordId = $uploadId;
        $pageId = $this->store->getVar(TYPO3_PAGE_ID, STORE_TYPO3, SANITIZE_ALLOW_ALNUMX);
        $sessionId = session_id();
        $feUser = $this->store->getVar(TYPO3_FE_USER, STORE_TYPO3, SANITIZE_ALLOW_ALNUMX);
        $clientIp = $this->store->getVar(CLIENT_REMOTE_ADDRESS, STORE_CLIENT . STORE_EMPTY);
        $userAgent = $this->store->getVar(CLIENT_HTTP_USER_AGENT, STORE_CLIENT . STORE_EMPTY);
        $sipData = json_encode($this->store->getStore(STORE_SIP), JSON_UNESCAPED_UNICODE);
        $formData = $deleteFlag ? 'deleted' : 'uploaded';
        $formId = 0;

        $sql = "INSERT INTO `FormSubmitLog` (`formData`, `sipData`, `clientIp`, `feUser`, `userAgent`, `formId`, `formName`, `recordId`, `pageId`, `sessionId`, `created`)" .
            "VALUES (?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, NOW())";

        $params = [$formData, $sipData, $clientIp, $feUser, $userAgent, $formId, $formName, $recordId, $pageId, $sessionId];

        $this->dbArray[$this->dbIndexFilePond]->sql($sql, ROW_REGULAR, $params);
        $formSubmitLogId = $this->dbArray[$this->dbIndexFilePond]->getLastInsertId();
        $this->store::setVar(EXTRA_FORM_SUBMIT_LOG_ID, $formSubmitLogId, STORE_EXTRA);

    }
}