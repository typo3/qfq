# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
CREATE TABLE IF NOT EXISTS `Period`
(
    `id`       INT(11)      NOT NULL AUTO_INCREMENT,
    `start`    DATETIME     NOT NULL DEFAULT '0000-00-00 00:00:00',
    `name`     VARCHAR(255) NOT NULL DEFAULT '',
    `modified` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (`id`),
    KEY `start` (`start`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;


