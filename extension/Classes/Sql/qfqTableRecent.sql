CREATE TABLE IF NOT EXISTS `Recent`
(
    `id`         INT(11)                               NOT NULL AUTO_INCREMENT,
    `recentType` ENUM ('', 'form open', 'switch user') NOT NULL DEFAULT '',
    `pId`        INT(11)                               NOT NULL DEFAULT 0,
    `pIdReal`    INT(11)                               NOT NULL DEFAULT 0,
    `feUser`     VARCHAR(64)                                    DEFAULT '',
    `formName`   VARCHAR(255)                          NOT NULL,
    `formId`     INT(11)                               NOT NULL DEFAULT 0,
    `formTitle`  TEXT                                  NOT NULL DEFAULT '',
    `args`       text                                           DEFAULT '',
    `recordId`   INT(11)                               NOT NULL DEFAULT 0,
    `created`    DATETIME                              NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `pIdFormIdRecentType` (`pId`, `formId`, `recentType`),
    KEY `pIdFormNameRecentType` (`pId`, `formName`, `recentType`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    AUTO_INCREMENT = 0;
