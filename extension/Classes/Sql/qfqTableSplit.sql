# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
CREATE TABLE IF NOT EXISTS `Split`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `tableName`    varchar(255) NOT NULL DEFAULT '',
    `xId`          int(11)      NOT NULL DEFAULT 0,
    `pathFileName` varchar(255) NOT NULL DEFAULT '',
    `modified`     datetime     NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`      datetime     NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
