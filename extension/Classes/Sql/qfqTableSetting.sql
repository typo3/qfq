# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# Used to save tablesorter.js column selection settings. Free to use for other settings as well.
CREATE TABLE IF NOT EXISTS `Setting`
(
    `id`       int(11)                 NOT NULL AUTO_INCREMENT,
    `type`     enum ('','tablesorter') NOT NULL DEFAULT '',
    `readonly` enum ('yes','no')       NOT NULL DEFAULT 'no' COMMENT 'Settings can''t be modified.',
    `name`     varchar(64)             NOT NULL DEFAULT '',
    `public`   tinyint(1)              NOT NULL DEFAULT 0,
    `feUser`   varchar(32)             NOT NULL DEFAULT '' COMMENT 'In case there is no logged in user, take QFQ cookie.',
    `tableId`  varchar(64)             NOT NULL DEFAULT '',
    `view`     text                    NOT NULL DEFAULT '',
    `modified` datetime                NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`  datetime                NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `name` (`name`),
    KEY `typeFeUserUidTableIdPublic` (`type`, `feUser`, `tableId`, `public`) USING BTREE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    AUTO_INCREMENT = 0;
