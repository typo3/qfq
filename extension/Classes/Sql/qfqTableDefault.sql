# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# QFQ Default tables
#
CREATE TABLE IF NOT EXISTS `Form`
(
    `id`                       int(11)                                                                              NOT NULL AUTO_INCREMENT,
    `name`                     varchar(255)                                                                         NOT NULL DEFAULT '',
    `title`                    varchar(1023)                                                                        NOT NULL DEFAULT '',
    `noteInternal`             text                                                                                 NOT NULL DEFAULT '',
    `tableName`                varchar(255)                                                                         NOT NULL DEFAULT '',
    `primaryKey`               varchar(255)                                                                         NOT NULL DEFAULT '',
    `permitNew`                enum ('sip','logged_in','logged_out','always','never')                               NOT NULL DEFAULT 'sip',
    `permitEdit`               enum ('sip','logged_in','logged_out','always','never')                               NOT NULL DEFAULT 'sip',
    `restMethod`               set ('get','post','put','delete')                                                    NOT NULL DEFAULT '',
    `escapeTypeDefault`        varchar(32)                                                                          NOT NULL DEFAULT 'c',
    `render`                   enum ('bootstrap','table','plain')                                                   NOT NULL DEFAULT 'bootstrap',
    `requiredParameterNew`     varchar(255)                                                                         NOT NULL DEFAULT '',
    `requiredParameterEdit`    varchar(255)                                                                         NOT NULL DEFAULT '',
    `dirtyMode`                enum ('exclusive','advisory','none')                                                 NOT NULL DEFAULT 'exclusive',
    `showButton`               set ('new','delete','close','save','note')                                           NOT NULL DEFAULT 'new,delete,close,save',
    `multiMode`                enum ('none','horizontal','vertical')                                                NOT NULL DEFAULT 'none',
    `multiSql`                 text                                                                                 NOT NULL DEFAULT '',
    `multiDetailForm`          varchar(255)                                                                         NOT NULL DEFAULT '',
    `multiDetailFormParameter` varchar(255)                                                                         NOT NULL DEFAULT '',
    `forwardMode`              enum ('auto','close','no','url','url-skip-history','url-sip','url-sip-skip-history') NOT NULL DEFAULT 'auto',
    `forwardPage`              varchar(511)                                                                         NOT NULL DEFAULT '',
    `labelAlign`               enum ('default','left','center','right')                                             NOT NULL DEFAULT 'default',
    `bsLabelColumns`           varchar(255)                                                                         NOT NULL DEFAULT '',
    `bsInputColumns`           varchar(255)                                                                         NOT NULL DEFAULT '',
    `bsNoteColumns`            varchar(255)                                                                         NOT NULL DEFAULT '',
    `parameter`                text                                                                                 NOT NULL DEFAULT '',
    `parameterLanguageA`       text                                                                                 NOT NULL DEFAULT '',
    `parameterLanguageB`       text                                                                                 NOT NULL DEFAULT '',
    `parameterLanguageC`       text                                                                                 NOT NULL DEFAULT '',
    `parameterLanguageD`       text                                                                                 NOT NULL DEFAULT '',
    `recordLockTimeoutSeconds` int(11)                                                                              NOT NULL DEFAULT 900,
    `deleted`                  enum ('yes','no')                                                                    NOT NULL DEFAULT 'no',
    `fileStats`                varchar(255)                                                                         NOT NULL DEFAULT '',
    `modified`                 datetime                                                                             NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`                  datetime                                                                             NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `name` (`name`),
    KEY `name_deleted` (`name`, `deleted`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 1000;


CREATE TABLE IF NOT EXISTS `FormElement`
(
    `id`                 INT(11)                                                                    NOT NULL AUTO_INCREMENT,
    `formId`             INT(11)                                                                    NOT NULL DEFAULT '0',
    `feIdContainer`      INT(11)                                                                    NOT NULL DEFAULT '0',
    `dynamicUpdate`      ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'no',
    `encryption`         ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'no',
    `encryptionMethod`   ENUM ('Default', 'AES-128','AES-256')                                      NOT NULL DEFAULT 'Default',
    `enabled`            ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'yes',

    `name`               VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `label`              VARCHAR(1023)                                                              NOT NULL DEFAULT '',

    `mode`               ENUM ('show', 'required', 'readonly', 'hidden')                            NOT NULL DEFAULT 'show',
    `modeSql`            TEXT                                                                       NOT NULL DEFAULT '',
    `class`              ENUM ('native', 'action', 'container')                                     NOT NULL DEFAULT 'native',
    `type`               ENUM ('checkbox', 'date', 'datetime', 'dateJQW', 'datetimeJQW', 'extra',
        'gridJQW', 'text', 'editor', 'annotate', 'time', 'note', 'password', 'radio', 'select',
        'subrecord', 'upload', 'chat', 'imageCut', 'fieldset', 'pill', 'templateGroup', 'beforeLoad',
        'beforeSave', 'beforeInsert', 'beforeUpdate', 'beforeDelete', 'afterLoad', 'afterSave',
        'afterInsert', 'afterUpdate', 'afterDelete', 'sendMail', 'paste')                           NOT NULL DEFAULT 'text',
    `subrecordOption`    SET ('edit', 'delete', 'new')                                              NOT NULL DEFAULT '',
    `encode`             ENUM ('none', 'specialchar', 'single tick')                                NOT NULL DEFAULT 'specialchar',
    `checkType`          ENUM ('auto', 'alnumx', 'digit', 'numerical', 'email', 'pattern', 'allbut',
        'all')                                                                                      NOT NULL DEFAULT 'auto',
    `checkPattern`       VARCHAR(255)                                                               NOT NULL DEFAULT '',

    `onChange`           VARCHAR(255)                                                               NOT NULL DEFAULT '',

    `ord`                INT(11)                                                                    NOT NULL DEFAULT '0',
    `tabindex`           INT(11)                                                                    NOT NULL DEFAULT '0',

    `size`               VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `maxLength`          VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `labelAlign`         ENUM ('default', 'left', 'center', 'right')                                NOT NULL DEFAULT 'default',
    `bsLabelColumns`     VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `bsInputColumns`     VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `bsNoteColumns`      VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `rowLabelInputNote`  SET ('row', 'label', '/label', 'input', '/input', 'note', '/note', '/row') NOT NULL DEFAULT 'row,label,/label,input,/input,note,/note,/row',
    `note`               TEXT                                                                       NOT NULL DEFAULT '',
    `adminNote`          TEXT                                                                       NOT NULL DEFAULT '',
    `tooltip`            VARCHAR(255)                                                               NOT NULL DEFAULT '',
    `placeholder`        VARCHAR(2048)                                                              NOT NULL DEFAULT '',

    `value`              TEXT                                                                       NOT NULL DEFAULT '',
    `sql1`               TEXT                                                                       NOT NULL DEFAULT '',
    `parameter`          TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageA` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageB` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageC` TEXT                                                                       NOT NULL DEFAULT '',
    `parameterLanguageD` TEXT                                                                       NOT NULL DEFAULT '',
    `clientJs`           TEXT                                                                       NOT NULL DEFAULT '',

    `deleted`            ENUM ('yes', 'no')                                                         NOT NULL DEFAULT 'no',
    `modified`           DATETIME                                                                   NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`            DATETIME                                                                   NOT NULL DEFAULT current_timestamp(),

    PRIMARY KEY (`id`),
    KEY `formId` (`formId`),
    KEY `formId_class_enabled_deleted` (`formId`, `class`, `enabled`, `deleted`),
    KEY `feIdContainer` (`feIdContainer`),
    KEY `ord` (`ord`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;


CREATE TABLE IF NOT EXISTS `Dirty`
(
    `id`                   int(11)                              NOT NULL AUTO_INCREMENT,
    `sip`                  varchar(255)                         NOT NULL DEFAULT '',
    `tableName`            varchar(255)                         NOT NULL DEFAULT '',
    `recordId`             int(11)                              NOT NULL DEFAULT 0,
    `expire`               datetime                             NOT NULL DEFAULT '0000-00-00 00:00:00',
    `recordHashMd5`        char(32)                             NOT NULL DEFAULT '',
    `tabUniqId`            char(32)                             NOT NULL DEFAULT '',
    `feUser`               varchar(255)                         NOT NULL DEFAULT '',
    `qfqUserSessionCookie` varchar(255)                         NOT NULL DEFAULT '',
    `dirtyMode`            enum ('exclusive','advisory','none') NOT NULL DEFAULT 'exclusive',
    `remoteAddress`        varchar(45)                          NOT NULL DEFAULT '',
    `modified`             datetime                             NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`              datetime                             NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `sip` (`sip`),
    KEY `tableName` (`tableName`),
    KEY `recordId` (`recordId`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;

CREATE TABLE IF NOT EXISTS `MailLog`
(
    `id`       int(11)       NOT NULL AUTO_INCREMENT,
    `grId`     int(11)       NOT NULL DEFAULT 0,
    `xId`      int(11)       NOT NULL DEFAULT 0,
    `xId2`     int(11)       NOT NULL DEFAULT 0,
    `xId3`     int(11)       NOT NULL DEFAULT 0,
    `receiver` text          NOT NULL DEFAULT '',
    `cc`       text          NOT NULL DEFAULT '',
    `bcc`      text          NOT NULL DEFAULT '',
    `sender`   varchar(255)  NOT NULL DEFAULT '',
    `subject`  varchar(255)  NOT NULL DEFAULT '',
    `body`     text          NOT NULL DEFAULT '',
    `header`   varchar(2048) NOT NULL DEFAULT '',
    `attach`   varchar(4096) NOT NULL DEFAULT '',
    `src`      varchar(255)  NOT NULL DEFAULT '',
    `modified` datetime      NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`  datetime      NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;

CREATE TABLE IF NOT EXISTS `FormSubmitLog`
(
    `id`        int(11)      NOT NULL AUTO_INCREMENT,
    `formData`  text         NOT NULL DEFAULT '',
    `sipData`   text         NOT NULL DEFAULT '',
    `clientIp`  varchar(64)  NOT NULL DEFAULT '',
    `feUser`    varchar(64)  NOT NULL DEFAULT '',
    `userAgent` text         NOT NULL DEFAULT '',
    `formId`    int(11)      NOT NULL DEFAULT 0,
    `formName`  varchar(255) NOT NULL DEFAULT '',
    `recordId`  int(11)      NOT NULL DEFAULT 0,
    `pageId`    int(11)      NOT NULL DEFAULT 0,
    `sessionId` varchar(32)  NOT NULL DEFAULT '',
    `created`   timestamp    NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `feUser` (`feUser`),
    KEY `clientIp` (`clientIp`),
    KEY `formId` (`formId`),
    KEY `formName` (`formName`),
    KEY `recordId` (`recordId`),
    KEY `pageId` (`pageId`),
    KEY `createdFeUserFormId` (`created`, `feUser`, `formId`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
