# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
CREATE TABLE IF NOT EXISTS `Clipboard`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `cookie`      varchar(255) NOT NULL DEFAULT '',
    `formIdPaste` int(11)      NOT NULL DEFAULT 0,
    `idSrc`       int(11)      NOT NULL DEFAULT 0,
    `xId`         int(11)      NOT NULL DEFAULT 0,
    `modified`    datetime     NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`     datetime     NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
