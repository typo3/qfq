# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
CREATE TABLE IF NOT EXISTS `Glue`
(
    `id`       int(11)  NOT NULL AUTO_INCREMENT,
    `grId`     int(11)  NOT NULL,
    `xId`      int(11)  NOT NULL,
    `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`  datetime NOT NULL DEFAULT current_timestamp(),

    PRIMARY KEY (`id`),
    KEY `grIdxId` (`grId`, `xId`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
