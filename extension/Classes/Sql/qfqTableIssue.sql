# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# Used to issues and their comments.
CREATE TABLE IF NOT EXISTS `Issue`
(
    `id`                int(11)                     NOT NULL AUTO_INCREMENT,
    `prId`              int(11)                     NOT NULL DEFAULT '0',
    `stId`              int(11)                     NOT NULL DEFAULT '0',
    `mode`              enum ('issue','note')       NOT NULL DEFAULT 'issue',
    `xId`               int(11)                     NOT NULL DEFAULT '0',
    `formId`            int(11)                     NOT NULL DEFAULT '0',
    `formName`          varchar(255)                NOT NULL DEFAULT '',
    `pIdCreator`        int(11)                     NOT NULL DEFAULT '0',
    `pIdAssignee`       int(11)                     NOT NULL DEFAULT '0',
    `pIdContact`        int(11)                     NOT NULL DEFAULT '0',
    `mailContact`       varchar(255)                NOT NULL DEFAULT '',
    `mailWatcherSet`    varchar(1024)               NOT NULL DEFAULT '',
    `parentId`          int(11)                     NOT NULL DEFAULT '0',
    `title`             varchar(255)                NOT NULL DEFAULT '',
    `description`       text                        NOT NULL DEFAULT '',
    `start`             date                        NOT NULL DEFAULT '0000-00-00',
    `due`               date                        NOT NULL DEFAULT '0000-00-00',
    `reminder`          datetime                    NOT NULL DEFAULT '0000-00-00 00:00:00',
    `reminderCount`     int(11)                     NOT NULL DEFAULT '0',
    `estimatedTime`     decimal(4,1)                NOT NULL DEFAULT '0.0',
    `access`            enum ('private','public')   NOT NULL DEFAULT 'private',
    `modified`          datetime                    NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `created`           datetime                    NOT NULL DEFAULT current_timestamp(),

    PRIMARY KEY (`id`),
    KEY `formIdxIdpIdCreator` (`formId`, `xId`, `pIdCreator`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
