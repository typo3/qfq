# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# Used to save uploads per default.
CREATE TABLE IF NOT EXISTS `FileUpload`
(
    `id`           INT(11)      NOT NULL AUTO_INCREMENT,
    `pathFileName` VARCHAR(512) NOT NULL DEFAULT '',
    `grId`         INT(11)      NOT NULL DEFAULT '0',
    `xId`          INT(11)      NOT NULL DEFAULT '0',
    `uploadId`     INT(11)      NOT NULL DEFAULT '0',
    `fileSize`     VARCHAR(32)  NOT NULL DEFAULT '0' COMMENT 'Filesize in bytes',
    `mimeType`     VARCHAR(64)  NOT NULL DEFAULT '',
    `ord`          INT(11)      NOT NULL DEFAULT '0',
    `modified`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `uploadId` (`uploadId`),
    KEY `pathFileNameGrIdXidUploadId` (`pathFileName`, `grId`, `xId`, `uploadId`) USING BTREE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
