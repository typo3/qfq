# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# Used to save chat conversations.
CREATE TABLE IF NOT EXISTS `Chat`
(
    `id`          INT(11)                                          NOT NULL AUTO_INCREMENT,
    `xId`         INT(11)                                          NOT NULL DEFAULT '0',
    `pIdCreator`  INT(11)                                          NOT NULL DEFAULT '0',
    `pIdTeam`     INT(11)                                          NOT NULL DEFAULT '0',
    `cIdTopic`    INT(11)                                          NOT NULL DEFAULT '0',
    `cIdThread`   INT(11)                                          NOT NULL DEFAULT '0',
    `cIdTag`      INT(11)                                          NOT NULL DEFAULT '0',
    `cIdLastRead` INT(11)                                          NOT NULL DEFAULT '0',
    `xGrIdStatus` INT(11)                                          NOT NULL DEFAULT '0',
    `type`        ENUM ('message','topic','tag','reminder','read') NOT NULL DEFAULT 'message',
    `message`     TEXT                                             NOT NULL DEFAULT '',
    `username`    VARCHAR(128)                                     NOT NULL DEFAULT '',
    `emoticon`    VARCHAR(255)                                     NOT NULL DEFAULT '',
    `reference`   VARCHAR(255)                                     NOT NULL DEFAULT '',
    `modified`    DATETIME                                         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`     DATETIME                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `xId` (`xId`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
