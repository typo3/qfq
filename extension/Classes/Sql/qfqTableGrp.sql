# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
# Currently used to save merge rule informations.
CREATE TABLE IF NOT EXISTS `Grp`
(
    `id`        INT(11)           NOT NULL AUTO_INCREMENT,
    `grId`      INT(11)           NOT NULL DEFAULT 0,
    `name`      VARCHAR(127)      NOT NULL DEFAULT '',
    `reference` VARCHAR(255)      NULL,
    `value`     TEXT              NOT NULL DEFAULT '',
    `value2`    TEXT              NOT NULL DEFAULT '',
    `value3`    VARCHAR(255)      NOT NULL DEFAULT '',
    `value4`    VARCHAR(255)      NOT NULL DEFAULT '',
    `comment`   TEXT              NOT NULL DEFAULT '',
    `enabled`   ENUM ('yes','no') NOT NULL DEFAULT 'no',
    `modified`  DATETIME          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`   DATETIME          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE `reference` (`reference`),
    KEY `grIdName` (`grId`, `name`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

# Create groups for merge rules and querys
INSERT INTO Grp (name, reference)
SELECT 'QFQ merge rule', 'qfq_merge_rule'
FROM DUAL
WHERE NOT EXISTS(SELECT id FROM Grp WHERE reference = 'qfq_merge_rule');

INSERT INTO Grp (name, reference)
SELECT 'QFQ merge query', 'qfq_merge_query'
FROM DUAL
WHERE NOT EXISTS(SELECT id FROM Grp WHERE reference = 'qfq_merge_query');