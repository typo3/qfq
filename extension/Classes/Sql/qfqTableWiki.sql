# QFQ tables: Filename has to start with 'qfq' and end with '.sql'
#
CREATE TABLE IF NOT EXISTS `WikiPage`
(
    `id`          INT(11)                           NOT NULL AUTO_INCREMENT,
    `wpIdParent`  INT(11)                                    DEFAULT NULL,
    `pageSlug`    VARCHAR(255)                      NOT NULL DEFAULT '',
    `name`        VARCHAR(255)                      NOT NULL DEFAULT '',
    `content`     TEXT                              NOT NULL DEFAULT '',
    `author`      VARCHAR(64)                       NOT NULL DEFAULT '',
    `roUser`      VARCHAR(512)                      NOT NULL DEFAULT '',
    `roGroup`     VARCHAR(512)                      NOT NULL DEFAULT '',
    `roPublic`    ENUM ('on','off')                 NOT NULL DEFAULT 'off',
    `rwUser`      VARCHAR(512)                      NOT NULL DEFAULT '',
    `rwGroup`     VARCHAR(512)                      NOT NULL DEFAULT '',
    `rwPublic`    ENUM ('on','off')                 NOT NULL DEFAULT 'off',
    `pageLock`    VARCHAR(64)                       NOT NULL DEFAULT '',
    `imageBorder` ENUM ('none','1pxblack','shadow') NOT NULL DEFAULT 'none',
    `wpIdCurrent` INT(11)                                    DEFAULT NULL,
    `modified`    DATETIME                          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`     DATETIME                          NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;

CREATE TABLE IF NOT EXISTS `WikiAttachment`
(
    `id`           INT(11)      NOT NULL AUTO_INCREMENT,
    `wpId`         INT(11)      NOT NULL DEFAULT '0',
    `feUser`       VARCHAR(64)  NOT NULL DEFAULT '',
    `pathFileName` VARCHAR(512) NOT NULL DEFAULT '',
    `modified`     DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created`      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
