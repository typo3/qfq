<?php
/**
 * @author Carsten Rose <carsten.rose@math.uzh.ch>
 */

$EM_CONF['qfq'] = array(
    'title' => 'Quick Form Query',
    'description' => 'Framework to build web applications: Form (dynamic), report, typeahead, multi language, link protection, PDF, send mail (dynamic attachments, PDFs), multiple databases, record locking, secure up/download.',
    'category' => 'fe',
    'author' => 'Carsten Rose, Benjamin Baer, Enis Nuredini, Jan Haller',
    'author_email' => 'carsten.rose@math.uzh.ch',
    'dependencies' => 'fluid,extbase',
    'clearcacheonload' => true,
    'state' => 'stable',
    'version' => '24.12.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.0.0-11.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
);

