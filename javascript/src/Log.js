/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global console */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{level: number, message: Function, debug: Function, warning: Function, error: Function}}
     *
     * @name QfqNS.Log
     */
    n.Log = {
        level: 3,
        message: function (msg) {
            if (this.level <= 0) {
                console.log('[message] ' + msg);
            }
        },
        debug: function (msg) {
            if (this.level <= 1) {
                console.log('[debug] ' + msg);
            }
        },
        warning: function (msg) {
            if (this.level <= 2) {
                console.log('[warning] ' + msg);
            }
        },
        error: function (msg) {
            if (this.level <= 3) {
                console.log('[error] ' + msg);
            }
        }

    };
})(QfqNS);