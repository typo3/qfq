/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */
/* global EventEmitter */

/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Handle file deletion.
     *
     * Upon instantiation, it attaches to all `<button>` elements having the class `delete-file`, within the elements
     * selected by the `formSelector`.
     *
     * @param formSelector jQuery selector of the `<form>` this instance is responsible for.
     * @param targetUrl endpoint URL to send delete requests to.
     * @constructor
     *
     * @name QfqNS.FileDelete
     */
    n.FileDelete = function (formSelector, targetUrl) {
        this.formSelector = formSelector;
        this.targetUrl = targetUrl;
        this.eventEmitter = new EventEmitter();

        this.setupOnClickHandler();
    };

    n.FileDelete.prototype.on = n.EventEmitter.onMixin;

    n.FileDelete.prototype.setupOnClickHandler = function () {
        $(this.formSelector + " button.delete-file").click(this.buttonClicked.bind(this));
    };

    n.FileDelete.prototype.buttonClicked = function (event) {
        event.preventDefault();
        var alert = new n.Alert({
            message: "Do you want to delete the file?",
            type: "warning",
            modal: true,
            buttons: [
                {label: "OK", eventName: "ok"},
                {label: "Cancel", eventName: "cancel", focus: true}
            ]
        });
        alert.on('alert.ok', function () {
            this.performFileDelete(event);
        }.bind(this));
        alert.show();
    };

    n.FileDelete.prototype.performFileDelete = function (event) {
        this.eventEmitter.emitEvent('filedelete.started', n.EventEmitter.makePayload(event.delegateTarget, null));

        var data = this.prepareData(event.delegateTarget);

        $.ajax({
            url: this.targetUrl,
            type: 'POST',
            data: data,
            cache: false
        })
            .done(this.ajaxSuccessHandler.bind(this, event.delegateTarget))
            .fail(this.ajaxErrorHandler.bind(this, event.delegateTarget));
    };

    n.FileDelete.prototype.prepareData = function (htmlButton) {
        if (!htmlButton.hasAttribute("name")) {
            throw new Error("File delete button element requires 'name' attribute");
        }

        var fileDeleteName = htmlButton.getAttribute('name');

        var data = {
            s: $(htmlButton).data('sip'),
            name: fileDeleteName
        };

        return data;
    };

    n.FileDelete.prototype.ajaxSuccessHandler = function (uploadTriggeredBy, data, textStatus, jqXHR) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, data, {
            textStatus: textStatus,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('filedelete.delete.successful', eventData);
        this.eventEmitter.emitEvent('filedelete.ended', eventData);
    };

    n.FileDelete.prototype.ajaxErrorHandler = function (uploadTriggeredBy, jqXHR, textStatus, errorThrown) {
        var eventData = n.EventEmitter.makePayload(uploadTriggeredBy, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        });
        this.eventEmitter.emitEvent('filedelete.delete.failed', eventData);
        this.eventEmitter.emitEvent('filedelete.ended', eventData);
    };


})(QfqNS);