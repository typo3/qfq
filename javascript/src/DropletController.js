/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */
/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Droplet.js */
/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
(function (n) {
    'use strict';

    n.DropletController = function() {
        this.droplets = [];
        this.eventEmitter = new EventEmitter();
    };

    n.DropletController.prototype.setUpDroplets = function() {
        var that = this;

        $(".qfq-droplet").each(function() {
            var url = false;
            var color = "grey";
            if ($(this).data("content")) {
                url = $(this).data("content");
            }
            if ($(this).data("color")) {
                color = $(this).data("color");
            }
            var droplet = new QfqNS.Droplet(url, color);
            droplet.setTrigger($(this));
            droplet.setContainer(droplet.createContainerBellowTrigger());

            that.droplets.push(droplet);
            droplet.on('droplet.toggle', function() { that.hideDroplets(); });
        });
    };

    n.DropletController.prototype.getDroplet = function(reference) {
        if (reference < this.droplets.length && reference >= 0) {
            return this.droplets[reference];
        }
    };

    n.DropletController.prototype.hideDroplets = function() {
        for(var i=0; i < this.droplets.length; i++) {
            this.droplets[i].hideDroplet();
        }
    };

})(QfqNS);