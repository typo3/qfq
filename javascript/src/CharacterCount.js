/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.CharacterCount = {};

    /**
     * Initialize character count.
     *
     * Character count keeps track of characters in `<input>` and `<textarea>` elements (tracked elements). By default,
     * all elements having the `qfq-character-count` class are initialized. When non-`<input>/<textarea>` elements are
     * encountered during initialization, the behavior is undefined.
     *
     * Each element eligible for character count must provide a `data-character-count-id` attribute holding the element
     * id of the element receiving the character count. The receiving element's text is replaced by the current number
     * of characters of the tracked element. The number of characters in a tracked element is updated in the receiving
     * element upon a `change` or `keyup` event.
     *
     * If the `maxlength` attribute is present on the tracked element, the receiving element will display
     *
     *     N/<maxlength>
     *
     * where `N` is the current number of characters of the tracked element. If `maxlength` is not present, the
     * receiving element will display
     *
     *     N/∞
     *
     * where `N` is the current number of characters of the tracked element.
     *
     * @param selector {string} optional selector. Defaults to `.qfq-character-count`.
     */
    n.CharacterCount.initialize = function (selector) {
        selector = selector || ".qfq-character-count";
        $(selector).each(function () {
            var characterCountTarget, $targetElement;

            var $element = $(this);

            characterCountTarget = "#" + n.CharacterCount.getCharacterCountTargetId($element);

            $element.data('character-count-display', $(characterCountTarget));

            n.CharacterCount.updateCountForElement($element);

            $element.on('change keyup', function (evt) {
                n.CharacterCount.updateCountForElement($(evt.delegateTarget));
            });

        });
    };

    n.CharacterCount.updateCountForElement = function ($targetElement) {
        var maxLength = $targetElement.attr('maxlength') || "∞";
        var currentLength = $targetElement.val().length;
        $targetElement.data('character-count-display').text(currentLength + "/" + maxLength);
    };

    n.CharacterCount.getCharacterCountTargetId = function ($element) {
        return $element.data('character-count-id');
    };

})(QfqNS);

