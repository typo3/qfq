/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */
/* global console */

var QfqNS = QfqNS || {};

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
(function (n) {
    'use strict';

    /**
     *
     * @param deleteUrl
     * @constructor
     *
     * @name QfqNS.QfqRecordList
     */
    n.QfqRecordList = function (deleteUrl) {
        console.log("initialized with this url", deleteUrl);
        this.deleteUrl = deleteUrl;
        this.deleteButtonClass = 'record-delete';
        this.recordClass = 'record';
        this.sipDataAttribute = 'sip';

        this.connectClickHandler();
    };

    /**
     * @private
     */
    n.QfqRecordList.prototype.connectClickHandler = function () {
        var that = this;
        var specificClass = 'record-delete'; // Replace this with the class you want to check

        $("." + this.deleteButtonClass).on('click', function (event) {
            // Check if the event is triggered by the "Enter" key
            var enterKeyTriggered = event.originalEvent.detail === 0;

            // Check if the delete button is focused
            var isButtonFocused = $(event.target).is(":focus");

            // If the event is triggered by the "Enter" key and the delete button has the specific class, stop the event
            if (enterKeyTriggered && !isButtonFocused && $(event.target).hasClass(specificClass)) {
                event.preventDefault();
                event.stopPropagation();
                return;
            }

            // If the event is not stopped, call the handleDeleteButtonClick method
            that.handleDeleteButtonClick(event);
        });
    };

    n.QfqRecordList.prototype.handleDeleteButtonClick = function (event) {
        var $eventTarget = $(event.delegateTarget);
        var $recordElement = this.getRecordElement(event.target);

        if ($recordElement.length !== 1) {
            throw new Error($recordElement.length + ' match(es) found for record class');
        }

        var sip = $eventTarget.data(this.sipDataAttribute);

        if (!sip) {
            throw new Error('No `sip` on delete button');
        }


        var alert = new n.Alert({
            message: "Do you really want to delete the record?",
            type: "warning",
            modal: true,
            buttons: [
                {label: "Yes", eventName: "ok"},
                {label: "No", eventName: "cancel", focus: true}
            ]
        });
        var that = this;
        alert.on('alert.ok', function () {
            $.post(that.deleteUrl + "?s=" + sip)
                .done(that.ajaxDeleteSuccessDispatcher.bind(that, $recordElement))
                .fail(n.Helper.showAjaxError);
        });
        alert.show();
    };

    /**
     *
     * @param $recordElement
     * @param data
     * @param textStatus
     * @param jqXHR
     *
     * @private
     */
    n.QfqRecordList.prototype.ajaxDeleteSuccessDispatcher = function ($recordElement, data, textStatus, jqXHR) {
        if (!data.status) {
            throw new Error("No 'status' property 'data'");
        }

        switch (data.status) {
            case "error":
                this.handleLogicDeleteError(data);
                break;
            case "success":
                this.handleDeleteSuccess($recordElement, data);
                break;
            default:
                throw new Error("Status '" + data.status + "' unknown.");
        }
    };

    n.QfqRecordList.prototype.handleDeleteSuccess = function ($recordElement, data) {
        if (data.redirect && data.redirect === "url" && data['redirect-url']) {
            window.location = data['redirect-url'];
            return;
        }
        if (data.redirect && data.redirect === "no") {
            var alert = new n.Alert("redirect=='no' not allowed", "error");
            alert.show();
        }

        var info = new n.Alert("Record successfully deleted", "info");
        info.timeout = 1500;
        info.show();
        $recordElement.fadeOut(function () {
            $recordElement.remove();
        });
    };

    n.QfqRecordList.prototype.getRecordElement = function (element) {
        return $(element).closest('.' + this.recordClass);
    };

    /**
     *
     * @param data
     *
     * @private
     */
    n.QfqRecordList.prototype.handleLogicDeleteError = function (data) {
        if (!data.message) {
            throw Error("Status is 'error' but required 'message' attribute is missing.");
        }
        var alert = new n.Alert(data.message, "error");
        alert.show();
    };


})(QfqNS);