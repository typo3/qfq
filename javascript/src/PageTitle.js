/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{set: Function, get: Function, setSubTitle: Function}}
     *
     * @name QfqNS.PageTitle
     */
    n.PageTitle = {
        set: function (title) {
            if (title !== null) {
                document.title = title;
            }
        },
        get: function () {
            return document.title;
        },
        setSubTitle: function (subTitle) {
            var currentTitle = this.get();
            var subtitleStrippedOff = currentTitle.replace(/ - (.*)$/, '');
            document.title = subtitleStrippedOff + " - (" + subTitle + ")";
        }
    };
})(QfqNS);