/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages Text Editor for Comments
     */
    n.LocalStorage = function (key) {
        this.storage = {};
        this.key = key;
        this.storage[this.key] = {};

        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
        this._read();
        if (this.storage[this.key] !== "undefined") {
            this.storage[this.key] = {};
        }
    };

    n.LocalStorage.prototype.on = n.EventEmitter.onMixin;

    n.LocalStorage.prototype._read = function() {
        var o = JSON.parse(localStorage.getItem("qfq"));
        if(o) {
            this.storage = o;
        }
    };

    n.LocalStorage.prototype._write = function() {
        localStorage.setItem("qfq", JSON.stringify(this.storage));
        console.log(localStorage.getItem("qfq"));
    };

    n.LocalStorage.prototype.get = function(key) {
        if (this.storage[this.key][key] !== "undefined") {
            console.log(this.storage);
            return this.storage[this.key][key];
        } else {
            return false;
        }
    };

    n.LocalStorage.prototype.set = function(key, object) {
        if (this.storage[this.key][key] === "undefined") {
            this.storage[this.key][key] = {};
        }
        if(object) {
            this.storage[this.key][key] = object;
            this._write();
        }
    };

    n.LocalStorage.prototype.update = function() {
        this._read();
    };


}(QfqNS));