/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Form Group represents a `<input>/<select>` element including the label and help block.
     *
     * It is not meant to be used directly. Use the specialized objects instead.
     *
     * The parameter `elementSelector` seems to be redundant, but `$enclosedElement` can be any element enclosed in
     * the form group, thus we need a way to identify the actual form element(s).
     *
     * @param $enclosedElement {jQuery} a jQuery object contained in the Form Group. It used to find the enclosing
     * HTML element having the `.form-group` class assigned.
     *
     * @param elementSelector {string} jQuery selector selecting the form element. It is used to find all elements
     * contained in the form group identified by $enclosedElement.
     *
     * @constructor
     * @name QfqNS.Element.FormGroup
     */
    n.FormGroup = function ($enclosedElement, elementSelector) {
        elementSelector = elementSelector || 'input:not([type="hidden"])';
        if (!$enclosedElement || $enclosedElement.length === 0) {
            throw new Error("No enclosed element");
        }

        this.$formGroup = this.$findFormGroup($enclosedElement);
        this.$element = this.$formGroup.find(elementSelector);
        this.$label = this.$formGroup.find('.control-label');
        this.$helpBlock = this.$formGroup.find(".help-block");
    };

    /**
     * Test if the Form Group is of the given type
     *
     * @param {string} type type name
     * @returns {boolean} true if the Form Group is of the given type. False otherwise
     * @protected
     */
    n.FormGroup.prototype.isType = function (type) {
        var lowerCaseType = type.toLowerCase();
        var isOfType = false;
        this.$element.each(function () {
            if (this.hasAttribute('type')) {
                if (this.getAttribute('type') === lowerCaseType) {
                    isOfType = true;
                    return true;
                } else {
                    isOfType = false;
                    return false;
                }
            } else {
                // <select> is not an attribute value, obviously, so check for nodename
                if (this.nodeName.toLowerCase() === lowerCaseType) {
                    isOfType = true;
                    return true;
                } else if (lowerCaseType === 'text') {
                    isOfType = true;
                    return true;
                } else {
                    isOfType = false;
                    return false;
                }
            }
        });

        return isOfType;
    };

    /**
     *
     * @param $enclosedElement
     * @returns {*}
     *
     * @private
     */
    n.FormGroup.prototype.$findFormGroup = function ($enclosedElement) {
        var idArray = $enclosedElement.attr('id').split("-");
        var searchString = "#";
        for(var i = 0; i < 8 && i < idArray.length; i++) {
            // Handle form-group for chat input element
            if (idArray[i] === 'chat') {
                continue;
            }
            searchString += idArray[i] + "-";
        }
        var $formGroup = $(searchString + 'i');
        
        if (!$formGroup || $formGroup.length === 0) {
            console.log("Unable to find Form Group for", $enclosedElement);
            console.log("trying with: " + '#' + $enclosedElement.attr('id') + '-i');
            throw new Error("Unable to find Form Group for", $enclosedElement);
        }

        if ($formGroup.length > 1) {
            $formGroup = $(searchString + 'i');
            console.log("Enclosed Element Id: " + $enclosedElement.attr('id'));
            if ($formGroup.length !== 1) {
                throw new Error("enclosed element yields ambiguous form group");
            }
        }

        return $formGroup;
    };

    /**
     * @public
     * @returns {boolean}
     */
    n.FormGroup.prototype.hasLabel = function () {
        return this.$label.length > 0;
    };

    /**
     * @public
     * @returns {boolean}
     */
    n.FormGroup.prototype.hasHelpBlock = function () {
        return this.$helpBlock.length > 0;
    };

    /**
     * @deprecated
     *
     * Read-only is mapped onto setEnabled(). We do not distinguish between those two.
     *
     * @param readonly
     * @public
     */
    n.FormGroup.prototype.setReadOnly = function (readonly) {
        this.setEnabled(!readonly);
    };

    /**
     * @public
     * @param enabled
     */
    n.FormGroup.prototype.setEnabled = function (enabled) {
        this.$element.prop('disabled', !enabled);

        if (enabled) {
            //this.$formGroup.removeClass("text-muted");
            //this.$label.removeClass("disabled");
            this.$element.parents("div.radio").removeClass("disabled");
        } else {
            //this.$formGroup.addClass("text-muted");
            //this.$label.addClass("disabled");
            this.$element.parents("div.radio").addClass("disabled");
        }
    };

    /**
     * @public
     * @param hidden
     */
    n.FormGroup.prototype.setHidden = function (hidden) {
        if (hidden) {
            this.$formGroup.addClass("hidden");
        } else {
            this.$formGroup.removeClass("hidden");
        }
    };

    /**
     * @public
     * @param required
     */
    n.FormGroup.prototype.setRequired = function (required) {
        if(this.$element.is('div')) {
            if (!!this.$element.find('input')) {
                console.log("children found");
                this.$element.find('input').prop('required', required);
            }
        } else {
            this.$element.prop('required', required);
        }
    };

    /**
     * @public
     * @param isError
     */
    n.FormGroup.prototype.setError = function (isError) {
        if (isError) {
            this.$formGroup.addClass("has-error has-danger");
        } else {
            this.$formGroup.removeClass("has-error has-danger");
        }
    };

    n.FormGroup.prototype.setHelp = function (help) {
        if (!this.hasHelpBlock()) {
            return;
        }

        this.$helpBlock.text(help);
    };

    n.FormGroup.prototype.clearHelp = function () {
        if (!this.hasHelpBlock()) {
            return;
        }

        this.$helpBlock.empty();
    };

})(QfqNS.Element);