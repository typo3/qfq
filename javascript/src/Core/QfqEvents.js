/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global EventEmitter */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @type {{makePayload: Function, onMixin: Function}}
     *
     */
    n.EventEmitter = {
        makePayload: function (target, data, additionalArgs) {
            return [$.extend({},
                typeof additionalArgs === "object" ? additionalArgs : null,
                {
                    target: target,
                    data: data
                }
            )];
        },
        onMixin: function (event, func) {
            this.eventEmitter.addListener(event, func);
        }
    };

})(QfqNS);