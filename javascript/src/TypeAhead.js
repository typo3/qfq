/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */
/* global Bloodhound */
/* global Math */

/* @depend Utils.js */

var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.TypeAhead = {};

    /**
     * Coerce corejs-typeahead into our use-case.
     *
     * @param typeahead_endpoint the endpoint to be called
     * @constructor
     */
    n.TypeAhead.install = function (typeahead_endpoint) {

        $('.qfq-typeahead:not(.tt-hint,.tt-input,.typeahead-installed)').each(function () {
            var bloodhoundConfiguration;

            var $element = $(this);

            if (typeahead_endpoint === null) {
                typeahead_endpoint = n.TypeAhead.getTypeAheadUrl($element);
            }

            // bloodhound is used to get the remote data (suggestions)
            bloodhoundConfiguration = {
                // We need to be notified on success, so we need a promise
                initialize: false,
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('key', 'value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (obj) {
                    return obj.key;
                },
                remote: {
                    url: n.TypeAhead.makeUrl(typeahead_endpoint, $element),
                    wildcard: '%QUERY'
                }
            };

            var url = n.TypeAhead.makeUrl(typeahead_endpoint, $element);
            url = url.replace('%QUERY', '');
            console.log(url);
            // Seems to trigger an empty query - why?
            //$.getJSON(url, {}, console.log);  // API by hand

            // initialize typeahead (either with or without tags)
            if ($element.data('typeahead-tags')) {
                n.TypeAhead.installWithTags($element, bloodhoundConfiguration);
            } else {
                n.TypeAhead.installWithoutTags(typeahead_endpoint, $element, bloodhoundConfiguration);
            }

            $element.addClass('typeahead-installed');
        });

    };

    n.TypeAhead.installWithTags = function ($element, bloodhoundConfiguration) {

        // initialize bloodhound (typeahead suggestion engine)
        var suggestions = new Bloodhound(bloodhoundConfiguration);
        suggestions.initialize();

        // create actual input field
        var $inputField = $('<input/>', {
            type: 'text',
            class: $element.attr('class')
        });
        $element.after($inputField);

        let inputContainer = $(`#` + $element.attr('id') + '-i');
        if (!inputContainer.length) {
            console.warn('Input container not found:', inputContainer);
        }

        // Function to sync `disabled`, `readonly`, and `background` styles
        function syncAttributes() {
            // Ensure the latest state of `disabled` is synced
            if ($element.is('[disabled]') || inputContainer.is('[disabled]')) {
                $inputField.attr('disabled', true);
            } else {
                $inputField.removeAttr('disabled');
            }

            // Ensure the latest state of `readonly` is synced
            if ($element.is('[readonly]') || inputContainer.is('[readonly]')) {
                $inputField.attr('readonly', true);
            } else {
                $inputField.removeAttr('readonly');
            }

            // Ensure the background style is set correctly
            if ($element.is('[disabled]') || $element.is('[readonly]') || inputContainer.is('[disabled]') || inputContainer.is('[readonly]')) {
                $inputField.css('background', '');
                $inputField.css('background-color', '');
                $inputField.css('background-color', '#eee !important');
                $inputField.css('cursor', 'not-allowed')
            } else {
                $inputField.css('background-color', '');
                $inputField.css('cursor', '')
            }
        }

        // Ensure attributes are applied correctly before observing changes
        syncAttributes();

        // Set up a MutationObserver to handle dynamic changes
        const observer = new MutationObserver(function (mutationsList) {
            let attributeChanged = false;
            for (const mutation of mutationsList) {
                if (mutation.attributeName === 'disabled' || mutation.attributeName === 'readonly') {
                    attributeChanged = true; // Detect relevant attribute changes
                }
            }

            if (attributeChanged) {
                syncAttributes(); // Sync
            }
        });

        // Observe changes on $element
        // Optionally observe `inputContainer` if it exists
        observer.observe($element[0], { attributes: true });
        if (inputContainer.length) {
            observer.observe(inputContainer[0], { attributes: true });
        }

        // prevent form submit when enter key is pressed
        $inputField.off('keyup');
        $inputField.on('keypress keyup', function (e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                e.preventDefault();
                return false;
            }
        });

        $element.off('keyup change');
        $element.on('keypress keyup change', function (e) {
            var code = e.keyCode || e.which;
            if (code === 13) {
                e.preventDefault();
                return false;
            }
        });


        // list to keep tracks of existing tags and those added during the current session
        // expected JSON format: [{value: "Alaska", key: "AK"}, {value: "Alabama", key: "AL"}]
        var existingTags = $element.val() !== '' ? JSON.parse($element.val()) : [];

        // list of current typeahead suggestions
        var typeaheadList = existingTags.slice();

        // get list of possible keys a user can press to push a tag (list of keycodes)
        var delimiters = $element.data('typeahead-tag-delimiters');
        delimiters = delimiters !== undefined ? delimiters : [9, 13, 44];

        // validator function for pedantic mode
        var pedanticValidator = function (tag) {
            // check if tag is in typeahead suggestions
            var tagLookup = typeaheadList.filter(function (t) {return t.value.toLowerCase() === tag.toLowerCase();})[0];
            return tagLookup !== undefined;
        };

        // initialize tagsManager
        var tagApi = $inputField.tagsManager({
            deleteTagsOnBackspace: false,
            hiddenTagListName: '',
            tagClass: 'qfq-typeahead-tag',
            delimiters: delimiters,
            validator: !!$element.data('typeahead-pedantic') ? pedanticValidator : null,
        });

        // when tag is pushed, look up key and add it to existingTags
        tagApi.bind('tm:pushed', function (e, tag) {
            if(tag === "") return;
            var tagLookup = typeaheadList.filter(function (t) {return t.value.toLowerCase() === tag.toLowerCase();})[0];
            if (undefined === tagLookup) {
                existingTags.push({key: 0, value: tag});
            } else {
                existingTags.push({key: tagLookup.key, value: tagLookup.value});
            }
            // Only change where the tag get inserted if the parent is a 'input-group' (extraButtonInfo/Lock/Password)
            const parent = $element.parent(); // Get the parent of $element
            if (parent.hasClass('input-group')){
                const tagElement = parent.find('.tm-tag').last(); // Get the last added tag
                tagElement.detach().insertBefore(parent); // Move the tag before the parent
            }
        });

        // when the hidden input field changes, overwrite value with tag object list
        tagApi.bind('tm:hiddenUpdate', function (e, tags) {
            var tagObjects = $.map(tags, function (t) {
                return existingTags.filter(function (tt) {return tt.value === t;})[0];
            });
            $element.val(JSON.stringify(tagObjects));
        });

        // if value of hidden field is changed externally, update tagManager
        $element.on('qfqChange', function () {
            tagApi.tagsManager('disableHiddenUpdate', true);
            existingTags = $element.val() !== '' ? JSON.parse($element.val()) : [];
            tagApi.tagsManager('empty');
            $.each(existingTags, function (i, tag) {
               tagApi.tagsManager('pushTag', tag.value);
            });
            tagApi.tagsManager('disableHiddenUpdate', false);
        });

        // add existing tags
        tagApi.tagsManager('disableHiddenUpdate', true);
        $.each(existingTags, function (i, tag) {
            tagApi.tagsManager('pushTag', tag.value);
        });
        tagApi.tagsManager('disableHiddenUpdate', false);

        console.log(JSON.parse(JSON.stringify(n.TypeAhead.getMinLength($element))));

        function suggestionsWithDefaults(q, sync, async) {
            if (q === '') {
                sync(suggestions.index.all().slice(0, 2)); // slice(start,end)
                // suggestions.search('', sync, async);
            }
            else {
                suggestions.search(q, sync, async);
            }
        }
        $inputField.data('bloodhound', suggestions);

        // add typahead
        $inputField.typeahead({
                // options
                hint: n.TypeAhead.getHint($element),
                highlight: n.TypeAhead.getHighlight($element),
                minLength: n.TypeAhead.getMinLength($element)
            }, {
                display: 'value',
                source: suggestionsWithDefaults,
                limit: n.TypeAhead.getLimit($element),
                templates: {
                    suggestion: function (obj) {
                        return "<div>" + n.TypeAhead.htmlEncode(obj.value) + "</div>";
                    },
                    // No message if field is not set to pedantic.
                    notFound: (function ($_) {
                        return function (obj) {
                            if (!!$element.data('typeahead-pedantic'))
                                return "<div>'" + n.TypeAhead.htmlEncode(obj.query) + "' not found";
                        };
                    })($inputField)
            }
        });

        // directly add tag when clicked on in typahead menu
        $inputField.bind('typeahead:selected', function (event, sugg) {
            tagApi.tagsManager("pushTag", sugg.value);
        });

        // update typahead list when typahead changes
        $inputField.bind('typeahead:render', function (event, sugg) {
            typeaheadList.length = 0;
            typeaheadList.push.apply(typeaheadList, sugg);
        });
    };

    n.TypeAhead.installWithoutTags = function (typeahead_endpoint, $element, bloodhoundConfiguration) {
        var $shadowElement;

        // Prefetch the value that is already in the field
        if ($element.val() !== '') {
            bloodhoundConfiguration.prefetch = {};
            bloodhoundConfiguration.prefetch.url = n.TypeAhead.makePrefetchUrl(typeahead_endpoint, $element.val(), $element);
            // Disable cache, we expect only a few entries. Caching gives sometimes strange behavior.
            bloodhoundConfiguration.prefetch.cache = false;
        }

        // create a shadow element with the same value. This seems to be important for the pedantic mode. (?)
        $shadowElement = n.TypeAhead.makeShadowElement($element);

        // prefetch data
        var suggestions = new Bloodhound(bloodhoundConfiguration);
        var promise = suggestions.initialize();

        // use shadow element to back fill field value, if it is in the fetched suggestions (why?)
        promise.done((function ($element, suggestions) {
            return function () {
                n.TypeAhead.fillTypeAheadFromShadowElement($element, suggestions);
            };
        })($element, suggestions));

        $element.typeahead({
                // options
                hint: n.TypeAhead.getHint($element),
                highlight: n.TypeAhead.getHighlight($element),
                minLength: n.TypeAhead.getMinLength($element)
            },
            {
                // dataset
                display: 'value',
                source: suggestions,
                limit: n.TypeAhead.getLimit($element),
                templates: {
                    suggestion: function (obj) {
                        return "<div>" + n.TypeAhead.htmlEncode(obj.value) + "</div>";
                    },
                    // No message if field is not set to pedantic.
                    notFound: (function ($_) {
                        return function (obj) {
                            if (!!$_.data('typeahead-pedantic'))
                                return "<div>'" + n.TypeAhead.htmlEncode(obj.query) + "' not found";
                        };
                    })($element)
                }
            });

        $element.css('background-color', '')
        // bind select and autocomplete events
        $element.bind('typeahead:select typeahead:autocomplete', function (event, suggestion) {
            var $shadowElement = n.TypeAhead.getShadowElement($(event.delegateTarget));
            $shadowElement.val(suggestion.key);
        });

        // bind change event
        if (!!$element.data('typeahead-pedantic')) {
            // Typeahead pedantic: Only allow suggested inputs
            $element.bind('typeahead:change', n.TypeAhead.makePedanticHandler(suggestions));
            $element.on('keydown', (function (suggestions) {
                return function (event) {
                    if (event.which === 13) {
                        n.TypeAhead.makePedanticHandler(suggestions)(event);
                    }
                };
            })(suggestions));
            // The pedantic handler will test if the shadow element has a value set (the KEY). If not, the
            // typeahead element is cleared. Thus we have to guarantee that no value exists in the shadow
            // element the instant the user starts typing since we don't know the outcome of the search.
            //
            // If we don't clear the shadow element the instant the user starts typing, and simply let the
            // `typeahead:select` or `typeahead:autocomplete` handler set the selected value, the
            // user might do following steps and end up in an inconsistent state:
            //
            //  1. Use typeahead to select/autocomplete a suggestion
            //  2. delete the suggestion
            //  3. enter a random string
            //  4. submit form
            //
            // This would leave a stale value in the shadow element (from step 1.), and the pedantic handler
            // would not clear the typeahead element, giving the impression the value in the typeahead element will be submitted.
            $element.on('input', (function ($shadowElement) {
                return function () {
                    $shadowElement.val('');
                };
            })($shadowElement));
        } else {
            $element.bind('typeahead:change', function (event, suggestion) {
                var $shadowElement = n.TypeAhead.getShadowElement($(event.delegateTarget));
                // If none pendatic, suggestion key might not exist, so use suggestion instead.
                $shadowElement.val(suggestion.key || suggestion);
            });
        }
    };


    n.TypeAhead.makePedanticHandler = function (bloodhound) {
        return function (event) {
            var $typeAhead = $(event.delegateTarget);
            var $shadowElement = n.TypeAhead.getShadowElement($typeAhead);
            if ($shadowElement.val() === '') {
                $typeAhead.typeahead('val', '');

                // This triggers after saving and causes unnecessary dialog window (do you really want to leave...)
                //$typeAhead.closest('form').change();
            }
        };
    };

    n.TypeAhead.makeUrl = function (endpoint, element) {
        return endpoint + "?_ta_query=%QUERY" + "&sip=" + n.TypeAhead.getSip(element);
    };
    n.TypeAhead.makePrefetchUrl = function (endpoint, prefetchKey, element) {
        return endpoint + "?_ta_prefetch=" + encodeURIComponent(prefetchKey) + "&sip=" + n.TypeAhead.getSip(element);
    };

    n.TypeAhead.getLimit = function ($element) {
        return $element.data('typeahead-limit');
    };

    n.TypeAhead.getSip = function ($element) {
        return $element.data('typeahead-sip');
    };

    n.TypeAhead.getName = function ($element) {
        return $element.attr('name');
    };

    n.TypeAhead.getValue = function ($element) {
        return $element.val();
    };

    n.TypeAhead.getMinLength = function ($element) {
        return $element.data('typeahead-minlength') !== undefined ? $element.data('typeahead-minlength') : 2;
    };

    n.TypeAhead.getHighlight = function ($element) {
        return $element.data('typeahead-highlight') || true;
    };

    n.TypeAhead.getHint = function ($element) {
        return $element.data('typeahead-hint') || true;
    };

    n.TypeAhead.htmlEncode = function (value) {
        return $('<div/>').text(value).html();
    };

    n.TypeAhead.makeShadowElement = function ($element) {
        var $parent, inputName, inputValue, uniqueId, $shadowElement;

        $parent = $element.parent();
        inputName = $element.attr('name');
        $element.removeAttr('name');

        inputValue = $element.val();

        $shadowElement = $('<input>')
            .attr('type', 'hidden')
            .attr('name', inputName)
            .val(inputValue);

        $element.data('shadow-element', $shadowElement);

        $parent.append($shadowElement);

        return $shadowElement;
    };

    n.TypeAhead.getShadowElement = function ($element) {
        return $element.data('shadow-element');
    };

    n.TypeAhead.fillTypeAheadFromShadowElement = function ($element, bloodhound) {
        var results;
        var $shadowElement = n.TypeAhead.getShadowElement($element);
        var key = $shadowElement.val();
        if (key === '') {
            return;
        }

        results = bloodhound.get(key);
        if (results.length === 0) {
            return;
        }
        $element.typeahead('val', results[0].value);
    };

    n.TypeAhead.getTypeAheadUrl = function ($element) {
        return $element.data('typeahead-url');
    };
})(QfqNS);

// fix for safari to make the right input field clickable. Safari doesn't get the right sequence of z-index from the two typeahead input fields which are overlaid.
// z-index can not be set in qfq because the input field is generated by typeahead.bundle.min.js, changes are needed to do at the end of DOM after everything is loaded.
$(window).on("load",function() {
    $(document).ready(function(){
        $(".tt-input").each(function(){
            if($(this).css("z-index") !== "auto"){
                $(this).prev().css("z-index",1);
            }
        });
    });
});