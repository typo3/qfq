/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Highlights Syntax with provided JSON
     *
     */
    n.SyntaxHighlighter = function () {
        this.highlightInstructions = {};
        this.uri = "";
        this.importDone = false;
        this.waitingForEnd = false;
        this.multiLineClass = "";
        this.line = '';
    };

    n.SyntaxHighlighter.prototype.importInstructions = function(json, callbackFn) {
        var that = this;
        console.log("Import instructions: " + json);
        $.getJSON(json, function(data) {
            that.highlightInstructions = data;
            that.importDone = true;
            if (callbackFn && typeof(callbackFn) === "function") {
                console.log("callback found");
                callbackFn();
            }
        });
    };

    n.SyntaxHighlighter.prototype.setLanguageUri = function(uri) {
        this.uri = uri;
    };


    n.SyntaxHighlighter.prototype.highlightLine = function(line) {
        this.line = line;
        if(!this.waitingForEnd) {
            if (this._multiLineHighlight()) {
                return this.line;
            }
            this._wordHighlight();
        } else {
            if (this._multiLineHighlight()) {
                return this.line;
            } else {
                this.line = this.wrapLine(this.multiLineClass, this.line);
                return this.line;
            }
        }
        return this.line;
    };

    n.SyntaxHighlighter.prototype._wordHighlight = function() {
        for (var i = 0; i < this.highlightInstructions.singleWord.length; i++) {
            var word = this.highlightInstructions.singleWord[i];
            var regex = new RegExp(word.regex, "g");
            var wrapClass = this.highlightInstructions.classes[word.styleId].name;
            this.line = this.wrapMatch(wrapClass, regex, this.line);
        }
    };

    n.SyntaxHighlighter.prototype._multiLineHighlight = function() {
        for (var i = 0; i < this.highlightInstructions.multiLine.length; i++) {
            var multiLine = this.highlightInstructions.multiLine[i];
            var regex = {};
            if (this.waitingForEnd) {
                regex = new RegExp(multiLine.end,"g");
            } else {
                regex = new RegExp(multiLine.start,"g");
            }

            if (regex.test(this.line)) {
                if(this.waitingForEnd) {
                    this.line = this.endWrap(this.multiLineClass, regex, this.line);
                    this.waitingForEnd = false;
                    this.multiLineClass = "";
                } else {
                    this.multiLineClass = this.highlightInstructions.classes[multiLine.styleId].name;
                    this.line = this.startWrap(this.multiLineClass, regex, this.line);
                    this.waitingForEnd = true;
                }
                return true;
            }
        }
        return false;
    };

    n.SyntaxHighlighter.prototype.wrapLine = function(spanClass, text) {
        var line = "<span class=\"" + spanClass + "\">" + text + "</span>";
        return line;
    };

    n.SyntaxHighlighter.prototype.startWrap = function(spanClass, regex, line) {
        var newLine = line.replace(regex, "<span class=\"" + spanClass + "\">$1$2</span>");
        return newLine;
    };

    n.SyntaxHighlighter.prototype.endWrap = function(spanClass, regex, line) {
        var newLine = line.replace(regex, "<span class=\"" + spanClass + "\">$1</span>$2");
        return newLine;
    };

    n.SyntaxHighlighter.prototype.wrapMatch = function (spanClass, regex, line) {
        var newLine = line.replace(regex, "$1<span class=\"" + spanClass + "\">$2</span>$3");
        return newLine;
    };

})(QfqNS);