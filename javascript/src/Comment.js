/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Displays Comment or an Editor to create a comment
     *
     * https://www.quora.com/What-is-the-best-way-to-check-if-a-property-or-variable-is-undefined
     *
     */
    n.Comment = function (comment, user, $container, options) {
        this.comment = comment;
        this.user = user;
        this.$parent = $container;
        this.$comment = {};
        this.$text = {};
        if (arguments.length === 3) {
            this.options = { readOnly: false };
        } else {
            this.options = options;
        }
        this.childrenController = {};
        this.eventEmitter = new EventEmitter();
        this.deleted = false;
    };

    n.Comment.prototype.on = n.EventEmitter.onMixin;

    n.Comment.prototype.display = function() {
        var displayElement;
        displayElement = this._buildComment();
        displayElement.appendTo(this.$parent);
        this.$comment = displayElement;
    };

    n.Comment.prototype.getParent = function() {
        return this.$parent;
    };

    n.Comment.prototype.height = function() {
        return this.$comment.height();
    };

    n.Comment.prototype._buildComment = function(allowEdit) {
        var $commentWrap = $('<div/>', {
            class: "qfqComment"
        });
        var $avatar = $('<img>', {
            src: this.user.avatar,
            class: "qfqCommentAvatar"
        });
        $avatar.appendTo($commentWrap);
        var $topLine = $('<div />', {
            class: "qfqCommentTopLine"
        });
        $('<span />', {
            class: "qfqCommentAuthor",
            text: this.user.name + ":"
        }).appendTo($topLine);
        $('<span />', {
            class: "qfqCommentDateTime",
            text: this.comment.dateTime
        }).appendTo($topLine);
        $topLine.appendTo($commentWrap);
        var $comment = $('<div />', {
            class: "qfqCommentText"
        });
        $comment.html(this.comment.comment);
        if (!this.options.readOnly) {
            $comment.append(this._getCommands());
        }
        this.$text= $comment;
        $comment.appendTo($commentWrap);
        return $commentWrap;
    };

    n.Comment.prototype._updateText = function(text) {
          this.$text.html(text);
          this.$text.append(this._getCommands());
    };

    n.Comment.prototype._getCommands = function () {
        var $commentCommands = $("<div />", {
            class: "qfqCommentCommands"
        });
        var that = this;
        $commentCommands.append(this._getCommand("Edit", "pencil", function(e) {
            that._editMe(e);
        }));
        $commentCommands.append(this._getCommand("Delete", "trash", function(e) {
            that._deleteMe(e);
        }));
        $commentCommands.append(this._getCommand("Reply", "comment", function(e) {
            that._replyToMe(e);
        }));
        return $commentCommands;
    };

    n.Comment.prototype._getCommand = function(description, icon, onClick) {
        var $command = $('<span />', {
            class: "glyphicon glyphicon-" + icon + " qfqCommentCommand",
            title: description
        });
        $command.bind("click", this, onClick);
        return $command;
    };

    n.Comment.prototype._deleteMe = function(e) {
        this.deleted = true;
        this.$comment.remove();
        this.eventEmitter.emitEvent('comment.deleted',
            n.EventEmitter.makePayload(this, this.comment));
    };

    n.Comment.prototype._editMe = function(e) {
        this.$comment.hide();
        var that = this;
        var editor = new QfqNS.Editor();
        var $editor = editor.buildEditor(this.comment.comment);
        editor.on("editor.submit", function(e) {
             that._updateComment(e);
        });
        this.$comment.after($editor);

    };

    n.Comment.prototype._replyToMe = function(e) {
        this.eventEmitter.emitEvent('comment.reply',
            n.EventEmitter.makePayload(this, this.comment));
    };

    n.Comment.prototype._updateComment = function(e) {
        this.comment.comment = e.data.text;
        this._updateText(e.data.text);
        this.$comment.show();
        e.data.$container.remove();
        this.eventEmitter.emitEvent('comment.edited',
            n.EventEmitter.makePayload(this, this.comment));
    };

})(QfqNS);