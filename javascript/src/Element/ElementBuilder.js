/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend ../QfqEvents.js */


/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.ElementBuilder = function(obj, parent) {
        this.type  = obj.type;
        this.class = obj.class || false;
        this.text  = obj.text || false;
        this.tooltip = obj.tooltip || false;
        this.label = obj.label || false;
        this.value = obj.value || false;
        this.width = obj.width || false;
        this.name = obj.name || false;
        this.onClick = obj.onClick || false;
        this.parent = parent || false;
        this.submitTo = obj.submitTo || false;
        this.checked = obj.checked || false;
        this.$element = {};
        this.children = [];
        this.eventEmitter = new EventEmitter();


        if (obj.children) {
            for (var i = 0; i < obj.children.length; i++) {
                var iparent = this;
                if (this.type !== "form") {
                    iparent = this.parent;
                }
                var element = new n.ElementBuilder(obj.children[i], iparent);

                this.children.push(element);
            }
        }

    };

    n.ElementBuilder.prototype.on = n.EventEmitter.onMixin;

    n.ElementBuilder.prototype.display = function() {
        var $element = {};

        if (this.type === "form") {
            $element = this._buildForm();
            var that = this;
            $element.submit(function(event) {
                event.preventDefault();
                that.handleSubmit();
            });
            this.$form = $element;
        }

        if (this.type === "row") $element = this._buildRow();
        if (this.type === "checkbox" ||
            this.type === "radio" ||
            this.type === "hidden") $element = this._buildInput();
        if (this.type === "label") $element = this._buildLabel();

        if (this.children) {
            for (var i = 0; i < this.children.length; i++) {
                var $child = this.children[i].display();
                $element.append($child);
            }
        }

        this.$element = $element;
        return $element;
    };

    n.ElementBuilder.prototype._buildRow = function() {
        var options = {
            class: "row" + this._getOption(this.class),
            text: this._getOption(this.text)
        };
        return $("<div />", options);
    };

    n.ElementBuilder.prototype._buildInput = function() {
        var $block = {};

        var options = {
            class: this._getOption(this.class),
            type: this._getOption(this.type),
            name: this._getOption(this.name),
            value: this._getOption(this.value)
        };

        if (this.type === "checkbox" || this.type === "radio") {
            options.checked = this.checked;
        }

        if (this.type !== "hidden") {
            $block = this._buildBlock(this.width);
        } else {
            options.required = false;
        }

        var $input = $("<input />", options);

        if (this.onClick === "submit") {
            var that = this;
            $input.on("click", function() {
                that.submit();
            });
        }

        if (this.type !== "hidden") {
            $block.append($input);
            return $block;
        } else {
            $input.removeAttr("pattern");
            return $input;
        }
    };

    n.ElementBuilder.prototype._buildLabel = function() {
        var $block = this._buildBlock(this.width, "qfq-label");
        var options = {
            class: "control-label" + this._prepareClass(this.class, true),
            text: this._getOption(this.text)
        };
        var $label = $("<span />", options);
        $block.append($label);
        return $block;
    };

    n.ElementBuilder.prototype._buildBlock = function(size, cssClass) {
        var options = {
            class: "col-md-" + size + this._prepareClass(cssClass, true)
        };
        return $("<div />", options);
    };

    n.ElementBuilder.prototype._prepareClass = function(value, isAddition) {
        if (isAddition) return " " + value;
        return "" + value;
    };

    n.ElementBuilder.prototype._buildForm = function() {
        return $("<form />");
    };

    n.ElementBuilder.prototype._getOption = function(o) {
        if (o !== undefined && o) {
            return o;
        } else {
            return "";
        }
    };

    n.ElementBuilder.prototype.submit = function() {
          if (this.type !== "form") {
              this.parent.$element.submit();
          } else {
              this.$element.submit();
          }
    };

    n.ElementBuilder.prototype.handleSubmit = function() {
        $.post(this.submitTo, this.$element.serialize())
            .done(this.submitSuccessHandler.bind(this))
            .fail(this.submitFailureHandler.bind(this));
    };

    n.ElementBuilder.prototype.submitSuccessHandler = function(data, textStatus, jqXHR) {
        var configuration = data['element-update'];
        n.ElementUpdate.updateAll(configuration);
        this.eventEmitter.emitEvent('form.submit.success',
            n.EventEmitter.makePayload(this, "submit"));
    };

    n.ElementBuilder.prototype.submitFailureHandler = function (data, textStatus, jqXHR) {
        console.error("Submit failed");
    };

})(QfqNS);
