class ImageAdjust {

    initialize(parentElement, qfqPage, n) {
        this.qfqPage = qfqPage
        this.n = n
        const canvas = document.createElement('canvas')
        this.parentElement = parentElement
        const elementSize = parentElement.getBoundingClientRect()
        this.options = parentElement.dataset
        canvas.width = this.options.outputWidth
        canvas.height = this.options.outputHeight
        this.rotation = 0
        this.canvasElement = canvas
        this._addControls()
        this.parentElement.append(canvas)
        this.canvas = this.__canvas = new fabric.Canvas(canvas, {
            isDrawingMode: false,
            selection: false,
            isDragging: false,
            enableRetinaScaling: false,
            centeredScaling: true,
            stateful: true
        })
        var that = this
        this.canvas.on('mouse:down', function(e) { that.panStart(e) })
        this.canvas.on('mouse:move', function(e) { that.panMove(e) })
        this.canvas.on('mouse:up', function(e) { that.panEnd() })
        this.canvas.on('mouse:wheel', function(e) { that.dynamicZoom(e) })
        this._setImage()
        this._addMargins()
    }

    _setImage() {
        fabric.util.loadImage(this.options.image, img => {
            this.image = new fabric.Image(img)
            this._sizeImageToCanvas()
            this.image.set({
                // Some options to make the image static
                selectable: false,
                evented: false,
                lockMovementX: true,
                lockMovementY: true,
                lockRotation: true,
                lockScalingX: true,
                lockScalingY: true,
                lockUniScaling: true,
                hasControls: false,
                imageSmoothing: true,
                hasBorders: false,
            })
            this.canvas.add(this.image)
            this.canvas.centerObject(this.image)
            this.image.setCoords()
            this.canvas.renderAll()
            this.importJSON()
        },  { crossOrigin: 'anonymous' })     
    }

    _sizeImageToCanvas() {
        this.scale = Math.max(this.canvas.width / this.image.width, this.canvas.height / this.image.height)
        this.image.scale(this.scale)

    }

    _addControls() {
        const wrap = document.createElement("div")
        this.parentElement.append(wrap)
        wrap.classList.add('qfq-image-adjust-buttons', 'btn-group')
        const buttons = this._getButtons()
        for (const button of buttons) {
            const element = this._createButton(button)
            wrap.append(element)
        }
        if(this.options.imgPreviewId) {
            const element = this.createElement({
                classList: ["btn","btn-default"],
                icon: {
                    classList: ["fas", "fa-camera-retro"]
                },
                text: "",
                click: this.showImage.bind(this)
            })
            wrap.append(element)
        }
    }

    _createButton(button) {
        const element = document.createElement("button")
        element.classList.add(...button.classList)
        const icon = document.createElement("i")
        if(button.icon.classList.length > 0) icon.classList.add(...button.icon.classList)
        element.textContent = button.text
        element.append(icon)
        element.addEventListener('click', (e) => { button.click(e) })
        return element
    }

    _addMargins() {
        const container = document.querySelector(".canvas-container")
        const margins = JSON.parse(this.options.darkenMargins)
        for(const [key, value] of Object.entries(margins)) {
            console.log(key, value)
            const margin = document.createElement("div")
            margin.classList.add("qfq-ia-margin-" + key)
            if(key === 'left' || key == 'right') {
                margin.style.width = value + "px"
                margin.style.height = this.canvas.height - margins.top - margins.bottom + "px"
                margin.style.top = margins.top + "px"
            } else {
                margin.style.height = value + "px"
                margin.style.width = this.canvas.width + "px"
            }
            container.append(margin)
        }
    }

    exportJSON() {
        const settings = {
            scale: this.scale,
            //coords: this.image._getCoords(true),
            //viewport: this.canvas.viewportTransform,
            left: this.image.left,
            top: this.image.top,
            rotation: this.rotation,
            flipX: this.image.flipX,
            flipY: this.image.flipY
        }
        const output = document.getElementById(this.options.fabricJsonId)
        if(output) output.value = JSON.stringify(settings)
    }

    importJSON() {
        const input = document.getElementById(this.options.fabricJsonId)
        if(input && input.value.length > 0) {
            const unescape = decodeURIComponent(input.value)
            console.log("Trying to parse settings", unescape)
            const settings = JSON.parse(unescape)
            console.log("Imported Settings", settings)
            this._applySettings(settings)
        }
    }

    _applySettings(settings) {
        this.scale = settings.scale
        this.image.rotate(settings.rotation)
        this.rotation = settings.rotation || 0
        this.image.left = settings.left || 0
        this.image.top = settings.top || 0
        this.image.flipX = settings.flipX
        this.image.flipY = settings.flipY
        //this.canvas.setViewportTransform(this.canvas.viewportTransform)
        this.image.scale(settings.scale)
        //this.image.aCoords = settings.coords
        //this.image.setCoords()
        this.canvas.renderAll()
    }

    flipX() {
        this.image.flipX = !this.image.flipX
        this.canvas.renderAll()
        this.changeHandler()
    }

    flipY() {
        this.image.flipY = !this.image.flipY
        this.canvas.renderAll()
        this.changeHandler()
    }

    rotate() {
        this.rotation += 90
        console.log(this.rotation)
        this.image.rotate(this.rotation)
        this.canvas.renderAll()
        this.changeHandler()
    }

    zoomIn(e) {
        const scale = this.scale + 0.05;
        this._setScale(scale)
    }

    zoomOut(e) {
        const scale = this.scale - 0.05;
        this._setScale(scale)
    }

    dynamicZoom(opt) {
        const delta = opt.e.deltaY * -0.0001
        const scale = this.scale + delta;
        console.log(delta)
        this._setScale(scale)
    }

    panLeft(e) {
        const pan = ""
    }

    panRight(e) {
        const pan = ""
    }

    panStart(opt) {
        const e = opt.e
        this.canvas.isDragging = true
        this.canvas.lastPosX = e.clientX
        this.canvas.lastPosY = e.clientY
    }

    panMove(opt) {
        if(!this.canvas.isDragging) return
        const e = opt.e
        /*
        const vpt = this.canvas.viewportTransform
        vpt[4] += e.clientX - this.canvas.lastPosX
        vpt[5] += e.clientY - this.canvas.lastPosY
        */
        this.image.left += e.clientX - this.canvas.lastPosX
        this.image.top += e.clientY - this.canvas.lastPosY
        this.canvas.requestRenderAll()
        this.canvas.lastPosX = e.clientX
        this.canvas.lastPosY = e.clientY
    }

    panEnd(e) {
        //this.canvas.setViewportTransform(this.canvas.viewportTransform);
        this.canvas.isDragging = false;
        this.changeHandler()
    }

    changeHandler() {
        this._exportImage(false)
        this.exportJSON()
        if (this.qfqPage.qfqForm) {
            this.qfqPage.qfqForm.eventEmitter.emitEvent('form.changed', this.n.EventEmitter.makePayload(this, null))
            this.qfqPage.qfqForm.changeHandler()
            this.qfqPage.qfqForm.form.formChanged = true
        } else {
            console.log("Error: Couldn't initialize qfqForm - not possible to send form.changed event")
        }
    }

    _setScale(scale) {
        if(scale > 2.0) scale = 2.0
        if(scale < 0.001) scale = 0.001
        this.image.scale(scale)
        this.scale = scale
        this.image.setCoords()
        this.canvas.renderAll()
        this.changeHandler()
    }

    export() {
        console.log("called export")
        this._exportImage(false)
    }

    showImage() {
        console.log("called show image")
        this._exportImage(true)
    }

    _getButtons() {
        return [{
            classList: ["btn","btn-default"],
            icon: {
                classList: ["fa", "fa-search-plus"]
            },
            text: "",
            click: this.zoomIn.bind(this)
        },
        {
            classList: ["btn","btn-default"],
            icon: {
                classList: ["fa", "fa-search-minus"]
            },
            text: "",
            click: this.zoomOut.bind(this)
        },
        {
            classList: ["btn","btn-default"],
            icon: {
                classList: ["fas", "fa-arrows-alt-h"]
            },
            text: "",
            click: this.flipX.bind(this)
        },
        {
            classList: ["btn","btn-default"],
            icon: {
                classList: ["fas", "fa-arrows-alt-v"]
            },
            text: "",
            click: this.flipY.bind(this)
        },
        {
            classList: ["btn","btn-default"],
            icon: {
                classList: ["fas", "fa-redo"]
            },
            text: "",
            click: this.rotate.bind(this)
        }]
    }

    _exportImage(asImage) {
        const dataURL = this.canvas.toDataURL({
            format: this.options.outputFormat,
            quality: this.options.outputQualityJpeg
        })
        
        const selector = asImage ? this.options.imgPreviewId : this.options.base64Id
        const output = document.getElementById(selector)
        if(output) {
            if(asImage) {
                output.src = dataURL
            } else {
                output.value = dataURL
            }
            
        } else {
            console.log("Target " + (asImage ? "img tag" : "input") + " not found", this.options)
        }
    }

}

/* init without qfq 
document.addEventListener("DOMContentLoaded", function() {
    const imageEditors = document.querySelectorAll(".qfq-image-adjust")
    console.log("Image Adjust Elements", imageEditors)
    imageEditors.forEach(editor => {
        console.log("Current Editor", editor)
        const imageAdjust = new ImageAdjust()
        imageAdjust.initialize(editor, {})
    })
})
*/