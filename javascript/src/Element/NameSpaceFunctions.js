/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Factory for FormGroupS.
     *
     * @param name
     * @returns {*}
     * @function QfqNS.Element.getElement
     */
    n.getElement = function (name) {
        var elementName;
        var $element = $('[name="' + QfqNS.escapeJqueryIdSelector(name) + '"]:not([type="hidden"])');
        if ($element.length === 0) {
            // Get right element for tinymce with given name (is hidden input field)
            $element = $('[name="' + QfqNS.escapeJqueryIdSelector(name) + '"]');
            $element = getTypeAheadInput($element);
            if ($element === undefined) {
                throw Error('No element with name "' + name + '" found.');
            }
        }

        // Handle <select> and <textarea>
        elementName = $element[0].nodeName.toLowerCase();
        if (elementName === "select") {
            return new n.Select($element);
        }

        if (elementName === "textarea") {
            return new n.TextArea($element);
        }

        // Handle chat element
        if ($element.hasClass('qfq-chat')) {
            return $element[0].querySelector('.qfq-chat-window');
        }

        // Since it is neither a <select> nor a <textarea>, we assume it is an <input> element. Thus we analyze the
        // type attribute
        if (!$element[0].hasAttribute('type')) {
            return new n.Textual($element);
        }

        var type = $element[0].getAttribute('type').toLowerCase();

        if (type === 'checkbox') {
            var $formGroup = $('#' + $element.attr('id') + '-i');
            if (!$formGroup || $formGroup.length === 0) {
                type = 'nonFormGroupCheckbox';
            }
        }

        switch (type) {
            case 'checkbox':
                return new n.Checkbox($element);
            case 'nonFormGroupCheckbox':
            case "file":
                return $element;
            case 'radio':
                return new n.Radio($element);
            case 'text':
            case 'number':
            case "email":
            case "url":
            case "password":
            case "datetime":
            case "datetime-local":
            case "date":
            case "month":
            case "time":
            case "week":
            case "search":
                return new n.Textual($element);
            default:
                throw new Error("Don't know how to handle <input> of type '" + type + "'");
        }

        // Get typeahead input element
        function getTypeAheadInput (elem) {
            var selector = '.twitter-typeahead';
            var sibling = elem.prev();
            var siblingCount = elem.siblings().length;
            // If the sibling matches our selector, use it
            // If not, jump to the next sibling and continue the loop
            for (var i = 0; i < siblingCount; i++) {
                if (sibling.is(selector)) return sibling.children('.tt-input');
                sibling = sibling.prev();
            }
        }
    };
})(QfqNS.Element);