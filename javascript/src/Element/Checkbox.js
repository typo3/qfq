/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* @depend FormGroup.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Checkbox (`<checkbox>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Checkbox
     */
    function Checkbox($element) {
        n.FormGroup.call(this, $element);

        var type = "checkbox";

        if (!this.isType(type)) {
            throw new Error("$element is not of type 'checkbox'");
        }

        // We allow one Form Group to have several checkboxes. Therefore, we have to remember which checkbox was
        // selected if possible.
        if ($element.length === 1 && $element.attr('type') === type) {
            this.$singleElement = $element;
        } else {
            this.$singleElement = null;
        }
    }

    Checkbox.prototype = Object.create(n.FormGroup.prototype);
    Checkbox.prototype.constructor = Checkbox;

    Checkbox.prototype.setValue = function (val) {
        if (this.$singleElement) {
            this.$singleElement.prop('checked', val);
        } else {
            this.$element.prop('checked', val);
        }
    };

    Checkbox.prototype.getValue = function () {
        if (this.$singleElement) {
            return this.$singleElement.prop('checked');
        } else {
            return this.$element.prop('checked');
        }
    };

    n.Checkbox = Checkbox;

})(QfqNS.Element);
