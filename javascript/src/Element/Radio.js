/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Radio (`<radio>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Radio
     */
    function Radio($element) {
        n.FormGroup.call(this, $element);

        if (!this.isType("radio")) {
            throw new Error("$element is not of type 'radio'");
        }
    }

    Radio.prototype = Object.create(n.FormGroup.prototype);
    Radio.prototype.constructor = Radio;

    Radio.prototype.setValue = function (val) {
        this.$element.prop('checked', false);
        this.$element.filter('[value="' + val.replace(/"/g, "\\\"") + '"]').prop('checked', true);
    };

    Radio.prototype.getValue = function () {
        return this.$element.filter(':checked').val();
    };

    n.Radio = Radio;

})(QfqNS.Element);