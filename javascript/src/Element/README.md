Element
===

On a QfqForm, an Element is comprised of several HTML elements enclosed in a Bootstrap `form-group`, for instance

    <div class="form-group">
        <div class="col-md-2">
           <label for="personHompage" class="control-label">Hompage</label>
        </div>
        <div class="col-md-6">
           <input id="personHompage" type="text" class="form-control">
        </div>

        <div class=" col-md-4">
           <p class="help-block">
             Alternative zur default 'Persoenlichen Homepage'. Falls kein Link auf
             eine Homepage gesetzt werden soll: <b>no_homepage</b>
           </p>
        </div>
    </div>
    
FormGroup is an abstraction which allows to perform common operations on a QfqForm Element. It hides the intricacies of 
the underlying HTML Form Elements and the Element.

The user should not instantiate FormGroup directly, but use the factory method `QfqNS.Element.getElement()` (defined 
in `NameSpaceFunctions.js`).