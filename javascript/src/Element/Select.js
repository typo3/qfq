/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Select (`<select>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Select
     */
    function Select($element) {
        n.FormGroup.call(this, $element, 'select');

        if (!this.isType("select")) {
            throw new Error("$element is not of type 'select'");
        }
    }

    Select.prototype = Object.create(n.FormGroup.prototype);
    Select.prototype.constructor = Select;

    /**
     * Set the value or selection of a `<select>` tag
     *
     * @param {string|array} val when passing a string, the corresponding <option> tag will get selected. If passed
     * array of objects, `<select>` will have its `<option>` tags set correspondingly.
     */
    Select.prototype.setValue = function (val) {
        if (['string', 'number'].indexOf(typeof(val)) !== -1) {
            this.setSelection(val);
        } else if (Array.isArray(val)) {
            this.$element.empty();

            // Fill array with new <select> elements first and add it to the dom in one step, instead of appending
            // each '<select>' separately.
            var selectArray = [];
            val.forEach(function (selectObj) {
                var $option = $('<option>')
                    .attr('value', selectObj.value ? selectObj.value : selectObj.text)
                    .prop('selected', selectObj.selected ? selectObj.selected : false)
                    .append(selectObj.text);
                selectArray.push($option);
            });
            this.$element.append(selectArray);
        } else {
            throw Error('Unsupported type of argument in Select.setValue: "' + typeof(val) + '". Expected either' +
                ' "string" or "array"');
        }
    };

    /**
     *
     * @param val
     *
     * @private
     */
    Select.prototype.setSelection = function (val) {
        this.clearSelection();

        // First, see if we find an <option> tag having an attribute 'value' matching val. If that doesn't work,
        // fall back to comparing text content of <option> tags.
        var $selectionByValue = this.$element.find('option[value="' + val.replace(/"/g, "\\\"") + '"]');
        if ($selectionByValue.length > 0) {
            $selectionByValue.prop('selected', true);
        } else {
            this.$element.find('option').each(function () {
                var $element = $(this);
                if ($element.text() === val) {
                    $element.prop('selected', true);
                }

                return true;
            });
        }
    };

    /**
     * @private
     */
    Select.prototype.clearSelection = function () {
        this.$element.find(':selected').each(function () {
            $(this).prop('selected', false);
        });
    };

    Select.prototype.getValue = function () {
        var returnValue = [];
        this.$element.find(':selected').each(
            function () {
                if (this.hasAttribute('value')) {
                    returnValue.push(this.getAttribute('value'));
                } else {
                    returnValue.push($(this).text());
                }
            }
        );

        return returnValue;
    };

    n.Select = Select;

})(QfqNS.Element);