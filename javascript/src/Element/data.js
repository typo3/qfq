/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';

    /**
     * Known values of `type` attribute of `<input>` elements
     *
     * @type {string[]}
     */
    n.knownElementTypes = [
        'text',
        'password',
        'checkbox',
        'radio',
        'button',
        'submit',
        'reset',
        'file',
        'hidden',
        'image',
        'datetime',
        'datetime-local',
        'date',
        'month',
        'time',
        'week',
        'number',
        'range',
        'email',
        'url',
        'search',
        'tel',
        'color'
    ];

    /*
     * See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input for input types ignoring the readonly
     * attribute.
     */
    n.readOnlyIgnored = [
        'hidden',
        'range',
        'checkbox',
        'radio',
        'file',
        'select'
    ];

})(QfqNS.Element);