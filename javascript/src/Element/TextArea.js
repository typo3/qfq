/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Textarea (`<textarea>`) Form Group.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.TextArea
     */
    function TextArea($element) {
        n.FormGroup.call(this, $element, 'textarea');

        if (!this.isType("textarea")) {
            throw new Error("$element is not of type 'textarea'");
        }
    }

    TextArea.prototype = Object.create(n.FormGroup.prototype);
    TextArea.prototype.constructor = TextArea;

    /**
     * Set the value or selection of a `<textarea>` tag
     *
     * @param {string} val content of the textarea
     */
    TextArea.prototype.setValue = function (val) {
        this.$element.val(val);
    };

    TextArea.prototype.getValue = function () {
        return this.$element.val();
    };

    n.TextArea = TextArea;

})(QfqNS.Element);
