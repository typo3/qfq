/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq.Element Namespace
 *
 * @namespace QfqNS.Element
 */
QfqNS.Element = QfqNS.Element || {};

(function (n) {
    'use strict';


    /**
     * Textual `<input>` form groups.
     *
     * @param $element
     * @constructor
     * @name QfqNS.Element.Textual
     */
    function Textual($element) {
        n.FormGroup.call(this, $element);

        var textualTypes = [
            'text',
            'datetime',
            'datetime-local',
            'date',
            'month',
            'time',
            'week',
            'number',
            'range',
            'email',
            'url',
            'search',
            'tel',
            'password',
            'hidden'
        ];
        var textualTypesLength = textualTypes.length;
        var isTextual = false;

        for (var i = 0; i < textualTypesLength; i++) {
            if (this.isType(textualTypes[i])) {
                isTextual = true;
                break;
            }
        }

        if (!isTextual) {
            throw new Error("$element is not of type 'text'");
        }
    }

    Textual.prototype = Object.create(n.FormGroup.prototype);
    Textual.prototype.constructor = Textual;

    Textual.prototype.setValue = function (val) {
        // Typeahead delivers an array with refreshed value after save progress.
        if (Array.isArray(val)) {
            this.setTypeAheadInput(val);
        } else {
            this.$element.val(val);
        }
    };

    // Apply and see changed value after save (no page reload needed), typeahead elements needs to be handled separately.
    Textual.prototype.setTypeAheadInput = function (value) {
        this.$element.val(value[0].key);
        // Display new value and correctly set new key in hidden input element.
        this.$element.eq(1).typeahead('val', value[0].value);
        var hiddenElement =  this.$element.eq(0).closest('div').find('input[type=hidden]');
        hiddenElement.val(value[0].key);
    };

    Textual.prototype.getValue = function () {
        return this.$element.val();
    };

    n.Textual = Textual;

})(QfqNS.Element);