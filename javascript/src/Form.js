/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Abstraction of `<form>`.
     *
     * @param formId
     * @param formChanged
     * @constructor
     * @name QfqNS.Form
     */
    n.Form = function (formId, formChanged) {
        this.formId = formId;
        this.eventEmitter = new EventEmitter();
        // In order to immediately emit events, we bind event handlers on `<form>` `change` and `input` and `paste` on
        // `<input> and `<textarea>`s. Without precaution, this will lead to emitting `form.changed` twice upon
        // `input` and `paste` events, since they eventually will raise a form `change` event.  We perform bookkeeping
        // using this flag, to avoid emitting `form.changed` twice when bubbling.
        //
        // Since we cannot predict the effect on disable bubbling of the `input` and `paste` events, we resort to this
        // home-brew solution.
        this.inputAndPasteHandlerCalled = false;
        this.filepondUploadProcessing = [];
        this.saveInProgress = false;

        if (!document.forms[this.formId]) {
            throw new Error("Form '" + formId + "' does not exist.");
        }

        this.formChanged = !!formChanged;
        this.formChangedTimestampInMilliseconds = n.Form.NO_CHANGE_TIMESTAMP;
        this.$form = $(document.forms[this.formId]);
        this.$form.on("change", (event) => {
            if (this.filepondUploadProcessing.length === 0) {
                this.changeHandler(event);
            }
        });

        this.$form.on("invalid.bs.validator", this.validationError.bind(this));
        this.$form.on("valid.bs.validator", this.validationSuccess.bind(this));

        // On <input> elements, we specifically bind this events, in order to update the formChanged property
        // immediately, not only after loosing focus. Same goes for <textarea>
        this.$form.find("input, textarea").on("input paste", (event) => {
            // Only trigger if no filepond upload is in progress
            if (this.filepondUploadProcessing.length === 0) {
                this.inputAndPasteHandler(event);
            }
        });

        // Fire handler while using dateTimePickerType qfq
        function getDatetimePickerChanges(element) {
            $('div tbody').on('click', 'td.day:not(.disabled)', formObject.inputAndPasteHandler.bind(formObject));
            var timepickerElements = 'td a.btn[data-action="incrementHours"], td a.btn[data-action="incrementMinutes"], td a.btn[data-action="incrementSeconds"], td a.btn[data-action="decrementHours"], td a.btn[data-action="decrementMinutes"], td a.btn[data-action="decrementSeconds"]';
            $('div table').on('click', 'td.hour, td.minute, td a[data-action="clear"], '+timepickerElements, formObject.inputAndPasteHandler.bind(formObject));

            element.addEventListener('keydown', function(event) {
                if (event.key === 'Delete') {
                    formObject.inputAndPasteHandler(event);
                }
            });
        }

        // Function to trigger onfocus event again while element is already focused
        function triggerFocus(element) {
            var eventType = "onfocusin" in element ? "focusin" : "focus",
                bubbles = "onfocusin" in element,
                event;

            if ("createEvent" in document) {
                event = document.createEvent("Event");
                event.initEvent(eventType, bubbles, true);
            }
            else if ("Event" in window) {
                event = new Event(eventType, { bubbles: bubbles, cancelable: true });
            }

            element.focus();
            element.dispatchEvent(event);
        }

        var formObject = this;
        // Open datetimepicker over click event even if first element is already focused and get all changes of datetimepicker for dirty lock
        this.$form.find(".qfq-datepicker").on("click", function(){
            triggerFocus(this);
            if (formObject.filepondUploadProcessing.length === 0) {
                getDatetimePickerChanges(this);
            }
        });

        // Fire handler while using dateTimePickerType browser
        this.$form.find("input[type=datetime-local]").on("click", (event) => {
            // Only trigger if no filepond upload is in progress
            if (this.filepondUploadProcessing.length === 0) {
                this.inputAndPasteHandler(event);
            }
        });

        // Use ctrl+alt+s for saving form
        document.addEventListener('keydown', function(event) {
            if (event.ctrlKey && event.altKey && event.key === 's') {
                $("#save-button-" + this.formId + ":not([disabled=disabled])").click();
            }
        });

        this.$form.on('submit', function (event) {
            event.preventDefault();
        });
    };

    n.Form.NO_CHANGE_TIMESTAMP = -1;

    n.Form.prototype.on = n.EventEmitter.onMixin;

    /**
     *
     * @param event
     *
     * @private
     */
    n.Form.prototype.changeHandler = function (event) {
        // Trim whitespace after change. Before validation happens.
        if (event.target.value !== undefined && event.target.type !== 'file') {
            var value = event.target.value;
            event.target.value = value.trim();
        }

        if (this.inputAndPasteHandlerCalled) {
            // reset the flag
            this.inputAndPasteHandlerCalled = false;
            // and return. The `form.changed` event has already been emitted by `Form#inputAndPasteHandler()`.
            return;
        }
        this.markChanged(event.target);
    };

    n.Form.prototype.inputAndPasteHandler = function (event) {
        this.inputAndPasteHandlerCalled = true;
        this.markChanged(event.target);
    };

    n.Form.prototype.getFormChanged = function () {
        return this.formChanged;
    };

    n.Form.prototype.markChanged = function (initiator) {
        if(!!initiator) {
            if(initiator.classList.contains("qfq-skip-dirty")) {
                return;
            }
        }
        this.setFormChangedState();
        this.eventEmitter.emitEvent('form.changed', n.EventEmitter.makePayload(this, null));
    };

    /**
     * @private
     */
    n.Form.prototype.setFormChangedState = function () {
        this.formChanged = true;
        this.formChangedTimestampInMilliseconds = Date.now();
    };

    n.Form.prototype.resetFormChanged = function () {
        this.resetFormChangedState();
        this.eventEmitter.emitEvent('form.reset', n.EventEmitter.makePayload(this, null));

    };

    /**
     * @private
     */
    n.Form.prototype.resetFormChangedState = function () {
        this.formChanged = false;
        this.formChangedTimestampInMilliseconds = n.Form.NO_CHANGE_TIMESTAMP;
    };

    n.Form.prototype.submitTo = function (to, queryParameters) {
        var submitUrl;

        this.eventEmitter.emitEvent('form.submit.before', n.EventEmitter.makePayload(this, null));
        submitUrl = this.makeUrl(to, queryParameters);

        // For better dynamic update compatibility (checkboxes). All input elements need to be not disabled for fully serializing by jquery.
        var form = $(this.$form[0]);
        // Get even disabled inputs
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var serializedForm = this.$form.serialize();
        // Reset disabled inputs
        disabled.attr('disabled','disabled');

        console.log("Serialized form", serializedForm);
        $.post(submitUrl, serializedForm)
            .done(this.ajaxSuccessHandler.bind(this))
            .fail(this.submitFailureHandler.bind(this));
    };

    n.Form.prototype.serialize = function () {
        return this.$form.serialize();
    };


    /**
     * @private
     * @param url base url
     * @param queryParameters additional query parameters
     * @returns {*}
     */
    n.Form.prototype.makeUrl = function (url, queryParameters) {
        var notFound = -1;
        var querySeparator = '?';
        var parameterSeparator = '&';
        var queryString;

        if (!queryParameters) {
            return url;
        }

        queryString = $.param(queryParameters);
        if (url.indexOf(querySeparator) === notFound) {
            return url + querySeparator + queryString;
        } else {
            return url + parameterSeparator + queryString;
        }
    };

    /**
     *
     * @param data
     * @param textStatus
     * @param jqXHR
     *
     * @private
     */
    n.Form.prototype.ajaxSuccessHandler = function (data, textStatus, jqXHR) {
        this.resetFormChangedState();
        this.eventEmitter.emitEvent('form.submit.successful',
            n.EventEmitter.makePayload(this, data, {
                textStatus: textStatus,
                jqXHR: jqXHR
            }));
        
        this.saveInProgress = false;
    };

    /**
     *
     *
     * @private
     */
    n.Form.prototype.submitFailureHandler = function (jqXHR, textStatus, errorThrown) {
        this.eventEmitter.emitEvent('form.submit.failed', n.EventEmitter.makePayload(this, null, {
            textStatus: textStatus,
            errorThrown: errorThrown,
            jqXHR: jqXHR
        }));
    };

    /**
     * @public
     * @returns {*}
     */
    n.Form.prototype.validate = function () {
        this.eventEmitter.emitEvent('form.validation.before', n.EventEmitter.makePayload(this, null));
        var isValid;
        var form = document.forms[this.formId];
        var $form = $(form);

        if ($form.data('bs.validator')) {
            $form.validator('validate');
            isValid = !$form.data('bs.validator').hasErrors();
        } else {
            isValid = form.checkValidity();
        }

        this.eventEmitter.emitEvent('form.validation.after', n.EventEmitter.makePayload(this, {validationResult: isValid}));

        return isValid;
    };

    /**
     * @private
     */
    n.Form.prototype.validationError = function (data) {
        this.eventEmitter.emitEvent('form.validation.failed', n.EventEmitter.makePayload(this, {element: data.relatedTarget}));
    };

    /**
     * @private
     */
    n.Form.prototype.validationSuccess = function (data) {
        this.eventEmitter.emitEvent('form.validation.success', n.EventEmitter.makePayload(this, {element: data.relatedTarget}));
    };

    /**
     * Uses standard HTML Validation in native javascript for input elements
     *
     * @public
     */
    n.Form.prototype.getFirstNonValidElement = function () {
        var index;
        var form = document.getElementById(this.formId);
        var inputs = form.elements;
        var elementNumber = inputs.length;

        for (index = 0; index < elementNumber; index++) {
            var element = inputs[index];
            if (!element.willValidate) {
                continue;
            }

            if (!element.checkValidity()) {
                return element;
            }
        }

        return null;
    };

})(QfqNS);