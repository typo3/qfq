/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

/**
 * A custom history to use for undo and redo functionality.
 **/

    n.History = function() {
        this.history = [];
        this.pointer = 0;
    };

    n.History.prototype.put = function(object) {
        if (this.history.length > 1) {
            if (this.canGoForward()) {
                console.log("trying to remove history");
                this._removeForwardHistory();
            }
        }
        if (JSON.stringify(this.history[this.pointer]) !== JSON.stringify(object)) {
            this.history.push(object);
            this.pointer = this.history.length - 1;
        }
        console.log(this);
    };

    n.History.prototype.back = function() {
        if (this.canGoBack()) {
            this.pointer = this.pointer - 1;
            console.log(this.pointer + "/" + this.history.length);
            console.log(this.history);
            return this.history[this.pointer];
        } else {
            console.log("At the beginning of history");
            return false;
        }
    };

    n.History.prototype.forward = function() {
        console.log(this.pointer);
        if (this.canGoForward()) {
            this.pointer = this.pointer + 1;
            return this.history[this.pointer];
        } else {
            console.log("At the end of history");
            return false;
        }
    };

    n.History.prototype.canGoBack = function() {
        return this.pointer > 0;
    };

    n.History.prototype.canGoForward = function() {
        return this.pointer < this.history.length - 1;
    };

    n.History.prototype._removeForwardHistory = function() {
        this.history.splice(this.pointer + 1, this.history.length - this.pointer);
    };



})(QfqNS);