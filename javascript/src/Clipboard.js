/* @author Benjamin Baer <benjamin.baer@math.uzh.ch> */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */


var QfqNS = QfqNS || {};

(function (n) {

    n.Clipboard = function (data) {
        this.text = '';
        this.events = [];
        if(data.text) {
            this.copyTextToClipboardAsync(data.text);
            return;
        }
        if(data.uri) {
            this.getDataFromUri(data.uri);
            return;
        }
        this.buildError("Called Clipboard without any Data to copy");
        console.error("Clipboard has to be called with an url or text to copy");
    };

    /**
     * @private
     * Has to be a bit hacky, since copy to clipboard only works with user confirmation from an API.
     * We fake this user confirmation by listening for the mouseup event after the button press.
     * As a fallback we also listen to a click event if the API took longer than the original click.
     * @param uri
     */
    n.Clipboard.prototype.getDataFromUri = function(uri) {
        var that = this;
        $.getJSON(uri, function(data) {
            if (data.text) {
                that.text = data.text;
                $(document).click(function() {that.copyTextToClipboardAsync(that.text); $(this).off();});
                $(document).mouseup(function() {that.copyTextToClipboardAsync(that.text); $(this).off();});
            } else {
                console.error("JSON Response didn't include a variable called 'text'");
                that.buildError("Didn't receive any Data to copy");
            }
        });
    };

    n.Clipboard.prototype.copyTextToClipboard = function(text) {
        var textArea = document.createElement("textarea");
       /* var focusedElement = $(":focus"); */
        textArea.value = text;
        document.body.appendChild(textArea);
       /* textArea.focus(); */
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
        } catch (err) {
            this.buildError("Couldn't copy text to clipboard: " + err);
        }

        document.body.removeChild(textArea);
       /* focusedElement.focus(); */
    };

    /**
     * Tries to copy text to the clipboard using asynchronous browser API.
     * If it doesn't exist, calls copyTextToClipboard (synchronous) instead.
     * @private String text
     */
    n.Clipboard.prototype.copyTextToClipboardAsync = function(text) {
        if (!navigator.clipboard) {
            this.copyTextToClipboard(text);
            return;
        }

        var that = this;
        navigator.clipboard.writeText(text).then(function() {
            console.log('Async: Copying to clipboard was successful!');
        }, function(err) {
            that.buildError("Could not copy text: " + err);
        });
    };

    n.Clipboard.prototype.buildError = function(message) {
        var alert = new n.Alert(
            {
                type: "error",
                message: message,
                modal: true,
                buttons: [{label: "Ok", eventName: 'close'}]
            }
        );
        alert.show();
    };

})(QfqNS);
