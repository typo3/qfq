/**
 * @author Elias Villiger <elias.villiger@uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

var QfqNS = QfqNS || {};
(function (n) {

    n.TablesorterController = function () {
        if (window.hasOwnProperty('tablesorterMockApi')) {
            this.tablesorterApiUrl = window.tablesorterMockApi;
        }
        $.tablesorter.themes.bootstrap.table = "";
    };

    // table: jquery selector of table!
    n.TablesorterController.prototype.setup = function (table, uniqueIdentifier) {
        var hasFilter = $(table).hasClass('tablesorter-filter');
        var hasPager = $(table).hasClass('tablesorter-pager');
        var hasColumnSelector = $(table).hasClass('tablesorter-column-selector');
        var hasViewSaver = $(table)[0].hasAttribute("data-tablesorter-view");
        let hasClearMe = $(table).hasClass('clear-filter')
        // Get base url from table attribute. Needed for newer typo3 versions.
        if (!window.hasOwnProperty('tablesorterMockApi')) {
            this.tablesorterApiUrl = $(table).data("tablesorter-base-url") + 'typo3conf/ext/qfq/Classes/Api/setting.php';
        }

        var tablesorterConfig = $(table).data("tablesorterConfig");
        if (!tablesorterConfig) { // revert to default
            tablesorterConfig = {
                theme: "bootstrap",
                widthFixed: true,
                headerTemplate: "{content} {icon}",
                dateFormat: "ddmmyyyy",
                widgets: ["uitheme", "filter", "saveSort", "columnSelector", "output"],
                widgetOptions: {
                    filter_columnFilters: hasFilter, // turn filters on/off with true/false
                    filter_reset: ".reset",
                    filter_cssFilter: "form-control",
                    filter_saveFilters: true,
                    columnSelector_mediaquery: false,
                    output_delivery: "download",
                    output_saveFileName: "tableExport.csv",
                    output_separator: ";"
                }
            };
        }

        $(table).tablesorter(tablesorterConfig);

        var tablesorterMenuWrapper;
        if (hasColumnSelector || hasViewSaver) {
            tablesorterMenuWrapper = this._doTablesorterMenuWrapper(table);
        }
        if (hasViewSaver) {
            this._doViewSaver(table, tablesorterMenuWrapper);
        }
        if (hasColumnSelector) {
            this._doColumnSelector(table, tablesorterMenuWrapper, uniqueIdentifier);
        }
        if (hasPager) {
            this._doPager(table, uniqueIdentifier);
        }
        if (hasColumnSelector || hasViewSaver) {
            var width = 10;
            var $elem = tablesorterMenuWrapper.children();
            for (var i = 0; i < $elem.length; i++) {
                child = $elem.children().eq(i);
                width += child.width();
            }
            console.log("element width", width);
            if (width < 10) width = 185;
            console.log("element found", $(table).children("caption").children(".pull-right"));
            $(table).children("caption").children(".pull-right").css("margin-right", width + "px");
        }
        if (hasClearMe) {
            // Add clear me class and styles required to correctly display clear me button.
            let thead = $(table).find('thead');
            thead.find('input').addClass('qfq-clear-me qfq-clear-me-table-sorter');
            thead.find('td').attr('style', 'position: relative;top: auto;');
        }
        $(table).tablesorter(tablesorterConfig);

        // Funktion zur Gruppierung der Buttons
        function groupButtons() {


            // Selektiere den Column Selector und die Page Selectors
            const columnSelector = $('#qfq-column-selector-0'); // Spaltenauswahl-Button
            const viewSaver = $('.btn-group.qfq-tablesorter-menu-item'); // Page-Selector-Dropdowns


            // Prüfe, ob mindestens eines der Elemente existiert
            if (columnSelector.length || viewSaver.length) {
                const buttonGroup = $('<div>')
                    .addClass('btn-group btn-group-sm') // Bootstrap-Klassen für kleinere Button-Gruppen
                    .attr('role', 'group') // Rolle für Barrierefreiheit
                    .attr('style', 'float: right;');

                columnSelector.attr('style', 'top: unset; right: 0px; border-top-right-radius: 100px !important; border-bottom-right-radius: 100px !important;');
                viewSaver.attr('style', 'top: unset; right: 46px;').addClass('btn-group-sm');
                viewSaver.children('button').removeClass('form-control');

                // Füge die Page Selectors (falls vorhanden) zur Gruppe hinzu
                if (viewSaver.length) {
                    console.log('Adding page selectors to button group.');
                    buttonGroup.append(viewSaver);
                }

                // Füge den Column Selector (falls vorhanden) zur Gruppe hinzu
                if (columnSelector.length) {
                    console.log('Adding column selector to button group.');
                    buttonGroup.append(columnSelector);
                }

                // Füge die erstellte Button Group in den Wrapper ein
                const wrapper = $('.qfq-tablesorter-menu-wrapper');
                if (wrapper.length) {
                    wrapper.append(buttonGroup);
                    console.log('Button group created and added to the wrapper.');
                }
            }
        }

// Rufe die Funktion auf, nachdem alle anderen Anpassungen fertig sind
        groupButtons();


    };


    n.TablesorterController.prototype.setTableView = function (table, newView, changedSelect) {
        if (newView.hasOwnProperty('columnSelection') && $.tablesorter.hasWidget(table, 'columnSelector')) {
            table.trigger('refreshColumnSelector', [newView.columnSelection]);
        }
        if (newView.hasOwnProperty('filters') && $.tablesorter.hasWidget(table, 'filter')) {
            // correct filter array length if shorter than no. of columns
            var columns = $.tablesorter.getFilters(table).length;
            var len = newView.filters.length;

            // Handle situation with saved views and empty subrecord or later changed database column count.
            if (len > columns) {
                return;
            }

            var arrayAppend = Array.apply(null, Array(columns - len)).map(function () {
                return "";
            });
            var filtersPadded = newView.filters.concat(arrayAppend);
            table.trigger('search', [filtersPadded]);
        }

        // Get and set last used view if its same from last time, otherwise use the one from setting API if view changed
        var config = table[0].config;
        if (newView.hasOwnProperty('sortList')) {
            if (config.sortList.length !== 0 && !changedSelect) {
                table.trigger('sorton', [config.sortList]);
            } else {
                table.trigger('sorton', [newView.sortList]);
            }
        }
    };

    n.TablesorterController.prototype.getTableView = function (table) {
        var view = {};
        var config = table[0].config;
        if ($.tablesorter.hasWidget(table, 'columnSelector')) {
            view.columnSelection = config.selector.states.map(function (e, i) {
                return e ? i : false;
            }).filter(function (e) {
                return e !== false;
            });
        }
        if ($.tablesorter.hasWidget(table, 'filter')) {
            view.filters = $.tablesorter.getFilters(table);
        }
        view.sortList = config.sortList;
        return view;
    };

    n.TablesorterController.prototype._doTablesorterMenuWrapper = function (table) {
        // in forms we need some distance to the top
        var addClass = '';
        if ($(table).find("caption").length > 0) addClass += ' qfq-no-margin-top';
        if ($(table).prev().is("h1,h2,h3")) addClass += ' qfq-only-top';
        var tablesorterMenuWrap = '<div class="qfq-tablesorter-menu-wrapper' + addClass + '"></div>';

        return $(tablesorterMenuWrap).insertBefore($(table));
    };

    n.TablesorterController.prototype._doViewSaver = function (table, tablesorterMenuWrapper) {
        var tableViews = JSON.parse(table.attr("data-tablesorter-view"));
        var that = this;
        // decode views from base64 (sql injection prevention)
        tableViews.forEach(function (view) {
            view.view = JSON.parse(atob(view.view));
        });

        // add 'Clear' public view if not exists
        if (!tableViews.some(function (v) {
            return v.name === 'Clear' && v.public;
        })) {
            setDefault('Clear', true);
        }

        // set default view
        function setDefault(name, publicBool) {
            var allColumns = Array($.tablesorter.getFilters(table).length).fill(0).map(function (e, i) {
                return i;
            });
            var view = {
                name: name,
                public: publicBool,
                view: {columnSelection: allColumns, filters: [], sortList: []}
            };
            tableViews.push(view);
        }

        var lastSelect = '';
        var changedSelect = false;

        // create view select dropdown
        var options = '';
        tableViews.forEach(function (view) {
            options += '<option class="qfq-fontawesome" value="' + (view.public ? 'public:' : 'private:') + view.name + '" >' +
                (view.public ? '&#xf0c0; ' : '&nbsp;&#xf007;&nbsp;&nbsp;') + view.name + '</option>';
        });
        var viewSelectorHtml = '<select class="form-control qfq-view-editor qfq-fontawesome qfq-tablesorter-menu-item" style="right: 80px; width: unset; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; height: 30px !important; top:0px !important;">' +
            '<option disabled selected value>Table view</option>' + options + '</select>';
        var select = $(viewSelectorHtml).appendTo(tablesorterMenuWrapper);
        var tableId = table.attr("data-tablesorter-id");
        select.change(function () {
            var viewFromSelect = that._parseViewSelectValue($(this).val());
            var view = tableViews.find(function (v) {
                return v.name === viewFromSelect.name && v.public === viewFromSelect.public;
            });

            // check for changed select (used dropdown)
            var actualSelect = $(this).val();
            if (actualSelect !== lastSelect) {
                changedSelect = true;
            }
            lastSelect = actualSelect;

            that.setTableView(table, view.view, changedSelect);
            that._updateTablesorterUrlHash(table, $(this).val());

            // save view choice in local storage
            localStorage.setItem('tablesorterView_' + tableId, $(this).val());
        });

        // select view on page load: first priority from url hash, second priority localstorage, third private view named 'Default', fourth is public view 'Clear'
        // Default (third priority) + Clear (fourth priority)
        var publicDefaultExists = tableViews.some(function (v) {
            return v.name === 'Default' && v.public;
        });
        var setValue = publicDefaultExists ? "public:Default" : "public:Clear";

        // local storage (second priority)
        var localStorageView = localStorage.getItem('tablesorterView_' + tableId);
        if (localStorageView !== 'undefined' && select.children('option[value="' + localStorageView + '"]').length > 0) {
            setValue = localStorageView;
        }

        // url hash (first priority)
        var hashParameters = this._getTablesorterUrlHash();
        var value = hashParameters[tableId];
        if (typeof value !== 'undefined' && select.children('option[value="' + value + '"]').length > 0) {
            setValue = value;
        }

        // apply view change and get last view
        lastSelect = setValue;
        select.val(setValue).change();

        // create edit view dropdown
        var viewDropdownHtml = '<div class="btn-group qfq-tablesorter-menu-item" style="right: 53px;">' +
            '<button type="button" class="btn btn-default btn-group form-control qfq-view-editor dropdown-toggle" data-toggle="dropdown" style="border-radius: 0 0 0 0;" >' +
            '<i class="fa fa-pencil-alt"></i>' +
            '</button>' +
            '<ul class="dropdown-menu pull-right" role="menu">' +
            '<li><a href="#" data-save-private-view>Save Personal View</a></li>' +
            '<li><a href="#" data-save-public-view>Save Group View</a></li>' +
            '<li><a href="#" data-delete-view>Delete View</a></li>' +
            '</ul>' +
            '</div>';
        var viewDropdown = $(viewDropdownHtml).appendTo(tablesorterMenuWrapper);

        var SavePrivateViewButton = viewDropdown.find('[data-save-private-view]');
        SavePrivateViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            that._saveTableViewPrompt(table, viewFromSelect.name, false);
        });

        var SavePublicViewButton = viewDropdown.find('[data-save-public-view]');
        SavePublicViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            that._saveTableViewPrompt(table, viewFromSelect.name, true);
        });

        var DeleteViewButton = viewDropdown.find('[data-delete-view]');
        DeleteViewButton.click(function () {
            var viewFromSelect = that._parseViewSelectValue(select.val());
            viewFromSelect.mode = 'delete';
            var sip = table.attr("data-tablesorter-sip");
            $.post(that.tablesorterApiUrl + "?s=" + sip, viewFromSelect, function (response) {
                location.reload(true);
            }, 'json').fail(function (xhr, status, error) {
                that._alert('Error while trying to save view:<br>' + JSON.parse(xhr.responseText).message);
            });
        });


    };

    n.TablesorterController.prototype._doColumnSelector = function (table, tablesorterMenuWrapper, uniqueIdentifier) {
        var columnSelectorId = "qfq-column-selector-" + uniqueIdentifier;
        var columnSelectorTargetId = "qfq-column-selector-target-" + uniqueIdentifier;
        var columnSelectorHtml = '<button id="' + columnSelectorId + '" class="btn btn-default qfq-tablesorter-menu-item qfq-column-selector" ' +
            'type="button">' +
            '<span class="dropdown-text"><i class="fa fa-columns"></i></span>' +
            '<span class="caret"></span></button>' +
            '<div class="hidden"><div id="' + columnSelectorTargetId + '" class="qfq-column-selector-target"> </div></div>';
        $(columnSelectorHtml).appendTo(tablesorterMenuWrapper);
        $.tablesorter.columnSelector.attachTo($(table), '#' + columnSelectorTargetId);
        $('#' + columnSelectorId).popover({
            placement: 'left',
            html: true, // required if content has HTML
            content: $('#' + columnSelectorTargetId)
        });

        table.on('columnUpdate', function () {
            var config = table[0].config;

            if ($.tablesorter.hasWidget(table, 'filter')) {
                var visibleColumns = config.selector.states;
                var filters = $.tablesorter.getFilters(table);

                // Clear filters for hidden columns
                for (var i = 0; i < visibleColumns.length; i++) {
                    if (!visibleColumns[i]) {
                        filters[i] = '';
                    }
                }

                // Apply / Trigger updated filters
                table.trigger('search', [filters]);
            }
        });
    };

    n.TablesorterController.prototype._doPager = function (table, uniqueIdentifier) {
        var pagerId = "qfq-pager-" + uniqueIdentifier;
        var pagerHtml = `
        <div id="${pagerId}" class="qfq-tablesorter-group qfq-tablesorter-group-bottom">
            <div class="btn-group " role="group" >
                <!-- Erster Button mit abgerundeten Ecken -->
                <button type="button" class="btn btn-default first" style="border-top-left-radius: 100px; border-bottom-left-radius: 100px; padding: 4px;">
                    <span class="glyphicon glyphicon-step-backward"></span>
                </button>
                <!-- Innerer Button -->
                <button type="button" class="btn btn-default prev" style="border-radius: 0; padding: 4px;">
                    <span class="glyphicon glyphicon-backward" style="padding-right: 3px"></span>
                </button>
            </div>
            <span class="pagedisplay qfq-tablesorter-font" style="border-color: #d6d6d6; border-width: 1px 0px 1px 0px; border-top-style: solid; border-bottom-style: solid;  color: #555; padding-top: 6px; "></span>
            <div class="page-controll" style="width: unset; display: inline;">
                <select class="form-control input-sm pagesize qfq-tablesorter-font" title="Select page size" style="display: inline; width: unset;  border-radius: 0px 0px 0px 0px !important; padding-left: 2px;">
                    <option selected="selected" value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="all">All Rows</option>
                </select><select class="form-control input-sm pagenum qfq-tablesorter-font" title="Select page number" style="display: inline; width: unset; border-radius: 0px 0px 0px 0px !important; padding-left: 2px;">
                </select>
            </div>
            <div class="btn-group" role="group">
                <!-- Innerer Button -->
                <button type="button" class="btn btn-default next" style="border-radius: 0; padding: 4px;">
                    <span class="glyphicon glyphicon-forward" style="padding-left: 3px"></span>
                </button>
                <!-- Letzter Button mit abgerundeten Ecken -->
                <button type="button" class="btn btn-default last" style="border-top-right-radius: 100px; border-bottom-right-radius: 100px; padding: 4px;">
                    <span class="glyphicon glyphicon-step-forward"></span>
                </button>
            </div>
        </div>
    `;
        $(pagerHtml).insertAfter($(table));
        $(table).tablesorterPager({
            container: $("#" + pagerId),
            cssGoto: ".pagenum",
            removeRows: false,
            output: '{startRow} - {endRow} / {filteredRows}'
        });
    };


    n.TablesorterController.prototype._setTablesorterUrlHash = function (parameters) {
        var hash = '';
        for (var key in parameters) {
            // remember last pill delivers own hash which has undefined value. Catch it correctly.
            if (parameters[key] === undefined) {
                hash += ',' + key;
            } else {
                hash += ',' + key + '=' + parameters[key];
            }
        }
        window.location.replace("#" + hash.substr(1));
    };

    n.TablesorterController.prototype._getTablesorterUrlHash = function () {
        var parameterList = window.location.hash.substr(1).split(',');
        var parameters = {};
        parameterList.forEach(function (par) {
            var keyValue = par.split(/=(.+)/);
            parameters[keyValue[0]] = keyValue[1];
        });
        delete parameters[""];
        return parameters;
    };

    n.TablesorterController.prototype._updateTablesorterUrlHash = function (table, value) {
        var tableId = table.attr("data-tablesorter-id");
        var hashParameters = this._getTablesorterUrlHash();
        hashParameters[tableId] = value;
        this._setTablesorterUrlHash(hashParameters);
    };

    n.TablesorterController.prototype._saveTableViewPrompt = function (table, viewNamePreset, isPublicView) {
        var tableId = table.attr("data-tablesorter-id");
        var viewName = prompt("Please enter a name for the view. If it already exists it will be overwritten.", viewNamePreset !== null ? viewNamePreset : "");

        // check if given view name is valid
        if (viewName === "") {
            this._alert("View not saved. Name is empty.");
            return;
        }
        if (viewName === null) {
            return;
        }
        var view = {name: viewName, public: isPublicView, tableId: tableId, view: this.getTableView(table)};

        // check if there are filters set on hidden columns.
        if (view.view.filters.some(function (f, i) {
            return f !== '' && !view.view.columnSelection.includes(i);
        })) {
            if (!confirm('There are filters set on hidden columns. Would you like to save anyway?')) {
                return;
            }
        }
        var that = this;
        var sip = table.attr("data-tablesorter-sip");
        view.view = btoa(JSON.stringify(view.view)); // encode view to base64 to prevent sql injections
        $.post(this.tablesorterApiUrl + "?s=" + sip, view, function (response) {
            that._updateTablesorterUrlHash(table, (view.public ? 'public:' : 'private:') + view.name);
            location.reload(true);
        }, 'json').fail(function (xhr, status, error) {
            that._alert('Error while trying to save view:<br>' + JSON.parse(xhr.responseText).message);
        });
    };

    n.TablesorterController.prototype._parseViewSelectValue = function (value) {
        var splitValue = value.split(/:(.+)/);
        return {name: splitValue[1], public: splitValue[0] === 'public'};
    };

    n.TablesorterController.prototype._alert = function (alertMessage) {
        var messageButtons = [{
            label: "Ok",
            eventName: 'close'
        }];
        var alert = new n.Alert({"message": alertMessage, "type": "error", modal: true, buttons: messageButtons});
        alert.show();
    };

})(QfqNS);