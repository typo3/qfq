/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages multiple Alerts and provides an Alert Container.
     *
     * Is usually initialized with the first Alert that is created. (see n.Alert)
     *
     * @param {object} options For later
     */

    n.AlertManager = function(options) {
        this.screenBlocked = false;
        this.alerts = [];
        this.$container = $("<div>");
        this.blockingAlert = {};
        this.eventEmitter = new EventEmitter();
        this.regularCheck = {};
        this.parent = $(".container") || $(".container-fluid");
        if(this.parent.length === 0) {
            this.parent = $(".container-fluid");
        }

        $("body").append(this.$container);
        console.log("Created Alert Container");
    };

    n.AlertManager.prototype.on = n.EventEmitter.onMixin;

    /**
     * Add an Alert to the Alert Manager
     * @param alert
     */
    n.AlertManager.prototype.addAlert = function(alert) {
        this.alerts.push(alert);
        alert.setIdentifier(this.alerts.length);
        console.log(this.alerts);
    };

    n.AlertManager.prototype.removeAlert = function(identifier) {
        for(var i=0; this.alerts.length > i; i++) {
            if (this.alerts[i].identifier === identifier) {
                this.alerts.splice(i, 1);
            }
        }
    };

    /**
     * Removes the last Alert in the Array. Can be used to safely delete all alerts in a loop.
     * Returns false when the AlertManager has no more Alerts.
     * @returns Boolean
     */
    n.AlertManager.prototype.removeLastAlert = function() {
        if (this.alert.length > 0) {
            var alert = this.alerts.pop();
            alert.removeAlert();
            return true;
        } else {
            return false;
        }
    };

    /**
     * Savely removes outdated Alerts with isShown = false
     */
    n.AlertManager.prototype.removeOutdatedAlerts = function() {
        for(var i = 0; this.alerts.length > i; i++) {
            if(!this.alerts[i].isShown) {
                this.alerts[i].removeAlert();
                this.alerts.splice(i, 1);
            }
        }
    };

    /**
     * Returns the number of Alerts currently active
     * @returns {number}
     */
    n.AlertManager.prototype.count = function() {
        return this.alerts.length;
    };

    /**
     * Creates a semi-transparent black screen behind the alert.
     * Used to block other user input by modal alerts
     * @param alert
     */
    n.AlertManager.prototype.createBlockScreen = function(alert) {
        if (!this.screenBlocked) {
            var $blockScreen = $("<div>")
                .addClass("blockscreenQfq")
                .appendTo(this.$container);
            $blockScreen.css({
                'width': '100%',
                'height': Math.max($(document).height(), $(window).height()) + "px"
            });
            this.parent.addClass("blur");

            var that = this;
            this.screenBlocked = true;
            this.blockingAlert = alert;
            this.regularCheck = setInterval(function () {
                that.checkAlert();
            }, 500);
        }
    };

    /**
     * Is used by the interval this.regularcheck to guarantee
     * that the screen block is removed.
     */
    n.AlertManager.prototype.checkAlert = function() {
        if (!this.blockingAlert.isShown) {
            this.removeModalAlert();
        }
    };

    /**
     * Remove modal alerts
     */
    n.AlertManager.prototype.removeModalAlert = function() {
        if (this.screenBlocked) {
            $(".blockscreenQfq").remove();
            this.parent.removeClass("blur");
            clearInterval(this.regularCheck);
            this.screenBlocked = false;
        }
        this.removeOutdatedAlerts();
    };

})(QfqNS);