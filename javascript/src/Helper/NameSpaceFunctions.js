/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     *
     * @param jqHXR
     * @param textStatus
     * @param errorThrown
     *
     * @function QfqNS.Helper.showAjaxError
     */
    n.showAjaxError = function (jqHXR, textStatus, errorThrown) {
        var alert = new QfqNS.Alert("Error:<br> " +
            errorThrown, "error");
        alert.show();
    };

    /**
     *
     * @param string
     * @returns {*}
     *
     * @function QfqNS.Helper.stringBool
     */
    n.stringToBool = function (string) {
        if (typeof string !== "string") {
            return string;
        }
        var lowerCase = string.toLowerCase().trim();

        switch (lowerCase) {
            case "1":
            case "yes":
            case "y":
            case "t":
            case "true":
            case "enabled":
            case "enable":
                return true;
            case "0":
            case "no":
            case "n":
            case "f":
            case "false":
            case "disabled":
            case "disable":
                return false;
            default:
                return false;
        }
    };
})(QfqNS.Helper);