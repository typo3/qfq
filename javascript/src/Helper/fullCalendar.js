/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global console */
/* global FullCalendar */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes fullcalendar.js. 
     *
     * The Full Calendar configuration has to be provided in the `data-config` attribute as JSON. E.g.
     *
     *      <div class="qfq-calendar" data-config='{ "plugins": [ 'dayGrid' ] }'></textarea>
     *
     * @function
     */
    var fullCalendar = function (form) {
        if (typeof FullCalendar === 'undefined') {
            console.log("FullCalendar JavaScript Files not loaded.");
            return;
        }

        $("div.qfq-calendar").each(
            function () {
                var config = {};
                var $this = $(this);
                //var element = $this.get(0);
                var configData = $this.data('config');
                if (configData) {
                    if (configData instanceof Object) {
                        // jQuery takes care of decoding data-config to JavaScript object.
                        config = configData;
                    } else {
                        QfqNS.Log.warning("'data-config' is invalid: " + configData);
                    }
                }
                $this.fullCalendar(configData);
                //calendar.render();
            }
        );
    };

    n.calendar = fullCalendar;

})(QfqNS.Helper);