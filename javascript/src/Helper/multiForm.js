/**
 * @author Zhoujie li <zhoujie.li@math.uzh.ch>
 * @Date: 13/05/2024
 */

/* global $ */
/* global tinymce */
/* global CodeMirror */

/**
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
var multiFormRowCounter = 0;  // Initialize a counter to keep track of added rows

/**
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    // Event listener for DOMContentLoaded to ensure the DOM is fully loaded before executing the script
    document.addEventListener('DOMContentLoaded', function () {


        // Function to add a new row
        function addRow() {
            const dummyRow = document.querySelector('.table-multi-form tr.dummy-row');
            if (!dummyRow) {
                return; // Exit if no dummy row found
            }

            // Clear any existing TinyMCE instances in the dummy row
            clearTinyMCEInstances(dummyRow);
            // Clear any existing CodeMirror instances in the dummy row
            clearCodeMirrorInstances(dummyRow);

            // Clone the dummy row and update its class
            const newRow = dummyRow.cloneNode(true);
            newRow.classList.remove('dummy-row');
            updateFormElementNames(newRow); // Update the form element names with new indices
            multiFormRowCounter++;

            // Append the new row to the table
            const table = document.querySelector('.table-multi-form tbody');
            if (table) {
                table.appendChild(newRow);
            }

            // Initialize form element listeners and CodeMirror for the new row
            initializeFormElementListeners(newRow);
            initializeCodeMirrorForRow(newRow);
        }

        // Function to update the names of form elements in a row
        function updateFormElementNames(row) {
            const formElements = row.querySelectorAll('input, select, textarea');
            formElements.forEach((formElement) => {
                if (formElement.name) {
                    const parts = formElement.name.split('-');
                    if (parts && parts.length === 3) {
                        const newIndex = multiFormRowCounter; // parseInt(parts[2]) + multiFormRowCounter;
                        formElement.name = parts[0] + '-' + parts[1] + '-' + newIndex;
                    }
                }

                // Update the id if the element has the class qfq-tinymce
                if (formElement.id && formElement.classList.contains('qfq-tinymce')) {
                    const idParts = formElement.id.split('-');
                    if (idParts.length > 1) {
                        const lastPart = idParts[idParts.length - 1];
                        // Ensure the last part is a number
                        const lastIndex = parseInt(lastPart);
                        idParts[idParts.length - 1] = lastIndex + multiFormRowCounter + 1;
                        // Add '-tinymce' to the id
                        formElement.id = idParts.join('-')
                    }
                }

                // Add an input event listener to update the checkbox state
                formElement.addEventListener('input', function () {
                    const checkbox = row.querySelector('input[type="checkbox"]');
                    if (checkbox && !checkbox.dataset.userChanged) {
                        checkbox.dataset.userChanged = true;
                        checkbox.checked = true;
                    }
                });
            });
        }

        // Function to initialize CodeMirror for a given row
        function initializeCodeMirrorForRow(row) {
            const textareas = row.querySelectorAll('textarea.qfq-codemirror');
            textareas.forEach((textarea) => {
                if (typeof CodeMirror !== 'undefined') {
                    const configData = textarea.dataset.config ? JSON.parse(textarea.dataset.config) : {};
                    const cm = CodeMirror.fromTextArea(textarea, configData);
                    let height = textarea.dataset.height;
                    let width = textarea.dataset.width;

                    // Ensure height and width are valid
                    height = height && parseInt(height) > 0 ? height : 'auto';
                    width = width && parseInt(width) > 0 ? width : (width === 0 ? 'auto' : null);

                    cm.setSize(width, height);

                    // Add change event listener to update the hidden textarea and check the checkbox
                    cm.on('change', function () {
                        textarea.value = cm.getValue();
                        const checkbox = row.querySelector('input[type="checkbox"]');
                        if (checkbox && !checkbox.dataset.userChanged) {
                            checkbox.dataset.userChanged = true;
                            checkbox.checked = true;
                        }
                    });
                }
            });
        }

        // Function to clear existing CodeMirror instances in a row
        function clearCodeMirrorInstances(row) {
            const codeMirrorDivs = row.querySelectorAll('.CodeMirror');
            codeMirrorDivs.forEach(div => {
                const cmInstance = div.CodeMirror;
                if (cmInstance) {
                    cmInstance.toTextArea();
                    cmInstance.setValue(''); // Clear the CodeMirror content
                }
                div.remove();
            });
        }

        // Function to delete a row
        function deleteRow(event) {
            if (event.target.classList.contains('deleteRowBtn') || event.target.closest('.deleteRowBtn')) {
                const rowDelete = event.target.closest('tr');
                if (rowDelete) {
                    rowDelete.remove();
                }
            }
        }

        // Function to handle checkbox change event
        function handleCheckboxChange(event) {
            const checkbox = event.target;
            if (checkbox.type === 'checkbox') {
                checkbox.dataset.userChanged = checkbox.checked ? 'true' : '';
            }
        }

        // Function to initialize existing rows on page load
        function initializeExistingRows() {
            const existingRows = document.querySelectorAll('.table-multi-form tbody tr');
            existingRows.forEach(row => {
                updateFormElementNames(row);
                initializeFormElementListeners(row);
                initializeCodeMirrorForRow(row); // Initialize CodeMirror for existing rows
                multiFormRowCounter++;
            });
        }

        // Function to initialize form element listeners in a row
        function initializeFormElementListeners(row) {
            const formElements = row.querySelectorAll('input, select, textarea');
            formElements.forEach((formElement) => {
                formElement.addEventListener('input', function () {
                    const checkbox = row.querySelector('input[type="checkbox"]');
                    if (checkbox && !checkbox.dataset.userChanged) {
                        checkbox.dataset.userChanged = true;
                        checkbox.checked = true;
                    }
                });

                // Initialize TinyMCE and CodeMirror change events
                if (formElement.classList.contains('qfq-tinymce')) {
                    tinymce.init({
                        selector: '#' + formElement.id,
                        setup: function (editor) {
                            editor.on('input', function () {
                                const checkbox = row.querySelector('input[type="checkbox"]');
                                if (checkbox) {
                                    checkbox.dataset.userChanged = true;
                                    checkbox.checked = true;
                                }
                                const event = new Event('input', { bubbles: true });
                                editor.getElement().dispatchEvent(event);
                                QfqNS.Form.prototype.changeHandler(event);
                            });
                        }
                    });
                }
            });
        }

        // Function to clear existing TinyMCE instances in a row
        function clearTinyMCEInstances(row) {
            const textareas = row.querySelectorAll('textarea.qfq-tinymce');
            textareas.forEach((textarea) => {
                const editor = tinymce.get(textarea.id);
                if (editor) {
                    editor.remove();
                }
            });
        }

        // Add event listeners for delete row and checkbox change events on the table body
        const tableBody = document.querySelector('.table-multi-form tbody');
        if (tableBody) {
            tableBody.addEventListener('click', deleteRow);
            tableBody.addEventListener('change', handleCheckboxChange);
        }

        // Add event listener to the add row button
        let addRowBtn = document.querySelector('button.addRowButton[id^="addRowBtn-qfq-form"]');
        if (addRowBtn) {
            addRowBtn.addEventListener('click', addRow);
        }

        // Initialize existing rows on page load only if the table has the class 'table-multi-form'
        if (document.querySelector('.table-multi-form')) {
            initializeExistingRows();
        }
    });
})(QfqNS.Helper);
