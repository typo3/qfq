/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global console */
/* global CodeMirror */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes codemirror. Only `<textarea/>` elements having the class `qfq-codemirror` are initialized.
     *
     * The codemirror configuration has to be provided in the `data-config` attribute as JSON. E.g.
     *
     *      <textarea class="qfq-codemirror" data-config='{ "mode": "qfq", "lineNumbers": true }'></textarea>
     *
     * @function
     */
    var codemirror = function () {
        if (typeof CodeMirror === 'undefined') {
            return;
        }

        $("textarea.qfq-codemirror:not(.cm-extern)").filter(function() {
            return !$(this).next().hasClass("CodeMirror");
        }).each(function () {
            var cmFocusOn = false;
            var oldElement = '';
            var config = {};
            var $this = $(this);
            var height = $this.data('height');
            var width = $this.data('width');
            var configData = $this.data('config');
            if (configData) {
                if (configData instanceof Object) {
                    config = configData;
                } else {
                    QfqNS.Log.warning("'data-config' is invalid: " + configData);
                }
            }
            var cm = CodeMirror.fromTextArea(this, configData);
                // Handle width and height settings
                height = height && parseInt(height) > 0 ? height : 'auto';
                width = width && parseInt(width) > 0 ? width : (width === 0 ? 'auto' : null);

                // Set Home key behavior to move to the start of the visual line
                CodeMirror.keyMap.default.Home = "goLineLeftSmart";

                // Set END key behavior to move to the end of  the visual line
                // CodeMirror.keyMap.default.End = "goLineRightSmart";

                cm.setSize(width, height);

            cm.on('change', (function ($form, $textArea) {
                return function (instance, changeObj) {
                    var actualValue = cm.getValue();
                    if (actualValue !== oldElement && cmFocusOn) {
                        $form.change();
                    }
                };
            })($(this).closest('form'), $this));

                    cm.on('focus', function () {
                        oldElement = $this.val();
                        cmFocusOn = true;
                    });

                    cm.on('blur', function () {
                        var actualValue = cm.getValue();
                        if (actualValue !== oldElement && cmFocusOn) {
                            $this.val(actualValue);
                            $this.trigger('change');
                        }
                        cmFocusOn = false;
                    });

                    // If codemirror has been loaded hidden, refresh once visible
                    $('a[data-toggle="tab"]').on('shown.bs.tab', function() {
                        if(cm.getGutterElement().clientHeight === 0) return;
                        if(cm.getGutterElement().style.getPropertyValue("height")) return;
                        cm.refresh();
                    });

                }
            );

    };

    n.codemirror = codemirror;

})(QfqNS.Helper);

// Separate js to force codemirror for reports in frontend. No Form needed with this.
$(document).ready(function () {
    document.activeElement.blur();
    var externWindow = $(".externWindow");
    var targetEditReportButton = $(".targetEditReport");
    var htmlContent = $('');

    // select all edit buttons from content records and remove onclick attribute to prevent showing content in primary window if clicked. onclick attribute is not removed in php to have the opportunity for old way of editing in front end.
    if (targetEditReportButton !== undefined) {
        for (var i = 0; targetEditReportButton.length > i; i++) {
            var currentTarget = targetEditReportButton[i];
            $(currentTarget).removeAttr("onclick");
        }
    }

    // We prepare the content for extern window and show it. Only if onclick doesn't exist. Compatibility for old way is given this way.
    $(targetEditReportButton).click(function () {
        var baseUrl = $(this).data('base-url');
        if (!$(this).is("[onclick]")) {
            var formContent = $($(this).next()[0].outerHTML);
            showHtmlEditor(formContent, baseUrl);
        }
    });

    //function to show editor window
    function showHtmlEditor(formContent, baseUrl) {
        $(formContent[0]).removeAttr("class");
        $(formContent[0]).addClass("externWindow");
        var idNameForWindow = $(formContent[0]).attr('id');
        htmlContent = '<!DOCTYPE html>' + $("head").html() + $(formContent)[0].outerHTML;
        newWindow(idNameForWindow);
    }

    // function to create new window with given content for editing
    function newWindow(windowName) {
        var w = window.open('//' + location.host + location.pathname + '?tt-content=' + windowName, windowName, 'width=900,height=700');
        w.document.write(htmlContent);
        w.document.close();
    }

    // Show same content editor with refreshed data again after save. Control it with the given get parameter. First fetch only needed html content again (form) and open it in same window.
    var urlParams = new URLSearchParams(window.location.search);
    var ttContentParam = urlParams.get('tt-content');

    if (ttContentParam !== null && $(targetEditReportButton).next("#" + ttContentParam)[0] !== undefined) {
        var formContent = $($(targetEditReportButton).next("#" + ttContentParam)[0].outerHTML);
        showHtmlEditor(formContent);
    }

    // execute changes(post) and reload page with id of tt-content as get parameter. Staying in same window with new content.
    $(externWindow).submit(function () {
        $.post($(externWindow).attr('action'), $(externWindow).serializeArray())
            .done(function (data) {
                var badge;
                if (data.status === "error") {
                    data.message = data.message.replace(/(<([^>]+)>)/gi, "\n");
                    alert(data.message);
                    badge = '<span style="position:absolute; top:5px; right:5px; background-color: red;" class="badge badge-danger">Failed</span>';
                } else {
                    badge = '<span style="position:absolute; top:5px; right:5px; background-color: green;" class="badge badge-success">Saved</span>';
                }
                $(badge)
                    .insertBefore('.save-message')
                    .delay(3000)
                    .fadeOut(function () {
                        $(this).remove();
                    });
            });
        return false;
    });

    // enable CodeMirror for extern window
    $(externWindow).children(".qfq-codemirror").each(
        function () {
            var config = {};
            var $this = $(this);
            var configData = $this.data('config');
            if (configData) {
                if (configData instanceof Object) {
                    // jQuery takes care of decoding data-config to JavaScript object.
                    config = configData;
                } else {
                    QfqNS.Log.warning("'data-config' is invalid: " + configData);
                }
            }

            // Add viewportMargin to the configuration, makes whole content searchable
            configData.viewportMargin = Infinity;

            var cm = CodeMirror.fromTextArea(this, configData);
            cm.on('change', (function ($form, $textArea) {
                return function (instance, changeObj) {
                    $textArea.val(instance.getValue());
                    $form.change();
                };

            })($(this).closest('form'), $this));
            // This added class is needed to not fire the script more than once
            $(this).addClass("cm-done");
            // For the extern window we use the whole place to show CodeMirror
            $(externWindow).css('height', '100%');
            var heightHeader = parseFloat($(this).prev()[0].offsetHeight / $(externWindow).height ()) * 100;
            var heightMain = 100 - heightHeader;
            $(this).next().css('height', heightMain + '%');

            window.addEventListener('resize', function(e){
                var heightHeader = parseFloat($('.tt-content-bar')[0].offsetHeight / $(externWindow).height()) * 100;
                var heightMain = 100 - heightHeader;
                $('.CodeMirror').css('height', heightMain + '%');
            }, true);
        }
    );
});