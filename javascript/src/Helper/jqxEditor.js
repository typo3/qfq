/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize a jqxEditor widget on all `<textarea>` elements having the class `jqw-editor`.
     * @function
     * @name QfqNS.Helper.jqxEditor
     */
    var jqxEditor = function () {
        var index;
        var $containers = $("textarea.jqw-editor");

        $containers.each(function (index, object) {
            (function ($container) {
                $container.jqxEditor();
            })($(object));
        });

    };

    n.jqxEditor = jqxEditor;


})(QfqNS.Helper);