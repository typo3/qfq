/**
 * @author Enis Nuredini <enis.nuredini@math.uzh.ch>
 */

/* global console */
/* global qfqChat */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    let qfqHistory = function (historyBtnElement, n) {
        if (historyBtnElement !== undefined) {
            new HistoryView(historyBtnElement, n);
        }
    }

    class HistoryView {
        constructor(historyBtnElement, n) {
            this.modalTitle = historyBtnElement.getAttribute("data-history-title");

            // Get existing history view template
            let template = document.getElementsByClassName('qfq-history-view')[0];
            if (template === undefined) {
                template = this.createModalTemplate();
                document.body.appendChild(template);
            }

            this.historyView = template;
            this.historyViewBody = this.historyView.getElementsByClassName('modal-body')[0];
            this.historyBtn = historyBtnElement;
            this.baseUrl = historyBtnElement.getAttribute("data-base-url");
            this.restCallUrl = this.baseUrl + 'typo3conf/ext/qfq/Classes/Api/load.php?s=';
            this.dataSipRules = historyBtnElement.getAttribute("data-sip");
            this.tablesorterViewJson = historyBtnElement.getAttribute("data-tablesorter-view-json");
            this.formElements = {};
            this.formId = null;
            this.header = null;
            this.rows = {};
            this.takeBtn = null;
            this.data = null;
            this.formLabelData = null;
            this.formNames = null;
            this.form = n.form;
            this.table = null;
            this.checkboxes = [];
            this.tablesorterController = new n.TablesorterController();

            this.initHistoryBtn();
        }

        initHistoryBtn() {
            let that = this;
            this.historyBtn.addEventListener('click', function () {
                that.initView();
                setTimeout(function () {
                    that.initCheckboxes();
                }, 500);
            });
        }

        // Initialize the basic history view
        async initView() {
            let url = this.restCallUrl + this.dataSipRules;
            // Create a new html content for the modal window
            this.historyViewBody.innerHTML = 'Loading...';
            $(this.historyView).modal('show');

            let data = await this.makeApiCall(url);
            // extract formLabelData and formNames from data
            this.formLabelData = data.formLabelData;
            this.formNames = data.formNames;
            this.formId = data.formId;

            // Separate the formLabelData, formNames and formId from the data
            delete data.formLabelData;
            delete data.formNames;
            delete data.formId;
            this.data = data;

            // Map the formLabelData to keep order
            this.formLabelData = new Map(this.formLabelData);

            // Put historyHtml in to the modal window
            this.historyViewBody.innerHTML = this.buildHistoryView();
            this.table = $(this.historyViewBody).find('table');
            this.tablesorterController.setup($(this.historyViewBody).find('table'));
            this.takeBtn = this.buildTakeBtn();
            this.historyViewBody.appendChild(this.takeBtn);
            this.initTakeBtn(this);
            this.initShowMoreTextBtn();
        }

        initCheckboxes() {
            // Get all main checkboxes
            let mainCheckboxes = this.historyView.getElementsByClassName('main-checkbox');

            // Get all other checkboxes
            this.checkboxes = this.historyView.getElementsByClassName('checkbox');
            let checkboxes = this.checkboxes;

            // Add event listener to each main checkbox
            Array.from(mainCheckboxes).forEach(cb => {
                cb.addEventListener('click', function () {
                    let created = cb.getAttribute('data-created');
                    Array.from(checkboxes).forEach(c => {
                        const td = c.parentNode;
                        if (c.getAttribute('data-created') === created && td.clientWidth !== 0) {
                            c.checked = cb.checked;
                        } else if (cb.checked === true) {
                            c.checked = false;
                        }
                    });

                    // Uncheck all the other main checkboxes
                    Array.from(mainCheckboxes).forEach(c => {
                        if (c.getAttribute('data-created') !== created) {
                            c.checked = false;
                        }
                    });
                });
            });

            // Add event listener to other checkboxes.
            // If one is checked then the others with same data-name should be unchecked
            Array.from(checkboxes).forEach(cb => {
                cb.addEventListener('click', function () {
                    let name = cb.getAttribute('data-name');
                    let created = cb.getAttribute('data-created');
                    Array.from(checkboxes).forEach(c => {
                        if (c.getAttribute('data-name') === name && c.getAttribute('data-created') !== created) {
                            c.checked = false;
                        }
                    });
                });
            });

        }

        initTakeBtn(that) {
            this.takeBtn.addEventListener('click', function () {
                let data = {};
                let output = '';

                Array.from(that.checkboxes).forEach(cb => {
                    let name = cb.getAttribute('data-name');

                    if (cb.checked) {
                        data[name] = cb.value;
                    }
                });

                output = that.buildSelectedOutput(data);


                if (confirm(output)) {

                    console.log("Checked data: ", data);

                    that.setFormValues(data);
                    that.form.inputAndPasteHandler(that.form);
                    // Close the modal
                    $(that.historyView).modal('hide');
                }
            });
        }

        initShowMoreTextBtn() {
            // Get all more text buttons
            let moreTextBtns = this.historyView.getElementsByClassName('btn-link');

            // Add event listener to each more text button
            Array.from(moreTextBtns).forEach(btn => {
                btn.addEventListener('click', function () {
                    // Get the previous sibling span element
                    const span = btn.previousElementSibling;
                    if (span.style.display === 'block') {
                        span.style.display = 'none';
                        btn.textContent = '[...]';
                    } else {
                        span.style.display = 'block';
                        btn.textContent = '[<<]';
                    }

                });
            });
        }

        async makeApiCall(url) {
            const response = await fetch(url, {method: 'POST'});
            let data = null;
            if (!response.ok) {
                throw new Error();
            } else {
                data = await response.json();
            }

            return data['record-history-data'];
        }

        buildHistoryView() {
            let data = this.data;
            let formLabelData = this.formLabelData;
            let dataObj = data;
            let html = '';
            let tablesorterConfig = this.getTableSorterConfigJson();
            let tablesorterView = JSON.parse(this.tablesorterViewJson);
            let tablesorterHtml = tablesorterView.data;
            html += '<div class="table-responsive"><table id="qfq-history-' + this.formId + '" ' + tablesorterHtml + ' data-tablesorter-config=\'' + tablesorterConfig + '\' class="table table-bordered table-hover qfq-table-50 tablesorter tablesorter-filter tablesorter-pager tablesorter-column-selector table-condensed">';

            let actualFormValues = this.getCurrentFormValues();
            actualFormValues = this.reorderArray(formLabelData, actualFormValues);
            let tablesorterHeader = this.buildTablesorterHeader(formLabelData, actualFormValues);
            let tablesorterBody = this.buildTablesorterBody(dataObj);
            html += tablesorterHeader;
            html += tablesorterBody;
            html += '</table></div>';

            return html;
        }

        // formLabelData is an array of label and name: [label1 => name1, label2 => name2, label3 => name3]
        // build header with that array
        buildTablesorterHeader(formLabelData, actualFormValues) {
            let header = '<thead class="qfq-sticky">';
            let headerRow = '<tr>';

            // Summary checkbox position
            headerRow += '<th class="filter-false sorter-false"></th>';
            headerRow += '<th>Created</th>';
            headerRow += '<th>feUser</th>';

            // Add the name as data-label attribute to the cell inside the iterating formLabelData
            formLabelData.forEach((label, name) => {
                if (label === '') {
                    label = '(' + name + ')';
                }
                headerRow += '<th data-name="' + name + '">' + label + '</th>';
            });

            headerRow += '</tr>';
            header += headerRow;

            header += this.buildFirstRow(actualFormValues);
            header += '</thead>';

            return header;
        }

        // dataObj is an array of arrays: [created => [formdata => [value = test1, label = label1], feUser => username1], created2 => [formdata => [value = test2, label = label2], feUser => username2]]
        // build row with that array. first cell is created date. second cell is feUser value. Rest of the cells are formdata values
        buildTablesorterBody(dataObj) {
            let body = '<tbody>';
            let rows = '';
            let that = this;

            Object.entries(dataObj).forEach(([created, row], index, array) => {
                let feUser = row.feUser;
                let formdata = row.formData;
                let unchangedData = row.unchangedData;

                let rowHtml = '<tr>';
                // Add a checkbox in the first cell
                rowHtml += '<td><input type="checkbox" class="main-checkbox" data-created="' + created + '"></td>';
                rowHtml += '<td>' + created + '</td>';
                rowHtml += '<td>' + feUser + '</td>';

                // Add a checkbox in each cell at the end of the cell
                Object.entries(formdata).forEach(([key, fd], fdIndex, fdArray) => {
                    let plainText = that.stripTags(fd.value);
                    let strippedText = that.truncateWithMoreText(plainText);
                    let changedData = fd.name in unchangedData  ? false : true;
                    let tooltip = '';

                    if (strippedText === '') {

                        // If data is unchanged by another form, use empty string
                        strippedText = changedData ? '<span style="font-style: italic;">empty</span>' : '';
                        tooltip = changedData ? '' : ' title="Value of this field was not changed."';

                    } else if (strippedText === '*hide in log*') {

                        // If data is hidden in log, use 'hidden in log'
                        strippedText = changedData ? '<span style="font-style: italic;" title="Value of this field is excluded from the log.">hidden in log</span>' : '';

                        // No checkbox will be used
                        changedData = false;
                    }

                    // Only use checkbox if data has been changed
                    let checkbox = changedData ? that.buildCheckbox(created, fd.name, fd.value) : '';

                    // Compare with next row's value if it exists
                    let comparisonHtml = '';
                    if (index < array.length - 1) {
                        let nextRow = array[index + 1][1];
                        let nextFd = nextRow.formData[key];
                        if (nextFd && fd.value !== nextFd.value && changedData) {
                            strippedText = '<strong>' + strippedText + '</strong>';
                        }

                    } else {
                        strippedText = '<strong>' + strippedText + '</strong>';
                    }

                    rowHtml += '<td' + tooltip + '><div style="display: flex; align-items:flex-start; gap: 5px">' + checkbox + ' ' + strippedText + '</div></td>';
                });

                rowHtml += '</tr>';
                rows += rowHtml;
            });

            body += rows;
            body += '</tbody>';
            return body;
        }

        buildCheckbox(created, name, value) {
            let checkbox = '';
            checkbox += '<input type="checkbox" class="checkbox" data-name="' + name + '" data-created="' + created + '" value="' + value + '">';

            return checkbox;
        }

        buildFirstRow(actualFormValues) {
            let that = this;
            let row = '<tr class="static-row">';
            // Add a checkbox in the first cell
            row += '<th class="filter-false sorter-false"></th>';
            row += '<th class="filter-false sorter-false">Current</th>';
            row += '<th class="filter-false sorter-false"></th>';
            actualFormValues.forEach((value, key) => {
                let plainText = that.stripTags(value);
                let strippedText = that.truncateWithMoreText(plainText);
                if (strippedText === '') {
                    strippedText = '<span style="font-style: italic;">empty</span>';
                }
                row += '<th class="filter-false sorter-false">' + strippedText + '</th>';
            });
            row += '</tr>';

            return row;
        }

        buildTakeBtn() {
            let takeBtn = document.createElement('button');
            takeBtn.setAttribute('id', 'take-btn');
            takeBtn.setAttribute('class', 'btn btn-default');
            takeBtn.innerHTML = 'Restore selected values';
            return takeBtn;
        }


        getCurrentFormValues() {
            // Get all elements of the form which has name given from array formNames
            let form = this.form.$form[0];
            let formNames = this.formNames;
            let formValues = [];
            // iterate over formNames
            Object.values(formNames).forEach(name => {
                let formElement = form.querySelector('[name="' + name + '"]');
                let type = 'input';
                this.formElements[name] = {};

                if (formElement !== null) {
                    let value = '';
                    // Check for type radio
                    if (formElement.getAttribute('type') === 'radio') {
                        formElement = form.querySelector(`[name="${name}"]:checked`);
                        if (formElement === null) {
                            formElement = form.querySelector(`[name="${name}"]`);
                        }
                        type = 'radio';
                    }

                    // Check for codemirror
                    if (formElement.classList.contains('qfq-codemirror')) {
                        type = 'codemirror';
                    }

                    // Check for tinyMce
                    if (formElement.classList.contains('qfq-tinymce')) {
                        type = 'tinymce';
                    }

                    // Single checkbox and typeahead handling. Usually hidden input
                    if (formElement.getAttribute('type') === 'hidden') {

                        // Different between checkbox and typeahead
                        // Get the parent div element and check for first child with class twitter-typeahead
                        const parent = formElement.parentNode;
                        const typeahead = parent.querySelector('.twitter-typeahead');

                        if (typeahead !== null && parent.classList.contains('qfq-form-body') === false) {
                            type = 'typeahead';
                            value = formElement.value;
                        } else {
                            // Get the other element with the same name. Exclude the hidden input (Checkbox)
                            const formElementNew = form.querySelector(`[name="${name}"]:not([type="hidden"])`);

                            if (formElementNew !== null) {
                                type = 'checkbox';

                                if (formElementNew.checked === false) {
                                    value = formElement.value;
                                } else {
                                    value = formElementNew.value;
                                }
                                formElement = formElementNew;
                            } else {
                                // uploads are handled here
                                value = formElement.value;
                            }
                        }
                    } else {
                        value = formElement.value;
                    }

                    this.formElements[name].element = formElement;
                    this.formElements[name].value = value;
                    this.formElements[name].type = type;
                    formValues.push({name: name, value: value});
                } else {
                    // In case of checkboxes
                    formElement = form.querySelectorAll(`[name="${name}[]"]`);
                    if (formElement !== null) {
                        let values = [];
                        formElement.forEach(el => {
                            // Get checkmark element
                            const checkmark = el.nextElementSibling;
                            if (el.checked === true) {
                                values.push(el.value);
                            }
                        });

                        this.formElements[name].element = formElement;
                        this.formElements[name].value = values;
                        this.formElements[name].type = 'checkbox';

                        formValues.push({name: name, value: values});
                    }
                }
            });

            return formValues;
        }

        reorderArray(referenceArray, arrayToReorder) {
            const result = new Map();
            const placeholder = '';

            // Convert arrayToReorder to a Map for easier lookup
            const arrayToReorderMap = new Map(
                arrayToReorder.map(item => [item.name, item.value])
            );

            // Process all keys from the reference array (Map)
            for (const [key, value] of referenceArray) {
                if (arrayToReorderMap.has(key)) {
                    result.set(key, arrayToReorderMap.get(key));
                } else {
                    result.set(key, placeholder);
                }
            }

            return result;
        }

        setFormValues(data) {
            let that = this;
            Object.entries(data).forEach(([name, value]) => {
                if (this.formElements[name]) {
                    const {element, type} = this.formElements[name];

                    switch (type) {
                        case 'radio':
                            const radioButton = element;
                            if (radioButton) {
                                radioButton.Attributes.remove('checked');

                                // Get the radio button with the same name and value
                                const parent = radioButton.closest('div');
                                const radioButtonNew = parent.querySelector(`input[name="${name}"][value="${value}"]`);

                                // Get parent label element
                                const label = radioButtonNew.closest('label');
                                console.log('label: ', label);
                                label.click();
                                radioButtonNew.checked = true;
                            }
                            break;

                        case 'checkbox':
                            if (element instanceof NodeList) {
                                element.forEach(checkbox => {
                                    if (Array.isArray(value)) {
                                        checkbox.checked = value.includes(checkbox.value);
                                    } else if (typeof value === 'string') {
                                        checkbox.checked = value.split(',').includes(checkbox.value);
                                    } else {
                                        checkbox.checked = value == checkbox.value;
                                    }
                                });
                            } else {
                                element.checked = value === element.value;
                            }
                            break;

                        case 'codemirror':
                            if (element) {
                                // Select next sibling element with class CodeMirror
                                const codemirror = element.nextElementSibling.CodeMirror;
                                element.value = value;
                                codemirror.setValue(value);
                            }
                            break;

                        case 'tinymce':
                            if (tinymce && tinymce.get(element.id)) {
                                tinymce.get(element.id).setContent(value);
                            }
                            break;

                        default:
                            element.value = value;
                    }


                    this.formElements[name].value = value;
                }
            });

        }

        makeTdTextBold(checkbox) {
            // Get the parent td element
            const td = checkbox.closest('td');

            const textNode = td.childNodes[0];

            if (textNode && textNode.nodeType === Node.TEXT_NODE) {
                // Create a new strong element
                const strongElement = document.createElement('strong');

                // Set the text content of the strong element
                strongElement.textContent = textNode.textContent.trim();

                // Replace the text node with the strong element
                td.insertBefore(strongElement, textNode);
                td.removeChild(textNode);
            }
        }

        resetBoldTdText(checkboxes) {
            checkboxes.forEach(checkbox => {
                // Get the parent td element
                const td = checkbox.closest('td');

                const strongElement = td.querySelector('strong');

                if (strongElement) {
                    // Create a new text node
                    const textNode = document.createTextNode(strongElement.textContent);

                    // Replace the strong element with the text node
                    td.insertBefore(textNode, strongElement);
                    td.removeChild(strongElement);
                }
            });
        }

        getTableSorterConfigJson() {
            let tablesorterConfig = {
                theme: "bootstrap",
                widthFixed: true,
                headerTemplate: "{content} {icon}",
                dateFormat: "ddmmyyyy",
                widgets: ["uitheme", "filter", "columnSelector", "output"],
                widgetOptions: {
                    filter_columnFilters: true,
                    filter_reset: ".reset",
                    filter_cssFilter: "form-control",
                    filter_saveFilters: true,
                    columnSelector_mediaquery: false,
                    output_delivery: "download",
                    output_saveFileName: "tableExport.csv",
                    output_separator: ";"
                }
            };

            return JSON.stringify(tablesorterConfig);
        }

        truncateWithMoreText(input, maxlen = 30, plaintext = false) {
            if (typeof input !== 'string') {
                return input;
            }

            if (maxlen < 1) {
                maxlen = 1;
            }

            if (input.length > maxlen) {
                const visibleText = input.slice(0, maxlen);
                const hiddenText = input.slice(maxlen);
                if (plaintext) {
                    return `${visibleText}...`;
                } else {
                    return `${visibleText}<span class="qfq-more-text">${hiddenText}</span><button class="btn btn-link" type="button" style="outline-width: 0px;">[...]</button>`;
                }
            } else {
                return input;
            }
        }

        stripTags(input, allowed) {
            if (typeof input !== 'string') {
                return input;
            }
            allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
            const tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
            return input.replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
        }

        // Create modal template for the history view
        createModalTemplate() {
            let modal = document.createElement('div');
            modal.setAttribute('class', 'modal fade qfq-history-view');
            modal.setAttribute('role', 'dialog');

            let modalDialog = document.createElement('div');
            modalDialog.setAttribute('class', 'modal-dialog');

            let modalContent = document.createElement('div');
            modalContent.setAttribute('class', 'modal-content');

            let modalHeader = document.createElement('div');
            modalHeader.setAttribute('class', 'modal-header');

            let modalTitle = document.createElement('h2');
            modalTitle.setAttribute('class', 'modal-title');
            modalTitle.style.display = 'contents';
            let titleContent = '';
            if (this.modalTitle) {
                titleContent = 'History: ' + this.modalTitle;
            } else {
                titleContent = 'History';
            }
            modalTitle.textContent = titleContent;

            let modalCloseBtn = document.createElement('button');
            modalCloseBtn.setAttribute('type', 'button');
            modalCloseBtn.setAttribute('class', 'close');
            modalCloseBtn.setAttribute('data-dismiss', 'modal');

            let modalCloseBtnSpan = document.createElement('span');
            modalCloseBtnSpan.setAttribute('aria-hidden', 'true');
            modalCloseBtnSpan.innerHTML = '&times;';

            let modalBody = document.createElement('div');
            modalBody.setAttribute('class', 'modal-body');

            modalCloseBtn.appendChild(modalCloseBtnSpan);
            modalHeader.appendChild(modalTitle);
            modalHeader.appendChild(modalCloseBtn);
            modalContent.appendChild(modalHeader);
            modalContent.appendChild(modalBody);
            modalDialog.appendChild(modalContent);
            modal.appendChild(modalDialog);

            return modal;
        }

        buildSelectedOutput(data) {
            let labels = this.formLabelData;
            let output = 'Restore following selected values:\n';
            output += '---------------------------------\n';
            Object.entries(data).forEach(([name, value]) => {
                let strippedValue = this.stripTags(value);
                let truncatedValue = this.truncateWithMoreText(strippedValue, 80, true);

                let displayLabel = labels.get(name) || ('(' + name + ')');

                output += '- ' + displayLabel + ': ' + truncatedValue + '\n';
            });
            output += '---------------------------------\n';

            return output;
        }


    }

    n.qfqHistory = qfqHistory;

})(QfqNS.Helper);