/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global console */
/* global tinymce */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize tinyMce. All HTML elements having the `qfq-tinymce` class will be initialized. TinyMce configuration
     * is provided in the `data-config` attribute. Each element further requires the `id` attribute to be present.
     *
     * @function
     */
    var tinyMce = function () {

        if (typeof tinymce === 'undefined') {
            //QfqNS.log.error("tinymce not loaded, cannot initialize Qfq tinymce.");
            return;
        }

        $(".qfq-tinymce").each(
            function () {
                var config = {};
                var myEditor = {};
                var $this = $(this);
                var tinyMCEId = $this.attr('id');
                if (!tinyMCEId) {
                    QfqNS.Log.warning("TinyMCE container does not have an id attribute. Ignoring.");
                    return;
                }

                var configData = $this.data('config');
                if (configData) {
                    if (configData instanceof Object) {
                        // jQuery takes care of decoding data-config to JavaScript object.
                        config = configData;
                    } else {
                        QfqNS.Log.warning("'data-config' is invalid: " + configData);
                    }
                }

                config.selector = "#" + QfqNS.escapeJqueryIdSelector(tinyMCEId);
                config.setup = function (editor) {
                    myEditor = editor;
                    var element = document.getElementById(QfqNS.escapeJqueryIdSelector(tinyMCEId));
                    var isReadOnly = $(element).hasClass("readonly");

                    // if form is opened in readonly mode disable buttons and make element gray.
                    if (isReadOnly) {
                        editor.setMode("readonly"); // Make TinyMCE read-only

                        // Apply styles directly
                        editor.on('init', function () {
                            // Disable and style TextArea
                            var iframe = document.querySelector("iframe#" + tinyMCEId + "_ifr");
                            if (iframe) {
                                var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
                                var body = iframeDoc.body;
                                body.style.backgroundColor = "#f4f4f4";  // Light gray
                                body.style.color = "#888";              // Gray text
                                body.style.cursor = "not-allowed";      // Disable text selection
                                body.style.pointerEvents = "none";      // Disable interactions
                            }
                            // Style Toolbar
                            var toolbar = document.querySelector(".mce-toolbar-grp");
                            if (toolbar) {
                                toolbar.style.backgroundColor = "#f4f4f4";
                                toolbar.style.pointerEvents = "none";  // Prevent clicks

                            }
                            // Disable all toolbar buttons
                            var buttons = document.querySelectorAll(".mce-btn");
                            buttons.forEach(button => {
                                button.setAttribute("disabled", "true");
                                button.style.pointerEvents = "none";  // Prevents clicking
                                button.style.opacity = "0.5";  // Make them faded
                                button.style.backgroundColor = "#f4f4f4";
                            });
                            // Footer / Statusbar visual changes
                            var statusbar = document.querySelector(".mce-statusbar");
                            if (statusbar) {
                                statusbar.style.pointerEvents = "none";  // Prevent interactions
                                statusbar.style.backgroundColor = "#f4f4f4";
                            }
                        });
                    }

                    var counterFlag = false;
                    if ($this.data('character-count-id') !== undefined) {
                        counterFlag = true;
                    }
                    var maxLength = config.maxLength; // set the maximum length here
                    var maxLengthDisplay = maxLength;
                    if (maxLength === '' || maxLength === 0) {
                        maxLength = false;
                        maxLengthDisplay = "∞";
                    }

                    var pattern = /[^\r\n\t\v\f ]/g;

                    editor.on('init', function() {
                        if (maxLength) {
                            editor.dom.setAttrib(editor.getBody(), 'maxlength', maxLength);
                        }

                        // initialize counter after first load
                        if (counterFlag) {
                            var content = editor.getContent({format: 'text'});
                            var charCount = (content.match(pattern) || []).length;
                            var characterCountTarget = "#" + $this.data('character-count-id');

                            $this.data('character-count-display', $(characterCountTarget));
                            $this.data('character-count-display').text(charCount + "/" + maxLengthDisplay);
                        }
                    });

                    editor.on('Change drop', function (e) {
                        // Ensure the associated form is notified of changes in editor.
                        QfqNS.Log.debug('Editor was changed');
                        var eventTarget = e.target;
                        var $parentForm = $(eventTarget.formElement);
                        if($parentForm.length === 0) {
                            $parentForm = $(element.closest("form"));
                        }
                        $parentForm.trigger("change");
                    });

                    var inputFlag = false;
                    // Needed for accurate character count. Update counter after input or keyup.
                    editor.on('input keyup', function() {
                        if (counterFlag) {
                            var content = editor.getContent({format: 'text'});
                            var charCount = (content.match(pattern) || []).length;
                            $this.data('character-count-display').text(charCount + "/" + maxLengthDisplay);
                        }
                        inputFlag = true;
                    });

                    // Preventing input of more than maxLength
                    editor.on('keydown', function(e) {
                        if (maxLength) {
                            var content = editor.getContent({format: 'text'}); // get content without HTML tags
                            var charCount = (content.match(pattern) || []).length;
                            if (charCount >= maxLength && e.keyCode !== 8) { // prevent input when maximum length is reached
                                e.preventDefault();
                                e.stopPropagation();
                            }
                        }
                    });

                    // Trigger enabled dynamic update for tinyMce after focus out
                    // To get content via dynamic update: It needs to be written in textarea value
                    editor.on('blur', function () {
                        element.value = tinymce.get(QfqNS.escapeJqueryIdSelector(tinyMCEId)).getContent();
                        if (inputFlag) {
                            $(config.selector).trigger('change');
                        }
                    });

                    /* Remove ReadOnly Again - we have to implement tinymce differently
                       to make it easier to change such  */
                    var me = editor;
                    var $parent = $(config.selector);
                    
                    $parent.on("blur", function(e, configuration) {
                        if(configuration.disabled || configuration.readonly) {
                            me.setMode("readonly");
                            $(this).siblings(".mce-tinymce").addClass("qfq-tinymce-readonly");
                        } else {
                            me.setMode("design");
                            $(this).siblings(".mce-tinymce").removeClass("qfq-tinymce-readonly");
                        }
                    });
                };

                var defaults = {
                    relative_urls : false,
                    remove_script_host : false,
                };

                var tinyConfig = Object.assign(defaults, config);

                tinymce.init(tinyConfig);
                if($(this).is('[disabled]')) {
                    myEditor.setMode("readonly");
                }
            }
        );
    };

    /**
     * Force update of the shadowed `<textarea>`. Usually called before a form submit.
     */
    tinyMce.prepareSave = function () {
        if (typeof tinymce === 'undefined') {
            return;
        }

        tinymce.triggerSave();
    };

    n.tinyMce = tinyMce;


})(QfqNS.Helper);