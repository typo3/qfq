/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initialize a jqxComboBox.
     *
     * Only `<div>` elements having a `jqw-combobox` class are taken into account during initialization.
     *
     * Data for the jqxComboBox has to be provided in a `<script>` element. In order for the widget to find the script
     * tag, the `<div>` element requries a `data-control-name` attribute. The id of the corresponding `<script>` tag is
     * derived by appending `_source` to the value of `data-control-name`.
     *
     * The content of the data `<script>` element has to be a valid JSON array of objects, e.g.
     *
     *     [
     *       {"text": "A", "value": "1"},
     *       {"text": "B", "value": "2"},
     *       ...
     *     ]
     *
     * The `text` and `value` attributes are required. `text` will be displayed to the enduser, and `value` is
     * submitted to the backend.
     *
     * @function
     * @name QfqNS.Helper.jqxComboBox
     */
    var jqxComboBox = function () {
        var index;
        var $containers = $("div.jqw-combobox");

        $containers.each(function (index, object) {
            (function ($container) {
                var controlName = $container.data('control-name');
                if (!controlName) {
                    QfqNS.Log.error("jqwComboBox container does not have a 'data-control-name' attribute.");
                    return;
                }

                var sourceId = controlName + "_source";
                var $sourceScript = $('#' + sourceId);
                if ($sourceScript.length !== 1) {
                    QfqNS.Log.error("Unable to find data for jqwComboBox using id '" + sourceId + "'");
                    return;
                }

                var source = JSON.parse($sourceScript.text());


                $container.jqxComboBox({
                    source: source,
                    displayMember: "text",
                    valueMember: "value"
                });

                // Our code creates a hidden input element for each jqxwidget as sibling of the widget. We do this,
                // because jqxwidget don't create named input elements, and thus the value would not be sent to the
                // server using a Plain Old Form submission (even if performed by an ajax request).
                var $hiddenInput = $("<input>")
                    .attr('type', 'hidden')
                    .attr('name', controlName);

                $container.after($hiddenInput);

                $hiddenInput.val($container.jqxComboBox('val'));

                $container.on('change', function (event) {
                    $hiddenInput.val(event.args.item.value);
                });
            })($(object));
        });
    };

    n.jqxComboBox = jqxComboBox;

})(QfqNS.Helper);