/**
 * @author Enis Nuredini <enis.nuredini@math.uzh.ch>
 */

/* global console */
/* global qfqChat */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    let qfqMerge = function (mergeWindowElement) {
        //let chatWindowsElements = document.getElementsByClassName("qfq-chat-window");
        let mergeWindow = null;
        if (mergeWindowElement !== undefined) {
            mergeWindow = new MergeWindow(mergeWindowElement);
        }
        return mergeWindow;
    }

    class MergeWindow {
        constructor(mergeWindowElement) {
            let that = this;
            this.mergeWindow = mergeWindowElement;
            this.baseUrl = mergeWindowElement.getAttribute("data-base-url");
            this.restCallUrl = this.baseUrl + 'typo3conf/ext/qfq/Classes/Api/dataReport.php?s=';
            this.dataSipRules = mergeWindowElement.getAttribute("data-sip-rules");
            this.merged = false;
            this.primaryElements = {};

            let primaryDeleteBtn = mergeWindowElement.querySelectorAll("button[disabled='disabled'][data-primary]");
            if (primaryDeleteBtn !== null) {
                Array.from(primaryDeleteBtn).forEach(btn => {
                    const primaryId = btn.getAttribute('data-primary');
                    const sip = btn.getAttribute('data-sip');
                    const sipUpdate = btn.getAttribute('data-sip-update');
                    const sipDelete = btn.getAttribute('data-sip-delete');
                    this.primaryElements[primaryId] = {};
                    this.primaryElements[primaryId].deleteBtn = btn;
                    this.primaryElements[primaryId].id = primaryId;
                    this.primaryElements[primaryId].sipUrl = this.restCallUrl + sip;
                    this.primaryElements[primaryId].sipUpdate = sipUpdate;
                    this.primaryElements[primaryId].sipDelete = sipDelete;
                    this.primaryElements[primaryId].relatedCheckboxes = mergeWindowElement.querySelectorAll(`input[type='checkbox'][data-id='${primaryId}']`);
                });
            }

            this.allCheckboxes = mergeWindowElement.querySelectorAll(".merge-window input[type='checkbox']");
            this.mergeBtn = mergeWindowElement.querySelector('#merge-window-btn');
            this.primaryCheckboxes = Array.from(mergeWindowElement.querySelectorAll('[name="checkAll"]'));
            this.checkboxes = mergeWindowElement.querySelectorAll("input[type='checkbox'][data-id]");
            this.ruleBtn = mergeWindowElement.querySelector('#merge-window-update-rule-btn');

            this.init();
        }

        init() {
            let that = this;

            // Iterate over primary elements and initialize delete btn
            Object.values(this.primaryElements).forEach(element => {
                // Check for existing merge data and enable delete buttons
                if (that.checkData(element.id)) {
                    element.deleteBtn.disabled = false;
                }

                element.deleteBtn.addEventListener('click', async function () {
                    if (that.checkData(element.id) && !that.merged) {
                        if (confirm("Are you sure you want to delete this data?")) {
                            console.log('Delete data click');
                            try {
                                const SQL_DELETE_PRIMARY = await that.makeApiCall(element.sipUrl, this, 'delete');
                            } catch (error) {
                            }
                        }
                    }
                });
            });

            // Initialize rule btn
            if (this.ruleBtn !== null) {
                this.ruleBtn.addEventListener('click', async function () {
                    try {
                        const sip_rules_url = that.restCallUrl + that.dataSipRules;
                        const SQL_INSERT = await that.makeApiCall(sip_rules_url, this, 'updateRules');
                    } catch (error) {
                    }
                });
            }

            // Initialize rest
            this.initTableContent();
            this.initCheckboxes(this);
            if (this.mergeBtn !== null) {
                this.initMergeBtn(this);
            }
        }

        checkData(dataId) {
            let foundCheckboxes = this.primaryElements[dataId].relatedCheckboxes;
            return foundCheckboxes.length === 0;
        }

        initTableContent() {
            this.mergeWindow.querySelectorAll('.merge-table-content').forEach(content => {
                let wrapper = content.parentElement;
                let button = wrapper.nextElementSibling;
                let icon = button.querySelector('i');
                if (content.scrollHeight > 150) {
                    button.classList.remove('hidden');
                } else {
                    button.classList.add('hidden');
                }
                button.addEventListener('click', function () {
                    if (!content.style.maxHeight || content.style.maxHeight === '150px') {
                        content.style.maxHeight = content.scrollHeight + 'px';
                        icon.classList.remove('fa-angle-double-down');
                        icon.classList.add('fa-angle-double-up');
                    } else {
                        content.style.maxHeight = '150px';
                        icon.classList.add('fa-angle-double-down');
                        icon.classList.remove('fa-angle-double-up');
                    }
                });
            });
        }

        initCheckboxes(that) {
            // Function to set initial state of a checkbox
            const setInitialState = (el) => {
                if (el.hasAttribute('checked')) {
                    el.dataset.state = "checked";
                    el.checked = true;
                } else if (el.dataset.state === "indeterminate") {
                    el.indeterminate = true;
                    // Note: indeterminate checkboxes might also be checked or unchecked in terms of their value submission.
                    // Adjust the following line if you have a convention for this.
                } else {
                    el.dataset.state = "unchecked";
                    el.checked = false;
                    el.unchecked = true;
                }
            };

            this.allCheckboxes.forEach(el => {
                // Set initial state for checkboxes
                setInitialState(el);
            });

            this.primaryCheckboxes.forEach(el => el.addEventListener('click', (e) => {
                let currentState = el.dataset.state || "unchecked";
                let nextState = currentState === "checked" ? "indeterminate" : (currentState === "indeterminate" ? "unchecked" : "checked");
                el.dataset.state = nextState;
                el.indeterminate = nextState === "indeterminate";
                el.checked = nextState === "checked";

                that.primaryElements[el.id].relatedCheckboxes.forEach(cb => {
                    cb.dataset.state = nextState;
                    cb.indeterminate = nextState === "indeterminate";
                    cb.checked = nextState === "checked";
                    cb.unchecked = nextState === "unchecked";

                    let parentNode = cb.parentNode.parentNode;
                    parentNode.classList.remove('bg-success', 'bg-danger', 'bg-warning');
                    if (nextState === "checked") {
                        parentNode.classList.add('bg-success');
                    } else if (nextState === "indeterminate") {
                        parentNode.classList.add('bg-warning');
                    } else {
                        parentNode.classList.add('bg-danger');
                    }
                });
            }));

            Object.values(that.primaryElements).forEach(element => {
                Array.from(element.relatedCheckboxes).forEach(cb => cb.addEventListener('click', () => {
                    let currentState = cb.dataset.state || "unchecked";
                    let nextState = currentState === "checked" ? "indeterminate" : (currentState === "indeterminate" ? "unchecked" : "checked");
                    cb.dataset.state = nextState;
                    cb.indeterminate = nextState === "indeterminate";
                    cb.checked = nextState === "checked";
                    cb.unchecked = nextState === "unchecked";

                    let parentNode = cb.parentNode.parentNode;
                    parentNode.classList.remove('bg-success', 'bg-danger', 'bg-warning');
                    if (nextState === "checked") {
                        parentNode.classList.add('bg-success');
                    } else if (nextState === "indeterminate") {
                        parentNode.classList.add('bg-warning');
                    } else {
                        parentNode.classList.add('bg-danger');
                    }
                }));
            });
        }

        initMergeBtn(that) {
            this.mergeBtn.addEventListener('click', async function () {
                if (that.merged) {
                    return;
                }

                if (confirm("Are you sure you want to process the selected checkboxes?")) {
                    let primaryId = that.mergeWindow.querySelector('#primaryId').value;
                    let checkedIds = {};
                    let uncheckedIds = {};
                    let columnSearch = {};

                   that.checkboxes.forEach(cb => {
                        let dataTable = cb.getAttribute('data-table');
                        let dataId = cb.getAttribute('id');
                        let columns = cb.getAttribute('data-column-found');

                        if (columnSearch[dataTable] === undefined) {
                            columnSearch[dataTable] = columns;
                        }

                        if (cb.checked) {
                            checkedIds[dataTable] = checkedIds[dataTable] ? checkedIds[dataTable] + ',' + dataId : dataId;
                        } else if (cb.unchecked) {
                            uncheckedIds[dataTable] = uncheckedIds[dataTable] ? uncheckedIds[dataTable] + ',' + dataId : dataId;
                        }
                    });

                    console.log("Checked IDs: ", checkedIds);
                    console.log("Unchecked IDs: ", uncheckedIds);

                    await that.processMerge(that, primaryId, checkedIds, columnSearch, 'update');
                    await that.processMerge(that, primaryId, uncheckedIds, columnSearch, 'delete');
                }
            });
        }

        async processMerge(that, primaryId, ids, columnSearch, type) {
            for (let dataTable in ids) {
                if (ids.hasOwnProperty(dataTable)) {
                    const dateValues = '&tableName=' + dataTable + '&columnIdList=' + ids[dataTable] + '&columnSearch=' + columnSearch[dataTable];
                    let sip = null;
                    if (type === 'update') {
                        console.log(`Processing UPDATE ${dataTable} with IDs: ${ids[dataTable]}`);
                        sip = that.primaryElements[primaryId].sipUpdate;
                    } else {
                        console.log(`Processing DELETE FROM ${dataTable} with IDs: ${ids[dataTable]}`);
                        sip = that.primaryElements[primaryId].sipDelete;
                    }
                    const url_sip = that.restCallUrl + sip + dateValues;
                    try {
                        const SQL_EXECUTE = await that.makeApiCall(url_sip, that.mergeBtn, 'update');
                    } catch (error) {
                    }
                }
            }
        }

        async makeApiCall(url, btn, type = '', body = '') {
            btn.classList.remove('btn-default');
            btn.classList.add('btn-warning');
            const response = await fetch(url, {method: 'POST', body: body});

            if (!response.ok) {
                btn.classList.remove('btn-default');
                btn.classList.remove('btn-warning');
                btn.classList.add('btn-danger');
                throw new Error();
            } else {
                btn.classList.remove('btn-default');
                btn.classList.remove('btn-warning');
                btn.classList.add('btn-success');

                // Case of update delete records
                if (type === 'update') {
                    this.merged = true;
                    this.allCheckboxes.forEach(checkbox => {
                        checkbox.disabled = true;
                    });
                }

                // Case of delete primary record
                if (type === 'delete') {
                    let idP = btn.getAttribute('data-id');
                    let optionElement = this.mergeWindow.querySelector('select[name="primaryId"] option[value="' + idP + '"]');
                    optionElement.remove();
                }
            }

            const data = await response.text();
            console.log(data);
            return data;
        }
    }

    n.qfqMerge = qfqMerge;

})(QfqNS.Helper);