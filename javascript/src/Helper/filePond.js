/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */

var QfqNS = QfqNS || {};

/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    n.filePond = function createFileUpload(inputElement) {
        // Retrieve all needed data and configurations
        this.inputElement = inputElement;
        this.pond = null;

        const configData = inputElement.getAttribute('data-config');
        this.configuration = configData ? JSON.parse(configData) : [];
        this.normalizeConfiguration();

        this.saveButton = false;
        if (this.configuration.form) {
            const formId = this.inputElement.closest('form.qfq-form').id;
            const saveButtonId = 'save-button-' + formId;
            this.saveButton = document.querySelector('#' + saveButtonId);

            // No save button for formModeGlobal=readonly
            if(this.saveButton) {
                this.saveButtonIcon = this.saveButton.querySelector('span');
                this.saveButtonLabel = this.saveButton.innerText;
            }
        }

        const apiUrls = inputElement.getAttribute('data-api-urls');
        this.apiUrls = apiUrls ? JSON.parse(apiUrls) : [];

        const sipValues = inputElement.getAttribute('data-sips');
        this.sipValues = sipValues ? JSON.parse(sipValues) : [];

        //Initialize existing preloaded files
        this.filePondFiles = this.getPreloadedFiles(this.inputElement);

        // Initialize flags
        this.lastUploadId = null;
        this.currentFieldId = false;
        this.deletedFileId = true;
        this.lastSipTmp = false;
    };

    n.filePond.prototype.createFilePondObject = function() {
        // Create the FilePond instance
        const pond = FilePond.create(this.inputElement, {
            allowMultiple: this.configuration.multiUpload,
            allowRemove: this.configuration.deleteOption,
            allowRevert: true,
            maxFileSize: this.configuration.maxFileSize,
            allowFileSizeValidation: this.configuration.activeSizeValidation,
            acceptedFileTypes: this.configuration.accept,
            labelFileProcessingComplete: this.configuration.completeMessage,
            fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                // Get file extension
                const extension = source.name.split('.').pop().toLowerCase();
                const mimeType = source.type;

                // Split the comma-separated string and handle it as an array
                let acceptedType = null;
                const acceptedTypes = this.configuration.accept.split(',');

                // Iterate over the accepted types and find a case-insensitive match
                acceptedTypes.forEach(accepted => {
                    if (accepted.trim().toLowerCase() === '.' + extension || accepted.trim().toLowerCase() === mimeType.toLowerCase()) {
                        acceptedType = accepted.trim();
                    }
                });

                // Check if the file extension or mime type is in the allowed extensions list
                if (this.configuration.extensions.map(ext => ext.toLowerCase()).includes(extension)) {
                    resolve(acceptedType); // If yes, use the detected type
                } else if (this.configuration.mimes.includes(mimeType)) {
                        resolve(type); // Direct match found
                    } else {
                        // Check for wildcard MIME type matches if no direct match is found
                        // example image/png split to image/*
                        const mimeTypeBase = mimeType.split('/')[0] + '/*';
                        if (this.configuration.mimes.includes(mimeTypeBase)) {
                            resolve(type); // Wildcard match found
                        } else {
                            reject(type); // No valid MIME type or wildcard match found
                        }
                    }
            }),
            allowFileTypeValidation: this.configuration.activeTypeValidation,
            allowImagePreview: this.configuration.imageEditor,
            allowImageEdit: this.configuration.imageEditor,
            allowDrop: this.configuration.allowUpload,
            allowBrowse: this.configuration.allowUpload,
            allowPaste: this.configuration.allowUpload,
            labelIdle: this.configuration.text,
            maxFiles: this.configuration.maxFiles,
            allowReorder: false,
            imagePreviewMaxHeight: 150,
            styleButtonRemoveItemPosition: 'right',
            credits: false,
            dropValidation: false,
            maxParallelUploads: 1,
            files: this.filePondFiles,
            iconRemove: '<i class="fas fa-trash" style="color: white;"></i>',
            server: {
                process: {
                    url: this.apiUrls.upload + "?s=" + this.sipValues.upload,
                    method: 'POST',
                    withCredentials: false,
                    headers: {},
                    ondata: (formData) => {
                        return this.setOnData(formData);
                    },
                    onload: (response) => {
                        // response is the JSON string returned by the server
                        const res = JSON.parse(response);
                        if (this.lastUploadId === null) {
                            this.lastUploadId = res.groupId;

                        }
                        // Here you can handle the unique file ID as needed
                        console.log('File uploaded successfully:', res.uniqueFileId);
                        console.log('Upload Id:', res.groupId);
                        console.log('sipTmp:', res.sipTmp);
                        this.lastSipTmp = res.sipTmp;

                        return res.uniqueFileId; // Must return the unique file ID to FilePond
                    },
                    onerror: (response) => {
                        // Handle error here
                        console.error('Error during upload:', response);
                    }
                },
                revert: (uniqueFileId, load, error) => {
                    this.setRevert(uniqueFileId, load, error);
                },
                remove: (uniqueFileId, load, error) => {
                    this.setRemove(uniqueFileId, load, error);
                },
                load: (source, load, error, progress, abort, headers) => {
                    console.log('loaded');
                }
            },
            onprocessfile: (error, fileItem) => {
                if (error) {
                    console.error('Error processing file:', error);
                    return;
                }

                setTimeout(() => {
                    const foundIndicators = this.findFalseIndicators();
                    // Changing style
                    foundIndicators.forEach(indicator => {
                        indicator.style.opacity = 0;
                    });
                }, 1000);
            },
            onremovefile: (error, file) => {
                if (error) {
                    console.error('Error removing file:', error);
                    return;
                }
                this.deletedFileId = true; // Reset fileId when a file is removed
            },
            onaddfile: (err, fileItem) => {
                if (err) {
                    console.error('Error adding file:', err);
                    return;
                }

                // Wait for the file item to be added to the DOM
                setTimeout(() => {
                    // Access the file item's element using FilePond's internal API
                    const item = pond.getFile(fileItem.id);
                    if (!item || !item.file || !item.id) {
                        console.error('The file item is missing information.');
                        return;
                    }

                    // If it's a form-element and downloadButton is not given then there is no download button needed.
                    if (this.configuration.allowDownload && (!this.configuration.form || this.configuration.form && this.configuration.downloadButton !== false)) {
                        this.createDownloadButton(fileItem, item);
                    }
                }, 100);
            },
            oninit: () => {
                // Change the styling for filePond uploads in form
                const rootElement = pond.element;
                const dropLabel = rootElement.querySelector('.filepond--drop-label');
                const listElement = rootElement.querySelector('.filepond--list');
                if (this.configuration.form) {
                    rootElement.id = this.configuration.formId;
                    if (listElement) {
                        listElement.classList.add('filepond--list-form');
                    }
                }

                if (dropLabel && this.configuration.dropBackground !== undefined) {
                    dropLabel.classList.add('filepond--drop-label-form');
                }

                // Check if the root element has the class 'hidden-input-style'
                if (rootElement.classList.contains('hidden-input') && dropLabel) {
                    dropLabel.style.marginTop = '10px'; // Apply the margin-top style
                }


                if (this.configuration.form && this.configuration.downloadButton === false) {
                    const sizeInfo = rootElement.querySelector('.filepond--file-info-sub');
                    if (sizeInfo !== null) {
                        sizeInfo.style.display = 'none';
                    }
                }
            }
        });

        this.pond = pond;
    };

    // This function will return an array of all processing complete indicators
    // that have a sibling with the revert button processing class.
    n.filePond.prototype.getPreloadedFiles = function () {
        const preloadedData = this.inputElement.getAttribute('data-preloadedFiles');
        const preloadedFiles = preloadedData ? JSON.parse(preloadedData) : [];

        return preloadedFiles.length > 0 ? preloadedFiles.map(file => ({
            source: file.id,
            options: {
                type: 'local',
                file: {
                    name: file.pathFileName.split('/').pop(),
                    size: file.fileSize,
                    type: file.mimeType
                },
                metadata: {
                    poster: file.pathFileName
                }
            }
        })) : [];
    };

    // This function will return an array of all processing complete indicators
    // that have a sibling with the revert button processing class.
    n.filePond.prototype.findFalseIndicators = function () {
        const indicatorsWithRevertSibling = [];

        // Select all processing complete indicators
        const indicators = document.querySelectorAll('.filepond--processing-complete-indicator');

        indicators.forEach(indicator => {
            // Check if the revert button processing class exists as a sibling
            const revertButton = indicator.closest('.filepond--item').querySelector('.filepond--file-action-button.filepond--action-revert-item-processing');
            if (revertButton) {
                indicatorsWithRevertSibling.push(indicator);
            }
        });

        return indicatorsWithRevertSibling;
    };

    n.filePond.prototype.setOnData = function (formData) {
        if (this.lastUploadId) {
            formData.append('groupId', this.lastUploadId);
        }
        // Add your own variables here
        formData.append('pathFileName', this.configuration.pathFileName);
        formData.append('pathDefault', this.configuration.pathDefault);
        formData.append('recordData', this.configuration.recordData);
        if (this.lastUploadId == null) {
            formData.append('groupId', this.configuration.groupId);
        }
        if (this.deletedFileId) {
            formData.append('uploadId', 0);
        } else {
            formData.append('uploadId', this.configuration.uploadId);
        }
        formData.append('table', this.configuration.table);

        // Return the modified FormData object
        return formData;
    };

    n.filePond.prototype.setRevert = function (uniqueFileId, load, error) {
        const formData = new FormData();
        formData.append('uploadId', uniqueFileId);
        formData.append('table', this.configuration.table);

        // The uniqueFileId parameter is the ID returned by the server during the 'process' call
        // This ID can be used to identify and delete the file on the server
        const xhr = new XMLHttpRequest();
        xhr.open('POST', this.apiUrls.upload + `?s=${this.sipValues.delete}`);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.onload = () => {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                // Update the new fileId from the response, if necessary
                //this.currentFieldId = response.uniqueFileId;
                load();
            } else {
                error('oh no');
            }
        };
        xhr.send(formData);
    };

    n.filePond.prototype.setRemove = function (uniqueFileId, load, error) {
        const formData = new FormData();
        formData.append('uploadId', uniqueFileId);
        formData.append('table', this.configuration.table);

        // The uniqueFileId parameter is the ID returned by the server during the 'process' call
        // This ID can be used to identify and delete the file on the server
        const xhr = new XMLHttpRequest();
        xhr.open('POST', this.apiUrls.upload + `?s=${this.sipValues.delete}`);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.onload = () => {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                // Update the new fileId from the response, if necessary
                //this.currentFieldId = response.uniqueFileId;
                load();
            } else {
                error('oh no');
            }
        };
        xhr.send(formData);
    };

    // Trying to load the pictures for preview for preloaded files. Currently not working. Maybe not needed if ImageEditor is implemented.
    n.filePond.prototype.setServerLoad = function (source, load, error, progress, abort, headers) {
        if (this.allowImagePreview) {
            const fetchRequest = new Request(this.apiUrls.download + `?type=preview&fileId=` + source);
            fetch(fetchRequest).then(response => {
                if (response.ok && response.headers.get('Content-Length') > 0) {
                    response.blob().then(blob => {
                        if (blob.size > 0) {
                            load(blob);
                        } else {
                            load('');
                        }
                    });
                } else {
                    load('');
                }
            }).catch(err => {
                // Log the error but don't trigger FilePond's error state
                console.error(err.message);
                load('');
            });
            return {
                abort: () => {
                }
            };
        }

    };

    // Create new downloadButton and append it to existing upload element.
    //
    n.filePond.prototype.createDownloadButton = function (fileItem, item) {
        // Create a download button
        const downloadButton = document.createElement('button');

        // Find the filepond--file-wrapper element for this file item
        const fileElementPanel = this.getFilePondElementPanel(item);

        if (fileElementPanel) {
            let removeButton = fileElementPanel.querySelector('.filepond--file-action-button.filepond--action-remove-item');
            const fileInfo = fileElementPanel.querySelector('.filepond--file-info');

            downloadButton.classList.add('filepond--file-action-button');
            downloadButton.classList.add('filepond--action-download-item');
            downloadButton.type = 'button';
            downloadButton.setAttribute('data-align', 'right');

            if (removeButton && window.getComputedStyle(removeButton).visibility === 'hidden') {
                downloadButton.style.marginRight = '110px';
            } else {
                downloadButton.style.marginRight = '30px'; // or set to some default value
            }

            // Create an icon element for the downward arrow
            const icon = document.createElement('i');

            if(this.configuration.glyphicon === ''){
                icon.classList.add('fas', 'fa-arrow-down');
                icon.style.color = 'white';
            } else {
                icon.classList.add('glyphicon', this.configuration.glyphicon);
            }


            // Append the icon to the button
            downloadButton.appendChild(icon);

            // Append download button text if downloadButton config given. Only in case of form-element possible.
            if (this.configuration.downloadButton !== false && this.configuration.downloadButton !== undefined) {
                const downloadButtonText = document.createElement('span');
                downloadButtonText.innerText = this.configuration.downloadButton;
                downloadButton.appendChild(downloadButtonText);
                downloadButton.title = this.configuration.tooltip;
                downloadButton.class = "glyphicon glyphicon-ok"

                downloadButtonText.style.width = 'fit-content';
                downloadButtonText.style.height = 'fit-content';
                downloadButtonText.style.position = 'unset';
                downloadButtonText.style.marginLeft = '5px';
                downloadButtonText.style.marginRight = '5px';
                downloadButtonText.style.clipPath = 'inset(100%)';

                icon.style.marginLeft = '4px';
                downloadButton.style.left = '5px';
                downloadButton.style.marginRight = 'unset';
                downloadButton.style.display = 'flex';
                downloadButton.style.alignItems = 'center';
                downloadButton.style.width = 'auto';
                downloadButton.style.borderRadius = '6px';

                fileInfo.style.display = 'none';
            }

            // Append the download button to the filepond--file element
            fileElementPanel.appendChild(downloadButton);
        }

        // Set up the click event listener to trigger the download
        downloadButton.addEventListener('click', () => {
            let sipParameter = this.sipValues.download;
            if (this.lastSipTmp !== false && this.lastSipTmp !== undefined) {
                sipParameter = this.lastSipTmp;
            }

            // Implement the download action here
            const downloadUrl = this.apiUrls.download + `?s=${sipParameter}&sipDownloadKey=${this.configuration.sipDownloadKey}&uploadId=${fileItem.serverId}`;
            window.open(downloadUrl, '_blank');
        });
    };

    // Get fileElement panel to customize.
    //
    n.filePond.prototype.getFilePondElementPanel = function (item) {
        const fileElement = document.querySelector(`#filepond--item-${item.id}`);
        const fileWrapper = fileElement.querySelector(`.filepond--file-wrapper`);
        if (fileWrapper) {
            const fileElementPanel = fileWrapper.querySelector('.filepond--file');
            if (fileElementPanel) {
                return fileElementPanel;
            } else {
                console.error('The filepond--file element was not found.');
            }
        } else {
            console.error('The filepond--file-wrapper element was not found.');
        }
    };

    // Normalize 'null' string values to actual nulls and string 'true' to boolean
    //
    n.filePond.prototype.normalizeConfiguration = function () {
        Object.keys(this.configuration).forEach(key => {
            if (this.configuration[key] === 'null') {
                this.configuration[key] = null;
            } else if (this.configuration[key] === 'true') {
                this.configuration[key] = true;
            } else if (this.configuration[key] === 'false') {
                this.configuration[key] = false;
            }
        });

        if (this.configuration.recordData === undefined) {
            this.configuration.recordData = '';
        }

        this.configuration.activeTypeValidation = this.configuration.accept !== null;
        this.configuration.activeSizeValidation = this.configuration.maxFileSize !== null;

        this.configuration.extensions = this.filterFileTypes(this.configuration.accept, true);
        this.configuration.mimes = this.filterFileTypes(this.configuration.accept);

    };

    n.filePond.prototype.filterFileTypes = function (acceptList, extension = false) {
        const cleanedTypes = acceptList.replace(/\s+/g, '').split(',');
        const mimeTypes = cleanedTypes.filter(type => type.includes('/'));
        const extensions = cleanedTypes.filter(type => !type.includes('/')).map(ext => ext.substring(1));

        if (extension) {
            return extensions;
        } else {
            return mimeTypes;
        }
    }

    n.initializeFilePondInContainer = function (container, form) {
        const inputElements = container.querySelectorAll('input[type="file"].fileupload');
        const visibleInputElements = Array.from(inputElements).filter(input => input.offsetParent !== null);

        // visibleInputElements.forEach(inputElement => {
        //     let fileObject = new n.filePond(inputElement);
        //     fileObject.createFilePondObject();


            // Initializes FilePond on each input element event hidden element
            inputElements.forEach(inputElement => {

                if (inputElement.offsetParent === null) {
                    // Add class or adjust style for non-visible input elements
                    inputElement.classList.add('hidden-input');
                }

                let fileObject = new n.filePond(inputElement);
                fileObject.createFilePondObject();


            // Call the form change after file remove
            if (form !== '') {
                fileObject.pond.on('addfile', function(error, file) {

                    // Disable save button from form and change label
                    if (fileObject.saveButton) {
                        if (file.file.size !== undefined && file.file.type !== undefined && file.status === 9) {
                            form.filepondUploadProcessing.push(fileObject);
                            fileObject.saveButton.disabled = true;
                            fileObject.saveButton.classList.remove('btn-info');
                            if (fileObject.saveButtonIcon !== null) {
                                fileObject.saveButtonIcon.classList.remove('glyphicon-ok');
                                fileObject.saveButtonIcon.classList.add('glyphicon-cog');
                            } else {
                                const processLabel = document.createElement('span');
                                processLabel.className = 'tmp-process-btn glyphicon glyphicon-cog';
                                fileObject.saveButton.innerText = '';
                                fileObject.saveButton.appendChild(processLabel);
                            }

                        }
                    }
                });

                fileObject.pond.on('removefile', function(file) {
                    const element = fileObject.pond.element;
                    if (form.filepondUploadProcessing.length === 0) {
                        form.inputAndPasteHandlerCalled = true;
                        form.markChanged(element);
                    }
                });

                fileObject.pond.on('processfile', function(file) {
                    const element = fileObject.pond.element;

                    let index = form.filepondUploadProcessing.indexOf(fileObject);
                    if (index !== -1) {
                        form.filepondUploadProcessing.splice(index, 1);
                    }

                    if (form.filepondUploadProcessing.length === 0) {
                        // reset save button icon after finalized upload
                        if (fileObject.saveButton) {
                            if (fileObject.saveButtonIcon !== null) {
                                fileObject.saveButtonIcon.classList.remove('glyphicon-cog');
                                fileObject.saveButtonIcon.classList.add('glyphicon-ok');
                            } else {
                                fileObject.saveButton.querySelector('.tmp-process-btn').remove();
                                fileObject.saveButton.innerText = fileObject.saveButtonLabel;
                            }

                        }

                        form.inputAndPasteHandlerCalled = true;
                        form.markChanged(element);
                    }
                });
            }
        });
    }

})(QfqNS.Helper);

