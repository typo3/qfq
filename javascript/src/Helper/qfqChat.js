/**
 * @author Enis Nuredini <enis.nuredini@math.uzh.ch>
 */

/* global console */
/* global qfqChat */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    let qfqChat = function (chatWindowsElements) {
        //let chatWindowsElements = document.getElementsByClassName("qfq-chat-window");
        let chatInstances = [];
        if (chatWindowsElements !== undefined) {
            for (let i = 0; i < chatWindowsElements.length; i++) {
                let chatInstance = new ChatWindow(chatWindowsElements[i]);
                chatInstances.push(chatInstance);

                if (!chatInstance.chatLoaded) {
                    chatInstance.getMessagesFromServer(chatInstance, 'first');
                }
            }
        }
        return chatInstances;
    }

    class TooltipManager {
        constructor(toolbarObject) {
            this.chatType = toolbarObject.chatType;
            if (this.chatType === 'thread') {
                this.chat = {};
                this.chat.chatMessages = toolbarObject.chatObject.element;
                this.chat.topBtnClicked = toolbarObject.chatObject.topBtnClicked;
                this.chat.isLoadingMessages = null;
                this.chat.openThread = false;
                this.chat.topBtnClicked = false;
                this.chat.connection = toolbarObject.chatObject.connection;
                this.chat.loadToolbarApi = toolbarObject.chatObject.loadToolbarApi;
                this.chat.typeAheadSip = toolbarObject.chatObject.typeAheadSip;
                this.chat.typeAheadUrl = toolbarObject.chatObject.typeAheadUrl;
                this.chat.messageId = toolbarObject.chatObject.messageId;
                this.chat.chatToolbar = null;
                this.chat.firstThreadContainer = null;
            } else {
                this.chat = toolbarObject.chatObject;
            }

            this.toolbarObject = toolbarObject;
            this.messageId = toolbarObject.messageId;
            this.toolbar = toolbarObject.toolbar;
            this.tagAddApiUrl = toolbarObject.config.tagAddApiUrl;
            this.tagDelApiUrl = toolbarObject.config.tagDelApiUrl;
            this.doneApiUrl = toolbarObject.config.doneApiUrl;
            this.tooltips = [];
            this.topBtn = null;
            this.initTooltips();
        }

        initTooltips() {
            const buttons = this.toolbar.querySelectorAll('.chat-toolbar-users-tag-more, .chat-toolbar-my-tag, .chat-toolbar-tag-btn, .chat-toolbar-users-done-more, .chat-toolbar-done-btn');
            buttons.forEach(button => {
                const tooltip = button.nextElementSibling;
                if (tooltip !== null && tooltip.classList.contains('chat-toolbar-popup')) {
                    const popperInstance = this.createPopperInstance(button, tooltip); // Create a Popper instance for each tooltip
                    button.dataset.isTooltipShown = 'false';
                    button.addEventListener('click', (event) => {
                        this.toggleTooltip(tooltip, popperInstance, button);
                        event.stopPropagation();
                    });
                    this.tooltips.push({ button, tooltip, popperInstance });
                }
            });

            const topBtn = this.toolbar.querySelector('.chat-toolbar-top-btn');
            if (topBtn) {
                topBtn.addEventListener('click', (event) => {
                    this.hideAllTooltipsExcept(null);

                    this.chat.topBtnClicked = true;

                    if (this.chatType !== 'thread') {
                        this.chat.loadNextMessages(this.chat);
                    }

                    this.chat.chatMessages.scrollTo({ top: 0, behavior: 'smooth' });
                    this.checkOverflow();

                });
                this.topBtn = topBtn;
            }

            const addTagBtn = this.toolbar.querySelector('.chat-toolbar-tag-btn');
            if (addTagBtn) {
                addTagBtn.addEventListener('click', (event) => {
                    let typeaheadInput = addTagBtn.nextSibling.querySelector('.tt-input');
                    typeaheadInput.focus();

                    typeaheadInput.addEventListener('keydown', function(event) {
                       if (event.key === 'Enter' || event.keyCode === 13){
                           let typeaheadInputBtn = typeaheadInput.parentElement.nextSibling;
                           typeaheadInputBtn.click();
                       }
                    });
                });
            }

            const tagDeleteBtn = this.toolbar.querySelectorAll('.chat-toolbar-tag-popup-delete');
            tagDeleteBtn.forEach(button => {
                if (button) {
                    button.addEventListener('click', (event) => {
                        this.hideAllTooltipsExcept(null);

                        let tagId = button.getAttribute('data-tag-id');
                        let deleteUrl = this.tagDelApiUrl + '&tagValue=' + encodeURIComponent(tagId) + '&cId=' + encodeURIComponent(this.messageId);
                        this.sendRequest(deleteUrl, function(error, response) {
                            if (error) {
                                console.log('Error deleting tag:', error);
                            } else {
                                let config = response['chat-update'].toolbarConfig;
                                let chatRoom = response['chat-update'].chatRoom;
                                this.toolbarObject.initToolbar(config);
                                this.initTooltips();
                                this.checkOverflow();

                                let pingToolbar = {
                                    type: 'toolbar-ping',
                                    data: this.messageId,
                                    chatRoom: chatRoom
                                }

                                this.chat.connection.send(JSON.stringify(pingToolbar));
                            }
                        }.bind(this));
                    });
                }
            });

            const typeaheadInputBtn = this.toolbar.querySelector('.chat-toolbar-typeahead-btn');
            if (typeaheadInputBtn) {
                typeaheadInputBtn.addEventListener('mousedown', function(event) {
                    event.preventDefault();
                });

                typeaheadInputBtn.addEventListener('click', (event) => {
                    let typeaheadInput = event.target.closest('.chat-toolbar-typeahead').querySelector('.tt-input');
                    let inputId = event.target.closest('.chat-toolbar-typeahead').querySelector('.chat-toolbar-typeahead > input');

                    let tagValue = typeaheadInput.value;

                    // Get id from typeahead if it was selected from user
                    if (inputId !== null) {
                        let typeaheadValue = inputId.value;

                        if (!isNaN(typeaheadValue) && typeaheadValue.trim() !== '') {
                            // typeaheadValue is numeric
                            tagValue = typeaheadValue;
                        }
                    }

                    if (tagValue) {
                        let addTagUrl = this.tagAddApiUrl + '&tagValue=' + encodeURIComponent(tagValue) + '&cId=' + encodeURIComponent(this.messageId);
                        this.sendRequest(addTagUrl, function(error, response) {
                            if (error) {
                                console.log('Error adding tag:', error);
                            } else {
                                if (response['chat-update'] === undefined || response['chat-update'].length === 0) {
                                    return;
                                }

                                let config = response['chat-update'].toolbarConfig;
                                let chatRoom = response['chat-update'].chatRoom;
                                this.toolbarObject.initToolbar(config);
                                this.initTooltips();
                                this.checkOverflow();
                                typeaheadInput.value = '';

                                let pingToolbar = {
                                    type: 'toolbar-ping',
                                    data: this.messageId,
                                    chatRoom: chatRoom
                                }

                                this.chat.connection.send(JSON.stringify(pingToolbar));
                            }
                        }.bind(this));
                    }
                    this.hideAllTooltipsExcept(null);
                });
            }

            const doneBtn = this.toolbar.querySelector('.chat-toolbar-done-btn');
            if (doneBtn) {
                doneBtn.addEventListener('click', (event) => {
                    this.hideAllTooltipsExcept(null);

                    let tagId = doneBtn.getAttribute('data-tag-id');
                    let doneApiUrl = this.doneApiUrl + '&tagValue=' + encodeURIComponent(tagId) + '&cId=' + encodeURIComponent(this.messageId);
                    this.sendRequest(doneApiUrl, function(error, response) {
                        if (error) {
                            console.log('Error switching done:', error);
                        } else {
                            let result = response['chat-update'].result;
                            let chatRoom = response['chat-update'].chatRoom;

                            if (result === 'noCiD or missing tagValue') {
                                console.log('cId is missing');
                                return;
                            }

                            if (result !== 'deleted') {
                                this.toggleDoneBtn(doneBtn, result);
                            } else {
                                this.toggleDoneBtn(doneBtn, null,true);
                            }

                            let pingToolbar = {
                                type: 'toolbar-ping',
                                data: this.messageId,
                                chatRoom: chatRoom
                            }

                            this.chat.connection.send(JSON.stringify(pingToolbar));
                        }
                    }.bind(this));
                });

                this.doneBtn = doneBtn;
            }

            this.chat.chatMessages.addEventListener('scroll', () => {
                if (this.chat.openThread) {
                    return;
                }
                if (this.chat.topBtnClicked) {
                    if (this.chat.chatMessages.scrollTop === 0) {
                        this.chat.topBtnClicked = false;
                    }
                    this.checkOverflow();
                    return;
                }
                this.checkScrollPosition();
                this.checkOverflow();
            });

            // Initialize typeahead for tag input
            QfqNS.TypeAhead.install(this.chat.typeAheadUrl);
        }

        toggleDoneBtn(doneBtn, result, active = false) {
            if (!active) {
                doneBtn.setAttribute('data-tag-id', result);
                doneBtn.title = 'Marked as done';
                doneBtn.firstChild.style.setProperty('color', 'black');
                doneBtn.classList.add('chat-toolbar-done-btn-success');
                doneBtn.blur();
            } else {
                doneBtn.setAttribute('data-tag-id', 'false');
                doneBtn.title = 'Undone';
                doneBtn.firstChild.style.setProperty('color', 'black');
                doneBtn.classList.remove('chat-toolbar-done-btn-success');
            }
        }

        // Load next 10 previous messages if scroll on top is reached
        checkScrollPosition() {
            // Check if the user has scrolled to the top
            if (this.chat.chatMessages.scrollTop === 0) {
                if (this.chat.isLoadingMessages) {
                    return;
                }

                if (this.chat.isLoadingMessages !== null) {
                    this.chat.isLoadingMessages = true;
                }

                let oldScrollHeight = this.chat.chatMessages.scrollHeight;

                if (this.chat.isLoadingMessages !== null) {
                    this.chat.loadNextMessages(this.chat);
                }

                setTimeout(() => {
                    let newScrollHeight = this.chat.chatMessages.scrollHeight;
                    this.chat.chatMessages.scrollTop += (newScrollHeight - oldScrollHeight);
                    if (this.chat.isLoadingMessages !== null) {
                        this.chat.isLoadingMessages = false;
                    }
                }, 100);
            }
        }

        // Hide or show the top button
        checkOverflow() {
            setTimeout(() => {
                let nextSibling = this.topBtn.nextSibling;
                if (this.chat.chatMessages.scrollTop > 5) {
                    if (nextSibling !== null) {
                        nextSibling.classList.remove('chat-toolbar-first-element');
                    }
                    this.topBtn.classList.add('chat-toolbar-first-element');
                    this.topBtn.style.display = 'block';
                } else if (this.chat.flagMoreRecords) {
                    if (nextSibling !== null) {
                        nextSibling.classList.remove('chat-toolbar-first-element');
                    }
                    this.topBtn.classList.add('chat-toolbar-first-element');
                    this.topBtn.style.display = 'block';
                } else {
                    if (nextSibling !== null) {
                        nextSibling.classList.add('chat-toolbar-first-element');
                        this.topBtn.classList.remove('chat-toolbar-first-element');
                    }
                    this.topBtn.style.display = 'none';
                }
            }, 200);
        }

        toggleToolbarDisplay() {
            if (this.toolbar.style.visibility === 'visible' || this.toolbar.style.visibility === '') {
                this.toolbar.style.visibility = 'hidden';
            } else {
                this.toolbar.style.visibility = 'visible';
            }
        }

        sendRequest(url, callback) {
            let xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    callback(null, response);
                } else {
                    console.log('Error in chat saving.');
                    callback(new Error('Error in chat saving'));
                }
            };

            xhr.onerror = function() {
                console.log('Request failed.');
                callback(new Error('Request failed'));
            };

            xhr.send();
        }

        toggleTooltip(tooltip, popperInstance, button) {
            let isShown = button.dataset.isTooltipShown === 'true';
            if (isShown) {
                this.hide(tooltip, popperInstance);
            } else {
                this.hideAllTooltipsExcept(button);
                this.show(tooltip, popperInstance);
            }
            button.dataset.isTooltipShown = !isShown;
        }

        hideAllTooltipsExcept(currentButton) {
            this.tooltips.forEach(({ button, tooltip, popperInstance }) => {
                if (button !== currentButton) {
                    this.hide(tooltip, popperInstance);
                    button.dataset.isTooltipShown = 'false';
                }
            });
        }

        /* Start popup js functions*/
        createPopperInstance(button, tooltip) {
            return Popper.createPopper(button, tooltip, {
                placement: 'bottom',
                modifiers: [
                    {
                        name: 'offset',
                        options: {
                            offset: [0, 8],
                        },
                    },
                ],
            });
        }

        show(tooltip, popperInstance) {
            tooltip.setAttribute('data-show', '');
            popperInstance.setOptions((options) =>
                Object.assign({}, options, {
                    modifiers: options.modifiers.concat([{ name: 'eventListeners', enabled: true }])
                })
            );
            popperInstance.update();
        }

        hide(tooltip, popperInstance) {
            tooltip.removeAttribute('data-show');

            // Clean all input field inside a tooltip -> tag typeahead
            const inputs = tooltip.querySelectorAll('input');
            inputs.forEach(input => {
                input.value = '';
            });

            popperInstance.setOptions((options) =>
                Object.assign({}, options, {
                    modifiers: options.modifiers.concat([{ name: 'eventListeners', enabled: false }])
                })
            );
        }
    }

    class ChatToolbar {
        constructor(chatObject, config, chatType = 'regular') {
            this.chatObject = chatObject;
            if (chatType === 'thread') {
                this.parentElement = chatObject.element;
            } else {
                this.parentElement = chatObject.chatMessages;
            }
            this.chatType = chatType;
            this.config = config;
            this.messageId = config.messageId;

            // In case of first room messsage
            if (this.messageId === 0 && chatType === 'regular') {
                this.messageId = chatObject.messageId;
            }
            this.toolbar = this.createToolbar();
            this.initToolbar(this.config);
            this.toolbarManager = new TooltipManager(this);

            // Close all open popup divs after clicking somewhere in window
            window.addEventListener('click', (event) => {
                if (!this.toolbar.contains(event.target)) {
                    this.toolbarManager.hideAllTooltipsExcept(null);
                }
            });
        }

        createToolbar() {
            let toolbar = this.parentElement.querySelector(':scope > .chat-toolbar');
            if (!toolbar) {
                toolbar = document.createElement('div');
                toolbar.className = 'chat-toolbar';
                if (this.chatType === 'thread') {
                    toolbar.classList.add('chat-toolbar-thread');
                }
                this.parentElement.insertBefore(toolbar, this.parentElement.firstChild);

                let nextSibling = toolbar.nextSibling;
                let clearFloat = document.createElement('div');
                clearFloat.className = 'clearfix';

                if (nextSibling && nextSibling.classList.contains('chat-thread-open-btn')) {
                    nextSibling.after(clearFloat);
                } else {
                    toolbar.after(clearFloat);
                }
            }
            return toolbar;
        }

        initToolbar(config) {
            this.toolbar.innerHTML = '';

            // Create all toolbar elements
            const elements = this.createToolbarElements(config);

            // Filter out popup divs.
            let btnElements = elements.filter(el => el.classList.contains('btn') || el.classList.contains('chat-toolbar-users-tag'));
            btnElements.forEach((el, index) => {
                // Add classes for first and last elements. Skip the first one > top btn. Is dynamically handled.
                if (index === 1) el.classList.add('chat-toolbar-first-element');
                if (index === btnElements.length - 1) el.classList.add('chat-toolbar-last-element');
            });

            // Append elements to the toolbar in order
            elements.forEach((el, index) => {
                this.addElement(el);
            });
        }


        createToolbarElements(config) {
            const elements = [];

            // If optionTag is set to 'my', filter out tags not created by the user
            if (config.optionTag === 'my') {
                let tagsArray = Array.isArray(config.tags) ? config.tags : [config.tags];

                config.cleanedTags = tagsArray.filter(tag =>
                    (tag.pIdCreator == config.pIdCreator || tag.username === config.username) &&
                    tag.pIdCreator !== '' && tag.username !== ''
                );
            } else {
                config.cleanedTags = config.tags;
            }

            // Add top btn if configured
            if (config.topBtn) {
                elements.push(this.createTopBtn());
            }

            // Show more tags button
            if (config.optionTag !== 'off' && config.cleanedTags.length > 3) {
                // Get all array elements from starting index 4.
                let restTags = config.cleanedTags.slice(3);

                let moreTagsObject = this.createMoreTagsButton(restTags, config.pIdCreator);
                elements.push(moreTagsObject.button);
                elements.push(moreTagsObject.popup);
            }

            // Add up to 3 tags
            if (config.optionTag !== 'off') {
                config.cleanedTags.slice(0, 3).forEach(tag => {
                    let tagObject = this.createTag(tag, config.pIdCreator, config.username);

                    elements.push(tagObject.button);
                    if (tagObject.popup !== null) {
                        elements.push(tagObject.popup);
                    }
                });
            }

            // Add tag btn typeahead
            if (config.optionTag !== 'off') {
                let tagBtnObject = this.createAddTagButton();
                elements.push(tagBtnObject.button);
                elements.push(tagBtnObject.popup);
            }

            // Show more done button
            if (config.optionTagDone === 'all' && config.usersDone.length > 3) {
                // Get all array elements from starting index 4.
                let restDones = config.usersDone.slice(3);

                let moreDonesObject = this.createMoreDonesButton(restDones);
                elements.push(moreDonesObject.button);
                elements.push(moreDonesObject.popup);
            }

            // Add up to 3 users done
            if (config.optionTagDone === 'all') {
                let usersDoneArray = Array.isArray(config.usersDone) ? config.usersDone : [config.usersDone];
                usersDoneArray.slice(0, 3).forEach(userDone => {
                    elements.push(this.createUserDoneDiv(userDone));
                });
            }

            if (config.optionTagDone !== 'off') {
                elements.push(this.createDoneButton(config.activeDone));
            }

            return elements;
        }

        addElement(element) {
            this.toolbar.appendChild(element);
        }

        removeElement(selector) {
            const element = this.toolbar.querySelector(selector);
            if (element) {
                this.toolbar.removeChild(element);
            }
        }

        createTopBtn() {
            const button = document.createElement('button');
            button.className = 'chat-toolbar-top-btn btn btn-default';
            button.style.display = 'none';

            let iconElement = document.createElement('i');
            iconElement.className = 'fas fa-arrow-up';
            iconElement.style.setProperty('color', '#000000');
            button.appendChild(iconElement);

            return button;
        }

        createMoreTagsButton(usersTag, pIdCreator) {
            let moreTagsObject = {};

            const button = document.createElement('button');
            button.className = 'chat-toolbar-users-tag-more btn btn-default';
            button.setAttribute("aria-describedby", "tooltip");

            var iconElement = document.createElement('i');
            iconElement.className = 'fas fa-ellipsis-v';
            iconElement.style.setProperty('color', '#000000');
            button.appendChild(iconElement);

            const tagsContainer = document.createElement('div');
            tagsContainer.innerHTML = 'Tags:<hr>';
            usersTag.forEach(userTag => {
                if (userTag.pIdCreator == pIdCreator) {
                    const tagDeleteBtn = this.createDeleteButton(userTag.id, userTag.username + ': ' + userTag.value);
                    tagDeleteBtn.className = tagDeleteBtn.className + ' btn-block';
                    tagDeleteBtn.setAttribute('data-tag-id', userTag.id);
                    tagsContainer.appendChild(tagDeleteBtn);
                } else {
                    const tagText = document.createElement('div');
                    tagText.innerHTML = userTag.username + ': ' + userTag.value;
                    tagsContainer.appendChild(tagText);
                }
            });

            let popupDiv = this.createPopupElement(tagsContainer.outerHTML);

            moreTagsObject.button = button;
            moreTagsObject.popup = popupDiv;

            return moreTagsObject;
        }

        createTag(tag, pIdCreator, username) {
            let tagObject = {};
            let popupDivDelete = null;

            const div = document.createElement('div');
            div.className = 'chat-toolbar-users-tag';
            div.textContent = tag.value.length > 5 ? tag.value.substring(0, 5) + '.' : tag.value;

            if (tag.pIdCreator == pIdCreator) {
                div.title = tag.value;
                div.className = div.className + ' chat-toolbar-my-tag btn alert-info';
                div.setAttribute('data-tag-id', tag.id);

                popupDivDelete = this.createPopupElement('', 'tag', tag.id);
            } else {
                div.title = tag.username + ': ' + tag.value;
            }

            tagObject.button = div;
            tagObject.popup = popupDivDelete;

            return tagObject;
        }

        createAddTagButton() {
            let tagBtnObject = {};
            let popupTypeahead = null;

            const button = document.createElement('button');
            button.className = 'chat-toolbar-tag-btn btn btn-default';
            // ... set up event listener for pop up
            var iconElement = document.createElement('i');
            iconElement.className = 'fas fa-tag';
            iconElement.style.setProperty('color', '#000000');
            button.appendChild(iconElement);

            popupTypeahead = this.createPopupElement('', 'typeahead');

            tagBtnObject.button = button;
            tagBtnObject.popup = popupTypeahead;

            return tagBtnObject;
        }

        createMoreDonesButton(usersDone) {
            let moreDonesObject = {};

            const button = document.createElement('button');
            button.className = 'chat-toolbar-users-done-more btn btn-default';
            button.setAttribute("aria-describedby", "tooltip");

            var iconElement = document.createElement('i');
            iconElement.className = 'fas fa-ellipsis-v';
            iconElement.style.setProperty('color', '#000000');
            button.appendChild(iconElement);

            let usersDoneContent = 'Done:<hr>';
            usersDone.forEach(userDone => {
                let userDoneHtml = userDone.username + '<br>';
                usersDoneContent += userDoneHtml;
            });

            let popupDiv = this.createPopupElement(usersDoneContent);

            moreDonesObject.button = button;
            moreDonesObject.popup = popupDiv;

            return moreDonesObject;
        }

        createPopupElement(content, type = 'text', userTagId = 0) {
            let popupDiv = document.createElement('div');
            popupDiv.className = "chat-toolbar-popup";
            popupDiv.setAttribute("role", "tooltip");

            switch (type) {
                case 'tag':
                    // Create delete button for single tag
                    const button = this.createDeleteButton(userTagId, content);
                    popupDiv.appendChild(button);
                    break;
                case 'typeahead':
                    const divContainer = document.createElement('div');
                    divContainer.className = 'chat-toolbar-typeahead';
                    const typeaheadInput = document.createElement('input');
                    typeaheadInput.style.color = 'black';
                    typeaheadInput.className = 'qfq-typeahead';
                    typeaheadInput.setAttribute('data-typeahead-sip', this.chatObject.typeAheadSip);
                    typeaheadInput.setAttribute('data-typeahead-limit', '10');
                    typeaheadInput.setAttribute('data-typeahead-min-length', '1');

                    const confirmBtn = document.createElement('button');
                    confirmBtn.className = 'chat-toolbar-typeahead-btn btn btn-default';

                    var iconElement = document.createElement('i');
                    iconElement.className = 'fas fa-check';
                    confirmBtn.appendChild(iconElement);

                    divContainer.appendChild(typeaheadInput);
                    divContainer.appendChild(confirmBtn);
                    popupDiv.appendChild(divContainer);
                    break;
                default:
                    popupDiv.innerHTML = content;
            }

            let arrowDiv = document.createElement("div");
            arrowDiv.className = "chat-toolbar-popup-arrow";
            arrowDiv.setAttribute("data-popper-arrow", "");
            popupDiv.appendChild(arrowDiv);

            return popupDiv;
        }

        createDeleteButton(userTagId, textContent = '') {
            const button = document.createElement('button');
            button.className = 'chat-toolbar-tag-popup-delete chat-toolbar-popup-delete-block btn btn-default';
            button.setAttribute('data-tag-id', userTagId);

            if(textContent !== '') {
                button.textContent = textContent;
                // how to add one space between the text content and following child iconElement?
                const space = document.createTextNode(' ');
                button.appendChild(space);
            }

            var iconElement = document.createElement('i');
            iconElement.className = 'far fa-trash-alt';
            button.appendChild(iconElement);

            return button;
        }

        createUserDoneDiv(userDone) {
            const div = document.createElement('div');
            div.className = 'chat-toolbar-users-done chat-toolbar-done-btn-success';
            div.title = userDone.username;
            div.textContent = userDone.username.substring(0, 3) + '.';
            return div;
        }

        createDoneButton(activeDone) {
            const button = document.createElement('button');
            button.className = 'chat-toolbar-done-btn btn btn-default';
            button.setAttribute('data-tag-id', activeDone);

            var iconElement = document.createElement('i');
            iconElement.className = 'fas fa-check';

            if (activeDone) {
                button.title = 'Marked as done';
                iconElement.style.setProperty('color', 'black');
                button.classList.add('chat-toolbar-done-btn-success');
            } else {
                button.title = 'Undone';
                iconElement.style.setProperty('color', 'black');
            }

            button.appendChild(iconElement);
            return button;
        }

        getConfig(messageId, chatRoom = false) {
            let apiRefreshUrl = this.chatObject.loadToolbarApi + '&messageId=' + encodeURIComponent(messageId) + '&chat_room=' + encodeURIComponent(chatRoom);
            this.toolbarManager.sendRequest(apiRefreshUrl, function(error, response) {
                if (error) {
                    console.log('Error fetching toolbar config:', error);
                } else {
                    let config = response['chat-update'];
                    this.initToolbar(config);
                    this.toolbarManager.initTooltips();
                }
            }.bind(this));
        }
    }

    /**
     *
     */
    class ChatWindow {
        constructor(chatWindowElement) {
            this.chatWindow = chatWindowElement;
            this.elementName = this.chatWindow.parentNode.name;
            this.chatSearch = this.chatWindow.querySelector(".chat-search");
            this.searchInput = this.chatWindow.querySelector(".chat-search-input");
            this.searchBtn = this.chatWindow.querySelector(".chat-search-btn");
            this.chatMessages = this.chatWindow.querySelector(".chat-messages");
            this.topBtn = this.chatWindow.querySelector(".chat-top-symbol");
            this.activateSearchBtn = this.chatWindow.querySelector(".chat-search-activate");
            this.chatSpinner = this.chatMessages.querySelector(".chat-loader-container");
            this.chatInputContainer = this.chatWindow.nextElementSibling;
            this.chatInput = this.chatInputContainer.querySelector(".chat-input-field")
            this.chatSendBtn = this.chatInputContainer.querySelector(".chat-submit-button")
            this.chatConfig = this.chatWindow.getAttribute("data-chat-config");

            // All relevant sip urls
            this.websocketUrl = this.chatWindow.getAttribute("data-websocket-url");
            this.loadApi = this.chatWindow.getAttribute("data-load-api");
            this.saveApi = this.chatWindow.getAttribute("data-save-api");
            this.loadToolbarApi = this.chatWindow.getAttribute("data-toolbar-load-api");
            this.typeAheadUrl = this.chatWindow.getAttribute("data-typeahead-url");
            this.typeAheadSip = this.chatWindow.getAttribute("data-typeahead-sip");

            this.chatContainer = {};
            this.threadContainer = {};
            this.markedThreads = [];
            this.currentSearchIndex = 0;
            this.searchResults = [];
            this.lastSearchTerm = '';
            this.connection = '';
            this.chatRefresh = false;
            this.chatLoaded = false;
            this.flagMoreRecords = false;
            this.topBtnClicked = false;
            this.isLoadingMessages = false;
            this.openThread = false;
            this.lastThreadBtn = null;
            this.currentThread = null;
            this.firstThreadContainer = null;
            this.messageId = null;
            this.resetDoneOnNewMessage = false;

            this.init();
        }

        init() {
            this.searchBtn.addEventListener("click", (event) => {
                event.preventDefault();
                this.handleSearch();
            });

            this.searchInput.addEventListener("keyup", (event) => {
                if (event.keyCode !== 13 && event.key !== 'ArrowLeft' && event.key !== 'ArrowRight') {
                    event.preventDefault();
                    event.stopPropagation();
                    this.handleSearch();
                }
            });

            this.activateSearchBtn.addEventListener('click', () => {

                if (this.chatSearch.style.display === 'block') {
                    // If visible, start the hiding process
                    this.searchInput.value = '';
                    this.handleSearch();
                    this.chatSearch.style.opacity = '0';
                    this.chatSearch.style.display = 'none';
                    this.handleThreadBtnMarker('remove');
                } else {
                    // If hidden, show the div
                    this.chatSearch.style.display = 'block';
                    this.chatSearch.style.opacity = '1';
                }
            });

            document.addEventListener("visibilitychange", function() {
                if (this.chatRefresh) {
                    this.scrollToBottom();
                    this.chatRefresh = false;
                }
            }.bind(this));

            window.addEventListener("beforeunload", () => {
                if (this.connection && this.connection.readyState === WebSocket.OPEN) {
                    this.connection.close();
                }
            });

            this.chatSendBtn.addEventListener('click', () => {
                let that = this;
                qfqChat.submit(that);
            });

            this.chatMessages.addEventListener('click', function(event) {
                let threadButton = event.target.closest('.chat-thread-btn');
                let plusButton = event.target.closest('.chat-thread-open-btn');
                let button = threadButton;

                if (button === null) {
                    button = plusButton;
                }

                if (button) {
                    event.stopPropagation();

                    this.lastThreadBtn = button;

                    // Get the parent chat-container of the clicked button
                    let chatContainer = button.closest('.chat-container');
                    let threadId = '';

                    if (chatContainer === null) {
                        chatContainer = button.closest('.chat-thread-container');
                        threadId = chatContainer.getAttribute('data-thread-id');
                    } else {
                        threadId = chatContainer.getAttribute('data-message-id');
                    }

                    if (plusButton === null) {
                        button.classList.toggle('chat-thread-btn-clicked');
                    }

                    if (!this.openThread) {
                        this.openThread = true;
                        this.chatInput.setAttribute('data-thread-id', threadId);
                        this.scrollPosition = this.chatMessages.scrollTop;
                        this.currentThread = chatContainer;
                        if (plusButton !== null) {
                            this.chatMessages.classList.toggle('chat-messages-no-overflow');
                            chatContainer.classList.toggle('chat-thread-open');
                            plusButton.firstElementChild.className = 'fas fa-minus';
                        }

                        this.toolbar.toolbarManager.toggleToolbarDisplay();
                    } else {
                        this.chatInput.removeAttribute('data-thread-id');
                        this.openThread = false;
                        if (plusButton !== null) {
                            this.chatMessages.classList.toggle('chat-messages-no-overflow');
                            chatContainer.classList.toggle('chat-thread-open');
                            chatContainer.scrollTop = 0;
                            plusButton.firstElementChild.className = 'fas fa-plus';
                            plusButton.blur()
                        }
                        this.currentThread = null;
                        this.toolbar.toolbarManager.toggleToolbarDisplay();
                    }

                    // Toggle mute on all chat containers except the parent
                    this.chatMessages.querySelectorAll('.chat-container, .chat-thread-container').forEach(function(container) {
                        if (container !== chatContainer) {
                            if (container.classList.contains('chat-thread-container')) {
                                // Hide 'chat-thread-container' elements
                                container.classList.toggle('chat-container-muted');
                            }
                            // Check if the element is a 'chat-container' and its parent is NOT 'chat-thread-container'
                            else if (container.classList.contains('chat-container') && !container.parentElement.classList.contains('chat-thread-container')) {
                                // Hide 'chat-container' elements that are not inside a 'chat-thread-container'
                                container.classList.toggle('chat-container-muted');
                            }
                        }
                    });

                    if (!this.openThread) {
                        this.chatMessages.scrollTop = this.scrollPosition;
                    }
                }
            }.bind(this));

            this.scrollToBottom();

            // Build up websocket connection
            if (this.websocketUrl !== null && this.websocketUrl !== '') {
                this.connection = new WebSocket(this.websocketUrl);
                this.connection.onopen = (e) => {
                    console.log("Connection established!");
                    let chatJsonConfigString = this.chatWindow.getAttribute('data-chat-config');
                    let chatJsonConfig = JSON.parse(chatJsonConfigString);

                    let chatConfig = {
                        type: "config",
                        data: chatJsonConfig
                    };

                    this.connection.send(JSON.stringify(chatConfig));

                    let keepConnection = {type: "heartbeat"};
                    // Send heartbeat message every 30 seconds to keep connection, some server configs doesn't allow longer connections
                    setInterval(() => {
                        this.connection.send(JSON.stringify(keepConnection));
                    }, 30000);
                };

                this.connection.onmessage = (e) => {
                    e = JSON.parse(e.data);

                    try {
                        // If the parsing succeeds and it's an array with more than one element
                        if (e.type === 'ping') {
                            let that = this;
                            this.getMessagesFromServer(that, 'ping');

                            // if reset done on new message active, refresh toolbar
                            if (this.resetDoneOnNewMessage) {
                                let cId = e.data;
                                let chatRoom = e.chatRoom;
                                this.refreshToolbar(cId, chatRoom);
                            }

                            // Set flag if users tab is not active.
                            if (document.visibilityState !== 'visible') {
                                this.chatRefresh = true;
                            }
                            return;
                        }

                        if (e.type === 'toolbar-ping') {
                            let cId = e.data;
                            let chatRoom = e.chatRoom;
                            // Get toolbar configuration from messageId
                            this.refreshToolbar(cId, chatRoom);
                        }
                    } catch (error) {}
                };

                this.connection.onerror = (e) => {
                    console.error("Connection error!", e);
                };

                this.connection.onclose = (e) => {
                    console.log("Connection closed!", e);
                };
            } else {
                console.log("No websocket url found. Set in qfq config if needed.");
            }
        }

        refreshToolbar(cId, chatRoom = false) {
            if (this.toolbar.messageId == cId && chatRoom) {
                this.toolbar.getConfig(cId, chatRoom);
                this.toolbar.toolbarManager.checkOverflow();
            } else if (this.threadContainer[cId] !== undefined) {
                this.threadContainer[cId].toolbar.getConfig(cId, chatRoom);
                this.threadContainer[cId].toolbar.toolbarManager.checkOverflow();
            }
        }

        // Scroll to the bottom of chat window.
        scrollToBottom() {
            this.chatMessages.scrollTop = this.chatMessages.scrollHeight;
        }

        // Handle search with given criteria.
        handleSearch() {
            let filter = this.searchInput.value;

            if (filter !== this.lastSearchTerm) {
                this.performSearch(filter);
                this.lastSearchTerm = filter;
            }

            // Reset if empty search
            if (filter === '') {
                this.resetFilter(filter);
                this.handleThreadBtnMarker('remove');
            }

            this.scrollToCurrentResult();
        }

        // Execute the search logic.
        performSearch(filter) {
            // Update this.searchResults based on the search
            // Reset this.currentSearchIndex to 0
            var messages = this.chatMessages.querySelectorAll(".chat-message-content");

            function escapeRegExp(string) {
                return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
            }

            // Store original HTML content if not done already
            messages.forEach(content => {
                if (!content.getAttribute('data-original-html')) {
                    content.setAttribute('data-original-html', content.innerHTML);
                }
            });

            if (!filter) {
                messages.forEach(content => {
                    content.innerHTML = content.getAttribute('data-original-html');
                });
                return;
            }

            this.handleThreadBtnMarker('remove');

            var searchRegEx = new RegExp(escapeRegExp(filter), "gi");

            // Reset only if the search term has changed
            if (filter !== this.lastSearchTerm) {
                this.resetFilter(filter);
            }

            messages.forEach(function(content) {
                content.innerHTML = content.getAttribute('data-original-html');

                let messageText = content.querySelector(".chat-message-text");
                if (!messageText) {
                    // If .message-text does not exist, create it and move the content into it
                    messageText = document.createElement("div");
                    messageText.className = "chat-message-text";
                    while (content.firstChild && content.firstChild !== content.querySelector('.chat-time')) {
                        messageText.appendChild(content.firstChild);
                    }
                    content.prepend(messageText);
                }

                let originalHTML = messageText.innerHTML;
                let matchFound = false;
                messageText.innerHTML = originalHTML.replace(searchRegEx, function(match) {
                    matchFound = true;
                    this.searchResults.push({ element: content, match: match });
                    return `<span class="chat-highlight">${match}</span>`;
                }.bind(this));

                this.handleThreadBtnMarker('set', matchFound, content);
            }, this);
        }

        // Scroll to currently searched result
        scrollToCurrentResult() {
            if (this.searchResults.length > 0 && this.searchResults[this.currentSearchIndex]) {
                const selectedElement = this.searchResults[this.currentSearchIndex].element;
                const chatMessagesRect = this.chatMessages.getBoundingClientRect();
                const selectedElementRect = selectedElement.getBoundingClientRect();

                // Calculate relative position of the element inside the chatMessages container
                const relativeTop = selectedElementRect.top - chatMessagesRect.top;
                const targetScrollTop = this.chatMessages.scrollTop + relativeTop - (this.chatMessages.clientHeight / 2);

                // Use scrollTo with smooth behavior
                this.chatMessages.scrollTo({ top: targetScrollTop, behavior: 'smooth' });

                this.currentSearchIndex = (this.currentSearchIndex + 1) % this.searchResults.length;
            }

            this.updateSearchInfo();
        }

        // Load the next 10 messages if scrolling is on top and there exists more records.
        loadNextMessages(that) {
            if (this.chatMessages.scrollTop === 0 && this.flagMoreRecords) {
                this.getMessagesFromServer(that, 'refresh');
            }
        }

        updateSearchInfo() {
            const infoElement = this.chatWindow.querySelector(".chat-search-info");
            if (this.searchResults.length > 0) {
                const currentIndex = this.currentSearchIndex === 0 ? this.searchResults.length : this.currentSearchIndex;
                infoElement.textContent = ` ${currentIndex}/${this.searchResults.length}`;
            } else {
                infoElement.textContent = '';
            }
        }

        // Reset the search filter input
        resetFilter(filter) {
            this.currentSearchIndex = 0;
            this.searchResults = [];
            this.lastSearchTerm = filter;
        }

        // Set up the html for the individual message bubble.
        createNewMessagePlain(key, message, actualChatState, dbColumnNames) {
            let chatConfig = {};
            let bubbleClass = 'chat-left-bubble';
            let username = '';
            let threadBtn = null;

            if (message[dbColumnNames.pIdCreator] == actualChatState.pIdCreator) {
                bubbleClass = 'chat-right-bubble alert-info';
            } else {
                username = message[dbColumnNames.username];
                if (this.thread) {
                    threadBtn = document.createElement('button');
                    threadBtn.className = 'chat-thread-btn';
                    let iconElement = document.createElement('i');
                    iconElement.className = 'fa fa-comment';
                    threadBtn.appendChild(iconElement);
                }
            }

            let timestamp = new Date(message[dbColumnNames.created]);
            let formattedMessageDate = timestamp.toLocaleDateString('de-DE', {
                day: '2-digit', month: '2-digit', year: 'numeric', hour: '2-digit', minute: '2-digit'
            });

            let currentTimestamp = new Date();
            let currentDate = currentTimestamp.toISOString().split('T')[0];
            let messageDate = timestamp.toISOString().split('T')[0];
            let formattedDateTime = timestamp.toLocaleDateString('de-DE', {
                day: '2-digit', month: '2-digit', year: 'numeric'
            });
            if (currentDate === messageDate) {
                formattedDateTime = timestamp.toLocaleTimeString('de-DE', {
                    hour: '2-digit', minute: '2-digit'
                });
            }

            chatConfig = {
                bubbleClass: bubbleClass,
                title: formattedMessageDate,
                message: message[dbColumnNames.message],
                chatTime: formattedDateTime,
                username: username
            };

            let chatContainerElement = document.createElement('div');
            chatContainerElement.className = 'chat-container ' + chatConfig.bubbleClass;
            chatContainerElement.title = chatConfig.title;
            chatContainerElement.setAttribute('data-message-id', key);
            if (threadBtn !== null) {
                chatContainerElement.appendChild(threadBtn);
            }

            if (chatConfig.username !== ''){
                let chatMessageUsernameElement = document.createElement('div');
                chatMessageUsernameElement.className = 'chat-message-user';
                chatMessageUsernameElement.textContent = chatConfig.username;

                chatContainerElement.appendChild(chatMessageUsernameElement);

            }

            let chatPointedCornerElement = document.createElement('div');
            chatPointedCornerElement.className = 'chat-pointed-corner';
            chatContainerElement.appendChild(chatPointedCornerElement);

            let chatMessageElement = document.createElement('div');
            chatMessageElement.className = 'chat-message-content';

            let chatMessageTextElement = document.createElement('div');
            chatMessageTextElement.className = 'chat-message-text';
            chatMessageTextElement.textContent = chatConfig.message;
            chatMessageElement.appendChild(chatMessageTextElement);

            let chatMessageTimeElement = document.createElement('span');
            chatMessageTimeElement.className = 'chat-time';
            chatMessageTimeElement.textContent = chatConfig.chatTime;
            chatMessageElement.appendChild(chatMessageTimeElement);

            chatContainerElement.appendChild(chatMessageElement);

            if (!this.chatContainer[key]) {
                this.chatContainer[key] = {};
            }

            this.chatContainer[key].element = chatContainerElement;
            this.chatContainer[key].pIdCreator = message[dbColumnNames.pIdCreator];
            this.chatContainer[key].username = message[dbColumnNames.username];
            this.chatContainer[key].message = chatConfig.message;
        }

        // Create the html chat content with response data elements from server
        // The messages will be automatically ordered and set in right place over the given id as key.
        // This function is very flexible and can manage messages which aren't in right order returned from server.
        // Server always gets a list from currently showing messages which allows us to response only the none existing ones.
        refreshChat(element, chatItems, actualChatState, dbColumnNames, load_mode) {
            let messageIdKey = dbColumnNames.id;
            let threadIdKey = dbColumnNames.cIdThread;
            let lastMessageThreadContainer = null;
            let activeThread = actualChatState.activeThread;

            // Convert chatItems object to an array
            let chatItemsArray = Object.values(chatItems);

            // Sort chatItems by messageId (assuming it's numeric)
            chatItemsArray.sort((a, b) => parseInt(a[messageIdKey], 10) - parseInt(b[messageIdKey], 10));

            let messageElement = element.querySelector('.chat-messages');
            let noMessageBanner = element.querySelector('.chat-no-message');

            // Hide noMessageBanner if new messages exist and it is visible
            if (chatItemsArray.length > 0 && noMessageBanner && noMessageBanner.style.display !== 'none') {
                noMessageBanner.style.display = 'none';
            }

            // Insert each new message at the correct position
            chatItemsArray.forEach(chatItem => {
                let isThread = false;
                let isThreadContainerNew = false;
                let threadId = chatItem[threadIdKey];
                let messageId = parseInt(chatItem[messageIdKey], 10);
                let toolbarConfig = null;

                if (chatItem.toolbarConfig !== undefined && chatItem.toolbarConfig !== null) {
                    toolbarConfig = chatItem.toolbarConfig;
                    toolbarConfig = JSON.parse(chatItem.toolbarConfig);
                }

                if (!this.chatContainer[messageId]) {
                    // Create new chat message element
                    this.createNewMessagePlain(messageId, chatItem, actualChatState, dbColumnNames);
                    this.chatContainer[messageId].threadId = threadId;

                    // Create thread container if it doesn't exist and threadId exists.
                    if (threadId !== 0) {
                        isThread = true;
                        if (this.threadContainer[threadId] === undefined) {
                            let muted = false;

                            if (this.openThread && load_mode === 'ping') {
                                muted = true;
                            }
                            this.createThreadContainer(threadId, toolbarConfig, muted);
                            toolbarConfig = null;
                        } else if (toolbarConfig !== null) {
                            // In case thread was already created but not the first one delivered with toolbar configuration
                            if (load_mode !== 'ping') {
                                let currentHeight = this.threadContainer[threadId].element.offsetHeight;
                                let newHeight = currentHeight + 22;
                                this.threadContainer[threadId].element.style.height = newHeight + 'px';
                            }

                            this.initThreadContainer(threadId, toolbarConfig);
                            toolbarConfig = null;
                        }

                        lastMessageThreadContainer = this.threadContainer[threadId].element;
                    } else {
                        lastMessageThreadContainer = null;
                    }
                }

                // Now check if the first message with messageId equal current threadId was created.
                if (this.threadContainer[threadId] !== undefined && !this.threadContainer[threadId].messageIds.includes(threadId)) {
                    this.createFirstThreadMessage(threadId);
                    isThreadContainerNew = true;
                } else if (threadId !== 0) {
                    this.threadContainer[threadId].messageIds.push(messageId);
                }

                let chatContainerElement = this.chatContainer[messageId].element;

                // Only append if it's not already in the DOM
                if (!messageElement.contains(chatContainerElement)) {

                    // if a new thread container is needed then add thread container to the chat room.
                    if (isThreadContainerNew) {
                        this.addThreadToChat(messageElement, threadId, activeThread);
                    }

                    this.addNewMessage(messageElement, chatContainerElement, isThread, threadId, messageId);

                    if (toolbarConfig !== null) {
                        this.toolbar = new ChatToolbar(this, toolbarConfig);
                        this.toolbar.toolbarManager.checkOverflow();
                    }
                }
            });

            if (load_mode !== 'refresh' && load_mode !== 'ping') {
                if (this.openThread) {
                    // Fresh started thread isn't set as currentThread.
                    if (this.currentThread.classList.contains('chat-container')) {
                        this.currentThread = this.currentThread.parentElement;
                    }
                    messageElement = this.currentThread;
                }
                if (messageElement !== null) {
                    messageElement.scrollTo({ top: messageElement.scrollHeight, behavior: 'smooth' });
                }
            }
        }

        // Load message data from server with given chat configuration.
        // Result is returned as json. - ping
        getMessagesFromServer(that, load_mode) {
            // Server request over load.php only if spinner exists // place holder !== null
            if ((that.chatSpinner === undefined || that.chatSpinner === null) && load_mode === 'first') {
                return;
            }

            // Get the set of existing message ids to prevent them loading on serverside - performance increase
            let messageIds = Array.from(that.chatWindow.querySelectorAll('.chat-messages div[data-message-id]')).map(el => parseInt(el.getAttribute('data-message-id'), 10));
            let messageIdList = messageIds.join(',');

            messageIdList = qfqChat.htmlEncode(messageIdList);
            let serializedForm = `load_mode=${load_mode}&messageIdList=${encodeURIComponent(messageIdList)}`;

            // In case user started with empty chat and another client wrote a new message
            if (load_mode === 'ping' && that.messageId === null) {
                serializedForm = serializedForm + `&flagFirstMsg=true`;
            }

            // Loading spinner only if page loads the first time
            if (load_mode === 'first') {
                that.chatSpinner.style.display = 'block';
            }

            // Create an XMLHttpRequest to send the data
            let xhr = new XMLHttpRequest();
            xhr.open("POST", that.loadApi, true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    let chatItems =  response['chat-update'].chat;
                    let dbColumnNames = response['chat-update'].dbColumnNames;
                    let toolbarConfig = JSON.parse(response['chat-update'].toolbarConfig);

                    // Save chat room messageId (head) and active thread flag
                    if (load_mode === 'first' || load_mode === 'ping' && that.messageId === null) {
                        that.messageId = response['chat-update'].firstMsgId;
                        that.thread = response['chat-update'].thread;
                        that.resetDoneOnNewMessage = response['chat-update'].optionTagDoneResetOnNewMessage;
                    }

                    // Latest message data from server
                    let actualState = that.getActualStateInfo(response);

                    that.refreshChat(that.chatWindow, chatItems, actualState, dbColumnNames, load_mode);

                    // Initialize Toolbar config for chat room
                    if ((load_mode === 'first' || that.chatSpinner === null) && toolbarConfig !== null) {
                        that.toolbar = new ChatToolbar(that, toolbarConfig);
                        that.toolbar.toolbarManager.checkOverflow();

                        if (that.threadContainer[that.messageId] !== undefined) {
                            that.firstThreadContainer = that.threadContainer[that.messageId];
                            that.threadContainer[that.messageId].toolbar.toolbarManager.chat.chatToolbar = that.toolbar;
                        }
                    }

                    if (that.chatSpinner !== null) {
                        that.chatSpinner.style.display = 'none';
                    }
                    console.log('Chat successfully load.');
                    that.chatLoaded = true;
                } else {
                    that.chatSpinner.style.display = 'none';
                    console.log('Error in chat loading.');
                }
            };

            xhr.onerror = function() {
                console.log('Request failed.');
            };

            xhr.send(serializedForm);
        }

        // Server information data from latest message.
        getActualStateInfo(response) {
            let actualState = [];
            actualState.xId = response['chat-update'].xId;
            actualState.pIdCreator = response['chat-update'].pIdCreator;
            if (response['chat-update'].flagMoreRecords !== null) {
                this.flagMoreRecords = response['chat-update'].flagMoreRecords;
            }
            actualState.activeThread = response['chat-update'].activeThread;

            return actualState;
        }

        createThreadContainer(threadId, toolbarConfig, muted = false) {
            this.threadContainer[threadId] = {};
            this.threadContainer[threadId].messageIds = [];
            let threadDiv = document.createElement('div');
            threadDiv.className = 'chat-thread-container';
            threadDiv.setAttribute('data-thread-id', threadId);

            if (muted) {
                threadDiv.classList.add('chat-container-muted');
            }

            this.threadContainer[threadId].element = threadDiv;
            this.threadContainer[threadId].topBtnClicked = false;
            this.threadContainer[threadId].toolbar = null;

            if (toolbarConfig !== null) {
                this.initThreadContainer(threadId, toolbarConfig);
            }
        }

        initThreadContainer(threadId, toolbarConfig) {
            this.threadContainer[threadId].connection = this.connection;
            this.threadContainer[threadId].loadToolbarApi = this.loadToolbarApi;
            this.threadContainer[threadId].typeAheadSip = this.typeAheadSip;
            this.threadContainer[threadId].typeAheadUrl = this.typeAheadUrl;
            this.threadContainer[threadId].messageId = this.messageId;
            this.threadContainer[threadId].toolbar = new ChatToolbar(this.threadContainer[threadId], toolbarConfig, 'thread');
            this.threadContainer[threadId].toolbar.toolbarManager.checkOverflow();
        }

        createFirstThreadMessage(threadId) {
            this.threadContainer[threadId].element.appendChild(this.chatContainer[threadId].element);
            this.threadContainer[threadId].firstElementHeight = this.chatContainer[threadId].element.offsetHeight + 10;
            this.threadContainer[threadId].messageIds.push(threadId);
        }

        addNewMessage(messageElement, chatContainerElement, isThread, threadId, messageId) {
            // Add new message to thread container or to chat room itself
            if (isThread) {
                let threadContainer = this.threadContainer[threadId].element;
                let insertAtIndex = Array.from(threadContainer.children).findIndex(el => parseInt(el.getAttribute('data-message-id'), 10) > messageId);

                if (insertAtIndex !== -1) {
                    threadContainer.children[insertAtIndex].before(chatContainerElement);
                } else {
                    threadContainer.appendChild(chatContainerElement);
                }

            } else {
                let insertAtIndex = Array.from(messageElement.children).findIndex(el => {
                    const elId = parseInt(el.getAttribute('data-message-id') || el.getAttribute('data-thread-id'), 10);
                    return elId > messageId;
                });

                if (this.openThread) {
                    chatContainerElement.classList.add('chat-container-muted');
                }

                if (insertAtIndex !== -1) {
                    messageElement.children[insertAtIndex].before(chatContainerElement);
                } else {
                    messageElement.appendChild(chatContainerElement);
                }
            }
        }

        addThreadToChat(messageElement, threadId, activeThread) {
            let firstMessageIndex = Array.from(messageElement.children).findIndex(el => {
                const elId = parseInt(el.getAttribute('data-message-id') || el.getAttribute('data-thread-id'), 10);
                return elId > threadId;
            });

            // Remove first message if it already exists and thread container is missing.
            let firstMessageElement = this.chatContainer[threadId].element;
            if (messageElement.contains(firstMessageElement)) {
                messageElement.removeChild(firstMessageElement);
            }

            // Create open button for thread
            let button = document.createElement('button');
            let iconElement = document.createElement('i');
            button.className = 'chat-thread-open-btn btn btn-default';
            this.threadContainer[threadId].element.insertBefore(button, this.threadContainer[threadId].element.firstChild);

            // Insert the new thread container at the place where the first message was deleted.
            if (firstMessageIndex === -1) {
                messageElement.appendChild(this.threadContainer[threadId].element);
            } else {
                messageElement.children[firstMessageIndex].before(this.threadContainer[threadId].element);
            }

            let offsetHeight = this.chatContainer[threadId].element.offsetHeight + 30;

            if (this.threadContainer[threadId].toolbar !== null) {
                offsetHeight = offsetHeight + 24;
            }

            if (!activeThread) {
                this.threadContainer[threadId].element.style.height = offsetHeight + 'px';
                iconElement.className = 'fas fa-plus';
            } else {
                iconElement.className = 'fas fa-minus';
                if (!this.threadContainer[threadId].element.style.height) {
                    this.threadContainer[threadId].element.style.height = offsetHeight + 'px';
                }
                this.threadContainer[threadId].element.classList.add('chat-thread-open');
                this.chatMessages.classList.toggle('chat-messages-no-overflow');
            }
            button.appendChild(iconElement);

            // Check if typeahead was initialized. In case that typeahead element is created over another user it will not be initialized if chat was already open.
            QfqNS.TypeAhead.install(this.typeAheadUrl);
            this.threadContainer[threadId].created = true;
        }

        handleThreadBtnMarker(mode, matchFound = false, content = null) {
            if (mode === 'set' && matchFound) {
                // Check if the parent's parent has the class 'chat-thread-container'
                let grandparent = content.parentElement ? content.parentElement.parentElement : null;
                if (grandparent && grandparent.classList.contains('chat-thread-container')) {
                    // Find the button with class 'chat-thread-open-btn' and add a new class
                    let openThreadButton = grandparent.querySelector('.chat-thread-open-btn');
                    if (openThreadButton) {
                        openThreadButton.classList.add('chat-thread-btn-marker');
                        this.markedThreads.push(openThreadButton);
                    }
                }
            }

            if (mode === 'remove') {
                if (this.markedThreads.length > 0) {
                    this.markedThreads.forEach(threadBtn => {
                        threadBtn.classList.remove('chat-thread-btn-marker');

                    });

                    this.markedThreads = this.markedThreads.filter(threadBtn => {
                        return threadBtn && threadBtn.classList.contains('chat-thread-btn-marker');
                    });
                }
            }
        }
    }

    // Set the new input state which comes over dynamic update. The only feature that is dependent on the form functionality.
    qfqChat.setInputState = function (element, configItem) {
        let inputContainer = element.nextElementSibling;
        let inputElement = inputContainer.querySelector(".chat-input-field");
        inputElement.disabled = configItem.disabled;
        inputElement.required = configItem.required;
    }

    // Get the disabled required properties from input element. Can be useful on server side.
    qfqChat.getInputState = function (inputElement) {
        let elementMode = {};
        elementMode.required = inputElement.required;
        elementMode.disabled = inputElement.disabled;

        return JSON.stringify(elementMode);
    }

    // Handle chat message submit.
    qfqChat.submit = function(that) {
        let chatInput = that.chatInput.value;
        chatInput = chatInput.trim();
        chatInput = this.htmlEncode(chatInput);
        let inputState = this.getInputState(that.chatInput);
        let cId = that.messageId;
        let chatRoom = true;

        // Get thread id from input field if exists. Is append if user uses the thread btn to answer.
        let threadId = that.chatInput.getAttribute('data-thread-id');

        // Check if the current submitted message is the first one
        let flagFirstMsg = false;

        if (threadId !== null) {
            chatRoom = false;
            if (that.threadContainer[threadId] === undefined) {
                flagFirstMsg = true;
            }
        } else if (Object.keys(that.chatContainer).length === 0) {
            flagFirstMsg = true;
        }

        // Build client information for server actions.
        let serializedForm = `message=${encodeURIComponent(chatInput)}&chatRoomMsgId=${that.messageId}&element_mode=${inputState}&flagFirstMsg=${flagFirstMsg}`;
        if (threadId !== null) {
            serializedForm = `threadId=${threadId}&` + serializedForm;
            cId = threadId;
        }

        // saveApi has already a sip delivered inside.
        let submitUrl = that.saveApi;

        if (chatInput === ''){
            return;
        }

        // Create an XMLHttpRequest to send the data
        let xhr = new XMLHttpRequest();
        xhr.open("POST", submitUrl, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        xhr.onload = function() {
            if (xhr.status === 200) {
                if (that.lastThreadBtn !== null) {
                    // Remove the visible comment button after first thread answer was sent. Instead of this the plus button is showed.
                    that.lastThreadBtn.classList.remove('chat-thread-btn-clicked');
                }

                let response = JSON.parse(xhr.responseText);
                let chatItems =  response['chat-update'].chat;
                let dbColumnNames = response['chat-update'].dbColumnNames;
                let messageId = response['chat-update'].messageId;
                if (that.messageId === null && messageId !== null) {
                    that.messageId = messageId;
                    that.thread = response['chat-update'].thread;
                    that.resetDoneOnNewMessage = response['chat-update'].optionTagDoneResetOnNewMessage;
                }

                // Get most recent/latest message data. This equals the highest response message id
                let actualState = that.getActualStateInfo(response);

                // Create chat content
                that.refreshChat(that.chatWindow, chatItems, actualState, dbColumnNames, 'save');

                // Clear input field
                that.chatInput.value = '';

                // Reset done btn
                if (that.resetDoneOnNewMessage) {
                    that.refreshToolbar(cId, chatRoom);
                }

                if (that.connection) {
                    let chatMessage = {
                        type: 'ping',
                        data: cId,
                        chatRoom: chatRoom
                    };

                    that.connection.send(JSON.stringify(chatMessage));
                }
                console.log('Chat successfully saved.');
            } else {
                console.log('Error in chat saving.');
            }
        };

        xhr.onerror = function() {
            console.log('Request failed.');
        };

        xhr.send(serializedForm);
    };

    // Encode message input to prevent JS scripting attacks.
    qfqChat.htmlEncode = function(str) {
        let div = document.createElement('div');
        div.textContent = str;
        return div.innerHTML;
    }

    n.qfqChat = qfqChat;

})(QfqNS.Helper);
