/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */


/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};
/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes jqxDateTimeInput widgets.
     *
     * It configures all `<div>`s having a class `jqw-datetimepicker` as jqxDateTimeInput.
     *
     * Given the HTML snippet
     *
     *     ...
     *     <div class="jqw-datetimepicker" data-control-name="datetimepicker" ></div>
     *     ...
     *
     * it will create a hidden input sibling element of the `<div>` where the selected date is stored for plain old
     * form submission, thus rendering the above snippet effectively to
     *
     *     ...
     *     <div class="jqw-datetimepicker" data-control-name="datetimepicker" ></div>
     *     <input type="hidden" name="datetimepicker">
     *     ...
     *
     * The jqxDateTimeInput can be configured using following `data` attributes
     *
     *  * `data-control-name`: Mandatory attribute. Hold the name of the input element.
     *  * `data-format-string': Optional Format string as required by jqxDateTimeInput. See also
     *    {@link http://www.jqwidgets.com/jquery-widgets-documentation/documentation/jqxdatetimeinput/jquery-datetimeinput-api.htm}.
     *    Default: "F".
     *  * `data-show-time-button`: Boolean value `true` or `false`, indicating whether or not a time picker will be
     *    displayed.
     *
     * @function
     * @name QfqNS.Helper.jqxDateTimeInput
     */
    var jqxDateTimeInput = function () {
        var index;
        var $containers = $("div.jqw-datetimepicker");

        $containers.each(function (index, object) {
            (function ($container) {
                var controlName = $container.data('control-name');
                if (!controlName) {
                    QfqNS.Log.error("jqwDateTimePicker does not have a 'data-control-name' attribute.");
                    return;
                }

                var formatString = $container.data('format-string');
                if (!formatString) {
                    formatString = "F";
                }

                var showTimeButton = $container.data('show-time-button');
                if (showTimeButton === undefined) {
                    showTimeButton = false;
                }

                var jqxDateTimeInputConfig = {
                    formatString: formatString,
                    showTimeButton: showTimeButton,
                    theme: "bootstrap"
                };

                $container.jqxDateTimeInput(jqxDateTimeInputConfig);

                // Our code creates a hidden input element for each jqxwidget as sibling of the widget. We do this,
                // because jqxwidget don't create named input elements, and thus the value would not be sent to the
                // server using a Plain Old Form submission (even if performed by an ajax request).
                var $hiddenInput = $("<input>")
                    .attr('type', 'hidden')
                    .attr('name', controlName);

                $container.after($hiddenInput);

                $hiddenInput.val($container.jqxDateTimeInput('value').toISOString());

                $container.on('valueChanged', function (event) {
                    $hiddenInput.val(event.args.date.toISOString());
                });
            })($(object));
        });

    };

    n.jqxDateTimeInput = jqxDateTimeInput;


})(QfqNS.Helper);