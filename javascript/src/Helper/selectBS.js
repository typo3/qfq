/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */

var QfqNS = QfqNS || {};

/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    /**
     * Initializes all FEs type select with the selectBS notation.
     * Only elements having the class "qfq-select-bs-parent" are initialized.
     *
     * @function
     */
    let selectBS = function () {

        // all selectBS <div> elements that contain the button and list used for the dropdown
        const dropdownParent = document.querySelectorAll(".qfq-select-bs-parent");

        let lastCursorPos = {x: 0, y: 0};
        let disabledClick = false;

        const observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {

                // selected option from dropdown
                let prevSelectedItem = mutation.target.querySelector(".qfq-select-bs-list > li[data-selected]");

                // focused option(s) from dropdown
                let prevFocusedItem = mutation.target.querySelector(".qfq-select-bs-list > li.qfq-item-focused");

                // remove class/style
                if (prevFocusedItem !== prevSelectedItem) {
                    prevFocusedItem.classList.remove('qfq-item-focused');
                }

                if (!prevSelectedItem) {
                    return;
                }

                prevSelectedItem.classList.add('qfq-item-focused');

                // dropdown is open
                if (mutation.target.classList.contains("open")) {

                    // scroll selected item into center of dropdown
                    prevSelectedItem.parentElement.scrollTop = prevSelectedItem.offsetTop - prevSelectedItem.parentElement.offsetHeight / 2;
                }
            });
        });

        dropdownParent.forEach(function (parentItem) {
            const enabledItems = parentItem.querySelectorAll(".qfq-select-bs-list > li:not(.qfq-item-disabled)");
            const disabledItems = parentItem.querySelectorAll(".qfq-select-bs-list > li.qfq-item-disabled");
            const dropdownButton = parentItem.querySelector(".qfq-select-bs-button");

            // adding events to all options
            enabledItems.forEach(function (item) {

                // hover style
                item.addEventListener("mousemove", function (event) {

                    // set current cursor position
                    let currentCursorPos = {x: event.screenX, y: event.screenY};

                    // cursor has not been moved
                    if (currentCursorPos.x === lastCursorPos.x && currentCursorPos.y === lastCursorPos.y) {
                        return;
                    }

                    lastCursorPos = {x: event.screenX, y: event.screenY};

                    let previousFocusedItem = parentItem.querySelector(".qfq-select-bs-list > li.qfq-item-focused");
                    previousFocusedItem.classList.remove('qfq-item-focused');

                    item.classList.add('qfq-item-focused');
                });

                // insert value
                item.addEventListener("click", function (event) {
                    selectBS.insertValue(item);
                    parentItem.classList.toggle("open");
                    dropdownButton.focus();
                });
            });

            disabledItems.forEach(function (item) {

                // nothing happens
                item.addEventListener("click", function (event) {
                    disabledClick = true;
                    dropdownButton.focus();
                    event.preventDefault();
                    event.stopPropagation();
                });
            });

            dropdownButton.addEventListener("click", function (event) {
                parentItem.classList.toggle("open");
            });

            dropdownButton.addEventListener("blur", function(event) {

                // wait for other stuff to finish
                setTimeout(function() {
                    if (disabledClick) {
                        disabledClick = false;
                        return;
                    }

                    parentItem.classList.remove("open");
                }, 200);
            });

            dropdownButton.addEventListener("keydown", function (event) {
                let previousSelectedItem = parentItem.querySelector(".qfq-select-bs-list > li.qfq-item-focused");
                let previousIndex = Array.prototype.indexOf.call(enabledItems, previousSelectedItem);

                let selectedItem = null;
                let selectedItemSibling = null;
                let scroll = null;

                switch (event.key) {
                    case 'ArrowDown':
                        event.preventDefault();
                        selectedItem = (previousSelectedItem) ? enabledItems[previousIndex + 1] : enabledItems[0];
                        selectedItemSibling = selectedItem.nextSibling || null;

                        // determine position of item in viewport
                        scroll = selectedItem.getBoundingClientRect().bottom - selectedItem.parentElement.getBoundingClientRect().bottom + 3;

                        // scroll > 0 means that the item is outside the lower border
                        scroll = (scroll > 0) ? scroll : 0;
                        break;

                    case 'ArrowUp':
                        event.preventDefault();
                        selectedItem = (previousSelectedItem) ? enabledItems[previousIndex - 1] : null;
                        selectedItemSibling = selectedItem.previousSibling || null;

                        // determine position of item in viewport
                        scroll = selectedItem.getBoundingClientRect().top - selectedItem.parentElement.getBoundingClientRect().top - 3;

                        // scroll < 0 means that the item is outside the top border
                        scroll = (scroll < 0) ? scroll : 0;
                        break;

                    case 'Enter':
                        event.preventDefault();
                        selectBS.insertValue(previousSelectedItem);
                        parentItem.classList.toggle("open");
                        return;

                    case 'Escape':
                        parentItem.classList.remove("open");
                        return;

                    default:
                        return;
                }

                // dropdown is closed, sibling exists
                if (dropdownButton === document.activeElement && !parentItem.classList.contains("open") && selectedItemSibling && selectedItem) {
                    selectBS.insertValue(selectedItem);
                    if (previousSelectedItem) previousSelectedItem.classList.toggle('qfq-item-focused');
                    selectedItem.classList.toggle("qfq-item-focused");

                // dropdown is open, sibling exists
                } else if (dropdownButton === document.activeElement && parentItem.classList.contains("open") && selectedItemSibling && selectedItem) {

                    if (previousSelectedItem) previousSelectedItem.classList.toggle('qfq-item-focused');
                    selectedItem.classList.toggle("qfq-item-focused");

                    // scroll if item is outside of viewport
                    selectedItem.parentElement.scrollBy(0, scroll);
                }
            });

            // mutation observer
            observer.observe(parentItem, {
                attributes: true,
                attributeFilter: ["class"]
            });
        });
    }

    // insert value of option into button and hidden input
    selectBS.insertValue = function(element) {
        let dropdownParent = element.closest(".qfq-select-bs-parent");
        let button = dropdownParent.querySelector("button");
        let input = dropdownParent.querySelector("input");
        button.innerHTML = element.innerHTML + ` <span class="caret"></span>`;
        input.value = element.getAttribute("value");
        let event = new Event("input");
        input.dispatchEvent(event);

        // mark (only) current option as selected
        if (!element.getAttribute("data-selected")) {
            if (element.parentElement.querySelector("li[data-selected]")) {
                element.parentElement.querySelector("li[data-selected]").removeAttribute("data-selected");
            }
            element.setAttribute("data-selected", "");
        }
    };

    n.selectBS = selectBS;

})(QfqNS.Helper);