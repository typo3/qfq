/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */

var QfqNS = QfqNS || {};

/**
 * Qfq Helper Namespace
 *
 * @namespace QfqNS.Helper
 */
QfqNS.Helper = QfqNS.Helper || {};

(function (n) {
    'use strict';

    let activeTooltips = new Map(); // Tracks tooltips by trigger element
    let pinnedTooltips = new Set(); // Stores pinned tooltips
    let hideTimeout = null; // Delay for hiding tooltips
    let showTimeout = null; // Delay for showing tooltips

    function createTooltipElement(tooltipText) {
        const tooltip = document.createElement('div');
        tooltip.classList.add('tooltip-container');
        tooltip.innerHTML = `
            <div class="tooltip-header">
                <button class="button pin-btn"><i class="fas fa-thumbtack"></i></button>
                <button class="button copy-btn"><i class="fas fa-clipboard"></i></button>
            </div>
            <div class="tooltip-content">${tooltipText.replace(/\n/g, '<br>')}</div>
        `;
        return tooltip;
    }

    function showTooltip(trigger, tooltipText) {
        if (activeTooltips.has(trigger)) {
            const { tooltip, popperInstance } = activeTooltips.get(trigger);
            tooltip.style.display = 'block';
            popperInstance.update();
            return tooltip;
        }

        const tooltip = createTooltipElement(tooltipText);
        document.body.appendChild(tooltip);

        const popperInstance = Popper.createPopper(trigger, tooltip, {
            placement: 'bottom',
            modifiers: [
                { name: 'preventOverflow', options: { boundary: 'viewport' } },
                { name: 'offset', options: { offset: [0, -3] } },
            ],
        });

        activeTooltips.set(trigger, { tooltip, popperInstance });
        addTooltipEventListeners(tooltip, trigger, popperInstance);


        tooltip.style.display = 'block';
        popperInstance.update();
        return tooltip;
    }

    function addTooltipEventListeners(tooltip, trigger, popperInstance) {
        const copyBtn = tooltip.querySelector('.copy-btn');
        const pinBtn = tooltip.querySelector('.pin-btn');

        copyBtn.addEventListener('click', () => {
            const textContent = tooltip.querySelector('.tooltip-content').innerText;
            navigator.clipboard.writeText(textContent)
                .then(() => {
                    copyBtn.innerHTML = '<i class="fas fa-check copied"></i>';
                    setTimeout(() => {
                        copyBtn.innerHTML = '<i class="fas fa-clipboard"></i>';
                    }, 1000);
                })
                .catch(err => console.error('Failed to copy text: ', err));
        });

        pinBtn.addEventListener('click', () => {
            if (pinnedTooltips.has(tooltip)) {
                tooltip.classList.remove('pinned');
                pinnedTooltips.delete(tooltip);
                pinBtn.innerHTML = '<i class="fas fa-thumbtack"></i>';
            } else {
                tooltip.classList.add('pinned');
                pinnedTooltips.add(tooltip);
                pinBtn.innerHTML = '<i class="fas fa-thumbtack copied"></i>';
            }
        });

        tooltip.addEventListener('mouseenter', () => {
            clearTimeout(hideTimeout);
        });

        tooltip.addEventListener('mouseleave', () => {
            if (!pinnedTooltips.has(tooltip)) {
                hideTooltipWithDelay(tooltip, popperInstance, trigger);
            }
        });
    }

    function hideTooltipWithDelay(tooltip, popperInstance, trigger) {
        hideTimeout = setTimeout(() => {
            if (!pinnedTooltips.has(tooltip)) {
                tooltip.style.display = 'none';
            }
        }, 200);
    }

    function initializeStickyToolTip() {
        document.querySelectorAll('.tooltip-trigger').forEach(trigger => {
            const tooltipText = trigger.getAttribute('title');
            if (tooltipText) {
                trigger.removeAttribute('title');
            }

            trigger.addEventListener('mouseenter', () => {
                clearTimeout(showTimeout);
                showTimeout = setTimeout(() => {
                    showTooltip(trigger, tooltipText);
                }, 500);
            });

            trigger.addEventListener('mouseleave', () => {
                clearTimeout(showTimeout);
                const tooltipData = activeTooltips.get(trigger);
                if (tooltipData && !pinnedTooltips.has(tooltipData.tooltip)) {
                    hideTooltipWithDelay(tooltipData.tooltip, tooltipData.popperInstance, trigger);
                }
            });
        });
    }

    n.initializeStickyToolTip = initializeStickyToolTip;
})(QfqNS.Helper);