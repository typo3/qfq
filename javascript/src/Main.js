/* global $ */
/* @depend TablesorterController.js */

Function.prototype.bind = Function.prototype.bind || function (thisp) {
    var fn = this;

    return function () {
        return fn.apply(thisp, arguments);
    };
};

var QfqNS = QfqNS || {};

$(document).ready(function () {
    (function (n) {
        n.form = '';
        try {
            var tablesorterController = new n.TablesorterController();
            $('.tablesorter').each(function (i) {
                tablesorterController.setup($(this), i);
            }); // end .each()

            $('.tablesorter-filter').addClass('qfq-skip-dirty');
            $('select.qfq-tablesorter-menu-item').addClass('qfq-skip-dirty');
            $('.tablesorter-column-selector>label>input').addClass('qfq-skip-dirty');
            // This is needed because after changing table-view, class of input field is empty again
            $('button.qfq-column-selector').click(function () {
                $('.tablesorter-column-selector>label>input').addClass('qfq-skip-dirty');
            });

            var collection = document.getElementsByClassName("qfq-form");
            var qfqPages = [];
            for (const form of collection) {
                const page = new n.QfqPage(form.dataset);
                qfqPages.push(page);
            }

            // Get form object for later manipulations (example: filePond objects)
            if (qfqPages[0] !== undefined) {
                n.form = qfqPages[0].qfqForm.form;
            } else {
                // Readonly fabric for report
                $(".annotate-graphic").each(function () {
                    var qfqFabric = new QfqNS.Fabric();
                    var page = {}
                    qfqFabric.initialize($(this), page);
                });

                $(".annotate-text").each(function () {
                    var codeCorrection = new QfqNS.CodeCorrection();
                    var page = {}
                    codeCorrection.initialize($(this), page);
                });

            }
        } catch (e) {
            console.log(e);
        }

        $('.qfq-auto-grow').each(function () {
            var minHeight = $(this).attr("rows") * 14 + 18;
            var newHeight = $(this).prop('scrollHeight');
            var maxHeight = $(this).data('max-height') || 0;

            if ($(this).val === '' || newHeight < minHeight) {
                return;
            }

            if (newHeight < maxHeight || maxHeight === 0) {
                $(this).height(newHeight);
            } else {
                $(this).height(maxHeight);
            }
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('.qfq-auto-grow').each(function () {
                var minHeight = $(this).attr("rows") * 14 + 18;
                var newHeight = $(this).prop('scrollHeight');
                var maxHeight = $(this).data('max-height') || 0;

                if ($(this).val === '' || newHeight < minHeight) {
                    return;
                }

                if (newHeight < maxHeight || maxHeight === 0) {
                    $(this).height(newHeight);
                } else {
                    $(this).height(maxHeight);
                }
            });
        });

        $('.qfq-auto-grow').on('input paste', function () {
            var newHeight = $(this).prop('scrollHeight');
            var maxHeight = $(this).data('max-height') || 0;
            if ($(this).outerHeight() < newHeight) {
                if (newHeight < maxHeight || maxHeight === 0) {
                    $(this).height(newHeight);
                }
            }
        });

        n.initializeQfqClearMe = function () {
            $('.qfq-clear-me').each(function () {
                var myInput = $(this);
                if (!myInput.is("input,textarea") || myInput.parent().children().hasClass('qfq-clear-me-button')) {
                    return;
                }
                var closeButton = $("<span>", {
                    class: "qfq-clear-me-button",
                    html: "&times;"
                });
                if (myInput.hasClass('qfq-clear-me-table-sorter')) {
                    // Clear me is in table-sorter table and needs some different style
                    closeButton.attr('style', 'right: 3px;top: 11px;');
                }
                if (myInput.val() == '' || myInput.is('[disabled=disabled]')) {
                    closeButton.addClass("hidden");
                }
                closeButton.on("click", function (e) {
                    myInput.val('');
                    closeButton.addClass("hidden");
                });
                $(this).after(closeButton);
                $(this).on("input", function () {
                    if (myInput.val() != '' && !myInput.is('[disabled=disabled]')) {
                        closeButton.removeClass("hidden");
                    } else {
                        closeButton.addClass("hidden");
                    }
                });

            });


            $('.qfq-clear-me-multiform').each(function () {
                var myInput = $(this);
                if (!myInput.is("input,textarea") || myInput.parent().children().hasClass('qfq-clear-me-button')) {
                    return;
                }
                var closeButton = $("<span>", {
                    class: "qfq-clear-me-multiform-button",
                    html: "&times;"
                });
                if (myInput.val() == '' || myInput.is('[disabled=disabled]')) {
                    closeButton.addClass("hidden");
                }
                closeButton.on("click", function (e) {
                    myInput.val('');
                    closeButton.addClass("hidden");
                });
                $(this).after(closeButton);
                $(this).on("input", function () {
                    if (myInput.val() != '' && !myInput.is('[disabled=disabled]')) {
                        closeButton.removeClass("hidden");
                    } else {
                        closeButton.addClass("hidden");
                    }
                });

            });
        };


        n.initializeDatetimepicker = function (flagDynamicElement) {
            var selector = '.qfq-datepicker';
            if (flagDynamicElement) {
                selector = '.qfq-datepicker:empty';
            }

            $(selector).each(function () {
                var dates = {};
                var dateArray = {};
                var datesToFormat = ["minDate", "maxDate"];
                var correctAttributeNames = ["mindate", "maxdate"];
                var onlyTime = false;
                for (var i = 0; i < datesToFormat.length; i++) {
                    var date = false;
                    if ($(this).data(correctAttributeNames[i])) {
                        var cleanDate = $(this).data(correctAttributeNames[i]).split(" ")[0];
                        var cleanTime = $(this).data(correctAttributeNames[i]).split(" ")[1];
                        if (cleanDate.includes('.')) {
                            dateArray = cleanDate.split(".");
                            date = dateArray[1] + "/" + dateArray[0] + "/" + dateArray[2];
                        } else if (cleanDate.includes('-')) {
                            dateArray = cleanDate.split("-");
                            date = dateArray[1] + "/" + dateArray[2] + "/" + dateArray[0];
                        } else {
                            var today = new Date();
                            var dd = String(today.getDate()).padStart(2, '0');
                            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                            var yyyy = today.getFullYear();
                            date = mm + "/" + dd + "/" + yyyy + " " + cleanDate;
                            onlyTime = true;
                        }
                        if (cleanTime !== '' && cleanTime !== undefined) {
                            date = date + " " + cleanTime;
                        } else if (correctAttributeNames[i] === "maxdate" && !onlyTime) {
                            date = date + " 23:59:59";
                        }
                    }
                    dates[datesToFormat[i]] = date;
                }

                var options = {
                    locale: $(this).data("locale") || "en",
                    daysOfWeekDisabled: $(this).data("days-of-week-disabled") || [],
                    minDate: dates.minDate,
                    maxDate: dates.maxDate,
                    format: $(this).data("format") || "DD.MM.YYYY HH:mm",
                    viewMode: $(this).data("view-mode-default") || "days",
                    showClear: ($(this).data("show-clear-button") !== undefined) ? $(this).data("show-clear-button") : true,
                    calendarWeeks: ($(this).data("show-calendar-weeks") !== undefined) ? $(this).data("show-calendar-weeks") : false,
                    useCurrent: ($(this).data("use-current-datetime") !== undefined) ? $(this).data("use-current-datetime") : false,
                    sideBySide: ($(this).data("datetime-side-by-side") !== undefined) ? $(this).data("datetime-side-by-side") : false,
                };

                var currentDatePicker = $(this).datetimepicker(options);
                currentDatePicker.on('dp.error', function(event) {
                    // Clear the input field
                    $(this).find('input').val('');
                    $(this).data('DateTimePicker').clear();

                    // Display your custom error message
                    var alert = new QfqNS.Alert({
                        message: "Invalid Date. Min: " + dates.minDate + " Max: " + dates.maxDate,
                        type: "warning",
                        timeout: 5000
                    });
                    alert.show();
                });

            });
        };

        n.initializeIgnoreHistoryBtn = function () {
            // Attaching the event listener to the document
            document.addEventListener('click', function (event) {
                var element = event.target;

                // Traverse up to find the element with 'data-ignore-history'
                while (element && !element.hasAttribute('data-ignore-history')) {
                    element = element.parentNode;
                    if (element === document) {
                        return; // Exit if reached the document without finding the target
                    }
                }

                if (element) {
                    handleIgnoreHistoryClick(event, element);
                }
            });
        };

        function handleIgnoreHistoryClick(event, element) {
            event.preventDefault();
            let alertButton = document.querySelector('.alert-interactive .btn-group button:first-child');
            let url = element.href;

            if (alertButton) {
                alertButton.onclick = function () {
                    if (url) {
                        window.location.replace(url);
                    }
                };
            } else {
                window.location.replace(url);
            }
        }

        n.initializeQfqClearMe();
        n.initializeDatetimepicker();
        n.initializeIgnoreHistoryBtn();
        n.Helper.codemirror();
        n.Helper.calendar();
        n.Helper.selectBS();
        n.Helper.initializeStickyToolTip();

        // Initialize chat elements
        let chatWindowsElements = document.getElementsByClassName("qfq-chat-window");
        let chatInstances = n.Helper.qfqChat(chatWindowsElements);

        if (typeof FilePond !== 'undefined') {
            FilePond.registerPlugin(FilePondPluginFileValidateSize);
            FilePond.registerPlugin(FilePondPluginFileValidateType);
        }

        // Initialize merge window
        let mergeWindow = document.querySelector('.merge-window');
        if (mergeWindow !== null) {
            let mergeInstance = n.Helper.qfqMerge(mergeWindow);
        }

        // Initialize form history
        let formHistoryBtn = document.querySelector('.qfq-history-btn');
        if (formHistoryBtn !== null) {
            n.Helper.qfqHistory(formHistoryBtn, n);
        }

        // Get currently shown upload elements to initialize them
        const activeTabContent = document.querySelector('.tab-pane.active');
        if (activeTabContent) {
            n.Helper.initializeFilePondInContainer(activeTabContent, n.form);
        } else {
            // In case of forms without tabs or reports
            n.Helper.initializeFilePondInContainer(document, n.form);
        }

    })(QfqNS);
});


