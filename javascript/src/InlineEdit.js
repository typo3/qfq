var QfqNS = QfqNS || {};
$(document).ready(function() {
    (function(n) {
        $('.qfq-inline-edit').on('click', function() {
            var dataSip = $(this).data('sip');
            var $input = $(this).children('textarea');
            var $label = $(this).children('div.qfq-inline-edit-label');

            if ($input.length === 0) {
                // If the textarea doesn't exist, retrieve it
                retrieveTextbox(dataSip, $label);
            } else {
                $input.removeClass('hidden');
                $input.focus();
                $label.addClass('hidden');
            }
        });
    })(QfqNS);
});
function retrieveTextbox(dataSip, $label) {
    $.ajax({
        url: "typo3conf/ext/qfq/Classes/Api/inlineEditLoad.php?s=" + dataSip,
        method: "POST",
        success: function(response) {
            // Create the textbox and modify its properties
            var $textBox = createTextbox(response);
            $textBox.removeClass('qfq-auto-grow');
            $label.addClass('hidden');
            insertAfterLabel($label, $textBox);
            setTextboxHeight($textBox);
            enableAutoGrow($textBox);
            registerBlurHandler($textBox, dataSip, $label);
        },
        error: function() {
            console.error('inlineEditLoad.php API error');
        }
    });
}
function registerBlurHandler($textBox, dataSip, $label) {
    // Register the blur event handler for the textbox
    $textBox.on('blur', function() {
        var updatedValue = $(this).val();

        // Send an AJAX request to update the record in the database
        $.ajax({
            url: "typo3conf/ext/qfq/Classes/Api/inlineEditSave.php?s=" + dataSip,
            method: "POST",
            data: { 'updatedValue': updatedValue },
            success: function(response) {
                $textBox.addClass('hidden');
                $label.text(response).removeClass('hidden');
            },
            error: function() {
                $textBox.addClass('hidden');
                $label.removeClass('hidden');
            }
        });
    });
}
function createTextbox(response) {
    // Create a textbox element from the server response
    var $textBox = $(response);
    $textBox.addClass('qfq-inline-edit-input');
    $textBox.attr('spellcheck', 'false');
    return $textBox;
}
function insertAfterLabel($label, $textBox) {
    // Insert the textbox after the label and set focus on the textbox
    $label.after($textBox);
    $textBox.focus();
}
function setTextboxHeight($textBox) {
    // Set the height of the textbox to match its content
    $textBox.css('height', 'auto');
    $textBox.css('height', $textBox[0].scrollHeight + 'px');
}
function enableAutoGrow($textBox) {
    // Enable the textbox to auto-grow based on its content
    $textBox.on('input', function() {
        // Set the height of the textbox to match its content
        $textBox.css('height', 'auto');
        $textBox.css('height', $textBox[0].scrollHeight + 'px');
    });
}


