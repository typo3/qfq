/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Comment.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages a group of comments
     *
     */
    n.CommentController = function () {
        this.comments = [];
        this.currentUser = {};
        this.$container = {};
        this.$parent = {};
        this.height = "auto";
        this.options = {};
        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
    };

    n.CommentController.prototype.on = n.EventEmitter.onMixin;

    n.CommentController.prototype.setCurrentUser = function(user) {
        this.currentUser = user;
    };

    /**
     * changeHandler emits custom events for actions.
     * Additionally writes log entries to console for easier
     * testing.
     * @private
     * @param event String containing possible change states
     * @return {boolean} true on success
     */
    n.CommentController.prototype._changeHandler = function(event, comment) {
        if (event === "edit") {
            this.eventEmitter.emitEvent('comment.edited',
                n.EventEmitter.makePayload(this, "edit"));
            console.log("[CommentController] Event comment.edit emitted");
            return true;
        } else if (event === "new") {
            this.eventEmitter.emitEvent('comment.added',
                n.EventEmitter.makePayload(this, comment));
            console.log("[CommentController] Event comment.add emitted");
            return true;
        } else if (event === "remove") {
            this.eventEmitter.emitEvent('comment.removed',
                n.EventEmitter.makePayload(this, comment));
        }
        console.error("[CommentController] Changehandler called without valid event");
        return false;
    };

    n.CommentController.prototype.emitEvent = function(event) {
        this._changeHandler(event);
    };

    n.CommentController.prototype.buildContainer = function($hook, options) {
        var $container = $("<div />", {
            class: "qfqCommentContainer"
        });
        this.options = options;
        $hook.after($container);
        this.$container = $container;
    };

    n.CommentController.prototype.hasComments = function() {
        if (this.comments.length > 0) {
            return true;
        } else {
            return false;
        }
    };

    n.CommentController.prototype.toggle = function() {
        this.$container.slideToggle("swing");
    };

    n.CommentController.prototype.getComment = function(reference) {
        if (reference < this.comments.length && reference >= 0) {
            return this.comments[reference];
        } else {
            console.error("[CommentController] Requested Comment doesn't exist");
            return false;
        }
    };

    n.CommentController.prototype.addComment = function(comment, user) {
        var commentObject = new n.Comment(comment, user, this.$container, this.options);
        commentObject.display();
        this.comments.push(commentObject);
        this._changeHandler("new", commentObject);
        this._setListeners(commentObject);
        this.updateHeight();
        return this.comments.length - 1;
    };

    n.CommentController.prototype._setListeners = function(commentObject) {
        var that = this;
        commentObject.on('comment.edited', function(e) {
            that.updateComment(e.data);
        });
        commentObject.on('comment.reply', function(e) {
            that.requestReply(e.data);
        });
        commentObject.on('comment.deleted', function(e) {
            that.removeComment(e);
        });
    };

    n.CommentController.prototype.displayComments = function() {
        for (var i = 0; this.comments; i++) {
            this.comments[i].display();
        }
        this.updateHeight();
    };

    n.CommentController.prototype.displayEditor = function() {
        if (!this.options.readOnly) {
            var editor = new n.Editor();
            var that = this;
            var $editor = editor.buildEditor();
            editor.on("editor.submit", function (editor) {
                that._handleEditorSubmit(editor);
            });
            $editor.appendTo(this.$container);
            editor.$textArea.focus();
        }
    };

    n.CommentController.prototype._handleEditorSubmit = function(editor) {
        var comment = this.buildCommentObject(editor.data.text);
        this.addComment(comment, this.currentUser);
        editor.data.destroy();
    };

    n.CommentController.prototype.buildCommentObject = function(text) {
        var comment = {};
        comment.comment = text.replace("&quot;", "'");
        comment.dateTime = new Date().toLocaleString('de-CH');
        comment.uid = this.currentUser.uid;
        return comment;
    };

    n.CommentController.prototype.getContainer = function() {
        return this.$container;
    };

    n.CommentController.prototype.removeComment = function(reference) {
        this._changeHandler("remove", reference);
    };

    n.CommentController.prototype.updateComment = function(data) {
        console.log("[Comment Changed] User: " + data.uid +
            " Text:" + data.comment.substring(0, 20) + "...");
        this.emitEvent("edit");
    };

    n.CommentController.prototype.requestReply = function(data) {
        this.displayEditor();
    };

    n.CommentController.prototype.updateHeight = function() {
        //this.height = this.$container.height();
        //this.$container.css("max-height", this.height);
    };

    n.CommentController.prototype.importComments = function(comments, users) {
        for (var i=0; i < comments.length; i++) {
            var user = this._searchUsersByUid(users, comments[i].uid);
            this.addComment(comments[i], user);
        }
        if (comments.length === 0) {
            this.displayEditor();
        }
    };

    n.CommentController.prototype.exportComments = function() {
        var comments = [];
        for(var i=0; i < this.comments.length; i++) {
            if (!this.comments[i].deleted) {
                comments.push(this.comments[i].comment);
            }
        }
        return comments;
    };

    n.CommentController.prototype._searchUsersByUid = function (users, uid) {
        for (var i=0; i < users.length; i++) {
            if (users[i].uid === uid) {
                return users[i];
            }
        }
    };

})(QfqNS);