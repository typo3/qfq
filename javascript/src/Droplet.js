/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Element/ElementBuilder.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {

    n.Droplet = function (url, color) {
        this._$trigger = {};
        this.position = {};
        this._$container = {};
        this.visible = false;
        this.eventEmitter = new EventEmitter();
        this.content = {};
        this.forms = [];
        this.url = url;
        this.color = color;
    };

    n.Droplet.prototype.on = n.EventEmitter.onMixin;

    n.Droplet.prototype.setTrigger = function($trigger) {
        this._$trigger = $trigger;
        var that = this;
        this._$trigger.click(function() {that.toggleDroplet();});
    };

    n.Droplet.prototype.setContainer = function($container) {
        this._$container = $container;
    };

    n.Droplet.prototype.setPosition = function(left, top) {
        this.position = {
            left: left,
            top: top
        };
    };

    n.Droplet.prototype.getContent = function() {
        var that = this;
        var response = jQuery.getJSON(this.url, function(data) {
            that._processResponse(data);
        });
        this._$container.text("Getting Data...");
    };

    n.Droplet.prototype._processResponse = function(data) {
        this._$container.text('');
        for(var i=0; i < data.elements.length; i++) {
            var element = data.elements[i];
            var $element = new n.ElementBuilder(element);
            this._$container.append($element.display());
            var that = this;
            if (element.type === "form") this.forms.push($element);
        }

        this.forms[0].on('form.submit.success',
            function() { that.toggleDroplet(); });
    };

    n.Droplet.prototype.createContainerBellowTrigger = function () {
        this.setPosition(
            this._$trigger.offset().left,
            this._$trigger.offset().top + this._$trigger.outerHeight()
        );
        var $container = $("<div />", {
            class: "qfq-droplet-container qfq-droplet-" + this.color
        });
        $container.css({
            position: 'absolute',
            zIndex: '100',
            top: this.position.top + 10 + "px",
            left: this.position.left + "px"
        });

        $(document.body).append($container);
        $container.addClass("qfq-droplet-container");
        $container.hide();
        return $container;
    };

    n.Droplet.prototype.getContainer = function() {
        if (this._$container) {
            console.error("No container has been created");
        } else {
            return this._$container;
        }
    };

    n.Droplet.prototype.toggleDroplet = function () {
        if (this.visible) {
            this._$container.hide();
            this.visible = false;
            this.forms = [];
        } else {
            this.eventEmitter.emitEvent('droplet.toggle',
                n.EventEmitter.makePayload(this, "toggle"));
            this._$container.show();
            this.visible = true;
            if (this.url) {
                this.getContent();
            }
        }
    };

    n.Droplet.prototype.hideDroplet = function() {
        if (this.visible) {
            this._$container.hide();
            this.visible = false;
        }
    };

})(QfqNS);
