/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
/* global $ */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    n.escapeJqueryIdSelector = function (idSelector) {
        return idSelector.replace(/(:|\.)/, "\\$1");
    };

})(QfqNS);


// Hide long text and append "more.." button
$(document).ready(function () {
    var more = '[...]';
    var less = '[<<]';
    var moreButtonHtml = '<button class="btn btn-link" type="button" style="outline-width: 0px;">' + more + '</button>';
    var moreButton = $(moreButtonHtml).insertAfter('span.qfq-more-text');
    moreButton.click(function () {
        var moreText = $(this).siblings('span.qfq-more-text');
        if ($(this).text() === more) {
            $(this).text(less);
            moreText.show();
        } else {
            $(this).text(more);
            moreText.hide();
        }
    });
});