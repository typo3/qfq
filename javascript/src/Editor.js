/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */
/* @depend Comment.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Manages Text Editor for Comments
     *
     */
    n.Editor = function () {
        this.$container = {};
        this.$textArea = {};
        this.$submitButton = {};
        this.text = "";
        // Event Emitter is a Library qfq uses to emit custom Events.
        this.eventEmitter = new EventEmitter();
    };

    n.Editor.prototype.on = n.EventEmitter.onMixin;


    n.Editor.prototype.buildEditor = function(text) {
        var that = this;
        this.$container = $("<div />", {
            class: "qfqEditorContainer"
        });
        this.$textArea = $("<div />", {
            class: "qfqEditor",
            contenteditable: true,
            tabindex: 0
        });
        if (text) {
            this.$textArea.html(text);
        }
        this.$textArea.keydown(function() { that.activateSubmit(); });
        this._addEditorControls();
        var controls = $("<div />", {
            class: "qfqEditorControls"
        });
        var submitButton = $("<button />", {
            class: "btn btn-primary",
            disabled: true,
            text: "Send"
        });
        submitButton.on("click", function() { that._handleClick();});
        submitButton.appendTo(controls);
        this.$submitButton = submitButton;

        this.$textArea.appendTo(this.$container);
        controls.appendTo(this.$container);
        return this.$container;
    };

    n.Editor.prototype.activateSubmit = function() {
        this.$submitButton.attr("disabled", false);
    };

    n.Editor.prototype.destroy = function() {
          this.$container.remove();
    };

    n.Editor.prototype._handleClick = function() {
        var that = this;
        var text = this.$textArea.text().replace(/\s/g, '');
        if (text === "") {
            var alert = new n.Alert({
                message: "Please input text before sending.",
                type: "warning",
                modal: true,
                buttons: [
                    {label: "Ok", eventName: "ok"}
                ]
            });
            alert.show();
        } else {
            this.text = this.$textArea.html();
            this.eventEmitter.emitEvent('editor.submit',
                n.EventEmitter.makePayload(this, that));
        }
    };

    n.Editor.prototype._playingWithSelection = function() {
        var selection = window.getSelection();
        console.log(selection);
        if (!selection.isCollapsed) {
            var currentNode = selection.anchorNode;
            var count = 1;
            if (selection.anchorNode.nextSibling !== null) {
                if (currentNode.nextSibling.isSameNode(selection.focusNode)) {
                    console.log("Selected 2 nodes");
                } else {
                    while (!currentNode.isSameNode(selection.focusNode)) {
                        count++;
                        if (currentNode.nextSibling !== null) {
                            currentNode = currentNode.nextSibling;
                        } else {
                            console.error("whoops");
                            break;
                        }
                    }
                    console.log("Selected " + count + " nodes");
                }
            } else {
                if (selection.focusNode.nextSibling !== null) {
                    if (selection.focusNode.nextSibling.isSameNode(selection.anchorNode)) {
                        console.log("Selected 2 nodes");
                    } else {
                        currentNode = selection.focusNode;
                        while (!currentNode.isSameNode(selection.focusNode)) {
                            count++;
                            if (currentNode.previousSibling !== null) {
                                currentNode = currentNode.previousSibling;
                            } else {
                                console.error("whoops");
                                break;
                            }
                        }
                        console.log("Selected " + count + " nodes");
                    }
                } else {
                    console.log(selection.toString());
                    selection.deleteFromDocument();
                }
            }
        } else {
            console.log("Selected one node");
        }
    };

    n.Editor.prototype._addEditorControls = function() {
        var that = this;
        var $addCode = $("<span />", {
            class: "glyphicon glyphicon-console qfqEditorControl qfqCodeAdd"
        });
        $addCode.on("click", function() { that._addCodeElement();});
        $addCode.appendTo(this.$container);
        var $addList = $("<span />", {
            class: "glyphicon glyphicon-list qfqEditorControl qfqCodeList"
        });
        $addList.on("click", function() { that._addListElement();});
        $addList.appendTo(this.$container);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addCodeElement = function() {
        var $code = $("<code />", {
            class: "qfqCodeElement",
            text: "Write your code here"
        });
        $code.appendTo(this.$textArea);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addListElement = function() {
        var $unsortedList = $("<ul />");
        var selection = window.getSelection();
        console.log(selection);
        var $listElement = $("<li />", {
            text: "Write your list"
        });
        $listElement.appendTo($unsortedList);
        $unsortedList.appendTo(this.$textArea);
        this._addStandardTextElement();
    };

    n.Editor.prototype._addStandardTextElement = function() {
        var $regular = $("<p />", {
                text: "\xA0"
            }
        );
        $regular.appendTo(this.$textArea);
    };

}(QfqNS));