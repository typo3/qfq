/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

/* global $ */
/* global EventEmitter */
/* @depend QfqEvents.js */
/* @depend Alert.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     * Display content of a file
     *
     *
     * @param {object} options option object has following properties
     * @param {string} options.webworker Path to webworker JS file
     * @param {string} options.filePath file that has to be displayed
     * @param {number} [options.interval] time interval in milliseconds.
     * @param {string} [options.highlight] Path to highlight instructions, optional
     * @param {string} [options.targetId] HTML id of the target where it should be displayed
     * @param {boolean} [options.isContinuous] Appends the text instead of replacing it
     *
     * @constructor
     */
    n.DisplayFile = function (options) {
        this.options = options;
        this.options.webworker = options.webworker ||
            "typo3conf/ext/qfq/Resources/Public/Javascript/Worker/GetFileContent.js";
    };

    n.DisplayFile.prototype.show = function() {
        var that = this;
        if (this.options.filePath) {
            if (this.options.interval) {
                var webWorker = new Worker(this.options.webworker);
                webWorker.postMessage([this.options.filePath, this.options.interval]);

                webWorker.onmessage = function(e) {
                      that._handleResponse(e);
                };
            }
        } else {
            console.alert("No filePath supplied");
          }
    };

    n.DisplayFile.prototype._handleResponse = function(e) {
        var result = e.data;
        if (this.options.targetId) {
            var $target = $("#" + this.options.targetId);
            if (this.options.isContinuous) {
                $target.append(result);
            } else {
                $target.text(result);
            }
        }
    };

})(QfqNS);