/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

/* global $ */
/* global console */
/* @depend QfqEvents.js */

/**
 * Qfq Namespace
 *
 * @namespace QfqNS
 */
var QfqNS = QfqNS || {};

(function (n) {
    'use strict';

    /**
     *
     * @param settings
     * @constructor
     *
     * @name QfqNS.QfqPage
     */
    n.QfqPage = function (settings) {
        console.log("Creating QFQPage", settings);
        this.qfqForm = {};
        this.settings = $.extend(
            {
                tabsId: "qfqTabs",
                formId: "qfqForm",
                submitTo: "typo3conf/ext/qfq/Classes/Api/save.php",
                deleteUrl: "typo3conf/ext/qfq/Classes/Api/delete.php",
                refreshUrl: "typo3conf/ext/qfq/Classes/Api/load.php",
                fileUploadTo: "typo3conf/ext/qfq/Classes/Api/upload.php",
                fileDeleteUrl: "typo3conf/ext/qfq/Classes/Api/filedelete.php",
                typeAheadUrl: "typo3conf/ext/qfq/Classes/Api/typeahead.php",
                dirtyUrl: "typo3conf/ext/qfq/Classes/Api/dirty.php",
                pageState: new n.PageState()
            }, settings
        );

        n.Log.level = settings.logLevel;

        this.intentionalClose = false;

        try {
            this.bsTabs = new n.BSTabs(this.settings.tabsId);

            var storedFormInfos = [];

            // get current state from session storage
            if(sessionStorage.getItem("formInfos") !== null) {
                storedFormInfos = JSON.parse(sessionStorage.getItem("formInfos"));
            }

            var currentForm = this.bsTabs.currentFormName;
            var currentRecordId = this.bsTabs.currentRecordId;
            var activeLastPill = this.bsTabs.currentActiveLastPill;

            var actualIndex = -1;
            var indexNr = 0;
            if(storedFormInfos.length !== 0){
                if(storedFormInfos[0] !== ''){
                    storedFormInfos.forEach(function callback(element) {
                        if(element === currentForm && storedFormInfos[indexNr+2] === currentRecordId) {
                            actualIndex = indexNr;
                        }
                        indexNr++;
                    });
                }
            }

            var currentState = this.settings.pageState.getPageState();

            // load from sessionStorage or from path given hash if not empty
            if(activeLastPill === "true") {
                if (actualIndex !== -1 && location.hash === "") {
                    currentState = storedFormInfos[actualIndex + 1];
                }
            }

            if (currentState !== "") {
                this.bsTabs.activateTab(currentState);
                n.PageTitle.setSubTitle(this.bsTabs.getTabName(currentState));
            } else {
                this.settings.pageState.setPageState(this.bsTabs.getCurrentTab(), n.PageTitle.get());
            }

            this.bsTabs.on('bootstrap.tab.shown', this.tabShowHandler.bind(this));
            this.bsTabs.on('bootstrap.tab.shown', function(e){
                // Initialize FilePond in the newly active tab content
                setTimeout(() => {
                    // Get the ID of the tab content
                    const tabContentId = e.target.currentTab;
                    const tabContent = document.querySelector(`#${tabContentId}`);
                    n.Helper.initializeFilePondInContainer(tabContent, n.form);
                }, 100);
            });

            this.settings.pageState.on('pagestate.state.popped', this.popStateHandler.bind(this));
        } catch (e) {
            n.Log.message(e.message);
            this.bsTabs = null;
        }

        try {
            this.qfqForm = new n.QfqForm(
                this.settings.formId,
                this.settings.submitTo,
                this.settings.deleteUrl,
                this.settings.refreshUrl,
                this.settings.fileUploadTo,
                this.settings.fileDeleteUrl,
                this.settings.dirtyUrl);
            this.qfqForm.setBsTabs(this.bsTabs);
            this.qfqForm.on('qfqform.destroyed', this.destroyFormHandler.bind(this));

            var that = this;
            this.qfqForm.on('qfqform.close-intentional', function () {
                that.intentionalClose = true;
            });

            window.addEventListener("beforeunload", this.beforeUnloadHandler.bind(this));
            // We have to use 'pagehide'. 'unload' is too late and the ajax request is lost.
            window.addEventListener("pagehide", (function (that) {
                return function () {
                    document.activeElement.blur();
                    that.qfqForm.releaseLock(true);
                };
            })(this));
            this.recordList = new n.QfqRecordList(settings.apiDeleteUrl);
        } catch (e) {
            n.Log.error(e.message);
            this.qfqForm = null;
        }

        var page = this;
        // Initialize Fabric to access form events
        try {
            console.log("Form ID: ", this.settings.formId)
            console.log("Looking for Fabric Element", $("#" + this.settings.formId + " .annotate-graphic"))
            if(!this.settings.formId) {
                throw new Error("QFQPage missing formId")
            }

            $("#" + this.settings.formId + " .annotate-graphic").each(function() {
                var qfqFabric = new QfqNS.Fabric();
                qfqFabric.initialize($(this), page);
            });

            $("#" + this.settings.formId + " .annotate-text").each(function() {
                var codeCorrection = new QfqNS.CodeCorrection();
                codeCorrection.initialize($(this), page);
            });

            /* initaliaze image-adjust */
            const imageEditors = document.querySelectorAll(".qfq-image-adjust")
            console.log("Image Adjust Elements", imageEditors)
            imageEditors.forEach(editor => {
                console.log("Current Editor", editor)
                const imageAdjust = new ImageAdjust()
                imageAdjust.initialize(editor, page, n)
            })
            
        } catch (e) {
            n.Log.error(e.message);
        }

        QfqNS.TypeAhead.install(this.settings.typeAheadUrl);
        QfqNS.CharacterCount.initialize();
        //n.initializeDatetimepicker(false);
    };

    /**
     * @private
     * 
     * Releaselock has to be handled at the pagehide event, not here
     */
    n.QfqPage.prototype.beforeUnloadHandler = function (event) {
        var message = "\0/";
        n.Log.debug("beforeUnloadHandler()");

        if (this.qfqForm.isFormChanged() && !this.intentionalClose) {
            n.Log.debug("Changes detected - not regular close");
            document.activeElement.blur();
            this.qfqForm.releaseLock(true);
            event.returnValue = message;
            return message;
        }
    };

    /**
     * @private
     */
    n.QfqPage.prototype.destroyFormHandler = function (obj) {
        this.settings.qfqForm = null;
        $('#' + this.settings.tabsId).remove();
    };

    n.QfqPage.prototype.tabShowHandler = function (obj) {
        // tabShowHandler will be called every time the tab will be shown, regardless of whether or not this happens
        // because of BSTabs.activateTab() or user interaction.
        //
        // Therefore, we have to make sure, that tabShowHandler() does not save the page state while we're restoring
        // a previous state, i.e. we're called because of the popStateHandler() below.
        if (this.settings.pageState.inPoppingHandler) {
            n.Log.debug("Prematurely terminating QfqPage.tabShowHandler(): called due to page state" +
                " restoration.");
            return;
        }
        var currentTabId = obj.target.getCurrentTab();
        n.Log.debug('Saving state: ' + currentTabId);

        // Implementation save current state in session storage
        var storedFormInfos = [];

        if(sessionStorage.getItem("formInfos") !== null){
            storedFormInfos = JSON.parse(sessionStorage.getItem("formInfos"));
        }

        var currentForm = obj.target.currentFormName;
        var currentRecordId = obj.target.currentRecordId;
        var activeLastPill = obj.target.currentActiveLastPill;

        var actualIndex = -1;
        var indexNr = 0;
        if(activeLastPill === "true") {
            if (storedFormInfos.length !== 0) {
                if (storedFormInfos[0] !== '') {
                    storedFormInfos.forEach(function callback(element) {
                        if (element === currentForm && storedFormInfos[indexNr + 2] === currentRecordId) {
                            actualIndex = indexNr;
                        }
                        indexNr++;
                    });
                }
            }

            // fill sessionStorage, there are 3 ways for filling the sessionStorage: 1.If empty - first time filling, 2.If there is anything - add it to them, 3
            // 1.If array from storage is empty - fill it first time
            if (storedFormInfos.length === 0) {
                storedFormInfos[0] = currentForm;
                storedFormInfos[1] = currentTabId;
                storedFormInfos[2] = currentRecordId;

                // 2.If there is anything in storage but not the actual opened forms - add this new information to the existing array
            } else if (actualIndex === -1) {
                storedFormInfos[indexNr] = currentForm;
                storedFormInfos[indexNr + 1] = currentTabId;
                storedFormInfos[indexNr + 2] = currentRecordId;

                // 3.If actual openend form is included in sessionStorage - only change the array values of the existing informations
            } else {
                storedFormInfos[actualIndex] = currentForm;
                storedFormInfos[actualIndex + 1] = currentTabId;
                storedFormInfos[actualIndex + 2] = currentRecordId;
            }

            // Set sessionStorage with customized array
            sessionStorage.setItem("formInfos", JSON.stringify(storedFormInfos));
        }
        n.PageTitle.setSubTitle(obj.target.getTabName(currentTabId));
        this.settings.pageState.setPageState(currentTabId, n.PageTitle.get());
    };

    n.QfqPage.prototype.popStateHandler = function (obj) {
        this.bsTabs.activateTab(obj.target.getPageState());
        n.PageTitle.set(obj.target.getPageData());
    };

})(QfqNS);