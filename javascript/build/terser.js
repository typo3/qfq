const { minify } = require("terser");
const fs = require('fs');

const jsPath = "javascript/build/dist/"
const extPath = "extension/Resources/Public/JavaScript/"
const cssPath = "extension/Resources/Public/Css/"

const todos = [
    {
        name: "qfq",
        input: "javascript/build/dist/qfq.debug.js",
        output: jsPath + "qfq.min.js"
    },{
        name: "qfqFabric",
        input: "javascript/src/Plugins/qfq.fabric.js",
        output: jsPath + "qfq.fabric.min.js"
    },{
        name: "qfqValidator",
        input: "javascript/src/Plugins/validator.js",
        output: jsPath + "validator.min.js"
    },{
        name: "qfqCodemirror",
        input: jsPath + "codemirror/codemirror-qfq.js",
        output: jsPath + "codemirror/codemirror-qfq.min.js",
    }
]

const defaultOptions = {
    compress: {
        defaults: false,
        ecma: 2015
    }
};

async function minifySource(input, output, options) {
    let sourceCode = fs.readFileSync(input, 'utf8');
    minify(sourceCode, options)
        .then( (res) => callWriteFile(output, res))    
}

function callWriteFile(output, sourceCode) {
    //console.log("Source Code", sourceCode)
    fs.writeFileSync(output, sourceCode.code)
}

for (const todo of todos) {
    console.log("minifying " + todo.name)
    let options = defaultOptions
    if(todo.hasOwnProperty("options")) options = todo.options
    if(todo.hasOwnProperty("mkdir")) {
        fs.mkdirSync(todo.mkdir, { recursive: true })
    }
    minifySource(todo.input, todo.output, options)
}

