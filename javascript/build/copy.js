const {ncp} = require("ncp")
const fs = require("fs")
const {exec} = require("child_process");

ncp.limit = 16

const options = {
    js: {
        clobber: true,  //overwrite dir
        stopOnErr: true,
        filter: /(.+(?<!\..*)|.+\.debug\.js|qfq\..*\.js|.+\.min\.js|.+\.min\.js\.map)$/
    },
    css: {
        clobber: true,  //overwrite dir
        stopOnErr: true,
        filter: /(.+(?<!\..*)|.*\.min\.css|qfq.*\.css|.*\.min\.css\.map)$/
    },
    font: {
        clobber: true,  //overwrite dir
        stopOnErr: true,
        filter: /(.+(?<!\..*)|.*\.ttf|.*\.svg|.*\.woff|.*\.woff2)$/
    }
}

const target = {
    js: "extension/Resources/Public/JavaScript/",
    css: "extension/Resources/Public/Css/",
    font: "extension/Resources/Public/fonts"
}

const target_dev = {
    js: "js/",
    css: "css/"
}

const todos = [
    {
        name: "bootstrap",
        js: "node_modules/bootstrap/dist/js/",
        css: "node_modules/bootstrap/dist/css/",
        font: "node_modules/bootstrap/dist/fonts/"
    }, {
        name: "jquery",
        js: "node_modules/jquery/dist/"
    }, {
        name: "tablesorter",
        custom: [
            {
                from: "node_modules/tablesorter/dist/js/jquery.tablesorter.combined.min.js",
                to: target.js
            }, {
                from: "node_modules/tablesorter/dist/js/extras/jquery.tablesorter.pager.min.js",
                to: target.js
            }, {
                from: "node_modules/tablesorter/dist/js/widgets/widget-columnSelector.min.js",
                to: target.js
            }, {
                from: "node_modules/tablesorter/dist/js/widgets/widget-output.min.js",
                to: target.js
            }
        ]
    }, {
        name: "mathjax",
        custom: [
            {
                from: "node_modules/mathjax/es5/tex-mml-chtml.js",
                to: target.js 
            }
        ]
    }, {
        name: "datetimepicker",
        js: "javascript/src/Plugins/bootstrap-datetimepicker.min.js",
        css: "javascript/src/Plugins/"
    }, {
        name: "chart-js",
        js: "node_modules/chart.js/dist/",
        custom: [
            {
                from: "node_modules/chart.js/dist/Chart.min.js",
                to: target.js
            }
        ]
    }, {
        name: "qfq",
        js: "javascript/build/dist/",
        css: "less/dist/"
    }, {
        name: "tinymce",
        js: 'node_modules/tinymce/',
        custom: [
            {
                from: "node_modules/tinymce/skins",
                to: target.js
            }, {
                from: "node_modules/tinymce/plugins",
                to: target.js
            }
        ]
    }, {
        name: "qfq plugins",
        js: "javascript/src/Plugins/",
        css: "javascript/src/Plugins/"
    }, {
        name: "codemirror qfq",
        custom: [
            {
                from: "javascript/src/Helper/codemirror/qfq.js",
                to: target.js + "codemirror/"
            }, {
                from: "less/dist/codemirror.css",
                to: target.css
            }
        ]
    }, {
        name: "fontAwesome",
        css: "node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css",
        custom: [
            {
                from: "node_modules/@fortawesome/fontawesome-free/css/all.min.css",
                to: target.css + "font-awesome.min.css"
            }, {
                from: "node_modules/@fortawesome/fontawesome-free/webfonts",
                to: "extension/Resources/Public/"
            }
        ]
    }, {
        name: "fontPassword",
        font: "resources/custom_fonts/"
    }, {
        name: "typeAhead",
        js: "node_modules/corejs-typeahead/dist/"
    }, {
        name: "EventEmitter",
        js: "node_modules/wolfy87-eventemitter/"
    }, {
        name: "fullcalendar",
        js: "node_modules/fullcalendar/dist/",
        css: "node_modules/fullcalendar/dist/"
    }, {
        name: "moment",
        js: "node_modules/moment/min/",
    }, {
        name: "jqwidgets",
        custom: [
            {
                from: "node_modules/jqwidgets-framework/jqwidgets/jqx-all.js",
                to: target.js
            }, {
                from: "node_modules/jqwidgets-framework/jqwidgets/globalization/globalize.js",
                to: target.js
            }, {
                from: "node_modules/jqwidgets-framework/jqwidgets/styles/jqx.base.css",
                to: target.css
            }, {
                from: "node_modules/jqwidgets-framework/jqwidgets/styles/jqx.bootstrap.css",
                to: target.css
            }
        ]
    }, {
        name: "filepond",
        js: "node_modules/filepond/dist/",
        css: "node_modules/filepond/dist/",
        custom: [
            {
                from: "node_modules/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.min.js",
                to: target.js
            }, {
                from: "node_modules/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js",
                to: target.js
            },
        ]
    }, {
        name: "popper",
        custom: [
            {
                from: "node_modules/@popperjs/core/dist/umd/popper.min.js",
                to: target.js
            }
        ],
    }, {
        name: "GetFileContent",
        custom: [
            {
                from: "javascript/src/Worker/GetFileContent.js",
                to: target.js
            }
        ],
    },
]

const types = ["js", "css", "font"]

console.log("Async copying files:")
for (const todo of todos) {
    for (const type of types) {
        if (todo.hasOwnProperty(type)) {
            ncp(todo[type], target[type], options[type], (err) => printProgress(err, todo.name, type));
        }
    }
    if (todo.hasOwnProperty("custom")) {
        for (const job of todo.custom) {
            if (!fs.existsSync(job.to.substring(0, job.to.lastIndexOf("/")))) {
                fs.mkdirSync(job.to.substring(0, job.to.lastIndexOf("/")));
            }
            exec('cp -r "' + job.from + '" "' + job.to + '"', (error, stdout, stderr) => printProgress(error, todo.name, "custom"))
            //ncp(job.from, job.to, options.custom, (err) => printProgress(err, todo.name, "custom"))
        }
    }
}

function printProgress(err, name, type) {
    if (err) {
        return console.error(err);
    }
    console.log(' * copied ' + type + ' ' + name);
}