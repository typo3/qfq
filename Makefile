SHELL := /bin/bash

PKG_VERSION = $(shell awk '/version/ { print $$3 }' extension/ext_emconf.php  | sed "s/'//g")

NIGHTLY_DATE = $(shell date '+%Y%m%d')
RELEASE_DATE = $(shell date '+%Y%m%d%H%M')

GIT_REVISION_SHORT = $(shell git rev-parse --short HEAD || true)
GIT_REVISION_LONG = $(shell git rev-parse HEAD || true)

VAR_TMP=/var/tmp/qfq_$(USER)
QFQ_GIT_LOG = qfq.git.log

EXTENSION_CONTENT = Classes Configuration Resources ext_emconf.php ext_localconf.php ext_tables.php ext_icon.png ext_conf_template.txt example.qfq.json RELEASE.txt vendor wikiScript.php $(QFQ_GIT_LOG)

DISTDIR=dist

# Use by jenkins to separate temp directories used by pip in order to allow concurrent builds
PIP_TMP ?= /tmp

all: archive

maintainer-clean:
	rm -f .doc_plantuml .npmpackages .phpdocinstall .plantuml_install .support .support_plantuml
	rm -rf doc/jsdoc doc/phpdoc doc/plantuml support node_modules .python_virtualenv build-dist

archive: clean qfq.zip

qfq.zip:
	cd extension/vendor; rm -rf phpoffice/phpspreadsheet/{C*,bin,c*,d*,m*,p*,sa*,.g*,.p*,.s*,.t*} doctrine myclabs phar-io phpdocumentor phpspec phpunit sebastian theseer webmozart
	cd extension; zip -r ../$@ $(EXTENSION_CONTENT)
	
clean:
#	cd doc/diagram ; $(MAKE) $@

git-revision: make-dist-dir
	echo $(GIT_REVISION_LONG) > $(DISTDIR)/revision.git

make-dist-dir:
	rm -rf $(DISTDIR)/
	mkdir $(DISTDIR)

dist-copy-extension:
	cd extension; git log | head -1000 >> $(QFQ_GIT_LOG)
	cd extension; cp -R $(EXTENSION_CONTENT) ../$(DISTDIR)/

build-dist: make-dist-dir dist-copy-extension git-revision
	echo "Distribution made in $(DIST)"

nightly: basic build-dist
	cd $(DISTDIR); zip -r ../qfq_$(VERSION)_$(RELEASE_DATE).zip  $(EXTENSION_CONTENT) revision.git -x config.ini

snapshot: basic build-dist
	cd $(DISTDIR) ; zip -r ../qfq_$(VERSION)_$(RELEASE_DATE).zip $(EXTENSION_CONTENT) revision.git -x config.ini

release: basic build-dist
	if [ -z "$(VERSION)" ] ; then  echo "Set VERSION make variable to the release version (fmt: maj.min.micro)" ; exit 1 ; fi
	cd $(DISTDIR) ; zip -r "../qfq_$(VERSION)_$(RELEASE_DATE).zip" $(EXTENSION_CONTENT) revision.git -x config.ini

plantuml:
	cd doc/diagram ; $(MAKE)

bootstrap: .npmpackages .plantuml_install .virtual_env
	npm install
	npm run build
	# take care that phpOffice is located under 'qfq/Resources/Private/vendor/phpoffice'
	# cd extension/Resources/Private; composer update
	cd extension; composer update

basic: .npmpackages .virtual_env
	npm install
	npm run build
	# IMPORTANT: install composer with no-dev flag for deployment!
	cd extension; composer install --no-dev --optimize-autoloader; cd vendor/phpoffice/phpspreadsheet; rm -rf .github bin docs samples .g* .s* .t* C* c* m* p*

.plantuml_install: .support_plantuml
	wget --no-check-certificate -O support/plantuml/plantuml.jar 'https://downloads.sourceforge.net/project/plantuml/plantuml.jar'
	touch $@

.npmpackages:
	echo "CURRENT USER '${USER}'"
	npm --version
	which npm
	node --version
	which node
	echo "${PATH}"
	# update npm at persistent location and copy node_modules (to speed up process)
	mkdir -p $(VAR_TMP)/npm
	/bin/cp package.json $(VAR_TMP)/npm/
	cd $(VAR_TMP)/npm; npm update
	/bin/cp -r $(VAR_TMP)/npm/node_modules ./
	touch $@

.support:
	mkdir -p support
	touch $@

.support_plantuml: .support
	mkdir -p support/plantuml
	touch $@

.virtual_env: pip-temp-directory
	virtualenv .python_virtualenv
	. .python_virtualenv/bin/activate ; TMPDIR="$(PIP_TMP)" pip install --upgrade sphinx==1.5.5
	touch $@

pip-temp-directory:
	test -d "$(PIP_TMP)" || mkdir -p "$(PIP_TMP)"

#.PHONY: nightly maintainer-clean snapshot release git-revision build-dist make-dist-dir dist-copy-extension pip-temp-directory plantuml sonar

copyReleaseNotes:
	cp Documentation/Release.rst extension/RELEASE.txt
	cp Documentation/Release.rst CHANGELOG.md

phpunit:
	# update composer with dev to install phpunit package
	cd extension; composer update
	# mock typo3 directory structure
	mkdir -p  typo3conf/ext/qfq
	mv -v extension/* typo3conf/ext/qfq/
	# create new kind of config (qfq.json)
	mkdir -p ../conf
	cp -v typo3conf/ext/qfq/Tests/phpunit_qfq.json ../conf/qfq.json; sed -i "s/#PHPUNIT_PASSWORD#/$(PHPUNIT_MYSQL_PASSWORD)/" ../conf/qfq.json
	cp -v typo3conf/ext/qfq/Tests/phpunit_LocalConfiguration.php typo3conf/LocalConfiguration.php
	# run phpunit
	cd typo3conf/ext/qfq/; pwd; vendor/bin/phpunit --configuration phpunit.xml

phpunit_snapshot: snapshot phpunit

phpunit_release: release phpunit

doc-local:
	cd Documentation && make docker-html
	xdg-open "Documentation/_build/html/index.html"

doc-qfqio:
	rsync -av "Documentation/_build/html/" root@w16.math.uzh.ch:/var/www/html/qfq/doc/

	
