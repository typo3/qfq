.. ==================================================
.. ==================================================
.. ==================================================
.. Header hierarchy
.. ==
..  --
..   ^^
..    ""
..     ;;
..      ,,
..
.. --------------------------------------------used to the update the records specified ------
.. Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
..             Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
..
.. Italic *italic*
.. Bold **bold**
.. Code ``text``
.. External Links: `Bootstrap <http://getbootstrap.com/>`_
.. Internal Link: :ref:`downloadButton` (default url text) or :ref:`download Button<downloadButton>` (explicit url text)
.. Add Images:    .. image:: ./Images/a4.jpg
..
.. Admonitions
..           .. note::   .. important::     .. tip::     .. warning::
.. Color:   (blue)       (orange)           (green)      (red)
..
.. Definition:
.. some text becomes strong (only one line)
..      description has to indented

.. -*- coding: utf-8 -*- with BOM.


.. include:: Includes.txt

.. _release:

Release
=======

Version 24.x.x
--------------

Date: <date>

Notes
^^^^^

Features
^^^^^^^^

Bug Fixes
^^^^^^^^^

Version 24.12.0
---------------

Date: 14.12.2024

Notes
^^^^^

* Class AbstractBuildForm refactored - the form renderer is now much cleaner to maintain.

Features
^^^^^^^^

* #17252 / Form / Form renderer refactor
* #10119 / Form / Style & catagorize dropdown lists (selectBS) - :ref:`input-select`

Bug Fixes
^^^^^^^^^

* 17998 / Form / By default, curly braces are now html encoded saved - this protects LaTeX code not to be treated as QFQ variables.


Version 24.10.0
--------------

Date: 27.10.2024

Notes
^^^^^

New Feature 'record history'.

Features
^^^^^^^^

* #7480 / Form / Record History
* #18909 / Form / QFQ Merge: Manual
* #18922 / QFQ / qfqWiki: improvements
* #19033 / QFQ / authentication in qfq
* #19054 / Report / show format definition in developer tooltip of '... AS _link'
* #19672 / QFQ / Typo3 extension QFQ version number now cotains branch name.
* #19764 / Docker / Integration of autocron in typo3docker repo
* #19788 / Docker / Integrated postfix and cron email configuration
* Doc: Update RST hints at the top

Bug Fixes
^^^^^^^^^

* #17991 / Form / filepond (upload): file extension always converted to lowercase and behaves case independent.
* #18583 / QFQ / qfqWiki: Replaced 'id=...' parameters.
* #19194 / Form / Codemirror fixed double initialization.
* #19661 / Form / Radio: fixed vertical problem.
* #19747 / Form / htmlAfter: fixed problem with dynamic update.
* #19776 / Form / extraButtonLock: fixed for dynamic update.
* Fix used constant name RENDER_MODE_7(_ONLY_URL)


Version 24.7.0
--------------

Date: 10.07.2024

Notes
^^^^^

* uploadType: options v1/v2 are now deprecated and replaced by button/dnd.
* Config.FormElement.subrecotrd FormElement.parameter.subrecord

Features
^^^^^^^^

* #15528 / Form / Subrecord: display configurable in case no record is selected.
* #15762 / Form / Copy Formelement as json.
* #16988 / Form / New FormElement.type = ImageCut
* #17989 / QFQ / oidc_in_T_store
* #18047 / Form / Filepond: uploadType: a) report invalid value, b) change `v1/v2` to `dnd/button`, c) CodeMirror
  keyword.
* #18244 / Form / Filepond: Update documentation
* #18479 / QFQ / Extension config 'update'='once'
* #18922 / QFQ / qfqWiki improvements
* #19033 / QFQ / Authentication in qfq
* #19034 / QFQ / HTTP redirection from QFQ via `... AS _link`
* QFQ / Extension config: Clean up various texts / Change qfq extension config options to drop down lists /
  Add new pill 'Form-Element'. Add option 'uploadType' to system config and form config.
* Sendmail: automatically remove '\r' in mail body text.

Bug Fixes
^^^^^^^^^

* #17991 / Form / Filepond: broken filter in file dialog box.
* #18549 / QFQ / Added SYSTEM STORE variable {{indexWiki:Y}} to configuration.
* #18713 / Form / Check for filepond parameters.
* #18742 / Form / Codemirror btncustom.
* #18793 / QFQ / T3 LocalConfiguration.php: seems to be corrupted by QFQ.
* #18984 / Form / In multiform tinymce miss to trigger dirty.
* #19042 / Form / multiform STORE_VAR variables are not replaced.
* #19043 / Form / Hidden columns (' AS "_<varname>") in MultiSql treated as without '_'.
* #19044 / Form / FE.typ='time' broken for column datetime on creating new records.
* Form / Add keyword 'processRow' in syntax highlight. Minor doc update.

Version 24.5.1
--------------

Date: 29.05.2024

Bug Fixes
^^^^^^^^^

* #18668 / Form / File download from upload FE (filepond) not possible with formModeGlobal=readonly
* #18669 / Form / File download button from upload FE (button/v1) is disabled when using formModeGlobal=readonly

Version 24.5.0
--------------

Date: 28.05.2024

Notes
^^^^^

* :ref:`customize-form-buttons` - Customize standard form buttons and add custom buttons to form.
* Heavy improvements in syntax highlighting and QFQ keyword typeahead. Best is to use the FrontEnd QFQ Edit.
* :ref:`formNote` - Add per form and record individual notes, inckl. reminder / tags / done and access control.
* :ref:`fillStoreSystemBySqlRow` - FillStoreSystemBySqlRowVariable can be used together with QFQ config variables.
* :ref:`merge-data` - Wizard to merge data, update slave id's.
* :ref:`qfq_keywords` - new report keyword *performanceReport* in report to show  a SQL Query Performance Report.
* Thumbnails will now be loaded with the HTML option 'lazy'.
* :ref:`imageCut` - new awesome FormElement ImageCut - reposition and cut image directly in the form, original not touch.
* From now on, only the most crucial QFQ delivered tables are created automatically - all others on demand.

Features
^^^^^^^^

* #10080 / Form / Popup on 'save' / 'close': configure dialog (answer yes/no/cancel)
* #12262 / Form / Buttons on top: more customable
* #17393 / Report / Improve FE Report Editor: add CodeMirror qfq language
* #17412 / Form / note, tag, reminder date, done flag
* #18022 / Form / Filepond colors / element size / font size (new UZH CD) / border
* #18141 / QFQ / Config: new FillStoreSystemBySqlRowVariable
* #18164 / QFQ / Wizard to merge duplicate data
* #18193 / Report / SQL Query Performance Report
* #18243 / QFQ / Variable / Escape: a) htmlentity, b) htmlentity all (incl. curly braces), c) striptags
* #18250 / Form / MultiForm fake outer record
* #18280 / Report / thumbnail: loading="lazy"
* #18285 / Form / FormElement ImageCut (ImageAdjust)
* #18404 / QFQ / New Default CharSet: utf8mb4
* #18454 / Form / Upload: Visual changes for upload FE (filepond)
* #18547 / QFQ / Create QFQ tables on demand
* Doc / Removed parameter "title" for subrecord. Comment from ticket: "Korrekt - 'title' wurde nie implementiert. Der Value von 'label' wird angezeigt.".
* Doc / Add package/lib requirement for PHP /password: libargon2

Bug Fixes
^^^^^^^^^

* #9281 / QFQ / Allow STRICT_TRANS_TABLES
* #10766 / Form / Radiobutton - parameter.buttonClass=btn-default: dynamic updat
* #11237 / Form / Radiobutton - parameter.buttonClass= btn-default - kein dirty Trigger
* #14530 / QFQ / Variables: Action 'X' does not output Type message
* #17257 / QFQ / Migrate QFQ system tables: set default value
* #17272 / Form / Save button dirty
* #17384 / Report / Thumbnail: a) touch(): Unable to create file, b) s:0 broken
* #17498 / Form / Bugfix Previous/Next Buttons on Form
* #17569 / Form / Form.title text/children not centered
* #17570 / Report / qfq-badge same as badge from bootstrap
* #17656 / Form / Browser: regexp Pattern Match broken/error
* #17778 / Form / Upload: Filepond Dynamic Update - first load hidden
* #17785 / QFQ / Undefined download storeSystem
* #17992 / Form / Upload: Filter on file extension is case dependent
* #18031 / QFQ / Fixed not compatible unit tests for php 8 and phpunit 9.6.15
* #18045 / Report / Download Jupyter Files (.ipynb): should offer 'save as', but shows inline
* #18048 / Form / Upload: FE.downloadButton missing tooltip
* #18058 / Report / Monitor not working: missing JS file
* #18073 / Form / typeAheadTagInsert failed to insert new tags.
* #18094 / Form / Loading Fabric without Form not working anymore
* #18186 / QFQ / Autocron Timezone Problems
* #18289 / QFQ / Merge Feature: Missing function for merge.log
* #18331 / Form / Implement to save multiple record with ID = 0 for multiform Refs
* #18354 / Form / Visual bug while saving multiform
* #18412 / Form / Upload: mimetype image/* broken
* #18507 / Form / Buttons customable: vertical alignment	Actions
* #18509 / Form / After saving Formelement need a reload
* #18604 / Form / CodeCorrection throws malformed json error
* #18610 / Form / ImageCut: Exception 'Missing imageSource' after upload + save


Version 24.3.0
--------------

Date: 02.03.2024

Features
^^^^^^^^

* Form / Add 'ø' to alnumx
* Add wikiScript.php to build
* CHAT.md: Add thougts about notfication
* #17393,#15361 / Form / Frontend Editing: highlighting, improve table.column hinting, support table aliases, improve indentation, add
  SQL highlighting inside QFQ variables, Generalize QFQ SQL variables to work for FormElement value and sql1, allow
  extra keyword definition in codemirror config and apply to FormEditor, add qfq sql functions to syntax highlighting
* #17593 / Report / Improve documentation and use constants for codemirror editor FE config
* #17739 / Form / multiform grid
* #17989 / Form / BE_FE_OIDC_as_QFQ_STORE_TYPO3_variables
* #18008 / Form / Refactoring upload columns.

Bug Fixes
^^^^^^^^^

* Form / Access to undefined index in FE datetime and FE upload.
* #17576 / Form / QFQ FrontEnd Editor head styling, fixes
* #17681 / Form / Wiki Bugfixing
* #17854 / Form / Fixed broken filepond css rules for chrome and brave.
* #17907 / Form / Fixed tableviewsaver js error in subrecords. Fixed not working combination of rememberLastPill and subrecord tableview.
* #17950 / Form / Readonly now disables filepond upload on client side.
* #17964 / Form / Added r upload support for filepond. Added new documentation about currently accepted extensions. Added xml upload support for filepond.
* #17997 / Form / Fixed client side check for maxFileSize. Add system maxFileSize for upload element in report.
* #18029 / Form / Store fillStoreVars in save process.
* #18044 / Form / Fabric not working


Version 24.1.0.rc1
------------------

Date: 16.01.2024

Notes
^^^^^

* Report: QFQ Syntax without numbering :ref:`nesting_alias`
* Chat: Full chat server with datebase support. FormElement :ref:`input-chat`, Config Web Socket :ref:`installation-websocket`.
  Check necessary Javascript includes (popper.min.js) :ref:`setup-css-js` or update to latest UZH_CD template.
* File Upload: New file upload function - either a FormElement :ref:`input-upload` or directly in Report :ref:`column-upload`. Supports Drag'n'Drop.
  Check necessary Javascript & CSS includes (filepond*.css|js)) :ref:`setup-css-js` or update to latest UZH_CD template.
* QfqWiki: Use TinyMCE together with QFQ to implement WIKI functionality :ref:`wiki`.
* Edit Report: syntax highlight, suggestions, indention - via FrontEnd :ref:`inline-report` editing or partly in the
  BackEnd (enable T3 extension `Editor with syntax highlighting` ). Check necessary Javascript includes
  (codemirror-qfq.min.js). :ref:`setup-css-js` or update to latest UZH_CD template.
* Form: Optional buttons for previous/next record, see :ref:`btnPreviousNextSql`.

Features
^^^^^^^^

* #4026 / QFQ / sqlLog.sql: log number of FE.id
* #4413 / Form / fieldset: show/hidden, modeSql, dynamicUpdate
* #8702 / Form / Load Record which is locked: missing user info
* #8975 / Report / Notation 2.0 - without numbers.
* #10793 / QFQ / Update NPM Package
* #15324 / Report / Direct upload via qfq/report - special case of 'Inline-Edit' ('formlet')
* #15634 / QFQ / Wiki
* #15790 / Form / Record lock (dirty): same user should be able to unlock
* #16236,#9129 / Form / FormEditor: sqlValidate - Dialog enhancements
* #16754 / QFQ / Web link checker
* #16990 / QFQ / Link 'A:data-ignore-history'
* #17086 / Form / Multiple Forms on a page
* #17192 / QFQ / Stored procedure: QIFPREPEND.
* #17200 / QFQ / Alert Container change description/buttons
* #17255 / Form / Migrate Record Locking Alerts to 'AlertInfo': not logged in qfq.log, friendly UI
* #17393 / Form / FE Report Editor: add CodeMirror qfq language
* #17461 / QFQ / Include WITH keyword
* #17462,#17540 / Form / FormElement Chat V1.0 & 1.1 - see `Documentation-develop/CHAT.md`
* #17498 / Form / Previous/Next Buttons on Form to change records

Bug Fixes
^^^^^^^^^

* #14233 / Report / AS _link: question - HTML is not rendered
* #14464 / Form / Exception: Missing button to edit FormElement with broken sqlValidate / beforeLoad
* #15880 / Form / FE Input type text and automatic slash before 'fileadmin/...'
* #16736 / QFQ / Destroys it own config
* #17169 / Form / FE.type=datetime: missing dirty on 'remove date'
* #17224 / Form / Undefined variable $rowHtml
* #17228 / T3 / Typo3 v11 deprecation registerPlugin
* #17229 / QFQ / QFQ writes Switch feUser logs
* #17256 / QFQ / Rename 'Autocron.php' - collision with 'autocron.php'
* #17293 / Doc / Undocumented Feature: emptyMeansNull works not just for text
* #17315 / Report / AS _encrypt=(...) does not encrypt data or truncates sql statement
* #17397 / Form / Dynamic Update Broken After CodingWeek23 only for r>0
* #17487 / Form / Dropdown disabled elements not showing correct width
* #17519 / Upload / FilePond js error
* #17529 / QFQ / Missing Chart.min.js in dev branch
* #17531 / QFQ / Inline editor dark mode path changed
* #17536 / QFQ / filepond upload initialization broken with multiple pills

Version 23.10.1
---------------

Date: 22.10.2023

Notes
^^^^^

Features
^^^^^^^^

* #15682 / Subrecord hide please save record first if there is no table title.
* #15098 / Implemented qfqFunction in QFQ variable for usage in forms.
* Doc: Replace many places single back tick by double back tick. Add hint for 'Row size too large'.  Add hint how to use
  mysqldump to export one row per record.
* index.rst: Added Enis & Jan as Developer.

Bug Fixes
^^^^^^^^^

* #17003 / inline edit - dark mode has wrong css path.
* #17075 / Fix broken '... AS _restClient'.
* #17091 / upload_Incorrect_integer_value_fileSize.
* #17148 / RTD: Fix broken readthedocs rendering.

Version 23.10.0
---------------

Date: 05.10.2023

Features
^^^^^^^^

* #16350 / QFQ Table 'FormSubmiLog': update recordid after insert.
* #16350 / sql.log: reference to FormSubmitLog entry. All SQL statements generated by one HTTP Post (Form Submit) can
  be identified.
* #16350 / Do not log Dirty.
* #16350 / If the T3 instance is behind a proxy, log HTTP_X_REAL_IP instead of REMOTE_ADDR in logfiles.
* #16584 / FormEditor Report: Default without statistics.
* #16589 / Implemented language configuration in backend for tt-content type qfq.
* #16798 / Report inline edit v2. Improved search inside inline edit report. Whole content will be searched. Added
  ability to switch the editor to dark mode.
* Doc: Refactor description of {{random:V}}.
* Doc: Add config option 'protectedFolderCheck'.

Bug Fixes
^^^^^^^^^

* #16573 / Fixed wrong built date and datetime string if default value was given.
* #16574 / Added multiple siteConfigurations compatibility for typo3 v10 and 11.
* #16616 / Fixed typeahead api query response problem if typeahead sql is not used.
* #16664 / Fix multi db user error.
* #16975 / Fix problem if a 'Form Submit' contains more than 64kB data. This can happen easily for 'fabric' elements.

Version 23.6.4
--------------

Date: 26.06.2023

Bug Fixes
^^^^^^^^^

* #16485 / TypeAhead: strpos() string, array given error.
* #16488 / Missing default values break saving records. New custom FE.parameter.defaultValue.
* #16491 / FE Typ Upload - JS failure: document.querySelector() is null.

Version 23.6.3
--------------

Date: 22.06.2023

Bug Fixes
^^^^^^^^^

* #16478 / Rest API: Fixed stream_get_contents failure in PHP8.1 Generic Error.

Version 23.6.2
--------------

Date: 21.06.2023

Bug Fixes
^^^^^^^^^

* #16475 / Spontaneous spaces in HTML emails.
* #16476 / SQL columns Text/Blog with default empty string becomes "''"


Version 23.6.1
--------------

Date: 16.06.2023

Notes
^^^^^

* QFQ is Typo3 V11 compatible

Bug Fixes
^^^^^^^^^

* #16372 / Upload Element 'Undefined index htmlDownloadButton' und 'unknown mode ID'
* #16381 / Form title dynamic update broken.
* #16392 / Reevaluate sanitize class for each store.
* Fix db column enum dropdown 'data truncated'
* DB Update 'alter index' pre check if exists
* FE: Upload - fix undefined index.

Version 23.6.0
--------------

Date: 09.06.2023

Features
^^^^^^^^

* Typo3 V11 compatible
* #9579 / Multiform with Process Row.
* #15770 / httpOrigin: Parameter missing in QFQ Config.
* #16285 / PHP V8 Migration.
* #16364 / T3 >=V10 pageSlug and ForwardPage.
* Add .phpunit.result.cache to .gitignore.
* Add bullet-orange.gif.
* Add index createdFeUserFormId to table FormSubmitLog.
* Doc: Add link to icons. Update use-case self-registration and add info in case of namesake. Unify word 'spam' to 'junk'.
  Add bootstrap links.
* Remove Form/FormElement parameter 'feGroup'.
* QFQ Typo3 Updatewizard: Sort output by page and tt-content ordering. Add page uid an title to be reported.
  Inline Edit: add tt_content uid and header as tooltip.
* Update package.json.
* Update to PHPUnit version 9.

Bug Fixes
^^^^^^^^^

* #12468 / Form: Dynamic Update Form.title after save.
* #14636 / 'Clear Me'-cross lighter and 2 px up.
* #15445 / Doc: JS Output Widged added.
* #15497 / Delete link: a) broken for 'table', b) broken with 'r:3'.
* #15527 / Form/subrecord: class 'qfq-table-50' no impact.
* #15654 / Form FE required 'Undefined index Error'.
* #15659 / Multi Form: Datetime Picker not aligned.
* #15691 / T3 V10: Export PDF (PDF Generator) invalid link causes error.
* #15726 / Formelement upload stays required when changed from required to hidden.
* #15729 / Form: text readonly input should show newline as '<br>'.
* #15747 / Typeahead does not work with special character. Added croatian special chars (Ć,ć,Č,č,Đ,đ,Š,š,Ž,ž) to alnumx
  whitelist.
* #15763 / Search/Refactor: a) search for ID, b) message if nothing is found - New highlighting for disabled content.
  Added output for not found in search.
* #15773 / TypeAhead missing check type warning.
* #15813 / Pressing 'Enter' in Form asks: Do you really want to delete the record?
* #15875 / Unecessary qfq-config save.
* #15905 / FE type time undefined index.
* #15913 / Refactor: Footer displayed at the wrong place.
* #15921 / Frontend edit Report Codemirror resizeable.
* #16004 / Template Group Fields broken.
* #16046 / Datepicker not initialized in Template Group.
* #16051 / Dropdown menu: Download file broken with ZIP File.
* #16055 / Dropdown option with render mode 3 format problem.
* #16064 / Dynamic Update Checkbox and Form Store in parameter.
* #16073 / saveButtonActive is disabled after first save.
* #16201 / Character Count Class Quotation Mark.
* #16204 / TypeAhead Prefetch Expects String Array Given.
* #16228 / extraButtonPassword unhide broken.
* #16264 / Multi DB Broken since QFQ V23.2.0.
* #16273 / TinyMCE image upload path incorrect for T3 v10 and upwards.

Version 23.3.1
--------------

Date: 31.03.2023

Bug Fixes
^^^^^^^^^

* #15920 / QFQ variable evaluation broken: wrong variable name

Version 23.3.0
--------------

Date: 30.03.2023

Features
^^^^^^^^

* #15491 / Search refactor redesign and T3 V10 compatiblity: underscore character, counter fixed, page alias and slug handling.
* #15529 / Form/subrecord: Design Titel / Box.
* #15570 / Changed DB handling in class FormAction. Fixed multi-db problem with this. Included change for check of
  existing form editor report.
* #15579 / Ability for searching all possible characters, includes not replaced QFQ variables.
* #15627 / Added character count and maxLength feature for TinyMCE.
* Add various icons to documentation.
* Doc Form.rst: reference 'orderColumn'.
* Doc Report.rst: fix typo, add icons, improved example for tablesorter.
* Add indexes for table FormSubmitLog.
* FormEditor: Show Tablename in pill 'table definition'.
* FormEditor: FE subrecord > show container id below name.

Bug Fixes
^^^^^^^^^

* #14754 / Using double quotes in tableview config caused sql error.
* #15474 / Form save button activated after clicking in TinyMCE editor. Should only activate after change.
* #15483 / Protected folder check fixed. Changed default of wget, preventing errors. Changed handling from protected
  folder check, new once a day.
* #15521 / FormEditor assigns always container, even none is selected. Change handling of form variables from type select,
  radio and checkbox. Expected 0 from client request instead of empty string.
* #15523 / Search/Refactor broken for Multi-DB.
* #15626 / Multi-DB: FormEditor save error.

Version 23.10.1
---------------

Date: 22.10.2023

Notes
^^^^^

Features
^^^^^^^^

* #15098 / Implemented qfqFunction in QFQ variable for usage in forms.
* Doc: Replace many places single back tick by double back tick. Add hint for 'Row size too large'.  Added Enis & Jan
  as Developer. Add hint use mysqldump with one row per record.

Bug Fixes
^^^^^^^^^

* #17003 / inline edit - dark mode has wrong css path.
* #17075 / Fix broken '... AS _restClient'.
* #17091 / upload_Incorrect_integer_value_fileSize.
* #15795 / Upload: download button not shown after pressing save.
* RTD: Fix broken readthedocs rendering.

Version 23.10.0
---------------

Date: 05.10.2023

Features
^^^^^^^^

* #16350 / QFQ Table 'FormSubmiLog': update recordid after insert.
* #16350 / sql.log: reference to FormSubmitLog entry. All SQL statements generated by one HTTP Post (Form Submit) can
  be identified.
* #16350 / Do not log Dirty.
* #16350 / If the T3 instance is behind a proxy, log HTTP_X_REAL_IP instead of REMOTE_ADDR in logfiles.
* #16584 / FormEditor Report: Default without statistics.
* #16589 / Implemented language configuration in backend for tt-content type qfq.
* #16798 / Report inline edit V2. Improved search inside inline edit report. Whole content will be searched. Added
  ability to switch the editor to dark mode.
* Doc: Refactor description of {{random:V}}.
* Doc: Add config option 'protectedFolderCheck'.

Bug Fixes
^^^^^^^^^

* #16573 / Fixed wrong built date and datetime string if default value was given.
* #16574 / Added multiple siteConfigurations compatibility for typo3 v10 and 11.
* #16616 / Fixed typeahead api query response problem if typeahead sql is not used.
* #16664 / Fix multi db user error.
* #16975 / Fix problem if a 'Form Submit' contains more than 64kB data. This can happen easily for 'fabric' elements.

Version 23.6.4
--------------

Date: 26.06.2023

Bug Fixes
^^^^^^^^^

* #16485 / TypeAhead: strpos() string, array given error.
* #16488 / Missing default values break saving records. New custom FE.parameter.defaultValue.
* #16491 / FE Typ Upload - JS failure: document.querySelector() is null.

Version 23.6.3
--------------

Date: 22.06.2023

Bug Fixes
^^^^^^^^^

* #16478 / Rest API: Fixed stream_get_contents failure in PHP8.1 Generic Error.

Version 23.6.2
--------------

Date: 21.06.2023

Bug Fixes
^^^^^^^^^

* #16475 / Spontaneous spaces in HTML emails.
* #16476 / SQL columns Text/Blog with default empty string becomes "''"


Version 23.6.1
--------------

Date: 16.06.2023

Notes
^^^^^

* QFQ is Typo3 V11 compatible

Bug Fixes
^^^^^^^^^

* #16372 / Upload Element 'Undefined index htmlDownloadButton' und 'unknown mode ID'
* #16381 / Form title dynamic update broken.
* #16392 / Reevaluate sanitize class for each store.
* Fix db column enum dropdown 'data truncated'
* DB Update 'alter index' pre check if exists
* FE: Upload - fix undefined index.

Version 23.6.0
--------------

Date: 09.06.2023

Features
^^^^^^^^

* Typo3 V11 compatible
* #9579 / Multiform with Process Row.
* #15770 / httpOrigin: Parameter missing in QFQ Config.
* #16285 / PHP V8 Migration.
* #16364 / T3 >=V10 pageSlug and ForwardPage.
* Add .phpunit.result.cache to .gitignore.
* Add bullet-orange.gif.
* Add index createdFeUserFormId to table FormSubmitLog.
* Doc: Add link to icons. Update use-case self-registration and add info in case of namesake. Unify word 'spam' to 'junk'.
  Add bootstrap links.
* Remove Form/FormElement parameter 'feGroup'.
* QFQ Typo3 Updatewizard: Sort output by page and tt-content ordering. Add page uid an title to be reported.
  Inline Edit: add tt_content uid and header as tooltip.
* Update package.json.
* Update to PHPUnit version 9.

Bug Fixes
^^^^^^^^^

* #12468 / Form: Dynamic Update Form.title after save.
* #14636 / 'Clear Me'-cross lighter and 2 px up.
* #15445 / Doc: JS Output Widged added.
* #15497 / Delete link: a) broken for 'table', b) broken with 'r:3'.
* #15527 / Form/subrecord: class 'qfq-table-50' no impact.
* #15654 / Form FE required 'Undefined index Error'.
* #15659 / Multi Form: Datetime Picker not aligned.
* #15691 / T3 V10: Export PDF (PDF Generator) invalid link causes error.
* #15726 / Formelement upload stays required when changed from required to hidden.
* #15729 / Form: text readonly input should show newline as '<br>'.
* #15747 / Typeahead does not work with special character. Added croatian special chars (Ć,ć,Č,č,Đ,đ,Š,š,Ž,ž) to alnumx
  whitelist.
* #15763 / Search/Refactor: a) search for ID, b) message if nothing is found - New highlighting for disabled content.
  Added output for not found in search.
* #15773 / TypeAhead missing check type warning.
* #15813 / Pressing 'Enter' in Form asks: Do you really want to delete the record?
* #15875 / Unecessary qfq-config save.
* #15905 / FE type time undefined index.
* #15913 / Refactor: Footer displayed at the wrong place.
* #15921 / Frontend edit Report Codemirror resizeable.
* #16004 / Template Group Fields broken.
* #16046 / Datepicker not initialized in Template Group.
* #16051 / Dropdown menu: Download file broken with ZIP File.
* #16055 / Dropdown option with render mode 3 format problem.
* #16064 / Dynamic Update Checkbox and Form Store in parameter.
* #16073 / saveButtonActive is disabled after first save.
* #16201 / Character Count Class Quotation Mark.
* #16204 / TypeAhead Prefetch Expects String Array Given.
* #16228 / extraButtonPassword unhide broken.
* #16264 / Multi DB Broken since QFQ V23.2.0.
* #16273 / TinyMCE image upload path incorrect for T3 v10 and upwards.

Version 23.3.1
--------------

Date: 31.03.2023

Bug Fixes
^^^^^^^^^

* #15920 / QFQ variable evaluation broken: wrong variable name

Version 23.3.0
--------------

Date: 30.03.2023

Features
^^^^^^^^

* #15491 / Search refactor redesign and T3 V10 compatiblity: underscore character, counter fixed, page alias and slug handling.
* #15529 / Form/subrecord: Design Titel / Box.
* #15570 / Changed DB handling in class FormAction. Fixed multi-db problem with this. Included change for check of
  existing form editor report.
* #15579 / Ability for searching all possible characters, includes not replaced QFQ variables.
* #15627 / Added character count and maxLength feature for TinyMCE.
* Add various icons to documentation.
* Doc Form.rst: reference 'orderColumn'.
* Doc Report.rst: fix typo, add icons, improved example for tablesorter.
* Add indexes for table FormSubmitLog.
* FormEditor: Show Tablename in pill 'table definition'.
* FormEditor: FE subrecord > show container id below name.

Bug Fixes
^^^^^^^^^

* #14754 / Using double quotes in tableview config caused sql error.
* #15483 / Protected folder check fixed. Changed default of wget, preventing errors. Changed handling from protected
  folder check, new once a day.
* #15521 / FormEditor assigns always container, even none is selected. Change handling of form variables from type select,
  radio and checkbox. Expected 0 from client request instead of empty string.
* #15523 / Search/Refactor broken for Multi-DB.
* #15626 / Multi-DB: FormEditor save error.

Version 23.2.0
--------------

Date: 05.02.2023

Notes
^^^^^

* QMANR, QMARK: new SQL prepared function to format Swiss Matrikelnumbers and mark found text.
* Refactoring: Search form to support refactoring. Search over tables FormElement,Form,tt_content,pages.
  On T3 page 'form', replace `file=_formEditor` by `file={{file:SU:::_formEditor}}`

Features
^^^^^^^^

* #7650 / For FormElement which are required: `indicateRequired=0|1` show or hide the 'required asterix'.
* #10013 / FormEditor: Several textarea inputs changed to 'editor' incl. SQL syntax highlighting.
* #14995 / Support for refactoring: search in QFQ Form/FormElement and Typo3 pages/tt-content tables.
* QMANR: SQL prepared function to format Swiss Matrikelnumbers

Bug Fixes
^^^^^^^^^

* #7899 / Fe.type=password / retype / required: always complain about missing value - bug has been fixed earlier.
* #8668 / Fix that if a pill becomes disabled, child FE should not respected during save.
* #15168 / Fixed language form save problem for t3 v >= 9.
* #15420 / TypeAhead Prefetch triggers 'form changed' after save.
* #15482 / Added missing password font file.


Version 23.1.1
--------------

Date: 22.01.2023

Notes
^^^^^

* Additional option to dynamically fill the STORE_SYSTEM: fillStoreSystemBySqlRow

Features
^^^^^^^^

* #11980 / check protected folder for access from outside after new qfq installation or update.
* #13566 / Cleanup: Delete config-example.qfq.php
* #15053 / New dynamic QFQ config option: fillStoreSystemBySqlRow.
* Update SANITIZE_ALLOW_EMAIL_MESSAGE to be more generic: 'Invalid email format'
* Update Documentation/Form.rst: Added QFQ Function QMANR(manr),  broken ref to restAuthorization, fixed typos.
* Update copyright year

Bug Fixes
^^^^^^^^^

* #2665 / Fixed not working dynamic update with tinymce and codemirror. Improved frontend edit visualization, elements in window will be resized automatically.
* #11517 / Fixed Bug not showing extraInfoButton on certain FormElements like: TextArea, Select, Checkbox, Radio, Upload, Editor.
* #12066 / Form: Enter as submit forward not working.

Version 23.1.0
--------------

Date: 05.01.2023

Features
^^^^^^^^

* #6250 / Enhance layout subrecord.
* #10003 / Fieldset: stronger visualize group.
* #15036 / FormElement subrecord summary additional row(s).
* #15154 / FormElement input check via regexp: trim whitespace before regexp.

Bug Fixes
^^^^^^^^^

* #14305 / Bug "No form found with this id".
* #14997 / Tablesorter: Filter value shown but no effect after 'Browser Back'
* #15191 / Broken datepicker in template group.
* #15214 / Inline report save history.
* #15229 / Typeahead and qfq-clear-me will be initialized after adding new fields.
* #15230 / Set own button class for dropdown.
* #15314 / FE & BE Email now filled in 'sipForTypo3Vars' and therefore available in API calls.
* #15316 / config.baseUrl might be overwritten if no scheme or multiple urls are given.
* Doc: Fix missing ! in doc: sqlValidate. Fix index undefined htmlAllow. Fix filemtime() Warning.
* Fixed date to datetime convert bug.
* Fixed error coming from typeahead when array is empty.


Version 22.12.1
---------------

Date: 18.12.2022

Notes
^^^^^

* New Button Sizes: btn-tiny, btn-small.
* Caching for on the fly rendered files (PDF, Excel, ZIP).
* Inline Report edit saves history to T3.

Features
^^^^^^^^

* #5715 / Caching for on the fly rendred files (PDF, Excel, ZIP).
* #6250 / Enhance form layout: a) Subrecord, b) Subrecord-Title
* #9927 / Do QFQ Update (config, table defintion) only if there is a BE user logged in the current session.
* #12503 / Detect likely unwanted UPDATE statement with missing WHERE.
* New Button Sizes: btn-tiny, btn-small

Bug Fixes
^^^^^^^^^

* #14305 / Inline Report edit saves history to Typo 3.
* #14506 / Tablesorter: after form close, already defined filter criteria in tablesorter will be applied.
* #15188 / Clean up config to T3 parameter.
* #15193 / No form found with this id error in MultiDB Setup.
* #15206 / Form Save: Internal Error in MultiDB Setup.
* Fix Doc: latest CCS/JS includes.

Version 22.12.0
---------------

Date: 11.12.2022

Notes
^^^^^

* Customable list of FE names that won't be logged to SubmitFormLog. Default is 'password'. List of FE can be customized
  per QFQ installation and/or per form.
* Subrecord:

  * Dynamically computed 'new' button in subrecord: FE.parameter.new. Use regular `... AS _link` syntax.
  * Per row customizeable edit and delete button. Use special column name `_rowEdit` and `_rowDelete`. Use regular `... AS _link` syntax.

* Link/Tablesorter: New link qualifier `|Y:...` which is invisible to the user, but will be respected by tablesorter and filter.
* Link: Text before and/or after a link - `|v:...|V:...`. Example: if fbeg/fend (</td>) is used and there should be more
  than one link inside::

  'p:{{pageAlias:T}}|t:Reload 1|v:<td>Some text before |V: - ' AS '_link|_noWrap',
  'p:{{pageAlias:T}}|t:Reload 2|V:</td>' AS '_link|_noWrap'

* QFQ Config option: forceSmtpSender - force sendmail to use specified sender address.

Features
^^^^^^^^

* #8187 / Subrecord: Dynamically computed subrecord 'new' button. Per row customizeable edit and delete button.
* #11892 / Link/Buttons in HTML table column sort/filterable via tablesorter. Qualifier 'Y:...'.
* #13945 / Link/Buttons: Text before and after link.
* #14320 / TinyMCE: htmlAllow parameter - client and  serverside check. Removes not allowed html tags.
* #15075 / New QFQ config sendmail configuration: forceSmtpSender
* #15111 / Special column name: _saveZip. Save zip on server.
* Documentation-develop/\*.md: CONFIG.md, Reformat
* Fix undefined index in SendMail.php. Show message 'forceSmtpSender is active' in catch all mail. Skip forcing sender
  address in redirectAll mode.


Bug Fixes
^^^^^^^^^

* #8891 / doNotLogColumn: list of FE.name in qfq config and/or form parameter. Value of such column will be logged as
  '*hide in log*'. List default is 'password'.
* #13716 / Mask typed password: Added new (local) font file.
* #14245 / After change via datetimepicker form save button keeps disabled.
* #14303 / Datetime broken with picker
* #14323 / Render mode: Fixed unit test.
* #15013,#15014 / (Again) Excel Import: fix import of regions.
* #15026 / Multiple checkbox dynamic update not working correctly.
* #15027 / Excel Export error when BE user is logged in.
* #15028 / Encryption with special column name broken.
* #15079 / Dynamic Update / Readonly: lost value
* #15090 / typeahead: fixed bug with given empty array.
* #15091 / Hidden FormElement: After save, value in typeahead input will be refreshed with actual data. Usability
  of processReadOnly is given even when dynamic update is active. Jquery serialize caused problems with checkbox setup.

Version 22.11.0
---------------

Date: 27.11.2022

Features
^^^^^^^^

* #15075 / sendmail: special option forceSmtpSender

Bug Fixes
^^^^^^^^^

* #11325 / SQL CALL() - insert() and select() should be supported now.
* #14622 / FormElement `unexpected error` on save.
* #15005 / TypeAhead Dynamic Update Mode Hidden to Required
* #15014 / Excel import broken if multiple regions defined.
* #15048 / STORE_USER broken.

Version 22.10.1
---------------

Date: 24.10.2022

Notes
^^^^^

* Migration of existing Typo3 V9 to V10: Check #12584, #12440

Features
^^^^^^^^

* #12584, #12440  / Typo3 V10 Migration Script Replace Alias Patterns
* #14738 / Datetimepicker: verify various setups
* #14802 / DateTime own class and unit tests
* Add doc to table vertical column via CSS,
* Add qfq-badge-* to doc

Bug Fixes
^^^^^^^^^

* #14618 / Changed logic behind getting first value from array (check for typeahead array was already given but was
  placed after the checkForEncryptedValue function). No exception possible. Function name changed to generic. Added new
  function is_multi_array to check if array is multidimensional and return true or false.
* #14736 / Datetimepicker: FormElement.parameter.dateFormat - required.
* #14755 / Fix for typeahead problem in t3 v9 and higher. Typeahead url will be setup same as save.php. Results to an absolute path.
* #14813 / Report: dbIndex - missing per level definition
* #14844 / Fixed bug of no working dynamic update and empty output in Form Store with Datetimepicker.
* #14857 / String expected, array given.
* #14885 / Fabric zoom problem.



Version 22.10.0
---------------

Date: 04.10.2022

Notes
^^^^^

* TinyMCE Feature "upload images via drag'n'drop" now supported.

Features
^^^^^^^^

* #14813 / Report: dbIndex - missing per level definition
* #12474 / Check BaseConfigURL if it is given and the the last char is '/'
* #12452 / baseUrl: add automatically '/' at end
* #10782 / Tiny MCE: Image Upload & Image drag'n'drop

Bug Fixes
^^^^^^^^^

* #14803 / MultiDB (different DB hosts): broken 'show table definition' in FormEditor -
* #14791 / datetimepicker: undefined index: timeParts[2]
* #14619 / Added note to missed bug fix.
* #12630 / Added code commentary. Reformatted code. getUniqueFileName function changed. New logFileMessages implemented.
* #14463 / llow statusbar (resize button included) to show as default.
* #14455 / Drag and drop triggers record lock.
* #13818 / htmlspecialchars_decode() - expects parameter 1 to be string, array given

Version 22.9.2
--------------

Date: 22.09.2022

Features
^^^^^^^^

* #12630 / dateTimePicker refactored. Mode: 'qfq', 'browser', 'no'. Default: 'qfq'
* Add release notes to update UZH CD to latest version and to remove QFQ JS/CSS includes from custom typoscript template.

Bug Fixes
^^^^^^^^^

* #14619 / dateTimePicher: missing popup on first click, record not dirty

Version 22.9.1
--------------

Date: 18.09.2022

Notes
^^^^^

In 22.5.0 CodeMirror has been implemented for Report editing in the front end. QFQ delivers now the minimized Javascript
version too. Please check http://docs.qfq.io/en/master/Installation.html#setup-css-js for updated JS includes.

T3 extension 'UZH_CD' should be updated to latest version >=22.09.18!

Attention: since 22.09.07 UZH_CD includes all JS/CSS (exception: fabric). Remove all QFQ standard JS/CSS includes
like datetimepicker, codemirror, ... from your typoscript template(s).


Features
^^^^^^^^

* #14616 / Fabric updated to version 5. Annotation of images should now be supported again by modern browsers.
* #10011,#10012 / RedirectAllMailto: optionally dynamically set by current logged in FE or BE User.
* #14718 / Added prepared MySQL statement: QLEFT(), QRIGHT().
* #14635 / Add comment to update /etc/ImageMagick-6/policy.xml in case of 'convert-im6.q16: no images defined'.
* #9052 / Report: CodeMirror with SQL Syntax Highlight in FE - minimized JS versions.
* #T3 QFQ extension config: change 'enter-as-submit' from string to boolean.

Bug Fixes
^^^^^^^^^

* #12452 / T3 QFQ extension setup: Fixes broken string in doc for 'baseUrl'.

Version 22.9.0
--------------

Date: 04.09.2022

Features
^^^^^^^^

* Report.rst: Add description details to tablesorter view saver. Fix Typo QESC_SQUOTE.

Bug Fixes
^^^^^^^^^

* #14660 / Tablesorter View Saver: Broken in Multi-DB Setup
* #14622 / Temp workaround for broken exception - Encrypt/Decrypt detection.

Version 22.8.1
--------------

Date: 28.08.2022

Features
^^^^^^^^

* #9221 / If FE_TYPEAHEAD_LDAP or FE_TYPEAHEAD_SQL is set, skip forcing maxlength to table column definition. Instead
  use custom supplied value.
* Minor update form 'FormElement'
* Refactor $buildElementFunctionName: remove indirect function calls from AbtractBuildForm.php to simplify debugging.
* Extend Support::getColumnSize with further MariaDB column type sizes
* Extension config description baseUrl: updated.

Bug Fixes
^^^^^^^^^

* #14618 / string expected, null given.
* #14611 / MultiDB: broken 'show table definition' in FormEditor. Add pill 'Table Definition' to form FormElement.
* #14590 / emptyTypeAheadRequest'.
* #9281 / QFQ system tables: Allow STRICT_TRANS_TABLES - Default '0' is now set for all INT columns. DB-Update will
  change column definition. TEXT columns still don't have a default: Before MariaDB 10.2.1, BLOB and TEXT columns could
  not be assigned a DEFAULT value. This restriction was lifted in MariaDB 10.2.1.
* Removed empty typeahead request at the beginning

Version 22.8.0
--------------

Date: 22.08.2022

Notes
^^^^^

* Add Enis Nuredini as developer.

Features
^^^^^^^^

* #11262 / stored Procedure: QNBSP - replace space by '&nbsp;'.
* #14270 / FormEditor: add pill 'Table Definition'.
* #14321 / Remember last used pill: on/off via QFQ config or per form.
* #14588 / FormEditor: Remove bell for debugging Form/Session mode.
* FormEditor: Column fe.mode shows now fe.modeSql by default. If empty, fe.mode will be shown.
* QFQ_Encrypt_Decrypt - pre version - not finalized.


Bug Fixes
^^^^^^^^^

* #4018 / Typeahead Attack Detected: Implement renamed keywords _ta_query, _ta_prefetch. Fix broken detection of typeahead mode.
* #13689 / Enter auf Eingabefeld mit ungültigem Wert führt zu blurry Seite.
* #14288 / Upload: removed upload + save > triggers general error.
* #14291 / Doc minor fix: Upload Doku table broken.
* #14292 / Upload: mode=required broken for advanced upload (non primary column) after first save.
* #14302 / Doc minor fix. SanitizeTest.php: minor const replacement.
* #14587 / Use case: Self Registration - table definiton broken in doc: Changed Table Person, to include index and
  auto increment + authExpire default Null.
* SYSTEM_SECURITY_GET_MAX_LENGTH: take care that minimum is 32.
* Update Release.rst and Installation.rst: add missing Codemirror.css|js
* RestClient undefined array element fix.

Version 22.5.0
--------------

Date: 16.05.2022

Notes
^^^^^

Please update your CSS and JS (T3 Main Template > Setup) to include the new DateTime Picker. Forms cannot save if the JS files are missing::

page.includeCSS.file08 = typo3conf/ext/qfq/Resources/Public/Css/bootstrap-datetimepicker.min.css
page.includeCSS.file09 = typo3conf/ext/qfq/Resources/Public/Css/codemirror.css
page.includeJS.file14 = typo3conf/ext/qfq/Resources/Public/JavaScript/moment.min.js
page.includeJS.file15 = typo3conf/ext/qfq/Resources/Public/JavaScript/bootstrap-datetimepicker.min.js
page.includeJS.file16 = typo3conf/ext/qfq/Resources/Public/JavaScript/codemirror.min.js
page.includeJS.file17 = typo3conf/ext/qfq/Resources/Public/JavaScript/code-mirror-mode/sql/sql.min.js

Features
^^^^^^^^

* #9052 / Report frontend editing: CodeMirror with syntax highlight.
* #10096 / Date time picker.
* #10782 / TinyMCE: image upload via drag'n'drop. 'fileUploadPath=fileadmin/...'.
* #13440 / Form: remember last used pill in localstore.
* #13543 / System - base url: multiple base url can be configured.
* #13562 / System - base url: if not set, set base url.
* #13572 / Form Load: better error message on try to load non existent record.
* #13581 / Copy to clipboard: small animation on icon.
* #13657 / Added two new twig report files to system: FormEditorTwig and autoCronTwig.
* #13679 / tablesorter/subrecord: view-saver added.
* #13788 / Date time picker: selectable weekdays.
* Update / doc: datetimepicker requires moment.js - put it in example code before datetimepicker.
* Update / QFQ config note to baseUrl.
* Skip selenium to speed up.
* Move Marc Egger to further contributor.

Bug Fixes
^^^^^^^^^

* #9520 / Button Save inside form.
* #10646 / typeAhead: maxLength by default 512.
* #11134 / Set samesite header for cookies.
* #11325 / SQL CALL is handled seperate. All statements will execute. If only a SELECT statement is used in PROCEDURE then the output will shown, otherwise not.
* #13658 / qfq.json: remove world readable.
* #12483 / qfq.json - Doc: update all references of config.qfq.php to qfq.json
* #12484 / config.render: old Both, new: Single.
* #13084,9724 / Dropdown list on iOS not clickable.
* #13527 / Form Multi: by default set r=0.
* #13677 / FormEditor: Preview disabled if 'new.required' is given.
* #13722 / Detect and fix pdfunite problems "Gen inside xref table too large (bigger than INT_MAX)" via pdf2ps, ps2pdf.
* #13767 / Date time picker: up down button better design if required.
* #13827 / Date time picker should not offer user credentials (like username / password).
* #13751 / Date time picker dynamic update aware.
* #13797 / Checkbox: dynamic update (single and multi) fixed.
* #13818 / typeahead: fix for key/value problem: expects parameter 1 to be string, array given.
* #13842 / Broken SIP: wkhtml and qfqpdf.
* #13933 / Broken SIP: exception handling buggy.
* #13860 / Safari: broken typeahead input.

Version 21.12.0
---------------

Date: 13.12.2021

Notes
^^^^^

* New HTML to PDF renderer: puppeteer. Wrapped and used by qfqpdf. Solves various JS and CSS problems of wkhtml. Install:

  mkdir /opt/qfqpdf; cd /opt/qfqpdf
  curl -L -o qfqpdf https://www.math.uzh.ch/repo/qfqpdf/current/qfqpdf-linux
  chmod a+x qfqpdf

* Module php-curl not needed anymore - has been replaced by php-stream.

Features
^^^^^^^^

* #4812 / Subrecord - check for reserved Typo3 keywords (id, type, L) and throw exception.
* #10145 / Typeahead Min Length = 0 is now possible.
* #10715 / qfqpdf (puppeteer).
* #13113 / Rewrite REST Client as php stream, add contentFile option.
* #13242 / Apply given sanitize class to all defined stores.
* #13330 / Multi Form: Upload.
* #13333 / Option: Switch off attack detect.
* #13496 / TinyMCEfontselect - Fontselect and fontsize are removed from the default configuration.
* #12511 / Typo3 Store: new variables.
* #13526 / QFQ tablesorter: Rename 'private view' to 'personal view' and 'public view' to 'group view'.
* #12541 / Page and link without pageAlias.

Bug Fixes
^^^^^^^^^

* #3446 / Unknown permission mode: 'logged_in'
* #9268 / SELECT with outer brackets not recognized as SELECT
* #13030 / Max length cuts - line endings \r\n has been counted as two chars. During input they are counted as 1 and
  therefore on data load the string has been cutted.
* #13139 / Tablesorter: some elements are in front of a sticky title row
* #13507 / QFQ function should work without 'sql='
* #13525 / makefile adjusted for multiple users


Version 21.6.0
--------------

Date: 29.06.2021

Notes
^^^^^

* For full image HEIC/HEIF support, please install package ``libheif-examples``.

Features
^^^^^^^^

* #8945 / Best practice :ref:`self-registration`
* #12551 / Add QFQ bootstrap diagram
* #12615 / Implements silent HEIC/HEIF conversion to png.
* #12636 / New FormElement.encoding = 'single tick'
* Form.rst: Update doc of Multi-Form

Bug Fixes
^^^^^^^^^

* #12701 / Bug 'spaces in mails'. Replaces '<br>' by '<br>\r\n'
* #9371 / MultiSQL now accepts `_id` too.
* #12674 / QFQ Function subheader: skip deleted
* Remove keySemId and keySemIdUser - outdated / deprecated since 0.19.0 (pre 09.2017)
* Download.php: Fix broken variable $this->$downloadDebugLog.

Version 21.5.1
--------------

Date: 17.05.2021

Notes
^^^^^

* The `log` directory was moved into the `qfqProject` directory in version 21.2.0 for new installations. But if the
  directory `fileadmin/protected/log` already exists then QFQ keeps storing logs there. This was added to release notes
  of 21.2.0 in hindsight.

Features
^^^^^^^^

* #12183 / Download table as csv
* #12159 / Make url paths absolute (relative to baseUrl)

Bug Fixes
^^^^^^^^^

* #12516 / Password Hashing Exception

Version 21.5.0
--------------

Date: 02.05.2021

Features
^^^^^^^^

* CodingGuideline.rst: add Form Best practice

Bug Fixes
^^^^^^^^^

* #10505 / Drag'n'Drop broken on Multi DB Instance - all checks done
* #10754 / Clean up stale requirements.txt
* #11769 / Missing description table in Form.rst
* #12352 / Form As Json: copy via JSON in FormEditor broken.
* #12398 / Fix check required for uploads
* #12475 / During QFQ update take care that all system tables exist.
* #12479 / Remove unwrap('p-tag') for TinyMCE - currently, this breaks regular consecutive <p> tags


Version 21.4.0
--------------

Date: 11.04.2021

Notes
^^^^^

* The new introduced 'Form As File' Feature in QFQ Version  21.3.* has been disabled. Instead every form can be imported
  and exported as JSON. The automatic import/export was not stable and a manual process promises less problems.

Features
^^^^^^^^

* #12015 / useAdvancedFormEditor: disable form sync using feature flag
* #12346 / Add new formJson.json to system forms
* #12345 / Update documentation of special column formJson
* REST.rst: small reformat.
* Add Test copyToClipboard
* HTTP Header: Add 'X-Api-Key'. Fix problem for token without argument name. Update doc.
* Add margin to qfq-badge
* Fix undefined clearMe
* File formEditor.sql only contains QFQ system table definitions anymore.

Bug Fixes
^^^^^^^^^

* #12341 / FormSubmitLog also saves FormName (not only FormId)
* #11265 / Dropdown Menu: wrap with <p> breaks dropdown
* #12268 / Enhanced FormSubmitLog Page Sample Code
* gitlab-ci.yml: ``mkdir -p ...`` suppresses warning if directory already exist. Add more descriptive message.
* docker/run_qfq_docker.sh: 'source' eines files, das nicht existiert, gibt immer einen Fehler auf stdout.
* Fix problem: /var/www/html/my/typo3conf/ext/qfq/Classes/Core/Form/FormAction.php / Line: 394 / Undefined index: saveFormJson
* extension/Classes/Core/Helper/SessionCookie.php: Index 'host' was empty


Version 21.3.2
--------------

Date: 22.03.2021

Features
^^^^^^^^

* #9528 / Implement option 'clearMe' to show a small X in input & textaera fields per FormElement, per Form or global.

Bug Fixes
^^^^^^^^^

* #12015 / use Advanced Form Editor - problem with uppercase f in Form for form name.

Version 21.3.1
--------------

Date: 21.03.2021

Notes
^^^^^

* Custom QFQ Function:

  * Call QFQ function from inside a report, at any place, as often as needed. The QFQ function code will be parsed only
    one time. Variables will be replaced in time.
  * Based on QFQ function: a new download source can be specified : source:<function name>

Features
^^^^^^^^

* #11998 / Custom QFQ-Function
* #12015 / Use Advanced FormEditor

Bug Fixes
^^^^^^^^^

* Bug with empty Fabric string fixed

Version 21.3.0
--------------

Date: 07.03.2021

Notes
^^^^^

* To enforce usage of the new form-list we throw an exception on QFQ update if the line "file=_formEditor" is not
  present in any tt-content bodytext. :ref:`form-editor`.
* Persistent download links, combined with SQL based access check. :ref:`download`.
* Check new file based definition of forms `form-as-file` - this makes it easier to work in team with GIT based files.

Features
^^^^^^^^

* #12085 / Persistent download links.
* #12022 / New Escape class HtmlSpecialChar 'h'
* #11957 / Get European Timezone {{start:R::t}}
* #11926 / Update Form As File Documentation
* #12015 / Add formEditor as a system QFQ report and enforce formEditor existence
* #11926 / Use alternate form and report directories for phpunit (form_phpunit)
* #11953 / Logger.php: replace makePathAbsolute with Path functions
* #11931 / Report minimal required PHP version and stop, if PHP version is too low.
* #9213 / Add note for tablesorter.
* Add EV tips for tablesorter as a table in doc

Bug Fixes
^^^^^^^^^

* #12016 / Run Typo3 autoloader before password hashing if API request
* #5444 / Typeahead FE value is now prefetched after save.

Version 21.2.0
--------------

Date: 01.02.2021

Notes
^^^^^

* The `log` directory was moved into the `qfqProject` directory in version 21.2.0 for new installations. But if the
  directory `fileadmin/protected/log` already exists then QFQ keeps storing logs there. This was added to release notes
  of 21.2.0 in hindsight.

Features
^^^^^^^^

* #10286 / Download Links: Glyphicon selbst wählen/ausblenden
* #11878 / Purge extension option config.documentation
* #6793 / Source files for ZIP archives might now specified with a path/filename how they are called inside the ZIP.
* log directory was moved into qfqProject directory

Bug Fixes
^^^^^^^^^

* #11925 / downloadButton Parameter 'd:Filename'
* #9355 / Increase column width header & attach
* #11865 / Form Editor exception when edited after saved
* #11798 / Use T3 password hashing API instead of hack
* #11750 / Checkbox does not work together correctly with required fields
* #11666 / PHP 7.4: Trying to access array offset on value of type int
* #11245 / If forwardMode column is set to '', set forwardMode to 'auto'

Version 20.11.0
---------------

Date: 12.11.2020

Features
^^^^^^^^

* #7156 / Throw exception when a report level is defined twice.
* #11335 / Sanitize Alnumx: allow 'ß'
* #11325 / Allow SQL CALL(). Unsupported: Multiple (like query_multi()) - only the first one will be returned.
* #11269 / REST Post might return a customized answer.
* #11512 / Twig: add access to Var store
* #11118 / Uniqueness is now guaranteed in {{random:V}}
* #11513 / Add special column AS _script
* #11509 / PDF split fails for broken PDF
* #10979 / Ajax Calls an API - dataReport

Bug Fixes
^^^^^^^^^

* #11430 / phpSpreadsheet throws exception that '' is not numeric
* #10257 / Delete AS _link only worked with certain parameter order.
* #11511 / Tablesorter: fix encoding error, sanitize base64 encoding
* #11146 / Sendmail dynamic PDF broken
* #10554 / Fix extrabuttonlock also for select, radio and checkbox
* #11245 / If forwardMode column is set to '', set forwardMode to 'auto'

Version 20.9.0
--------------

Date: 06.09.2020

Notes
^^^^^

* New SIP protected AJAX calls.
* Report now fires calls to websocket (remote hosts).
* Report now fires REST calls to remote hosts.

Features
^^^^^^^^

* #10979 / Report: do SIP protected AJAX calls to typo3conf/ext/qfq/Classes/Api/dataReport.php.
* #11076 / Report: trigger call to websockets.
* #11119 / Report: REST Client calls - incl. processing of answer.
* Add CodingGuideline.rst.

Bug Fixes
^^^^^^^^^

* #10919 / AutoCron: Fix missing FillStoreSystemBySql.
* #11075 / Form: Missing SQL error message in FE Action Elements.
* #11039 / Fix CSS for Checkbox.

Version 20.6.2
--------------

Date: 25.06.2020

Bug Fixes
^^^^^^^^^

* #10641 / TypeAheadTag: Fehler beim gleichzeitigen anlegen mehrerer neuer Tags
* #10794 / Documentation: Crontab entry more clearly


Version 20.6.1
--------------

Date: 24.06.2020

Features
^^^^^^^^

* #10778 / Upload ZIP and unpack


Version 20.6.0
--------------

Date: 14.06.2020

Notes
^^^^^

* Add note in Installation.rst to start Apache with Locale en_US.UTF-8. This helps to support Umlaut and other characters
  in filenames and wkhtml commandline options (like header/footer).
* Migrate documentation from T3 to ReadTheDocs.io - looks older but 'search' is much more better. New: chapters separated
  in individual files.
* For the image to PDF feature, installation of `img2pdf` is required (please check :ref:`preparation`).

Features
^^^^^^^^

* #10751 / Allow images to be concatenated for PDF download.
* Fontawesome updated 5.13.
* Extend FE.label size to 1023.
* Local documentation rendering directly via Sphinx.
* Manual: Search is working, table width not truncated anymore, PDF & epub export, redirect qfq.io/doc to docs.qfq.io.
* Update copyright notice.

Bug Fixes
^^^^^^^^^

* #10507 / FormElement.type: 'annotate' is defined two times in Enum
* #10705 / New function 'HelperFile::joinPathFilename($pre, $post)'. Joins only if $post is without leading slash.


Version 20.4.1
--------------

Date: 30.04.2020

Notes
^^^^^

* Developer: update local npm/font awesome packages via ``make bootstrap``.

Features
^^^^^^^^

* #10433 / Update to Font Awesome 5.0
* #10379 / Stored Procedure: SLUGIFY()
* Manual.rst: Example Report 'render'. Update several places to fit latest ReST layout rules.

Version 20.4.0
--------------

Date: 05.04.2020

Notes
^^^^^

* New Feature: :ref:`type-ahead-tag` - extend regular input with multiple values via typeAhead. _
* MySQL StoredProcedure:

  * strip_tags() - Simple strip html tags.
  * QCC() - Escape colon / coma. Useful for QFQ link arguments like 'text' or 'tooltip'.

Features
^^^^^^^^

* #9686 / Download: sanitize output filename.
* #10358 / Configure path/environment via QFQ config: qpdf, gs, pdfunite.
* #9517, #10145, #10177, #10117 / typeAheadTag.
* #10152 / QCC() - Stored Procedure to escape colon / coma.
* FabricJS: replaced glyphicons with font awesome.
* Rename config.qfq.example.php config-example.qfq.php.

Bug Fixes
^^^^^^^^^

* #6798 / Close didn't worked with r=0.
* #10199 / Form.forwardMode: missing mode 'Close' / 'Auto'.
* #10173 / Dynamic Update: Readonly element can't be activated via dynamic update.
* Fix broken default value for Form.forwardMode.
* Fix problem with reporting broken TG-FormElements.
* Add error message if primary table does not exist.

Version 20.2.0
--------------

Date: 02.02.2020

Notes
^^^^^

* Add new keyword 'render' in tt-content Report and QFQ config. 'render' will control if a) only Form OR Report will be
  rendered  (render=single) or b) as previous Form AND Report together (render=both).

  * Advantage: with 'render=single' no more SELECT ... FROM (SELECT '') AS fake WHERE '{{form:SE}}'=''.

  * Attention: NEW default behaviour in new QFQ installations - render=single. Behaviour in old installations is unchanged.

* tt-content records with 'render = api' can stay on the same page as the link to get the content (e.g. Excel Export).
* Change default doc page to qfq.io/doc.
* Add new specialColumnName: '_noWrap' - skips wrapping of fbeg,fsep,fend.
* First version of 'FullCalendar.io' - new SpecialColumnName will follow in the future.

Features
^^^^^^^^

* #9929 / New keyword '_noWrap' for column names (alias) - skips wrapping of fbeg/fskip/fend.
* #9905 / Keyword 'render' in Report. Final implementation. Doc updated.
* #9959 / Update QFQ Config on the fly.
* #9990 / Describe order of FormElement processing - :ref:`form-process-order`.
* #8658 / FullCalendar.io V3 implemented.
* #9535 / VerticalText new implementation.
* Manual.rst: Add list of icons. Enhance sendmail doc.
* Change color of qfq-info-* from blue to light-blue. Add qfq-primary, qfq-danger.

Bug Fixes
^^^^^^^^^

* #10010 / FE.type=sendmail will now be fired together with fe.type=after* (not after).
* #5869 / Table names not properly escaped.
* #9638 / TextArea: Autosize - broken when using clipboard,
* Fixed problem with border showing when qfq-color-white is set.
* Fix selenium tests, remove chromedriver from npm.
* Log problem that crashes qfq when calendar dependencies are missing.
* Fixed gruntfile problem.


Version 20.1.1
--------------

Date: 13.01.2020

Bug Fixes
^^^^^^^^^

* #7705 / Fix problem with wrong value after save and form update.
* #8587 / A form triggers a save only, if there are real table columns.

Version 20.1.0
--------------

Date: 09.01.2020

Notes
^^^^^

* Deprecated: Form.parameter.mode. Use Form.parameter.formModeGlobal

Features
^^^^^^^^

* #9805 / Form.parameter.activateFirstRequiredTab.
* #9858 / Form.parameter: replace 'mode' by 'formModeGlobal'
* #9860 / SQL function QMORE(): change text '...' to '[...]'.
* Update Developer doc for record locking.
* Mockup for error handling.

Bug Fixes
^^^^^^^^^

* #7925 / Error in split PDF file during upload. Fix the cwd error in Logger.
* #9789 / Record lock release to early on 'leave page'. QfqJS: Moved release lock to before unload.
* #9861 / Fix problem with broken sql.log filename.
* #8851 / Revert implementation: LogMode 'modify' vs. 'modifyAll'.
* #9859 / Database Update: check for 'Update specialColumnName needed' breaks new QFQ install.
* #9813 / During QFQ database update, skip errors like 'Error 1060 - Duplicate Column'.
* Manual.rst: Fix various broken table layouts.

Version 19.12.0
---------------

Date: 17.12.2019

Notes
^^^^^

* Switch the whole homepage to readonly: FormModeGlobal and STORE_USER

Features
^^^^^^^^

* TinyMCE: grey out controls when readonly.
* Mockup:

  * Update files to get CSS & JS files from their own directory structure (not an installed QFQ extension).
  * Add fontawesome, tablesorter to mockup 'formCheckbox.html'.

* CI: Download selenium logs (only failed) under artifacts.
* Dev: .gitignore: exclude some docker & selenium.
* Merge Selenium Python Checks into Master.
* #9686 / html decode and sanitize an export filename to become the 'save as'-filename.
* #9666 / min-width for extraButtonInfo.
* FormEditor: optimize minWidth for 'rowLabelInputNote' field.

Bug Fixes
^^^^^^^^^

* #9720 / Checkbox dynamic update varoious settings:

  * Multi Plain Vertical & Horizontal, Checkbox Multi BS.
  * Fixed that label of 'checkbox' are bold and label of 'checkbox-inline' are normal.

* #7974 / TinyMCE: ReadOnly.
* #9424 / modeSql: skip if it starts with '#'.
* #9674 / Select Required Dynamic Update.
* #9678 / textarea now trigger DynamicUpdate.
* #9679 / FormModeGlobal: add STORE_USER - system wide readonly.
* #9690 / Select Required.
* #9691 / Checkbox: dynamic update > readonly. HTML ID for checkbox elements. Dynamic update switch 'readonly' for 'checkbox plain multi' and 'radio plain multi'.
* #9692 / Keyboard Select Checkbox.
* #9720 / Checkbox: Various setups with dynamic update.
* #9733 / Identify different tabs. Record lock for same tab will always be granted.
* #9734 / Fix 'dirty lock release' - leaving a dirty form without closing, leaves a stale lock record. Added a releaselock() before window.unload. Dirty remove on goBack.
* #9735 / File Delete: no dirty trigger.

* Download / PDF merge: skip leading errors, interpret only 'Could not merge encrypted files'.
* DragAndDrop broken: after refactoring Support.php, the dragAndDrop was broken - missed init of '$store'.

Version 19.11.3
---------------

Date: 29.11.2019

Features
^^^^^^^^

* #8886 / Check pattern: after focus lost.
* #9655 / Checkboxes and radios now defined with a min-width in horizontal plain mode.
* #9617 / formModeGlobal:

  * Two modes of 'formModeGlobal' available: 'requiredOff' and 'requiredOffButMark'.
  * 'requiredOffButMark':

    * Renamed temporary 'skipRequiredCheck' to 'requiredOffButMark'.
    * Keep required marks after save.
    * Stop hiding helpblocks per default, set with class qfq-only-active-error.

* Radio: new class 'qfq-disabled' if readonly is set. Softer blue. Mark disabled - changed hover. Text in darker orange.
  Simple-error renamed to qfq-notify - removed box around error.
* New default class for 'form' tag: 'qfq-notify'.
* Manual.rst: Add info for 'letter-no-break'.
* Add validator.js to list of used packages.

Bug Fixes
^^^^^^^^^

* #9670 / If `qpdf` fails to decrypt a PDF, try `gs`.
* #3995 / Implemented partly: CheckBox and Radio can now be locked.
* #8091 / Checkbox required:

  * If radio or checkbox is required and empty on submit, form save brings the element to front.
  * Fix radio plain vertical.
  * Fix label2 not to be bold.
  * Checkbox Plain Vertical: forces 'font-weight: 400;'.
  * Updated colors for checkboxes/radios.

* #9638 / Textarea Sizing: Now also listens for paste.
* #7891 / Added missing 'type="button"' to button element.
* Remove ' style="font-size: 1em;"' in extraButton - this causes extra space between multiple extraButton inline in one input element.

Version 19.11.2
---------------

Date: 25.11.2019

Notes
^^^^^

* Enhance formModeGlobal=requiredOff/-ButMark (temporarily skipRequiredCheck): fill's '{{allRequiredGiven:V}}' before
  save to 1 (all given) else 0.
* Offers user to save form, even if not all required data are given and offers application logic to check easily if all
  required fields has been filled.

Features
^^^^^^^^

* #9526 / Mark required fields more visible.
* #9617 / Improve 'formModeGlobal=requiredOff'.

  * Feature `Form.formModeGlobal` implemented - STORE_SIP overwrites form definition.
  * New STORE_VAR variable 'allRequiredGiven'. Becomes '1' if all required fields are given, else 0.

* Add param 'L' & 'type' automatically to form save.
* Manual.rst: Procedure to find an irreproducible error.
* Change definition of QFQ system tables for 'modified' and 'created'. Use DATETIME instead of TIMESTAMP.

Bug Fixes
^^^^^^^^^

* #7639 / subrecord drag n drop:

  * `orderInterval` has not been respected.
  * Update Manual.rst.
  * Fake STORE_SIP so it can be used during processing sql1.
  * The record, currently loaded into form, is available via STORE_RECORD.
  * Check for id/_id and ord/_ord.
  * Throw meaningful exception if missing 'id' or 'ord'.

* Fixes bug that no mime_type_content is called if there is on file.
* Fix broken regex101 url.

Version 19.11.1
---------------

Date: 11.11.2019

Bug Fixes
^^^^^^^^^

* #9532 / 'Advanced Upload' broken -  slaveId/sqlUpdate/... have been processed two times, after multiform code changes.

Version 19.11.0
---------------

Date: 08.11.2019

Notes
^^^^^

New CSS Class:

* 'qfq-sticky' - to make an element sticky.

Update/new stored procedures:

* QMORE: change symbols from '..more' / '..less' to '...' / '<<'.
* QIFEMPTY: add empty detection for a) date, b) datetime, c) '0'.
* QDATE_FORMAT(timestamp). Return text in 'dd.mm.yyyy hh:mm' format or '-' if timestamp is empty.

Features
^^^^^^^^

* #9521 / Textarea auto-grow. Update Manual.rst. New class qfq-auto-grow for textareas.
* Update FormEditor to 80,1,350.
* Alerts a) changed overlay to blur and white, b) optimized to better reading for developer - ':hover' inside of error messages: black text and blue highlight.
* Add new CSS class 'qfq-sticky-element' - to stick individual elements.

Bug Fixes
^^^^^^^^^

* #9354 / Missing border around form if there are no pills / no title
* #9053 / Using the tablesorter filter in form triggers dirty detection for save. Set qfq-skip-dirty on all tablesorter-filter.
* #2720 / Formelement 'radio' - required failed.
* #9512 / PDF merge fails on encrypted PDFs. Try to use qpdf to decrypt.
* #6232 / Missing required: Pill/Input not 'bring to front' - Textarea, Editor, Radio.
* #9300 / Fix Name restMethod-FormElement.
* Add '@' to various PHP/IO function to suppress generic OS exception - now QFQ will report customized messages.
* FormEditor / error message: fix missing formId in STORE_SIP on calling FE directly from error message.
* Manual.rst: Tip for wkhtml added. Add description for 'fileButtonText'. Adjustments for 'slaveId' concept.


Version 19.10.0
---------------

Date: 17.10.2019

Notes
^^^^^

* Background color of popups & error messages changed.
* slaveId/sqlInsert/sqlUpdate/sqlDelete are now available for all FormElements.
* New MySQL stored procedure strip_tags() function.

Features
^^^^^^^^

* #5695 / First implementation of multiform.
* #7495 / Removed dirty flag when 'enable-save-button' is set.
* Add MySQL strip_tags() function.

Bug Fixes
^^^^^^^^^

* #9329 / Fabric annotations. Fixed Scaling Problem on Static Canvas instances.
* #9298 / Fix timeout of file_get_contents. Extend timeout for downloadPage to 10min.
* #9274 / PHP 7.3 reports: switch statement with 'continue 2'.
* #9269 / fillStoreForm now fired two times in API calls. The first time during loadFormDefinition and the second time after the STORE_TYPO3 has been faked via SIP.
* Manual.rst: Add '!' to fillStoreVar Query
* Increase z-index in CSS 'dropdown-menu' class to 1060. The 'tablesorter > columnselector' will be hidden with 'qfq-sticky' by values < 1000.
* Reduce z-index in CSS 'qfq-sticky' class from 9999 to 1000. The 'tablesorter > columnselector' will be hidden with values > 1060.

Version 19.9.1
--------------

Date: 21.09.2019

Notes
^^^^^

* Use the CSS class 'qfq-sticky' in `<thead>` to make a table header 'sticky' - on long pages such tables headers are always
  visible.
* The page with the list of all forms: a new best practice report includes useful statistics :ref:`form-editor` - best
  is to replace the old code.
* The DB User needs privileges to the Typo3 database of at least table `tt_content` with SELECT.

Features
^^^^^^^^

* #9203 / Pin the header of table (CCS 'sticky'), to make it always visible even if the page scrolls down.
* #9172 / AutoCron: new colum 'autoGenerated', 'xId'
* #9089 / Move Stored Procedure to SECURITY=INVOKER
* Manual.rst: Best practice :ref:`form-editor`
* Reduce BS legend.font-size from 21 to 17.
* Change doc of tablesorter to use 'sorter-false' as class.

Bug Fixes
^^^^^^^^^

* #9074 / QFQ query with nested QFQ query failed, if the outer QFQ query is a multi column query ( ='{{!' ). Fixed.

Version 19.9.0
--------------

Date: 09.09.2019

Notes
^^^^^

* Size of input elements now might be specified dynamically (with min and max height).
* Twig converts JSON objects to an array.

Features
^^^^^^^^

* Report.php: Twig, convert JSON object into associative array.
* Test SQL stored procedure in Form report.
* qfq-bs.css.less: a) reduce 'qfq-note' padding-top from 7 to 2 px, b) reduce 'legend' margin-bottom from 25 to 0.
* Debug output sendmail redirect all: addresses now always space delimited.
* FormEditor:

  * Remove FormElement 'tabindex'.
  * Change FormElement.class from 'Select' to 'Radio'.

* Manual.rst:

  * Clean syntax highlight.
  * Update realtime log file QFQ code.
  * Add 'Best Practice' code to show QFQ log files in realtime. Reformat some content.

* #7849 / New option 'fileTrash' and 'fileTrashText'.
* #7682 / 'Input textarea auto height'
* #4434 / Special column names now have to start with underscore. Earlier it was recognised even if there was no underscore.

Bug Fixes
^^^^^^^^^

* #7860 / Special column name 'mailto' no handles text correctly if multi byte encoded. Fixed.
* #7849 / Missed filename after the file chooser selected an file. Fixed.
* #1201 / FE.parameter option 'tabindex' has never been implemented. Removed.
  According https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex only the values '0' and '-1' are good.
* Add table html-id to subrecords, created by QFQ. Needed for tablesorter.
* Add TinyMCE to list of 'Software distributed together with QFQ'

Version 19.8.0
--------------

Date: 28.08.2019

Features
^^^^^^^^

* Manual.rst:

  * Update tablesorter ``class="sorter-false"``.
  * Add note to include font-awesome.
  * QFQ_LOG documentation.
  * Enhance wkhtml debug info.
  * Add link to https://mariadb.com/kb/en/library/stored-routine-privileges/

* Increase Length of column Form.forwardPage limit from 255 to 511.
* Index.rst: Add Rafi, Elias, Nicola as further contributors.
* Add SQL stored procedure QIFEMPTY().
* #8806 / Add SQL stored procedure QNL2BR().
* #8851 / Add sqlLogMode modifyAll and restrict sqlLogMode modify.
* #8859 / FormEditor: Filter + ColumnSelector.
* #8856 / AutoCron: SQL logMode should be defined separately.

Bug Fixes
^^^^^^^^^

* #8933 / Broken record lock: lock extend does not work - fixed.
* #8846 / FormEditor: subrecord of FormElement might be wider than Form.
* #8853 / fe.class=action: container should be hidden - hidden is wrong - FE-action might be assigned to templateGrou
  Container. New solution: if FE.class='action' only templateGroups are in the 'container' list.
* #6656 / Merge DragnDrop into 'master'
* Update compatibility: API change in T3 makes QFQ incompatible with Typo3 >=9.4. Change compatibility back to <9.3.
  Problem: sys_language_uid
* Update QFQ from earlier/equal version than 0.19.2 fails to create table 'Cron'.


Version 19.7.1
--------------

Date: 17.07.2019

Notes
^^^^^

* TWIG integration

Features
^^^^^^^^

* TWIG:

  * Add Twig Extension to parse QFQ Links
  * Pass assoc-array of query result as context
  * allow to pass template as string

* Database.php: extend function sql error message
* #8179 / extraButtonLock and extraButtonPassword might be specified without a value or with 0 or with 1.

Bug Fixes
^^^^^^^^^

* Database.php: fix CodeException
* Fabric: Readonly now displays annotations again.

Version 19.7.0
--------------

Date: 02.07.2019

Notes
^^^^^

* Settings for tablesorter can now be saved.
* Selenium docker based is integrated now.

Features
^^^^^^^^

* #7284 / tablesorter save sort order
* #8592 / Fixed button display, added Alert instead of disabled button.
* #8204 / position required mark
* Excel.php: Delete superficial autoloader require
* Cleanup: hashPassword.php moved from Api to External.
* Cleanup unwanted git tracked files: Documentation-develop/jsdoc|...
* Selenium + Docker
* Update .gitignore - documentation directory moved several days ago.

Bug Fixes
^^^^^^^^^

* #6917 / Fix Monitor does not work.
* #8098 / Fix Language specific retype label or note has not been set correctly.
* Fix undefined index and potential undefined index.
* Fix Autoloader for External/AutoCron.php.
* Small fixes Manual.rst.
* Fix broken Manual: unwanted '<' after '{{!' lead to fully broken Manual.
* Fix path Source/api to Source/Api.

Version 19.6.2
--------------

Date: 21.06.2019

Notes
^^^^^

The Rest API path changed:

* old: typo3conf/ext/qfq/Source/api/rest.php
* new: typo3conf/ext/qfq/Classes/Api/rest.php

QFQ is now PSR4 compliant.

Features
^^^^^^^^

* #8520 / Switch to psr4 composer.
* #8272 / Disable 'save' button as long as an upload is running.
* #7529 / Guarantee Sip Action plays only once.
* #8577 / Documentation enhanced for behaviour of SELECT '_test'.

Bug Fixes
^^^^^^^^^

* #7903 / Delete with 'table' as parameter doesn't work anymore.
* #8571 / Primary Key: report error if lower/uppercase don't match - in general if given primary key is not found in table definition.
* #8465 / Var substitute: '{{name:R:::0}}' default not used if it is '0'.

Version 19.6.1
--------------

Date: 16.06.2019

Notes
^^^^^

* 'pdftk' is no longer used.
* Please install 'poppler-utils' - the command 'pdfunite' is the replacement of 'pdftk'.

Features
^^^^^^^^

* #8558 / Split error message in toUser and toDeveloper. Add error code.
* #8562 / Replace pdftk by pdfunite. 'pdftk' is outdated. It's hard to install on Ubuntu 18. It fails for recent PDFs.
  'pdfunite' is based on poppler. URLs are preserved. Orientation and size are preserved.

Bug Fixes
^^^^^^^^^

* Fix uniqIdQfq() - returned always badcaffeee1234
* Refactor function.sql to function.sql. Implement constants for Version numbers.
* Update NewDoc.md

Version 19.6.0
--------------

Date: 12.06.2019

Notes
^^^^^

* Add Marc Egger as Developer

Bug Fixes
^^^^^^^^^

* #8523 / htmlid: remove spaces (war #8460). Replace uniqid() by uniqIdQfq() which delivers a constant id in case of running unit tests
* #8430 / QFQ Doc mising.

  * Index.rst: customize URL for 'report a problem'.
  * Sitemap.rst: new created to be included by Index.rst.
  * Reformat Index.rst according https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual.git.
  * Move doc to docker generation. Update ReST Syntax URLs.
  * Remove 'extension/Documentation.outdated'.
  * Update .gitignore to not commit rendered doc.

* Manual.rst: Excel Export / PDF export replace 'uid:<int>' with 'uid:<tt-content record id>'
* Update SQL functions to hanlde errors more efficient.


Version 19.5.1
--------------

Date: 22.05.2019

Notes
^^^^^

* New dropdown menu, fully dynamic via '... AS _link' incl. SIP generation.
* New SQL stored procedure for use directly in SQL queries:

  * QMORE(text, length) - limits a text to 'length' characters, click on 'more' shows complete text.
  * QBAR(text) - escapes the character '|'. That one is heavily used in format strings for '... AS _link'.

Features
^^^^^^^^

* #8391 / Additional line 'Website: ...' in sendmail redirect all.
* #8270 / SQL Stored procedure QMORE(text,length): Limit long text to 'length' characters. Show button 'more'.
* #8270 / SQL Stored procedure QBAR(text): Escape the character '|'.
* #8348 / Dynamic Dropdown menu via '... AS _link'

Bug Fixes
^^^^^^^^^

* #8116 / Error-Dialog - Button for FE element is broken. First problem: replacing '\n' with '<br>' failed for FE link. Second problem: FormEditor fixed in case missing {{formId:S}}.
* #8315 / FE Datetime: 'd.m.yy hh:mm' thows an error
* #8278: Too long GET var crashes QFQ - Bug 1 - check GET Vars but try to report problem from POST var. Bug 2: writing log message crashed due to not initialized QFQ-config.
* Html2Pdf.php: Change logfile from sql.log to qfq.log
* formEditor.sql: Add specific pattern message for Form.name allowed characters
* Config.php: more precise error message

Version 19.5.0
--------------

Date: 03.05.2019

Notes
^^^^^

* QFQ is now T3 V9.5 compatible.
* Minimal required version increased to T3 V7.
* QFQ description in ext_emconf.php updated > will be displayed on TER.

Features
^^^^^^^^

* #5103 / Upload any file type: ``accept=*`` or ``accept=*/*`` or ``accept=*.*``.
* Manual.rst: update all links to bootstrap to fixed version 3.4: https://getbootstrap.com/docs/3.4/...

Bug Fixes
^^^^^^^^^

* Manual.rst: Update BS glyphicon URL
* Manual.rst: replace '{{feUser:Y}}' by '{{feUser:T}}'
* #8109 / Change email pattern and disable sanitize for FORM_UPDATE
* #8149 / Excel Export cutoff at Column AA
* #8113 / Fehler in Dokumentation für Default value


Version 19.3.2
--------------

Date: 18.03.2019

Notes
^^^^^

* New Escape/Action Class:

  * 'X': Throw an exception if a variable isn't found. Custom message can be defined.
  * 'S': Stop replacing nested variables.

* Form forward:

  * 'url-sip': Call a target page with SIP encoded parameter
  * 'url-sip-skip-history': Call a target page with SIP encoded parameter, do not add current page to browser history.

* Report: new token 'fskipwrap' - comma separated list of column indexes. Suche columns won't be rendered with 'fbeg, fend'.

* Upload max file size:

  * Default changed:

    * old: 10MB
    * new: System limit

  * Definition via a) system configuration, b) qfq setup, c) per Form, d) per FormElement

Features
^^^^^^^^

* #8043 / New Escape/Action Class "Exception" if variable is not found.
* #8012 / New Escape/Action Class  "Stop replace" of nested variables.
* #8067 / url-sip / url-sip-skip-history - Form forwarding can now be used with SIP encoded parameter.
* #8072 / New token for Report: fskipwrap - wrapping of fields can be disabled per column.
* #8041 / Upload: maxFileSize based on system maximum, qfq config, form config, formElement config
* Manual.rst / Add more detailed description for '... AS _exec'. Add notes for 'fileSplit'.

Bug Fixes
^^^^^^^^^
* #8035 / Missing sql.log: throws an error - fixed.
* #8077 / PDF/fileSplit: uploaded PDF file with missing extension '.pdf' have not been recognized as PDF files - fixed.
* #8076 / splitPdf: using 'fileSplit=jpeg' and uploading a PDF with only one page, results in filename 'split.jpg'
  (missing index '-0'). New: split.jpg is renamed internally to split-0.jpg to provide a unified naming scheme.
* #8075 / Catch exception on filesize() - fixed: now return '-' for non existing files.


Version 19.3.1
--------------

Date: 15.03.2019

Bug Fixes
^^^^^^^^^

* 8058 / Form > fillStoreVar: broken for TemplateGroup - Form.fillStoreVar not available during fillStoreForm().
* 8048 / A retype FE should not be checked for 'required' during save.


Version 19.3.0
--------------

Date: 05.03.2019

Notes
^^^^^

* New FormElement 'datalist' for fe.type='select'.
* FormElement 'annotate':

  * Now supports 'ReadOnly'.
  * 'grafic': Undo/Redo

Features
^^^^^^^^

* #7729 / Select as datalist
* #7783 / FormElement 'annotate' - ReadOnly mode is now supported for grafic/text.
* FormElement 'annotate/grafic' History for undo / redo
* FormEditor: by creating a new FormElement, the feIdContainer is now preselected based on the last choice.
* Manual.rst: started to use more predefined Sphinx formatting styles.

Bug Fixes
^^^^^^^^^

* #7978 / mail.log: not written in some cases - fixed
* #7949 / Table MailLog: missing column 'cc,bcc'
* Manual.rst: correct parameter 'mode' for special column name '_sendMail'


Version 19.2.3
--------------

Date: 22.02.2019

Notes
^^^^^

New: `QFQ REST <https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html#rest>`_
interface implemented.

Features
^^^^^^^^

* Rest Implementation with GET,PUT,POST,DELETE and authorization token
* REST.md: add
* Manual.rst: reformat all sql code to use tyoposcript - it seems mysql does not work
* Manual.rst: Add some admonitions

Bug Fixes
^^^^^^^^^

* #7925 / Change CWD during split reduced to splitting only.
* #7925 / Fixed problem in mkDirParent() with absolute paths.
* #7925 / Make logger independent of CWD.
* #7925 / Fixed problem with ``mktemp --tmpdir`` (difference Ubuntu 16 / 18) by using PHP function again.
* #7925 / Fixed some 'undefined index' problems.

Version 19.2.2
--------------

Date: 19.02.2019

Notes
^^^^^

* QFQ now offers a basic REST API. Check https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html#rest

Features
^^^^^^^^

* 7910 / Check for double form names
* 7904 / REST api export. Manual.rst: describe QFQ REST API
* Latest phpStorm IDE complains about missing ext-json in composer.json. Added.
* Manual.rst: Example how to use 'password' escape class.

Bug Fixes
^^^^^^^^^


Version 19.2.1
--------------

Date: 18.02.2019

Notes
^^^^^

* New default session timeout: if nothing special is needed, leave the config.sessionTimeout empty. If there is an old value
  best is to remove it.
* Variables with escape class='p' now hashes the content to be used as T3 FE passwords. This let's QFQ handle FE User
  registration or password reset.

Features
^^^^^^^^

* Manual.rst: update pathFilename to pathFileName an all places.
* Apply padding-top|botttom to fieldset via qfq-fieldset class.
* F7165 / fe user registration. New escape type 'p' for T3 passwords.

Bug Fixes
^^^^^^^^^

* #7634 / Session Timeout too short. Annoying 'session expired' message removed. Default timeout now takes the system defautl.
* #7864 / 'required'-FE Elements, deactivated via formModeGlobal=requiredOff missed the read marker. Class
  'required-field' is now always assigned. The final 'required' mode is still temporarily disabled.
* #7848 / extraColumName 'paged' - easier handling in case 'r=0' or empty 'U:...' - fixes #7848


Version 19.2.0
--------------

Date: 07.02.2019

Bug Fixes
^^^^^^^^^

* #7714 / autocron fails to open logfiles - adjust CWD based on argv(0).


Version 19.1.3
--------------

Date: 28.01.2019

Notes
^^^^^

* If a variable violates a sanitize class, the substituted result can now be configured: a) !!<class>!!, b) '0', c) '', d) '<custom message>'.
* Alerts (based on _link class), might now show only 'ok' (alone, without 'cancel').
* Excel Import - three new options: importNamedSheetsOnly, importSetReadDataOnly, importListSheetNames

Features
^^^^^^^^

* SQL Error / underlining in exception dialog: Add two SQL errors to be underlined in exceptions. Extend to "... in 'order clause'"
* Extend allowed SQL commands in QFQ vars (have been already subscribed in that way in Manual.rst).
* New escape mode 'C' - escapes ':' by '\'.' - useful for variables in variable definition.
* fillStoreVar: Replace setStore() with appendToStore()
* #6914 / Customized typeMessageViolation. Incl. unit tests.
* #7743 / Move error messages to Constants.php. Unit tests use those constants now. 'data-pattern-error' only delivered
  if a 'pattern' is given. 'required' attribute only delivered is set. Detection of 'pattern error' on per QFQ default,
  custom instance wide, per form or per FormElement - per FormElement overwrites other. Move default pattern to constants.
  Make default error text more specific (only if default error text is not explicit set in config, form or form-element)
* #7747 / New options to import Excel files: importNamedSheetsOnly, importSetReadDataOnly, importListSheetNames
* #7684 / Optional hide second button (cancel) in link/question alerts.

Bug Fixes
^^^^^^^^^

* #7743 / Form-Element: Explicit given '0' for MIN or MAX has been interpreted as 'not set'.
* #7702 / Form,Form-Element: Unnecessary evaluation of column 'noteInternal' / 'adminNote'.
* #7695 / Form/URL Forward: pageAlias not substituted.
* #7686 / FormAction/sendmail: uninitialised sendMailAttachment.
* #7685 / Open FormElement from QFQ error message and save modified record: report error about missing {{formId:F}}.
* FormElement.type=select: fixed problem with dynamic update and mode=readonly - list was still selectable.
* Hint to skip leading zeros in version number.

Version 19.1.2
--------------

Date: 17.01.2019

Notes
^^^^^

* Align FormElement labels left/center/right.
* Cleanup FormEditor.
* All SQL commands now are allowed.

Features
^^^^^^^^

* #7620 / Label align left/center/right. Defined by config/form/formelement.
* #7647 / Preparation for Selenium Tests - Implement new token 'A'  to add any custom attribute to '... AS _link'
* Cleanup FormEditor: FormElement as second pill. Hide MultiForm as long as it not active. Rename Various to Layout.
* Manual.rst: Add some tips. Add note how to extend TinyMCE valid_elements.
* Delete composer in Resources dir, run phpunit tests in gitlab pipeline.
* Database.php: allow all SQL commands.

Bug Fixes
^^^^^^^^^

* #7671 / On FormElements, 'fillStoreVar' is now detected and fired at first.
* Fix for checkbox-inline / radio-inline.
* Check for non unique FormElements: multiple empty FE.name (e.g. for FE.type=subrecord) are allowed now.


Version 19.1.1
--------------

Date: 04.01.2019

Bug Fixes
^^^^^^^^^

* #7600 / Path to sendEmail has changed and is updated now.
* #7603 / Fix problem: formEditor.sql broken - missing semicolon. QFQ updates did not played formEditor.sql.
  Unit test for DatabaseUpdate(). Especially that formEditor.sql is running fine.
* #7594 / FE.type=extra: don't name it 'type' or 'id' or 'L' - more detailed error message and an explanation in Manual.rst.
* Config.php: error message about to high session timeout now reports the PHP settings.

Version 19.1.0
--------------

Date: 03.01.2019

Features
^^^^^^^^

* Session expired: report details about session timestamps
* File not found in FE.type=file: Show more clearly that the pathfilename is only shown when ShowDebugInfo=on.

Bug Fixes
^^^^^^^^^

* #7553 / If a pill is hidden, FE on that one should not be processed during save/update.
* #7573 / Upload: Do FillStoreVar before slaveId.
* #7551 / General error: Access to undefined index.
* #7544 / General error: Download.php / Line: 175.
* #7543 / General error: QuickFormQuery.php / Line: 1364. Happened during upload.
* Download Excel (and all other download types): Content-Disposition header delivered/suppressed in the opposite
  meaning as it should be. Seems to be fixed now.

Version 18.12.3
---------------

Date: 25.12.2018

Features
^^^^^^^^

* Form: Add text 'Record id/Created/Modified' to tooltip of save button.

Bug Fixes
^^^^^^^^^

* #7540 / Form: Upload broken. HelperFile.php: correctRelativePathFileName() broken after refactor of qfq paths.
* #7514 / Report: Broken defaults in _pdf, _file, _link.
* #7538 / Report: Excel.php - access to undefined index.
* #7289 / Report: {{<level>.line.insertId}} - missing for altsql.
* #7539 / Report: Copy to clipboard not reliable. 'Direct' content now correctly encoded. 'Copy file content' fully implemented
  for text files.


Version 18.12.2
---------------

Date: 24.12.2018

Notes
^^^^^

* Version skipped. Build problems in CI queue.

Version 18.12.1
---------------

Date: 22.12.2018

Notes
^^^^^

* Existing installations: update QFQ extension config form-layout.formBsColumns/formBsLabelColumns/formBsInputColumns,formBsNoteColumns.
  old: '12', new: 'col-md-12 col-lg10' resp. smaller values for individual columns.
* New config values:

  * Config/flagProduction: yes/now - differentiate between development und production system. Will be used for
    'throwExceptionGeneralError' too.
  * Debug/throwExceptionGeneralError - shows/hide exception of general errors.

* Renamed config values:

  * SITE_PATH >> sitePath
  * EXT_PATH >> extPath
  * _dbName >> dbName

* Record locking: revert to old behaviour, that a locked record can't be modified by another form, even if the second
  form has modeRecordLock=NONE.

* Autocron: update the system crontab entry to the new path (old 'qfq', new 'Source'):

  .../typo3conf/ext/qfq/Classes/External/autocron.php

Features
^^^^^^^^

* #7228 / Show error if form element with same name and class already exists.
* #7494 / Exception 'General Error': disable/enable per config.
* Adapt class paths in composer.json to the newly refactored folder names.
* Add 'col-lg-10' to notes section.
* Added Alert Manager to have better control over the Alerts.
* Config.php: make 'reading dbname' Typo3 version dependent.
* Delete two old composer.json and call new composer in Makefile.
* Excel.php: change autoload.php path to new composer folder.
* Manual.rst: several places where the bs-column description are updated with the new way 'col-md-...' instead of '12'.
  Replace ''{{pageId' with '{{pageAlias'. Replace '... AS _Page' by '... AS _page'. Add 'tablesorter tablesorter-filter'
  to FormEditor example page.
* Move bootstrap.php and BindParamTest.php due to refactoring.
* phpunit.xml: implement const 'PHPUNIT_QFQ'. Store.php: set self::$phpUnit on const 'PHPUNIT_QFQ'
* Refactor: 'extension/qfq/qfq/...' to 'extension/Classes/Core/...'
* Refactor: Manual.rst update config variables (reorder), add 'qfqLog'. Support.php: formSubmitLog hardcoded to
  fileadmin/protected/log. DOCUMENTATION_QFQ > SYSTEM_DOCUMENTATION_QFQ. Remove config var 'logDir'.
* Refactor: SITE_PATH >> sitePath, EXT_PATH >> extPath, SYSTEM_PATH_EXT >> SYSTEM_EXT_PATH
* Remove report/Define.php, report/Error.php.

Bug Fixes
^^^^^^^^^

* #3464 / Checkboxes now disabled (readonly), even when rendered as Bootstrap. Fixes missing readonly for Template Groups.
* #6467 / Sanitizing a hidden field makes the form unsubmittable. Updated elementupdate for pattern, added "false" as an
  alternative to null.  'element-update' now get's 'pattern=<pattern>|false' on element-update.
* #7089 / FE.type=extra: value already set in SIP store.
* #7223 / Add "-" as allowed characters in filenames.
* #7455 / phpunit: Remove outdated report syntax. Catch exception on trying to open a non existing logfile.
* #7461 / Bug in Doku.
* #7464 / DragAndDrop - Undefined index: ord. Bug in subrecord fixed.
* AbstractException.php: fixed problem with empty $match in sql syntax highlighting.
* Block screen stuck seems fixed.
* Check not to try to number_format() empty string.
* config.qfq.example.php: add missing '>'.
* Fixed broken init in phpunit run. Fixed access to uninitialized var. Throw exception if dndTable or form is missing.
* Fixed missing $formElement[FE_DECIMAL_FORMAT]. Add 'missing primary record' check. Fixed missing fe['id'].
* Fixed problem with '?:' - implemented in 2016.
* Fixed problem with resultset in 'altsql'.
* formEditor.sql, Manual.rst: renamed '{{_dbName...:Y}}' to '{{dbName...:Y}}'.
* Manual.rst: Unify 'dbName*' documentation..
* phpunit: Update fixtures table 'Form', 'FormElement' & 'Dirty' definition. Update LDAP Test - surrounding spaces seems
  to be escaped now by '\20'. Create tests dir outside of source, create phpunit.xml, move some tests and make them work.
  Rename tests directory to Tests
* Record Lock. Revert change in cb2e2a70cfe5c251cffbfce65bdc0899da75a9c5: if a record lock exist, another form, with
  record lock mode=NONE, can't get write access to it. This is what the manual describes.
* Sanitize.php: fixes "Uninitialized string offset: 0" error.
* Save.php: Fixed exception (file size, mime type) for non-existing uploads.
* Store.php: fixed problem with wrong config varname for DB.
* Timeout check throws error since php.ini cookie lifetime is zero during unit test. Fix filepath relative to Typo3 dir
  when running unit test.


Version 18.12.0
---------------

Date: 10.12.2018

Notes
^^^^^

* Config.qfq.php: the variable T3_DB_NAME is not necessary anymore.
* Following SYSTEM_STORE variables renamed. Old: '_dbNameQfq' ,'_dbNameData'. New: 'dbNameQfq' ,'dbNameData'
* New: Bootstrap 'col-lg-10' is defined on every form. On screens greater than 'md' the forms won't be expanded to 100% anymore.

Features
^^^^^^^^

* #3992 / STORE_SYSTEM: dbNameQfq, dbNameData, dbNameT3
* Config.php: read 'dbNameT3' from TYPO3_CONF_VARS or from T3 config file.
* Download.php: get dbNameT3 now from STORE_SYSTEM
* #4906 / Php Session Timeout: can be specified globally in configuration and per form. Affects only logged in FE User.
* #6866 / On logout destroy STORE_USER. Session.php: check if _COOKIE['fe_typo_user'] has changed - yes: clear STORE_USER. Store.php: Rearrange functions.
* #6999 / Bootstrap/Form: define columns for desktop 'col-lg-10'
* #7138 / PDF / single source: deliver without converting
* #7293 / Implement new logging for file upload.
* #7406 / dbinit might contain multiple sql statements now.
* #7407 / MariaDB / Ubuntu 18 complains about missing values if column of type TEXT isn't explicit specified in INSERT. New default for database.init=SET sql_mode = "NO_ENGINE_SUBSTITUTION"
* #7431 / FE.type=afterSave (FE Action): SQL won't report the causing FE.name/id
* #7434 / FE.type=beforeLoad / sqlValidate: Validation message not shown to user
* FormEditor.sql: Switch off 'MySQL strict setting of default values'
* Logger.php: remove UserAgent - that one is logged in FormSubmitLog Table. Add 'cookie' to filter individual actions.
* New css classes for icons: .icon-flipped (mirrors icon), .icon-spin (icon spins once on hover), .icon-spin-reverse (mirror of icon spin).

Bug Fixes
^^^^^^^^^

* #7001 / Error message: if 'modeSql' fails, error message does not contain a reference to the causing FE.
* ErrorHandler.php: raise an error as an exception has been stopped in mid 2017 - reactivated now.
* Fix problem in upload elements: a) in exception FE_ID was not reported, b) fillStoreVar was not fired.
* FormAction.php: throw exception if 'fillStoreVar' selects more than one row.


Version 18.10.3
---------------

Date: 29.10.2018

Notes
^^^^^

* QFQ now recommends ImageMagick instead of GraphicsMagic, due to much better 'auto orient' (JPEG) capabilities.

Features
^^^^^^^^

* #4901 / Implement 'delete split files' if primary file is deleted (record) or replaced against a new one.
* #4901 / Implement 'fileSplitOptions'.
* #4901 / Implement PDF split based on IM 'convert' (jpeg).
* #7012 / Memory limit for file download - replace file_get_content() by readfile().
* #7112 / fabric: configure default color.
* thumbnail: empty path filename is no ignored, instead of throwing an exception. This is the same behaviour as
  building links - no definition means 'no link', and not 'error'.
* Refactor chmod(), unlink(), rename(), rmdir(), copy() to use by QFQ version which throws an exception.

Bug Fixes
^^^^^^^^^

* #3613 - Revert due to bug in dynamic show/hide (Revision 0bb99fd, Revision 77096ca7) -

Version 18.10.2
---------------

Date: 13.10.2018

Features
^^^^^^^^

* #2509 / Render encrypted mailto as single link
* #3281 / Trim form inputs
* #4649 / Add sqlBefore & sqlAfter to sendmail FE, move validate() before sqlBefore()/sqlAfter().
* #4922 / Some minor improvements to the excel import
* #5112 / Add incompatibility warning for encode specialchar and checkType allbut…
* #5450 / MySQL Exception: underline faulty area
* #6596 / uid ExcelExport / PDF
* #6944 / Add double comma SQL Hint


Bug Fixes
^^^^^^^^^

* #3529 / No double urldecode() of GET parameters
* #3613 / Label input note layout
* #3850 / Change set input maxlength
* #4545 / After delete: reload page with original parameters
* #4751 / Add bulleted und numbered lists back to tinyMCE Editor
* #4765 / Extend tooltip visibility for checkboxes and radio buttons
* #6911 / Only fire afterInsert on new record (also fix afterUpdate, beforeInsert,…
* #6929 / Treat single file argument like several file argument (savePdf: copy,…


Version 18.10.1
---------------

Date: 12.10.2018

Features
^^^^^^^^

* #5578 / Safari only handles one filetype in upload dialog.
* #6991 / Optional process 'readonly' FE during save. New FE parameter 'processReadOnly = 0|1'.

Bug Fixes
^^^^^^^^^

* #6880 / Fixed Exceptions with too many details to end user.
* 'Drag and drop' failed due to fillStoreForm requests {{form:S}} which was not necessary for drag and drop.
* Upload: rename 'chmod' to 'chmodFile'. Implement 'chmodDir'. Permissions applied for all new created directories.
* Upload: replace 'rename' with 'copy/unlink'

Version 18.10.0
---------------

Date: 04.10.2018

Features
^^^^^^^^

* #6894 / Upload: chmod for file creation.
* #6886 / Upload: Auto Orient - implementation.
* #6721 / Log switching {{feUser:U}} to qfq.log. Log {{feUser:U}} to sql.log.
* #5458 / Add '{{feUser:U}}' to be shown on exception.
* Manual.rst: Fix missing single tick in special column name '_=<var>'
* Report / Variables copied to STORE_USER via "... AS '_=<varname>'" are now available in STORE_RECORD by {{<varname>:R}}

Bug Fixes
^^^^^^^^^

* #6902 / Drag and drop in subrecords: expect 1 row got nothing.

Version 18.9.2
--------------

Date: 16.09.2018

Notes
^^^^^

* To use the new 'tablesorter' feature, please add 'tablesorter-bootstrap.css', 'jquery.tablesorter.combined.min.js',
  'jquery.tablesorter.pager.min.js', 'widget-columnSelector.min.js' in your Typo3 template record.
  See https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html#setup-css-js

  * *Existing* QFQ installations: update your CSS/JS includes! The new tablesorter jquery plugin might break (JS errors
    seen on the console) your installation, if it isn't included!

  * If you use the extension 'UZH Corporate Design Template':

    * Update to the latest version 18.9.0.
    * Add constants in the Typo3 template record 'ext: main': ::

            cd.extra.css.file5 = typo3conf/ext/qfq/Resources/Public/Css/tablesorter-bootstrap.css

            cd.extra.js.file10 = typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.combined.min.js
            cd.extra.js.file11 = typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.pager.min.js
            cd.extra.js.file12 = typo3conf/ext/qfq/Resources/Public/JavaScript/widget-columnSelector.min.js

* STORE_USER: check the examples - great new feature to temporary save user settings.

Features
^^^^^^^^

* #6721 / STORE_USER: variables per browser session
* #6690 / Tablesorter for subrecords

Version 18.9.1
--------------

Date: 15.09.2018

Notes
^^^^^

* Report

  * Type of nesting delimiter in 'report' now limited to '{', '[', '(', '['
  * Hide/reuse report content later: `10.content=hide` and later `1000.head = {{10.content}}`

* New Excel import - copy Excel files directly into a DB table.
* Forms with named primary key different than 'id' are now supported.

Features
^^^^^^^^

* #1261 / Tablesorter, incl. saved sort, combined column sort, filters, pagination
* #3129 / suggestion for subrecord title design
* #4922 / Excel import
* #6300 / Disable preview button for requiredNew forms
* #6314 / HTML Mode & sendMailSubjectHtmlEntity for Forms.
* #6481 / Add custom primary key option to Forms.
* #6645 / better error message for incomplete download as link
* #6650 / Report hide content (line) - use later via a variable. First version - problem with enclosed ticks.
* #6653 / Add save button class/glyphicon/tooltip for submit button
* Thesis code correction
* Update QFQ download URL

Bug Fixes
^^^^^^^^^

* AbtractBuildForm.php: fix problem with subrecords in MultiDB Environment.
* #2340 / Report: Problematic Bracket - form now on, only ''{[(<'' will change the nesting token.
* #3333 / Fixed subquery recognition in reports '10.sql, 10.1.sql ...'
* #4837 / Don't display hidden pills
* #5467 / Fill Record Store when evaluating min/max parameters
* #6621 / Fix shifted subrecord head
* #6646 / Fix broken extraInfoButton for some FEs


Version 18.9.0
--------------

Date: 07.09.2018

Features
^^^^^^^^

* #6357 / Save pdf on server
* #5381 / Stored procedures can be called from QFQ Reports
* #4996 / Log QFQ Update with timestamp.
* #6255 / Inline Report Editing - now with SIP and save.php api

Bug Fixes
^^^^^^^^^
* #6465 / Allow newlines in form action queries (e.g. sqlInsert)
* #4654 / Better FE color highlighting (UX)
* #5689 / Default BS Columns for FormElement match Form setting
* #6484 / Download Links mit css class
* #6576 / download buttons are now rendered disabled with render mode r:3
* Cookie Sitepath: wrong detected in case of API calls.

Version 18.8.2
--------------

Date: 28.08.2018

Features
^^^^^^^^

* #6563 / Accept 0 as required.

Bug Fixes
^^^^^^^^^

* DatabaseUpdateData.php: add missed 'on the fly' update for Form.title, changed in FormEditor.sql in 18.8.1
* 6562 / sendmail: redirect all mail - the sender is replaced too.
* Manual.rst: several typos fixed

Version 18.8.1
---------------

Date: 26.08.2018

Features
^^^^^^^^
* #4432 / Every 'form submit' will be logged with raw data.
* #4763 / Render vertical text more stable: '... AS _vertical'
* #4996 / Log QFQ Version update
* #5403 / Tooltip on pills are now supported
* #5876 / Subrecord title of column 'Edit' & 'Delete' are now customizable.
* #6249 / Subrecords can now be reordered via drag and drop.
* #6333 / Add to qfq.log: IP Address, User Agent, QFQ Cookie, FE User

Bug Fixes
^^^^^^^^^

* #6401 / Handle Backticks in sendmail
* #6452 / Empty form title: no title row will be rendered anymore.

Version 18.8.0
--------------

Date: 25.08.2018

Notes
^^^^^

* Excel export
* Copy to clipboard

Features
^^^^^^^^

* #4922 / Excel Export - create Excel sheets from scratch or based on a template.
* #3294 / Improve Typo3 QFQ backend layout. Add sparql syntax highlighting.
* #5878 / Formelement.type=note with #!report - whitespace is trimmed.
* #6314 / HTML Mails enabled by specifying flag 'mode=html'.
* Import/Merge form: A new form 'copyFormFromExt' (see file `copyFormFromExt.sql`) offers a one click import of external
  QFQ forms (incl. renumbering of id's).
* formEditor.sql: resized Form.title from 255 to 511 (requested by IK Tool)
* Drag and Drop now offers the possibility to show the renumbered values.
* Manual.rst: security hints, T3 Setup best practice, text input retype, charactercountwrap.
* Config.qfq: central defaults for DATA_MATCH, DATA_ERROR
* Bootstrap QFQ development: switched from bower to npm only.

Bug Fixes
^^^^^^^^^

* #5843 / File upload: limitation to file extensions are no case insensitive.
* #6247 / Replace deprecated each function
* #6281 / FormElement / column 'note': token '#!report' - STORE_RECORD does not work.
* #6331 / File Upload: Wrong error message if filesize is much too big.
* #6229 / Add QFQ icon to content element and content element wizard
* AbstractException.php: fixed problem with htmlEntities() on link to 'Edit Form' and 'Edit FormElement'.


Version 18.6.1
--------------

Date: 21.06.2018

Notes
^^^^^

* Configuration QFQ: form-config.formDataPatternError. New behaviour: If this field is empty, a more specific default
  message is shown (instead of one message for all situations). Best is to clear this field.

Features
^^^^^^^^

* sqlHint / Note if a query fails and contains some not replaced variables.
* #4438 / Log attack detected: will be logged now to fileadmin/protected/log/qfq.log.
* #4041 / Subrecord: Spalte 'id' automatisch mit '<span class="text-muted">' wrappen.
* #5885 / show 'sql.log' in FE.
* #6121 / Formular: ID per Default in Titel.

Bug Fixes
^^^^^^^^^

* #6283 / Form: hide title frame if empty.
* #4299 / HiddenSelect' into 'master'.
* #6276 / default data-required-error moved to central Config.php.
* #5884 / sql.log by default public - protect against access.
* #6276 / Default check_type messages not shown.
* #6233 / Alert 'Form incomplete' - stays until click - auto disappear would be better.

Version 18.6.0
--------------

Date: 13.06.2018

Notes
^^^^^

* config.qfq.ini migrated to config.qfq.php - the old config.qfq.ini get's `chmod 000`.
* Most of config.qfq.ini migrated to Typo3 / Extension Manager - all but the DB /LDAP credentials.
* Keep in config.qfq.ini: ::

  # Rename DB credentials from DB_<key> to DB_1_<key>, with key = 'NAME|HOST|USER|PASSWORD'
  DB_1_USER = ...
  DB_1_SERVER = ...
  DB_1_PASSWORD = ...
  DB_1_NAME = ...

* NEW: Drag and drop to sort elements! Check the Manual.
* `URL forwardMode`

* `client` renamed to `auto`.
* `close` added.

Features
^^^^^^^^

* #6100 / Url Forward Auto: Update Manual.rst. The F.parameter.saveAndClose has been removed again. Mode 'close' can be assigned statically or dynamic.
* #6178 / Input: Step: New option 'step' for FE.parameter.
* Download.php: references to non existing files now reported as missing file, not 'wrong mimetype' anymore.
* #4918 / Drag'n'Drop reorder elements DRAGANDDROP.md, PROTOCOL.md: Doc for "drag'n' drop" implementation.
* dragAndDrop.php: API endpoint DragAndDrop.php: Class for implementing drag'n' drop functionality.
* Link.php: implement new renderMode=8 - returning only the sip. QuickFormQuery.php: New entry point for processing "drag'n' drop".
* #3971 / Form title: new design from form title.

Bug Fixes
^^^^^^^^^

* #5077 / Dynamic Update & FE.type=required: Server fixed -

a) dynamic calculated modeSql respected,
b) formModeGlobal=requiredOff respected,
c) dynamic FE with mode='hidden' are not saved anymore.

* #6176 / Icon not aligned when error text: Buttons now wrapped in one 'input-group'.
* Manual.rst: reformat autocron QFQ code.
* #5880 / Skip Error Message during dynamicUpdate.
* #5870 / Missing file config.qfq.ini: Clean QFQ message.
* #5924 / config.qfq.ini/LocalConfiguration.php: several places in formEditor.sql still contained the 'dbIndex...'.
* #6168 Configuration language setting ignored: Form and FormElement editor still used uppercase config values for
  language configuration. Updated to the new camel case notation.
* #5890 / config.qfq.ini is public readable. Renamed file to config.qfq.php. Implement a basic migration assistant to
  copy DB credentials to new config.qfq.php. All other values have to be copied to extmanager/qfq-configuration manually.
* #6216 / Oops, an error occurred! Code - unhandled exception will be caught now.


Version 18.4.4
--------------

Date: 28.04.2018

Bug Fixes
^^^^^^^^^

* Fix broken ext_emconf.php

Version 18.4.3
--------------

Date: 28.04.2018

Bug Fixes
^^^^^^^^^

* Version Number ...04... not supported by TE. Changing naming scheme to omit leading zero.

Version 18.04.1
---------------

Date: 28.04.2018

Bug Fixes
^^^^^^^^^

* config: broken dbIndexQfq, dbIndexData.

Version 18.04.0
---------------

Date: 26.04.2018

Notes
^^^^^

* QFQ marked as 'stable'
* New version numbering: Year.Month.Index
* Manual.rst:

  * AutoCron documentation enhanced.
  * Replace '{{form:S}}' against '{{form:SE}}'.
  * Check list for 'new installations'.
  * Description for config variables enhanced.
  * Details 'how record locking' is done.
  * Details: extraButtonInfo.
  * Replace config.qfq.ini on most places with 'configuration'.

* Path of 'sql.log' / 'mail.log' are now relative to <site path> (not <ext path> as before).

Features
^^^^^^^^

* formEditor.sql: update table cron.
* AutoCron.php: allow https connections with invalid certificate (e.g. 'localhost' is not listed as a valid hostname).
* ext_conf_template.txt: Extension manager configuration setup.

Bug Fixes
^^^^^^^^^

* AutoCron:

  * Update form 'cron' to load/save records in DB_INDEX_QFQ.
  * Fix problem with array in checkForOldJobs().
  * Implement check that re-trigger asynchronous cron jobs are handled correctly.



Version 0.25.15
---------------

Date: 20.03.2018

Features
^^^^^^^^

* Fabric Read Only mockup.

Bug Fixes
^^^^^^^^^

* #5706 / Fixed that problematic characters in 'fileDestination' has not been sanitized.
* Fixed problem with buttons clipping trough alert.
* Client: wrong variable, updated CSS for long errors.

Version 0.25.14a
----------------

Date: 15.03.2018

Features
^^^^^^^^

* Change getMimeType() in Report in case file is missing or `file` beaks: instead to throw an exception, an empty string is returned.
* Updated protocol.md with Alert description.
* Update Status message for save/delete.
* Makefile: 1) remove sonar, add dependency to let update-qfq-doc run. 2) do qfq doc commit inside of the Makefile.
* Client: Changed save timeout from 1500 to 3000.
* Client: removing the blackout screen when modal gets dismissed.
* Client: modal alerts are now blocking everything.
* Manual.rst: fix RST syntax errors.

Bug Fixes
^^^^^^^^^

* #5677-TinyMCE broken - fixed.


Version 0.25.14
---------------

Date: 14.03.2018

Features
^^^^^^^^

* Change notification from 'save: success' to 'Save' and 'delete: success' to 'Delete'.
* DB update: write intermediate QFQ version after every step.

Bug Fixes
^^^^^^^^^

* #5652 / TypeAheadSql: destroyed SQL statement. Fixed broken compare and missing init of $sqlTest.
* #5668 / Fix Broken SIP after login.


Version 0.25.13
---------------

Date: 08.03.2018

Features
^^^^^^^^

* AutoCron: Added doc for autocron. Extend AutoCron.php to be MultiDB aware. Update der AutoCron form.
* #4720 / Separate Database for Form & FormElement - Multi DB - fixed problem that 'Quick Edit Form / FormElement' has been broken in MultiDB Setup.
* #5603 / Report: final value of report columns (special column name).
* Fabric / delete now triggers form.changed / emojis work again.
* #5571 / File Upload: save filesize and mimetype automatically in 'upload mode simple',if those columns exist.
* #5423 / two new column names 'filesize', 'mimetype'.
* #5571 / File Upload: save filesize and mimetype.

  * STORE_VARS contains now 'mimeType' and 'fileSize'.
  * sqlBefore and sqlAfter will be fired in Upload Advanced and new in Upload Simple as well.
  * STORE_VARS contains now `filenameOnly`. It can be used in downloadButton=....

Bug Fixes
^^^^^^^^^

* Fabric: Corrected resizing with changed width in editor.
* #5640 / UTF8 encoded strings: MAX LENGTH wrong.

Version 0.25.12
---------------

Date: 18.02.2018

Notes
^^^^^

* New

  * FE.parameter:

    * timeIsOptional
    * enterAsSubmit

  * FE.checkType: Auto
  * Thumbnail rendering. Public or secure.

* Update

  * Multi DB Support: Form & Report

Features
^^^^^^^^

* #5064 / Throw user form exception on invalid date.
* #5308 / TimeIsOptional parameter.
* #5318 / Allow sendmail speaking word token, adjust documentation and fix some typos.
* #5347 / Error Message (Exception): BS colored box for report error messages. Hide technical informations, show it on click.
* #5392 / Violate message with expected date format.
* #5414 / Add checkType Auto, refactor setDefault methods, add smart detection of defaults, extend documentation and rules.
* #3470 / Enter As Submit= on/off - implemented.
* #4437 / violate sanitize message.
* #4542 / input-type-decimal' into 'master'.
* #5298 / Update docs for HTML mails.
* #5333 / Thumbnail: implementation.
* #5425 / Thumbnail: render mode 7 - implemented, rewrite - secure thumbnails are now rendered on first access, not when
  'AS _thumbnail' is called.
* Implemented $dbIndex for Report.
* Implemeted two new STORE_SYSTEM variables: '_dbNameData' and '_dbNameQfq' - those will be automatically filled qfq
  during instantiation QuickFormQuery(). They can be used in Report to easily access the needed DB.
* Increased Formelement.label from 255 to 511.
* Make DB_INIT in config.qfq.ini set by default.
* Notes how to optimize PDF thumbnailing.
* Reformat manual for config.qfq.ini. Copy config.qfq.example.ini to MANUAL.rst. Migrate config defaults from
  setIfNotSet() to array_merge().
* Security: hide $SQL in error messages to regular user.
* New FE.parameter 'inputType'. Can optional be given by webmaster. Additional, the 'type="number"' will be automatically
  set, if the column is of type 'int' or if 'min' and 'max' is numerically.

Bug Fixes
^^^^^^^^^

* #3192 / Fill STORE_RECORD before loading table title.
* #5285 / Make typeAheadPedantic the default.
* #5348 / Exception/Report: level key missing.
* #5367 / Error Report: reworked alerts, updated css for alerts, 'full level' missing, content too much escaped: Fixed
  too much escaping. Form / FormElement Links in error messages now with BS Buttons.
* #5382 / Double quotes in tooltips are now escaped with &quot;.
* #5390 / input validation decimal broken. fixed.
* #5430 / Add unique ID to each radio button for dynamic update.
* Form: 'FormElement' > 'Container' - relied on '{{formId:S}}' even if the FE record already exist - fixed.
* Subrecord Title - now wrapped with <label class='control-label'>.

Version 0.25.11
---------------

Date: 31.01.2018

Notes
^^^^^

* Violating a sanitize class now returns '!!<sanitize class>!!' instead of an empty string.

Features
^^^^^^^^

* #5022 / Variable violates santize class: 'msg' instead of empty string - new identifier "!!<sanitize class>!!".
* #4813 / Exception during form load: show 'form edit link' if editor is logged in.
* formEditor.sql: Increas size of Form.title to give more room for SQL statements in.
* Manual.rst: enhance debug tipps.
* #5321 / Plain Link - render mode- only url - implemented.
* Add regex101 link to checkPattern FormEditor.

Bug Fixes
^^^^^^^^^

* Fixed some broken help links in formEditor.sql.
* #5306 / Exception: tt_content_uid wrong - fixed.
* #4303 / Download von doc/docx-Dateien / Download.php - Mime type wird nicht mehr an Dateiname angehängt.
* #5316 / Help on how to send an E-Mail is wrong - several places fixed.
* #5311 / Error Msg SLQ_RAW != SQL_FINAL: Debug message shows outdated SQL_RAW.
* #5309 / min/max broken for date fields. Add min/max attributes to input and date input tag.
* Fabric now detects 'dirty'.
* Manual.rst: Remove broken link to W3C file upload.


Version 0.25.10
---------------

Date: 26.01.2018

Notes
^^^^^

* PROTOCOL.md: update notes.
* Form / Upload: new option 'downloadButton' - if given renders a download button instead of showing the pathFileName.

Features
^^^^^^^^

* #5023 / Fabric: Cut, rotate and enhance uploaded images. Update Manual.
* All FE 'typeahead' fields are set to 'autocomplete="off"'. Respect user setting for 'autocomplete' - if none given
  (mostly), set it for FE 'typeahead' to 'off'.
* #5295 / Upload: check if given QFQ 'maxFileSize' is higher than php.in post_max_size, upload_max_filesize.
* FE.Subrecord: rearranged column order, start columns with uppercase letter.
* New CSS class 'qfq-full-width-left': especially for buttons to become full width.
* New CSS class 'qfq-table-100' - 100% width, with auto width per column. FE.subrecord changed to  'qfq-table-100'.
* #5302 / remove CSS class 'internal / external'.

Bug Fixes
^^^^^^^^^

* #5189 / BCC SendMail Problem - fixed missing double ticks.
* Manual.rst: Update documentation that the default escape type is 'm'. Remove subrecord/list (have been removed long
  time ago). Fix enumeration problem FE.type=radio `classButton`. Add short note for typeahead.js. Remove never
  implemented 'keySemdId...', 'ANREDE'. Fixed typo - replace '\' by '\\' on most places (not in code sections).
  More generic SQL to extract filename from pathFileName. Fixed several phinx syntax errors. Add example for
  'recent list'-records.
* Fixed problem with missing 'if note exits' in CREATE TABLE `Split`.
* #5030 / Manual.rst: Fixed example with XSS vulnerability.
* #5275 / typeahead.bundle.min.js missing in Manual.rst: fixed.
* FormEditor: 'typeahead' for column 'name' fixed. Attention: only succeed if DB_1_NAME is the final DB (mostly given).
* #5048 / Default value NULL in pathFileName breaks uploads.
* #5028 / Links im FormularEditor zeigen ins Leere (Fehlende Ziel-Anker) - fixed.
* Make readonly BS radio buttons non-selectable.

Version 0.25.9
--------------

Date: 17.12.2017

Features
^^^^^^^^

* #5133 / sendmail: subject and body html entity decode: Introduce options for 'subject' and 'body' to switch on/off HTML encoding / decoding
* Manual.rst: Add notes to QFQ installation, wkhtml problems, paragraph on 'sendEmail' Html2Pdf.php: Add error codes and a hint on wkhtml fails.
* Reformat table qfq-letter.css.less: redefined h1, letter-receiver.

Bug Fixes
^^^^^^^^^

* Bug in sendEmail: invalid SSL_version specified at /usr/share/perl5/IO/Socket/SSL.pm line 575. Patch for sendEmail
  (see https://unix.stackexchange.com/a/68952).


Version 0.25.8
--------------

Date: 11.12.2017

Features
^^^^^^^^

* #5080 / Dynamic PDF Letter.
* #5083 / Bodytext / Report: join lines without spaces.

Bug Fixes
^^^^^^^^^

* Fix problem with commit from 8.12.17 / Store.php: appendToStore.php stopped working - 'report' failed to replace
  '{{<column>:R}}'.
* Store.php: fix problem with empty 'appendToStore()' call.

Version 0.25.7
--------------

Date: 07.12.2017

Notes
^^^^^

* Report: parameter in '... AS _sendmail' needs token now - position dependent is removed now.
* Report: parameter 'a:' in '... AS _sendmail' replaced by 'F:' to be compatible with downloads. Do not separate files by comma.
* Manual: most occurences of 'U:' replaced by 'p:' - same meaning.

Features
^^^^^^^^

* #4255 / Attachments for emails implemented.

Bug Fixes
^^^^^^^^^

* Bug - PHP Warning: Declaration of qfq\BuildFormTable::head() should be compatible with
  qfq\AbstractBuildForm::head($mode = qfq\FORM_LOAD) - fixed.


Version 0.25.6
--------------

Date: 03.12.2017

Notes
^^^^^

Bigger changes in update form after save/dynamic update.

Bug Fixes
^^^^^^^^^

* #4865 / Pill Dynamic Updates Show / Hide.
* #5031 / Missing details in DbException: New definition of SYSTEM_SHOW_DEBUG_INFO: even after config.qfq.ini is parsed
  and SIP Infos has been read - if there is no BE User logged in, the value stays on 'auto' (earlier it has been replaced
  to 'no'). Staying on 'auto' keeps the information that replacing is still open and not replaced means 'no'-BE User logged in.
* #5016 / Loose checkbox value on save - Dirty workaround - better solution necessary.
* #5017 / STORE_RECORD used in FormElement and via '#!report' - save & restore STORE_RECORD.
* #5004 / FormElement with state 'ReadOnly' will be saved with empty value - existing values will be overwritten - fixed.
* 'element-update' for type 'UPLOAD seems to make trouble. Exclude it like 'SELECT'.


Version 0.25.5
--------------

Date: 23.11.2017

Bug Fixes
^^^^^^^^^

* #4771: Workaround which switches off updates to SELECT lists, if they are part of a Multi-FE-Row.


Version 0.25.4
--------------

Date: 22.11.2017

Notes
^^^^^

* New keywords / features in report:

  * `altsql`: Fire the query if there is no record selected in `sql`. Shown after `althead`.
  * `shead`: Static head - will always be shown (before `head`), independent of sql selects records or not.
  * `stail`: Static tail - will always be shown (after `tail`), independent of sql selects records or not.

Features
^^^^^^^^

* #2948 / altsql, shead, stail - new directives in Report.
* #4255 / Attachments fuer 'Email'. Static files can be attached to mails.

Bug Fixes
^^^^^^^^^

* #4980 / Variables in Report: a) nested not replaced, b) 'rbeg' not replaced, c) missing unit tests.


Version 0.25.3
--------------

Date: 19.11.2017

Notes
^^^^^

* Report:

  * Special column name 'sendmail': the old way of position dependent parameter are deprecated. Instead use the new
    defined token. See https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Manual.html#column_sendmail

  * Every row is now merged in STORE_RECORD. Inner SQL statement can now retrieve outer values via STORE_RECORD.
    E.g. `{{column:R}}`. No more level keys!

* The config.qfq.ini directive `VAR_ADD_BY_SQL` is replaced by `FILL_STORE_SYSTEM_BY_SQL_?`. Up to 3 statements are possible.

Features
^^^^^^^^

* Report / sendmail: control via token.
* #4967 / config.qfq.ini: Rename 'VAR_ADD_BY_SQL' to 'FILL_STORE_SYSTEM_BY_SQL_1'. Handle up to 3 FILL_STORE_SYSTEM_SQL_x.
  Implement an optional error message together with a full stop.
* #4766 / Set STORE_RECORD in Report per row.

Bug Fixes
^^^^^^^^^

* #4966 / Variable {{feUser:T}} is not available in config.qfq.ini `FILL_STORE_SYSTEM_?` - changed ordering of store
  initialization. Now: TCY...
* #4944 / Delete: broken when using 'tableName' (instead of form).
* #4904 / Undefined Index: DIRTY_FE_USER - PHP problem that constants cant be replaced inside of single ticks. Fixed.
* #4965: insert path to QFQ cookie/session, to make usage of multiple QFQ installation on one host possible.


Version 0.25.2
--------------

Date: 08.11.2017

Notes
^^^^^

* Starting with this release, the default escape mode is 'm' (mysql_real_escape).

Features
^^^^^^^^

* Default Escape Type changed from 's' to 'm'. DatabaseUpdateData.php: removed the DB update from last commit - not necessary.
  Config.php: New default 'm' Evaluate.php: Respect EscapeTypeDefault in form definition.
  QuickFormQuery.php: Replace 'EscapeTypeDefault' in form defintion very early.
* #4049 / QFQ Variables '{{...}}' might now contain a default value.
* If 'pageAlias:T' is empty, take 'pageId:T'.

Bug Fixes
^^^^^^^^^

* #4836 / Multiple entries in table after several clicks on save. Created a saveInProgress Variable.
* Replaced latest project homepage URL in Manual.rst.
* Fix example SQL for periodId in config.qfq.ini in Manual.rst.
* Remove multiple header 'RELEASE' - there has to be only one.

Version 0.25.1
--------------

Date: 03.11.2017

Bug Fixes
^^^^^^^^^

* #4857 / broken (stale) download: multiple 'u:..' or 'u:...'.
* #4212 / Broken JSON on response to save new record 'Unknown index' fixed by isset().

Version 0.25.0
--------------

Date: 10.10.2017

Notes
^^^^^

* The config.qfq.ini directives DB_USER, DB_NAME, DB_HOST, DB_PASSWORD are replaced by DB_1_USER, DB_1_NAME, DB_1_HOST,
  DB_1_PASSWORD. The old directives are still used, as long as the new directives does not exist.

* New config.qfq.ini directives: DB_INDEX_DATA, DB_INDEX_QFQ.

Features
^^^^^^^^
* #4720 / Separate database handles for QFQ 'form' and QFQ 'data' - 'Form' might  now load/save from forign database/host/user.


Version 0.24.0
--------------

Date: 09.10.2017

Notes
^^^^^

* Change Remove SYSTEM_SECURITY_ABSOLUTE_GET_MAX_LENGTH - makes no sense to hardcode an upper limit.

Features
^^^^^^^^

* Feature Manual.rst: Doc updated for latest subrecord column special names.
* Feature AbstractBuildForm.php: new function subrecordHead(). Replaced several hard coded subrecord column names against constants.
* Feature #4456 / formModeGlobal=requiredOff - update Manual.rst.
* Feature #4606 / _link: qualifier to render bootstrap button - fix unit tests for tooltip. Add tooltip to button/text,
  even if there is no link. Implement token  'b:...' for link class. Manual is updated. Open: `pageX` should be recoded
  to use the new 'b:' instead of hardcoed behaviour to render a button.
* Feature: Upload Button - wrapped with Bootstrap Button. New option 'fileButtonText' to specify a button text.
* Feature #3752 / Pills auf mode|modeSql=hidden|readonly setzen - implemented during 'form load' (not dynamic update).
* Feature: Neu wird nach dem Speichern das Formular nochmal komplett geladen. Das ist wichtig um die durch aftersave
  geaenderten Records in die Formularelemente zu bekommen.
* Feature #4511 / Form: URL Forward - mode dynamic computed - more generic implementation.

Bug Fixes
^^^^^^^^^

* Bug #4731 / Dynamic Update: load(post) triggers 'check required' - makes no sense during filling a form - fixed.
* Bug #4730 / InvalidDate-00-00-2000 FE.type=date - detection of empty date was broken for '00.00.0000'.
* Bug Fixed problem in subrecord when no record is selected.
* Bug #4620 / Easy Fix: saveButtonText / closeButtonText Formatierung.

Version 0.23.1
--------------

Date: 23.09.2017

Bug Fixes
^^^^^^^^^

* #4620 / Easy Fix: saveButtonText / closeButtonText Formatierung.


Version 0.23.0
--------------

Date: 17.09.2017

Features
^^^^^^^^

* #3752 / Pills auf mode|modeSql=hidden|readonly setzen - implemented during 'form load' (not dynamic update).

Bug Fixes
^^^^^^^^^

* #4548 /Template Group: 'form-update' broken - Broken Redirect after Save - Broken same HTML ID for FE copies in a
  template group.
* #4548 /Template Group: 'form-update' broken - max tg element value/index shown after save instead of last user
  supplied value, but save is ok. Neu wird nach dem Speichern das Formular nochmal komplett geladen. Das ist wichtig um
  die durch aftersave geaenderten Records in die Formularelemente zu bekommen.

Version 0.22
------------

Date: 14.09.2017

Notes
^^^^^

* Form Editor: element 'forwardPage' is static again (no dynamic update) - see features in #4511.

Features
^^^^^^^^

* #4511 / Form: URL Forward - mode dynamic computed.

Bug Fixes
^^^^^^^^^

* #4512 | SIP URL does not respect anker token '#'- fixed PLUS: L and type _GET Params included in links which contain a SIP (regular links still open).
* #4508 / Form: during Save with FE with 'report'-Note/Values an exception is thrown - report does not expect, to be called without typo3 - but this is the case during save and generating the JSON.
* #4021 / "required" asterik does not handle multi column labels correctly.
* #4423 / Date inputs with readonly: label is grey.
* Empty date might create '2001-00-00'.
* #4504 / Upload Button: required asterik missing after save - seems to be a problem for every element - should be fixed now.

Version 0.21.0
--------------

Date: 10.09.2017

Notes
^^^^^

* The Form-Editor now has two 'requiredParamter' fields: one for 'New' record and one 'Edit'. Existing settings will be
  automatically copied to both.
* The FormElement-Editor field 'Note' is not anymore a TinyMCE Editor. Instead a regular 'textarea' is used. Main reason
  are incompatibilities between TinyMCE HTML mode and the neede CR/LF linebreaks needed for 'Report' Syntax in the 'note'
  column.

Features
^^^^^^^^

* #4431 / FE.type=note: QFQ Report Syntax in 'FE.value' and 'FE.note'.
* #4456 / formModeGlobal=requiredOff - Switches FormElement.mode=required to 'show' for all FE of the current Form.
* #4356 / Form: required parameter - split between 'New' & 'Edit'.

Version 0.20.0
--------------

Changes
^^^^^^^

* New configuration value EXTRA_BUTTON_INFO_POSITION in config.qfq.ini.

Features
^^^^^^^^

* #4386 Fuer GRC: Optional Info Button bei 'input' wie bei 'textarea' - EXTRA_BUTTON_INFO_POSITION=below.
* #4429 / subrecord: new FE parameter 'subrecordTableCass' - a custom class for the subrecord table might be specified.
* #4428 / subrecord: mode=readonly.
* #4421 / subrecord: column of the sql1 row should go into the edit link - implemented.
* #4399 / Do not render '_pdf' when r:5 or empty string.

Bug Fixes
^^^^^^^^^

* #4396 / FE: Justify DATE and TIME in case it's DATETIME on a non primary table.
* #2414 / Deaktivieren von Option 'new' bei subrecord hat keine Folge.
* #4426 / Subrecord: mode=hidden - still shown.
* #4425 / Subrecords: Table head is not wrapped in <thead>.
* #4331 / SQL Statement 'REPLACE' not fired - Keyword missing in list of SQL Keywords.


Version 0.19.7
--------------

Changes
^^^^^^^

* #4306 / Update Text Subrecord: Please save this record first.

Features
^^^^^^^^

Bug Fixes
^^^^^^^^^

* #4278 / Language: Check that language settings are respectet inside of container / pill / fieldset / templateGroup.
* #4310 / Fixed error where custom values wouldn't be saved, nor not found for non pedantic.
* #4311 / Record Lock: expired lock wird nicht geloescht bei form reload.
* #4309 / typeahead: allow free entry.

Version 0.19.6
--------------

Features
^^^^^^^^

* #4299 / HTML Element 'Select': Placeholder.
* Changes to the alert generation and added btn-group for multiple buttons.
* Should only show reload button and be modal when the conflict is mandatory.
* #4144 / Close/New: bei acquireLock=false anschliessend keine Nachfrage ob gespeichert werden soll.
* #4120 / Removed Timeout from Dirty Alert Message.
* #4283 / FE.parameter=emptyMeansNull.

Bug Fixes
^^^^^^^^^

* #4281 Prevent save from being clicked multiple times. Save no turns orange when saving.

Version 0.19.5
--------------

Features
^^^^^^^^

* #3790 / Multilanguage: German/ English/ ...

Bug Fixes
^^^^^^^^^
* #4274 / ItemList: escape ',' ':'


Version 0.19.4
--------------

Features
^^^^^^^^

* Feature: Form Paste Records - skip columns during copy if they do not exist on the source side.

Bug Fixes
^^^^^^^^^

* #4266 / FormElement Type=Editor: value not saved - fixed.
* #4253 / Record Lock not deleted, when window closes without save.

Version 0.19.3
--------------

Changes
^^^^^^^

* Changing buttons for the dirty Events depending on status.

Bug Fixes
^^^^^^^^^

* #4257 / Dynamic update broken - after changing JSON data structure, update load.php has been missed.

Open
^^^^

* #4253 / Record Lock not deleted when window closes without save.

Version 0.19.2
--------------

Features
^^^^^^^^

* #4250 / autocron: sending mails.
* #4248 / FormElement: TypeAhead fuer den Spaltennamen - Implemented.
* #4144 / Close/New: bei acquireLock=false anschliessend keine Nachfrage ob gespeichert werden soll.
* #4120: Removed Timeout from Dirty Alert Message.

Version 0.19.1
--------------

Features
^^^^^^^^

* #4172 / record locking: Bob tries to delete a record and get 'status=error': Client should disable 'delete' button.
* #4185 / Detect modified record.
* #4143 / New alert removes old alert(s).
* #4173 / Form: User open's a new tab and press close - alert to inform user that he has to close the tab.
* #1930, #3980 /  Client: Bei Form Submit den Status 'submit_reason=save|save,close' mitsenden.
* Implemented: New > Close (save) now closes correctly the current page. Addtional, #1930 has been solved implizit.

Bug Fixes
^^^^^^^^^

* Bug #4174 / record locking: error message if delete fails due to record locking.
* Bug: SQL 'CREATE' implemented as a valid command.

Version 0.19.0
--------------

Changes
^^^^^^^

* bower.json: change bootstrap version number from micro to minor.
* Sip.php: Guarantee that uniqid() is unique at least for the current user.
* Makefile: change installation of phpDocumentor to --alldeps and remove 'phpdoc/'.

Features
^^^^^^^^

* #3881 / Variables: Ex 'keySemId', New 'periodId' (System Store).
* AbstractBuildForm.php: if a datetime / timestamp has the string 'CURRENT_TIMESTAMP' it will be replaced by the current date/time.
* Add new STORE_TYPO3 vars: pageAlias, pageTitle.
* Config.php: cleanup of checking GET variables.
* #3981 / Record Locking.
* Manual.rst: add documentation for record locking.
* Manual.rst: more details about QFQ variables.
* plantuml: sequence diagrams for record locking.

Bug Fixes
^^^^^^^^^

* Bug #4158 / Delete Button im Form fehlen die SIP Parameter.
* Bug #4159 / missing htmlspecialchar_decode() for FE.value supplied content.
* Bug Makefile: fixed unwanted removing of whole 'doc' by 'maintainer-clean' - doc nowadays contains QFQ related
  manually created documentation.
* Bug typeahead.php: An exception catched in typeahead.php has been assigned as array element, instead of a whole array. Fixed.




Version 0.18.7
--------------

Changes
^^^^^^^

* Makefile: 'make bootstrap' udpates all JS Lib packages. Double npm install removed.

Features
^^^^^^^^

* #3947 / Attack detectect: logout current user (only QFQ, FE User still logged in).
* #3959 / Class _link: implement 'any' Bootstrap glyphicons. New token 'G:' for '_link'.

Bug Fixes
^^^^^^^^^

* #3953 / Radio buttons: Auswahl nicht angezeigt, wenn per itemList definiert.
* #3982 / Filename Santize: remove spaces. Filename not properbly enclosed by double ticks.

Version 0.18.6
--------------

Features
^^^^^^^^

* #3460 / Report: new column types '_striptags, '_htmlentities', '_+Tag'.

Version 0.18.5
--------------

Features
^^^^^^^^

* QuickFormQuery.php: added function to check if there are copyForm paste records.
* Manual.rst, formEditor.sql: add several links in Form 'FormEditor' to the online documentation. The FormEditor now
  contains links to the Online Documentation. Add missing explanations: Required Parameter, Forward.

Bug Fixes
^^^^^^^^^

* #3912 / templateGroup: max. 5 instances are saved.
* Manual.rst: Fixed missing '{{' and '%' in examples.
* #3925 / templateGroup / non primary / delete records: only one at a time.
* Makefile: Artifactory builds fails at chromedriver - temporary remove npm install of chromedriver.

Version 0.18.4
--------------

Bug Fixes
^^^^^^^^^

* #3910 / 'submitButtonText' not shown.

Version 0.18.3b
---------------

Features
^^^^^^^^

* #3906 / Mark required inputs with an asterik.

Bug Fixes
^^^^^^^^^

* #3903 / Copy/Paste form: references inside a record are not updated at all.

Version 0.18.3a
---------------

Changes
^^^^^^^

* Copy / Paste form: take care that there is now Form.id=3. This is now the reserved formId for the 'copyForm'.

      UPDATE `FormElement` SET formId=100 WHERE formId=3
      UPDATE `Form` SET `id` = '100' WHERE `Form`.`id` = 3;

Bug Fixes
^^^^^^^^^

Version 0.18.3
--------------

Features
^^^^^^^^

* #3899 / Copy/Paste

  * DatabaseUpdate.php: New table Clipboard, New FE.type='paste', New Form.forwardMode='url-sip' - will be applied for 0.18.3.
  * Store.php: New member in STORE_CLIENT 'CLIENT_COOKIE_QFQ' - might be used to identify current user.
  * formEditor.sql: New form 'copyForm'. New table 'Clipboard'.


Version 0.18.2
--------------

Changes
^^^^^^^

* #3891 / Dynamic Update: Multiple Elements in a row - single show/hide - First implementation: Show / Hide via
  dynamicUpdate for a second element, wrapped in an own 'col-md' div.

Bug Fixes
^^^^^^^^^

* #3875 / FormElement 'extra': Fehler bei neuen Records.
* #3647 / Dynamic Update: Multiple Elements in a row not updated properly.
* #3890 / upload-FormElement: mode 'hide' without effect.
* #3876 / upload-FormElement: verschwindet bei dynamicUpdate.

Version 0.18.1
--------------

* Update unit tests

Version 0.18.0
--------------

Changes
^^^^^^^
*  New defaults to FormElement.type=`file` (=Upload).

* `access` = `application/pdf`
* `maxFileSize` = `10485760` (10MB)

Features
^^^^^^^^

* Make the image shown for ExtraButtonInfo configureable. Manual.rst: added documentation for new config.qfq.ini
  variables GFX_EXTRA_BUTTON_INFO_INLINE, GFX_EXTRA_BUTTON_INFO_BELOW.
* Manual.rst: description added for `extraButtonLock` ,`extraButtonPassword`, `extraButtonInfo`. Example on how to reuse
  custom vars defined in `config.qfq.ini`. Add three more examples to _pdf. How to access local online documentation.
  Add note about wkhtml only on linux boxes. More wkhtml explanations.
* #3773 / Button: Info / Unlock / ShowPassword: FE_PARAMETER: extraButtonLock / extraButtonPassword / extraButtopnInfo.
* #3769 / Allow specific GET variables longer than SECURITY_GET_MAX_LENGTH. config.php: implemented special handling of
  GET vars, named with '..._<num>'.
* #3766 / SQL_LOG per tt_content record einstellbar machen. Add `sqlLog` and `sqlLogMode` to QFQ tt-content records.
  Add mode 'error' and `none` to sqlLogMode. Manual.rst: Added explanations for SQL_LOG, SQL_LOG_MODE, and tt-content
  pendants sqlLog, sqlLogMode. Update config.qfq.ini to latest attributes.
* Config.php: Set defaults for config.qfq.ini SQL_LOG and SQL_LOG_MODE.
* config.qfq.example.ini: Remove DB_NAME_TEST, Add some details about SQL_LOG, add example for TECHNICAL_CONTACT.
* Session.php: Activate cookie_httponly for QFQ cookies.
* Html2Pdf.php:  recode setting of $this->logFile.
* config.qfq.ini: new syntax for SHOW_DEBUG_INFO - [yes,no,auto][,download].
* #3711 / Upload Button opens the camera on a smartphone. New attribute 'capture'.
* Database.php: Add SQL keyword 'DROP'.
* #3706 / Check File Upload: a) mime type, b) max file size. Implemented: file upload check for mime type and max file size.
* New 'pageForward'-Mode: 'url-skip-history'.
* FormEditor:

  * Most radio FEs changed from HTML standard to Bootstrap Design.
  * FE 'feIdContainer' will only be shown if there is at least one FE container element.
  * Use dynamicUpdate to hide/show FE 'forwardPage'.
  * FE 'subrecordOption' shown only for FE.type==subrecord.
  * FE 'checkPattern' shown only for FE.checkType=='pattern|min%'.
  * In form 'FormElement' the FE 'formId' removed - not necessary.

* #3568 / Form: fuer alle Buttons (save, close, new, delete) eine optionale class & text konfigurierbar.
* #3569 / Input Optional '0' unterdruecken.
* #3863 / New config var 'DB_UPDATE' in config.qfq.ini.

Bug Fixes
^^^^^^^^^

* #3770 / Attack Delay: merge processing to one codeplace. Config.php: new function attackDetectedExitNow(). Sip.php:
  replace local sleep(PENALTY_TIME_BROKEN_SIP) with central function attackDetectedExitNow().
* #3768 / log to sql.log: ip, formname, feuser.
* Store.php: Fix problem that an empty SQL_LOG will be added to SYSTEM_PATH_EXT. Logger.php: do nothing if there is no
  file defined.
* #3751 / download FE_GROUP protected pages failed. Implement additional 'SHOW_DEBUG_INFO = download' to track down
  problems with 'session forwarding'.
* Allow spaces in value when selection <radio> and <select> tags.
* #3812 / extraButtonInfo (extraButtonLock, extraButtonPassword) - Problems in TemplateGroups.
* #3832 / Dynamic Update und Radio buttons - leerer value ( '' ) kann nicht angewählt werden.
* #3612 / Konflikt typeAheadLdap mit dynamic modesql: inputs with typeahead lacks 'dynamicUpdate' via 'modeSql'.
* #3853 / New > Save: Reload des Forms mit neuer SIP und neu erstellter recordId.
* #3854 / Wrong final page: a) New > Save > Close, b) New > Save > Delete, c) New > New
  formEditor.sql: update table 'Form.forwardMode' to ('client', 'no', 'url', 'url-skip-history').
* #2337 / Checkbox: checked/unchecked parameters genügen nicht.
* #2542 / FormElement-Typ 'note' funktioniert nicht mit dynamic update.
* #3863 / DB Update Fails: Expected no record, got 2 rows: SHOW TABLE STATUS WHERE Name='Form'.

Version 0.17.0
--------------

Changes
^^^^^^^

* ALTER TABLE  `FormElement` ADD  `encode` ENUM(  'none',  'specialchar' ) NOT NULL DEFAULT  'specialchar' AFTER  `subrecordOption` ;
* UPDATE `FormElement` SET encode='none' WHERE class='native' AND type='editor'

* ALTER TABLE  `Form` ADD  `escapeTypeDefault` ENUM(  '',  's',  'd',  'l',  'L',  'm',  '-' ) NOT NULL DEFAULT  '' AFTER  `permitEdit` ;

* In order to not break functionality of existing forms, it might be necessary (bad for security, good for stability) to
  leave existing forms untouched: `UPDATE Form SET escapeTypeDefault='-'`

* Play formEditor.sql

Features
^^^^^^^^

* New security option `escapeTypeDefault`: will be defined 1) sytem wide in config.qfq.ini, or 2) more specific per
  Form or 3) individually per variable. The later has priority.
* #3544 / Form: view current form - It's now possible to direct view a form, which is currently loaded/edited in the
  FormEditor: Button 'eye' near left of button 'save'.
* #3552 / typeAheadLdapSearchPerToken - webpass kann nicht gleichzeitig nach Vornamen und Nachnamen suchen. Added option
  typeAheadLdapSearchPerToken to split search value in token and OR-combine every search with the individual tokens.
* Download latest QFQ builds and releases: https://w3.math.uzh.ch/qfq/.
* #3218, #3600 / download.php / export: QFQ is now able to create PDFs and ZIPs on the fly. The sources might be
  uploaded PDFs or Websites (local or remote) which will be converted to PDFs.
* Implement 'encode=specialchar' - new option per FormElement which is now the default for every FormElement.
* Santize.php: New function urlDecodeArr(). Decode all _GET vars.
* Implemented max GET parameter lenght. Default: 50.
* Implemented new escape class 'mysql' (realEscapeString).
* LICENSE.txt: Add GPLv3.
* Html2Pdf.php: Add SIP support wkhtmltopdf URLs. Move cookies for wkhtmltopdf from commandline arguments to filebased.
* SessionCookie.php: New class to save current cookies in a file.
* Html2Pdf.php: implemented session forwarding to wkhtmltopdf.
* Session.php: introduced close(). This will unlock the current session. Take care on subsequent calls to reopen primary session again.
* Database.php: Set charset to real_escape_string() functions properly. Proxy for mysqli::real_escape_string().
* Implement honeypot variables to detect bots.
* HTML special char encode all URL GET parameter. This can't be skipped.

Bug Fixes
^^^^^^^^^

* Sip.php: Parameter XDEBUG_SESSUIB_START excluded from GET parameter copied to SIP.
* Manual.rst: add libxrender1 to install by using wkhtmltopdf.
* Download.php: Skip 'pdftk' if there is only one PDF file to concatenate.
* #3615 / download.php: Das Popup schliesst nicht automatisch bei ZIP, im FF, Warnung in der Console, hourglass wobbles.
* Split PHP 'print.php' in a pure API file 'print.php' and a class 'Html2Pdf.php' - the class will be reused by Download.php.
* #3573 / TypeaheadLdap: Prefetch funktioniert nicht.
* #3547 / FE of type 'note' causes writing of empty fields.
* #3546 / Throw of a UserFormException with wrong parameter. Fixed.
* #3545 / Errormessages via API/JSON not displayed.
* #3536 / a) Datum (datetime / timestamp) werden nicht angezeigt, b) Angezeigte Datumsformat String und aktzeptierte Eingabe matchen nicht.
* #3533 / afterSave: sqlUpdate auf child-record ändert xId von Hauptrecord auf 0.

Version 0.16
------------

Changes
^^^^^^^

* Play:

  ALTER TABLE `FormElement` ADD INDEX `feIdContainer` (`feIdContainer`);
  ALTER TABLE `FormElement` ADD INDEX `ord` (`ord`);
  ALTER TABLE `FormElement` ADD INDEX `feGroup` (`feGroup`);

  ALTER TABLE `FormElement` ADD `adminNote` TEXT NOT NULL AFTER `note`;

* Play formEditor.sql

Features
^^^^^^^^

* formEditor.sql:

  * Added 'on update current timestamp'.
  * Add three indexes to formEditor.sql.
  * Column FormElement.adminNote added.
  * Change default width on Form for subrecord FormElement.name.
  * Changed input height of 'parameter of FormEditor and FormElementEditor to 8 lines.

* Enlarge placeholder value in FormElement. Old 512, New 2048..
* #3466 / Input Typeahead: optional only allow specified input. Mode: typeAheadPedantic.
* #3465 / Save button: optional 'active after form load': `Form.parameter.saveButtonActive` - if this attribute is set,
  the save button will be enabled directly on form load.
* #3463 / form.mode=readonly. Make a form complete `readonly`. This can be done statically or dynamically via variable (e.g. SIP).
* #3447 / Icons das man im FrontEnd direkt das gewaehlte FormElement im Formulareditor bearbeiten kann. Add checkbox left.
  to the 'EditForm'-Button, to toogle the 'FormElemnt'-Icons. Like the  'Form Edit'-Pencil, the 'FormElement Checkbox'
  is only displayed if the user is logged in BE.
* #3456 / LDAP: with Credentials (e.g. to access 'webpass'). Updated doc for a) config.qfq.ini: LDAP_1_RDN, LDAP_1_PASSWORD,
  b) Form.parameter|FormElement.parameter: ldapUseBindCredentials.

  * ErrorHandler.php: removed details - the end user should not too many details.
  * FormAction.php, Ldap.php, QuickFormQuery.php: implement 'ldapUseBindCredentials'.
  * Ldap.php: set_error_handler() to catch ldap_bind() problems. Always set LDAP_OPT_PROTOCOL_VERSION=3 - this might
    cause problems with som LDAP Servers - we will see.


Bug Fixes
^^^^^^^^^

* 3509 / SELECT Element: value wird nicht selektiert.
* 3502 / TemplateGroups: Checkboxen werden beim ersten Speichern (insert) nicht geschrieben - ein anschliessendes Update
  ist ok.
* 3385 / templateGroup: insert/update/delete non primary records.

  * Non primary record leads to a problem that the default values, given as an array, are not replaced by scalar values. fixed.
  * Update doc how to insert/update/delete non primary templateGroup records.
  * Removed $templateGroupIndex - solved implicit by defining a LIMIT on 'slaveId' . Implemented '%D' (one below %d).
    Implemented FE_SQL_HONOR_FORM_ELEMENTS - reduces unecassary SQL queries.
  * Fill STORE_RECORD during Formload - to read templateGroup records very early. Local copy of `getNativeFormElements()`,
    new `explodeTemplateGroupElements()`

* TypeAhead.js: Handle <ENTER> key properly.
* #3462 / FormElement.parameter: requiredList not ok for non numeric content. STORE_FORM had been called without 'santize class'.
  Therefore, all non numeric values has been sanitized by default. New: SANITIZE_ALLOW_ALL.
* Corrected error message to use 'itemList' instead of 'itemValues'. Renamed constant too.
* #2542 / FormElement-Typ 'note' funktioniert nicht mit dynamic update. 'Label' and 'note' are fixed - 'value' is still not updated, open.

Version 0.15
------------

Changes
^^^^^^^

* Play formEditor.sql.

* Form 'FormElement' failed to display the formtitle of the current form in case of a new FE.
* Updated subrecord in 'Form' for 'FormElements' - columns 'size' and 'sql1' removed and 'dyn' inserted. Play formEditor.sql.

*  #3431, Parameter keyword 'typeAheadLdapKeyPrintf' changed to 'typeAheadLdapIdPrintf'.::

   UPDATE FormElement SET parameter = REPLACE(parameter, 'typeAheadLdapKeyPrintf', 'typeAheadLdapIdPrintf')

* Size 'placeholder' increased::

  ALTER TABLE  `FormElement` CHANGE  `placeholder`  `placeholder` VARCHAR( 2048 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  '';

Features
^^^^^^^^

* Introduce new config.qfq.ini setting 'EDIT_FORM_PAGE' - by default set to 'form' - the wrench symbol on every Form
  will direct to that page. Fix #3420 / Quicklink 'editform' on form: directs to the current T3 page which might be insufficient.
* Form 'subrecord' columns: the default width limit of 20 chars are disabled if 'nostrip' is specifed.
* #3431 / typeAheadSql: columnname 'key' is a reserverd SQL statement - replace by 'id'. Additional the parametername
  'typeAheadLdapKeyPrintf' renamed to 'typeAheadLdapIdPrintf'. By using 'id' instead of 'key' the quoting of the columnname
  is not necessary anoymore.



Bug Fixes
^^^^^^^^^

* #3419 / typeAheadSql: Array with only one column or non standard columnnames are not handeld properbly.
  Detection of missing LIMIT implemented.
* #3425 / Form.parameter, FormElement.parameter: comment handling, trailing & leading spaces
  Manual.rst: commented handling of 'comment character' and 'escaping of leading/trailing spaces'
  Support.php: new funtion handleEscapeSpaceComment().
* Evaluate.php: parse all F|FE.parameter via handleEscapeSpaceComment(). A leading '#' or ' ' might be escaped by '\'.
* Saving 'extra' FE in STORE_SIP has been done with inappropiate FE_NAME. Correct is the pure FE_NAME, without any
  extension like recordId. Unessary and broken decoding removed.
* #3426 / Dynamic Update: Inputs loose the new content and shows the old value.
* Through fix #2064 the FE.checkType has not been used anymore. This is fixed now.
* #3433 / templateGroup on primary Record: Values of removed copies are not deleted. The new implementation creates empty
  fake instances of all copies of templateGroup FormElements. Those are empty. Before save, the submitted form values
  will be expanded with the empty fake templateGroup FormElements and such empty values will be saved.


Version 0.14
------------

Changes
^^^^^^^

* Play formEditor.sql.

* All Form & FormEditor input elements now have a maxlength definition of 0, which means take the column definition value.
* Drop-down list of container assignment:

* Display 'type' ('pill', 'fieldset', 'templategroup') instead of 'class' (always 'container').
* Display 'name' (internal name) instead of 'label' (shown on the website and might not so usefull as 'name' which
  is nowhere else used than in that drop-down.

* FormElement.placeholder colum width extended to 512:

         ALTER TABLE `FormElement` CHANGE `placeholder` `placeholder` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''

* New class Ldap.php.

Features
^^^^^^^^

* Typeahead for SQL and LDAP Datasources implemented.
* formEditor.sql: Changed width of column FormElement.placeholder from 255 to 512. Removed hardcoded 'size' in FormElement 'placeholder'.
* Character Count: Display a `counter` on input or textarea fields, activated by specifying the formElement.parameter 'characterCountWrap'.
* Evaluate.php: Two new escape options 'l' and 'L'. Backport of ldap_escape() for PHP <5.6. Multiple escaping for one value now possible.
* Manual.rst: add some example for TypeAhead and for saving LDAP value.
* Load foreign values in templatGroups - saving is not implemented yet.
* Manual: Added howto prevent <p>-wrap in TinyMCE.
* TemplateGroup: Add button now disabled if max. number of copies reached.
* #3414 / QuickFormQuery.php: wrap whole form in 'col-md-XX' - User controls the width of an QFQ form.

Bug Fixes
^^^^^^^^^

* Dynamic Update has been broken since implementing of 'element-update' (#3180). Now both methods, 'element-update' and 'form-update' should be fine.
* qfq-bs.css.less: Fixed problem with 'typeahead input elements' not expanded to Bootstrap column width. Changed
  Layout/Design Typeahead drop-down box. Add hoover for the drop-down box with a blue background.
* AbstractBuildForm.php: #3374 - textarea elements now contains 'maxlength' attribute.
* BuildFormBootstrap.php: wrapping of optional 'submitButtonText' now done with the 'per form' values.
* typeahead.php: if there is an exception, the message body is sent as regular 'content' for the drop-down box. At the
  moment this is the only way to transmit any error messages.
* formEditor.sql: removed all 'maxLength' string values for 'Form' and 'FormElement' forms.
* Save button becomes active if a templateGroup copy is removed.
* #3413 / Form ohne Pill hat kein padding am Rand. Fix: if there are no pills, an additinal col-md-12 will be rendered.


Version 0.13
------------

Changes
^^^^^^^

* Play formEditor.sql.
* formEditor.sql:

  * Checktype of `Form.name` restricted to `alnumx` (prior `all`).
  * Changed `access` for Form `form` & '`ormElement` from `always` to `sip`.

* Table `FormElement`:

  * Modified column: `checkType` - new value `numerical`.

    ALTER TABLE FormElement MODIFY COLUMN checkType ENUM('alnumx','digit','numerical','email','min|max','min|max date',
    'pattern','allbut','all') NOT NULL DEFAULT 'alnumx'

* Example Report for `forms` extended by a delete button per row.

Features
^^^^^^^^

* print.php: offers 'print page' for any local page - create a PDF on the fly (printout is then browser independent).

  * Install `wkhtmltopdf` on the webserver (http://wkhtmltopdf.org/).
  * In config.qfq.ini setup:

        BASE_URL_PRINT=http://www.../
        WKHTMLTOPDF=/opt/wkhtmltox/bin/wkhtmltopdf

* Check and error report if 'php_intl' is missing.
* New Checktype 'allow numerical'.
* Documentation: example for 'radio' with no pre selection.
* #3063 / Radios and checkboxes optional rendered in Bootstrap layout.
* Added 'help-box with-errors'-DIV after radios and checkboxes.
* Respect attribute `data-class-on-change` on save buttons.


Bug Fixes
^^^^^^^^^

* #2138 / digit sanitize: new class 'numerical' implemented.
* Fixed recursive thrown exception.
* #2064 / search of a default value for a non existing tablecolumn returns 'false'.

  * Fixed setting of STORE_SYSTEM / showDebugInfo during API call.

* #2081, #3180 / Form: Label & note - update via `DynamicUpdate`.
* #3253 / if there is no STORE_TYPO3 (calls through .../api/ like save, delete, load): use SIP / CLIENT_TYPO3VARS.
* qfq-bs.css:

  * Alignment of checkboxes and radios optimized.
  * CSS class 'qfq-note' for 'notes' (third column in a form).


Version 0.12
------------

Changes
^^^^^^^

* Table 'FormElement'
  * New column: rowLabelInputNote:

    ALTER TABLE  `FormElement` ADD `rowLabelInputNote` set('row','label','/label','input','/input','note','/note','/row')
    NOT NULL DEFAULT 'row,label,/label,input,/input,note,/note,/row' AFTER  `bsNoteColumns` ;

  * Modified column: 'type' - new value 'templateGroup':

    ALTER TABLE  `FormElement` CHANGE  `type`  `type` ENUM(  'checkbox',  'date',  'datetime',  'dateJQW',  'datetimeJQW',  'extra',
    'gridJQW',  'text',  'editor',  'time',  'note',  'password',  'radio',  'select',  'subrecord',  'upload',  'fieldset', 'pill',
    'templateGroup',  'beforeLoad',  'beforeSave',  'beforeInsert',  'beforeUpdate',  'beforeDelete',  'afterLoad',  'afterSave',
    'afterInsert',  'afterUpdate',  'afterDelete',  'sendMail' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'text'

* formEditor.sql: Added HTML 'placeholder' in FormEditor for bs*Columns.

  * PLAY 'formEditor.sql'.

* User Input will be UTF8 normalized.

  * INSTALL 'php5-intl' or 'php7.0-intl' on Webserver.

* Add globalize.js to be included. Needed by jqx-all.js.

  * UPDATE EXISTING TypoScript TEMPLATES of QFQ Installation.

* Name of variable '_filename' (used in field 'parameter') has changed. Old: '_filename', New: 'filename'.

  * UPDATE `FormElement` SET parameter = REPLACE(parameter, "_filename", "filename")


Features
^^^^^^^^

* User input will be UTF8 normalized.
* config.qfq-ini:

  * New configuration values: FORM_BS_LABEL_COLUMNS / FORM_BS_INPUT_COLUMNS / FORM_BS_NOTE_COLUMNS.
  * Comment empty variables - the new default setting is, that empty parameter in config.qfq.ini means EMPTY
    (=parameter is set and will not be overwritten by internal default), not UNDEFINED (overwritten by internal default).

* FileUpload:

  * Implemented new Formelement.parameter: fileReplace=always - will replace existing files.
  * Multiple / Advanced Upload: new logic implements slaveId, sqlInsert, sqlUpdate, sqlDelete.

* FormElement.parameter: sqlBefore / sqlAfter fired during 'Form' save for action elements.
* STORE FORM: variable 'filename' moved to STORE VAR - sanitize class needs no longer specified.
* STORE VAR: two new variables 'filename' and 'fileDestination' valid during processing of current upload FormElement.
* Default store priority list changed. Old: 'FSRD', New: 'FSRVD'.
* CODING.md: update doc for FormElement 'upload' and general 'Form' rendering & save (recursive rendering).
* User manual:

  * Described form layout options: description for bsLabelColumn, bsInputColumn, bsNoteColumn.
  * Update 'file-upload' doc.
  * Described 3 examples for upload forms.

* Administrator manual:

  * Add description page.meta...

* New FormElement (type= 'container') added: 'templateGroup'.

  * FormElement.parameter.tgAddClass | tgAddText | tgRemoveClass | tgRemoveText | tgClass.
  * FormElement.maxSize: max number of duplicates.
  * #3230 / templateGroup: margin between copies. 'tgClass' implemented.

* Native FormElements:

  * FormElement.parameter.htlmlBefore|htmlAfter - add the specified HTML code before or after the element (outside of any wrapping).
  * #3224, #3231 / Html Tag <hr> als FormElement. >> htmlBefore | htmlAfter.
  * FormElement.parameter.wrapLabel | wrapInput | wrapAfter | wrapRow - if specified, any default wrapping is omitted.
  * FormElement.bsNoteColumns | bsInputColumns | bsNoteColumns - a '0' will suppress the whole rendering of the item.
  * FormElement.rowLabelInputNote - switch on/off rendering of the corresponding system wrapping items.

* #3232 / Define custom 'on-change' color - used for the save button: Form.parameter.buttonOnChangeClass=...
* Form.parameter & FormElement.parameter: Lines starting with '#' are treated as comments and will not be parsed.

Bug fixes
^^^^^^^^^

* User manual:

  * Fixed double include of validator.js in T3 Typoscript template example.
  * Fixed wrong store name SYSTEM: S > Y.
  * Fixed wrong STORE_FORM variable names.
  * Reformat FormElement.parameter description.
  * Styling errors fixed.

* Use of 'decryptCurlyBraces()' to get better error messages.
* Skip unwanted parameter expansion during save.
* Fixed bug with uninitialized FE_SLAVE_ID.
* formEditor.sql:
  * The defintion as 'editor' (not text) for FormElement 'note' has been lost - reinserted.
  * Fixed problem while playing SQL query - deleting old FormElements of Formeditor deleted also FormElements of other forms.
* #3066 / help-text with-error - CSS class 'hidden' will be rendered by default (as long there is no error).
* Labels are skipped, if FormElement.bsLabelColumns=0.
* Respect attribute `data-class-on-change` on save buttons.

Version 0.11
------------

Features
^^^^^^^^

* Added STORE_BEFORE, #3146 - Mainly used to compare old and new values during a form 'save' action.
* Added 'best practice' for defining and using of 'Central configure values' in UserManual.
* Added accent characters to sanitize class 'alnumx', #3183.
* Set default all QFQ send mails to 'auto-submit'.
* Added possibility to customize error messages ('data-pattern-error', 'data-rquired-error', 'data-match-error',
  'data-error') if validation fails. Customization can be done on global level (config.qfq.ini), per Form or per FormElement.
* *FormElement*: Double an input element and validate that the input match: FormElement.parameter.retype=1.
* Autofocus in Forms is now supported. By default the first Input Element receives the focus. Can be customized.
* Added a timestamp in shown exceptions. Usefull for screenshots, send by customer, to find the problem in SQL logfiles.

Bug fixes
^^^^^^^^^

* Fixed missing docutmentation for FormElement 'note'.
* Failed SQL queries will now always be logged, even if they do not modify some data.

Version 0.10
------------

Features
^^^^^^^^

* Implemented Parameter 'extraDeleteForm' for 'forms' and 'subrecords'. Update doc.

Bug fixes
^^^^^^^^^

* Suppress rendering of form title during a 'delete' call. No one will see it and required parameters are not supplied.
* In case of broken SQL queries, print them in ajax error message.
* Remove parameter 'table' from Delete SIP URLs. ToolTip updated.

Version 0.9
-----------

Features
^^^^^^^^

* FormEditor:
  * design update - new default background color: grey.
  * per form configureable background colors.
  * Optional right align of all form element labels.
  * Added config.qfq.ini values CSS_CLASS_QFQ_FORM_PILL, CSS_CLASS_QFQ_FORM_BODY, CSS_CLASS_QFQ_CONTAINER.

Bug fixes
^^^^^^^^^

* BuildFormBootstrap.php: added new class name 'qfq-label' to form labels - needed to assign 'qfq-form-right' class.
  Changed wrapping of formelements from 'col-md-8' (wrong) to 'col-md-12'.
* QuickFormQuery.php: Set default for new F_CLASS_PILL & F_CLASS_BODY.
* formEditor.sql: New default background color for formElements is blue.
* qfq-bs.css.less: add classes qfq-form-pill, qfq-form-body, form-group (center), qfq-color-..., qfq-form-right.
* Index.rst: Add note to hierachy chars. Fixed uncomplete doc to a) bs*Columns, showButton. Add classPill, classBody.
  Rewrote form.paramter.class.
* QuickFormQuery.php: Button save/ close/ delete/ new - align to right border of form.
* UsersManual/index.rst: renamed chapter for formelements. Cleanup formelement types. Wrote chapter 'Detailed concept'.
* QuickFormQuery.php, FormAction.php: '#2931 / afterSave Hauptrecord xId nicht direkt verfügbar' - load master record
  again, after 'action'-elements has been processed.
* UsersManual/index.rst: Startet FAQ section.
* config.qfq.example.ini: Added comment where to save config.qfq.ini.
* UsersManual/index.rst: Rewrite of 'action'-FormElement definition.
* #2739 / beforeDelete / afterDelete.
* PROTOCOL.md: update 'delete' description.
* delete.php: fixed unwanted loose of MSG_CONTENT.
* Report.php: Fixed double '&&' in building UrlParam.
* FormAction.php: In case of 'AFTER_DELETE', do not try to load primary record - that one is already deleted.
* Sip.php: Do not skip SIP_TARGET_URL as parameter for the SIP.
* #3001 / Report: delete implementieren.
* Index.rst, Constants.php: reverted parameter '_table' in delete links back to 'table' - Reason: 'form' needs to be
  'form' (instead of '_form') due to many used places already.
* Sip.php: move SIP_TARGET_URL back to stored inside SIP - it's necessary for 'delete'-links.
* Report.php, Constants.php: Remove code to handle unecessary 'p:' tag for delete links.
* Link.php: Check paged / Paged that the parameter r, table and form are given in the right combination.
* Link.php, Report.php: New '_link' token 'x'. '_paged' and '_Paged' are rendered via Link() class, Link() class now
  supports delete links.
* QuickFormQuery.php: for modeForm='Form Delete' the 'required param' are not respected - this makes sense, cause these
  parameters typically filled in newly created records.
* #3076 / Delete Button bei Subrecords erzeugt sporadisch Javascript Exceptions (Webkit: Chrome / Vivaldi) - kein loeschen moeglich.
