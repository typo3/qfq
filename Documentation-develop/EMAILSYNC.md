<!-- -*- markdown -*- -->

# Email Sync (IMAP)

* https://www.php-imap.com/

## Features

* Copy emails from an IMAP Mailaccount to a DB table ES_TABLENAME ('EmailSync').
* Move emails from one folder to another folder (clean inbox).
* Mark emails as read / unread

## Technical

* fetch
  * IMAP::FT_PEEK - messages remain unread
  * IMAP::FT_UID - every 'read' message is flagged as 'read'.
* sequence
  * IMAP::ST_MSGN - communication is based on the message number
  * IMAP::ST_UID - communication is based the the uid.
* fetch_body
  * true|false - Disabling this can greatly increase the overall performance.

## Questions

* https://www.php-imap.com/configuration/advanced
  * Welchen Sinn macht das Array `common_folders`

## Hints

* TOC https://www.php-imap.com/table-of-contents
* Check wie man auf O365 zugreift: https://www.php-imap.com/examples/oauth
* Client: https://www.php-imap.com/api/client - connect/close/reopen/check, getFolders,
  createFolder, checkFolder (number of messages, the next uid and a list off all available flags),
* API / Query: https://www.php-imap.com/api/query - getMessageByUid, all (='getAll'), markAsRead,
  setFetchBody(false), $query->all()->count();
* API / Message: https://www.php-imap.com/api/query - hasTextBody(), hasHTMLBody(), getTextBody(),
  getHTMLBody(), getBodies() (text & HTML), $message->copy(), $message->move(), $message->delete(),
  $message->getFlags(), $message->setFlag(['Seen', 'Flagged']), $message->unsetFlag('Flagged'),
  $message->hasAttachments(), $message->getAttachments(), $message->getAttributes()
* API / Attachment: https://www.php-imap.com/api/attachment - $attachment->save(),
  $attachment->getAttributes()
* API / Events: https://www.php-imap.com/api/event

## Notes

* A mode to copy folder structure into DB
  * Marc specific folder as 'subject|body|attachment'
* Define is folder structure should be monitored (folder rename, add, delete)
* There is an idle mode (https://www.php-imap.com/api/folder)

## Table

| Column     | Description                                 |
|------------|---------------------------------------------|
| account    | Account name wie in der Config angegeben    | 
| sender     |                                             |
| receiver   |                                             |
| cc         |                                             |
| subject    |                                             |
| bodyText   |                                             |
| bodyHtml   |                                             |
| attachment | JSON inkl. Metadaten (Size, Filename, Type) |
| header     | JSON (key/value)                            |
| flags      |                                             |
| folder     | Folder (incl Path)                          |
