Thumbnail
=========

General
=======

* Thumbnails are created by special column name '_thumbnail'. 
* The given parameter controls size and secure/public access mode.
* The thumbnails are stored in a public available directory or in a access protected directory. Both directories are 
  central configured in config.qfq.ini. 
* The column '_thumbnail' checks if the thumbnail already exist or if it has to be created first.  
* Cleaning the thumbnail directory is fine at any time. The next page reload will recreate any needed thumbs.   
* The thumbnail filename is a MD5 hash of the original pathFilename. To detect any modified source, the timestamp of source
  has to be older than of the thumbnail.
* SVG files will be rendered by 'inkscape'.
* All other file formats are rendered by GraphicsMagick 'convert'.  
* The rendering process starts the rendering in the background. Therefore QFQ returns quickly, even for several new thumbnails.
* Secured thumbnails will detect if there is a rendering process pending: before the rendering starts, the thumbnail will 
  be created as an empty file. Public thumbnails are not protected. The client has to reload the page.
   
Secured by SIP
--------------

Thumbnails might contain sensitive data and needs to be protected by SIP. 

QFQ / report generates a `<img src="api/download.php?s=...">` HTML tag. 

If a thumbnail file is empty, `api/download.php` will wait up to ten seconds and than returning a 404.

Public
------

Saved in a public readable directory, referenced like `<img src="<public dir>/<hash>.png">` 

How to use thumbnails
=====================

Report column '_thumbnail'
--------------------------

Secure: `SELECT 'T:<pathFilename>|[W:[<width>][x<Height>]]' AS _thumbnail`  >> <img src="api/thumbnail.php?s=badcaffee1234">
Public: `SELECT 's:0|T:<pathFilename>|[W:[<width>][x<Height>]]' AS _thumbnail`  >> <img src="<public dir>/<hash>.png">
  
Report column '_link'
---------------------

Part of a '_link' definition. Secure or Public access is equal to the link itself.

Form
----

Inside of a form with the new feature #5422 {{COLUMN '...' AS _thumbnail}}  


Workflow
--------

Report creates a SIP:
 