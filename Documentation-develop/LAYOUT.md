Plain
=====

<form>

    # Element 1
    <p>Title</p>
    <p><input type="input"></p>
    
    # Element n
    <p>Name</p>
    <p><input type="input"></p>

</form>

# Subrecord

<table> 
    <tr><th> id </th> </tr>
    <tr><td> 1 </td></tr>
    <tr><td> 2 </td></tr>
</table>

Table
=====

<form>
    <table>

    <tr>
        # Element 1
        <td>Title</td> <td><input type="input"></td> <td>note</td>
    </tr>
    
    <tr>
        # Element 2
        <td>Name</td> <td><input type="input"></td> <td>note</td>
    </tr>

    <tr>
        # Fieldset
        <td colspan=3>
            <table>
                <tr>
                    # Element 3
                    <td>Name</td> <td><input type="input"></td> <td>note</td>
                </tr>
            </table>
        </td>
    </tr>
    </table>

</form>

Subrecord
=========

<table> 
    <tr><th> id </th> </tr>
    <tr><td> 1 </td></tr>
    <tr><td> 2 </td></tr>
</table>



Bootstrap
=========

<div class="container-fluid">
    # Ttitle
    <div class="row hidden-xs">
        <div class="col-md-12">
            <h1>Title with a long text</h1>
        </div>
    </div>

    # Pills & Button
    <div class="row">
        <div class="col-md-10">
            <ul id="myTabs" class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="#person" data-toggle="tab">Person</a></li>
                <li role="presentation"><a href="#person2" data-toggle="tab">Person2</a></li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                        more <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#arbeitsgruppe" data-toggle="tab">Arbeitsgruppe</a></li>
                        <li><a href="#publikation" data-toggle="tab">Publikation</a></li>
                    </ul> # Dropdown-menu
                </li> # Dropdown item
            </ul> # pill
        </div> # class="col-md-10"
        <div class="col-md-2 ">
            # button
        </div> # Button
    </div> #  class="row" (Pill & Button)
       
    # Form
    <form class="form-horizontal"><div class="tab-content">
        # Pill 1
        <div role="tabpanel" class="tab-pane active" id="person">
        
            <div class="form-group">
                <div class="col-md-2">
                    <label for="id" class="control-label">Name</label>
                </div>
                <div class="col-md-6 ">
                    <input id="name" type="text" class="form-control">
                </div>
                <div class="col-md-4">
                    <p class="help-block ">Abgekürzter Vorname. Für "Christian" z.B. "Ch."</p>
                </div>
            </div> # class="form-group"
        
            <div class="form-group">
                <div class="col-md-2">
                    <label for="id" class="control-label">Firstname</label>
                </div>
                <div class="col-md-6 ">
                    <input id="firstname" type="text" class="form-control">
                </div>
                <div class="col-md-4">
                    <p class="help-block ">Please write the complete firstname</p>
                </div>
            </div> # class="form-group"
        
        </div> # class="tab-pane" Pill 1
        
        # Pill 2
        <div role="tabpanel" class="tab-pane active" id="person">

            <div class="form-group">
                <div class="col-md-2">
                    <label for="id" class="control-label">Name</label>
                </div>
                <div class="col-md-6 ">
                    <input id="name" type="text" class="form-control">
                </div>
                <div class="col-md-4">
                    <p class="help-block ">Abgekürzter Vorname. Für "Christian" z.B. "Ch."</p>
                </div>
            </div> # class="form-group"

            <div class="form-group">
                <div class="col-md-8">
                    <fieldset>
                        <div class="form-group">
                            <div class="col-md-2"> ...
                            <div class="col-md-6"> ...
                            <div class="col-md-4"> ...
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <p class="help-block ">Abgekürzter Vorname. Für "Christian" z.B. "Ch."</p>
                </div>
            </div> # class="form-group"

            # fieldset: nested
            <div class="tab-content">
                
            </div> # class="tab-content"
        
        </div> # class="tab-pane" Pill 2
        
        
    </div> # class="tab-content"    </form>
    
</div> # class="container-fluid"






