Design / Notes for fabric.js
==========================================

General
-------

* graphical annotation of PNG, SVG,.... 
* For the first version, no fullscreen

Concept
-------

* The fabric element:
** is implemented as a form element
** is wrapped in col-md
* Server may send all the user images as inline/hidden HTML element with an HTML-ID.
* The images will be given to fabric using a json element as seen here::

````
<html>
...
<div class="annotate-graphic" data-images='{"images": [{"page": 1, "selector": "qfq-fabric-image-1", "refId": "image.id"}, {"page":2, "selector": "#qfq..."}]}'> 
...
</html>
````

* id Selector without #
* Form Submit is a regular Save.
* Per Exercise exists one Form Element which contain the Fabric Drawing JSON Data.

Design / Notes for Code Annotation
==================================

General
-------

* Highlighting Code Syntax
* Write comments to code lines

Concept
-------

* Use a form Element
* Hidden field, value with annotation json:

````
{ 
    "annotations": [
                { "lineNumber": ##,
                  "comments": [
                    { "uid": ##,
                      "comment": "Text / HTML Blob",
                      "dateTime": "Formatted Date"
                    }]
                }, { "lineNumber": ###,
                     "comments": [
                      { "uid": ##,
                        "comment": "2 Comment for one line, first",
                        "dateTime": "2018-05-12 09:45:00"
                    },
                    { "uid": ##,
                        "comment": "second",
                        "dateTime": "0000-00-00 00:00:00"
                    }]
            }
    ],
    "users": [
        { "uid": ##,
        "name": "Displayed Username",
        "avatar": "Gravatar, Standard Picture, Photo"
        }
    ]
}
````
* Target for rendering, div with class="annotate-text"
  * data-target: Referencing hidden input form from above
  * data-file: Script or Textfile to be read and displayed
  * data-uid: JSON of logged in user, like:
    ````
    {
        uid: 5, 
        name: "Definitely not an AI", 
        avatar: "mockData/avatar1.png"
    }
    ````
  * data-highlight: Name of the language, that needs highlighting, if required