QFQ Update
==========

QFQ saves the current installed QFQ version number in table `Form` in the comment field.
(`SHOW TABLE STATUS WHERE Name='Form'` >> column 'Comment')

This makes sense: If a QFQ database is copied to different installation, maybe there is a different QFQ Version
installed, than those tables/functions should be updated. If the version number would be taken from a file - such files
are typically not restored during a backup - the mismatch can't be automatically detected.

Database Schema: QFQ version number.
Stored Procedures: Hash over all functions.

DB schema & stored procedures
-----------------------------

* QFQ-Base: `extension/Classes/Sql/qfqDefaultTables.sql`
* QFQ-Optional: `extension/Classes/Sql/*.sql`
* QFQ-Functions: `extension/Classes/Sql/function.sql`
* Incremental-Updates: `extension/Classes/Core/Database/DatabaseUpdateData.php`

Which tables are optional? Every table which is not listed QFQ-Base or better: if there is a dedicated SQL for it.

* Take care to prefix updates on optional table, so that they are only played if the table already exist!

New DB tables
-------------

* New tables should be added to QFQ-Base or QFQ-Optional (depending of use).
* New functions (stored procedures) should be added to QFQ-Functions.

Automatic Update
----------------

In QFQ a check is done if a database table update is required:

    if ('' != $this->store::getVar(TYPO3_BE_USER, STORE_TYPO3 . STORE_EMPTY) || defined('PHPUNIT_QFQ')) {
      // Check and update only in a session of a BE User.
      $updateDb->checkNupdate($dbUpdate, $t3ConfigQfq);
    }

Class DatabaseUpdate() does the update.

Exceptional Update
------------------

There are several tables (Chat, FileUpload, Split, ...) which do not belong to QFQ core functionality. Such tables
are not installed by default.

If an application developer uses such tables and they are not installed at that time, QFQ detects this (MariaDB will
throw an error 'unknown table') and an update dialog will be offered - only if SYSTEM_SHOW_DEBUG_INFO_YES is set (e.g.
BE User is logged in).

If an application developer uses a table name identically to an optional QFQ table, this should not interfere
with the described approach: cause than there is no 'missing table' (MariaDB exception).