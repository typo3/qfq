REST
====

* https://en.wikipedia.org/wiki/Representational_state_transfer
* https://restfulapi.net
* https://poe-php.de/tutorial/rest-einfuehrung-in-die-api-erstellung
* https://blog.restcase.com/top-5-rest-api-security-guidelines/

General Concept
===============

* There is one PHP file to handle all REST calls:

    typo3conf/ext/qfq/Classes/Api/rest.php

* All further endpoints are appended after rest.php, seperated by '/'. Example:

    http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson/1/restAddress/123?myEmail=jonni@miller.com

    The argument 'myEmail' is just to show how  GET variables will be submitted.
    
* Each `level` is a QFQ form. In the above example: `restPerson` and `restAddress`     
* A QFQ form will be enabled for REST calls via field 'Permit REST'. Possible options: get, insert (post), update (put), delete
* An optional HTML header token based 'authorization' is supported.
* At least one `level` (= form name) has to be given. 
* Multiple `level/id` tuple are possible.
* Only the last level will be used. The last `level` becomes automatically `form` in STORE_TYPO3.
* The last `id` becomes automatically `r` in STORE_TYPO3.
* Previous `level` and `id` are accessible via `{{_id1:C}}`, `{{_form1:C:alnumx}}`,`{{_id2:C}}`, `{{_form2:C:alnumx}}`, ...
* Import/Export data has to be/is JSON encoded.
* The following settings has no impact to QFQ forms called via REST: `form.Permit New`, `form.Permit Edit`  

HTML Requests
=============

GET - export
------------
 
Example:

   curl -X GET "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson" 

Details:

* no `id` or `id=0` (example: 1, 123): The result of `Form.parameter.restSqlList` will be generated.
* `id>0` (example: 1, 123): the result of `Form.parameter.restSqlData` will be generated.
* The whole resultset will be JSON encoded.
* It's not possible to render subrecords. This has to be done via a sub level (next form). 
* Future: If this is not sufficient, a possible solution might be a `report`-notation (special FormElement), which do 
  not implode all output, but leave the rows/cells intact as an array - the json_encode will then to the rest. 

POST - insert
-------------

Example:

   curl -X POST "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson" -d '{"name":"Miller","firstname":"Jonni"}'

Details:

* The data has to be JSON encoded transferred to the REST API. 
* The JSON stream will be decoded to an array and copied to $_POST.
* The further process is identically to a standard 'form submit'.
* There should be no `id` given or `id=0`.

PUT - update
------------

Example:

   curl -X PUT "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson/1" -d '{"name":"Miller","firstname":"Jonni"}'

Details:

* The data has to be JSON encoded transferred to the REST API. 
* The JSON stream will be decoded to an array and copied to $_POST.
* The further process is identically to a standard 'form submit'.
* There have to be an `id>0`.

Delete
------

Example:

   curl -X DELETE "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson/1" 

Details:

* The data has to be JSON encoded transferred to the REST API. 
* The JSON stream will be decoded to an array and copied to $_POST.
* The further process is identically to a standard 'form submit'.
* There have to be an `id>0`.

Header Token Authorization 
==========================

Example::

    curl -X GET -H 'Authorization: token="mySuperSecretToken"' "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson/"
    curl -X GET -H 'X-Api-Key: token="mySuperSecretToken"' "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson/"

* Access the token via ``{{Authorization:C:alnumx}}`` or ``{{X-Api-Key:C:alnumx}}``.
* The argument ``token`` can be anything and is not used at all.

Static token
------------
    
Per form configure `form.parameter.restToken=mySuperSecretToken`.

Dynamic token
-------------

The client supplied authorization token is available via the client store: `{{Authorization:C:alnumx}}`.

Take the Client token and check if it saved in a table with all user token:   
 
    form.parameter.restToken={{SELECT a.token FROM Auth AS a WHERE a.token='{{Authorization:C:alnumx}}' }}

DEBUG
=====

Append the GET variable `?XDEBUG_SESSION_START=1`

Example: 

    curl -X POST "http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/rest.php/restPerson?XDEBUG_SESSION_START=1" -d '{"name":"Miller","firstname":"Jonni"}'

PhpStorm with activated debugger will stop at any breakpoint and 'stepping' through the code is possible.
