HTML
====

This document explains the HTML markup used by QFQ.

Hooks
-----

Hooks are used on the Client to gather information required for asynchronous requests and to add predefined event
handlers to HTML Elements.

form.data-toggle="validator"
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adding the attribute `data-toggle="validator"` to a `<form>` element, enables the Bootstrap Validator on that HTML Form.

.data-sip ^^^^^^^^^

Asynchronous requests require to pass a SIP to the Server. Elements triggering an asynchronous request, may gather the
SIP from the
`data-sip` attribute assigned to the HTML Form Element.

.class="record-delete"
^^^^^^^^^^^^^^^^^^^^^^

HTML Form Buttons having the class `record-delete` set, will get an
`onclick` handler attached by `QfqNS.QfqRecordList`. Each `<button>`
also requires an `data-sip` attribute.

.data-load=""
^^^^^^^^^^^^^

HTML Form Elements having the attribute `data-load`, will trigger a call to `api/load.php` upon change.

### id="save-button"
### id="close-button"
### id="delete-button"
### id="form-new-button"

Typeahead
---------

Typeahead capable text input elements will be defined by the following attributes:

.class='qfq-typeahead'
^^^^^^^^^^^^^^^^^^^^^^

.data-typeahead-sip ^^^^^^^^^^^^^^^^^^^

The SIP will store:

Use with SQL: `typeAheadSql`

Use with LDAP: `typeAheadLdap`
* `ldapServer`
* `ldapBaseDn`
* `typeAheadLdapSearch`
* `typeAheadLdapValuePrintf`
* `typeAheadLdapKeyPrintf`

.data-typeahead-limit ^^^^^^^^^^^^^^^^^^^^^

* Defines the limit of entries shown on the client. Default on client is 5. The server will always send a value. 
  The server default is 20.

.data-typeahead-minlength ^^^^^^^^^^^^^^^^^^^^^^^^^

* Defines the string minlength, typed by the user, before the first lookup is started. Default is 2.

data-typeahead-pedantic ^^^^^^^^^^^^^^^^^^^^^^^

* If present, only suggested values are allowed in the input element

Tags Form Element
-----------------

The tags form element depends on Typeahead by default. The following attributes define the tags form element, additional
to the attributes for Typeahead (see above).

Mockups can be found in `mockup/typahead.php`

.data-typeahead-tags ^^^^^^^^^^^^^^^^^^^^

* If present, the field becomes a tag field

.data-typeahead-tag-delimiters ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* List of ascii key codes of the keys which may be pressed to add a new tag when typing.

.value ^^^^^^

* JSON encoded list of key value pairs of existing tags. e.g. 
  
  `[{value: "Alaska", key: "AK"}, {value: "Alabama", key: "AL"}]`

POST data ^^^^^^^^^

* JSON encoded list of key value pairs of the selected tags. e.g.

  `[{value: "Alaska", key: "AK"}, {value: "Alabama", key: "AL"}]`

