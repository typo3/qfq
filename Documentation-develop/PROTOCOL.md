<!-- -*- markdown -*- -->

# Client/Server Protocol


## General Protocol

The Client may asynchronously send requests to the Server. The Server
is expected to return responses as outlined below.

The response must contain at least a [Minimal Response]. Depending on
the request, it may provide additional responses as outlined in this
section.


### Minimal Response

Asynchronous request (read AJAX) initiated by the Client receive a
JSON Response from the server containing at least:

	{
		"status": "success"|"error",
		"message": "<message>"
	}
   
`status` indicates whether or not the request has been fulfilled by
the server (`"success"`) or encountered an error (`"error"`). On
`"error"` the Client must display `"<message>"` to the user. On
`"success"`, the Client may display `"<message>"` to the user.

Depending on the request, the server may provide additional
information in the response, as outlined below.

### Alert

     {
       "message": "<message>",
       "type": "<type of message>", 
       "timeout": <timeout in milliseconds>, 
       "modal": <boolean>, 
       "buttons": [{
                    "label":  "<label>",
                    "eventName": "<eventName>",
                    "focus": <boolean>
       }]
     }
`"type"` 
: can be `"info"`, `"warning"`, or `"error"`. Default is `"info"`.

`"timeout"` 
: If timeout is less than or equal to 0, the alert won't timeout and stay open until dismissed by the user. Default `n.Alert.constants.NO_TIMEOUT`.

`"modal"` 
: whether or not alert is modal, i.e. prevent clicks anywhere but the dialog. Default is `false`.

`"buttons"` 
: what buttons to display on alert. If empty array is provided, no buttons are displayed and a click anywhere in the alert will dismiss it.
  
`"buttons.label"`
: Label of the button.

`"buttons.eventName"`
: Name of the event to be executed when button is clicked.

`"buttons.focus"`
: Whether or not button has focus by default. Default is `false`.

### HTML Form Element Validation Response

The Server may perform a serverside validation of values submitted as
part of a HTML Form submission. If the validation fails, it may notify
the Client by adding following name/value pairs to the response JSON
Stream

	{
        "status": "error",
        ...
        "field-name": "<field name>",
        "field-message": "<message>",
        ...
	}
	
Only one validation failure per request can be reported to Client.

The Server is expected to set the status `"status"` to `"error"`, and
the Client is expected to treat the error as explained in [Minimal Response]
and must obey the rules of redirection as explained in [Redirection Response].

The Client must visibly highlight the HTML Form Element that caused the
validation failure.

`"field-name"`
:	The value of the `name` attribute of the HTML Form Element that
	caused the validation failure.
	
`"field-message"`
:	Message to the User, indicating the nature of the failure.


### Form Group Configuration Response

As part of the server response, the JSON stream may contain a key
`form-update`. This response is used to reconfigure HTML Form Elements
and Form Groups on the clientside, based on conditions evaluated on
the serverside. It contains an array of objects
having the following structure

    {
		...
		"form-update" : [
			{
				"form-element": "<element_name>",
				"hidden": true | false,
				"disabled": true | false,
				"required": true | false,
				"value": <value>
			},
			...
		],
		...
    }

`"form-element"`
:	 the name of the HTML Form Element as it appears in the `name` attribute.

`"hidden"`
:   whether the Form Group is visible (value: `false`) or invisible (value: `true`).
	
`"disabled"`
:   whether or not the Form Element is disabled HTML-wise.

`"required"`
:   whether or not the Form Element receives the HTML5 `required` attribute.

`"value"`
:	For textual HTML Form Input elements, it is supposed to be a scalar
	value, which is set on the element.
	
	When `"form-element"` references a `<select>` element, a scalar
    value selects the corresponding value from the option list. In
    order to replace the entire option list, use an array of objects
    as value to `"value"`, having this format
	   
	    [
			...
			{
				"value": 100,
				"text": "a",
				"selected": true
			},
			{
				"value": 200,
				"text": "b",
				"selected": false
			}
			...
        ]
		
	`"select"` is optional, as is `"text"`. If `"text"` is omitted, it
    will be derived from value.
	
	HTML checkboxes are ticked with a `"value"` of `true`. They are
    unchecked with `false`.
	
	HTML radio buttons are activated by providing the value of the
    radio button `value`-attribute to be activated in `"value"`.
	

### Element Configuration Response

As part of the server response, the JSON stream may contain a key
`element-update`. This key stores information on how to modify HTML elements identified by `id`. Modifying in this 
context refers to:

 * Setting attribute values
 * Deleting attributes
 * Setting content of a HTML element.
 
The content of `element-update` is outlined below

    {
        ...
        "element-update" : {
            "<element_id1>": {
                "attr": {
                    "<attr_name1>": "<value1>" | null,
                    ...
                    "<attr_nameN>": "<valueN>" | null
                },
                "content": "<element_content>"
            },
            ...
            "<element_idN>": {
                "attr": {
                    "<attr_name1>": "<value1>" | null,
                    ...
                    "<attr_nameN>": "<valueN>" | null
                },
                "content": "<element_content>"
            }
        },
        ...
    }
    
The presence of `element-update` is optional. `<element_idN>` refers to the element's `id`-attribute value. It used 
to uniquely identify the HTML element in the DOM. The properties `"attr"` and `"content"` are both optional.

Supplying `null` as value for `"<attr_nameN>"` will remove the attribute from the HTLM element identified by 
`"<element_idN>"`.

If the element has no `"<attr_nameN>"` attribute, the attribute will be created. In any case, the attribute's value will be set 
to the value specified by `"<valueN>"`. See above for handling of `null` value.

### Redirection Response

Depending on the request, the server may return redirection
information to the Client. It is up to the Client to respect the
redirection information.

The Client must not perform a redirect in case the status in
`"status"` is `"error"`.

The format of redirect information is outlined below

	{
		...
		"redirect": "no" | "url" | "url-skip-history" | "auto" | "close"
		"redirect-url": "<url>"
		...
	}
	

`"redirect"`
:	type of redirection. 

 * `"no"` advises the Client to stay on the	Current Page. 
 * `"close"` the client goes back one in history - if there is no history, stays on the same page.
 * `"auto"` the Client decide where to redirect to.
   * if the user clicks 'save', stay on the same page.
   * if the user clicks 'close', go back one in history - if there is no history, stays on the same page. 
 * `"url"` advices the Client to redirect to the URL provided in `"redirect-url"`. 
 * `"url-skip-history"` behaves like `"url"` but the current page will skip the browser history.
	
`"redirect-url"`
:	Used to provide an URL when `"redirect"` is set to `"url"`. It
	should be disregarded unless `"redirect"` is set to `"url"`.

### Typeahead dict Response

    {
		...
		[
			{
				"key": "<key value>",
				"value": <display value>
			},
			...
		],
		...
    }


## API Endpoints


### Form Update

The Client may request an updated set of Form Group Configuration and
HTLM Element states. In order for the Server to compile the set of
Form Group Configuration and HTML Element states, it requires the
entire HTML Form in the POST body, without any HTML Input Elements of
type `file`.

The Client must include the SIP using an HTML Input Element (most
likely of `type` `hidden`).

Request URL
:	api/load.php

Request Method
:	POST

URL Parameters
:	none

Server Response
:   The response contains at least a [Minimal Response]. In addition,
	a [Form Group Configuration Response] may be included.


### Form Save

The Client submits the HTML Form for persitent storage to the
Server. The submission should not contain `<input>` HTML Elements of
type `file`.

The Client must include the SIP using an HTML Input Element (most
likely of `type` `hidden`).

Request URL
:	api/save.php

Request Method
:	POST

URL Parameters
:	`submit_reason=save` | `submit_reason=save,close`

Server Response
:   The response contains at least a [Minimal Response]. In addition, a
	[Form Group Configuration Response],
	[HTML Form Element Validation Response] and/or
	[Redirection Response] may be included.


### File Upload

Files are uploaded asynchronously. Each file to be uploaded requires
one request to the Server, using a Multi part form with file content,
parameter `s` containing SIP, and parameter `name` containing the name
of the HTML Form Element.

Request
:	api/file.php

Request Method
:	POST

URL Parameters
:	`action=upload`

Server Response
:	The response contains a [Minimal Response].


### File Delete

Files are delete asynchronously. Each file to be delete on the
serverside requires on request to the Server. The parameters
identifying the file to be deleted are sent as part of the POST
body. The SIP of the request is included in the parameter name
`s`. The value of the `name` attribute of the HTML Form Element is
provided in `name`.

Request
:	api/file.php

Request Method
:	POST

URL Parameters
:	`action=delete`

Server Response
:	The response contains a [Minimal Response].


### Record delete

Request the deletion of the record identified by the SIP. The SIP might contain a SIP_TABLE and/or a SIP_FORM.
If both are specified, SIP_FORM will be taken. With SIP_FORM, the tableName is derived from the form. 

Request
:	api/delete.php

Request Method
:	POST

URL Parameters
:	`s=<SIP>`

Server Response
:	The response contains a [Minimal Response].
	[Redirection Response] may be included.

### Download

Request
:  api/download.php
 
Request Method
:	GET

URL Parameters
:	`s=<SIP>`

Server Response
:	header("Content-type: $mimetype");
	header("Content-Length: $length");
	header("Content-Disposition: inline; filename='$outputFilename'");
	header("Pragma: no-cache");
	header("Expires: 0");
	file_get_contents($file);
 
A download might be:
  * a single file (any type, will be detected on the fly), 
  * an export of several files as a ZIP archive,
  * an export of a T3-'XML'-Page converted to Excel,
  * a converted HTML page to PDF,
  * a PDF file, concatenated on single PDF files and/or converted HTML page to PDF,
  * a thumbnail, streamed from cache dir of if not present/recent rendered on request,
  * a file to monitor constantly,
  * a file, delivered as a JSON structure, used in 'copy to clipboard',

'api/download.php' will be called with a SIP (no other vars used). The SIP contains:
  * DOWNLOAD_EXPORT_FILENAME - any target filename, if none given take DOWNLOAD_OUTPUT_PDF ('output.pdf'). 
  * DONWLOAD_MODE - file / pdf / excel / zip / thumbnail / monitor. If not specified:
      a) 'file' is the default, if only one source is given and if that is a file.
      b) 'pdf' is the default, if there are multiple TOKEN_URL, TOKEN_URL_PARAM, TOKEN_FILE in SIP_DOWNLOAD_PARAMETER found.
      
  * DONWLOAD_MODE: file / pdf / excel / zip    

    * SIP_DOWNLOAD_PARAMETER (base64 encoded) - contains all parameter to source elements.
     
        Format: <format 1>:<element 1>|<format 2>:<element 2>|...|<format n>:<element n>|
        
        <format>: TOKEN_URL, TOKEN_URL_PARAM, TOKEN_FILE, TOKEN_THUMBNAIL_DIMENSION
        <element>: depending on the token - see below
        
        URL:  a) 'u:http://w3c.org', b) 'u:w3c.org/', c) 'u:w3c.org/2017/index.php?issue=23'
        URL_PARAM:  a) 'U:id=export&r=123', b) 'U:id=export&r=123&_orientation=landscape&_page-size=a3'
        FILE: a) 'F:fileadmin/example.png'
          
        * In URL_PARAM extra parameter used by `wkhtmltopdf` can be specified. All Parameter, starting with '-'
          will be extracted from the regular URL_PARAM and instead forwarded as options to `wkhtmlpdf`

  * DONWLOAD_MODE: thumbnail    

      * SIP_DOWNLOAD_PARAMETER (base64 encoded) 

          * T:<pathFilename Source>
          * W:<dimension>
          * r:<render mode>
          * Render the thumbnail

          Download.php will be called with the SIP. After decoding the SIP, the base64 encoded parameter are used with
          DONWLOAD_MODE=file and  SIP_DOWNLOAD_PARAMETER=F:<thumbnail>

  * DONWLOAD_MODE: monitor    

      SIP encoded parameter
      
      * file: <filename>
      * tail: <number of last lines>
      * append: 0|1
      
      The retrieved lines are outputted without any conversion.
   
  * DOWNLOAD_OUTPUT_FORMAT: raw (default), json
  
      * If this parameter is missing, 'raw' ist meant.
      * 'json' is used for 'copy to clipboard'.
           
  * The base64 encoding is necessary:
  
    * to deliver multiple elements with the same token (e.g. multiple PDF files to concatenate).
    * special parameter names, like 'id', should not force the regular interpretation of 'id' during conversion to a SIP. 
      
During preparing and delivering the download (file / pdf / excel / zip), a popup shows a spinning gear by default. The 
popup itself will display an individual message. The popup needs some HTML code (only once per T3 page). 
Download links might be generated in `report` as well as in `subrecords of forms`. To trigger the generation of the HTML 
popup code, a variable DOWNLOAD_POPUP_REQUEST in STORE_SYSTEM will be set to 'true' (string) in class Link(), as soon as 
the first download link is rendered. During internal rendering of the download link, the const text token 
DOWNLOAD_POPUP_REPLACE_TEXT and DOWNLOAD_POPUP_REPLACE_TITLE will be replaced with individual texts, defined per download link.


### Typeahead

The Client initiates Typeahead actions via a GET request. A JSON key/value dict will we be send back as response.
The Client GET request contains a 'sip' and the already typed value as 'query' paramter.    

Request URL
:	api/typeahead.php

Request Method
:	GET

URL Parameters
:	`sip`, `query`

Server Response
:   The response contains at least a [Minimal Response]. In addition, a [Typeahead dict],

### Record lock

Request, extend or release a lock for a record, identified by the SIP. The SIP contain a SIP_FORM and a SIP_R (record id).

See `Documentation-develop/diagram` for a state diagram.

To detect record change at time of 'record lock' or 'record save', a MD5 hash is provided from the server
and needs to pass back to dirty.php as well.

Request
:	api/dirty.php

Request Method
:	GET

URL Parameters
:  `s=<SIP>`   (form, r)
:  `action=lock`, `action=extend`, `action=release>`
:  `recordHashMd5=<value of hidden form element 'recordHashMd5'>`
:  `tabUniqId=<unique id>`

Server Response
:	The response contains an [Lock Response].

### Lock Response

Asynchronous request (read AJAX) initiated by the Client receive a
JSON Response from the server (extended [Minimal Response]) containing:

:	{
:		"status": "success"|"error"|"conflict"|"conflict_allow_force",
:		"message": "<message>"e5
:   }
   
`status` indicates how the request has been fulfilled by the server. 
On`"success"`, the Client display nothing to the user. 
On one of`"error"|"conflict"|"conflict_allow_force"` the Client must display `"<message>"` to the user.
On `"conflict"` the Client opens the alert as modal dialog (user can't change anything on the form) with a 'reload current
form' button.
On `"conflict_allow_force"` the Client opens the alert non-modal (default).

### tabUniqId

* Client: Every browser tab get's a `tabUniqId` (timestamp) as soon as the first time a lock is requested (acquire). 
  This `tabUniqId` remains, until the tab is close. It is saved in window.name.
  
* Server: On lock request, the `tabUniqId` is saved in dirty record.
 
  * The `tabUniqId` helps to implement 'optimistic locking': if there is no argument against 'grant lock' - grant it.
    Example: In browser tab 'A' a lock is requested. For unknown reason, there is a stale lock for the
    user session and the given tab id: grant the log. 
     
  * Unexpected async behaviour in client: On page reload (F5), the 'lock release' might come after 'lock acquire'. 
    The form goes in 'read only' mode - an additional page reload (F5) solves the situation - but this is boring.   
  

### Drag And Drop (sort)

Request
:	api/dragAndDrop.php

Request Method
:	GET

URL Parameters:

: `s=<SIP>`  (`form=<formname>`)
:  
: `dragId=<data-dnd-id of dragged element>`
: `dragPosition=<client internal position (numbering) of element before dragging>`
: `setTo=before`, `setTo=after`
: `hoverId=<data-dnd-id of dragged element>`
: `hoverPosition=<client internal position (numbering) of element after dragging>`

Server Response
:   The response contains at least a [Minimal Response]. In addition, a
	[HTML Element Update] may be included.
  
## Glossary

SIP
:   Server Id Pairs 

HTML Form Element
:   Any `<input>` or `<select>` HTML tag. Synonymous to *Form Element*.

Form Group
:   The sourrounding `<div>` containing the `.control-label`,
    `.form-control` `<div>`s, and `.help-block` `<p>`.

Client
:   Application that enables a user to interact with QFQ, i.e. a Web Browser.


Current Page
:	The currently displayed page in the Client.

Redirect
:	Issued by the Server. It is a command prompting the Client to
	navigate away from the Current Page.

### Tablesorter Save View

To save a table view the client sends a post request with the following data:

    {
       "name": "<name of view>",
       "public": <boolean>, 
       "tableId": "<id of table to which view belongs>", 
       "view": {
                    "columnSelection": <array of chosen column ids>,
                    "filters": <array of strings>,
                    "sortList": <array of arrays of length 2>
       }
    }

Request URL
:	api/save.php

Request Method
:	POST

URL Parameters
: `s=<SIP>` - to prove permission.

Server Response
:   The response contains at least a [Minimal Response].
