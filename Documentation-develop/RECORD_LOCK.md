<!-- -*- markdown -*- -->

# Record locking

## Concept: Late locking

* A lock is required on first modification.
* Multiple forms might open the same record, all seems to have write access. The first one who modifies the record
  get the lock, all following will switch to form=readonly on their first try to modify the record.
  
## Lock mode: Exclusive 

* A lock can't be overwritten.

## Lock mode: Advisory 

* A lock can be ignored.
* Last save win's.

## Lock mode: None

* No locking at all.

# Workarounds

* At least one Browser (FF 71, maybe other in the future too), do not allow to wrap the 'leave page' dialog anymore.
  This might result in stale lock files (modified record, click on browser tab close or any link), cause the lock 
  logic does not know that the user leaves the page. 
  
  * Workaround: before 'do you want to leave the page' appears, the lock is released, independent if the user answers 'no'.
    As soon as the users modifies the record again, a new lock is acquired. This is better than a stale record lock.
    
* Reload a page (F5) on a modified record, opens the form in readonly mode (record lock found).

  * Reason: the lock release is fired by the browser AFTER form load - than the lock-logic reports 'record is already locked'.
  * Workaround: with the above workaround, this does not happen anymore. Nevertheless, a 'tabUniqId' has been implemented.
    That one is saved as record lock and a page reload origin can be identified as the same tab as where the lock has been
    acquired.  

= State Diagram =

See `Documentation-develop/diagram` for a state diagram.
