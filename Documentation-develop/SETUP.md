QFQ Development Setup
=================

Requirements
============

* Node.js
* PHP7.x/8.x - for local testing
* Python
* IDE (Used: PhpStorm, VSCode)

Mac OS X
--------
To be able to install various unix tools that are used, it is recommended to install homebrew from https://brew.sh

Afterwards use:
```brew install php@7.2 wget```

VSCode
------

It is recommended to install the standard PHP, Python and Javascript plugins (Can be selected at the start). 

Additional useful Extensions: 
 * `Tasks Panel` - allows you to start Grunt Tasks.
 * `Beautify` - Opinionated Javascript Code beautifier.
 * `JSLint` - Javascript Linting for old Ecma Script.
 * `PHP Debug` - Support for PHP XDebug
 * `PHP IntelliSense` - Advanced Autocompletion and Refactoring for PHP
 * `PHP Intelephense` - Similar features to PHPStorm (has additional premium functionality)

Preparations
------------

You can use the command `make bootstrap` from the root folder of the git QFQ project to setup your Development Enviroment.

Or run:
```
sudo npm install grunt-cli -g
npm install
grunt default
```
manually from the root directory of the git QFQ project.

Working with Mockups
--------------------
Mockups are in the `mockup` directory.

To access them, you need a locally running webserver. The easiest way is to use the local webserver provided by php. 

`php -S localhost:8000`

Now you can access the mockups by going to http://localhost:8000/mockup/qfqform.html 

