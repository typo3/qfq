Neuer Build
===========


* release: Wird ein *Tag* vergeben (egal welcher Branch) der mit 'v' beginnt, erzeugt das automatisch einen Build - https://w3.math.uzh.ch/qfq/release.
* snapshot: Jeder Commit (egal welcher Branch) erzeugt einen Snapshot - https://w3.math.uzh.ch/qfq/snapshot.
* nightly: Nach einem Commit auf Branch 'master' tagsueber, wird um 23:55 ein 'nightly' Build erstellt - https://w3.math.uzh.ch/qfq/nightly.

Neue Versionsnummer
===================

1) Fuer jede neue Version ein Ticket erstellen. Template: #6994

* Merge 'open Branches' to **Develop**.
* Aktueller Branch: Develop

3) Die aktuellen Commits anschauen und wichtige Topics uebernehmen (git log > ~/qfq.log, alles bis zum letzten TAG anschauen):

   * **All commits since last tag**:
     git log $(git describe --tags --abbrev=0)..HEAD --oneline | cut -c9- > /tmp/out; gedit /tmp/out

       * All commits since tag 'v20.11.0'
         git log v20.11.0..HEAD --oneline

       * complicated:
         git log | grep -v -e '^commit ' -e  '^Author: ' -e '^Date: ' -e '^Merge: ' > /tmp/out; pluma  /tmp/out

   * **Anpassen**: qfq/extension/Documentation/Release.rst

   * Release.rst **verteilen**:  make copyReleaseNotes

   * Manuell:

       * Den Inhalt von Release.rst kopieren nach qfq/extension/RELEASE.txt.
       * Den Inhalt von Release.rst kopieren nach CHANGELOG.md.

   * Tickets

       * Alle offenen Tickets im aktuellen Milestones verschieben auf den naechsten Milestone.
       * Abgeschlossene Tickets schliessen.

4) In folgenden Files anpassen:

   **Achtung**: die Release Minor darf KEINE fuehrenden Nullen enthalten!!! Ansonsten funktioniert die Verteilung vie
   TER nicht.

   **Auto**: ./setVersion.sh 24.12.0

   Manuell:

    * extension/Documentation/_make/conf.py: release, version-
    * Documentation/Settings.cfg: version
    * extension/ext_emconf.php: version

5) **Update Version & Commit**

    * **Commit & Push** to develop branch:

   New version v24.12.0

6) **Check RTD logs:**

    * https://readthedocs.org/projects/qfq/builds/
    * Search the logs (raw view) for problems (Start after **Running Sphinx**) - fix them.

7) **GIT: Master**

    * Merge 'Develop' to **Master**: git.math.uzh.ch > QFQ > Merge Requests > New merge request > 'Develop >> Master'
    * Checkout **Master**.

8) **New Tag**:

    * Neuen tag via Browser auf dem **master** branch setzen: git.math.uzh.ch > QFQ > Repository > Code > Tags > New tag

   Tag: v24.12.0

   # Den tag mit diesem Command zu setzen scheint den Build Prozess nicht zu triggern.
   git tag -a v24.12.0 -m 'New version v24.12.0' git push


9) **Merge 'master' into 'develop'**

10) **Tickets:**

    * Schliessen und der QFQ Version zuweisen.
    * Aktuellen Milestone schliessen

11) **QFQ Doc aktualisieren:**

   Erfolgt implicit durch Commit in Branch Master & Develop.

12) Extension hochladen:

    TER: https://extensions.typo3.org/ > Log in > My Extensions.


Upload new version to TER
=========================

* https://extensions.typo3.org/ > Log in > My Extensions.
* Rename the ZIP file to be TER compatible: e.g. qfq_18.6.0.zip.
* Upload - that's all.
