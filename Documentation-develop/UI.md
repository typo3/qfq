USER INTERFACE
==============

Button states
-------------
If the HTML Form has no modifications, the `Close`, `New` and `Delete`
buttons are enabled. The `Save` button is disabled. 
  
If the HTML Form has modifications, the `Save`, `Close`, `New`, and
`Delete` button is enabled. No button is disabled.


Save Button 
-----------

* User presses the Save button
  1. Reset all validation states
  1. Client validates HTML Form
  1. Form is submitted to server
	 * Success: 
		 1. Show message provided by server
		 1. Current formelements and data will be reloaded.
		 1. Process server reponse 'redirect':
			* 'client': No redirect.
			* 'no': No redirect.
			* 'url': Redirect to URL
	 * Failure: Happens on communication errors, if data validation
       fails, form actions fails or saving data fails.
	     1. Show error message.
         1. Client: Ignore server reponse 'redirect'. Client stays on current page.
    


Close Button
------------
* User presses the Close button
	1. Form not modified by user: Client redirects to previous page.
	1. Form modified by user: Ask User 'You have unsaved changes. Do you want to save first?'
       * Yes: Client redirects to previous page.
       * No: Client stays on current page.
       * Save & Close: 
		 1. Client reset all validation states
		 1. Client validates HTML Form
		 1. Client submits form to server.
			* Success: Process server response 'redirect':
			  * 'client': Client shows previous page.
	          * 'no': No redirect.
              * 'url': Redirect to URL
		    * Failure: Happens on communication errors, if data validation
	          fails, form actions fails or saving data fails.
			  * Show error message. 
			  * Client: No redirect. Ignore server reponse 'redirect'.

Delete Button: Main record
--------------------------
* User presses the button. Ask User 'Do you really want to delete the record?
  * Yes: 
    * Delete record on server.
	* Process server reponse 'redirect':
		* 'client': Client redirects to previous page.
		* 'no': Error message.
		* 'url': Redirect to URL
  * No:
	  * Client does not delete record on server.
	  * No redirect. Client stays on current page.

New Button
----------

User presses the button

* Form not modified by user: Client redirects to href url.
* Form modified by user: Ask User 'You have unsaved changes. Do you want to save first?'

Yes:

* Client reset all validation states
* Client validates HTML Form
* Form is submitted to server
	* Success:
		* Client: Ignore server reponse 'redirect'. Client redirects to href url.
	* Failure: Happens on communication errors, if data validation fails, form actions fails or saving data fails.
		* Show error message.
		* Client: Ignore server reponse 'redirect'. Client stays on current page.
		  No:
* Client does not save the modified record.
* Client redirects to href url.

Cancel:

* Client does not save the modified record.
* Client stays on current page.

File Handling: Upload
---------------------

* No previous uploaded file present
	1. User presses the Browse button
	1. User selects file
		1. File is uploaded to qfq immediately
		1. Browse button gets disabled and hidden
		1. File delete button is shown
	1. User cancels file selection
		1. no action
* Previous uploaded file present
	1. User deletes file
	1. File delete button gets disabled and hidden
	1. Browse button gets enabled and displayed

