# Phpunit

* Versions: https://phpunit.de/supported-versions.html
* Installation: https://phpunit.de/getting-started/phpunit-9.html

* Doc: https://docs.phpunit.de/
* Doc um Exceptions zu testen: https://docs.phpunit.de/en/9.6/writing-tests-for-phpunit.html

## TODO: if (!defined('PHPUNIT_QFQ')) {...}

rewrite $phpUnit to: "if (!defined('PHPUNIT_QFQ')) {...}

## Run unit tests from CLI:

Make sure dev dependencies are installed:

```shell script
# in extension directory
composer update --dev
```

Run all tests:

```shell script
# in extension directory
vendor/bin/phpunit --configuration phpunit.xml
```

Run single test:

```shell script
# in extension directory
vendor/bin/phpunit --configuration phpunit.xml --filter <test_name>
```

## Setup

Requirements for running the php unittests:

- `make bootstrap` was executed
- The following files exist at the same location (either `extension/` or `typo3conf/`):
  * `qfq.json`
  * `LocalConfiguration.php`
- The database credentials in `qfq.json` are correct
- The database with the name `DB_1_NAME` followed by `_phpunit` exists. E.g. `app_qfq_phpunit` where `DB_1_NAME=app_qfq`

In Tests/Unit/ you may find a mockup of `LocalConfiguration.php`
and a template for `qfq.json`.

## Run unit tests from commandline

REMARK: Running the unit tests without specifying the configuration file `phpunit.xml` will not work. See section
"Autoloader" for explanation.

From the extension folder run:

`vendor/bin/phpunit --configuration phpunit.xml`

## Phpunit configurations

Phpunit configurations are stored in extension/phpunit.xml Running the tests without specifying these configurations
will not work.

## Autoloader

The test classes use the composer autoloader to reference to the source classes. The autoloader is loaded by phpunit
before each test as specified in phpunit.xml by the line `<phpunit bootstrap="vendor/autoload.php">`

## Run tests without typo3 installation (e.g. gitlab runner)

As defined in the phpunit command of projectRoot/Makefile.

The files phpunit_qfq.json and phpunit_LocalConfiguration.php are copied outside the extension folder by the Makefile.
