Config
======

During QFQ bootstrap three config files are read:

* qfq.project.path.php (QFQ)
* fileadmin/protected/qfqProject/qfq.json (QFQ)
* typo3conf/LocalConfiguration.php (Typo3 > QFQ)

Get config
==========

Stacktrace bis die LocalConfiguration.php gelesen wird:

IMATHUZH\Qfq\Core\QuickFormQuery->__construct()
IMATHUZH\Qfq\Core\Store\Store->__construct()
IMATHUZH\Qfq\Core\Store\Store::getInstance()
IMATHUZH\Qfq\Core\Store\Store::fillStoreSystem()
IMATHUZH\Qfq\Core\Store\Config::getConfigArray()
IMATHUZH\Qfq\Core\Store\Config::readConfig()

How to create a new config option
==================================

To create a new config option, you have to make the changes specified below in the following files.

ext_conf_template.txt
---------------------

The following variables must be set:

**cat**
(category where the new option will be located, in the extension configuration of your typo3 backend)

**type** (datatype of the config option)

possible datatypes:
* boolean (checkbox)
* color (colorpicker)
* int (integer value)
* int+ (positive integer value)
* integer (integer value)
* offset (offset)
* options (option select)
```type=options[label1=value1,label2=value2,value3];```
* small (small text field)
* string (text field)
* user (user function)
```type=user[Vendor\MyExtensionKey\ViewHelpers\MyConfigurationClass->render];```
* wrap (wrap field)

**label** (title and description of the config option, split by ":")

**myVariable** (name the variable of the config option and assign a default value)

**Example**

```
# cat=config/config; type=boolean; label=MyLabel:Description
myVariable = value1
```

Constants.php
-------------

Best practice would be defining constants with the name of your variable,
since this name should never be changed.

```
const SYSTEM_MY_VARIABLE = 'myVariable';
const F_MY_VARIABLE = 'SYSTEM_MY_VARIABLE';
const FE_MY_VARIABLE = 'SYSTEM_MY_VARIABLE';
```


Config.php
---------------------

In the function **setDefaults()** a default value should be set.
</br>Important in case of new variables: new variables do not exist in QFQ extension config and do not get the default defined in ext_conf_template.txt

```
default = [
    ...
    SYSTEM_MY_VARIABLE => 'true',
    ...
];
```

Support.php
----------

To set the default value of a FormElement you can use the **setFeDefaults()** function.
</br>Wich provides the default value for the FormElement using the **system store**.
</br>The **system store** contains all the variables defined in the typo3 extension configuration.

```
self::setIfNotSet($formElement, FE_MY_VARIABLE, $store->getVar(SYSTEM_MY_VARIABLE, STORE_SYSTEM));
```

StoreTest.php
-------------

The expected default value must be specified in the **testConfigIniDefaultValues()** function so that the unit test can run without errors.

```
$expect = [
    ...
    SYSTEM_MY_VARIABLE => 'true',
    ...
];
```

How to handle variables
--------------------------------------

Here is an example on how you would go about handling variables that are defined on all levels (SYSTEM -> Form -> FormElement)

```
$myVar = $store->getVar(SYSTEM_MY_VARIABLE, STORE_SYSTEM);
if(isset($this->formSpec[F_MY_VARIABLE])){
    $myVar = $this->formSpec[F_MY_VARIABLE];
}
if(isset($this->formElement[FE_MY_VARIABLE])){
    $myVar = $this->formElement[FE_MY_VARIABLE];
}
```
