Drag And Drop
=============

Sort
----
Initialize a dnd container by adding the class "qfq-dnd"

Set container object class to `class="qfq-dnd qfq-dnd-sort"`.

Add the data elements: `data-dnd-api="url"` and `data-dnd-key="key"`.

For the children inside of the container (just the first children):
add `data-dnd-id` to a reference you can handle (probably record id).

Request will be sent containing following GET variables:

* dragId = `data-dnd-id` of the dragged object,
* dragPosition = client internal old position of the dragged object.
* setTo = "after" or "before",
* hoverId = `data-dnd-id` id of the element the dragged element is now hovering, meaning before or after.
* hoverPosition = client internal position of currently hovered element.﻿

Example: http://something/bla?dragId=uno&dragPosition=1&setTo=before&hoverId=tre&hoverPosition=3

Drag'n'drop in forms
--------------------

* For FormElement.typ=subrecord DND is automatically detected. 
* The encoded SIP contains: dnd-subrecord-form-id, dnd-subrecord-form-table, dnd-subrecord-id, dndTable, oderColumn, 
  oderInterval, dnd-db-index. 
