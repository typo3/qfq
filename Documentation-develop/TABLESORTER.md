Tablesorter
===========

* https://mottie.github.io/tablesorter/docs/index.html

General Concept
===============

Tablesorter supports filter, column on/off and sort.

A QFQ enhancement allows save/delete/activate of custom tablesorter `views`. This feature is described in this document.

QFQ uses the table 'Setting' to store per user, public and readonly settings. At time of writing only 'tablesorter' settings
are supported. Further settings might come in the future.

Show table with tablesorter activate/save/delete
================================================

A page with a HTML table includes the command `{{'<uniqueName>' AS _tablesorter-view-saver}}` inside of the HTML table tag. 
QFQ will replace the command::

  <table {{ 'allperson' AS _tablesorter-view-saver }} class="tablesorter tablesorter-filter tablesorter-column-selector"> ... </table>

by 

  <table data-tablesorter-id='allperson' data-tablesorter-sip='5d0e29c4eacc2' 
    data-tablesorter-view='[{"name":"test","public":false,"tableId":"mytable","view":"eyJjb2x1bW5TZWxlY3Rpb24iOlswLDEsMiwzXSwiZmlsdGVycyI6WyIiLCIiLCIiLCIiXSwic29ydExpc3QiOltdfQ=="}]' class="table tablesorter tablesorter-column-selector tablesorter-filter tablesorter-pager">


data-tablesorter-id:

  '<uniqueName>'

data-tablesorter-sip:

  tableId:  the '<uniqueName>', in the example it's 'allperson'
  feUser: the current logged in FE user. If no FE user is logged in, take the QFQ cookie. With this workaround, it's at
          least possible to work with 'views' during a browser session.

data-tablesorter-view: JSON encoded array

  Array of views. 
  Each view:
  
  ['name'] - Name of the view. Value is html entity encoded.
  ['public'] - true or false. false mean's 'private'.
  ['tableId'] - '<uniqueName>' - this is the filter to assign views to a specific HTML table.
  ['view'] - base64 encoded JSON array. It's base64 encoded for two reasons: 
        a) we do not have to take care about SQL injection (the user might supply filter strings, incl. regex expressions)
        b) the JSON array will contain '}}', which confuses QFQ/evaluate. 

QFQ will collect all views:

  a) all private views of the current table and FE User,
  b) all public views of the current table.


Save tablesorter view
=====================

The user might click on 'Save Private View' or 'Save Public View'.

A Ajax Post request to

  http://localhost/qfq/typo3conf/ext/qfq/Classes/Api/setting.php?s=badcaffee1234
  
The SIP is the one given by `data-tablesorter-sip` and contains `tableId` and `feUser`. The SIP is right to manipulate the views.

The POST contains: 

  feUser - fe user login name
  mode - missing or 'delete' if the given view should be deleted
  public - true|false
  name - name of the view
  view - base64 encoded tablesorter filter/order/column selector.


Response setting.php
====================

setting.php will always answer with a JSON stream (Minimal response):

    {
		"status": "success"|"error",
		"message": "<message>"
    }

FormElemnt Subecord / tablesorter / view saver
==============================================

Additional parameter for subrecord to give own attributes. Selecting views or columns doesn't impact browser history and
record lock. Only saving a new view or deleting an existing view still impacts browser history
(but not record lock), It's impossible to prevent it because we need the page reload to get new existing views after
deleting or saving a new one.
