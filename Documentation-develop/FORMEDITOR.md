Formeditor
==========

The Formeditor is defined in `Resources/Private/Form/form.json`, `formElement.json`, `formJson.json`.

Changes
=======

Any modifications on the Formeditor have to be applied in the corresponding JSON files.

It is fine to

* do all customizations directly via FormEditor,
* create the JSON file (T3 page 'form' > click on JSON for the wished form)
* copy the content to form.json.

Adding new keywords to FormEditor
=================================

Check CODEMIRROR.md > "## Adding new keywords to FormEditor / FormElement parameter field:"
