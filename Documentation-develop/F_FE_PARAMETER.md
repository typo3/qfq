# Config, Form & FormElement parameter

## QFQ Config > Form > FormElement

* QFQ Config: Add to ext_conf_template.txt

  * Update PHP Unittest
  * Define a default in case there is no T3 Localconf file.

* Form

  * Inherit from QFQ config
  * Add in ... (Form) init.

* FormElement

  * Inherit from Form
  * Add in Formelrment init

## Check parameter

To add a new Form/FormElement parameter, include it in the constant array `$FORM_RULES` in `HelperFormElement`.
Adding rules for the parameter is optional but recommended. This array is used to verify whether a parameter in
the Form Element is recognized by QFQ. If a parameter is not listed in the array, a `UserFormException` will be thrown.

Location: `Qfq\Core\HelperFormElement->$FORM_RULES[]`

The `$FORM_RULES` array should contain all parameters along with their corresponding wrapping rules. To add a
new parameter, simply insert a key-value pair into the array.

Example:

```php
FE_AUTOFOCUS => [NO_BRACE]
```
Note: Empty values are skipped during wrapping validation but are still checked for existence in `$FORM_RULES`.

## Rule

When configuring a new rule array, you can choose from the following options (multiple options allowed).
At least one of the selected options must be satisfied for the input to be considered valid.
If no rules are configured ([]), no wrapping checks will be performed.

### Option

* NO_BRACE: no wrap
* DOUBLE_OPEN_BRACE: wrap by `{{ }}`.
* DOUBLE_OPEN_BRACE_WITH_EXCLAMATION: wrap by `{{! }}`.


## Things to Keep in Mind

*  Dynamic Parameter Values
    If a parameter's value needs to be dynamically set and should only allow `{{VAR:STORE}}`,  use the rule
    `DOUBLE_OPEN_BRACE` in the configuration. This ensures proper validation and dynamic value handling.

    Example:

    ```php
    FE_DYNAMIC_PARAMETER => [DOUBLE_OPEN_BRACE]
    ```

*  Static Text with Dynamic Replacements
    If the parameter contains static text with placeholders for dynamic values (e.g., `Hi {{name:F}}`), there is no need to include the `DOUBLE_OPEN_BRACE` rule in the configuration. Such cases are implicitly handled by the system.

    Example:

    ```plaintext
    paramName = Hi {{name:F}}
    ```

*  Consistency

   Regularly review and update the `$FORM_RULES` array to ensure it remains aligned with actually implemented parameters.
