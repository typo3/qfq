T3 Documentation
================

* QFQ Documentation: https://docs.typo3.org/p/IMATHUZH/qfq/master/en-us/Index.html
* Build local documentation: ``make doc-local``
* https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingDocForExtension
* Example Code Documentation: https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual.git

* Pushing a tag on the project triggers a rebuild.
  
Manual trigger of documentation rebuild
---------------------------------------

* https://git.math.uzh.ch > QFQ > Settings > Integrations > Webhook: https://docs-hook.typo3.org: Test 

Log errors
----------

* https://intercept.typo3.com/admin/docs/deployments >> Recent Actions
* https://git.math.uzh.ch > QFQ > Settings > Integrations > Webhook: https://docs-hook.typo3.org > Edit: Recent Deliveries 

Best Practice T3 reST
---------------------

* Best Practice T3 reST: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html
* Reference: https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html
