Multi-DB
========

QFQ handles true Multi-DB setup. For different DBs, separated db handles are used (instead of <dbname>.<tablename> in 
SQL statements). This implies a lot of overhead (always pass the current used db handle), but makes the whole setup 
quite flexible.

* In qfq.json:
  
  * Multiple databases can be configered.
    
        DB_1_NAME=..., DB_1_PASSWORD=..., DB_1_SERVER=..., DB_1_USER=...
        DB_2_NAME=..., DB_2_PASSWORD=..., DB_2_SERVER=..., DB_2_USER=...
        ...
    
  * At least one database has to be configured.
    
* Databases are refenced by their indexes (e.g. 1,2,...)
* In 'Typo3 > Settings > Extension > QFQ > Database' the mapping of which database (=index) is used for which purpose 
  (data, system, wiki). Check #20304
  
  * data: all tables with user data
  * system: all Cron, Form, FormElement, Setting. 
  * wiki: wiki tables

SIP Variable: __dbIndexData
---------------------------

SIP link might contain a variable `__dbIndexData`.

Form
^^^^

* Per QFQ config `database.indexData` data or per form `Form.parameter.dbIndex=...`, a custom data-database can be specified.
* During form load, it's hard to get the dbIndex from the form definition itself (cause it's loaded at that moment).
* Therefore `_dbIndexData` is defined on the SIP and can be used to load data from the required DB.

TypeAheadSql
^^^^^^^^^^^^

* Typeahead should be fast. Therefore not much classes are instantiated. Esecially not STORE.
* During form load, the required database is known, but not during a typeahed event.
* Therefore `_dbIndexData` is defined on the SIP and can be used to load data from the required DB.
* This is **independent** of Multi-DB setup - even on a Single-DB setup, the config `database.indexData` might be 
  different from the default. In order **not** to need class STORE, the dbIndex have to be specified earlier.



  
