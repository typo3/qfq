# CI - Continuous Integration

## Build qfq.zip

* Build qfq.zip as T3 extension
* Build Host: https://git.math.uzh.ch/tyo3/qfq
* File `.gitlab-ci.yml`

## PHPUnit Tests

### Setup gitlab runner

* Prepare runner on git.math.uzh.ch

    * https://git.math.uzh.ch/typo3/qfq > Settings > CI/CD > Runners: Expand > New project unner

* Create runner on alfredXX (https://wikiit.math.uzh.ch/it/software/gitlab)::

```bash
[root@alfredXX] gitlab-runner register -n --url https://git.math.uzh.ch/ --token ... --executor shell --description "QFQ Docker" --tag-list phpunit-u22
```

* MariaDB on alfredXX

```bash
# Install MariaDB Server
[root@alfredXX] apt install mariadb-server

# Create DB & User
[root@alfredXX] mysql
> CREATE DATABASE phpunit_qfq_phpunit;
> CREATE USER 'phpunit'@'localhost' IDENTIFIED BY '<kpdev>'; 
> GRANT ALL PRIVILEGES ON * . * TO 'phpunit'@'localhost' with GRANT OPTION;
```

* File: .gitlab-ci.yml

```markdown
stages:
  ...
  - test
  
...
  
tests:
  stage: test
  tags:
    - phpunit-u22
  script:
    - make phpunit
```

# CD - Continuous Delivery

* Currently not used.