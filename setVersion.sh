#!/bin/bash

# Documentation/_make/conf.py: release, version
# Documentation/Settings.yml: version
# extension/ext_emconf.php: version

ST="'"

IS_DEV=false
if [ "$2" = "true" ]; then
    IS_DEV=true
fi

# Function to remove leading zeros from version components
function remove_leading_zeros() {
    echo "$1" | sed 's/^0\+\([1-9]\)/\1/; s/^0\+$/0/'
}

function confPy() {
#    FILE=Documentation/_make/conf.py
#
#    sed -i -r 's/^\s*version\s*=.+$/version = '"'"$SHORT"'"'/g'  ${FILE}
#    sed -i -r 's/^\s*release\s*=.+$/release = '"'"$VERSION"'"'/g'  ${FILE}

    FILE=Documentation/conf.py

    sed -i -r 's/^\s*version\s*=.+$/version = '"'"$SHORT"'"'/g'  ${FILE}
    sed -i -r 's/^\s*release\s*=.+$/release = '"'"$VERSION"'"'/g'  ${FILE}
}

function settingsCfg() {

    FILE="Documentation/Settings.cfg"

    sed -i -r 's/^\s*version\s*=.+$/version     = '$SHORT'/g'  ${FILE}
    sed -i -r 's/^\s*release\s*=.+$/release     = '$VERSION'/g'  ${FILE}

}

function extEmConf() {
    FILE="extension/ext_emconf.php"
    # Add branch name to version if it is a dev version
    if [ "$IS_DEV" = true ]; then
        VERSION="${VERSION}-${CI_COMMIT_REF_NAME}"
    fi

    sed  -i -r 's/^\s*'${ST}'version'${ST}'\s*=>.+$/    '${ST}'version'${ST}' => '${ST}${VERSION}${ST},'/g'  ${FILE}
}

function versionFile() {
    FILE="version"
    echo ${VERSION} > ${FILE}
}

function newVersionMD() {

  FILE="Documentation-develop/NewVersion.md"

# New version v23.3.0
  sed -i -r 's/New version v[0-9]+\.[0-9]+\.[0-9]/New version v'$VERSION'/g'  ${FILE}

# Tag: v23.3.0
  sed -i -r 's/Tag: v[0-9]+\.[0-9]+\.[0-9]/Tag: v'$VERSION'/g'  ${FILE}

#   git tag -a v23.3.0 -m 'New version v23.3.0' git push
  sed -i -r 's/^\s*git tag -a v[0-9]+\.[0-9]+\.[0-9] /   git tag -a v'$VERSION' /g'  ${FILE}

  sed -i -r 's/^\s*git push -u origin v.+$/   git push -u origin v'$VERSION'/g'  ${FILE}

}

function myExit() {
    [ ! -z "$2" ] && echo "$2"

    exit $1
}

#==================================
OLDIFS=$IFS
IFS=.
set $1
IFS=$OLDIFS

MAJOR="$1"
MINOR="$2"
MICRO="$3"

# in case there is a 'rcX'
[ ! -z "$4" ] && MICRO="${MICRO}.$4"

[ -z "$MAJOR" -o -z "$MINOR" -o -z "$MICRO" ] && echo "Usage: $0 <major>.<minor>.<mirco> [is_dev]" && myExit 1 "Missing Version"

MAJOR=$(remove_leading_zeros "$MAJOR")
MINOR=$(remove_leading_zeros "$MINOR")
MICRO=$(remove_leading_zeros "$MICRO")

VERSION="$MAJOR.$MINOR.$MICRO"
SHORT="$MAJOR.$MINOR"

#confPy
settingsCfg
extEmConf
versionFile
confPy
newVersionMD

