var path = require('path');
module.exports = function (grunt) {
    'use strict';

    var typo3_css = 'extension/Resources/Public/Css/';
    var typo3_css_codemirror = 'extension/Resources/Public/Css/codemirror/';
    var typo3_js = 'extension/Resources/Public/JavaScript/';
    var typo3_fonts = 'extension/Resources/Public/fonts/';
    var typo3_webfonts = 'extension/Resources/Public/webfonts/';
    var js_sources = [
        'javascript/src/Helper/*.js',
        'javascript/src/Element/*.js',
        'javascript/src/*.js'
    ];
    var js_plugins = 'javascript/src/Plugins/*.js';
    var js_worker = 'javascript/src/Worker/*.js';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            bootstrap: {
                files: [
                    {
                        cwd: 'node_modules/bootstrap/dist/css/',
                        src: [
                            'bootstrap.min.css',
                            'bootstrap-theme.min.css'
                        ],
                        dest: typo3_css,
                        filter: 'isFile',
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/bootstrap/dist/js/',
                        src: [
                            'bootstrap.min.js'
                        ],
                        dest: typo3_js,
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/bootstrap/dist/fonts/',
                        expand: true,
                        src: [
                            '*'
                        ],
                        dest: typo3_fonts,
                        flatten: true
                    }
                ]
            },
            bootstrap_dev: {
                files: [
                    {
                        cwd: 'node_modules/bootstrap/dist/css/',
                        src: [
                            'bootstrap.min.css',
                            'bootstrap-theme.min.css'
                        ],
                        dest: 'css/',
                        filter: 'isFile',
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/bootstrap/dist/js/',
                        src: [
                            'bootstrap.min.js'
                        ],
                        dest: 'js/',
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/bootstrap/dist/fonts/',
                        expand: true,
                        src: [
                            '*'
                        ],
                        dest: 'fonts/',
                        flatten: true
                    }
                ]
            },
            jquery: {
                files: [
                    {
                        cwd: 'node_modules/jquery/dist/',
                        src: [
                            'jquery.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/jquery/dist/',
                        src: [
                            'jquery.min.js'
                        ],
                        expand: true,
                        dest: "js/",
                        flatten: true
                    }
                ]
            },
            jquery_tablesorter: {
                files: [
                    {
                        cwd: 'node_modules/tablesorter/dist/js/',
                        src: [
                            'jquery.tablesorter.combined.min.js',
                            'extras/jquery.tablesorter.pager.min.js',
                            'widgets/widget-columnSelector.min.js',
                            'widgets/widget-output.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/tablesorter/dist/js/',
                        src: [
                            'jquery.tablesorter.combined.min.js',
                            'extras/jquery.tablesorter.pager.min.js',
                            'widgets/widget-columnSelector.min.js',
                            'widgets/widget-output.min.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    }
                ]
            },
            bootstrap_datepicker: {
                files: [
                    {
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/js/',
                        src: [
                            'bootstrap-datetimepicker.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/js/',
                        src: [
                            'bootstrap-datetimepicker.min.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/css/',
                        src: [
                            'bootstrap-datetimepicker.css',
                            'bootstrap-datetimepicker.min.css',
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/css/',
                        src: [
                            'bootstrap-datetimepicker.css',
                            'bootstrap-datetimepicker.min.css',
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    }
                ]
            },
            ChartJS: {
                files: [
                    {
                        cwd: 'node_modules/chart.js/dist/',
                        src: [
                            'Chart.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/chart.js/dist/',
                        src: [
                            'Chart.min.js'
                        ],
                        expand: true,
                        dest: "js/",
                        flatten: true
                    }
                ]
            },
            jqwidgets: {
                files: [
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/',
                        src: [
                            'jqx-all.js',
                            'globalization/globalize.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/styles/',
                        src: [
                            'jqx.base.css',
                            'jqx.bootstrap.css'
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/styles/',
                        src: [
                            'images/**'
                        ],
                        expand: true,
                        dest: typo3_css
                    }
                ]
            },
            jqwidgets_devel: {
                files: [
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/',
                        src: [
                            'jqx-all.js',
                            'globalization/globalize.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/styles/',
                        src: [
                            'jqx.base.css',
                            'jqx.bootstrap.css'
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/jqwidgets-framework/jqwidgets/styles/',
                        src: [
                            'images/**'
                        ],
                        expand: true,
                        dest: 'css/'
                    }
                ]
            },
            tinymce: {
                files: [
                    {
                        cwd: 'node_modules/tinymce/',
                        src: [
                            'tinymce.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/tinymce/',
                        src: [
                            'themes/*/theme.min.js',
                            'plugins/*/plugin.min.js',
                            'skins/**'
                        ],
                        dest: typo3_js,
                        expand: true,
                        flatten: false
                    }
                ]
            },
            tinymce_devel: {
                files: [
                    {
                        cwd: 'node_modules/tinymce/',
                        src: [
                            'tinymce.min.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/tinymce/',
                        src: [
                            'themes/*/theme.min.js',
                            'plugins/*/plugin.min.js',
                            'skins/**'
                        ],
                        dest: 'js/',
                        expand: true,
                        flatten: false
                    }
                ]
            },
            fullCalendar: {
                files: [
                    {
                        cwd: 'node_modules/fullcalendar/dist',
                        src: '*.min.js',
                        dest: typo3_js,
                        expand: true,
                        flatten: false
                    },
                    {
                        cwd: 'node_modules/fullcalendar/dist',
                        src: '*.min.css',
                        dest: typo3_css,
                        expand: true,
                        flatten: false
                    },
                    {
                        cwd: 'node_modules/fullcalendar/dist',
                        src: '*.min.*',
                        dest: "js/",
                        expand: true,
                        flatten: false
                    },
                    {
                        cwd: 'node_modules/moment/min',
                        src: [
                            'moment.min.js',
                            'locales.min.js'
                        ],
                        dest: typo3_js,
                        expand: true,
                        flatten: false
                    },
                    {
                        cwd: 'node_modules/moment/min',
                        src: [
                            'moment.min.js',
                            'locales.min.js'
                        ],
                        dest: 'js/',
                        expand: true,
                        flatten: false
                    },
                ]
            },
            eventEmitter: {
                files: [
                    {
                        cwd: 'node_modules/wolfy87-eventemitter/',
                        src: [
                            'EventEmitter.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/wolfy87-eventemitter/',
                        src: [
                            'EventEmitter.min.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    }
                ]
            },
            fabric: {
                files: [
                    {
                        cwd: 'javascript/',
                        src: [
                            'fabric.min.js'
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'javascript/',
                        src: [
                            'fabric.min.js'
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    }
                ]
            },
            typeahead: {
                files: [
                    {
                        cwd: 'node_modules/corejs-typeahead/dist',
                        src: [
                            "typeahead.bundle.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/corejs-typeahead/dist',
                        src: [
                            "typeahead.bundle.min.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    }
                ]
            },
            codemirror: {
                files: [
                    {
                        cwd: 'node_modules/codemirror/lib',
                        src: [
                            "codemirror.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/codemirror/lib',
                        src: [
                            "codemirror.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/codemirror/lib',
                        src: [
                            "codemirror.css"
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/codemirror/theme',
                        src: [
                            "monokai.css"
                        ],
                        expand: true,
                        dest: typo3_css_codemirror,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/codemirror/lib',
                        src: [
                            "codemirror.css"
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/codemirror/mode',
                        src: [
                            "**"
                        ],
                        expand: true,
                        dest: typo3_js + "/code-mirror-mode",
                        flatten: false
                    },
                    {
                        cwd: 'node_modules/codemirror/mode',
                        src: [
                            "**"
                        ],
                        expand: true,
                        dest: "js/code-mirror-mode",
                        flatten: false
                    }
                ]

            },
            qfqPlugins: {
                files: [
                    {
                        src: js_plugins,
                        expand: true,
                        dest: "js/",
                        flatten: true
                    },
                    {
                        src: js_plugins,
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    }
                ]
            },
            filepond: {
                files: [
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.min.css"
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.css"
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.esm.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond/dist',
                        src: [
                            "filepond.esm.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-file-validate-type/dist',
                        src: [
                            "filepond-plugin-file-validate-type.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-file-validate-type/dist',
                        src: [
                            "filepond-plugin-file-validate-type.min.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-file-validate-size/dist',
                        src: [
                            "filepond-plugin-file-validate-size.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-file-validate-size/dist',
                        src: [
                            "filepond-plugin-file-validate-size.min.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-preview/dist',
                        src: [
                            "filepond-plugin-image-preview.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-preview/dist',
                        src: [
                            "filepond-plugin-image-preview.min.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-preview/dist',
                        src: [
                            "filepond-plugin-image-preview.min.css"
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-preview/dist',
                        src: [
                            "filepond-plugin-image-preview.min.css"
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-edit/dist',
                        src: [
                            "filepond-plugin-image-edit.min.js"
                        ],
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-edit/dist',
                        src: [
                            "filepond-plugin-image-edit.min.js"
                        ],
                        expand: true,
                        dest: 'js/',
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-edit/dist',
                        src: [
                            "filepond-plugin-image-edit.min.css"
                        ],
                        expand: true,
                        dest: typo3_css,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/filepond-plugin-image-edit/dist',
                        src: [
                            "filepond-plugin-image-edit.min.css"
                        ],
                        expand: true,
                        dest: 'css/',
                        flatten: true
                    }
                ]
            },
            worker: {
                files: [
                    {
                        src: js_worker,
                        expand: true,
                        dest: "js/Worker/",
                        flatten: true
                    },
                    {
                        src: js_worker,
                        expand: true,
                        dest: typo3_js,
                        flatten: true
                    },
                    {
                        src: js_worker,
                        expand: true,
                        dest: "mockup/Worker/",
                        flatten: true
                    }
                ]
            },
            fontAwesome: {
                files: [
                    {
                        cwd: 'node_modules/@fortawesome/fontawesome-free/css/',
                        src: [
                            'all.min.css'
                        ],
                        dest: typo3_css,
                        filter: 'isFile',
                        expand: true,
                        flatten: true,
                        rename: function(dest, src) {
                            return dest + src.replace('all','font-awesome');
                        }
                    },
                    {
                        cwd: 'node_modules/@fortawesome/fontawesome-free/css/',
                        src: [
                            'font-awesome.min.css'
                        ],
                        dest: 'css/',
                        filter: 'isFile',
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/@fortawesome/fontawesome-free/webfonts',
                        expand: true,
                        src: [
                            '*'
                        ],
                        dest: typo3_webfonts,
                        flatten: true
                    },
                    {
                        cwd: 'node_modules/@fortawesome/fontawesome-free/webfonts',
                        expand: true,
                        src: [
                            '*'
                        ],
                        dest: 'webfonts/',
                        flatten: true
                    }
                ]
            },
            fontPassword: {
                files: [
                    {
                        cwd: 'resources/custom_fonts/',
                        src: [
                            'password-dots.ttf'
                        ],
                        expand: true,
                        dest: 'extension/Resources/Public/fonts/',
                        flatten: true
                    }
                ]
            },
        },
        terser: {
            options: {
                //banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
            },
            build: {
                src: ['js/<%= pkg.name %>.debug.js'],
                dest: typo3_js + '<%= pkg.name %>.min.js'
            },
            qfqFabric: {
                src: ['javascript/src/Plugins/qfq.fabric.js'],
                dest: typo3_js + 'qfq.fabric.min.js'
            },
            qfqValidator: {
                src: ['javascript/src/Plugins/validator.js'],
                dest: typo3_js + 'validator.min.js'
            },
            codeMirror: {
                src: ['node_modules/codemirror/lib/codemirror.js'],
                dest: typo3_js + 'codemirror.min.js'
            },
            codeMirrorSql: {
                src: ['node_modules/codemirror/mode/sql/sql.js'],
                dest: typo3_js + 'code-mirror-mode/sql/sql.min.js'
            }
        },
        jshint: {
            all: js_sources,
            options: {
                'esversion': 10,
            }
        },
        concat_in_order: {
            debug_standalone: {
                options: {
                    extractRequired: function (filepath, filecontent) {
                        var workingdir = path.normalize(filepath).split(path.sep);
                        workingdir.pop();

                        var deps = this.getMatches(/\*\s*@depend\s(.*\.js)/g, filecontent);
                        deps.forEach(function (dep, i) {
                            var dependency = workingdir.concat([dep]);
                            deps[i] = path.join.apply(null, dependency);
                        });
                        return deps;
                    },
                    extractDeclared: function (filepath) {
                        return [filepath];
                    },
                    onlyConcatRequiredFiles: false
                },
                files: {
                    'js/<%= pkg.name %>.debug.js': js_sources
                }
            },
            debug_extension: {
                options: {
                    extractRequired: function (filepath, filecontent) {
                        var workingdir = path.normalize(filepath).split(path.sep);
                        workingdir.pop();

                        var deps = this.getMatches(/\*\s*@depend\s(.*\.js)/g, filecontent);
                        deps.forEach(function (dep, i) {
                            var dependency = workingdir.concat([dep]);
                            deps[i] = path.join.apply(null, dependency);
                        });
                        return deps;
                    },
                    extractDeclared: function (filepath) {
                        return [filepath];
                    },
                    onlyConcatRequiredFiles: false
                },
                files: {
                    'extension/Resources/Public/JavaScript/<%= pkg.name %>.debug.js': js_sources
                }
            }
        },
        less: {
            production: {
                files: {
                    "extension/Resources/Public/Css/qfq-bs.css": "less/qfq-bs.css.less",
                    "extension/Resources/Public/Css/qfq-letter.css": "less/qfq-letter.css.less",
                    "extension/Resources/Public/Css/qfq-plain.css": "less/qfq-plain.css.less",
                    "extension/Resources/Public/Css/tablesorter-bootstrap.css": "less/tablesorter-bootstrap.less"
                },
                options: {
                    compress: true
                }
            },
            devel: {
                files: {
                    "css/qfq-bs.css": "less/qfq-bs.css.less",
                    "css/qfq-letter.css": "less/qfq-letter.css.less",
                    "css/qfq-plain.css": "less/qfq-plain.css.less",
                    "css/tablesorter-bootstrap.css": "less/tablesorter-bootstrap.less"
                },
                options: {
                    banner: "/* Change qfq-bs.css.less, not qfq-bs.css */"
                }
            }
        },
        jasmine: {
            unit: {
                src: ['tests/jasmine/unit/spec/*Spec.js'],
                options: {
                    vendor: [
                        'js/jquery.min.js',
                        'js/bootstrap.min.js',
                        'js/EventEmitter.min.js',
                        'js/jqx-all.js',
                        'js/qfq.debug.js'
                    ],
                    helpers: ['tests/jasmine/helper/mock-ajax.js'],
                    template: 'tests/jasmine/unit/SpecRunner.tmpl'
                }
            }
        },
        watch: {
            scripts: {
                files: js_sources.concat(['less/*.less']),
                tasks: ['only-js', 'less'],
                options: {
                    spawn: true
                }
            }
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-terser');
    //grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-concat-in-order');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'concat_in_order', 'terser', 'copy', 'less']);

    grunt.registerTask('only-js', ['jshint', 'concat_in_order', 'terser', 'copy:qfqPlugins', 'copy:worker']);

    grunt.registerTask('run-jasmine', ['jshint', 'concat_in_order', 'jasmine']);

};