Tested on Ubuntu 16.04 with Docker version 18.09.2

# Other Documentation

More documentation and docker tips are located in:

- https://wikiit.math.uzh.ch/it/bestpractice/Docker
- https://systemvcs.math.uzh.ch/megger/qfq_docker

# Build Images

1. Clone the docker image repository:  
```git clone https://systemvcs.math.uzh.ch/megger/qfq_docker.git```

2. Build images as explained in the README.md of the image repository.
For the below scripts to work please name the images ```python-selenium``` and ```typo3-qfq``` respectively.

# Run and Interact with Containers

## Run qfq in docker container from scratch

1. clone qfq git project

   ```git clone https://git.math.uzh.ch/typo3/qfq```
   
2. change to project directory (qfq)
3. ```make bootstrap```
4. ``` cd docker```
5. ```./run_qfq_docker.sh```
6. open the newly generated file run_qfq_docker.output and copy the value of T3_PORT. In a browser go to 127.0.0.1:<T3_PORT>.

## Deploy qfq extension changes to running container

Assumes you have run qfq in docker as explained above.

1. change to docker directory
2. ```./deploy_to_container.sh```

## Dump QFQ and (truncated) Typo3 databases to docker directory

Assumes you have run qfq in docker as explained above.

1. again from the docker directory run  
```./dump_databases.sh``` (this will overwrite the db_fixtrue_*.sql files)

## Run Phpmyadmin

ATTENTION: Use Firefox if Phpmyadmin login does not work in Chrome!

Assumes you have run qfq in docker as explained above.

1. change to docker directory and run ```./run_phpmyadmin.sh```
2. open the file run_qfq_docker.output and copy the value of PMA_PORT. In a browser (Firefox) go to 127.0.0.1:<PMA_PORT>.

## Run selenium tests on local machine (with visible browser)

Assumes you have run qfq in docker as explained above.

0. Install selenium for python:  ```python3 -m pip install --user selenium```
1. again from the docker directory run ```./run_selenium_tests_local.sh```

## Run a single selenium test file on local machine

Assumes you have run qfq in docker as explained above.

1. copy T3_PORT from docker/run_qfq_docker.output
2. Export variables (replace <T3_PORT>):  
```export SELENIUM_URL="http://127.0.0.1:<T3_PORT>" SELENIUM_HEADLESS="no"```
3. in extension/Tests/selenium run
```python <selenium test file>.py```

## Run selenium tests in docker container (test execution not visible)

Assumes you have run qfq in docker as explained above.

1. from the docker directory run ```./run_selenium_tests_docker.sh```

## Permanently remove all above created containers and their data

(only removes the containers listed in run_qfq_docker.output)

1. from the docker directory run ```./remove-containers.sh```

# TROUBLE SHOOT

## WebDriverException: Message: 'chromedriver' executable needs to be in PATH

1. Download Chromedriver:  
```wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip```

2. Unzip chromedriver to current working directory  
```unzip /tmp/chromedriver.zip chromedriver```

3. export chromedriver path environment variable before running selenium tests locally  
```export CHROMEDRIVER_PATH=<absolute path to chromedriver>/chromedriver```
