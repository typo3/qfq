#!/bin/bash -ex

source run_qfq_docker.output
MYSQL_ROOT_PASSWORD=MN7HKEB7AnvdVKQE

# qfq database dump
mysqldump -u root --password=${MYSQL_ROOT_PASSWORD} --port=${DB_PORT} -h 127.0.0.1 qfq_db > db_fixture_qfq.sql

# typo3 database dump, truncate unimportant tables
to_truncate="
TRUNCATE cache_md5params;
TRUNCATE cache_treelist;
TRUNCATE cf_cache_hash;
TRUNCATE cf_cache_hash_tags;
TRUNCATE cf_cache_imagesizes;
TRUNCATE cf_cache_imagesizes_tags;
TRUNCATE cf_cache_pages;
TRUNCATE cf_cache_pagesection;
TRUNCATE cf_cache_pagesection_tags;
TRUNCATE cf_cache_pages_tags;
TRUNCATE cf_cache_rootline;
TRUNCATE cf_cache_rootline_tags;
TRUNCATE sys_log;
TRUNCATE sys_news;
TRUNCATE sys_note;
TRUNCATE tx_extensionmanager_domain_model_extension;
"
mysql -u root --password=${MYSQL_ROOT_PASSWORD} --port=${DB_PORT} -h 127.0.0.1 -D t3_db -e "$to_truncate"
mysqldump -u root --password=${MYSQL_ROOT_PASSWORD} --port=${DB_PORT} -h 127.0.0.1 t3_db > db_fixture_t3.sql
