#!/bin/bash -ex
#
# First Argument: Path to extension folder

PATH_TO_EXTENSION=../extension
if [[ "$1" ]]; then
    PATH_TO_EXTENSION=$1
fi

source run_qfq_docker.output
docker exec ${T3_CONTAINER} rm -rf /var/www/html/typo3conf/ext/qfq
docker cp ${PATH_TO_EXTENSION} ${T3_CONTAINER}:/var/www/html/typo3conf/ext/qfq
docker exec ${T3_CONTAINER} chown -R www-data:www-data /var/www/html/typo3conf/ext/qfq

# call the qfq website once, since it shows an error the first time it is called
docker exec ${T3_CONTAINER} curl 127.0.0.1:80 > /dev/null