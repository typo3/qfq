#!/bin/bash -ex

source run_qfq_docker.output
source _helper_functions.sh

PMA_CONTAINER_NAME= # if empty choose random
PMA_PORT=0    # if set to 0 choose random

PMA_CONTAINER=$(docker run -d --name "${PMA_CONTAINER_NAME}" -p ${PMA_PORT}:80 --link ${DB_CONTAINER}:db  phpmyadmin/phpmyadmin)
PMA_PORT="$(getHostPort 80 ${PMA_CONTAINER})"
addUsernameToContainerName ${PMA_CONTAINER}

echo "
PMA_CONTAINER=${PMA_CONTAINER}
PMA_PORT=${PMA_PORT}
" >> run_qfq_docker.output

########################################
##           User Output              ##
########################################

echo "Finished. Go to: http://localhost:${PMA_PORT}"