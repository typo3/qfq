#!/bin/bash -ex

source _helper_functions.sh

source run_qfq_docker.output

SELENIUM_IMAGE=python-selenium

makePathExecutable "${PWD}/../" # needed for volume mounting

SELENIUM_CONTAINER=$(docker run \
    --link ${T3_CONTAINER}:typo3container \
    --volume="${PWD}/../":/workingdir \
    --shm-size="2g" \
    -e HOST_UID=${UID} \
    ${SELENIUM_IMAGE} \
    "cd /workingdir/extension/Tests/selenium; python -m unittest discover")
