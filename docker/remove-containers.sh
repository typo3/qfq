#!/bin/bash -e

source _helper_functions.sh

source run_qfq_docker.output

echo "The following containers and their data will be deleted completely:"
echo "$(getContainerName ${DB_CONTAINER}), $(getContainerName ${T3_CONTAINER}), $(getContainerName ${PMA_CONTAINER} 2>/dev/null || true)"
read -p "Are you really really really sure you want to do this? (y/n): " -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "removing..."
	docker rm -fv ${DB_CONTAINER} || true
    docker rm -fv ${T3_CONTAINER} || true
    docker rm -fv ${PMA_CONTAINER} 2>/dev/null || true
    rm run_qfq_docker.output
    echo "done."
fi
