#!/bin/bash -ex

function getHostPort()
{
    local CONTAINER_PORT="$1"
    local CONTAINER="$2"
    echo $(docker inspect -f "{{ (index (index .NetworkSettings.Ports \"${CONTAINER_PORT}/tcp\") 0).HostPort }}" $CONTAINER)
}
function removeContainerAfterTimeout()
{
    local TIMEOUT="$1"
    local CONTAINER="$2"
    (sleep ${TIMEOUT} && docker rm -f ${CONTAINER})&
    disown
}
function makePathExecutable()
(
cd "$(dirname "$1")"
while [[ "$PWD" != "/" ]]
do
    echo "make $PWD executable"
    chmod o+x . || true
    cd ..
done
)
function getContainerName()
{
    local CONTAINER="$1"
    local CONTAINER_NAME=$(docker inspect --format="{{.Name}}" ${CONTAINER})
    echo ${CONTAINER_NAME:1}
}
function addUsernameToContainerName()
{
    local CONTAINER="$1"
    local CONTAINER_NAME=$(getContainerName ${CONTAINER})
    docker rename ${CONTAINER_NAME} ${USER}_${CONTAINER_NAME}
}