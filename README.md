
[![pipeline status](https://git.math.uzh.ch/typo3/qfq/badges/master/pipeline.svg)](https://git.math.uzh.ch/typo3/qfq/commits/master)

QFQ: Quick Form Query - Typo3 Extbase Extension
===============================================

Version: see `extension/ext_emconf.php`

Download
--------

Get from here: https://qfq.io/download

 * release - Stable
 * nightly - Development version(1)
 * snapshot - Current, development version(1)

(1) To install the extension, the name of an uploaded ZIP file has to follow rules:

 * \<ext\>.zip or
 * \<ext\>_\<major\>.\<minor\>[.\<micro\>][_\<something\>].zip

Documentation
-------------

Latest stable version under https://qfq.io/doc

Local: See the documentation provided with the extension inside Typo3
 <your Typo3 installation>/typo3conf/ext/qfq/Documentation/html/Index.html

If you get a 'Page forbidden / not found' there might be restrictions imposed by the webserver. E.g. the Typo3 example of `.htaccess`
in the Typo3 installation folder will forbid access to any extension documentation (which is a good idea on a productive
server). On a development server you can activate the documentation. `.htaccess`: 

<pre>
  production:   RewriteRule (?:typo3conf/ext|typo3/sysext|typo3/ext)/[^/]+/(?:Configuration|Resources/Private|Tests?|Documentation|docs?)/ - [F]
  development:  RewriteRule (?:typo3conf/ext|typo3/sysext|typo3/ext)/[^/]+/(?:Configuration|Resources/Private|Tests?|docs?)/ - [F]
</pre>  
 

