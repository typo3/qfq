<?php

// TEST FILE TO RUN QFQ REPORT WITHOUT TYPO3 //
///////////////////////////////////////////////









//////////////////////////////////////
////////// PAGE DISABLED /////////////
exit(); // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!












const IS_DEBUG = true; // TODO: replace this with better debug meachanism

require_once(__DIR__ . '/../vendor/autoload.php');

use http\Exception;
use IMATHUZH\Qfq\Core\Helper\Path;
use IMATHUZH\Qfq\Core\QuickFormQuery;

class User {
    public $user;
    public function __construct()
    {
        $this->user = [
            "uid" => 1, // Backend user: 2
            // $feUserLoggedIn = isset($GLOBALS["TSFE"]->fe_user->user["uid"]) && $GLOBALS["TSFE"]->fe_user->user["uid"] > 0;
            // $arr["fe_user_uid"] = $GLOBALS["TSFE"]->fe_user->user["uid"] ?? '-';
            // $arr["be_user_uid"] = (isset($GLOBALS['BE_USER'])) ? $GLOBALS['BE_USER']->user["uid"] : '-';
            // $feUidLoggedIn = $GLOBALS["TSFE"]->fe_user->user["uid"] ?? false;
            // $t3vars[TYPO3_FE_USER_UID] = isset($GLOBALS["TSFE"]->fe_user->user["uid"]) ? $GLOBALS["TSFE"]->fe_user->user["uid"] : '';

            "username" => "megger", // Backend user: "megger"
            // $arr["fe_user"] = $GLOBALS["TSFE"]->fe_user->user["username"] ?? '-';
            // $feUserSession = $GLOBALS["TSFE"]->fe_user->user["username"] ?? false;
            // $beUser = $GLOBALS["BE_USER"]->user["username"] ?? false;
            // if (isset($GLOBALS["TSFE"]->fe_user->user["username"]) && isset($_COOKIE['fe_typo_user'])) {
            // $t3vars[TYPO3_FE_USER] = isset($GLOBALS["TSFE"]->fe_user->user["username"]) ? $GLOBALS["TSFE"]->fe_user->user["username"] : '';
            // $t3vars[TYPO3_BE_USER] = isset($GLOBALS["BE_USER"]->user["username"]) ? $GLOBALS["BE_USER"]->user["username"] : '';

            "usergroup"=> "1" // Backend user: ""
            // $feUserGroup = $GLOBALS["TSFE"]->fe_user->user["usergroup"] ?? false;
            // $t3vars[TYPO3_FE_USER_GROUP] = isset($GLOBALS["TSFE"]->fe_user->user["usergroup"]) ? $GLOBALS["TSFE"]->fe_user->user["usergroup"] : '';
        ];
    }

    public function logoff()
    {
        // $GLOBALS['TSFE']->fe_user->logoff();
    }
}

class TSFE
{
    public $fe_user;

    public $id = 1;
    // $arr["page_id"] = $GLOBALS["TSFE"]->id;
    // $t3vars[TYPO3_PAGE_ID] = isset($GLOBALS["TSFE"]->id) ? $GLOBALS["TSFE"]->id : '';

    public $type = 0;
    // $arr["page_type"] = $GLOBALS["TSFE"]->type;
    // $t3vars[TYPO3_PAGE_TYPE] = isset($GLOBALS["TSFE"]->type) ? $GLOBALS["TSFE"]->type : '';

    public $sys_language_uid = 0;
    // $arr["page_language_uid"] = $GLOBALS["TSFE"]->sys_language_uid;
    // $t3vars[TYPO3_PAGE_LANGUAGE] = isset($GLOBALS["TSFE"]->sys_language_uid) ? $GLOBALS["TSFE"]->sys_language_uid : '';

    public $beUserLogin = true;
    // return (!empty($GLOBALS["TSFE"]->beUserLogin) && $GLOBALS["TSFE"]->beUserLogin === true);
    // $t3vars[TYPO3_BE_USER_LOGGED_IN] = (isset($GLOBALS["TSFE"]->beUserLogin) && $GLOBALS["TSFE"]->beUserLogin === true) ? 'yes' : 'no';

    public $page = [
        "alias" => 1,
        // $t3vars[TYPO3_PAGE_ALIAS] = empty($GLOBALS["TSFE"]->page["alias"]) ? $t3vars[TYPO3_PAGE_ID] : $GLOBALS["TSFE"]->page["alias"];

        "title" => "two"
        // $t3vars[TYPO3_PAGE_TITLE] = isset($GLOBALS["TSFE"]->page["title"]) ? $GLOBALS["TSFE"]->page["title"] : '';
    ];

    public function __construct()
    {
        $this->fe_user = new User();
        // if (isset($GLOBALS["TSFE"]->fe_user)) {
    }
}


$extConf = [
    "flagProduction" => "yes",
    "render" => "both",
    "maxFileSize" => "",
    "baseUrl" => "",
    "dateFormat" => "dd.mm.yyyy",
    "thumbnailDirSecure" => "fileadmin/protected/qfqThumbnail",
    "thumbnailDirPublic" => "typo3temp/qfqThumbnail",
    "cmdInkscape" => "inkscape",
    "cmdConvert" => "convert",
    "cmdWkhtmltopdf" => "/opt/wkhtmltox/bin/wkhtmltopdf",
    "cmdQpdf" => "qpdf",
    "cmdGs" => "gs",
    "cmdPdfunite" => "pdfunite",
    "sendEMailOptions" => "",
    "documentation" => "https://docs.typo3.org/typo3cms/drafts/github/T3DocumentationStarter/Public-Info-053/Manual.html",
    "fillStoreSystemBySql1" => "",
    "fillStoreSystemBySqlErrorMsg1" => "",
    "fillStoreSystemBySql2" => "",
    "fillStoreSystemBySqlErrorMsg2" => "",
    "fillStoreSystemBySql3" => "",
    "fillStoreSystemBySqlErrorMsg3" => "",
    "throwExceptionGeneralError" => "auto",
    "formSubmitLogMode" => "all",
    "redirectAllMailTo" => "",
    "sqlLogMode" => "modify",
    "sqlLogModeAutoCron" => "error",
    "sqlLog" => "fileadmin/protected/log/sql.log",
    "qfqLog" => "fileadmin/protected/log/qfq.log",
    "mailLog" => "fileadmin/protected/log/mail.log",
    "showDebugInfo" => "auto",
    "init" => "SET names utf8; SET sql_mode = \"NO_ENGINE_SUBSTITUTION\";",
    "update" => "auto",
    "indexData" => "1",
    "indexQfq" => "1",
    "escapeTypeDefault" => "m",
    "securityVarsHoneypot" => "email,username,password",
    "securityAttackDelay" => "5",
    "securityShowMessage" => "true",
    "securityGetMaxLength" => "50",
    "securityFailedAuthDelay" => "3",
    "sessionTimeoutSeconds" => "1800",
    "recordLockTimeoutSeconds" => "900",
    "enterAsSubmit" => "1",
    "editFormPage" => "form",
    "formDataPatternError" => "",
    "formDataRequiredError" => "",
    "formDataMatchError" => "",
    "formDataError" => "",
    "showIdInFormTitle" => "0",
    "cssClassColumnId" => "text-muted",
    "labelAlign" => "left",
    "cssClassQfqContainer" => "",
    "cssClassQfqForm" => "",
    "cssClassQfqFormPill" => "qfq-color-grey-1",
    "cssClassQfqFormBody" => "qfq-color-grey-2",
    "formBsColumns" => "col-md-12",
    "formBsLabelColumns" => "col-md-3 col-lg-3",
    "formBsInputColumns" => "col-md-6 col-lg-6",
    "formBsNoteColumns" => "col-md-3 col-lg-3",
    "extraButtonInfoInline" => "<span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span>",
    "extraButtonInfoBelow" => "<span class=\"glyphicon glyphicon-info-sign text-info\" aria-hidden=\"true\"></span>",
    "extraButtonInfoPosition" => "auto",
    "extraButtonInfoClass" => "",
    "formLanguageAId" => "",
    "formLanguageALabel" => "",
    "formLanguageBId" => "",
    "formLanguageBLabel" => "",
    "formLanguageCId" => "",
    "formLanguageCLabel" => "",
    "formLanguageDId" => "",
    "formLanguageDLabel" => "",
    "saveButtonText" => "",
    "saveButtonTooltip" => "Save",
    "saveButtonClass" => "btn btn-default navbar-btn",
    "saveButtonClassOnChange" => "alert-info btn-info",
    "saveButtonGlyphIcon" => "glyphicon-ok",
    "closeButtonText" => "",
    "closeButtonTooltip" => "Close",
    "closeButtonClass" => "btn btn-default navbar-btn",
    "closeButtonGlyphIcon" => "glyphicon-remove",
    "deleteButtonText" => "",
    "deleteButtonTooltip" => "Delete",
    "deleteButtonClass" => "btn btn-default navbar-btn",
    "deleteButtonGlyphIcon" => "glyphicon-trash",
    "newButtonText" => "",
    "newButtonTooltip" => "New",
    "newButtonClass" => "btn btn-default navbar-btn",
    "newButtonGlyphIcon" => "glyphicon-plus",
    "custom1" => "",
    "custom2" => "",
    "custom3" => "",
    "custom4" => "",
    "custom5" => "",
    "custom6" => "",
    "custom7" => "",
    "custom8" => "",
    "custom9" => "",
    "custom10" => "",
    "custom11" => "",
    "custom12" => "",
    "custom13" => "",
    "custom14" => "",
    "custom15" => "",
    "custom16" => "",
    "custom17" => "",
    "custom18" => "",
    "custom19" => "",
    "custom20" => "",
    "custom21" => "",
    "custom22" => "",
    "custom23" => "",
    "custom24" => "",
    "custom25" => "",
    "custom26" => "",
    "custom27" => "",
    "custom28" => "",
    "custom29" => "",
    "custom30" => "",
    "cmdImg2pdf" => "img2pdf"
];


$GLOBALS["TSFE"] = new TSFE();
// if (isset($GLOBALS["TSFE"])) {

$GLOBALS["BE_USER"] = new User();   /// THIS STOPS RENDERING

$GLOBALS['TYPO3_CONF_VARS'] = [
    'EXT' => ['extConf'=>[EXT_KEY=>serialize($extConf)]], // THIS STOPS RENDERING
    // $configT3qfq = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][EXT_KEY]);

    'DB' => [
        "additionalQueryRestrictions" => [],
        "Connections" => [
            "Default" => [
                "charset" => "utf8",
                "dbname" => "megger_qfq_t3",
                "driver" => "mysqli",
                "host" => "127.0.0.1",
                "password" => "R76HL8s6xsDdHiuu",
                "port" => 3306,
                "user" => "megger_qfq"
            ]
        ]
    ]
    // $configT3qfq[SYSTEM_DB_NAME_T3] = self::getDbName($GLOBALS['TYPO3_CONF_VARS']['DB']);
];

$GLOBALS['TYPO3_LOADED_EXT'] = [
    EXT_KEY => ['ext_localconf.php' => '/var/www/html/megger/qfq/typo3conf/ext/qfq/ext_localconf.php']
    // if ($pos === false && isset($GLOBALS['TYPO3_LOADED_EXT'][EXT_KEY]['ext_localconf.php'])) {
    // $config[SYSTEM_EXT_PATH] = dirname($GLOBALS['TYPO3_LOADED_EXT'][EXT_KEY]['ext_localconf.php']);
];

// Cookies
// In SessionCookie.php all cookie data is saved in a file. but this isonly used in Html2Pdf.php:  foreach ($_COOKIE as $name => $value) {

$_COOKIE[SESSION_NAME] = "rnhngphhsia6nulti8mqe35i80";
// isset($_COOKIE[SESSION_NAME])
// "Cookie: " . SESSION_NAME . "=" . $_COOKIE[SESSION_NAME] . "\r\n",
// $cookie = isset($_COOKIE[SESSION_NAME]) ? $_COOKIE[SESSION_NAME] : '<no session cookie>';
// $feUser = $_COOKIE[SESSION_NAME] ?? 'fake';
// if (isset($_COOKIE[SESSION_NAME])) { $cookie[CLIENT_COOKIE_QFQ] = $_COOKIE[SESSION_NAME]; }
// if (isset($_COOKIE[SESSION_NAME])) {    unset($_COOKIE[SESSION_NAME]);    setcookie(SESSION_NAME, '', time() - 86400, '/'); }

$_COOKIE['fe_typo_user'] = "1d4751e1531fac9e145914bc12cb83f6";
// $cookieFe = ($_COOKIE['fe_typo_user']) ?? false;
// if (isset($GLOBALS["TSFE"]->fe_user->user["username"]) && isset($_COOKIE['fe_typo_user'])) {

////
echo <<<EOF

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" media="all">
<link rel="stylesheet" type="text/css" href="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/Css/bootstrap-theme.min.css?1594738199" media="all">
<link rel="stylesheet" type="text/css" href="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/Css/qfq-bs.css" media="all">
<link rel="stylesheet" type="text/css" href="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/Css/tablesorter-bootstrap.css?1594738199" media="all">
<link rel="stylesheet" type="text/css" href="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/Css/font-awesome.min.css?1594738199" media="all">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/validator.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/tinymce.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/EventEmitter.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/qfq.debug.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.combined.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/jquery.tablesorter.pager.min.js" type="text/javascript"></script>
<script src="https://webwork16.math.uzh.ch/megger/qfq/typo3conf/ext/qfq/Resources/Public/JavaScript/widget-columnSelector.min.js" type="text/javascript"></script>
EOF;

$t3data = [
    "uid" => 3,
    "pid" => 3,
    "t3ver_oid" => 0,
    "t3ver_id" => 0,
    "t3ver_wsid" => 0,
    "t3ver_label" => "",
    "t3ver_state" => 0,
    "t3ver_stage" => 0,
    "t3ver_count" => 0,
    "t3ver_tstamp" => 0,
    "t3ver_move_id" => 0,
    "t3_origuid" => 0,
    "tstamp" => 1596017434,
    "crdate" => 1544445105,
    "cruser_id" => 2,
    "editlock" => 0,
    "hidden" => 0,
    "sorting" => 256,
    "CType" => "qfq_qfq",
    "header" => "QFQ: Person",
    "header_position" => "",
    "rowDescription" => "",
    "bodytext" => "file=home/two/qfq-person.qfqr",
    "bullets_type" => 0,
    "uploads_description" => 0,
    "uploads_type" => 0,
    "assets" => 0,
    "image" => 0,
    "imagewidth" => 0,
    "imageorient" => 0,
    "imagecols" => 2,
    "imageborder" => 0,
    "media" => 0,
    "layout" => 0,
    "frame_class" => "default",
    "deleted" => 0,
    "cols" => 0,
    "spaceBefore" => 0,
    "spaceAfter" => 0,
    "space_before_class" => "",
    "space_after_class" => "",
    "records" => null,
    "pages" => null,
    "starttime" => 0,
    "endtime" => 0,
    "colPos" => 0,
    "subheader" => "",
    "fe_group" => "",
    "header_link" => "",
    "image_zoom" => 0,
    "header_layout" => "0",
    "list_type" => "",
    "sectionIndex" => 1,
    "linkToTop" => 0,
    "file_collections" => null,
    "filelink_size" => 0,
    "filelink_sorting" => "",
    "target" => "",
    "date" => 0,
    "recursive" => 0,
    "imageheight" => 0,
    "sys_language_uid" => 0,
    "pi_flexform" => null,
    "accessibility_title" => "",
    "accessibility_bypass" => 0,
    "accessibility_bypass_text" => "",
    "l18n_parent" => 0,
    "l18n_diffsource" => "a:15:{s:6:\"header\";N;s:5:\"CType\";N;s:8:\"bodytext\";N;s:6:\"hidden\";N;s:8:\"fe_group\";N;s:9:\"starttime\";N;s:7:\"endtime\";N;s:6:\"colPos\";N;s:15:\"header_position\";N;s:16:\"sys_language_uid\";N;s:6:\"layout\";N;s:13:\"header_layout\";N;s:9:\"linkToTop\";N;s:12:\"sectionIndex\";N;s:10:\"categories\";N;}",
    "l10n_source" => 0,
    "selected_categories" => null,
    "category_field" => "",
    "table_class" => "",
    "table_caption" => null,
    "table_delimiter" => 124,
    "table_enclosure" => 0,
    "table_header_position" => 0,
    "table_tfoot" => 0,
    "tx_impexp_origuid" => 0,
    "l10n_state" => null,
    "categories" => 0
];

$html = '';
$origErrorReporting = '';
$flagOk = false;

try {

    // By T3 default 'E_NOTICE' is unset. E.g. 'Undefined Index' will throw an exception.
    // QFQ like to see those 'E_NOTICE'
    $origErrorReporting = error_reporting();
    error_reporting($origErrorReporting | E_NOTICE);

    $t3data["bodytext"] = "file=Home/selenium/QFQ__basicform.qfqr";
    $qfq = new QuickFormQuery($t3data);
    $html = $qfq->process();
    $html = '<div class="container">' . $html . '</div>';
    $flagOk = true;

} catch (\UserFormException $e) {
    $html = $e->formatMessage();

} catch (\UserReportException $e) {
    $html = $e->formatMessage();

} catch (\CodeException $e) {
    $html = $e->formatMessage();

} catch (\DbException $e) {
    $html = $e->formatMessage();

} catch (\ShellException $e) {
    $html = $e->formatMessage();

} catch (\DownloadException $e) {
    $html = $e->formatMessage();

} catch (\Error | \Exception $e) {
    $html = "Generic Exception: " . $e->getMessage() . (IS_DEBUG ? ("<br>" . nl2br($e->getTraceAsString())) : '');
}

if (isset($e) && $e->getCode() == ERROR_QUIT_QFQ_REGULAR) {
    $flagOk = true;
}

if (!$flagOk) {
    $html = "<div class='alert alert-warning'>$html</div>";
}

// Restore has to be outside of try/catch - E_NOTICE needs to unset for further T3 handling after an QFQ Exception.
error_reporting($origErrorReporting);

echo $html;