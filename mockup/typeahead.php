<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/jqx.base.css">
    <link rel="stylesheet" href="../css/qfq-bs.css">
    <title>Type Ahead Mock</title>
</head>
<body>


<label>Submit to
    <select name="submitTo" id="submitTo">
        <option>404 error</option>
        <option>save_error_matno.json</option>
        <option>save_error_geburtstag.json</option>
        <option>save_no_redirect.json</option>
        <option>save_server_redirect.json</option>
        <option>save_client_redirect.json</option>
    </select>
</label>

<label>Delete URL
    <select name="deleteUrl" id="deleteUrl">
        <option>404 error</option>
        <option>delete_client_redirect.json</option>
        <option>delete_error.json</option>
        <option>delete_no_redirect.json</option>
        <option>delete_server_redirect.json</option>
    </select>
</label>

<label>Upload to
    <select name="uploadTo" id="uploadTo">
        <option>404 error</option>
        <option>uploadhandler.php</option>
        <option>uploadhandler_error.php</option>
    </select>
</label>

<label>File Delete Url
    <select name="fileDeleteUrl" id="fileDeleteUrl">
        <option>404 error</option>
        <option>delete_file_ok.json</option>
        <option>delete_file_error.json</option>
    </select>
</label>


<div class="container-fluid">
    <div class="row hidden-xs">
        <div class="col-md-12">
            <h1>Title with a long text</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="btn-toolbar pull-right" role="toolbar">
                <div class="btn-group" role="group">
                    <button id="save-button" type="button" class="btn btn-default navbar-btn"
                            data-class-on-change="wdc"><span
                            class="glyphicon glyphicon-ok"></span></button>
                    <button id="close-button" type="button" class="btn btn-default navbar-btn"><span
                            class="glyphicon glyphicon-remove"></span></button>
                </div>
                <div class="btn-group" role="group">
                    <button id="delete-button" type="button" class="btn btn-default navbar-btn"><span
                            class="glyphicon glyphicon-trash"></span></button>
                </div>
                <div class="btn-group" role="group">
                    <a id="form-new-button" href="personmock.html?s=badcaffe1" class="btn btn-default navbar-btn"><span
                            class="glyphicon glyphicon-plus"></span></a>
                </div>
            </div>
        </div>

    </div>


    <form id="myForm" class="form-horizontal" data-toggle="validator">

        <div id="formgroup1" class="form-group">
            <div class="col-md-2">
                <label for="dropdown1" class="control-label">Text input</label>
            </div>

            <div class="col-md-6">
                <input id="dropdown1" type="text" class="form-control qfq-typeahead" name="dropdown1"
                       data-typeahead-sip="abcde" data-typeahead-minlength="1" data-typeahead-limit="3">
            </div>

        </div>

        <div id="formgroup2" class="form-group">
            <div class="col-md-2">
                <label for="dropdown2" class="control-label">Text input 2 (pedantic, required)</label>
            </div>

            <div class="col-md-6">
                <input id="dropdown2" type="text" class="form-control qfq-typeahead" name="dropdown2"
                       data-typeahead-sip="abcdef" data-typeahead-limit="10" data-typeahead-minlength="1"
                       data-typeahead-pedantic="true" required>
            </div>

            <div class="col-md-4">
                <p class="help-block with-errors"></p>
            </div>

        </div>

        <div id="formgroup3" class="form-group">
            <div class="col-md-2">
                <label for="dropdown3" class="control-label">Text input 3 (prefilled)</label>
            </div>

            <div class="col-md-6">
                <input id="dropdown3" type="text" class="form-control qfq-typeahead" name="dropdown3"
                       data-typeahead-sip="abcdef" data-typeahead-limit="10" data-typeahead-minlength="1"
                       value="TX">
            </div>

        </div>

        <?php

        $tags = [
            ['value' => "Alabama", 'key' => "AL"],
            ['value' => "Alaska", 'key' => "AK"]
        ];

        $tagsSafeJson = htmlentities(json_encode($tags), ENT_QUOTES, 'UTF-8');

        ?>

        <div id="formgroup4" class="form-group">
            <div class="col-md-2">
                <label for="tags1" class="control-label">Text input 4 (tags)</label>
            </div>

            <div class="col-md-6">
                <input id="tags1" type="hidden" class="form-control qfq-typeahead" name="tags1"
                       data-typeahead-sip="abcdef"
                       data-typeahead-limit="10"
                       data-typeahead-minlength="0"

                       data-typeahead-tags="true"
                       data-typeahead-pedantic="0"
                       data-typeahead-tag-delimiters="[9, 13]"
                       value="<?php echo $tagsSafeJson; ?>"
                       >
            </div>

        </div>

        <div id="formgroup5" class="form-group">
            <div class="col-md-2">
                <label for="tags2" class="control-label">Text input 4 (tags, pedantic)</label>
            </div>

            <div class="col-md-6">
                <input id="tags2" type="hidden" class="form-control qfq-typeahead" name="tags2"
                       data-typeahead-sip="abcdef"
                       data-typeahead-limit="10"
                       data-typeahead-minlength="1"

                       data-typeahead-tags="true"
                       data-typeahead-pedantic="true"
                       data-typeahead-tag-delimiters="[9, 44]"
                       value="<?php echo $tagsSafeJson; ?>"
                >
            </div>

        </div>

    </form>
</div>

<script type="text/x-handlebars-template">

</script>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/validator.min.js"></script>
<script src="../js/jqx-all.js"></script>
<script src="../js/EventEmitter.min.js"></script>
<script src="../js/typeahead.bundle.min.js"></script>
<script src="../js/qfq.debug.js"></script>
<script type="text/javascript">
    $(function () {

        // Do not remove, or selenium tests will fail.
        QfqNS.QfqPage.prototype.beforeUnloadHandler = function (evt) {
        };

        var qfqPage = new QfqNS.QfqPage({
            tabsId: 'myTabs',
            formId: 'myForm',
            submitTo: 'api/' + $("#submitTo").val(),
            deleteUrl: 'api/' + $("#deleteUrl").val(),
            fileUploadTo: 'api/' + $("#uploadTo").val(),
            fileDeleteUrl: 'api/' + $("#fileDeleteUrl").val(),
            typeAheadUrl: 'api/typeahead.php'
        });

        $("#submitTo").on("change", function (evt) {
            qfqPage.settings.submitTo = 'api/' + $(evt.target).val();
            qfqPage.qfqForm.submitTo = 'api/' + $(evt.target).val();
        });

        $("#deleteUrl").on("change", function (evt) {
            qfqPage.settings.deleteUrl = 'api/' + $(evt.target).val();
            qfqPage.qfqForm.deleteUrl = 'api/' + $(evt.target).val();
        });

        $("#uploadTo").on("change", function (evt) {
            qfqPage.settings.fileUploadTo = 'api/' + $(evt.target).val();
            qfqPage.qfqForm.fileUploader.targetUrl = 'api/' + $(evt.target).val();
        });

        $("#fileDeleteUrl").on("change", function (evt) {
            qfqPage.settings.fileDeleteUrl = 'api/' + $(evt.target).val();
            qfqPage.qfqForm.fileDeleter.targetUrl = 'api/' + $(evt.target).val();
        });

        $('#myForm').on('invalid', function () {
            console.log("Invalid event catched");
        });

        QfqNS.Log.level = 0;
    });
</script>
</body>
</html>
