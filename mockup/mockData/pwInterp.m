function [pc] = pwInterp(N,I,m,h,x)
%Input: [a,b] Interval, N number of subintervals, m local polynomial degree,
% f values of f at m*N+ 1 nodes, x evaluation points.
% Output: p values of the interpolation polynomial at given evalutation
% points.
% test with pwInterp(4,[-4,4],1,@(x) sin(x).* cos(x),[linspace(-4,4,1000)])

xi=linspace(I(1),I(2),m*N+1);
f=h(xi);
n=length(xi);
y1=zeros(n,n+1);
p=zeros(length(x),N);

for p=1:n
    y1(p,1)=xi(p);
    y1(p,2)=f(p);
end
b=zeros(m+1,N);
for i=1:N
y=zeros(m+1,m+2);
        y(1:m+1,1)=y1((i-1)*m+1:i*m+1,1);
        y(1:m+1,2)=y1((i-1)*m+1:i*m+1,2);
for k=2:m+1
    for j=3:k+1
        if j==3
            y(k,j)= ( y(k,2) - y(k-1,2) ) / ( y(k,1) - y(k-1,1) );
        else
            y(k,j)= ( y(k,j-1) - y(k-1,j-1)) / ( y(k,1) - y(k-(j-2),1) );
        end
    end
end
for l=1:m+1
    b(l,i)=y(l,l+1);
end
x1=linspace(I(1),I(2),N+1);
x1=linspace(x1(i),x1(i+1),length(x)/N);
n=m+1;
   for o=1:length(x1)
    w1=1;
    p0=y(1,2);
    for l=1:n-1
      w0=(x1(o)-y(l,1))*w1;
      w1=w0;
      ps=p0 + (b(l+1,i)*w1);
      p0=ps;

      p(o,i)=ps;

    end

   end
end

pc=p(:);

for i=1:N-1
      pc(i*length(x1)-i+1) = [];

end

end


fileadmin/shorttimedata/18m/studentupload/fs18/MAT801/16720096/0/56901.pwInterp.m