<?php
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
session_name("SANDBOXSESSION");
session_start();
session_destroy();
setcookie("SANDBOXSESSION", "", time() - 3600, '/');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Session Sandbox</title>
</head>

<body>
<h1>Session destroyed</h1>

<p>
    Go <a href="sessionFiller.php" title="back">back</a>
</p>
</body>

</html>
