<?php
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

session_name("SANDBOXSESSION");
session_start();

if (isset($_POST['VALUE_A'])) {
    $_SESSION['A'] = $_POST['VALUE_A'];
}
if (isset($_POST['VALUE_B'])) {
    $_SESSION['B'] = $_POST['VALUE_B'];
}

if (!isset($_SESSION['A'])) {
    $_SESSION['A'] = "New Session value A";
}

if (!isset($_SESSION['B'])) {
    $_SESSION['B'] = "New Session value B";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Session Sandbox</title>
</head>

<body>
<h1>Session Sandbox</h1>

<p>
    Current Session id: <?php echo session_id(); ?>
</p>

<p>
    Current Cookies
<ul>
    <?php
    foreach ($_COOKIE as $name => $value) {
        echo "<li>" . $name . " => " . $value . "</li>";
    }
    ?>
</ul>
</p>

<h2>Session Values</h2>

<div>
    Value A: <?php echo htmlentities($_SESSION['A']) ?>
</div>

<div>
    Value B: <?php echo htmlentities($_SESSION['B']) ?>
</div>

<h2>Set Session Values</h2>

<form method="post">
    <div>
        <label>
            New Value A <input type="text" name="VALUE_A" placeholder="new value A">
        </label>
    </div>
    <div>
        <label>
            New Value B <input type="text" name="VALUE_B" placeholder="new value B">
        </label>
    </div>

    <div>
        <button type="submit" name="submit">Submit</button>
    </div>
</form>
<p>
    To <a href="sessionToPdf.php" title="Make pdf">pdf</a>
</p>

<p>
    <a href="sessionDestroy.php" title="Destroy session">Destroy</a> session
</p>
</body>
</html>
