<?php
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */
session_name("SANDBOXSESSION");
session_start();

header("Content-Type: application/pdf");
header("Content-Disposition: attachment; filename=\"file.pdf\"");

unlink("/tmp/cookie.jar");
foreach ($_COOKIE as $name => $value) {
    // e.g.: SANDBOXSESSION=a83f1o69jbv12932q54hmgphk6; domain=qfq.math.uzh.ch; path=/;
    $line = $name . "=" . $value . "; domain=qfq.math.uzh.ch; path=/;\n";
    file_put_contents("/tmp/cookie.jar", $line, FILE_APPEND);
}

system('/opt/wkhtmltox/bin/wkhtmltopdf -q --cookie-jar /tmp/cookie.jar http://qfq.math.uzh.ch/raos/qfq/sandbox/sessionFiller.php /tmp/bla.pdf </dev/null');

echo file_get_contents("/tmp/bla.pdf");

?>