<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/tablesorter-bootstrap.css">

    <link rel="stylesheet" href="../css/qfq-bs.css">
    <title>Input Mode Switcher</title>

</head>
<body style="background-color: #f5f5f5;">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

            <br><br><br><br><br>

            <?php

            $tablesorterViews = [
                [
                    "name" => "Default",
                    "public" => true,
                    "uid" => 235645,
                    "tableId" => "tableNr1",
                    "view" => "eyJjb2x1bW5TZWxlY3Rpb24iOlswLDFdLCJmaWx0ZXJzIjpbIiIsImhpIl0sInNvcnRMaXN0IjpbWzAsMV1dfQ==
"
                ],
                [
                    "name" => "main",
                    "public" => false,
                    "uid" => 235645,
                    "tableId" => "tableNr1",
                    "view" => "eyJjb2x1bW5TZWxlY3Rpb24iOlswLDNdLCJmaWx0ZXJzIjpbIm5vIl0sInNvcnRMaXN0IjpbWzAsMV1dfQ=="
                ]
            ];

            $tablesorterViewsSafeJson = htmlentities(json_encode($tablesorterViews), ENT_QUOTES, 'UTF-8');
            $tablesorterTableId = 1;
            $tablesorterUid = 1234;
            $tablesorterSip = 'xy3424kljn';

            ?>

            <table class="table table-hover qfq-table-50 tablesorter tablesorter-filter tablesorter-column-selector"
                   data-tablesorter-view="<?php echo $tablesorterViewsSafeJson; ?>"
                   data-tablesorter-id="<?php echo $tablesorterTableId; ?>"
                   data-tablesorter-sip="<?php echo $tablesorterSip; ?>">

                <tr>
                    <thead>
                    <th><a href="#" class="btn btn-default"><span class="glyphicon glyphicon-plus" ></span></a></th>
                    <th>Nr</th>
                    <th>Submitted</th>
                    <th>Status</th>
                    <th>Labels</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Title</th>
                    <th>Email</th>
                    <th>Affiliation</th>
                    <th>Other Affiliation</th>
                    <th>Position</th>
                    <th>Other Position</th>
                    <th>Supervisor Name</th>
                    <th>Supervisor Affiliation</th>
                    <th>Supervisor Other Affiliation</th>
                    <th>Supervisor Email</th>
                    <th>Purpose</th>
                    <th>Other Purpose</th>
                    <th>Experiment Descr.</th>
                    <th>Research Questions</th>
                    <th>Statistical Questions</th>
                    <th>Other Statistical Questions</th>
                    <th>Descr. of Stat. Questions</th>
                    <th>Software</th>
                    <th>Other Software</th>
                    <th>Previous Consulting</th>
                    <th>Last Consulting</th>
                    <th>Last Consulting ID</th>
                    <th>Attached Files</th>
                    <th>del</th>
                    </thead>
                </tr>
                <tr>
                    <td><a href="#" class="btn btn-default" ><span class="glyphicon glyphicon-pencil" ></span></a></td>
                    <td>2019-aw-001</td>
                    <td>2019/04/18</td>
                    <td>in progress</td>
                    <td></td>
                    <td>Hans</td>
                    <td>Becker</td>
                    <td></td>
                    <td>hans.becker@uzh.ch</td>
                    <td>University Medicine inc. USZ</td>
                    <td></td>
                    <td>PhD candidate</td>
                    <td></td>
                    <td>Roger Gruber</td>
                    <td>Faculty of Science</td>
                    <td></td>
                    <td>aslkdf@usz.ch</td>
                    <td>Publication</td>
                    <td></td>
                    <td>Es war einmal ein Frosch. </td>
                    <td>Dieser Frosch war lokal inspiriert.</td>
                    <td>Ein Frosch der voller Ueberraschungen war.</td>
                    <td></td>
                    <td>Ein Frosch gegen den Rest der Welt.</td>
                    <td>R</td>
                    <td></td>
                    <td>No</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="#" class="btn btn-default"><span class="glyphicon glyphicon-trash" ></span></a></td>
                </tr>
            </table>

        </div>
    </div>

</div>
<p><br></p>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/validator.min.js"></script>
<script src="../js/EventEmitter.min.js"></script>
<script src="../js/qfq.debug.js"></script>
<script src="../js/jquery.tablesorter.combined.min.js"></script>
<script src="../js/jquery.tablesorter.pager.min.js"></script>
<script src="../js/widget-columnSelector.min.js"></script>

<script type="text/javascript">
    window.tablesorterMockApi = 'api/tablesorter.php';

    $(function () {

        var qfqPage = new QfqNS.QfqPage({
            tabsId: 'myTabs',
            formId: 'myForm',
            submitTo: 'api/' + $("#submitTo").val(),
            deleteUrl: 'api/' + $("#deleteUrl").val(),
            fileUploadTo: 'api/' + $("#uploadTo").val(),
            fileDeleteUrl: 'api/' + $("#fileDeleteUrl").val()
        });

        QfqNS.Log.level = 0;

    });
</script>
</body>
</html>