<?php
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

header("Content-Type: text/json");

foreach ($_FILES as $key => &$value) {
    $value['file_content'] = file_get_contents($value['tmp_name']);
}

echo json_encode([
    'status' => "ok",
    'files_received' => $_FILES,
    'request_variables' => $_REQUEST
]);

