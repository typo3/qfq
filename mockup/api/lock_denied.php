<?php

header('Content-type: application/json');
if (!isset($_GET['s']) ||
    !isset($_GET['action'])
) {
    http_response_code(400);
    echo json_encode([
        'status' => 'error',
        'message' => 'Bad Request'
    ]);
    exit(0);
}

switch ($_GET['action']) {
    case "lock":
        echo json_encode([
            'status' => 'conflict',
            'message' => 'other user has lock'
        ]);
        break;
    case "extend":
        echo json_encode([
            'status' => 'error',
            'message' => 'lock lost to other user'
        ]);
        break;
    case "release":
        echo json_encode([
            'status' => 'error',
            'message' => 'error releasing lock'
        ]);
        break;
    default:
        http_response_code(400);
        echo json_encode([
            'status' => 'error',
            'message' => 'Bad Request: action=' . $_GET['action']
        ]);
        break;
}

exit(0)
?>