<?php
/**
 * @author Rafael Ostertag <rafael.ostertag@math.uzh.ch>
 */

header("Content-Type: text/json");

echo json_encode([
    'status' => "error",
    'message' => "error uploading file"
]);

