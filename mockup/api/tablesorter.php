<?php

header('Content-type: application/json');

// POST api for view saving ***
[
    $_POST['name'],
    $_POST['public'],
    $_POST['uid'],
    $_POST['tableId'],
    $_POST['view']['columnSelection'],
    $_POST['view']['filters'],
    $_POST['view']['sortList']
];
// ***

if (true) {
    // answer on view saving failure
    http_response_code(400);
    echo json_encode([
        'status' => 'error',
        'message' => '???' // sensible message
    ]);
    exit(0);
}

// answer for successful view saving
echo json_encode([
    'status' => 'success',
    'message' => ''
]);
exit(0)
?>