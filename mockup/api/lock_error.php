<?php

header('Content-type: application/json');
if (!isset($_GET['s']) ||
    !isset($_GET['action'])
) {
    http_response_code(400);
    echo json_encode([
        'status' => 'error',
        'message' => 'Bad Request'
    ]);
    exit(0);
}

echo json_encode([
    'status' => 'error',
    'message' => 'Locking error'
]);


exit(0)
?>