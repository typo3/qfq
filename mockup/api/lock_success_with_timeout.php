<?php

header('Content-type: application/json');
if (!isset($_GET['s']) ||
    !isset($_GET['action'])
) {
    http_response_code(400);
    echo json_encode([
        'status' => 'error',
        'message' => 'Bad Request'
    ]);
    exit(0);
}

switch ($_GET['action']) {
    case "lock":
        echo json_encode([
            'status' => 'success',
            'message' => 'locked',
            'lock_timeout' => 15
        ]);
        break;
    case "extend":
        echo json_encode([
            'status' => 'success',
            'message' => 'lock renewed',
            'lock_timeout' => 15
        ]);
        break;
    case "release":
        echo json_encode([
            'status' => 'success',
            'message' => 'lock released'
        ]);
        break;
    default:
        http_response_code(400);
        echo json_encode([
            'status' => 'error',
            'message' => 'Bad Request: action=' . $_GET['action']
        ]);
        break;
}

exit(0)
?>